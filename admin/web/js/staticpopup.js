
isIphone = (/iphone/gi).test(navigator.appVersion); isIpad = (/ipad/gi).test(navigator.appVersion); isAndroid = (/android/gi).test(navigator.appVersion); isTouch = isIphone || isIpad || isAndroid;


function overlayGAdget() {

    var httpprotocpl = ('https:' == document.location.protocol ? 'https://' : 'http://');
    function __apk() {
        var isIe7 = false;
        if (navigator.appVersion.indexOf("MSIE") != -1)
            isIe7 = true;
        if(window.appointyHeight === undefined)
            appointyHeight = '700';
        if(window.appointyWidth === undefined)
            appointyWidth = '900';

        if(window.ShowSchedulemeImg === undefined)
            ShowSchedulemeImg = true;
        //if showSchedulemeImg is set to false then it will override the properties below. This can be used if you want to call overlay from your own custom link.
        if(window.ScheduleMeBgImg === undefined)
            ScheduleMeBgImg = httpprotocpl+'static.appointy.com/Widget/Images/schedule-an-appointment.png';
        if(window.ScheduleMeBg === undefined)
            ScheduleMeBg = '#000000';
        if(window.ScheduleMeWidth === undefined)
            ScheduleMeWidth = '50';
        if(window.ScheduleMeHeight === undefined)
            ScheduleMeHeight = '350';
        if(window.ScheduleMePosition === undefined)
            ScheduleMePosition = 'right';

        if (window.AppointyExtraParameter === undefined)
            AppointyExtraParameter = '';




        var acn_ = document.getElementsByTagName('body')[0];

        var ae_ = __apl();
        var aco_ = document.createElement('div');
        aco_.setAttribute('id', 'appointyOverlayGdOv');
        aco_.setAttribute('style', 'position: fixed; top: 0px; left: 0px; 	filter: alpha(opacity=30);	-moz-opacity: 0.3;	opacity: 0.3; background-color: rgb(0, 0, 0); z-index: 2000000003; width: 100%; height: ' + ae_[1] + 'px; display: none;');


        if (isIe7) {
            aco_.style.position = 'absolute';
            aco_.style.left = '0px';
            aco_.style.top = '0';
            aco_.style.filter = 'alpha(opacity=30)';
            aco_.style.backgroundColor = 'rgb(0, 0, 0)';
            aco_.style.zIndex = '2000000003';
            aco_.style.width = '100%';
            aco_.style.height = ae_[1] + 'px';
            aco_.style.display = 'none';
        }

        //aco_.style.display = 'none';
        acn_.appendChild(aco_);
        //aco_.style.height= ae_[1]+'px';


        var acp_ = document.createElement('div');
        acp_.setAttribute('id', 'appointyOverlayGdIf');
        acp_.setAttribute('style', '-moz-border-radius: 5px 5px 5px 5px; -moz-box-shadow: 3px 3px 3px #CCCCCC;	-webkit-border-radius: 0px 0px 4px 4px;	-webkit-box-shadow: 3px 3px 3px #ddd;    background-color: #F4F4F4; border: 2px solid #CCCCCC; left: 50%; margin-left: -'+ appointyWidth/2 +'px;    padding: 10px; position: absolute; top: 50px; width: ' + appointyWidth + 'px; z-index: 2000000004;    display: none; background-image:url('+httpprotocpl+'static.appointy.com/Widget/Images/loading.png);background-repeat:no-repeat;background-position:center center');




        acp_.innerHTML = '<iframe src="" frameborder="0" id="appoiontyOvIfram" style="width:'+ appointyWidth +'px; height: '+ appointyHeight +'px; border: 0px;"></iframe>';

        if (isIe7) {
            acp_.style.backgroundColor = '#F4F4F4';
            acp_.style.border = '2px solid #CCCCCC';
            acp_.style.left = '50%';
            acp_.style.marginLeft = -appointyWidth / 2 + 'px';
            acp_.style.padding =  '10px';
            acp_.style.position = 'absolute';
            acp_.style.top = '50px';
            acp_.style.width = appointyWidth + 'px';
            acp_.style.zIndex = '2000000004';
            acp_.style.display = 'none';
            acp_.style.backgroundImage = 'url('+httpprotocpl+'static.appointy.com/Widget/Images/loading.png)';
            acp_.style.backgroundRepeat = 'no-repeat';
            acp_.style.backgroundPosition = 'center center';
        }


        if(isTouch){

            acp_.style.left = '0%';
            acp_.style.marginLeft = '0px';
            acp_.style.width =   '100%';

            acp_.style.padding =  '0px';
        }


        acn_.appendChild(acp_);


        if(isTouch){

            document.getElementById('appoiontyOvIfram').style.width =   '100%';
        }

        var iframClosSp = document.createElement('span');
        iframClosSp.setAttribute('style', 'background-image: url('+httpprotocpl+'static.appointy.com/Widget/Images/buttonClose1.gif); background-position: left top;        background-repeat: no-repeat; cursor: pointer; padding: 12px; position: absolute;        right: -15px; top: -15px; z-index: 2000000005;');

        if (isIe7) {
            iframClosSp.style.backgroundImage = 'url('+httpprotocpl+'static.appointy.com/Widget/Images/buttonClose1.gif)';
            iframClosSp.style.backgroundPosition = 'left top';
            iframClosSp.style.backgroundRepeat = 'no-repeat';
            iframClosSp.style.cursor = 'pointer';
            iframClosSp.style.padding = '12px';
            iframClosSp.style.position = 'absolute';
            iframClosSp.style.right = '-15px';
            iframClosSp.style.top = '-15px';
            iframClosSp.style.zIndex = '2000000005';
        }
        acp_.appendChild(iframClosSp);


        var acq_ = document.createElement('div');
        acq_.setAttribute('id', 'appScheduleMeBt');
        acq_.innerHTML = 'Appointment Scheduling Software';
        acq_.setAttribute('style', 'background-color:' + ScheduleMeBg + '; background-image: url('+ScheduleMeBgImg+');        color: #FFFFFF; cursor: pointer; height: '+ScheduleMeHeight+'px; '+ScheduleMePosition+': -3px;		margin-left: -7px; overflow: hidden; background-repeat:no-repeat; position: fixed; text-indent: -100000px; top: 25%; width: '+ScheduleMeWidth+'px; z-index: 100000;');


        if (isIe7) {
            acq_.style.backgroundColor = ScheduleMeBg;
            acq_.style.backgroundImage = 'url(' + ScheduleMeBgImg + ')';
            acq_.style.color = '#FFFFFF';
            acq_.style.cursor = 'pointer';
            acq_.style.height = ScheduleMeHeight + 'px';
            acq_.style[ScheduleMePosition] = '-3px';
            acq_.style.marginLeft = '-7px';
            acq_.style.overflow = 'hidden';
            acq_.style.backgroundRepeat = 'no-repeat';
            acq_.style.position = 'fixed';
            acq_.style.textIndent = '-10000000px';
            acq_.style.top = '25%';
            acq_.style.width = ScheduleMeWidth + 'px';
            acq_.style.zIndex = '1000000';
        }
        /*
         acq_.style.backgroundColor=ScheduleMeBg;
         acq_.style.backgroundImage='url('+ScheduleMeBgImg+')';
         acq_.style.color='#FFFFFF';
         acq_.style.cursor='pointer';
         acq_.style.height=ScheduleMeHeight+'px';
         acq_.style[ScheduleMePosition]='-3px';
         acq_.style.marginLeft='-7px';
         acq_.style.overflow='hidden';
         acq_.style.backgroundRepeat='no-repeat';
         acq_.style.position='fixed';
         acq_.style.textIndent='-10000000px';
         acq_.style.top='25%';
         acq_.style.width=ScheduleMeWidth+'px';
         acq_.style.zIndex='1000000';
         */
        //acq_.setAttribute('style', 'background-color:' + ScheduleMeBg + '; background-image: url('+ScheduleMeBgImg+');        color: #FFFFFF; cursor: pointer; height: '+ScheduleMeHeight+'px; '+ScheduleMePosition+': -3px; margin-left: -7px; overflow: hidden; background-repeat:no-repeat; position: fixed; text-indent: -100000px; top: 25%; width: '+ScheduleMeWidth+'px; z-index: 100000;');

        if(!ShowSchedulemeImg)
            acq_.style.display='none';

        acn_.appendChild(acq_);

        acq_.onclick = function () {
            if (isTouch) {
                window.open('http://localhost/boocked/customer/web/index.php/');
                return false;
            }
            //document.documentElement.scrollTop = '0px';
            acp_.style.top = ( document.body.scrollTop || document.documentElement.scrollTop) + 50 + 'px';
            acp_.style.display = ''; aco_.style.display = '';
            if (window.innerHeight < parseInt( appointyHeight)) {
                document.getElementById('appoiontyOvIfram').style.height= (window.innerHeight-60) +'px' ;
            }
            else { document.getElementById('appoiontyOvIfram').style.height= appointyHeight ; }

                document.getElementById('appoiontyOvIfram').src = 'http://localhost/boocked/customer/web/index.php/'}
            //  document.getElementById('appoiontyOvIfram').src = httpprotocpl+'' + appointy + '.appointy.com/?isgadget=1&'+AppointyExtraParameter }
        iframClosSp.onclick = function () { acp_.style.display = 'none'; aco_.style.display = 'none'; document.getElementById('appoiontyOvIfram').src='' }
    }




    function __apl() {
        var xScroll, yScroll;
        if (window.innerHeight && window.scrollMaxY) {

            xScroll = document.body.scrollWidth;
            yScroll = window.innerHeight + window.scrollMaxY;
        } else if (document.body.scrollHeight > document.body.offsetHeight) {

            xScroll = document.body.scrollWidth;
            yScroll = document.body.scrollHeight;
        } else {
            //document.documentElement.scrollHeight;
            xScroll = document.documentElement.offsetWidth;
            yScroll = document.documentElement.scrollHeight;
        }
        var windowWidth, windowHeight;
        if (self.innerHeight) {
            windowWidth = self.innerWidth;
            windowHeight = self.innerHeight;
        } else if (document.documentElement && document.documentElement.clientHeight) {
            windowWidth = document.documentElement.clientWidth;
            windowHeight = document.documentElement.clientHeight;
        } else if (document.body) {
            windowWidth = document.body.clientWidth;
            windowHeight = document.body.clientHeight;
        }
        if (yScroll < windowHeight) {
            pageHeight = windowHeight;
        } else {
            pageHeight = yScroll;
        }
        if (xScroll < windowWidth) {
            pageWidth = windowWidth;
        } else {
            pageWidth = xScroll;
        }

        arrayPageSize = new Array(pageWidth, pageHeight, windowWidth, windowHeight);
        return arrayPageSize;
    };

    __apk();
}
window.onload = function () { overlayGAdget() };

function ShowAppointyInOverlay()
{
    if(document.getElementById('appScheduleMeBt').click)
        document.getElementById('appScheduleMeBt').click();
    else
        document.getElementById('appScheduleMeBt').onclick();
}