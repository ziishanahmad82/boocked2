$(function () {
    $('[data-toggle="tooltip"]').tooltip();
});

jQuery(document).ready(function () {
    var foo = jQuery('#foo');

    function updateTime() {
        var now = new Date();
        foo.val(now.toString());
    }

    updateTime();
    setInterval(updateTime, 5000); // 5 * 1000 miliseconds
});