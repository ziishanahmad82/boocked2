<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/bootstrap.css',
        //'css/custom.css',
        'css/bootstrap.min.css',
        'css/jquery.mCustomScrollbar.min.css',
        'css/bootstrap-theme.css',
        'css/bootstrap-theme.min.css',
        'css/style.css',
        'css/normalize.css',
        'css/monthly.css',
        'css/menu_elastic.css',
        'css/font-awesome.min.css',
        'css/jquery.ui.timepicker.css',
        'css/jquery-ui-1.10.0.custom.min.css',
        'bootstrap_switch/bootstrap-switch.css',
        'https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.css',
        'https://cdn.datatables.net/1.10.12/css/jquery.dataTables.min.css',
        'https://cdn.datatables.net/buttons/1.2.1/css/buttons.dataTables.min.css',
        'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/css/bootstrap-datepicker.css',

       
        
        //'css/site.css',
    ];
    public $js = [
       
        'js/bootstrap.js',
        'js/monthly.js',
        'js/snap.svg-min.js',
        'js/main.js',
        'js/main3.js',
        'js/chart.js',
        'js/classie.js',
        'js/calendar.js',
        'js/timezones.full.js',
        'js/jquery.ui.timepicker.js',
        'js/jquery.ui.position.min.js',

        'bootstrap_switch/bootstrap-switch.js',
      //  'js/analytics.js',
        'https://cdn.datatables.net/v/dt/dt-1.10.12/datatables.min.js',
        'https://cdn.datatables.net/buttons/1.2.1/js/dataTables.buttons.min.js',
        'http://cdn.datatables.net/buttons/1.2.1/js/buttons.flash.min.js',
        '//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js',
        '//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/pdfmake.min.js',
        '//cdn.rawgit.com/bpampuch/pdfmake/0.1.18/build/vfs_fonts.js',
        '//cdn.datatables.net/buttons/1.2.1/js/buttons.html5.min.js',
        '//cdn.datatables.net/buttons/1.2.1/js/buttons.print.min.js',
       // 'js/jquery.gauge.js',
        'js/jqwidgets/jqxcore.js',
        'js/jqwidgets/jqxdraw.js',
        'js/jqwidgets/jqxgauge.js',
        'js/jquery.AshAlom.gaugeMeter-2.0.0.min.js',
        
        'js/jquery.mCustomScrollbar.min.js',
        'js/jquery.mCustomScrollbar.concat.min.js',
        'js/customjs.js',



     //   'js/jquery.mobile-1.0.min.js',




      //  'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.js',
      //  'https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js'


    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
