<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/bootstrap.css',
        //'css/custom.css',
        'css/bootstrap.min.css',
        'css/bootstrap-theme.css',
        'css/bootstrap-theme.min.css',
        'css/style.css',
        'css/normalize.css',
        'css/monthly.css',
        'css/menu_elastic.css',
        'css/font-awesome.min.css',
        'css/jquery.ui.timepicker.css',
        'css/jquery-ui-1.10.0.custom.min.css',
       
        
        //'css/site.css',
    ];
    public $js = [

        'js/bootstrap.js',
        'js/monthly.js',
        'js/main.js',
        'js/chart.js',
        'js/classie.js',
        'js/snap.svg-min.js',
        'js/calendar.js',
        'js/timezones.full.js',
        'js/jquery.ui.timepicker.js',
        'js/jquery.ui.position.min.js',
        'js/customjs.js',


    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
