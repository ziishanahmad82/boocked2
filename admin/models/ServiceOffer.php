<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "professions_list".
 *
 * @property integer $profession_id
 * @property string $profession_name
 */
class ServiceOffer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_offer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service_id','price','offer_name','discount'], 'required'],
            [[ 'service_id'], 'integer'],
            [['price'], 'integer', 'max' => 10000000],
            [['discount'], 'integer', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [

        ];
    }
    public function getservicebyid($id)
    {
        $offer = ServiceOffer::findOne(['service_id' => $id]);
        return $offer;
    }
}
