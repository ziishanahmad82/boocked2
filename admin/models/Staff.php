<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "services".
 *
 * @property integer $service_id
 * @property string $service_name
 * @property string $service_description
 * @property integer $service_duration
 * @property string $service_price
 * @property integer $service_capacity
 * @property integer $business_id
 */
class Staff extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service_name', 'service_description', 'service_duration', 'service_price', 'service_capacity'], 'required'],
            [['service_description','staff_status'], 'string'],
            [['service_duration', 'service_capacity', 'business_id'], 'integer'],
            [['service_name'], 'string', 'max' => 255],
            [['service_price'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'service_id' => 'Service ID',
            'service_name' => 'Service Name',
            'service_description' => 'Service Description',
            'service_duration' => 'Service Duration (Minutes)',
            'service_price' => 'Service Price',
            'service_capacity' => 'Service Capacity',
            'business_id' => 'Business ID',
        ];
    }
}
