<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sent_emails".
 *
 * @property integer $sent_email_id
 * @property string $to_email
 * @property string $subject
 * @property string $content
 * @property string $business_id
 * @property string $status
 * @property string $updated_at
 * @property string $created_at
 */
class SentEmails extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sent_emails';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['to_email', 'subject', 'content', 'business_id', 'status', 'updated_at', 'created_at'], 'required'],
            [['to_email', 'subject', 'content'], 'string'],
            [['business_id', 'status', 'updated_at', 'created_at'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sent_email_id' => 'Sent Email ID',
            'to_email' => 'To Email',
            'subject' => 'Subject',
            'content' => 'Content',
            'business_id' => 'Business ID',
            'status' => 'Status',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }
}
