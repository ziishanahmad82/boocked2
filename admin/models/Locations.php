<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "locations".
 *
 * @property integer $location_id
 * @property string $location_name
 * @property string $test_type
 *
 * @property CustomerReservedTimeslots[] $customerReservedTimeslots
 */
class Locations extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'locations';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['location_name', 'test_type'], 'required'],
            [['location_name'], 'string', 'max' => 255],
            [['test_type'], 'string', 'max' => 6]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'location_id' => 'Location ID',
            'location_name' => 'Location Name',
            'test_type' => 'Test Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerReservedTimeslots()
    {
        return $this->hasMany(CustomerReservedTimeslots::className(), ['location_id' => 'location_id']);
    }
}
