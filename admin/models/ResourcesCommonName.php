<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "resources_common_name".
 *
 * @property integer $rcn_id
 * @property string $sigular_name
 * @property string $plural_name
 * @property integer $site_id
 */
class ResourcesCommonName extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'resources_common_name';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['singular_name', 'plural_name' ], 'required'],
            [['site_id'], 'integer'],
            [['singular_name', 'plural_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rcn_id' => 'Rcn ID',
            'singular_name' => 'Enter Singular Name',
            'plural_name' => 'Enter Plural Name',
            'site_id' => 'Site ID',
        ];
    }
    public function commonRelatedNames()
    {
        $site_id=1;
        $modele = ResourcesCommonName::find()
            ->where(['site_id' => $site_id])
            ->one();

        return [ $modele
        ];
    }
}
