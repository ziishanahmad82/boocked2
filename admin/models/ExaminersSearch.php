<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Examiners;

/**
 * ExaminersSearch represents the model behind the search form about `app\models\Examiners`.
 */
class ExaminersSearch extends Examiners
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['examiner_id', 'location_id'], 'integer'],
            [['examiner_name', 'examiner_email', 'examiner_phone', 'test_type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Examiners::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'examiner_id' => $this->examiner_id,
            'location_id' => $this->location_id,
        ]);

        $query->andFilterWhere(['like', 'examiner_name', $this->examiner_name])
            ->andFilterWhere(['like', 'examiner_email', $this->examiner_email])
            ->andFilterWhere(['like', 'examiner_phone', $this->examiner_phone])
            ->andFilterWhere(['like', 'test_type', $this->test_type]);

        return $dataProvider;
    }
}
