<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "timezones_list".
 *
 * @property integer $id
 * @property string $timezone_location
 * @property string $gmt
 * @property integer $offset
 */
class TimezonesList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'timezones_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['offset'], 'integer'],
            [['timezone_location'], 'string', 'max' => 30],
            [['gmt'], 'string', 'max' => 11]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'timezone_location' => 'Timezone Location',
            'gmt' => 'Gmt',
            'offset' => 'Offset',
        ];
    }
}
