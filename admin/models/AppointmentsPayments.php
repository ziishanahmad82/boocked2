<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "appointments_payments".
 *
 * @property integer $ap_id
 * @property integer $to_pay
 * @property integer $additional_charges
 * @property integer $discount
 * @property integer $total
 * @property string $payment_note
 * @property integer $appointment_id
 */
class AppointmentsPayments extends \yii\db\ActiveRecord
{
    public $paymentcnt;
    public $paymentuser;
    public $payment_date;
    public $totalpayment;
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'appointments_payments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['to_pay','membership_name'], 'required'],
            [['to_pay', 'additional_charges', 'discount', 'total', 'appointment_id'], 'integer'],
            [['created_at'], 'safe'],
            [['payment_note','payment_method','created_at'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ap_id' => 'Ap ID',
            'to_pay' => 'To Pay',
            'additional_charges' => 'Additional Charges',
            'discount' => 'Discount',
            'total' => 'Total',
            'payment_note' => 'Payment Note',
            'appointment_id' => 'Appointment ID',
            'payment_method' => 'Payment Method',
        ];
    }
    public function getAppointments() {
        return  $this->hasOne(Appointments::className(), ['appointment_id' => 'appointment_id']);

    }
    public function getServices() {
        return  Appointments::hasOne(Services::className(), ['service_id' => 'service_id']);
    }
    public function getUsers() {
        return  Appointments::hasOne(Users::className(), ['id' => 'user_id']);
    }
    public function getCustomers() {
        return  $this->hasOne(Customers::className(), ['customer_id' => 'customer_id']);
    }
}
