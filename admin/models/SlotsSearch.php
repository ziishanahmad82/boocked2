<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Slots;

/**
 * SlotsSearch represents the model behind the search form about `app\models\Slots`.
 */
class SlotsSearch extends Slots
{
    /**
     * @inheritdoc
     */
    public $permit_number;
    public function rules()
    {
        return [
            [['id', 'duration'], 'integer'],
            [['calendar_type', 'slot_date', 'start_time', 'status', 'is_reserved'], 'safe'],
            [['permit_number'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Slots::find();

        $query->joinWith(['permitnumber']);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'PageSize' => 100,
            ],
        ]);

        $dataProvider->sort->attributes['permit_number'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['customers.learners_permit_number' => SORT_ASC],
            'desc' => ['customers.learners_permit_number' => SORT_DESC],
        ];

        if(@$params["SlotsSearch"]["slot_date"])
            $params["SlotsSearch"]["slot_date"]=date('Y-m-d', strtotime($params["SlotsSearch"]["slot_date"]));

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }


        $query->andFilterWhere([
            'slots.id' => $this->id,
            'slots.duration' => $this->duration,
            'slots.slot_date' => $this->slot_date,
            'slots.start_time' => $this->start_time,
            'slots.is_reserved' => $this->is_reserved,
        ]);

        $query->andFilterWhere(['like', 'slots.status', $this->status])
            ->andFilterWhere(['like', 'customers.learners_permit_number', $this->permit_number]);


        if(@stristr(Yii::$app->user->identity->user_group, 'thirdparty-')){
            $query->andFilterWhere(['=', 'slots.third_party_user_id', Yii::$app->user->identity->id]);

        } else {
            if(stristr($this->calendar_type, 'thirdparty-')){

                $third_party_id=explode('-',$this->calendar_type);
                $third_party_id=$third_party_id[1];
                //$query->andFilterWhere(['=', 'slots.calendar_type', 'Third party']);
                $query->andFilterWhere(['=', 'slots.third_party_user_id', $third_party_id]);


            } else if($this->calendar_type=='Third Party'){
                $query->andFilterWhere(['Like', 'slots.calendar_type', 'Third party']);
            } else {
                $query->andFilterWhere(['=', 'slots.calendar_type', $this->calendar_type]);
            }
        }







        return $dataProvider;
    }
}
