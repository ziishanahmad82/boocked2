<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "resouces_service".
 *
 * @property integer $rs_id
 * @property integer $service_id
 * @property integer $resource_id
 *
 * @property Services $service
 * @property Resources $resource
 */
class ResoucesService extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'resouces_service';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service_id', 'resource_id'], 'required'],
            [['service_id', 'resource_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rs_id' => 'Rs ID',
            'service_id' => 'Service ID',
            'resource_id' => 'Resource ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getService()
    {
        return $this->hasOne(Services::className(), ['service_id' => 'service_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResource()
    {
        return $this->hasOne(Resources::className(), ['resource_id' => 'resource_id']);
    }
}
