<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customize_email".
 *
 * @property integer $custom_email_id
 * @property string $email_subject
 * @property string $email_content
 * @property string $emai_type
 * @property integer $business_id
 */
class CustomizeEmail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customize_email';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email_subject', 'email_content', 'email_type', 'business_id'], 'required'],
            [['email_content'], 'string'],
            [['business_id'], 'integer'],
            [['email_subject', 'email_type'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'custom_email_id' => 'Custom Email ID',
            'email_subject' => 'Email Subject',
            'email_content' => 'Email Content',
            'email_type' => 'Email Type',
            'business_id' => 'Business ID',
        ];
    }
}
