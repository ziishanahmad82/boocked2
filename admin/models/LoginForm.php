<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\SettingBookingRules;
use app\models\Timezoneandcurrency;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $rememberMe = true;

    private $_user = false;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['username', 'password'], 'required'],
            // rememberMe must be a boolean value
            ['rememberMe', 'boolean'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        if (!$this->hasErrors()) {
            $user = $this->getUser();

            if (!$user || !$user->validatePassword($this->password)) {
                $this->addError($attribute, 'Incorrect username or password.');
            }
        }
    }

    /**
     * Logs in a user using the provided username and password.
     * @return boolean whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {

            Yii::$app->session->set('ROLESZ', $this->getUser()->business_id);
            $settingrules =   SettingBookingRules::find()->where(['business_id'=>$this->getUser()->business_id])->all();
            foreach($settingrules as $setting_rule){
              Yii::$app->session->set('bk_'.$setting_rule->bkrules_name, $setting_rule->bkrules_value);
              //  echo 'Name  :   '.$setting_rule->bkrules_name; echo 'Value  :   '.$setting_rule->bkrules_value.'<br/>';

            }
            $timezone_currency  =  Timezoneandcurrency::findOne(['business_id'=>$this->getUser()->business_id]);
            $selected_currency = CurrencyList::showcurrency_name($timezone_currency->currency);
            Yii::$app->session->set('bk_currency', $selected_currency);
            return Yii::$app->user->login($this->getUser());
           
            //return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600*24*30 : 0);
        } else {
            return false;
        }
    }

    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    public function getUser()
    {


        if ($this->_user === false) {
            $this->_user = User::findByUsername($this->username);

        }
        return $this->_user;
    }
}
