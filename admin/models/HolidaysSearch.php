<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Holidays;

/**
 * HolidaysSearch represents the model behind the search form about `app\models\Holidays`.
 */
class HolidaysSearch extends Holidays
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['holiday_id', 'holiday_span'], 'integer'],
            [['holiday_date', 'holiday_name'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Holidays::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'holiday_id' => $this->holiday_id,
            'holiday_date' => $this->holiday_date,
            'holiday_span' => $this->holiday_span,
        ]);

        $query->andFilterWhere(['like', 'holiday_name', $this->holiday_name]);

        return $dataProvider;
    }
}
