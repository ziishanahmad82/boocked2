<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "coupans".
 *
 * @property integer $coupan_id
 * @property string $coupan_code
 * @property string $coupan_title
 * @property string $coupan_description
 * @property string $image_url
 * @property string $work_over
 * @property string $service_id
 * @property string $staff_id
 * @property string $start_date
 * @property string $end_date
 * @property string $start_time
 * @property string $end_time
 * @property string $weekdays
 * @property integer $valid_for_new_customer
 * @property integer $valid_for_one_time
 * @property integer $first_booking_nums
 * @property string $updated_at
 * @property string $created_at
 */
class Coupans extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'coupans';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['coupan_code', 'coupan_title','discount_value', 'updated_at', 'created_at'], 'required'],
         //   'coupan_description', 'image_url', 'work_over', 'service_id', 'staff_id', 'start_date', 'end_date', 'start_time', 'end_time', 'weekdays', 'valid_for_new_customer', 'valid_for_one_time', 'first_booking_nums',
            [['image_url'], 'string'],
            [['start_date', 'end_date'], 'safe'],
            [['valid_for_new_customer', 'valid_for_one_time', 'first_booking_nums','discount_value','discount_unit','work_over','business_id'], 'integer'],
            [['coupan_code', 'coupan_title', 'coupan_description', 'work_over', 'service_id', 'staff_id', 'start_time', 'end_time', 'weekdays', 'updated_at', 'created_at'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'coupan_id' => 'Coupan ID',
            'coupan_code' => 'Coupan Code',
            'coupan_title' => 'Coupan Title',
            'coupan_description' => 'Coupan Description',
            'image_url' => 'Image Url',
            'work_over' => 'Work Over',
            'service_id' => 'Service ID',
            'staff_id' => 'Staff ID',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'start_time' => 'Start Time',
            'end_time' => 'End Time',
            'weekdays' => 'Weekdays',
            'valid_for_new_customer' => 'Valid For New Customer',
            'valid_for_one_time' => 'Valid For One Time',
            'first_booking_nums' => 'First Booking Nums',
            'business_id'=>'Business ID',
            'discount_value'=>'Discount Value',
            'discount_unit'=>'Discount Unit',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }
}
