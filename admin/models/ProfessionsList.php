<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "professions_list".
 *
 * @property integer $profession_id
 * @property string $profession_name
 */
class ProfessionsList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'professions_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['profession_name'], 'required'],
            [['profession_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'profession_id' => 'Profession ID',
            'profession_name' => 'Profession Name',
        ];
    }
    public function profession_name($id)
    {
        $profession = ProfessionsList::findOne($id);
        return $profession->profession_name;
    }
}
