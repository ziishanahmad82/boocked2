<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "contact_phone".
 *
 * @property integer $contact_phone_id
 * @property string $home_phone
 * @property string $work_phone
 * @property string $mobile_phone
 * @property integer $user_id
 */
class ContactPhone extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contact_phone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['home_phone', 'work_phone', 'mobile_phone', 'user_id'], 'required'],
            [['user_id'], 'integer'],
            [['home_phone', 'work_phone', 'mobile_phone'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'contact_phone_id' => 'Contact Phone ID',
            'home_phone' => 'Home Phone',
            'work_phone' => 'Work Phone',
            'mobile_phone' => 'Mobile Phone',
            'user_id' => 'User ID',
        ];
    }
}
