<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "time_slots_configuration".
 *
 * @property integer $id
 * @property string $slot_start_time
 * @property string $slot_end_time
 * @property string $applicable_from_date
 * @property string $weekdays
 * @property string $test_type
 */
class TimeSlotsConfiguration extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $duration;
    public $date_range_start;
    public $date_range_end;
    public static function tableName()
    {
        return 'time_slots_configuration';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['slot_start_time', 'slot_end_time', 'test_type'], 'required'],
            [['slot_start_time', 'slot_end_time', 'applicable_from_date'], 'safe'],
            [['weekdays'], 'string', 'max' => 255],
            [['test_type'], 'string', 'max' => 6]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'slot_start_time' => 'Start Time',
            'slot_end_time' => 'End Time',
            'applicable_from_date' => 'Date',
            'weekdays' => 'Weekdays',
            'test_type' => 'Test Type',
            'duration' => 'Duration (mins)',
            'date_range_start' => 'Start Date',
            'date_range_end' => 'End Date',
        ];
    }
}
