<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "blocked_date_times".
 *
 * @property integer $blckdate_id
 * @property string $block_date
 * @property string $blocktime
 * @property string $block_reason
 * @property integer $staff_id
 * @property integer $business_id
 */
class BlockedDateTimes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'blocked_date_times';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['block_date', 'blocktimeFrom', 'blocktimeTo', 'block_reason', 'staff_id', 'business_id'], 'required'],
            [['block_date', 'blocktimeTo','blocktimeFrom'], 'safe'],
            [['block_reason'], 'string'],
            [['staff_id', 'business_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'blckdate_id' => 'Blckdate ID',
            'block_date' => 'Block Date',
            'blocktimeFrom' => 'Blocktime From',
            'blocktimeTo'=> 'Blocktime To',
            'block_reason' => 'Block Reason',
            'staff_id' => 'Staff ID',
            'business_id' => 'Business ID',
        ];
    }
}
