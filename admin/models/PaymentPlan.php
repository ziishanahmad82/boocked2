<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "payment_plan".
 *
 * @property integer $pp_id
 * @property string $plan_name
 * @property integer $plan_amount
 * @property integer $plan_credits
 * @property string $plan_description
 * @property string $plan_status
 */
class PaymentPlan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_plan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['plan_name', 'plan_amount', 'plan_credits', 'plan_description', 'plan_status'], 'required'],
            [['plan_amount', 'plan_credits'], 'integer'],
            [['plan_description'], 'string'],
            [['plan_name'], 'string', 'max' => 255],
            [['plan_status'], 'string', 'max' => 10]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pp_id' => 'Pp ID',
            'plan_name' => 'Plan Name',
            'plan_amount' => 'Plan Amount',
            'plan_credits' => 'Plan Credits',
            'plan_description' => 'Plan Description',
            'plan_status' => 'Plan Status',
        ];

    }
    public function getActiveplans(){
       $payemnt_plan = PaymentPlan::find()->where(['plan_status'=>'active'])->all();
       return $payemnt_plan;
    }
}
