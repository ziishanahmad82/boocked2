<?php
namespace app\models;

use yii\base\Model;
use yii\web\UploadedFile;

class UploadForm extends Model
{
/**
* @var UploadedFile
*/
public $profile_image;

public function rules()
{
return [
[['profile_image'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg'],
];
}

public function upload()
{
if ($this->validate()) {
$this->profile_image->saveAs('uploads/' . $this->profile_image->baseName . '.' . $this->profile_image->extension);
return true;
} else {
return false;
}
}
}
?>