<?php

namespace app\models;

use Yii;
use app\models\ResoucesService;

/**
 * This is the model class for table "resources".
 *
 * @property integer $resource_id
 * @property string $resource_name
 * @property string $resource_status
 *
 * @property ResoucesService[] $resoucesServices
 */
class Resources extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'resources';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['resource_name', 'resource_status'], 'required'],
            [['resource_name', 'resource_status'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'resource_id' => 'Resource ID',
            'resource_name' => 'Resource Name',
            'resource_status' => 'Resource Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResoucesServices()
    {
        return $this->hasMany(ResoucesService::className(), ['resource_id' => 'resource_id']);
    }


    public static function get_resource_service($id){
        $model = ResoucesService::find()->where(["resource_id" => $id])->one();
        if(!empty($model)){
            return $model->service_id;
        }

        return null;
    }


}
