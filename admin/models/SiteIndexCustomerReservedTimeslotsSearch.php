<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CustomerReservedTimeslots;

/**
 * CustomerReservedTimeslotsSearch represents the model behind the search form about `app\models\CustomerReservedTimeslots`.
 */
class SiteIndexCustomerReservedTimeslotsSearch extends CustomerReservedTimeslots
{
    /**
     * @inheritdoc
     */
    public $dl_no;
    public $full_name;
    public $third_user;
    public function rules()
    {
        return [
            [['timeslot_id', 'customer_id', 'location_id', 'examiner_id', 'confirmed', 'third_user_id', 'tough_book_id'], 'integer'],
            [['test_date', 'test_start_time', 'test_end_time', 'test_type', 'status'], 'safe'],
            [['dl_no'], 'safe'],
            [['full_name'], 'safe'],
            [['third_user'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerReservedTimeslots::find();

        $query->joinWith(['customer', 'thirdparty']);



        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'PageSize' => false,
            ],
        ]);

        $dataProvider->sort->attributes['dl_no'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['customers.learners_permit_number' => SORT_ASC],
            'desc' => ['customers.learners_permit_number' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['full_name'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['customers.first_name' => SORT_ASC],
            'desc' => ['customers.first_name' => SORT_DESC],
        ];

        $dataProvider->sort->attributes['third_user'] = [
            // The tables are the ones our relation are configured to
            // in my case they are prefixed with "tbl_"
            'asc' => ['third_party_users.user_name' => SORT_ASC],
            'desc' => ['third_party_users.user_name' => SORT_DESC],
        ];

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'timeslot_id' => $this->timeslot_id,
            'customer_id' => $this->customer_id,
            'location_id' => $this->location_id,
            'test_date' => $this->test_date,
            'test_start_time' => $this->test_start_time,
            'test_end_time' => $this->test_end_time,
            'examiner_id' => $this->examiner_id,
            'confirmed' => $this->confirmed,
            'third_user_id' => $this->third_user_id,
            'tough_book_id' => $this->tough_book_id,

        ]);

        $today_date = date("Y-m-d");

        $query->andFilterWhere(['like', 'customer_reserved_timeslots.test_type', $this->test_type])
            ->andFilterWhere(['like', 'customer_reserved_timeslots.status', $this->status])
            ->andFilterWhere(['like', 'customers.learners_permit_number', $this->dl_no])
            ->andFilterWhere(['like', 'customers.first_name', $this->full_name])
            ->andFilterWhere(['like', 'third_party_users.user_name', $this->third_user])
            ->andFilterWhere(['=', 'customer_reserved_timeslots.confirmed', 1])
           // ->andFilterWhere(['=', 'customer_reserved_timeslots.test_date', $today_date])
            //->andFilterWhere(['=', 'customer_reserved_timeslots.third_user_id', 0])



        ;

        $get=Yii::$app->request->get();
        if(@$selected_date=$get["check"]){
            $query->andFilterWhere(['=', 'customer_reserved_timeslots.test_date', $selected_date]);
        } else {
            $query->andFilterWhere(['=', 'customer_reserved_timeslots.test_date', $today_date]);
        }


        return $dataProvider;
    }
}
