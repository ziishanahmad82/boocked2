<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Toughbooks;

/**
 * ToughbooksSearch represents the model behind the search form about `app\models\Toughbooks`.
 */
class ToughbooksSearch extends Toughbooks
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['toughbook_id'], 'integer'],
            [['toughbook_code', 'toughbook_status'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Toughbooks::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'toughbook_id' => $this->toughbook_id,
        ]);

        $query->andFilterWhere(['like', 'toughbook_code', $this->toughbook_code])
            ->andFilterWhere(['like', 'toughbook_status', $this->toughbook_status]);

        return $dataProvider;
    }
}
