<?php

namespace app\models;
use app\models\Resources;
use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "services".
 *
 * @property integer $service_id
 * @property string $service_name
 * @property string $service_description
 * @property integer $service_duration
 * @property string $service_price
 * @property integer $service_capacity
 * @property integer $business_id
 */
class Services extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $imageFile1;
    public $imageFile2;
    public $imageFile3;
    public static function tableName()
    {
        return 'services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service_name', 'service_description', 'service_duration', 'service_price', 'service_capacity'], 'required'],
            [['service_description'], 'string'],
            [['service_duration', 'service_capacity', 'business_id','service_cat_id'], 'integer'],
            [['service_name'], 'string', 'max' => 255],
            [['service_price'], 'string', 'max' => 20],
            [['service_video'], 'file','extensions' => 'mp4, mp3','maxFiles' => 1],
          //  [['imageFile1,imageFile2,imageFile3'], 'file', 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'service_id' => 'Service ID',
            'service_name' => 'Service Name',
            'service_description' => 'Service Description',
            'service_duration' => 'Duration (Min)',
            'service_price' => 'Price',
            'service_capacity' => 'Capacity',
            'business_id' => 'Business ID',
            'service_cat_id'=> 'Service Category'
        ];
    }
    public function getResource() {
        return $this->hasMany(Resources::className(), ['resource_id' => 'resource_id'])
            ->viaTable('resouces_service', ['service_id' => 'service_id']);
    }
    public function getListServices(){
       // return (new \yii\db\Query())->select(['service_id', 'service_name']);
        $site_id = Yii::$app->user->identity->business_id;
        $modele = Services::find()
            ->where(['business_id' => $site_id])
            ->all();
        return $modele;

    }

    public function getServicesList($business_id){
        // return (new \yii\db\Query())->select(['service_id', 'service_name']);
      //  $site_id = 1;
        $modele = Services::find()
            ->where(['business_id' => $business_id])
            ->all();
        return $modele;

    }
    public function upload($field_name)
    {
        if ($this->validate()) {
exit('121312312312');
            $this->$field_name->saveAs('uploads/' . $this->$field_name->baseName . '.' . $this->$field_name->extension);
            return true;
        } else {
            return false;
       }
    }

    public function getLinkedActiveServices(){
        $site_id = Yii::$app->user->identity->business_id;
        $model = Services::find()
            ->where(['business_id' => $site_id]) ->andWhere(['service_status'=>'active']) ->andWhere((['<>','linked_status', 'no']))
            ->all();

        return $model;
    }


    public function getservice_name($id){
        $model = Services::findOne(['service_id' => $id]);
           // ->where(['service_id' => $id])->one();
        if(!empty($model)) {
            return $model->service_name;
        }else {
            return 'Unknown';

        }
    }
}
