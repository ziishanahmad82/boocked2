<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "slots".
 *
 * @property integer $id
 * @property string $calendar_type
 * @property integer $duration
 * @property string $slot_date
 * @property string $start_time
 * @property string $status
 * @property string $is_reserved
 * @property integer $reserved_by_customer_id
 * @property integer $third_party_user_id
 * @property integer $batch_number
 */
class Slots extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $cnt;
    public static function tableName()
    {
        return 'slots';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['calendar_type', 'duration', 'slot_date', 'start_time'], 'required'],
            [['duration', 'reserved_by_customer_id'], 'integer'],
            [['slot_date', 'start_time'], 'safe'],
            [['calendar_type'], 'string', 'max' => 20],
            [['status'], 'string', 'max' => 15],
            [['is_reserved'], 'string', 'max' => 3],
            [['third_party_user_id'], 'integer'],
            [['batch_number'], 'integer'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'calendar_type' => 'Calendar Type',
            'duration' => 'Duration',
            'slot_date' => 'Slot Date',
            'start_time' => 'Start Time',
            'status' => 'Status',
            'is_reserved' => 'Reserved',
            'reserved_by_customer_id' => 'Customer ID',
            'batch_number' => 'Batch',
        ];
    }

    public function getPermitnumber()
    {
        //return $this->hasOne(ThirdPartyUsers::className(), ['id' => 'third_user_id']);
        return $this->hasOne(Customers::className(), ['customer_id' => 'reserved_by_customer_id']);
    }
    public function getUser()
    {
        //return $this->hasOne(ThirdPartyUsers::className(), ['id' => 'third_user_id']);
        return $this->hasOne(Users::className(), ['id' => 'third_party_user_id']);
    }

    public function calendarfilter(){
        $third_party_users=Users::find()
            ->select(['displayname', 'id'])
            ->andFilterWhere([
                'and',
                ['like', 'user_group', 'thirdparty'],

            ])
            ->all();

        $filter=array("CDL"=>"CDL","NCDL"=>"Standard","Third party standard"=>"Third Party Standard", "Third party cdl"=>"Third Party CDL");

        foreach($third_party_users as $third_party_user){
            $filter=array_merge($filter,array('thirdparty-'.$third_party_user->id=>'>> '.$third_party_user->displayname));
        }

        return $filter;
    }
    public function getrescheduledate($data){
        $new_slot_date = date('Y-m-d', strtotime($data->slot_date . ' +2 day'));
        $next_available_slots = Slots::find()->select(['*'])
            ->andFilterWhere([
                'and',
                ['>', 'slot_date', $new_slot_date],
                ['=', 'start_time', $data->start_time],
                ['=', 'calendar_type', $data->calendar_type],
                ['=', 'third_party_user_id', $data->third_party_user_id],
                ['!=', 'status', 'Reserved'],
            ])->all();
        if($next_available_slots){
            $str="<select name='r_date-{$data->id}' onchange=\"$('input[name=\'selection[]\'][value=\'$data->id\']').prop('checked', true)\">";
            $str.="<option value='0'>Reschedule to : </option>";
            foreach($next_available_slots as $next_available_slot){
                $str.="<option value='{$next_available_slot->id}'>".date('m-d-Y', strtotime($next_available_slot->slot_date))."</option>";
            }
            $str.="</select>";
        } else {
            $str="No slots available.";
        }

        return $str;
    }
}
