<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "resources".
 *
 * @property integer $resource_id
 * @property string $resource_name
 * @property string $resource_status
 * @property string $linked_status
 * @property string $services_data
 *
 * @property ResoucesService[] $resoucesServices
 */
class Resources extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'resources';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['resource_name', 'linked_status', 'services_data'], 'required'],
            [['resource_status', 'services_data','business_id'], 'string'],
            [['resource_name', 'linked_status'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'resource_id' => 'Resource ID',
            'resource_name' => 'Resource Name',
            'resource_status' => 'Resource Status',
            'linked_status' => 'Linked Status',
            'services_data' => 'Services Data',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResoucesServices()
    {
        return $this->hasMany(ResoucesService::className(), ['resource_id' => 'resource_id']);
    }
   


    public static function get_resource_service($id){
        $model = ResoucesService::find()->where(["resource_id" => $id])->one();
        if(!empty($model)){
            return $model->service_id;
        }

        return null;
    }

}
