<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customer_reserved_timeslots".
 *
 * @property integer $timeslot_id
 * @property integer $customer_id
 * @property integer $location_id
 * @property string $test_date
 * @property string $test_start_time
 * @property string $test_end_time
 * @property integer $examiner_id
 * @property string $test_type
 * @property integer $confirmed
 * @property integer $third_user_id
 *
 * @property Customers $customer
 * @property Locations $location
 */
class CustomerReservedTimeslots extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $cnt;
    public static function tableName()
    {
        return 'customer_reserved_timeslots';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'location_id', 'test_date', 'test_start_time', 'test_end_time', 'examiner_id', 'test_type'], 'required'],
            [['customer_id', 'location_id', 'examiner_id', 'confirmed', 'third_user_id', 'tough_book_id'], 'integer'],
            [['test_date', 'test_start_time', 'test_end_time'], 'safe'],
            [['test_type'], 'string', 'max' => 6]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'timeslot_id' => 'Timeslot ID',
            'customer_id' => 'Customer ID',
            'location_id' => 'Location',
            'test_date' => 'Test Date',
            'test_start_time' => 'Test Start Time',
            'test_end_time' => 'Test End Time',
            'examiner_id' => 'Examiner ID',
            'test_type' => 'Test Type',
            'confirmed' => 'Confirmed',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Customers::className(), ['customer_id' => 'customer_id']);
    }

    public function getThirdparty()
    {
        //return $this->hasOne(ThirdPartyUsers::className(), ['id' => 'third_user_id']);
        return $this->hasOne(Users::className(), ['id' => 'third_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLocation()
    {
        return $this->hasOne(Locations::className(), ['location_id' => 'location_id']);
    }

    public function customer_status_dropdown($current_value, $select_id){
        $str_return='<select name="status" id="'.$select_id.'">
                                        <option value="waiting for check in">Waiting for check in</option>
                                        <option value="checked in">Checked in</option>
                                        <option value="no show">No Show</option>
                                        <option value="cancelled">Cancelled</option>
                                        <option value="non-compliant">Non-compliant</option>
                                        <option value="passed">Passed</option>
                                        <option value="failed">Failed</option>
                                    </select>';
        $str_return='<select name="status" id="'.$select_id.'">
                                        <option value=""></option>
                                        <option value="Applicant Non Compliance">Applicant Non Compliance</option>
                                        <option value="Vehicle Non Compliance">Vehicle Non Compliance</option>
                                        <option value="No Show">No Show</option>
                                        <option value="Administrative Cancel">Administrative Cancel</option>


                                    </select>';
        return $str_return;
    }
}
