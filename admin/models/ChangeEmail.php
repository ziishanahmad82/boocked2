<?php
namespace app\models;

use app\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class ChangeEmail extends Model
{

    public $email;


    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [

            'email'=>'Email',

        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
           

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => Yii::t('app','This email address has already been taken.')],

          
        ];
    }



    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */

}
