<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "gift_certificates".
 *
 * @property integer $gc_id
 * @property integer $gift_value
 * @property integer $gift_selling_price
 * @property integer $gift_validity
 * @property integer $sale_limit
 * @property integer $gift_sale_limit
 * @property string $gift_image
 * @property integer $gift_status
 * @property integer $business_id
 * @property string $created_at
 * @property string $updated_at
 */
class GiftCertificates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'gift_certificates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['gift_value', 'gift_selling_price', 'gift_validity', 'sale_limit', 'gift_sale_limit', 'gift_image', 'gift_status', 'business_id', 'created_at', 'updated_at'], 'required'],
            [['gift_value', 'gift_selling_price', 'gift_validity', 'sale_limit', 'gift_sale_limit', 'gift_status', 'business_id'], 'integer'],
            [['gift_image','gift_validity_date','gift_validity_day','purchase_window','purchase_window_date', 'created_at', 'updated_at'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'gc_id' => 'Gc ID',
            'gift_value' => 'Gift Value',
            'gift_selling_price' => 'Gift Selling Price',
            'gift_validity' => 'Gift Validity',
            'sale_limit' => 'Sale Limit',
            'gift_sale_limit' => 'Gift Sale Limit',
            'gift_image' => 'Gift Image',
            'gift_status' => 'Gift Status',
            'business_id' => 'Business ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
