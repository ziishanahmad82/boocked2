<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "sold_gift_certificates".
 *
 * @property integer $sgc_id
 * @property string $to_name
 * @property string $to_email
 * @property string $from_name
 * @property string $from_email
 * @property string $message
 * @property integer $amount
 * @property integer $business_id
 * @property string $updated_at
 * @property string $created_at
 */
class SoldGiftCertificates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'value' => new Expression('NOW()'),
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    public static function tableName()
    {
        return 'sold_gift_certificates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['to_name', 'to_email', 'from_name', 'from_email', 'message', 'amount', 'business_id','gc_id', 'updated_at', 'created_at'], 'required'],
            [['amount', 'business_id'], 'integer'],
            [['to_name', 'to_email', 'from_name', 'certificate_code', 'purchased_at','from_email', 'message', 'updated_at', 'created_at'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'sgc_id' => 'Sgc ID',
            'to_name' => 'To Name',
            'to_email' => 'To Email',
            'from_name' => 'From Name',
            'from_email' => 'From Email',
            'message' => 'Message',
            'amount' => 'Amount',
            'business_id' => 'Business ID',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }
    public function giftrecord($id){
        $model = GiftCertificates::findOne($id);
        if($model){
            return $model;

        }
    }
}
