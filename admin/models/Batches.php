<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "batches".
 *
 * @property integer $batch_id
 * @property string $start_date
 * @property string $end_date
 * @property string $calendar_type
 */
class Batches extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'batches';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['start_date', 'end_date', 'calendar_type'], 'required'],
            [['start_date', 'end_date'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'batch_id' => 'Batch ID',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'calendar_type' => 'Calendar Type',
        ];
    }
}
