<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
/**
 * This is the model class for table "email_templates".
 *
 * @property integer $et_id
 * @property string $template_name
 * @property string $template_subject
 * @property string $template_content
 * @property integer $business_id
 * @property string $created_at
 * @property string $updated_at
 */
class EmailTemplates extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'value' => new Expression('NOW()'),
                'updatedAtAttribute' => 'updated_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    public static function tableName()
    {
        return 'email_templates';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['template_name', 'template_subject', 'template_content', 'business_id', 'created_at', 'updated_at'], 'required'],
            [['template_content'], 'string'],
            [['business_id'], 'integer'],
            [['template_name', 'template_subject', 'created_at', 'updated_at'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'et_id' => 'Et ID',
            'template_name' => 'Template Name',
            'template_subject' => 'Template Subject',
            'template_content' => 'Template Content',
            'business_id' => 'Business ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
