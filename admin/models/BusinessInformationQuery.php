<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[BusinessInformation]].
 *
 * @see BusinessInformation
 */
class BusinessInformationQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return BusinessInformation[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return BusinessInformation|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}