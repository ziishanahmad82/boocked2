<?php

namespace app\models;

use Yii;
use yii\validators\Validator;

/**
 * This is the model class for table "restricted_domains".
 *
 * @property integer $rd_id
 * @property string $domain_name
 * @property integer $business_id
 */
class RestrictedDomains extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'restricted_domains';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['domain_name', 'business_id'], 'required'],
            ['domain_name', 'unique', 'targetAttribute' => ['domain_name', 'business_id']],
            [['domain_name'], 'string'],
            [['business_id'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'rd_id' => 'Rd ID',
            'domain_name' => 'Domain Name',
            'business_id' => 'Business ID',
        ];
    }
}
