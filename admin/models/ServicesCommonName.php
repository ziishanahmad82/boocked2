<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "services_common_name".
 *
 * @property integer $scn_id
 * @property string $singular_name
 * @property string $plural_name
 * @property integer $site_id
 */
class ServicesCommonName extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'services_common_name';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['singular_name', 'plural_name'], 'required'],
            [['business_id'], 'integer'],
            [['singular_name', 'plural_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'scn_id' => 'Scn ID',
            'singular_name' => 'Enter singular form ',
            'plural_name' => 'Enter plural form ',
            'business_id' => 'Business ID',
        ];
    }
    public function commonRelatedNames()
    {
        $site_id=Yii::$app->user->identity->business_id;
        $modele = ServicesCommonName::find()
            ->where(['business_id' => $site_id])
            ->one();

        return [ $modele
        ];
    }
}
