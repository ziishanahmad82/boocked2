<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "payment_transactions".
 *
 * @property integer $pt_id
 * @property integer $payment_amount
 * @property string $txn_id
 * @property string $payment_date
 * @property integer $user_id
 * @property integer $business_id
 * @property string $datetime
 */
class PaymentTransactions extends \yii\db\ActiveRecord
{


    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'datetime',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_transactions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_amount', 'txn_id', 'payment_date', 'user_id', 'business_id'], 'required'],
            [['payment_amount', 'user_id', 'business_id'], 'integer'],

            [['txn_id', 'payment_date','datetime'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pt_id' => 'Pt ID',
            'payment_amount' => 'Payment Amount',
            'txn_id' => 'Txn ID',
            'payment_date' => 'Payment Date',
            'user_id' => 'User ID',
            'business_id' => 'Business ID',
            'datetime' => 'Datetime',
        ];
    }
}
