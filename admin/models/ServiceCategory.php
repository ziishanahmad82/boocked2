<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "service_category".
 *
 * @property integer $service_cat_id
 * @property string $service_category_name
 * @property integer $business_id
 */
class ServiceCategory extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service_category_name', 'business_id'], 'required'],
            [['business_id'], 'integer'],
            [['service_category_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'service_cat_id' => 'Service Cat ID',
            'service_category_name' => 'Service Category Name',
            'business_id' => 'Business ID',
        ];
    }

    /**
     * @inheritdoc
     * @return ServiceCategoryQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new ServiceCategoryQuery(get_called_class());
    }
}
