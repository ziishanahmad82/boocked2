<?php
namespace app\models;

use app\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $displayname;
    public $email;
    public $password;
    public $password_repeat;
    public $user_group;
    public $business_id;
    public $status;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('app', 'User name'),
            'displayname' => Yii::t('app', 'Full name'),
            'email' => Yii::t('app', 'Email address'),
            'password' => Yii::t('app', 'Password'),
            'password_repeat' => Yii::t('app', 'Repeat Password'),
            'user_group' => Yii::t('app', 'User Group'),

        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => Yii::t('app','This username has already been taken.')],
            ['username', 'string', 'min' => 4, 'max' => 20],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => Yii::t('app','This email address has already been taken.')],

            [['password','password_repeat'], 'required'],
            [['password','password_repeat'], 'string', 'min' => 4],
            [['password'], 'in', 'range'=>['password','Password','Password123','123456','12345678','letmein','monkey'] ,'not'=>true, 'message'=>Yii::t('app', 'You cannot use any really obvious passwords')],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message' => Yii::t("app", "The passwords must match")],
            [['user_group'], 'string', 'max' => 20],
            [['displayname'], 'string', 'max' => 50]
        ];
    }



    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
            $user = new User();
            $user->username = $this->username;
            $user->displayname = $this->displayname;
            $user->email = $this->email;
            $user->business_id = $this->business_id;
            $user->user_group = $this->user_group;
            //$user->status = 0;
            $user->setPassword($this->password);
            $key=$user->generateAuthKey();
            $user->save();
            $auth_assignment=new AuthAssignment();
            $auth_assignment->item_name=$user->user_group;
            echo $auth_assignment->user_id="$user->id";
            if(!$auth_assignment->save()){
                var_dump($auth_assignment->getErrors());
                exit();
            }
            /*$msg = "Please click on below link to verify your account\n http://boocked.com/admin/web/index.php/site/verify?code=".$key;
            mail("majidfiaz53@hotmail.com","My subject",$msg);

            $to = "majidfiaz53@hotmail.com";
            $subject = "Verify Email";
            $txt = "Please click on below link to verify your account\n http://boocked.com/admin/web/index.php/site/verify?code=".$key;;
            $headers = "From: majidcust@gmail.com" . "\r\n" .
                "CC: itdsol@outlook.com";

            mail($to,$subject,$txt,$headers);*/
            return $user;
        }

        return null;
    }
}
