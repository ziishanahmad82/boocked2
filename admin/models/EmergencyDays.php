<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "emergency_days".
 *
 * @property integer $holiday_id
 * @property string $holiday_date
 * @property string $holiday_name
 * @property integer $holiday_span
 */
class EmergencyDays extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emergency_days';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['holiday_date', 'holiday_name', 'holiday_span'], 'required'],
            [['holiday_date'], 'safe'],
            [['holiday_span'], 'integer'],
            [['holiday_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'holiday_id' => 'ID',
            'holiday_date' => 'Emergency Date',
            'holiday_name' => 'Emergency Name',
            'holiday_span' => 'Emergency Span (days)',
        ];
    }
}
