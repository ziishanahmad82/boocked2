<?php
namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Users;

class PasswordForm extends Model{
    public $oldpass;
    public $newpass;
    public $repeatnewpass;

    public function rules(){
        return [
            [['oldpass','newpass','repeatnewpass'],'required'],
            ['oldpass','findPasswords'],
            ['repeatnewpass','compare','compareAttribute'=>'newpass'],
        ];
    }

    public function findPasswords($attribute, $params){
        $user = Users::find()->where([
            'username'=>Yii::$app->user->identity->username
        ])->one();
         $authkey = $user->auth_key;
         $hash= $user->password_hash;
        if (!Yii::$app->getSecurity()->validatePassword($this->oldpass, $hash)) {
            $this->addError($attribute,'<span style="color:red ">Old password is not correct</span>');
        } 


    }

    public function attributeLabels(){
        return [
            'oldpass'=>'Old Password',
            'newpass'=>'New Password',
            'repeatnewpass'=>'Repeat New Password',
        ];
    }


}