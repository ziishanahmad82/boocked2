<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TimeSlotsConfiguration;

/**
 * TimeSlotsConfigurationSearch represents the model behind the search form about `app\models\TimeSlotsConfiguration`.
 */
class TimeSlotsConfigurationSearch extends TimeSlotsConfiguration
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['slot_start_time', 'slot_end_time', 'applicable_from_date', 'weekdays', 'test_type'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TimeSlotsConfiguration::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'slot_start_time' => $this->slot_start_time,
            'slot_end_time' => $this->slot_end_time,
            'applicable_from_date' => $this->applicable_from_date,
        ]);

        $query->andFilterWhere(['like', 'weekdays', $this->weekdays])
            ->andFilterWhere(['like', 'test_type', $this->test_type]);

        return $dataProvider;
    }
}
