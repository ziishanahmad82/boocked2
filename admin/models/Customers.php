<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "customers".
 *
 * @property integer $customer_id
 * @property string $learners_permit_number
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $date_of_birth
 * @property string $email
 * @property string $cell_phone
 * @property string $test_type
 * @property string $payment_terms
 * @property string $status
 * @property integer $confirmed
 * @property integer $third_user_id
 *
 * @property CustomerReservedTimeslots[] $customerReservedTimeslots
 */
class Customers extends \yii\db\ActiveRecord
{
    
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'createdat',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['future_booking', 'first_name', 'middle_name', 'last_name', 'date_of_birth', 'email', 'cell_phone', 'third_user_id'], 'required'],
            [['date_of_birth'], 'safe'],
            [['confirmed', 'third_user_id'], 'integer'],
            [['customer_zip'], 'integer','max' => 9999999],
            [['future_booking', 'email', 'customer_region', 'customer_country', 'customer_city', 'customer_address'], 'string', 'max' => 255],
            [['first_name', 'middle_name', 'last_name','createdat'], 'string', 'max' => 100],
            [['cell_phone', 'payment_terms', 'work_phone', 'home_phone','business_id','verify_status'], 'string', 'max' => 50],

            [['status','sort_order'], 'string', 'max' => 6]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'customer_id' => 'Customer ID',
            'future_booking' => 'Future Booking',
            'first_name' => 'First Name',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'date_of_birth' => 'Date Of Birth',
            'email' => 'Email',
            'cell_phone' => 'Cell Phone',
            'payment_terms' => 'Payment_term',
            'confirmed' => 'Confirmed',

        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAppointments() {
        return  $this->hasMany(Appointments::className(), ['customer_id' => 'customer_id']);
    }
    public function getCustomerReservedTimeslots()
    {
        return $this->hasMany(CustomerReservedTimeslots::className(), ['customer_id' => 'customer_id']);
    }
}
