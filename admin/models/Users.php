<?php

namespace app\models;

use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $displayname
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $role
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @var profile_image
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $profile_image;
    public $password;
    public $password_repeat;
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['username', 'auth_key', 'displayname', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],
            [['role', 'status', 'created_at', 'updated_at','business_id'], 'integer'],
            [['username', 'password_hash', 'password_reset_token', 'email','mobile_phone'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['displayname'], 'string', 'max' => 50],
            [['profile_image'], 'string'],
            ['username', 'filter', 'filter' => 'trim'],

            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => Yii::t('app','This username has already been taken.')],


            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => Yii::t('app','This email address has already been taken.')],
         //   [['old_password', 'new_password', 'repeat_password'], 'required', 'on' => 'changePwd'],
         //   [['old_password', 'findPasswords'], 'on' => 'changePwd'],
         // ['repeat_password', 'compare', 'compareAttribute'=>'new_password', 'on'=>'changePwd'],
    
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'displayname' => 'Examiner',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'role' => 'Role',
            'profile_image' => 'Profile Image',
            'status' => 'Status',
            'mobile_phone'=>'Phone',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
    public function uploadFile() {
        // get the uploaded file instance
        $image = UploadedFile::getInstance($this, 'profile_image');


        // if no image was uploaded abort the upload
        if (empty($image)) {
            return false;
        }

        // generate random name for the file
        $this->pic = time(). '.' . $image->extension;

        // the uploaded image instance
        return $image;
    }

    public function getUploadedFile() {
        // return a default image placeholder if your source avatar is not found
        $pic = isset($this->pic) ? $this->pic : 'default.png';
        return Yii::$app->params['fileUploadUrl'] . $pic;
    }
    public function upload()
    {
        if ($this->validate()) {
            $this->imageFile->saveAs('uploads/' . $this->imageFile->baseName . '.' . $this->imageFile->extension);
            return true;
        } else {
            return false;
        }
    }
    public function getusername($id){
        $model = Users::find()
            ->where(['id' => $id])->all();
        if(!empty($model)) {
            return $model[0]->username;
        }else {
            return 'Unknown';

        }
    }
    public function getActiveUsers($business_id){
            $model = Users::find()
            ->where(['business_id' => $business_id,'status'=>10])
            ->all();
        return $model;

    }



}
