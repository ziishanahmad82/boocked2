<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "currency_list".
 *
 * @property integer $currency_id
 * @property string $country
 * @property string $currency
 * @property string $code
 * @property string $symbol
 */
class CurrencyList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['country', 'currency', 'code', 'symbol'], 'required'],
            [['country', 'currency', 'code', 'symbol'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'currency_id' => 'Currency ID',
            'country' => 'Country',
            'currency' => 'Currency',
            'code' => 'Code',
            'symbol' => 'Symbol',
        ];
    }
    public function showcurrency_name($id)
    {
        $model = CurrencyList::findOne($id);
         return  $model->code.'('.$model->symbol.')';
    }

}
