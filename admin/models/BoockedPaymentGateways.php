<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "boocked_payment_gateways".
 *
 * @property integer $pg_id
 * @property string $api_login
 * @property string $transaction_key
 * @property string $hash_value
 * @property string $gateway_type
 * @property integer $business_id
 * @property integer $pg_status
 */
class BoockedPaymentGateways extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'boocked_payment_gateways';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['api_login', 'transaction_key', 'hash_value', 'gateway_type', 'business_id'], 'required'],
            [['business_id', 'pg_status'], 'integer'],
            [['api_login', 'transaction_key', 'hash_value', 'gateway_type'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pg_id' => 'Pg ID',
            'api_login' => 'Api Login',
            'transaction_key' => 'Transaction Key',
            'hash_value' => 'Hash Value',
            'gateway_type' => 'Gateway Type',
            'business_id' => 'Business ID',
            'pg_status' => 'Pg Status',
        ];
    }
}
