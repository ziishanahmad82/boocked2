<?php

namespace app\controllers;

use app\models\BusinessInformation;
use Yii;
use app\models\CustomizeEmail;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * CustomizeController implements the CRUD actions for CustomizeEmail model.
 */
class CustomizeController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomizeEmail models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => CustomizeEmail::find(),

        ]);
        $business_id = Yii::$app->user->identity->business_id;
        $business_information = BusinessInformation::findOne($business_id);
       // ::find()->where(['business_id'=>1])->all()
        $booking_confirmation = CustomizeEmail::find()->where(['email_type'=>'booking_confirmation','business_id'=>$business_id])->one();
        if(empty($booking_confirmation)){
            $booking_confirmation = CustomizeEmail::find()->where(['email_type'=>'booking_confirmation','business_id'=>0])->one();
        }
        $waiting_approval = CustomizeEmail::find()->where(['email_type'=>'waiting_approval','business_id'=>$business_id])->one();
        if(empty($waiting_approval)){
        $waiting_approval = CustomizeEmail::find()->where(['email_type'=>'waiting_approval','business_id'=>0])->one();
        }
        $approved_appointment = CustomizeEmail::find()->where(['email_type'=>'approved_appointment','business_id'=>$business_id])->one();
        if(empty($approved_appointment)){
            $approved_appointment = CustomizeEmail::find()->where(['email_type'=>'approved_appointment','business_id'=>0])->one();
        }
        $appointment_denied = CustomizeEmail::find()->where(['email_type'=>'appointment_denied','business_id'=>$business_id])->one();
        if(empty($appointment_denied)){
            $appointment_denied = CustomizeEmail::find()->where(['email_type'=>'appointment_denied','business_id'=>0])->one();
        }
        $appointment_reschedule = CustomizeEmail::find()->where(['email_type'=>'appointment_reschedule','business_id'=>$business_id])->one();
        if(empty($appointment_reschedule)){
            $appointment_reschedule = CustomizeEmail::find()->where(['email_type'=>'appointment_reschedule','business_id'=>0])->one();
        }
        $appointment_cancel = CustomizeEmail::find()->where(['email_type'=>'appointment_cancel','business_id'=>$business_id])->one();
        if(empty($appointment_cancel)){
            $appointment_cancel = CustomizeEmail::find()->where(['email_type'=>'appointment_cancel','business_id'=>0])->one();
        }
        $new_registration = CustomizeEmail::find()->where(['email_type'=>'new_registration ','business_id'=>$business_id])->one();
        if(empty($new_registration )){
            $new_registration = CustomizeEmail::find()->where(['email_type'=>'new_registration','business_id'=>0])->one();
        }
        $reminder = CustomizeEmail::find()->where(['email_type'=>'reminder ','business_id'=>$business_id])->one();
        if(empty($reminder )){
            $reminder = CustomizeEmail::find()->where(['email_type'=>'reminder','business_id'=>0])->one();
        }
        $thank_you = CustomizeEmail::find()->where(['email_type'=>'thank_you','business_id'=>$business_id])->one();
        if(empty($thank_you )){
            $thank_you = CustomizeEmail::find()->where(['email_type'=>'thank_you','business_id'=>0])->one();
        }



        return $this->render('index', [
            'business_information'=>$business_information,
            'dataProvider' => $dataProvider,
            'booking_confirmation' => $booking_confirmation,
            'waiting_approval'=>$waiting_approval,
            'approved_appointment'=>$approved_appointment,
            'appointment_denied' =>$appointment_denied,
            'appointment_reschedule'=>$appointment_reschedule,
            'appointment_cancel'=>$appointment_cancel,
            'new_registration'=>$new_registration,
            'reminder'=>$reminder,
            'thank_you'=>$thank_you

        ]);
    }

    /**
     * Displays a single CustomizeEmail model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CustomizeEmail model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CustomizeEmail();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->custom_email_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CustomizeEmail model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->custom_email_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CustomizeEmail model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    public function actionSendbooking_confirmation ()
    {
        $business_id = Yii::$app->user->identity->business_id;
        $booking_confirmation = CustomizeEmail::find()->where(['email_type' => 'booking_confirmation', 'business_id' => $business_id])->one();
        if (empty($booking_confirmation)) {
            $booking_confirmation = CustomizeEmail::find()->where(['email_type' => 'booking_confirmation', 'business_id' => 0])->one();
        }
    }

    public function actionSendwaiting_approval(){
        $business_id = Yii::$app->user->identity->business_id;
            $waiting_approval = CustomizeEmail::find()->where(['email_type' => 'waiting_approval', 'business_id' => $business_id])->one();
            if (empty($waiting_approval)) {
                $waiting_approval = CustomizeEmail::find()->where(['email_type' => 'waiting_approval', 'business_id' => 0])->one();
            }
    }
    public function actionSendapproved_appointment()
    {
        $business_id = Yii::$app->user->identity->business_id;
        $approved_appointment = CustomizeEmail::find()->where(['email_type' => 'approved_appointment', 'business_id' => $business_id])->one();
        if (empty($approved_appointment)) {
            $approved_appointment = CustomizeEmail::find()->where(['email_type' => 'approved_appointment', 'business_id' => 0])->one();
        }
    }
    public function actionSendappointment_denied()
    {
        $business_id = Yii::$app->user->identity->business_id;
        $appointment_denied = CustomizeEmail::find()->where(['email_type' => 'appointment_denied', 'business_id' => $business_id])->one();
        if (empty($appointment_denied)) {
            $appointment_denied = CustomizeEmail::find()->where(['email_type' => 'appointment_denied', 'business_id' => 0])->one();
        }
    }
    public function actionSendappointment_reschedule(){
        $business_id = Yii::$app->user->identity->business_id;
        $appointment_reschedule = CustomizeEmail::find()->where(['email_type'=>'appointment_reschedule','business_id'=>$business_id])->one();
        if(empty($appointment_reschedule)){
            $appointment_reschedule = CustomizeEmail::find()->where(['email_type'=>'appointment_reschedule','business_id'=>0])->one();
        }
    }
    public function Sendappointment_cancel(){
        $business_id = Yii::$app->user->identity->business_id;
        $appointment_cancel = CustomizeEmail::find()->where(['email_type'=>'appointment_cancel','business_id'=>$business_id])->one();
    //    print_r($appointment_cancel);
    //    exit();
        if(empty($appointment_cancel)){
            $appointment_cancel = CustomizeEmail::find()->where(['email_type'=>'appointment_cancel','business_id'=>0])->one();
        }
    }
    public function actionSendnew_registration(){
        $business_id = Yii::$app->user->identity->business_id;
        $new_registration = CustomizeEmail::find()->where(['email_type'=>'new_registration ','business_id'=>$business_id])->one();
        if(empty($new_registration )){
            $new_registration = CustomizeEmail::find()->where(['email_type'=>'new_registration','business_id'=>0])->one();
        }
    }
    public function actionSendreminder(){
        $business_id = Yii::$app->user->identity->business_id;
        $reminder = CustomizeEmail::find()->where(['email_type'=>'reminder ','business_id'=>$business_id])->one();
        if(empty($reminder )){
            $reminder = CustomizeEmail::find()->where(['email_type'=>'reminder','business_id'=>0])->one();
        }
    }
    public function actionSendthank_you($apnt_id){
        return 1;
        $thank_you = CustomizeEmail::find()->where(['email_type'=>'thank_you','business_id'=>$business_id])->one();
        if(empty($thank_you )){
            $thank_you = CustomizeEmail::find()->where(['email_type'=>'thank_you','business_id'=>0])->one();
        }

    }

    /**
     * Finds the CustomizeEmail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomizeEmail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionCustomize_emailvalidate(){
        $model = new CustomizeEmail();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;

        }
        return ActiveForm::validate($model);

    }
    public function actionUpdate_customemail(){

        $business_id = Yii::$app->user->identity->business_id;

       $model  =CustomizeEmail::find()->where(['business_id'=>$business_id,'email_type'=>$_POST['CustomizeEmail']['email_type']])->one();
        if(empty($model)){
            $model = new CustomizeEmail();
        }
        $request = \Yii::$app->getRequest();
        if ($model->load(Yii::$app->request->post())) {

            $model->business_id= $business_id;
            if($model->save()){
                return 1;

            }


        }
       // return ActiveForm::validate($model);

    }

    protected function findModel($id)
    {
        if (($model = CustomizeEmail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
