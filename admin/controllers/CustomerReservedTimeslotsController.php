<?php

namespace app\controllers;

use app\models\BusinessInformation;
use app\models\CustomizeEmail;
use Yii;
use app\models\CustomerReservedTimeslots;
use app\models\CustomerReservedTimeslotsSearch;
use app\models\Customers;
use app\models\Holidays;
use app\models\Services;
use yii\helpers\Json;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \DateTime;
use app\models\SiteIndexCustomerReservedTimeslotsSearch;
//use app\models\SiteIndexTab2CustomerReservedTimeslotsSearch;
//use app\models\SiteIndexTab3CustomerReservedTimeslotsSearch;
//use app\models\SiteIndexTab4CustomerReservedTimeslotsSearch;
use yii\data\ActiveDataProvider;
use app\models\Appointments;
use app\models\AppointmentsPayments;
use app\models\Users;
use app\models\WeeklyRecurringTime;
/**
 * CustomerReservedTimeslotsController implements the CRUD actions for CustomerReservedTimeslots model.
 */
class CustomerReservedTimeslotsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomerReservedTimeslots models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerReservedTimeslotsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);



        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionIndex2(){

        
     $business_id = Yii::$app->user->identity->business_id;
        $reserved_timeslot = new CustomerReservedTimeslots();
        $reserved_timeslots = $reserved_timeslot->find()->all();
        $appointments = new Appointments();
        $appointments_slots = $appointments->find()->where(['business_id'=>$business_id])->all();
        $staffList = Users::find()->where(['business_id'=>$business_id])->all();
      //  $query = Appointments::find()->joinWith(['users']);

     //   $items = $query->select(['*'])->where(['appointments.business_id'=>Yii::$app->user->identity->business_id])->all();

        //= Yii::$app->user->identity->business_id;
        $searchModel = new CustomerReservedTimeslotsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
    

        return $this->render('index2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'reserved_timeslot'=>$reserved_timeslots,
            'appointments'=>$appointments_slots,
            'staffList'=>$staffList
        ]);
    }
    public function actionDashboardCalendar()
    {
        $reserved_timeslot = new CustomerReservedTimeslots();
        $reserved_timeslots = $reserved_timeslot->find()->all();
        $appointments = new Appointments();
        $appointments_slots = $appointments->find()->where(['business_id'=>Yii::$app->user->identity->business_id])->all();
        //  $query = Appointments::find()->joinWith(['users']);

        //   $items = $query->select(['*'])->where(['appointments.business_id'=>Yii::$app->user->identity->business_id])->all();

        //= Yii::$app->user->identity->business_id;
        $searchModel = new CustomerReservedTimeslotsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->renderAjax('index2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'reserved_timeslot'=>$reserved_timeslots,
            'appointments'=>$appointments_slots,
        ]);
    }

    public function actionForm2($time,$date)
    {
        $get=Yii::$app->request->get();


        $this->layout = 'noheader';
        if (!Yii::$app->user->isGuest) {
            $services_model = new Services();
            $business_id = Yii::$app->user->identity->business_id;
            $service_list = $services_model->getServicesList($business_id);
            $staff = new Users();
            $active_staff = $staff->getActiveUsers($business_id);
            
            $searchModel = new SiteIndexCustomerReservedTimeslotsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $count1=$dataProvider->getTotalCount();

        //    $searchModel2 = new SiteIndexTab2CustomerReservedTimeslotsSearch();
       //     $dataProvider2 = $searchModel2->search(Yii::$app->request->queryParams);
       //     $count2=$dataProvider2->getTotalCount();

       //     $searchModel3 = new SiteIndexTab3CustomerReservedTimeslotsSearch();
      //      $dataProvider3 = $searchModel3->search(Yii::$app->request->queryParams);
      //      $count3=$dataProvider3->getTotalCount();

        //    $searchModel4 = new SiteIndexTab4CustomerReservedTimeslotsSearch();
       //     $dataProvider4 = $searchModel4->search(Yii::$app->request->queryParams);
      //      $count4=$dataProvider4->getTotalCount();

            return $this->render('_form2', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'count1' => $count1,
          //      'searchModel2' => $searchModel2,
           //     'dataProvider2' => $dataProvider2,
            //    'count2' => $count2,
            //    'searchModel3' => $searchModel3,
            //    'dataProvider3' => $dataProvider3,
            //    'count3' => $count3,
            //    'searchModel4' => $searchModel4,
           //     'dataProvider4' => $dataProvider4,
            //    'count4' => $count4,
                'service_list'=> $service_list,
                'time'=>$time,
                'date'=>$date,
                'active_staff'=>$active_staff,
            ]);
            //return $this->render('index');
        } else {

            return $this->redirect(['login']);

        }

    }



    public function actionCellcolor(){
        $request = Yii::$app->request;
        $get = $request->get();
        $post = $request->post();
        $session = Yii::$app->session;

        $res_slots_count_normal=CustomerReservedTimeslots::find()
            ->select(['COUNT(timeslot_id) AS cnt'])
            ->where([
                'test_date'=>$get["date"],
                'confirmed'=>'1',
                'third_user_id'=>'0'
            ])
            ->one()->cnt;

        $res_slots_count_third=CustomerReservedTimeslots::find()
            ->select(['COUNT(timeslot_id) AS cnt'])
            ->where([
                'test_date'=>$get["date"],
                'confirmed'=>'1'
            ])->andWhere(['>', 'third_user_id', '0'])
            ->one()->cnt;

        //echo $res_slots_count->cnt;
        //$total_slots_per_day=$_REQUEST["total_slots_per_day"];
        $total_slots_per_day=8;
//        if($res_slots_count->cnt>=$total_slots_per_day)
//            $returnthis=['bgcolor'=>'coral', 'date'=>$get["date"], 'total_slots_per_day'=>$total_slots_per_day, 'day_status'=>'<h4>Third party : 0</h4><h4>Nomral : 0</h4>'];
//        else
        $returnthis=['bgcolor'=>'#5cb85c', 'date'=>$get["date"], 'total_slots_per_day'=>$total_slots_per_day, 'day_status'=>"<br/><br/><h4>Third party : $res_slots_count_third</h4><h4>DMV : $res_slots_count_normal</h4>"];

        $holidays=Holidays::find()
            ->where('holiday_date>=CURDATE()')
            ->all();

        //var_dump($holidays);

      //  $all_holidays =array();

        foreach($holidays as $holiday){
            $holiday->holiday_name;
            $holiday->holiday_span;
            $holiday->holiday_date;
            $i=0;
            while($i < $holiday->holiday_span){


                $holiday_date = new DateTime($holiday->holiday_date);

                $holiday_date->modify("+{$i} day");
                $holiday_date = $holiday_date->format('Y-m-d') . "\n";

                $all_holidays[]=['holiday_name'=>'<br/><br/><h4>'.$holiday->holiday_name.'</h4>', 'holiday_date'=>$holiday_date];
                $i++;
            }

        }

        $returnthis['all_holidays']=$all_holidays;
//        echo "<pre>";
//        var_dump($returnthis);
//        echo "</pre>";

        $returnthis=json_encode($returnthis);
        return $returnthis;
    }


    public function actionSavestatus(){
        $request = Yii::$app->request;
        $get = $request->get();
        $post = $request->post();
        $session = Yii::$app->session;
        $model = $this->findModel($post['timeslot_id']);
        if($post['assign_toughbook']=='yes'){


            $model->tough_book_id=$post['toughbook-select1'];
            $model->status='Assigned to toughbook';
            if($model->save()){
                $session->setFlash('success', 'Test assigned to tough book.');
                $this->redirect(Yii::getAlias("@web").'/index.php');
            }

        } elseif($post['assign_toughbook']=='no'){
            $model->status=$post['status'];
            if($model->save()){
                $session->setFlash('success', 'Status Changed.');
                $this->redirect(Yii::getAlias("@web").'/index.php');
            }
        }



    }
    /**
     * Displays a single CustomerReservedTimeslots model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CustomerReservedTimeslots model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CustomerReservedTimeslots();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->timeslot_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CustomerReservedTimeslots model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->timeslot_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CustomerReservedTimeslots model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CustomerReservedTimeslots model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomerReservedTimeslots the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionLoadcustomertype($cust){
        return $this->renderAjax('customer_types', [
            'customer' => $cust,
        ]);

    }
    public function actionSearchcustomer($search){
        $business_id = Yii::$app->user->identity->business_id;

       $results = Customers::find()->orFilterWhere(['and',['like','first_name',$search],['=','business_id',$business_id]])
        ->orFilterWhere(['and',['like','email',$search],['=','business_id',$business_id]]) ->orFilterWhere(['and',['like','cell_phone',$search],['=','business_id',$business_id]])->all();

        //    ->orFilterWhere(['like','cell_phone',$search]),;
      /*  $dataProvider = new ActiveDataProvider([
            'query' => Customers::find()->orFilterWhere(['like','first_name',$search])
                ->orFilterWhere(['like','email',$search])
                ->orFilterWhere(['like','cell_phone',$search]),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]); */
        return $this->renderAjax('customer_search', [
            'results' => $results,
        ]);

    }

    /**
     * @return Action
     */
    public function actionAppointmentsave()
    {
       // $customer = $_POST['Customers'];
      //  print_r($customer['email']);
      //  exit();
   //     print_r($_POST);
    //    exit();
        $appointments = new Appointments();
        $appointments->business_id = Yii::$app->user->identity->business_id;

       if($_POST['customer_type']=='new_cust'){
           $email = $_POST['Customers']['email'];
          $model = Customers::find()->where(['email'=>$email,'business_id' => Yii::$app->user->identity->business_id])->all();


        if(!empty($model)){
            $appointments->customer_id = $model[0]->customer_id;


           }else {
               $customer = new Customers();
            $customer->business_id = Yii::$app->user->identity->business_id;
            if ($customer->load(Yii::$app->request->post()) && $customer->save(false)) {

                $appointments->customer_id = Yii::$app->db->getLastInsertID();
            }


           }

         //  $model->service_cat_id = Yii::$app->db->getLastInsertID();
           if ($appointments->load(Yii::$app->request->post()) && $appointments->save(false)) {
               return 1;
           }





       }else if($_POST['customer_type']=='3'){
           $appointments->customer_id=0;
           if ($appointments->load(Yii::$app->request->post()) && $appointments->save(false)) {
               return 1;
           }

       }
       else if($_POST['customer_type']=='exi_cust'){
        //   $email = $_POST['Customers']['email'];
           if ($appointments->load(Yii::$app->request->post()) && $appointments->save(false)) {
               return 1;
           }

       }
    }
    public function actionAppointmentinformation($id){
        $business_id=Yii::$app->user->identity->business_id;
        $query = Appointments::find()->joinWith(['services','customers']);

       $items = $query->select(['*'])->where(['appointments.appointment_id'=>$id])->one();
        $appointment_payment =  AppointmentsPayments::find()->where(['appointment_id'=>$id])->all();
        $staffList = Users::find()->where(['business_id'=>$business_id])->all();
           return $this->renderAjax('customer_information', [
            'items'=>$items,
            'appointment_payment'=>$appointment_payment,
            'staffList'=>$staffList,
       ]);
     //   print_r($items['services']->service_name);


    }
    public function actionUpdateappointnote(){
        $id = $_POST['id'];
        if(Yii::$app->db->createCommand()->update('appointments', ['appointment_note' => $_POST['value']], 'appointment_id = ' . $id)->execute()){

            return 1;
        }

    }
    public function actionAppointmentspayments(){
        $model = new AppointmentsPayments();

        $id = $_POST['AppointmentsPayments']['appointment_id'];
       $customer_id= Appointments::findOne($id);
        $model->customer_id =$customer_id->customer_id;

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            if(Yii::$app->db->createCommand()->update('appointments', ['payment_status' =>'paid'], 'appointment_id = ' . $id)->execute()){

                return 1;
            }


            //  return $this->redirectAjax(['view', 'id' => $model->service_id]);
           // Yii::$app->db->getLastInsertID();
            // return $this->renderAjax('index', ['success'=>'Success']);
        }


    }
    public function actionAppointmentstatusupdate($status,$apnt_id){
          if(Yii::$app->db->createCommand()->update('appointments', ['appointment_status' =>$status], 'appointment_id = ' . $apnt_id)->execute()){

              return 1;
          }

    }
    public function actionAppointmentstatuscancel($apnt_id){

         $send_email   = CustomizeController::Sendappointment_cancel();
        if(Yii::$app->db->createCommand()->update('appointments', ['appointment_status' =>'cancel'], 'appointment_id = ' . $apnt_id)->execute()){

            return 1;
        }

    }
    public function actionCalendarreload(){
        $reserved_timeslot = new CustomerReservedTimeslots();
        $reserved_timeslots = $reserved_timeslot->find()->all();
        $appointments = new Appointments();
        $appointments_slots = $appointments->find()->where(['business_id'=>Yii::$app->user->identity->business_id])->all();
        //  $query = Appointments::find()->joinWith(['users']);

        //   $items = $query->select(['*'])->where(['appointments.business_id'=>Yii::$app->user->identity->business_id])->all();

        //= Yii::$app->user->identity->business_id;
        $searchModel = new CustomerReservedTimeslotsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->renderAjax('calendar', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'reserved_timeslot'=>$reserved_timeslots,
            'appointments'=>$appointments_slots,
        ]);
    }
    public function actionJsoncalendar($start=NULL,$end=NULL,$_=NULL){
        $events = array();
         $appointment_id = $_REQUEST['id'];

//Testing
        if($appointment_id=='all') {
            $appointments = Appointments::find()->where(['business_id' => Yii::$app->user->identity->business_id])->all();
        }else {

                $appointments = Appointments::find()->where(['business_id' => Yii::$app->user->identity->business_id,'user_id'=>$appointment_id])->all();

        }
        foreach($appointments as $appointment) {

            $test_date = $appointment['appointment_date'];
            $test_time = $appointment['appointment_start_time'];
            $endTime = strtotime("+30 minutes", strtotime($test_time));
            $endTime= date('h:i:s', $endTime);






            $Event = new \yii2fullcalendar\models\Event();
            $Event->id = $appointment['appointment_id'];
            $Event->title = Users::getusername($appointment['user_id']);


            $Event->start = $test_date.'T'.$test_time.'Z';
            $Event->end = $test_date.'T'.$endTime.'Z';
            if($appointment['appointment_status']=='cancel'){
                $Event->backgroundColor = 'red';
                $Event->borderColor = 'red';

            }
            $events[] = $Event;

        }
  //  print_r($events);
        header('Content-type: application/json');
        echo Json::encode($events);
       // Json::encode($Event)


        Yii::$app->end();
    }
    public function actionSendreview(){
     
        $customer_id = $_POST['cust'];
        $apnt_id = $_POST['apnt'];

        $business_id = Yii::$app->user->identity->business_id;
        $business_information = BusinessInformation::findOne($business_id);


        $custom_email =  CustomizeEmail::findOne(['email_type'=>'thank_you','business_id'=>$business_id]);
        if(isset($custom_email)){

        }else {
            $custom_email =  CustomizeEmail::findOne(['email_type'=>'thank_you','business_id'=>0]);
        }
        $customers     = Customers::findOne($customer_id);
        $admin_email   = Yii::$app->user->identity->email;
        $business_information = BusinessInformation::findOne($business_id);
       // $email_type    =  CustomizeEmail::findOne(['business_id'=>$business_id,'email_type'=>$email_type]);
        $email_type    =  $custom_email;
        $email_subject = $email_type->email_subject;
        $email_content = $email_type->email_content;
        $email_content = preg_replace("/{{business_name}}/",$business_information->business_name, $email_content);
        $email_content = preg_replace("/{{businessaddress}}/",$business_information->address, $email_content);
        $email_content = preg_replace("/{{fname}}/",$customers->first_name, $email_content);
        $email_content = preg_replace("/{{lname}}/",$customers->last_name, $email_content);
        $email_content = preg_replace("/{{businessphone}}/",$business_information->business_phone, $email_content);
        $email_content = preg_replace("/{{ReviewLink}}/",'http://localhost/boocked/customer/web/index.php/site/reviews?apnt_id='.$apnt_id.'&bid='.$business_id, $email_content);


        $cancelation_policy = Yii::$app->session->get('bk_cancelation_policy');
        $email_content = preg_replace("/{{cancellationpolicy}}/",$cancelation_policy, $email_content);

     //   print_r($cancelation_policy);

        $email_content = preg_replace("/{{businessemail}}/",$admin_email, $email_content);
     //   print_r($email_content);


        if(Yii::$app->mailer->compose()
            ->setFrom('from@domain.com')
            ->setTo('sikandar.maqbool@tabsusa.com')
            ->setSubject($email_subject)->setHtmlBody($email_content)
            ->send()){
            return 1;
        }


    }
    public function actionRescheduleslots(){
           $selected_date = $_POST['reschedule'];
           $day = strtolower(date('l',strtotime($selected_date)));
           $check_appointment_date = date('Y-m-d',strtotime($selected_date));


            $business_id = Yii::$app->user->identity->business_id;
            $service_id = $_POST['srID'];
            $staff_id = $_POST['stID'];
            $weekly = WeeklyRecurringTime::find()->where(['business_id' => $business_id,'service_id' => $service_id, 'staff_id' =>$staff_id])->one();
            $appointments = Appointments::find()->where(['business_id' => $business_id,'service_id' => $service_id, 'user_id' =>$staff_id,'appointment_date'=>$check_appointment_date])->one();
            $slotsstart = strtotime('12 AM');
         //   $endSlot = strtotime('+ 1 hour',$startSlot);
        $business_hours = json_decode($weekly->$day);
        $hourz_size = sizeof($business_hours->from);
       // print_r($business_hours);
        $not_hours=[];
        $hours = [];
        for ($x = 0; $x != 24; $x++) {
          //  echo date('h:i A', strtotime('+ ' . $x . ' hour', $slotsstart)) . '<br/>';

            //  echo $slotsstart;



            // print_r($business_hours->from);
            //  exit();
            for ($ac = 0; $ac != $hourz_size; $ac++) {
                //  echo '<pre>';

                // echo '</pre>';
                if (!empty($business_hours->from[$ac]) && !empty($business_hours->to[$ac])) {

                    $slt = strtotime('+ ' . $x . ' hour', $slotsstart) ;
                    if((strtotime($business_hours->from[$ac]) <= $slt) && (strtotime($business_hours->to[$ac]) >=$slt)){

                       $hours[] =  date('h:i A', strtotime('+ ' . $x . ' hour', $slotsstart));
                        }else {
                        $not_hours[]  = date('h:i A', strtotime('+ ' . $x . ' hour', $slotsstart));
                    }


                }
            }
        }
     //   print_r($hours);
     //   echo '<br/>';
     //   print_r($not_hours);
        $not_hours = array_unique($not_hours);
        $hours = array_unique($hours);
        $not_hours = array_diff($not_hours, $hours);
        $nothours = array_diff($not_hours, $hours);
     //   print_r($nothours);
        echo '<select>';
        foreach($nothours as $nothour) {
          echo  '<option class="option_nothours" value="'.$nothour.'"><span style="color:red">'.$nothour.'</span></option>';
        }
        $hours = array_diff($hours, $not_hours);
    //    print_r($hours);
        foreach($hours as $hour ){
            echo  '<option class="optionhour" value="'.$hour.'"><span style="color:blue">'.$hour.'</span></option>';
        }
        echo '</select>';


    }
    
    public function actionReschedule_appointments(){
    //    print_r($_POST['apntID']);
        $old_appointment_id = $_POST['apntID'];
        $service_id =  $_POST['srID'];
        $staff_id =  $_POST['stID'];
        $reschedule_date =  date('Y-m-d',strtotime($_POST['reschedule_date']));;
        $reschedule_time =  date('H:i',strtotime($_POST['reschedule_time']));
      //  echo($reschedule_time);
      //  echo($reschedule_date);
      //  exit();
        $business_id = Yii::$app->user->identity->business_id;
        $old_appointment = Appointments::find()->where(['business_id'=>$business_id,'appointment_id'=>$old_appointment_id])->one();
        if($old_appointment){
            $new_appointment = new Appointments();
            $new_appointment->appointment_start_time = $reschedule_time;
            $new_appointment->appointment_date = $reschedule_date;
            $new_appointment->business_id = $business_id;
            $new_appointment->user_id = $staff_id;
            $new_appointment->service_id = $service_id;
            $new_appointment->customer_id = $old_appointment->customer_id;
            if($new_appointment->save(false)){
                $old_appointment->apnt_reschedule_id =$new_appointment->appointment_id;
                if($old_appointment->save(false)){
                    return 1;
                }


            }
        }
    }
    
    protected function findModel($id)
    {
        if (($model = CustomerReservedTimeslots::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
