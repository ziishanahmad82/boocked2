<?php

namespace app\controllers;

use app\models\CustomizeEmail;
use app\models\Services;
use app\models\Users;
use Yii;
use app\models\Customers;
use app\models\CustomersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Appointments;
use app\models\AppointmentsPayments;
use yii\db\Expression;
use app\models\Coupans;
use app\models\EmailTemplates;
use app\models\GiftCertificates;
use app\models\BusinessInformation;
use app\models\SoldGiftCertificates;
use Google_Service_Calendar;
use Google_Client;
use Google_Service_Calendar_Event;

/**
 * CustomersController implements the CRUD actions for Customers model.
 */
class MarketingController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Customers models.
     * @return mixed
     */
    public function actionIndex()
    {
       $searchModel = new CustomersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $CustomerList = Customers::find()->where(['business_id'=>Yii::$app->user->identity->business_id])->all();
        $businessInformation = BusinessInformation::findOne(Yii::$app->user->identity->business_id);

        $NewCustomer = new Customers();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'NewCustomer' => $NewCustomer,
            'CustomerList'=> $CustomerList,
            'businessInformation'=>$businessInformation,
        ]);
    }

    public function actionNew()
    {
        $searchModel = new CustomersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $CustomerList = Customers::find()->where(['business_id'=> Yii::$app->user->identity->business_id])
           ->andWhere(['between', 'createdat',  date('Y-m-d')-30,  date('Y-m-d') ])->all();
        $NewCustomer = new Customers();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'NewCustomer' => $NewCustomer,
            'CustomerList'=> $CustomerList,
        ]);
    }
    public function actionActive()
    {
        $searchModel = new CustomersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $query = Customers::find()->joinWith(['appointments']);
        $CustomerList = $query->select(['*'])
            ->andFilterWhere(['<', 'appointments.appointment_date',  date('Y-m-d')-30 ])
            ->andFilterWhere(['=', 'customers.business_id', Yii::$app->user->identity->business_id ])
            ->groupBy(['customers.customer_id'])
        ->all();
     //   $CustomerList = Customers::find()->where(['business_id'=>Yii::$app->user->identity->business_id])
        //    ->andWhere(['<', 'createdat',  date('Y-m-d')-30 ])->all();
       // print_r($CustomerList[0]->customer_id);
       // exit();
        $NewCustomer = new Customers();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'NewCustomer' => $NewCustomer,
            'CustomerList'=> $CustomerList,
        ]);
    }
    public function actionCoupans(){
        $business_id = Yii::$app->user->identity->business_id;
        $business_information = BusinessInformation::findOne($business_id);
        $services = Services::find()->where(['business_id'=>$business_id,'service_status'=>'active'])->all();
        $staff = Users::find()->where(['business_id'=>$business_id])->all();
        $coupan_list = Coupans::find()->where(['business_id'=>$business_id])->all();
          $coupan = new Coupans();
        return $this->render('coupans', [
            'coupan' => $coupan,
            'services'=>$services,
            'staff'=>$staff,
            'coupan_list'=>$coupan_list,
            'business_information'=>$business_information,
        ]);

    }
    public function actionMycoupans(){
        $business_id = Yii::$app->user->identity->business_id;
        $business_information = BusinessInformation::findOne($business_id);
        $services = Services::find()->where(['business_id'=>$business_id,'service_status'=>'active'])->all();
        $staff = Users::find()->where(['business_id'=>$business_id])->all();
        $coupan_list = Coupans::find()->where(['business_id'=>$business_id])->all();
        $coupan = new Coupans();
        return $this->render('mycoupans', [
            'coupan' => $coupan,
            'services'=>$services,
            'staff'=>$staff,
            'coupan_list'=>$coupan_list,
            'business_information'=>$business_information
        ]);

    }
    public function actionOffers(){
        $business_id = Yii::$app->user->identity->business_id;
        $services = Services::find()->where(['business_id'=>$business_id,'service_status'=>'active'])->all();
        $staff = Users::find()->where(['business_id'=>$business_id])->all();
        $coupan_list = Coupans::find()->where(['business_id'=>$business_id])->all();
        $coupan = new Coupans();
        return $this->render('offers', [
            'coupan' => $coupan,
            'services'=>$services,
            'staff'=>$staff,
            'coupan_list'=>$coupan_list
        ]);

    }
    public function actionCoupans2(){
        $business_id = Yii::$app->user->identity->business_id;
        $services = Services::find()->where(['business_id'=>$business_id,'service_status'=>'active'])->all();
        $staff = Users::find()->where(['business_id'=>$business_id])->all();
        $coupan_list = Coupans::find()->where(['business_id'=>$business_id])->all();
        $coupan = new Coupans();
        return $this->render('coupans2', [
            'coupan' => $coupan,
            'services'=>$services,
            'staff'=>$staff,
            'coupan_list'=>$coupan_list
        ]);

    }
    public function actionCoupan_create(){

        $model = new Coupans();
        if ($model->load(Yii::$app->request->post())) {
            if($_POST['Coupans']['service_id']!="all") {
                $model->service_id =  json_encode($_POST['Coupans']['service_id']);
            }
            if($_POST['Coupans']['staff_id']!="all") {
                $model->staff_id     = json_encode($_POST['Coupans']['staff_id']);
            }
            if($_POST['Coupans']['weekdays']!="all") {
                //  echo 'hereee';
                //  exit();
                $model->weekdays = json_encode($_POST['Coupans']['weekdays']);
            }


            $model->coupan_status=1;
            $model->business_id = Yii::$app->user->identity->business_id;

            if($model->save(false)){
                $this->redirect('mycoupans');
                return 1;

            }

        }

        // print_r($_POST);


    }
    public function actionChangecoupan_status(){
        if(isset($_POST)){
           // print_r($_POST);
            $business_id = Yii::$app->user->identity->business_id;
            if($_POST['b_id']==$business_id){
               $model = Coupans::findOne(['business_id'=>$_POST['b_id'],'coupan_id'=>$_POST['cp_id']]);
                $model->coupan_status = $_POST['cpstate'];
                if($model->save(false)){
                    return 1;

                }
                }
        }

    }



    public function actionEmailmarketing(){
        $business_id = Yii::$app->user->identity->business_id;
        $new_template  = new EmailTemplates();
        $all_templates = EmailTemplates::find()->where(['business_id'=>$business_id])->all();
        return $this->render('email_marketing', [
             'new_template' => $new_template,
             'all_templates'=>$all_templates,
        ]
        );

    }
    public function actionCreate_email_template(){
       $model = new EmailTemplates();
        if ($model->load(Yii::$app->request->post())) {
            $model->business_id= Yii::$app->user->identity->business_id;
            if($model->save(false)){
                return 1;

            }

        }
        print_r($_POST);
        exit();


    }
    public function actionGiftcertificates(){
        $business_id =  Yii::$app->user->identity->business_id;
        $new_gift_certificate = new GiftCertificates();
        $all_gift_certificate = GiftCertificates::find()->where(['business_id'=>$business_id])->all();
        $business_information = BusinessInformation::findOne($business_id);

        return $this->render('gift_certificates', [
           'new_gift_certificate' => $new_gift_certificate,
           'business_information'=>$business_information,
           'all_gift_certificate'=>$all_gift_certificate,
        ]);

    }
    public function actionCreate_gift_certificate(){
        $model = new GiftCertificates();
        if ($model->load(Yii::$app->request->post())) {
            $model->business_id=Yii::$app->user->identity->business_id;

            if($model->save(false)){
                //return $this->redirect(['view', 'id' => $model->customer_id]);
            return 1;
            }
        }

    }
    public function actionChangecertificate_status(){
        // print_r($_POST);
        $business_id = Yii::$app->user->identity->business_id;

            $model = GiftCertificates::findOne(['business_id'=>$business_id,'gc_id'=>$_POST['gc_id']]);
            $model->gift_status = $_POST['gcstate'];
            if($model->save(false)){
                return 1;


        }
    }
    public function actionEdit_certificate($id){
        $business_id = Yii::$app->user->identity->business_id;
        $new_gift_certificate = GiftCertificates::findOne(['business_id' => $business_id, 'gc_id' => $id]);

             return $this->renderAjax('edit_certificate', [
                    'new_gift_certificate' => $new_gift_certificate,
                    'type'=>'edit',

                ]
            );


    }

    public function actionSell_certificate($id){
        $business_id = Yii::$app->user->identity->business_id;
        $new_gift_certificate = GiftCertificates::findOne(['business_id' => $business_id, 'gc_id' => $id]);


            $model = new SoldGiftCertificates();
            return $this->renderAjax('edit_certificate', [
                    'model' => $model,
                    'new_gift_certificate'=>$new_gift_certificate,
                    'type'=>'sell',


                ]
            );



    }
    public function actionCreate_sell_gift_certificate($id){
        $business_id = Yii::$app->user->identity->business_id;
        
        $new_gift_certificate = GiftCertificates::findOne(['business_id' => $business_id, 'gc_id' => $id]);
        if($new_gift_certificate){
            $model =  new SoldGiftCertificates();
            if ($model->load(Yii::$app->request->post())) {
                $model->purchased_at='desk';
                $model->certificate_code = $this->randomnum(9);

                $model->gc_id = $id;
                $model->business_id = $business_id;
                print_r($model);
                exit();
                if($model->save(false)) {
                    return 1;
                }
            }

        }

    }


    public function actionUpdate_gift_certificate($id){
        $business_id = Yii::$app->user->identity->business_id;
        $model = GiftCertificates::findOne(['business_id'=>$business_id,'gc_id'=>$id]);
        if ($model->load(Yii::$app->request->post()) &&  $model->save(false)) {
            return 1;
        }

    }
    public function actionManage_giftcertificates(){
        $business_id = Yii::$app->user->identity->business_id;
        $model = SoldGiftCertificates::find(['business_id'=>$business_id])->all();
        return $this->render('manage_gift_certificates', [
            'model' => $model,
        ]);

    }
    public function actionInactive()
    {
        $searchModel = new CustomersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $query = Customers::find()->joinWith(['appointments']);
        $CustomerList = $query->select(['*'])
            ->andFilterWhere(['>', 'appointments.appointment_date',  date('Y-m-d')-30 ])
            ->andFilterWhere(['=', 'customers.business_id', Yii::$app->user->identity->business_id ])
            ->groupBy(['customers.customer_id'])
            ->all();
        //   $CustomerList = Customers::find()->where(['business_id'=>Yii::$app->user->identity->business_id])
        //    ->andWhere(['<', 'createdat',  date('Y-m-d')-30 ])->all();
        // print_r($CustomerList[0]->customer_id);
        // exit();
        $NewCustomer = new Customers();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'NewCustomer' => $NewCustomer,
            'CustomerList'=> $CustomerList,
        ]);
    }
    /**
     * Displays a single Customers model.
     * @param integer $id
     * @return mixed
     */
    public function actionInsertfb(){
        $_SESSION['fb']=$_POST['fb'];


    }
    public function actionView($id)
    {
        $this->layout = 'noheader';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Customers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Customers();
        $model->business_id = Yii::$app->user->identity->business_id;
        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            //return $this->redirect(['view', 'id' => $model->customer_id]);
            return 1;
        }
    }

    /**
     * Updates an existing Customers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->customer_id]);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Customers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    public function actionCustomerdetails()
    {
      $customer_id  = $_POST['csr_id'];
        $appointments_payments =  AppointmentsPayments::find()->where(['customer_id'=>$customer_id])->all();
        $new_appointments_payments =   new AppointmentsPayments();

        $query = Appointments::find()->joinWith(['services','users','appointments_payments']);

      //  $items = $query->select(['*'])->where(['appointments.appointment_id'=>$id])->one();
      $model = Customers::find()->where(['customer_id'=>$_POST['csr_id'],'business_id'=>Yii::$app->user->identity->business_id])->all();
      $modelban = Customers::findOne(['customer_id'=>$_POST['csr_id'],'business_id'=>Yii::$app->user->identity->business_id]);

        $past_appointments = $query->select(['*'])
            ->andFilterWhere([
          'and',
          ['=', 'appointments.appointment_date', new Expression('NOW()')],
          ['<', 'appointments.appointment_start_time', new Expression('NOW()')],
          ['appointments.customer_id'=>$customer_id],
      ]) ->orFilterWhere([
          'and',
          ['<', 'appointments.appointment_date', date('Y-m-d')],
                ['appointments.customer_id'=>$customer_id],
      ])
          ->all();
        $upcoming_appointments = Appointments::find()
        ->andFilterWhere([
            'and',
            ['=', 'appointment_date', date('Y-m-d')],
            ['>', 'appointment_start_time', new Expression('NOW()')],
            ['appointments.customer_id'=>$customer_id],
        ])->orFilterWhere([
                'and',
                ['>', 'appointment_date', date('Y-m-d')],
                ['appointments.customer_id'=>$customer_id],
            ])
            ->all();

        return $this->renderAjax('customer_details',[
            'model'=>$model,
            'past_appointments'=>$past_appointments,
            'upcoming_appointments'=>$upcoming_appointments,
            'appointments_payments'=>$appointments_payments,
            'new_appointments_payments'=>$new_appointments_payments,
            'modelban'=>$modelban,
        ]);
    }

    /**
     * Finds the Customers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Customers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionSearchcustomer(){
        $search = $_POST['searchtxt'];
        $results = Customers::find()->andFilterWhere(['like','first_name',$search.'%', false])
            ->andFilterWhere(['=','business_id',Yii::$app->user->identity->business_id])->all();
        return $this->renderAjax('customer_search_results',[
            'CustomerList'=>$results,
        ]);
       
      //  $searchtxt =

    }
    public function actionVerify_customer(){
        $customer_id = $_POST['cstr_id'];
        $model = Customers::findOne(['business_id'=>Yii::$app->user->identity->business_id,'customer_id'=>$customer_id]);

        $model->verify_status = "1";
        if($model->save(false)){
            return 1;

        }

    }
    public function actionDelete_customer(){
        $customer_id = $_POST['cstr_id'];
        $model = Customers::findOne(['business_id'=>Yii::$app->user->identity->business_id,'customer_id'=>$customer_id]);
        if($model->delete()){
            return 1;
        }

    }
    public function actionAddmembershippayment(){
        $model = new AppointmentsPayments();

        $customer_id = $_POST['customer_id'];
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            $model->customer_id = $customer_id;
            if ($model->save(false)) {
                return 1;
            }
        }
    }
    public function actionUpdateban()
    {
        $customer_id =  $_POST['Customers']['customer_id'];
        $model = Customers::findOne(['business_id' => Yii::$app->user->identity->business_id, 'customer_id' => $customer_id]);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            return 1;
        }
    }
    public function actionUpdateinformation()
    {
        $customer_id =  $_POST['cstr_id'];
        $model = Customers::findOne(['business_id' => Yii::$app->user->identity->business_id, 'customer_id' => $customer_id]);
            $model->customer_information = $_POST['cstr_inf'];
        if ($model->save(false)) {
            return 1;
        }
    }
    public function actionUnique(){
        $this->randomnum(9);

exit();
        $coupan_code = random_num(9);
        $model = SoldGiftCertificates::find()->where(['certificate_code'=>$coupan_code])->all();
        if($model){
            echo 'if';

        }else {

            echo 'else';
        }
    }


    public function randomnum($size)
    {

        $alpha_key = '';
        $keys = range('A', 'Z');

        for ($i = 0; $i < 2; $i++) {
            $alpha_key .= $keys[array_rand($keys)];
        }

        $length = $size - 2;

        $key = '';
        $keys = range(0, 9);

        for ($i = 0; $i < $length; $i++) {
            $key .= $keys[array_rand($keys)];
        }

        //   return $alpha_key . $key;


        $coupan_code = $alpha_key . $key;
        $model = SoldGiftCertificates::find()->where(['certificate_code' => $coupan_code])->all();

        if ($model) {
            $this->randomnum(9);

        } else {
           return $coupan_code;



        }

    }
    public function actionCoupanshare(){
        $this->layout = 'noheader';
        return $this->render('coupanshare');

    }

    public function actionSendemail(){

         $customer_id = 84;
         $customers = Customers::findOne($customer_id);
        // $email_type =   'appointment_reschedule';
         $email_type  =   'reminder';
         $business_id = Yii::$app->user->identity->business_id;
         $admin_email = Yii::$app->user->identity->email;
         $business_information = BusinessInformation::findOne($business_id);
         $email_type  =  CustomizeEmail::findOne(['business_id'=>$business_id,'email_type'=>$email_type]);
         $email_content = $email_type->email_content;
         $email_content = preg_replace("/{{business_name}}/",$business_information->business_name, $email_content);
         $email_content = preg_replace("/{{businessaddress}}/",$business_information->address, $email_content);
         $email_content = preg_replace("/{{fname}}/",$customers->first_name, $email_content);
         $email_content = preg_replace("/{{lname}}/",$customers->last_name, $email_content);
         $email_content = preg_replace("/{{businessphone}}/",$business_information->business_phone, $email_content);

         $cancelation_policy = Yii::$app->session->get('bk_cancelation_policy');
        $email_content = preg_replace("/{{cancellationpolicy}}/",$cancelation_policy, $email_content);

        print_r($cancelation_policy);

        $email_content = preg_replace("/{{businessemail}}/",$admin_email, $email_content);
        print_r($email_content);
        exit();

                Yii::$app->mailer->compose()
                ->setFrom('from@domain.com')
                ->setTo('sikandar.maqbool@tabsusa.com')
                ->setSubject('this is subject')->setTextBody('It is so simple to send a message.')
                ->send();

    }

    public function actionGoogleauth(){
        define('APPLICATION_NAME', 'sh1');
        define('CREDENTIALS_PATH', '~/.credentials/calendar-php-quickstart.json');
        define('CLIENT_SECRET_PATH', __DIR__ . '/client_secret.json');



        define('SCOPES', implode(' ', array(
                Google_Service_Calendar::CALENDAR)
        ));
//$client = new Google_Client();
//$client->setScopes(SCOPES);
//$client->setAuthConfig(CLIENT_SECRET_PATH);
//$client->setAccessType('offline');


            $client = new Google_Client();
            $client->setApplicationName(APPLICATION_NAME);
            $client->setScopes(SCOPES);
            $client->setAuthConfig(CLIENT_SECRET_PATH);
            $client->setAccessType('offline');
           // $client->setRedirectUri('http://' . $_SERVER['HTTP_HOST'] . '/oauth2callback.php');
            $client->setRedirectUri('http://admin.boocked.com/admin/web/index.php/marketing/googleauthback');
                $_SESSION['glclient'] = $client;
            // Load previously authorized credentials from a file.

        //    $credentialsPath = expandHomeDirectory(CREDENTIALS_PATH);
         //   if (file_exists($credentialsPath)) {
          //      print_r($credentialsPath);

      //          $accessToken = json_decode(file_get_contents($credentialsPath), true);

        //    } else {

                // Request authorization from the user.
                $authUrl = $client->createAuthUrl();

                return $authUrl;

          //  }

    }
    public function actionGoogleauthback($code){
        $client = $_SESSION['glclient'];
        $accessToken = $client->fetchAccessTokenWithAuthCode($code);
        $accessToken =   $client->getAccessToken();
        print_r($accessToken);
        print_r($client->getRefreshToken());
      //  exit();
        // Store the credentials to disk.
       // if(!file_exists(dirname($credentialsPath))) {
       //     mkdir(dirname($credentialsPath), 0700, true);
       // }
     //   file_put_contents($credentialsPath, json_encode($accessToken));
     //   printf("Credentials saved to %s\n", $credentialsPath);
       //  }


    if ($client->isAccessTokenExpired()) {
        $client->fetchAccessTokenWithRefreshToken($client->getRefreshToken());
        //   file_put_contents($credentialsPath, json_encode($client->getAccessToken()));
    }     $service = new Google_Service_Calendar($client);

// Print the next 10 events on the user's calendar.
  //      $calendarId = 'adeeelbaigg@gmail.com';
        $calendarId = 'primary';
        $optParams = array(
            // 'maxResults' => 10,
            'orderBy' => 'startTime',
              'singleEvents' => TRUE,
            //  'timeMin' => date('c'),
        );

//, $optParams

        $results = $service->events->listEvents($calendarId,$optParams);

        if (count($results->getItems()) == 0) {
            print "No upcoming events found.\n";
        } else {
            print "Upcoming events:\n";
            foreach ($results->getItems() as $event) {
                $start = $event->start->dateTime;
                if (empty($start)) {
                    $start = $event->start->date;
                }
                printf("%s (%s)\n", $event->getSummary(), $start);
            }
        }


// return
//   echo '<br/><br/><br/><br/><br/><br/>'; print_r($client);



        $event = new Google_Service_Calendar_Event(array(
            'summary' => 'Google I/O 2015',
            'location' => '800 Howard St., San Francisco, CA 94103',
            'description' => 'A chance to hear more about Google\'s developer products.',
            'start' => array(
                'dateTime' => '2016-11-28T09:00:00-07:00',
                'timeZone' => 'America/Los_Angeles',
            ),
            'end' => array(
                'dateTime' => '2016-11-28T17:00:00-07:00',
                'timeZone' => 'America/Los_Angeles',
            ),
            'recurrence' => array(
                'RRULE:FREQ=DAILY;COUNT=2'
            ),
            'attendees' => array(
                array('email' => 'lpage@example.com'),
                array('email' => 'sbrin@example.com'),
            ),
            'reminders' => array(
                'useDefault' => FALSE,
                'overrides' => array(
                    array('method' => 'email', 'minutes' => 24 * 60),
                    array('method' => 'popup', 'minutes' => 10),
                ),
            ),
        ));

        $calendarId = 'primary';
        $event = $service->events->insert($calendarId, $event);
        print_r($event);





    }

    protected function findModel($id)
    {
        if (($model = Customers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}
/*->select(['COUNT(timeslot_id) AS cnt'])
    ->where([
        'test_date' => $today_date,
        'confirmed' => '1',
        //'third_user_id' => '0'
    ])
    ->andFilterWhere([
        'and',
        ['!=', 'status', 'Checked-in'],
        ['!=', 'status', 'Waiting for check in'],
        ['!=', 'status', 'Authenticated'],
        //['!=', 'status', 'Assigned to toughbook'],
    ])
    ->one()->cnt;
*/