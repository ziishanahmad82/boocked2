<?php

namespace app\controllers;

use Yii;
use app\models\PaymentPlan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\db\Query;
use app\models\PaymentTransactions;
use yii\behaviors\TimestampBehavior;



/**
 * SubscriptionController implements the CRUD actions for PaymentPlan model.
 */
class SubscriptionController extends Controller
{

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all PaymentPlan models.
     * @return mixed
     */
    public function beforeAction($action)
    {
        if ($action->id == 'authorize_records') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => PaymentPlan::find(),
        ]);
          $payment_plan = PaymentPlan::getActiveplans();

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'payment_plan'=> $payment_plan,
        ]);
    }

    /**
     * Displays a single PaymentPlan model.
     * @param integer $id
     * @return mixed
     */
    public function actionAuthorize_records()
    {

     //   print_r(Yii::$app->user->identity);

        if(isset($_REQUEST['payment_gross'])){

            $model = new PaymentTransactions();
            $model->payment_amount = $_REQUEST['payment_gross'];
            $model->txn_id = $_REQUEST['txn_id'];
            $model->user_id = Yii::$app->user->identity->id;
            $model->business_id = Yii::$app->user->identity->business_id;
            $model->payment_date = $_REQUEST['payment_date'];
            $model->touch('datetime');
            if($model->save(false)){
            echo 'saved';

            }

        }

       
        return $this->redirect(['/site/index']);










/*
        exit();
        $token="";
        if (isset($_REQUEST['token']))
        {
            $token = $_REQUEST['token'];

        }
        return $this->render('authorize_records', [
          'token'=>$token,
        ]);
    */
    }

    /**
     * Creates a new PaymentPlan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new PaymentPlan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pp_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
        public function actionNotify_url(){
            header('HTTP/1.1 200 OK');
            $req = 'cmd=_notify-validate';

// Loop through the notification name-value pairs
            foreach ($_POST as $key => $value) {
                // Encode the values
                $value = urlencode(stripslashes($value));
                // Add the name-value pairs to the acknowledgement
                $req .= "&$key=$value";
            }
            $header = "POST /cgi-bin/webscr HTTP/1.1\r\n";

// Set up other acknowledgement request headers
            $header .= "Content-Type: application/x-www-form-urlencoded\r\n";
            $header .= "Content-Length: " . strlen($req) . "\r\n\r\n";

// If testing on Sandbox use:
            $header .= "Host: www.sandbox.paypal.com:443\r\n";
// For live servers use $header .= "Host: www.paypal.com:443\r\n";

// Open a socket for the acknowledgement request
// If testing on Sandbox use:
            $fp = fsockopen ('ssl://www.sandbox.paypal.com', 443, $errno, $errstr, 30);
// For live servers use $fp = fsockopen ('ssl://www.paypal.com', 443, $errno, $errstr, 30);

// Send the HTTP POST request back to PayPal for validation
            fputs($fp, $header . $req);


while (!feof($fp)) {
    // While not EOF
    $res = fgets($fp, 1024);
    // Get the acknowledgement response
    if (strcmp ($res, "VERIFIED") == 0) {
        // Response contains VERIFIED - process notification
        // Authentication protocol is complete - OK to process notification contents
        // Possible processing steps for a payment include the following:
        // Check that the payment_status is Completed
        // Check that txn_id has not been previously processed
        // Check that receiver_email is your Primary PayPal email
        // Check that payment_amount/payment_currency are correct
        // Process payment
    } else if (strcmp ($res, "INVALID") == 0) {

			// Authentication protocol is complete - begin error handling
			// Send an email announcing the IPN message is INVALID

		}


    fclose ($fp);
}





            exit();
         $abc = json_encode($_POST);
        $query = new Query;
        if ($query->createCommand()->insert('setting_booking_rules', [
            'bkrules_name' =>$abc,
            'business_id' =>'1',
            'bkrules_value'=>'dsad',

        ])->execute()
        ) { echo 'heree'; }
        exit();
    }
    /**
     * Updates an existing PaymentPlan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->pp_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing PaymentPlan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the PaymentPlan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return PaymentPlan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = PaymentPlan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
