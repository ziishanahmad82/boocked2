<?php

namespace app\controllers;

use app\models\Appointments;
use app\models\ChangeEmail;
use app\models\SiteSettings;
use app\models\User;
use app\models\Users;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SignupForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\Customers;
use app\models\CustomersSearch;
use app\models\CustomerReservedTimeslots;
use app\models\SiteIndexCustomerReservedTimeslotsSearch;
use app\models\SiteIndexTab2CustomerReservedTimeslotsSearch;
use app\models\PasswordForm;
use app\models\BusinessInformation;
use app\models\ServicesCommonName;
use app\models\StaffCommonName;
use app\models\Services;
use yii\db\Query;
use app\models\Timezoneandcurrency;
use app\models\Notification;
use app\models\Countries;
use app\models\States;
use app\models\Cities;
use app\models\ProfessionsList;
use app\models\Reviews;
use yii\web\UploadedFile;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\web\View;





class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?','@'],
                    ],
                    [
                        'actions' => ['logout','changepassword'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'language' => ['post'],
                    'customerportalstatus'=>['post'],
                ],
            ],
        ];

    }
    public function beforeAction($action)
    {
        if ($action->id == 'login') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {

        $business_id=Yii::$app->user->identity->business_id;
        $business_information  = BusinessInformation::findone(['business_id'=>Yii::$app->user->identity->business_id]);
        $first_month = date("m");
        $first_month_user = Customers::find()->where(['business_id'=>$business_id,'MONTH(createdat)'=>$first_month])->count();
        $first_month = Appointments::find()->where(['business_id'=>$business_id,'MONTH(appointment_date)'=>$first_month])->count();
        $satisfaction = Reviews::find()->where(['business_id'=>$business_id,'MONTH(created_at)'=>$first_month])->sum('review_status');
        $total_reviews = Reviews::find()->where(['business_id'=>$business_id,'MONTH(created_at)'=>$first_month])->count();
        if($total_reviews!=0) {
            $satisfaction_percent = $satisfaction * 100;
            $total_rev = $total_reviews * 4;
            $satisfaction = $satisfaction_percent / $total_rev;
        }else {
            $satisfaction = 0;

        }

        $first_query = Appointments::find()->joinWith(['services'])->where(['appointments.business_id'=>$business_id,'YEAR(appointments.appointment_date)'=>date('Y'),'MONTH(appointments.appointment_date)'=>$first_month]);
        $first_estimated_sales = $first_query->sum('services.service_price');



        $second_month= date("m", strtotime("-1 months"));
        $second_month_user = Customers::find()->where(['business_id'=>$business_id,'MONTH(createdat)'=>$second_month])->count();
        $second_month = Appointments::find()->where(['business_id'=>$business_id,'MONTH(appointment_date)'=>$second_month])->count();
        $second_query = Appointments::find()->joinWith(['services'])->where(['appointments.business_id'=>$business_id,'YEAR(appointments.appointment_date)'=>date('Y'),'MONTH(appointments.appointment_date)'=>$second_month]);
        $second_estimated_sales = $second_query->sum('services.service_price');





        $third_month = date("m", strtotime("-2 months"));
        $third_month_user = Customers::find()->where(['business_id'=>$business_id,'MONTH(createdat)'=>$third_month])->count();
        $third_month = Appointments::find()->where(['business_id'=>$business_id,'MONTH(appointment_date)'=>$third_month])->count();
        $third_query = Appointments::find()->joinWith(['services'])->where(['appointments.business_id'=>$business_id,'YEAR(appointments.appointment_date)'=>date('Y'),'MONTH(appointments.appointment_date)'=>$third_month]);
        $third_estimated_sales = $third_query->sum('services.service_price');



        $four_month = date("m", strtotime("-3 months"));
        $four_month_year = date("Y", strtotime("-3 months"));
        $four_month_user = Customers::find()->where(['business_id'=>$business_id,'MONTH(createdat)'=>$four_month])->count();
        $four_month = Appointments::find()->where(['business_id'=>$business_id,'MONTH(appointment_date)'=>$four_month])->count();

        $four_query = Appointments::find()->joinWith(['services'])->where(['appointments.business_id'=>$business_id,'YEAR(appointments.appointment_date)'=>$four_month_year,'MONTH(appointments.appointment_date)'=>$four_month]);
        $four_estimated_sales = $four_query->sum('services.service_price');


        $five_month = date("m", strtotime("-4 months"));
        $five_month_year = date("Y", strtotime("-4 months"));
        $five_month_user = Customers::find()->where(['business_id'=>$business_id,'MONTH(createdat)'=>$five_month])->count();
        $five_month = Appointments::find()->where(['business_id'=>$business_id,'MONTH(appointment_date)'=>$five_month])->count();

        $five_query = Appointments::find()->joinWith(['services'])->where(['appointments.business_id'=>$business_id,'YEAR(appointments.appointment_date)'=>$five_month_year,'MONTH(appointments.appointment_date)'=>$five_month]);
        $five_estimated_sales = $five_query->sum('services.service_price');

        $six_month = date("m", strtotime("-5 months"));
        $six_month_year = date("Y", strtotime("-5 months"));
        $six_month_user = Customers::find()->where(['business_id'=>$business_id,'MONTH(createdat)'=>$six_month])->count();
        $six_month = Appointments::find()->where(['business_id'=>$business_id,'MONTH(appointment_date)'=>$six_month])->count();

        $six_query = Appointments::find()->joinWith(['services'])->where(['appointments.business_id'=>$business_id,'YEAR(appointments.appointment_date)'=>$six_month_year,'MONTH(appointments.appointment_date)'=>$six_month]);
        $six_estimated_sales = $six_query->sum('services.service_price');

                $NewService = new Services();
                $NewStaff= new Users();
                $business_id = Yii::$app->user->identity->business_id;
                $appointments_count = Appointments::find()->where(['business_id'=>$business_id,'YEAR(appointment_date)'=>date('Y'),'MONTH(appointment_date)'=>date('m')])->count();
                $today_appointments_count = Appointments::find()->where(['business_id'=>$business_id,'appointment_date'=>date('Y-m-d')])->count();
                $query = Appointments::find()->joinWith(['services'])->where(['appointments.business_id'=>$business_id,'YEAR(appointments.appointment_date)'=>date('Y'),'MONTH(appointments.appointment_date)'=>date('m')]);
                $estimated_sales = $query->sum('services.service_price');
                $staffList = Users::find()->where(['business_id'=>$business_id])->all();
                $new_customers = Customers::find()->where(['business_id'=>$business_id])->all();


     

            if($business_information->business_status==0){
                $countries  = Countries::find()->all();
                $states = States::find()->where(['country_id'=>$business_information->country])->all();
                $cities = Cities::find()->where(['state_id'=>$business_information->state])->all();
                $professions_list = ProfessionsList::find()->all();

                $serviceCommonNameModel = ServicesCommonName::findone(['business_id'=>Yii::$app->user->identity->business_id]);
                $staffCommonNameModel = StaffCommonName::findone(['business_id'=>Yii::$app->user->identity->business_id]);
                $allServices = Services::find()->where(['business_id'=>Yii::$app->user->identity->business_id])->all();



                if(empty($serviceCommonNameModel)) {
                    $serviceCommonNameModel = new ServicesCommonName();
                 
                }
                if(empty($staffCommonNameModel)) {
                    $staffCommonNameModel = new StaffCommonName();

                }
               
                    //ServicesCommonName
                return $this->render('index', [
                    'business_information'=> $business_information,
                    'serviceCommonNameModel'=>$serviceCommonNameModel,
                    'NewService'=>$NewService,
                    'allServices'=>$allServices,
                    'staffList'=>$staffList,
                    'NewStaff'=>$NewStaff,
                    'staffCommonNameModel'=>$staffCommonNameModel,
                    'information_required'=> 'yes',
                    'appointments_count'=>$appointments_count,
                    'estimated_sales'=>$estimated_sales,
                    'new_customers'=>$new_customers,
                    'today_appointments_count'=>$today_appointments_count,
                    'first_month'=>$first_month,
                    'second_month'=>$second_month,
                    'third_month'=>$third_month,
                    'four_month'=>$four_month,
                    'five_month'=>$five_month,
                    'six_month'=>$six_month,
                    'first_month_user'=>$first_month_user,
                    'second_month_user'=>$second_month_user,
                    'third_month_user'=>$third_month_user,
                    'four_month_user'=>$four_month_user,
                    'five_month_user'=>$five_month_user,
                    'six_month_user'=>$six_month_user,
                    'countries'=>$countries,
                    'cities'=>$cities,
                    'states'=>$states,
                    'professions_list'=>$professions_list,
                    'satisfaction'=>$satisfaction,
                    'total_reviews'=>$total_reviews,
                    'first_estimated_sales'=>$first_estimated_sales,
                    'second_estimated_sales'=>$second_estimated_sales,
                    'third_estimated_sales'=>$third_estimated_sales,
                    'four_estimated_sales'=>$four_estimated_sales,
                    'five_estimated_sales'=>$five_estimated_sales,
                    'six_estimated_sales'=>$six_estimated_sales,



                ]);

            } else {
                return $this->render('index', [
                    'business_information'=> $business_information,
                    'information_required'=> 'no',
                    'staffList'=>$staffList,
                    'appointments_count'=>$appointments_count,
                    'estimated_sales'=>$estimated_sales,
                    'new_customers'=>$new_customers,
                    'today_appointments_count'=>$today_appointments_count,
                    'first_month'=>$first_month,
                    'second_month'=>$second_month,
                    'third_month'=>$third_month,
                    'four_month'=>$four_month,
                    'five_month'=>$five_month,
                    'six_month'=>$six_month,
                    'first_month_user'=>$first_month_user,
                    'second_month_user'=>$second_month_user,
                    'third_month_user'=>$third_month_user,
                    'four_month_user'=>$four_month_user,
                    'five_month_user'=>$five_month_user,
                    'six_month_user'=>$six_month_user,
                    'third_estimated_sales'=>$third_estimated_sales,
                    'satisfaction'=>$satisfaction,
                    'total_reviews'=>$total_reviews,
                    'first_estimated_sales'=>$first_estimated_sales,
                    'second_estimated_sales'=>$second_estimated_sales,
                    'third_estimated_sales'=>$third_estimated_sales,
                    'four_estimated_sales'=>$four_estimated_sales,
                    'five_estimated_sales'=>$five_estimated_sales,
                    'six_estimated_sales'=>$six_estimated_sales,


                ]);




            }


    }

    public function actionRoutes()
    {
        return $this->render('routes');
    }

    /**
     * Ajax handler for language change dropdown list. Sets cookie ready for next request
     */
    public function actionLanguage()
    {
        if ( Yii::$app->request->post('_lang') !== NULL && array_key_exists(Yii::$app->request->post('_lang'), Yii::$app->params['languages']))
        {
            Yii::$app->language = Yii::$app->request->post('_lang');
            $cookie = new yii\web\Cookie([
                'name' => '_lang',
                'value' => Yii::$app->request->post('_lang'),
            ]);
            Yii::$app->getResponse()->getCookies()->add($cookie);
        }
        Yii::$app->end();
    }

    public function actionLogin()
    {

        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();


        if (Yii::$app->request->post()) {
            $login_form = Yii::$app->request->post('LoginForm');
            $model->username = $login_form['username'];
            $model->password = $login_form['password'];
            if($model->login()){
                return $this->goBack();
            }
            $this->layout = 'noheader';
            return $this->render('login', [
                'model' => $model,
            ]);
         //
        } else {

            $this->layout = 'noheader';
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }



    public function actionCustomerportalstatus(){
        $action=Yii::$app->request->get('action');
        $model=SiteSettings::findOne('1');
        if($action=='disable'){
            $model->value='inactive';
        } else {
            $model->value='active';
        }
        if($model->save()){
            $this->redirect(Yii::$app->request->referrer);
        }

    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    public function actionAbout()
    {
        return $this->render('about');
    }

    public function actionSignup()
    {

        if(@$id=Yii::$app->request->get('id')){
            $model = User::findOne($id);
        } else {
            $model = new SignupForm();
        }

        $business_information = new BusinessInformation();
        if ($model->load(Yii::$app->request->post())) {
            if(@$user_id=Yii::$app->request->post('user_id')){

                $user=Users::findOne($user_id);

                return $this->redirect(Yii::getAlias('@web').'/index.php/users/index',302);

            } else {
                if ($user = $model->signup()) {
                    //if (Yii::$app->getUser()->login($user)) {
                    return $this->redirect(Yii::getAlias('@web').'/index.php/users/index',302);
                    //}
                }
            }

        }

        return $this->render('signup', [
            'model' => $model,
            'business_information'=>$business_information,
        ]);
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function checkPersmission($permission){
        $this_user_permissions = Yii::$app->authManager->getPermissions();
        if($this_user_permissions["/*"])
        {
            return true;
        } elseif($this_user_permissions[$permission])
        {
            return true;
        } else {
            return false;
        }
    }


    public function actionChangepassword(){
        $model = new PasswordForm();
        //$modeluser = new User();
        $modeluser = User::find()->where([
            'username'=>Yii::$app->user->identity->username
        ])->one();

        $request = \Yii::$app->getRequest();
        if($model->load(Yii::$app->request->post())){
            $success = false;
            $error = [];
            if($model->validate()){
                try{


                //    $modeluser->auth_key =$modeluser->generateAuthKey();
                   // $modeluser->password_hash =  $modeluser->setPassword($model->newpass);
                  //  print_r($model->newpass);
                    $modeluser->password_hash = Yii::$app->security->generatePasswordHash($model->newpass);
                 

                    if($modeluser->save()){

                        Yii::$app->getSession()->setFlash(
                            'success','Password changed'
                        );
                     //   return $this->redirect(['index']);
                    }else{
                        Yii::$app->getSession()->setFlash(
                            'error','Password not changed'
                        );
                       // return $this->redirect(['index']);
                    }
                }catch(Exception $e){
                    Yii::$app->getSession()->setFlash(
                        'error',"{$e->getMessage()}"
                    );
                    return $this->renderAjax('changepassword',[
                        'model'=>$model
                    ]);
                }
            }else{
              //  echo 'dasdasd';
              //  exit();
                $error = $model->getErrors();
                header('Cache-Control: no-cache, must-revalidate');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Content-type: application/json');

                echo json_encode(['success' => $success, 'error' => $error ]);

                //return $this->renderAjax('changepassword',[
                 //   'model'=>$model
               // ]);
            }
        }else{
            return $this->renderAjax('changepassword',[
                'model'=>$model
            ]);
        }
    }
    public function actionChangeemail(){
        $model = new ChangeEmail();

        //$modeluser = new User();
        $modeluser = User::find()->where([
            'username'=>Yii::$app->user->identity->username
        ])->one();

      //  $request = \Yii::$app->getRequest();
        if($model->load(Yii::$app->request->post())){
            $success = 2;
            $error = [];
            if($model->validate()){
                try{
                    $modeluser->email = $model->email;
                    if($modeluser->save()){
                        $success = 1;

                      //  Yii::$app->getSession()->setFlash();
                       // echo 'saved';
                        //   return $this->redirect(['index']);
                    }else{
                      //  Yii::$app->getSession()->setFlash();
                        // return $this->redirect(['index']);
                    }



                }catch(Exception $e){

                }
            }else{
                $error = $model->getErrors();
                header('Cache-Control: no-cache, must-revalidate');
                header('Expires: Mon, 26 Jul 1997 05:00:00 GMT');
                header('Content-type: application/json');

                echo json_encode(['success' => $success, 'error' => $error ]);


            }
        }else{
            echo 'not validate';
            //return $this->renderAjax('changeepassword',[
           //     'model'=>$model
           // ]);
        }



    }


    public function updatePassword($new_password) {
        return $password_hash = Yii::$app->security->generatePasswordHash($new_password);
    }

    public function actionValidate()
    {

        $model = new ServicesCommonName();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;

        }
        return ActiveForm::validate($model);
    }

    /////sikandar functions
    public function actionSignup_service_create(){
    $model = new Services();

    if ($model->load(Yii::$app->request->post())) {
        $model->business_id = Yii::$app->user->identity->business_id;
        $model->service_cat_id = 0;

        $model->google_address = Yii::$app->request->post("google_ad");
        $model->lat = Yii::$app->request->post("lat");
        $model->lng = Yii::$app->request->post("lng");


        if (!empty(UploadedFile::getInstance($model, 'service_video'))) {
        $model->service_video = UploadedFile::getInstance($model, 'service_video');

            $random_digit = rand(0000, 9999999);
            if ($model->service_video->saveAs('uploads/' . str_replace(' ', '', $model->service_video->baseName) . $random_digit . '.' . $model->service_video->extension)) {
                $model->service_video = 'uploads/' . str_replace(' ', '', $model->service_video->baseName) . $random_digit . '.' . $model->service_video->extension;

            }
        }



       /* echo $model->imageFile1 = UploadedFile::getInstance($model, 'imageFile1');
        echo $model->imageFile2 = UploadedFile::getInstance($model, 'imageFile2');
        echo $model->imageFile3 = UploadedFile::getInstance($model, 'imageFile3');*/


        // $service_model =  new Services;

            if(!empty(UploadedFile::getInstance($model, 'imageFile1'))) {
                $model->imageFile1 = UploadedFile::getInstance($model, 'imageFile1');
                $random_digit = rand(0000, 9999999);
                if ($model->imageFile1->saveAs('uploads/' . $model->imageFile1->baseName . $random_digit . '.' . $model->imageFile1->extension)) {
                    $model->service_car_image1 = 'uploads/' . $model->imageFile1->baseName . $random_digit . '.' . $model->imageFile1->extension;

                }
            }
            if(!empty(UploadedFile::getInstance($model, 'imageFile2'))) {
                $model->imageFile2 = UploadedFile::getInstance($model, 'imageFile2');
                $random_digit = rand(0000, 9999999);
                if ($model->imageFile2->saveAs('uploads/' . $model->imageFile2->baseName . $random_digit . '.' . $model->imageFile2->extension)) {
                    $model->service_car_image2 = 'uploads/' . $model->imageFile2->baseName . $random_digit . '.' . $model->imageFile2->extension;

                }
            }
            if(!empty(UploadedFile::getInstance($model, 'imageFile3'))) {
                $model->imageFile3 = UploadedFile::getInstance($model, 'imageFile3');
                $random_digit = rand(0000, 9999999);
                if($model->imageFile3->saveAs('uploads/' . $model->imageFile3->baseName.$random_digit. '.' . $model->imageFile3->extension)){
                    $model->service_car_image3  = 'uploads/'.$model->imageFile3->baseName.$random_digit. '.' . $model->imageFile3->extension;

                }


        }
        /*$model->service_image = Yii::$app->request->post("imageFile1");
        $model->service_car_image1 = Yii::$app->request->post("imageFile2");
        $model->service_car_image2 = Yii::$app->request->post("imageFile3");
        $model->service_video = Yii::$app->request->post("service_video");*/

        if($model->save(false)) {
            $services = ServicesCommonName::findOne(['business_id'=>Yii::$app->user->identity->business_id]);
            if ($services->load(Yii::$app->request->post()) && $services->save(false) ) {

            }
            return 1;

        }
    } else {
        return 2;
    }

    }
    public function actionSignup_service_delete(){
        $model = Services::find()->where(['business_id'=>Yii::$app->user->identity->business_id,'service_id'=>$_POST['sr_id']])->one();
        if ($model->delete()){

                return 1;


        } else {
            return 2;
        }

    }
    public function actionSignup_service_count(){
        $model = Services::find()->where(['business_id'=>Yii::$app->user->identity->business_id])->count();;
        if($model>0){
            return 1;

        }else {
            return 0;
        }
    }
    public function actionSignup_staff_create(){
        $model = new Users();
        if ($model->load(Yii::$app->request->post())) {
            $model->business_id=Yii::$app->user->identity->business_id;
            if($model->save(false)) {
                $staff = StaffCommonName::findOne(['business_id'=>Yii::$app->user->identity->business_id]);
                if ($staff->load(Yii::$app->request->post()) && $staff->save(false) ) {

                }
                return 1;

            }
        } else {
            return 2;
        }

    }
    public function actionSignup_staff_delete(){
        $model = Users::find()->where(['business_id'=>Yii::$app->user->identity->business_id,'id'=>$_POST['sr_id']])->one();
        if ($model->delete()){

            return 1;


        } else {
            return 2;
        }

    }
    public function actionSignupbusiness_hours()
    {
        $business_id = Yii::$app->user->identity->business_id;
        $Staff_user = Users::find()->where(['business_id' => $business_id])->all();
        $business_services = Services::find()->where(['business_id' => $business_id])->all();
        $staffsun = json_encode($_POST['staffsun']);
        $staffmon = json_encode($_POST['staffmon']);
        $stafftue = json_encode($_POST['stafftue']);
        $staffwed = json_encode($_POST['staffwed']);
        $staffthurs = json_encode($_POST['staffthurs']);
        $stafffri = json_encode($_POST['stafffri']);
        $staffsat = json_encode($_POST['staffsat']);
        foreach ($Staff_user as $staff) {

            foreach ($business_services as $service) {
                $query = new Query;
                if ($query->createCommand()->insert('weekly_recurring_time', [
                    'sunday' => $staffsun,
                    'monday' => $staffmon,
                    'tuesday' => $stafftue,
                    'wednesday' => $staffwed,
                    'thursday' => $staffthurs,
                    'friday' => $stafffri,
                    'saturday' => $staffsat,
                    'staff_id' => $staff->id,
                    'service_id' => $service->service_id,
                    'business_id' => $business_id
                ])->execute()
                ) {
                }
            }
        }
        if (Yii::$app->db->createCommand()->update('business_information', [
            'business_status' => '1'
        ], 'business_id = ' . $business_id)->execute()
        ) {
            return 1;

        }
    }
        // $business_id = Yii::$app->user->identity->business_id;  $staff_id , $service_id
        //  yRecurringTime
        //  exit();
        public function actionSignup_timezone_currency(){
            $business_id = Yii::$app->user->identity->business_id;
            $timezone = Timezoneandcurrency::findOne(['business_id'=>Yii::$app->user->identity->business_id]);
            if(empty($timezone)){
                $timezone = new Timezoneandcurrency();

            }
           

            if ($timezone->load(Yii::$app->request->post())) {
                $timezone->business_id=$business_id;
                if($timezone->save(false)){

                }


            }
            $model = BusinessInformation::find()->where(['business_id'=>Yii::$app->user->identity->business_id])->one();
            if ($model->load(Yii::$app->request->post()) && $model->save(false) ) {

                return 1;


            } else {
                return 2;
            }

        }

    public function actionShowappointmentnotification(){
        $type=1;
        $business_id =  Yii::$app->user->identity->business_id;
        $model = Notification::find()->where(['business_id'=>$business_id,'notification_status'=>'unread','notification_type'=>$type])->count();
        return $model;

    }
    public function actionShowreviewnotification(){
        $type=3;
        $business_id =  Yii::$app->user->identity->business_id;
        $model = Notification::find()->where(['business_id'=>$business_id,'notification_status'=>'unread','notification_type'=>$type])->count();
        return $model;

    }
    public function actionShowcustomernotification(){
        $type=2;
        $business_id =  Yii::$app->user->identity->business_id;
        $model = Notification::find()->where(['business_id'=>$business_id,'notification_status'=>'unread','notification_type'=>$type])->count();
        return $model;

    }
    public function actionMonthcount(){
        $start_date = date("Y-m-d", strtotime("-6 months"));
        $first_month = date("m");
        $first_month = Appointments::find()->where(['business_id'=>'1','MONTH(appointment_date)'=>$first_month])->count();

        $second_month= date("m", strtotime("-1 months"));
        $second_month = Appointments::find()->where(['business_id'=>'1','MONTH(appointment_date)'=>$second_month])->count();
        echo $second_month;
        $third_month = date("m", strtotime("-2 months"));
       $third_month = Appointments::find()->where(['business_id'=>'1','MONTH(appointment_date)'=>$third_month])->count();
        echo $third_month;
        $four_month = date("m", strtotime("-3 months"));
        $four_month = Appointments::find()->where(['business_id'=>'1','MONTH(appointment_date)'=>$four_month])->count();
        echo $four_month;
        $five_month = date("m", strtotime("-4 months"));
        $five_month = Appointments::find()->where(['business_id'=>'1','MONTH(appointment_date)'=>$five_month])->count();
        echo $five_month;
        $six_month = date("m", strtotime("-5 months"));
        $six_month = Appointments::find()->where(['business_id'=>'1','MONTH(appointment_date)'=>$six_month])->count();
        echo $six_month;
            //->andFilterWhere(['between', 'appointment_date', $start_date, $end_date])->count();

        //  $query
     //   $model = Notification::find()->where(['business_id'=>$business_id,'notification_status'=>'unread','notification_type'=>$type])->count();
     //   return $model;

    }
    public function actionDasboardservicesummary(){
        if(isset($_POST)){
             $type =  $_POST['type'];
            if($type=='week'){
                $startstrtotime=$_POST['selectdate'];

                $startstrtotime= strtotime($_POST['selectdate']);
                $start_date = date('Y-m-d',$startstrtotime);

                $date = strtotime("+6 day", $startstrtotime);
                $end_date = date('Y-m-d',$date);
                $end_date = date('Y-m-d',$date);
             // =  ->andFilterWhere(['between', 'price', $this ->price, $this -> to_price])

            }else if($type=='month') {
                $startstrtotime= strtotime('01-'.$_POST['selectdate']);
                $month = date('m',$startstrtotime);
                $year = date('Y',$startstrtotime);

            }else if($type=='year'){
                $year = $_POST['selectdate'];
            //    $start_date = date('Y-m-d',$startstrtotime);
            }

            $service_id = $_POST['service_id'];
            $st_id = $_POST['st_id'];
            $business_id =  Yii::$app->user->identity->business_id;
            if($st_id==0 && $service_id==0) {
                if($type=='week') {
                    $appointments_count = Appointments::find()->where(['business_id' => $business_id])->andFilterWhere(['between', 'appointment_date', $start_date, $end_date])->count();
                    $query = Appointments::find()->joinWith(['services'])->where(['appointments.business_id' => $business_id])->andFilterWhere(['between', 'appointments.appointment_date', $start_date, $end_date]);

                    $satisfaction = Reviews::find()->where(['business_id'=>$business_id])->andFilterWhere(['between', 'created_at', $start_date, $end_date])->sum('review_status');
                    $total_reviews = Reviews::find()->where(['business_id'=>$business_id])->andFilterWhere(['between', 'created_at', $start_date, $end_date])->count();

                }else if($type=='month'){
                    $appointments_count = Appointments::find()->where(['business_id' => $business_id,'YEAR(appointment_date)'=>$year,'MONTH(appointment_date)'=>$month])->count();
                    $query = Appointments::find()->joinWith(['services'])->where(['appointments.business_id' => $business_id,'YEAR(appointments.appointment_date)'=>$year,'MONTH(appointments.appointment_date)'=>$month]);

                    $satisfaction = Reviews::find()->where(['business_id'=>$business_id,'YEAR(created_at)'=>$year,'MONTH(created_at)'=>$month])->sum('review_status');
                    $total_reviews = Reviews::find()->where(['business_id'=>$business_id,'YEAR(created_at)'=>$year,'MONTH(created_at)'=>$month])->count();



                }else if($type=='year'){
                    $appointments_count = Appointments::find()->where(['business_id' => $business_id,'YEAR(appointment_date)'=>$year])->count();
                    $query = Appointments::find()->joinWith(['services'])->where(['appointments.business_id' => $business_id,'YEAR(appointment_date)'=>$year]);

                    $satisfaction = Reviews::find()->where(['business_id'=>$business_id,'YEAR(created_at)'=>$year])->sum('review_status');
                    $total_reviews = Reviews::find()->where(['business_id'=>$business_id,'YEAR(created_at)'=>$year])->count();


                }
            }else if($service_id==0 && $st_id!=0) {
                if($type=='week') {
                $appointments_count = Appointments::find()->where(['business_id' => $business_id, 'user_id' => $st_id])->andFilterWhere(['between', 'appointment_date', $start_date, $end_date])->count();
                $query = Appointments::find()->joinWith(['services'])->where(['appointments.business_id' => $business_id,'appointments.user_id' => $st_id])->andFilterWhere(['between', 'appointments.appointment_date', $start_date, $end_date]);

                $satisfaction = Reviews::find()->where(['business_id'=>$business_id,'staff_id'=>$st_id])->andFilterWhere(['between', 'created_at', $start_date, $end_date])->sum('review_status');
                $total_reviews = Reviews::find()->where(['business_id'=>$business_id,'staff_id'=>$st_id])->andFilterWhere(['between', 'created_at', $start_date, $end_date])->count();

                }else if($type=='month'){
                    $appointments_count = Appointments::find()->where(['business_id' => $business_id, 'user_id' => $st_id,'YEAR(appointments.appointment_date)'=>$year,'MONTH(appointments.appointment_date)'=>$month])->count();
                    $query = Appointments::find()->joinWith(['services'])->where(['appointments.business_id' => $business_id,'appointments.user_id' => $st_id,'YEAR(appointments.appointment_date)'=>$year,'MONTH(appointments.appointment_date)'=>$month]);

                    $satisfaction = Reviews::find()->where(['business_id'=>$business_id,'staff_id'=>$st_id,'YEAR(created_at)'=>$year,'MONTH(created_at)'=>$month])->sum('review_status');
                    $total_reviews = Reviews::find()->where(['business_id'=>$business_id,'staff_id'=>$st_id, 'YEAR(created_at)'=>$year,'MONTH(created_at)'=>$month])->count();


                }else if($type=='year') {
                    $appointments_count = Appointments::find()->where(['business_id' => $business_id, 'user_id' => $st_id,'YEAR(appointment_date)'=>$year])->count();
                    $query = Appointments::find()->joinWith(['services'])->where(['appointments.business_id' => $business_id,'appointments.user_id' => $st_id,'YEAR(appointment_date)'=>$year]);

                    $satisfaction = Reviews::find()->where(['business_id'=>$business_id,'staff_id'=>$st_id,'YEAR(created_at)'=>$year])->sum('review_status');
                    $total_reviews = Reviews::find()->where(['business_id'=>$business_id,'staff_id'=>$st_id,'YEAR(created_at)'=>$year])->count();
                }





            }else if($service_id!=0 && $st_id==0) {
                if($type=='week') {

                $appointments_count = Appointments::find()->where(['business_id' => $business_id,'service_id' => $service_id ])->andFilterWhere(['between', 'appointment_date', $start_date, $end_date])->count();
                $query = Appointments::find()->joinWith(['services'])->where(['appointments.business_id' => $business_id,'services.service_id' => $service_id])->andFilterWhere(['between', 'appointments.appointment_date', $start_date, $end_date]);

                $satisfaction = Reviews::find()->where(['business_id'=>$business_id,'service_id' => $service_id ])->andFilterWhere(['between', 'created_at', $start_date, $end_date])->sum('review_status');
                $total_reviews = Reviews::find()->where(['business_id'=>$business_id,'service_id' => $service_id ])->andFilterWhere(['between', 'created_at', $start_date, $end_date])->count();

                }else if($type=='month'){
                    $appointments_count = Appointments::find()->where(['business_id' => $business_id,'service_id' => $service_id,'YEAR(appointments.appointment_date)'=>$year,'MONTH(appointments.appointment_date)'=>$month ])->count();
                    $query = Appointments::find()->joinWith(['services'])->where(['appointments.business_id' => $business_id,'services.service_id' => $service_id,'YEAR(appointments.appointment_date)'=>$year,'MONTH(appointments.appointment_date)'=>$month]);

                    $satisfaction = Reviews::find()->where(['business_id'=>$business_id,'service_id' => $service_id ,'YEAR(created_at)'=>$year,'MONTH(created_at)'=>$month])->sum('review_status');
                    $total_reviews = Reviews::find()->where(['business_id'=>$business_id,'service_id' => $service_id , 'YEAR(created_at)'=>$year,'MONTH(created_at)'=>$month])->count();

                }else if($type=='year') {
                    $appointments_count = Appointments::find()->where(['business_id' => $business_id,'service_id' => $service_id,'YEAR(appointment_date)'=>$year ])->count();
                    $query = Appointments::find()->joinWith(['services'])->where(['appointments.business_id' => $business_id,'services.service_id' => $service_id,'YEAR(appointment_date)'=>$year]);

                    $satisfaction = Reviews::find()->where(['business_id'=>$business_id,'service_id' => $service_id ,'YEAR(created_at)'=>$year])->sum('review_status');
                    $total_reviews = Reviews::find()->where(['business_id'=>$business_id,'service_id' => $service_id ,'YEAR(created_at)'=>$year])->count();

                }
            } else {
                if($type=='week') {
                $appointments_count = Appointments::find()->where(['business_id' => $business_id, 'service_id' => $service_id, 'user_id' => $st_id])->andFilterWhere(['between', 'appointment_date', $start_date, $end_date])->count();
                $query = Appointments::find()->joinWith(['services'])->where(['services.service_id' => $service_id, 'appointments.user_id' => $st_id])->andFilterWhere(['between', 'appointments.appointment_date', $start_date, $end_date]);

                    $satisfaction = Reviews::find()->where(['business_id'=>$business_id,'service_id' => $service_id,'staff_id' => $st_id])->andFilterWhere(['between', 'created_at', $start_date, $end_date])->sum('review_status');
                    $total_reviews = Reviews::find()->where(['business_id'=>$business_id,'service_id' => $service_id,'staff_id' => $st_id ])->andFilterWhere(['between', 'created_at', $start_date, $end_date])->count();

            }else if($type=='month'){
                    $appointments_count = Appointments::find()->where(['business_id' => $business_id, 'service_id' => $service_id, 'user_id' => $st_id,'YEAR(appointments.appointment_date)'=>$year,'MONTH(appointments.appointment_date)'=>$month])->count();
                    $query = Appointments::find()->joinWith(['services'])->where(['services.service_id' => $service_id, 'appointments.user_id' => $st_id,'YEAR(appointments.appointment_date)'=>$year,'MONTH(appointments.appointment_date)'=>$month]);

                    $satisfaction = Reviews::find()->where(['business_id'=>$business_id,'service_id' => $service_id,'staff_id' => $st_id, 'YEAR(created_at)'=>$year,'MONTH(created_at)'=>$month])->sum('review_status');
                    $total_reviews = Reviews::find()->where(['business_id'=>$business_id,'service_id' => $service_id,'staff_id' => $st_id, 'YEAR(created_at)'=>$year,'MONTH(created_at)'=>$month])->count();

            }else if($type=='year') {

                    $appointments_count = Appointments::find()->where(['business_id' => $business_id, 'service_id' => $service_id, 'user_id' => $st_id,'YEAR(appointment_date)'=>$year])->count();
                    $query = Appointments::find()->joinWith(['services'])->where(['services.service_id' => $service_id, 'appointments.user_id' => $st_id,'YEAR(appointment_date)'=>$year]);

                    $satisfaction = Reviews::find()->where(['business_id'=>$business_id,'staff_id' => $st_id,'service_id' => $service_id ,'YEAR(created_at)'=>$year])->sum('review_status');
                    $total_reviews = Reviews::find()->where(['business_id'=>$business_id,'staff_id' => $st_id, 'service_id' => $service_id ,'YEAR(created_at)'=>$year])->count();

            }
            }
            if($total_reviews!=0) {
                $satisfaction_percent = $satisfaction * 100;
                $total_rev = $total_reviews * 4;
                $satisfaction = $satisfaction_percent / $total_rev;
            }else {
                $satisfaction = 0;

            }

        $estimated_sales = $query->sum('services.service_price');
            $new_customers = Customers::find()->where(['business_id'=>$business_id])->all();
        return $this->renderAjax('dasboardservicesummary',[
            'estimated_sales'=>$estimated_sales,
            'appointments_count'=>$appointments_count,
            'new_customers'=>$new_customers,
            'satisfaction'=>$satisfaction,
            'total_reviews'=>$total_reviews,
        ]);
        }
        }


    public function actionShowappointmentnotification_results(){
        $type = $_POST['type'];
        $business_id = Yii::$app->user->identity->business_id;
if($type == '1') {

    $query = Appointments::find()->joinWith(['services', 'users', 'customers']);
    $model = $query->select(['*'])->andFilterWhere([
        'and',
        ['=', 'appointments.business_id', $business_id],
        //  ['<', 'appointments.appointment_start_time', new Expression('NOW()')],
    ])->all();

    $notification = Notification::find()->where(['business_id'=>$business_id,'notification_status'=>'unread','notification_type'=>$type])->all();
    foreach ($notification as $index=>$notification) {
        // $notification->notification_status = $index;
        $notification->notification_status='read';
        // then save
        $notification->save(false);
    }

}else if($type == '2') {

    $model = Customers::find()->where(['business_id'=>$business_id])->all();
    //.business_id', $business_id],
 //   ])->all();
    $notification = Notification::find()->where(['business_id'=>$business_id,'notification_status'=>'unread','notification_type'=>$type])->all();
   // $notification->notification_status='read';
    foreach ($notification as $index=>$notification) {
       // $notification->notification_status = $index;
         $notification->notification_status='read';
        // then save
        $notification->save(false);
    }


}else if($type == '3'){
    $model = Reviews::find()->where(['business_id'=>$business_id])->all();

}
       //  print_r($model);
        return $this->renderAjax('appointmentnotification_results',[
            'model'=>$model,
            'type'=>$type,
        ]);
     //   foreach($model as $appointments_noti){
     //   echo ' First Name '.$appointments_noti['customers']['first_name'];

    //    }
    //    echo '</pre>';
    }





}



