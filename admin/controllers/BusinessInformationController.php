<?php

namespace app\controllers;

use Yii;
use app\models\BusinessInformation;
use app\models\Users;
use app\models\Timezoneandcurrency;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\db\Query;
use app\models\ContactPhone;
use yii\db\ActiveQuery;
use app\models\SignupForm;
use app\models\WeeklyRecurringTime;
use app\models\Services;
use app\models\BlockedDateTimes;
use app\models\Countries;
use app\models\States;
use app\models\Cities;
use app\models\CurrencyList;
use app\models\ProfessionsList;

/**
 * BusinessInformationController implements the CRUD actions for BusinessInformation model.
 */
class BusinessInformationController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all BusinessInformation models.
     * @return mixed
     */
    public function actionIndex()
    {

        $business_id = Yii::$app->user->identity->business_id;
        $model = BusinessInformation::findone(['business_id'=>Yii::$app->user->identity->business_id]);

        $timezoneCurrency = Timezoneandcurrency::find()->where(['business_id'=>$business_id])->all();

        $staff_list =  Users::getActiveUsers(Yii::$app->user->identity->business_id);
        $countries  = Countries::find()->all();
        $states = States::find()->where(['country_id'=>$model->country])->all();
        $cities = Cities::find()->where(['state_id'=>$model->state])->all();
        $currency_list = CurrencyList::find()->all();

        $contact_phone = ContactPhone::find()->where(['user_id'=>Yii::$app->user->identity->id])->asArray()->all();
       // $business_information  = BusinessInformation::find()->where(['business_id' => $business_id])->all();
        $business_information  = BusinessInformation::find()->where(['business_id' => $business_id])->one();


        return $this->render('index', [
            'business_information' => $business_information,
            'model'=> $model,
            'timezoneCurrency'=>$timezoneCurrency,
            'contact_phone'=>$contact_phone,
            'staff_list'=>$staff_list,
            'countries'=>$countries,
            'states'=>$states,
            'cities'=>$cities,
            'currency_list'=>$currency_list
        ]);
    }

    /**
     * Displays a single BusinessInformation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BusinessInformation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BusinessInformation();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->business_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing BusinessInformation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        //Yii::$app->user->identity->business_id;
        $model = BusinessInformation::findone(['business_id'=>Yii::$app->user->identity->business_id]);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
           // return $this->redirect(['view', 'id' => $model->business_id]);
            echo 1;
        } else {

            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing BusinessInformation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    public function actionValidate()
    {

        $model = new BusinessInformation();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;

        }
        return ActiveForm::validate($model);
    }
    /**
     * Finds the BusinessInformation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BusinessInformation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdatetimezone($id)
    {
      //  echo $_POST['item_value'];

        if (isset($_POST['item_name'])) {

            $query = new Query;
            if($query->createCommand()->update('timezoneandcurrency', [$_POST['item_name'] => $_POST['item_value']], 'tnc_id = ' . $id)->execute()){
               if($_POST['item_name']=='currency'){
                   Yii::$app->session->set('bk_'.$_POST['item_name'], CurrencyList::showcurrency_name($_POST['item_value']));

               }
             //   Yii::$app->session->set('bk_'.$_POST['item_name'], $_POST['item_value']);
                return 1;
            }
        }
    }
    public function actionUpdatephone($id)
    {
        if($id==0){
            $query = new Query;

            if($query->createCommand()->insert('contact_phone', [
                $_POST['phone_name'] =>  $_POST['phone_value'],
                'user_id' => Yii::$app->user->identity->id,
            ])->execute()){
                $id = Yii::$app->db->getLastInsertID();
                return $id;

            }



        }else {


           // echo $_POST['item_value'];
            if (isset($_POST['phone_name'])) {

                $query = new Query;
                if ($query->createCommand()->update('contact_phone', [$_POST['phone_name'] => $_POST['phone_value']], 'contact_phone_id = ' . $id)->execute()) {

                    return 'updated';
                }
            }
        }
    }
    public function actionUpdateadminemail($email){
        $id = Yii::$app->user->identity->id;

        $model = SignupForm::find($id);

        if ($model->load(Yii::$app->request->post())) {

            $model->save();

            return 1;
        }
  /*      $SignupForm = new SignupForm;
        $SignupForm->load($email);
print_r($SignupForm['email']);
        if($SignupForm['email']->validate()) {
            echo 'adada';
            //$email = SignupForm->email;

       // }){

            echo 'yes';
            exit();
        }
*/

    }
    public function actionUserbusiness_hours(){
        $business_id = Yii::$app->user->identity->business_id;
     //   $model =
        if($business_id==$_POST['bs_id']){
           $blocked_results = BlockedDateTimes::find()->where(['business_id'=>$business_id])->groupBy(['YEAR(block_date)'])->all();
            $blocked_time_results = BlockedDateTimes::find()->where(['business_id'=>$business_id])
                ->andWhere(['<>','blocktimeTo','00:00:00'])->andWhere(['<>','blocktimeFrom','00:00:00'])->groupBy(['block_date'])->all();
            $blockedtimes  = new BlockedDateTimes();
             $staff_id  = $_POST['st_id'];
            $staff_list =  Users::getActiveUsers(Yii::$app->user->identity->business_id);
            $current_user = Users::find()->where(['id'=>$_POST['st_id'],'business_id'=>$business_id])->all();
        //    $weekly_times = WeeklyRecurringTime::find()->where(['user_id'=>Yii::$app->user->identity->id])->all();
            $query = WeeklyRecurringTime::find()->joinWith(['services']);
            $weekly_times = $query->select(['*'])->where(['weekly_recurring_time.staff_id'=>$_POST['st_id']])->all();
            return $this->renderAjax('business-hours', [
                'weekly_times' => $weekly_times,
                'blockedtimes' => $blockedtimes,
                'blocked_results'=>$blocked_results,
                'staff_id' => $staff_id,
                'staff_list'=> $staff_list,
                'current_user'=>$current_user,
                'blocked_time_results'=>$blocked_time_results,


            ]);


        }else {
            echo  'You have no access to this user';

        }


    }
    public function actionAddscedule(){
        $business_id = Yii::$app->user->identity->business_id;
         $services_list = Services::getServicesList($business_id);
       // $staff_list = Users::getActiveUsers($business_id);
        $staff_list = Users::find()->where(['business_id'=>$business_id])->all();
        return $this->renderAjax('add-scedule', [
            'services_list' => $services_list,
            'staff_list'=> $staff_list,
        ]);
        
    }
    public function actionBusiness_scedule()
    {
        $business_id = Yii::$app->user->identity->business_id;
        if($_POST['staff_name']=='all'){


        } if($_POST['service_name']=='all'){


    }
        $weekly = WeeklyRecurringTime::find()->where(['business_id' => $business_id,'service_id' => $_POST['service_name'], 'staff_id' =>$_POST['staff_name']])->all();
        if(empty($weekly)){
            $service_id = $_POST['service_name'];
            $staff_id =  $_POST['staff_name'];
            $staffsun = json_encode($_POST['staffsun']);
            $staffmon = json_encode($_POST['staffmon']);
            $stafftue = json_encode($_POST['stafftue']);
            $staffwed = json_encode($_POST['staffwed']);
            $staffthurs = json_encode($_POST['staffthurs']);
            $stafffri = json_encode($_POST['stafffri']);
            $staffsat = json_encode($_POST['staffsat']);
            $query = new Query;
            if ($query->createCommand()->insert('weekly_recurring_time', [
                'sunday' => $staffsun,
                'monday' => $staffmon,
                'tuesday' =>$stafftue,
                'wednesday' =>$staffwed,
                'thursday' => $staffthurs,
                'friday' => $stafffri,
                'saturday' => $staffsat,
                'staff_id' => $staff_id,
                'service_id' => $service_id,
                'business_id'=> $business_id,
            ])->execute()
            ) {
                return 1;

            }


        }else {
            foreach ($weekly as $weeklytimes) {
                echo 1;
                if (isset($_POST['staffsun'])) {

                    $s = 0;
                    foreach ($_POST['staffsun']['from'] as $staff_Sunfrom) {


                        $mytimeSundayStart = $staff_Sunfrom;
                        $mytimeSundayEnd = $_POST['staffsun']['to'][$s];
                        $mytimestart = strtotime($staff_Sunfrom);
                        $mytimeend = strtotime($_POST['staffsun']['to'][$s]);


                        $sunday = json_decode($weeklytimes->sunday, true);
                        $sunday_size = sizeof($sunday['from']);
                        $a = 0;

                        for ($x = 0; $x != $sunday_size; $x++) {


                            $timeStart = strtotime($sunday['from'][$x]);
                            $timeEnd = strtotime($sunday['to'][$x]);


                            if (($mytimeend > $timeEnd && $mytimestart > $timeEnd && $mytimeend > $timeStart && $mytimestart > $timeStart) || ($mytimestart < $timeStart && $timeStart > $mytimeend)) {  //top
                                $form['from'][] = $mytimeSundayStart;
                                $form['to'][] = $mytimeSundayEnd;
                                $form['from'][] = $sunday['from'][$x];
                                $form['to'][] = $sunday['to'][$x];


                            } elseif ($mytimestart >= $timeStart && $mytimeend <= $timeEnd) {                //1
                                echo 'both are inside existing slot';
                                $form['from'][] = $sunday['from'][$x];
                                $form['to'][] = $sunday['to'][$x];


                            } elseif ($mytimestart <= $timeStart && $mytimeend >= $timeEnd) {    //2
                                $form['from'][] = $mytimeSundayStart;
                                $form['to'][] = $mytimeSundayEnd;
                                echo 'mytimestart in time end and print  $mytimeend.both are larger than existing slot';

                            } elseif ($mytimestart <= $timeStart && $mytimeend <= $timeEnd) {  //3
                                $form['from'][] = $mytimeSundayStart;
                                $form['to'][] = $sunday['to'][$x];
                                echo '$mytimestart $timeEnd large ';


                            } elseif ($mytimestart >= $timeStart && $mytimeend >= $timeEnd) {  //4
                                $form['from'][] = $sunday['from'][$x];
                                $form['to'][] = $mytimeSundayEnd;

                                echo ' $timeStart and $mytimeend are greeater than other ';

                            } else {
                                echo 'not matching rows';

                            }

                            echo '<br/>';
                        }


                        $s++;
                    }
                }
                ///  monday Start

                if (isset($_POST['staffmon'])) {

                    $s = 0;
                    foreach ($_POST['staffmon']['from'] as $staff_Sunfrom) {


                        $mytimeSundayStart = $staff_Sunfrom;
                        $mytimeSundayEnd = $_POST['staffmon']['to'][$s];
                        $mytimestart = strtotime($staff_Sunfrom);
                        $mytimeend = strtotime($_POST['staffmon']['to'][$s]);


                        $sunday = json_decode($weeklytimes->monday, true);
                        $monday_size = sizeof($sunday['from']);
                        $a = 0;

                        for ($x = 0; $x != $monday_size; $x++) {


                            $timeStart = strtotime($sunday['from'][$x]);
                            $timeEnd = strtotime($sunday['to'][$x]);


                            if (($mytimeend > $timeEnd && $mytimestart > $timeEnd && $mytimeend > $timeStart && $mytimestart > $timeStart) || ($mytimestart < $timeStart && $timeStart > $mytimeend)) {  //top
                                $mondaydata['from'][] = $mytimeSundayStart;
                                $mondaydata['to'][] = $mytimeSundayEnd;
                                $mondaydata['from'][] = $sunday['from'][$x];
                                $mondaydata['to'][] = $sunday['to'][$x];


                            } elseif ($mytimestart >= $timeStart && $mytimeend <= $timeEnd) {                //1
                                echo 'both are inside existing slot';
                                $mondaydata['from'][] = $sunday['from'][$x];
                                $mondaydata['to'][] = $sunday['to'][$x];


                            } elseif ($mytimestart <= $timeStart && $mytimeend >= $timeEnd) {    //2
                                $mondaydata['from'][] = $mytimeSundayStart;
                                $mondaydata['to'][] = $mytimeSundayEnd;


                            } elseif ($mytimestart <= $timeStart && $mytimeend <= $timeEnd) {  //3
                                $mondaydata['from'][] = $mytimeSundayStart;
                                $mondaydata['to'][] = $sunday['to'][$x];


                            } elseif ($mytimestart >= $timeStart && $mytimeend >= $timeEnd) {  //4
                                $mondaydata['from'][] = $sunday['from'][$x];
                                $mondaydata['to'][] = $mytimeSundayEnd;


                            } else {
                                echo 'not matching rows';

                            }

                            echo '<br/>';
                        }


                        $s++;
                    }
                }
                if (isset($_POST['stafftue'])) {

                    $s = 0;
                    foreach ($_POST['stafftue']['from'] as $staff_Sunfrom) {


                        $mytimeSundayStart = $staff_Sunfrom;
                        $mytimeSundayEnd = $_POST['stafftue']['to'][$s];
                        $mytimestart = strtotime($staff_Sunfrom);
                        $mytimeend = strtotime($_POST['stafftue']['to'][$s]);


                        $sunday = json_decode($weeklytimes->tuesday, true);
                        $monday_size = sizeof($sunday['from']);
                        $a = 0;

                        for ($x = 0; $x != $monday_size; $x++) {


                            $timeStart = strtotime($sunday['from'][$x]);
                            $timeEnd = strtotime($sunday['to'][$x]);


                            if (($mytimeend > $timeEnd && $mytimestart > $timeEnd && $mytimeend > $timeStart && $mytimestart > $timeStart) || ($mytimestart < $timeStart && $timeStart > $mytimeend)) {  //top
                                $tuesdaydata['from'][] = $mytimeSundayStart;
                                $tuesdaydata['to'][] = $mytimeSundayEnd;
                                $tuesdaydata['from'][] = $sunday['from'][$x];
                                $tuesdaydata['to'][] = $sunday['to'][$x];


                            } elseif ($mytimestart >= $timeStart && $mytimeend <= $timeEnd) {                //1
                                echo 'both are inside existing slot';
                                $tuesdaydata['from'][] = $sunday['from'][$x];
                                $tuesdaydata['to'][] = $sunday['to'][$x];


                            } elseif ($mytimestart <= $timeStart && $mytimeend >= $timeEnd) {    //2
                                $tuesdaydata['from'][] = $mytimeSundayStart;
                                $tuesdaydata['to'][] = $mytimeSundayEnd;


                            } elseif ($mytimestart <= $timeStart && $mytimeend <= $timeEnd) {  //3
                                $tuesdaydata['from'][] = $mytimeSundayStart;
                                $tuesdaydata['to'][] = $sunday['to'][$x];


                            } elseif ($mytimestart >= $timeStart && $mytimeend >= $timeEnd) {  //4
                                $tuesdaydata['from'][] = $sunday['from'][$x];
                                $tuesdaydata['to'][] = $mytimeSundayEnd;


                            } else {
                                echo 'not matching rows';

                            }


                        }


                        $s++;
                    }
                }
//////Wednesday
                if (isset($_POST['staffwed'])) {

                    $s = 0;
                    foreach ($_POST['staffwed']['from'] as $staff_Sunfrom) {


                        $mytimeSundayStart = $staff_Sunfrom;
                        $mytimeSundayEnd = $_POST['staffwed']['to'][$s];
                        $mytimestart = strtotime($staff_Sunfrom);
                        $mytimeend = strtotime($_POST['staffwed']['to'][$s]);


                        $sunday = json_decode($weeklytimes->wednesday, true);
                        $monday_size = sizeof($sunday['from']);
                        $a = 0;

                        for ($x = 0; $x != $monday_size; $x++) {


                            $timeStart = strtotime($sunday['from'][$x]);
                            $timeEnd = strtotime($sunday['to'][$x]);


                            if (($mytimeend > $timeEnd && $mytimestart > $timeEnd && $mytimeend > $timeStart && $mytimestart > $timeStart) || ($mytimestart < $timeStart && $timeStart > $mytimeend)) {  //top
                                $wednesdaydata['from'][] = $mytimeSundayStart;
                                $wednesdaydata['to'][] = $mytimeSundayEnd;
                                $wednesdaydata['from'][] = $sunday['from'][$x];
                                $wednesdaydata['to'][] = $sunday['to'][$x];


                            } elseif ($mytimestart >= $timeStart && $mytimeend <= $timeEnd) {                //1
                                echo 'both are inside existing slot';
                                $wednesdaydata['from'][] = $sunday['from'][$x];
                                $wednesdaydata['to'][] = $sunday['to'][$x];


                            } elseif ($mytimestart <= $timeStart && $mytimeend >= $timeEnd) {    //2
                                $wednesdaydata['from'][] = $mytimeSundayStart;
                                $wednesdaydata['to'][] = $mytimeSundayEnd;


                            } elseif ($mytimestart <= $timeStart && $mytimeend <= $timeEnd) {  //3
                                $wednesdaydata['from'][] = $mytimeSundayStart;
                                $wednesdaydata['to'][] = $sunday['to'][$x];


                            } elseif ($mytimestart >= $timeStart && $mytimeend >= $timeEnd) {  //4
                                $wednesdaydata['from'][] = $sunday['from'][$x];
                                $wednesdaydata['to'][] = $mytimeSundayEnd;


                            } else {
                                echo 'not matching rows';

                            }


                        }


                        $s++;
                    }
                }
                //////////////////// thursday
                if (isset($_POST['staffthurs'])) {

                    $s = 0;
                    foreach ($_POST['staffthurs']['from'] as $staff_Sunfrom) {


                        $mytimeSundayStart = $staff_Sunfrom;
                        $mytimeSundayEnd = $_POST['staffthurs']['to'][$s];
                        $mytimestart = strtotime($staff_Sunfrom);
                        $mytimeend = strtotime($_POST['staffthurs']['to'][$s]);


                        $sunday = json_decode($weeklytimes->thursday, true);
                        $monday_size = sizeof($sunday['from']);
                        $a = 0;

                        for ($x = 0; $x != $monday_size; $x++) {


                            $timeStart = strtotime($sunday['from'][$x]);
                            $timeEnd = strtotime($sunday['to'][$x]);


                            if (($mytimeend > $timeEnd && $mytimestart > $timeEnd && $mytimeend > $timeStart && $mytimestart > $timeStart) || ($mytimestart < $timeStart && $timeStart > $mytimeend)) {  //top
                                $thursdaydata['from'][] = $mytimeSundayStart;
                                $thursdaydata['to'][] = $mytimeSundayEnd;
                                $thursdaydata['from'][] = $sunday['from'][$x];
                                $thursdaydata['to'][] = $sunday['to'][$x];


                            } elseif ($mytimestart >= $timeStart && $mytimeend <= $timeEnd) {                //1
                                echo 'both are inside existing slot';
                                $thursdaydata['from'][] = $sunday['from'][$x];
                                $thursdaydata['to'][] = $sunday['to'][$x];


                            } elseif ($mytimestart <= $timeStart && $mytimeend >= $timeEnd) {    //2
                                $thursdaydata['from'][] = $mytimeSundayStart;
                                $thursdaydata['to'][] = $mytimeSundayEnd;


                            } elseif ($mytimestart <= $timeStart && $mytimeend <= $timeEnd) {  //3
                                $thursdaydata['from'][] = $mytimeSundayStart;
                                $thursdaydata['to'][] = $sunday['to'][$x];


                            } elseif ($mytimestart >= $timeStart && $mytimeend >= $timeEnd) {  //4
                                $thursdaydata['from'][] = $sunday['from'][$x];
                                $thursdaydata['to'][] = $mytimeSundayEnd;


                            } else {
                                echo 'not matching rows';

                            }


                        }


                        $s++;
                    }
                }
                ///////Friday
                if (isset($_POST['stafffri'])) {

                    $s = 0;
                    foreach ($_POST['stafffri']['from'] as $staff_Sunfrom) {


                        $mytimeSundayStart = $staff_Sunfrom;
                        $mytimeSundayEnd = $_POST['stafffri']['to'][$s];
                        $mytimestart = strtotime($staff_Sunfrom);
                        $mytimeend = strtotime($_POST['stafffri']['to'][$s]);


                        $sunday = json_decode($weeklytimes->friday, true);
                        $monday_size = sizeof($sunday['from']);
                        $a = 0;

                        for ($x = 0; $x != $monday_size; $x++) {


                            $timeStart = strtotime($sunday['from'][$x]);
                            $timeEnd = strtotime($sunday['to'][$x]);


                            if (($mytimeend > $timeEnd && $mytimestart > $timeEnd && $mytimeend > $timeStart && $mytimestart > $timeStart) || ($mytimestart < $timeStart && $timeStart > $mytimeend)) {  //top
                                $fridaydata['from'][] = $mytimeSundayStart;
                                $fridaydata['to'][] = $mytimeSundayEnd;
                                $fridaydata['from'][] = $sunday['from'][$x];
                                $fridaydata['to'][] = $sunday['to'][$x];


                            } elseif ($mytimestart >= $timeStart && $mytimeend <= $timeEnd) {                //1
                                echo 'both are inside existing slot';
                                $fridaydata['from'][] = $sunday['from'][$x];
                                $fridaydata['to'][] = $sunday['to'][$x];


                            } elseif ($mytimestart <= $timeStart && $mytimeend >= $timeEnd) {    //2
                                $fridaydata['from'][] = $mytimeSundayStart;
                                $fridaydata['to'][] = $mytimeSundayEnd;


                            } elseif ($mytimestart <= $timeStart && $mytimeend <= $timeEnd) {  //3
                                $fridaydata['from'][] = $mytimeSundayStart;
                                $fridaydata['to'][] = $sunday['to'][$x];


                            } elseif ($mytimestart >= $timeStart && $mytimeend >= $timeEnd) {  //4
                                $fridaydata['from'][] = $sunday['from'][$x];
                                $fridaydata['to'][] = $mytimeSundayEnd;


                            } else {
                                echo 'not matching rows';

                            }


                        }


                        $s++;
                    }
                }
                //////////Satuday

                if (isset($_POST['staffsat'])) {

                    $s = 0;
                    foreach ($_POST['staffsat']['from'] as $staff_Sunfrom) {


                        $mytimeSundayStart = $staff_Sunfrom;
                        $mytimeSundayEnd = $_POST['staffsat']['to'][$s];
                        $mytimestart = strtotime($staff_Sunfrom);
                        $mytimeend = strtotime($_POST['staffsat']['to'][$s]);


                        $sunday = json_decode($weeklytimes->saturday, true);
                        $monday_size = sizeof($sunday['from']);
                        $a = 0;

                        for ($x = 0; $x != $monday_size; $x++) {


                            $timeStart = strtotime($sunday['from'][$x]);
                            $timeEnd = strtotime($sunday['to'][$x]);


                            if (($mytimeend > $timeEnd && $mytimestart > $timeEnd && $mytimeend > $timeStart && $mytimestart > $timeStart) || ($mytimestart < $timeStart && $timeStart > $mytimeend)) {  //top
                                $saturdaydata['from'][] = $mytimeSundayStart;
                                $saturdaydata['to'][] = $mytimeSundayEnd;
                                $saturdaydata['from'][] = $sunday['from'][$x];
                                $saturdaydata['to'][] = $sunday['to'][$x];


                            } elseif ($mytimestart >= $timeStart && $mytimeend <= $timeEnd) {                //1
                                echo 'both are inside existing slot';
                                $saturdaydata['from'][] = $sunday['from'][$x];
                                $saturdaydata['to'][] = $sunday['to'][$x];


                            } elseif ($mytimestart <= $timeStart && $mytimeend >= $timeEnd) {    //2
                                $saturdaydata['from'][] = $mytimeSundayStart;
                                $saturdaydata['to'][] = $mytimeSundayEnd;


                            } elseif ($mytimestart <= $timeStart && $mytimeend <= $timeEnd) {  //3
                                $saturdaydata['from'][] = $mytimeSundayStart;
                                $saturdaydata['to'][] = $sunday['to'][$x];


                            } elseif ($mytimestart >= $timeStart && $mytimeend >= $timeEnd) {  //4
                                $saturdaydata['from'][] = $sunday['from'][$x];
                                $saturdaydata['to'][] = $mytimeSundayEnd;


                            } else {
                                echo 'not matching rows';

                            }


                        }


                        $s++;
                    }
                }


                $newformsize = sizeof($form['from']);
                $mondaysize = sizeof($mondaydata['from']);
                $tuesdaysize = sizeof($tuesdaydata['from']);
                $wednesdaysize = sizeof($wednesdaydata['from']);
                $thursdaysize = sizeof($thursdaydata['from']);
                $fridaysize = sizeof($fridaydata['from']);
                $saturdaysize = sizeof($saturdaydata['from']);

                for ($b = 0; $b != $newformsize; $b++) {
                    //= $form['from'][$b];
                    $form['to'][$b];
                    strtotime($form['from'][$b]);

                    for ($c = 0; $c != $newformsize; $c++) {
                        if ($c != $b) {

                            if ((strtotime($form['to'][$b]) >= strtotime($form['from'][$c])) && (strtotime($form['from'][$b]) > strtotime($form['from'][$c]))) {

                            } else if (strtotime($form['to'][$b]) >= strtotime($form['from'][$c])) {

                                $form['from'][$c] = $form['from'][$b];
                                if (strtotime($form['to'][$b]) >= strtotime($form['to'][$c])) {
                                    $form['to'][$c] = $form['to'][$b];

                                } elseif (strtotime($form['to'][$b]) <= strtotime($form['to'][$c])) {
                                    $form['to'][$b] = $form['to'][$c];

                                }

                            } elseif ((strtotime($form['to'][$b]) <= strtotime($form['from'][$c])) && (strtotime($form['from'][$b]) >= strtotime($form['from'][$c]))) {
                                $form['from'][$b] = $form['from'][$c];
                                if (strtotime($form['to'][$b]) <= strtotime($form['to'][$c])) {
                                    $form['to'][$b] = $form['to'][$c];

                                }
                            }
                        }

                    }
                }

                for ($b = 0; $b != $mondaysize; $b++) {
                    //= $form['from'][$b];


                    for ($c = 0; $c != $mondaysize; $c++) {
                        if ($c != $b) {

                            if ((strtotime($mondaydata['to'][$b]) >= strtotime($mondaydata['from'][$c])) && (strtotime($mondaydata['from'][$b]) > strtotime($mondaydata['from'][$c]))) {

                            } else if (strtotime($mondaydata['to'][$b]) >= strtotime($mondaydata['from'][$c])) {

                                $mondaydata['from'][$c] = $mondaydata['from'][$b];
                                if (strtotime($mondaydata['to'][$b]) >= strtotime($mondaydata['to'][$c])) {
                                    $mondaydata['to'][$c] = $mondaydata['to'][$b];

                                } elseif (strtotime($mondaydata['to'][$b]) <= strtotime($mondaydata['to'][$c])) {
                                    $mondaydata['to'][$b] = $mondaydata['to'][$c];

                                }

                            } elseif ((strtotime($mondaydata['to'][$b]) <= strtotime($mondaydata['from'][$c])) && (strtotime($mondaydata['from'][$b]) >= strtotime($mondaydata['from'][$c]))) {
                                $mondaydata['from'][$b] = $mondaydata['from'][$c];
                                if (strtotime($form['to'][$b]) <= strtotime($form['to'][$c])) {
                                    $mondaydata['to'][$b] = $mondaydata['to'][$c];

                                }
                            }
                        }

                    }
                }

                for ($b = 0; $b != $tuesdaysize; $b++) {

                    for ($c = 0; $c != $tuesdaysize; $c++) {
                        if ($c != $b) {

                            if ((strtotime($tuesdaydata['to'][$b]) >= strtotime($tuesdaydata['from'][$c])) && (strtotime($tuesdaydata['from'][$b]) > strtotime($tuesdaydata['from'][$c]))) {

                            } else if (strtotime($tuesdaydata['to'][$b]) >= strtotime($tuesdaydata['from'][$c])) {

                                $tuesdaydata['from'][$c] = $tuesdaydata['from'][$b];
                                if (strtotime($tuesdaydata['to'][$b]) >= strtotime($tuesdaydata['to'][$c])) {
                                    $tuesdaydata['to'][$c] = $tuesdaydata['to'][$b];

                                } elseif (strtotime($tuesdaydata['to'][$b]) <= strtotime($tuesdaydata['to'][$c])) {
                                    $tuesdaydata['to'][$b] = $tuesdaydata['to'][$c];

                                }

                            } elseif ((strtotime($tuesdaydata['to'][$b]) <= strtotime($tuesdaydata['from'][$c])) && (strtotime($tuesdaydata['from'][$b]) >= strtotime($tuesdaydata['from'][$c]))) {
                                $tuesdaydata['from'][$b] = $tuesdaydata['from'][$c];
                                if (strtotime($tuesdaydata['to'][$b]) <= strtotime($tuesdaydata['to'][$c])) {
                                    $tuesdaydata['to'][$b] = $tuesdaydata['to'][$c];

                                }
                            }
                        }

                    }
                }

                for ($b = 0; $b != $wednesdaysize; $b++) {
                    //= $form['from'][$b];


                    for ($c = 0; $c != $wednesdaysize; $c++) {
                        if ($c != $b) {

                            if ((strtotime($wednesdaydata['to'][$b]) >= strtotime($wednesdaydata['from'][$c])) && (strtotime($wednesdaydata['from'][$b]) > strtotime($wednesdaydata['from'][$c]))) {

                            } else if (strtotime($wednesdaydata['to'][$b]) >= strtotime($wednesdaydata['from'][$c])) {

                                $wednesdaydata['from'][$c] = $wednesdaydata['from'][$b];
                                if (strtotime($wednesdaydata['to'][$b]) >= strtotime($wednesdaydata['to'][$c])) {
                                    $wednesdaydata['to'][$c] = $wednesdaydata['to'][$b];

                                } elseif (strtotime($wednesdaydata['to'][$b]) <= strtotime($wednesdaydata['to'][$c])) {
                                    $wednesdaydata['to'][$b] = $wednesdaydata['to'][$c];

                                }

                            } elseif ((strtotime($wednesdaydata['to'][$b]) <= strtotime($wednesdaydata['from'][$c])) && (strtotime($wednesdaydata['from'][$b]) >= strtotime($wednesdaydata['from'][$c]))) {
                                $wednesdaydata['from'][$b] = $wednesdaydata['from'][$c];
                                if (strtotime($wednesdaydata['to'][$b]) <= strtotime($wednesdaydata['to'][$c])) {
                                    $wednesdaydata['to'][$b] = $wednesdaydata['to'][$c];

                                }
                            }
                        }

                    }
                }

                for ($b = 0; $b != $thursdaysize; $b++) {
                    //= $form['from'][$b];


                    for ($c = 0; $c != $thursdaysize; $c++) {
                        if ($c != $b) {

                            if ((strtotime($thursdaydata['to'][$b]) >= strtotime($thursdaydata['from'][$c])) && (strtotime($thursdaydata['from'][$b]) > strtotime($thursdaydata['from'][$c]))) {

                            } else if (strtotime($thursdaydata['to'][$b]) >= strtotime($thursdaydata['from'][$c])) {

                                $thursdaydata['from'][$c] = $thursdaydata['from'][$b];
                                if (strtotime($thursdaydata['to'][$b]) >= strtotime($thursdaydata['to'][$c])) {
                                    $thursdaydata['to'][$c] = $thursdaydata['to'][$b];

                                } elseif (strtotime($thursdaydata['to'][$b]) <= strtotime($thursdaydata['to'][$c])) {
                                    $thursdaydata['to'][$b] = $thursdaydata['to'][$c];

                                }

                            } elseif ((strtotime($thursdaydata['to'][$b]) <= strtotime($thursdaydata['from'][$c])) && (strtotime($thursdaydata['from'][$b]) >= strtotime($thursdaydata['from'][$c]))) {
                                $thursdaydata['from'][$b] = $thursdaydata['from'][$c];
                                if (strtotime($form['to'][$b]) <= strtotime($form['to'][$c])) {
                                    $thursdaydata['to'][$b] = $thursdaydata['to'][$c];

                                }
                            }
                        }

                    }
                }

                for ($b = 0; $b != $fridaysize; $b++) {


                    for ($c = 0; $c != $fridaysize; $c++) {
                        if ($c != $b) {

                            if ((strtotime($fridaydata['to'][$b]) >= strtotime($fridaydata['from'][$c])) && (strtotime($fridaydata['from'][$b]) > strtotime($fridaydata['from'][$c]))) {

                            } else if (strtotime($fridaydata['to'][$b]) >= strtotime($fridaydata['from'][$c])) {

                                $fridaydata['from'][$c] = $fridaydata['from'][$b];
                                if (strtotime($fridaydata['to'][$b]) >= strtotime($fridaydata['to'][$c])) {
                                    $fridaydata['to'][$c] = $fridaydata['to'][$b];

                                } elseif (strtotime($fridaydata['to'][$b]) <= strtotime($fridaydata['to'][$c])) {
                                    $fridaydata['to'][$b] = $fridaydata['to'][$c];

                                }

                            } elseif ((strtotime($fridaydata['to'][$b]) <= strtotime($fridaydata['from'][$c])) && (strtotime($fridaydata['from'][$b]) >= strtotime($fridaydata['from'][$c]))) {
                                $fridaydata['from'][$b] = $fridaydata['from'][$c];
                                if (strtotime($form['to'][$b]) <= strtotime($form['to'][$c])) {
                                    $fridaydata['to'][$b] = $fridaydata['to'][$c];

                                }
                            }
                        }

                    }
                }

                for ($b = 0; $b != $saturdaysize; $b++) {


                    for ($c = 0; $c != $saturdaysize; $c++) {
                        if ($c != $b) {

                            if ((strtotime($saturdaydata['to'][$b]) >= strtotime($saturdaydata['from'][$c])) && (strtotime($saturdaydata['from'][$b]) > strtotime($saturdaydata['from'][$c]))) {

                            } else if (strtotime($saturdaydata['to'][$b]) >= strtotime($saturdaydata['from'][$c])) {

                                $saturdaydata['from'][$c] = $saturdaydata['from'][$b];
                                if (strtotime($saturdaydata['to'][$b]) >= strtotime($saturdaydata['to'][$c])) {
                                    $saturdaydata['to'][$c] = $saturdaydata['to'][$b];

                                } elseif (strtotime($saturdaydata['to'][$b]) <= strtotime($saturdaydata['to'][$c])) {
                                    $saturdaydata['to'][$b] = $saturdaydata['to'][$c];

                                }

                            } elseif ((strtotime($saturdaydata['to'][$b]) <= strtotime($saturdaydata['from'][$c])) && (strtotime($saturdaydata['from'][$b]) >= strtotime($saturdaydata['from'][$c]))) {
                                $saturdaydata['from'][$b] = $saturdaydata['from'][$c];
                                if (strtotime($form['to'][$b]) <= strtotime($form['to'][$c])) {
                                    $saturdaydata['to'][$b] = $saturdaydata['to'][$c];

                                }
                            }
                        }

                    }
                }


                $form['from'] = array_values(array_unique($form['from'], SORT_REGULAR));
                $form['to'] = array_values(array_unique($form['to'], SORT_REGULAR));
                $sundaydata = json_encode($form);
               // $form['to'] = array_values($form['to']);

              //  $mondaydata['to'] = array_unique($mondaydata['to'], SORT_REGULAR);
                //$mondaydata = json_encode($mondaydata);
                //print_r();
                $mondaydata['from'] =  array_values(array_unique($mondaydata['from'], SORT_REGULAR));
                $mondaydata['to'] =  array_values(array_unique($mondaydata['to'], SORT_REGULAR));
                $mondaydata = json_encode($mondaydata);
              //  print_r(array_values($mondaydata));

                $tuesdaydata['from'] =  array_values(array_unique($tuesdaydata['from'], SORT_REGULAR));
                $tuesdaydata['to'] =  array_values(array_unique($tuesdaydata['to'], SORT_REGULAR));
                $tuesdaydata = json_encode($tuesdaydata);

                $wednesdaydata['from'] =  array_values(array_unique($wednesdaydata['from'], SORT_REGULAR));
                $wednesdaydata['to'] = array_values( array_unique($wednesdaydata['to'], SORT_REGULAR));
                $wednesdaydata = json_encode($wednesdaydata);

                $thursdaydata['from'] =  array_values(array_unique($thursdaydata['from'], SORT_REGULAR));
                $thursdaydata['to'] =  array_values(array_unique($thursdaydata['to'], SORT_REGULAR));
                $thursdaydata = json_encode($thursdaydata);

                $fridaydata['from'] =  array_values(array_unique($fridaydata['from'], SORT_REGULAR));
                $fridaydata['to'] =  array_values(array_unique($fridaydata['to'], SORT_REGULAR));
                $fridaydata = json_encode($fridaydata);

                $saturdaydata['from'] =  array_values(array_unique($saturdaydata['from'], SORT_REGULAR));
                $saturdaydata['to'] =  array_values(array_unique($saturdaydata['to'], SORT_REGULAR));
                $saturdaydata = json_encode($saturdaydata);

               // print_r($mondaydata);
             //   print_r($tuesdaydata);
              //  print_r($thursdaydata);
              //  print_r($fridaydata);
               // print_r($saturdaydata);

                if (Yii::$app->db->createCommand()->update('weekly_recurring_time', [
                    'sunday' => $sundaydata,
                    'monday' => $mondaydata,
                    'tuesday' =>$tuesdaydata,
                    'wednesday' =>$wednesdaydata,
                    'thursday' => $thursdaydata,
                    'friday' => $fridaydata,
                    'saturday' => $saturdaydata
                ], 'wrt_id = ' . $weeklytimes->wrt_id)->execute()) {
                    echo 'saved';
                }
              //  echo 'aa'.$weeklytimes->wrt_id.'bb';



            }
        }
        return 1;

    }
    public function actionValidateblock(){
        $model = new BlockedDateTimes();

        $request = \Yii::$app->getRequest();
        $model->business_id = Yii::$app->user->identity->business_id;
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;

        }
        return ActiveForm::validate($model);
        
    }
    public function actionSavedateblock(){

       $blockeddate = explode(",",$_POST['BlockedDateTimes']['block_date']);



       // $model = new BlockedDateTimes();
      //  print_r($_POST['staff_id']);
        foreach($_POST['staff_id'] as $staff__id) {
         //   echo $staff__id;
           foreach ($blockeddate as $blockdate) {

                $model = new BlockedDateTimes();


                $time = strtotime($blockdate);

                // date('Y-m-d',$time);
                //   echo date_format($blockdate, 'Y-m-d');

                $request = \Yii::$app->getRequest();
                if ($request->isPost && $model->load($request->post())) {
                    if(isset($_POST['BlockedDateTimes']['blocktimeFrom']) && isset($_POST['BlockedDateTimes']['blocktimeTo'])){
                        $model->blocktimeFrom = date('H:i:s',strtotime($_POST['BlockedDateTimes']['blocktimeFrom']));
                        $model->blocktimeTo = date('H:i:s', strtotime($_POST['BlockedDateTimes']['blocktimeTo']));
                        //  echo date('Y-m-d H:i:s')
                       // print_r($model->blocktimeFrom);
                    }
                    $model->business_id = Yii::$app->user->identity->business_id;
                    $model->block_date = date('Y-m-d', $time);
                    $model->staff_id =  $staff__id;
                    if ($model->save(false)) {
                        //  return 1;

                    }

                }

            }
       }
        return 1;

    }
    public function actionAddress_states(){
        if($_POST['type']=='country'){
             $country_id =  $_POST['country_id'];
            $states = States::find()->where(['country_id'=>$country_id])->all();
            $all_states='';
            foreach ($states as $state){


               $all_states .= '<option value="'.$state->id.'" >'.$state->name.'</option>';

            }
            return $all_states;

        }
        if($_POST['type']=='state'){
            $state_id =  $_POST['state_id'];
            $cities = Cities::find()->where(['state_id'=>$state_id])->all();
            $all_cities='';
            foreach ($cities as $city){


                $all_cities .= '<option value="'.$city->id.'" >'.$city->name.'</option>';

            }
            return $all_cities;

        }

    }
  public function actionUpdatelogo(){
       $business_id =  Yii::$app->user->identity->business_id;
       $model =  BusinessInformation::findOne($business_id);
      if (($_FILES['business_logo']['type'] == 'image/jpeg') || ($_FILES['business_logo']['type'] == 'image/png')) {

          $uploaddir = 'uploads/';
          $random_digit = rand(000000, 999999);
          $uploadfile = $uploaddir . $random_digit . basename($_FILES['business_logo']['name']);

          if (move_uploaded_file($_FILES['business_logo']['tmp_name'], $uploadfile)) {
              $model->logo = $uploadfile;
              if ($model->save(false)) {

                  echo $uploadfile;
              }
          } else {
              echo "Possible file upload attack!\n";
          }


      }else {

          return 0;
      }

  }
    public function actionUpdateaboutimage(){
        $business_id =  Yii::$app->user->identity->business_id;
        $model =  BusinessInformation::findOne($business_id);
        if (($_FILES['business_about_image']['type'] == 'image/jpeg') || ($_FILES['business_about_image']['type'] == 'image/png')) {

            $uploaddir = 'uploads/';
            $random_digit = rand(000000, 999999);
            $uploadfile = $uploaddir . $random_digit . basename($_FILES['business_about_image']['name']);

            if (move_uploaded_file($_FILES['business_about_image']['tmp_name'], $uploadfile)) {
                $model->about_image = $uploadfile;
                if ($model->save(false)) {

                    echo $uploadfile;
                }
            } else {
                echo "Possible file upload attack!\n";
            }


        }else {

            return 0;
        }

}
    public function actionDeltelogo(){
        $business_id =  Yii::$app->user->identity->business_id;
        if($_POST['type']=='logo'){
            $type='logo';

        }else if($_POST['type']=='cover'){
            $type='about_image';
        }
        $model =  BusinessInformation::findOne($business_id);
        $model->$type ='';
        if($model->save(false)){
            return 1;

        }
    }
    protected function findModel($id)
    {
        if (($model = BusinessInformation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
