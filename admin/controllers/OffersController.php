<?php

namespace app\controllers;

use Yii;
use app\models\Services;
use app\models\ServiceCategory;
use app\models\ServicesSearch;
use app\models\ServicesCommonName;
use app\models\WeeklyRecurringTime;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\db\Query;
use yii\web\View;
use app\models\Users;
use yii\web\UploadedFile;
use app\models\ServiceOffer;

$connection  = \Yii::$app->db;

/**
 * ServicesController implements the CRUD actions for Services model.
 */
class OffersController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    //'delete' => ['post'],
                ],
            ],


        ];
    }

    /**
     * Lists all Services models.
     * @return mixed
     */
    public function actionIndex()
    {
        $business_id = Yii::$app->user->identity->business_id;
        $searchModel = new ServicesSearch();
        $serviceCommonNameModel = new ServicesCommonName();
        $servicesCommonNameData = $serviceCommonNameModel->commonRelatedNames();
        $query = new Query;

        $query->select(['*'])
            ->from('service_category')->where(['business_id'=>$business_id]);
        $command = $query->createCommand();
// $command->sql returns the actual SQL
        $services = $command->queryAll();
       // echo '<pre>';
       // print_r($services);
       // echo '</pre>';
      //  exit();
        // print_r($rows);
       // exit();

        $userCreateModel = new Users();
        $staffList = Users::find()->where(['business_id'=> $business_id])->all();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $offerModel = new ServiceOffer();

        return $this->render('index', [
            'business_id' => $business_id,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'services_categories' => $services,
            'serviceCommonNameModel' => $serviceCommonNameModel,
            'servicesCommonNameData' => $servicesCommonNameData,
            'userCreateModel' =>  $userCreateModel,
            'staffList' => $staffList,
            'offerModel'=>$offerModel,
        ]);
    }
    public function actionDelete()
    {
        $business_id = Yii::$app->user->identity->business_id;

        $query = new Query;
        $query->select([
                'services.*',
                'services.service_id as sid',
                'business_information.*',
                'service_offer.*']
        )
            ->from('service_offer')
            ->join('LEFT JOIN', 'services',
                'services.service_id=service_offer.service_id')
            ->join('INNER JOIN', 'business_information',
                'services.business_id=business_information.business_id')
            ->where(['business_information.business_id' => $business_id,'service_offer.id'=>$_GET['id']]);
        $command = $query->createCommand();
        $applist = $command->queryAll();
        if (!empty($applist)){
            $model = ServiceOffer::findOne($_GET['id']);
            $model->delete();
            //echo "<pre>";
            //print_r($model);
        }

        return $this->redirect('index');
        die();
    }
    public function actionOffer()
    {
        //print_r($_FILES);
        // print_r(Yii::$app->request->post('pname'));
        if (Yii::$app->request->post()) {

            $offerPost = Yii::$app->request->post('ServiceOffer');
            $id = Yii::$app->request->post('o_id');
            $desc = Yii::$app->request->post('o_desc');
            $sdate = Yii::$app->request->post('offer_start');
            $edate = Yii::$app->request->post('offer_end');
            $discount = $offerPost['discount'];
            $price = $offerPost['price'];
            //try to load model with available id i.e. unique key

            // $model = ServiceOffer::getservicebyid($id);

            //if(!$model){
            $model = new ServiceOffer();
            $model->service_id = $id;
            //}
            $model->offer_name =  $offerPost['offer_name'];
            $model->offer_desc = $desc;
            $model->offer_start = $sdate;
            $model->offer_end = $edate;
            $model->discount = $discount;
            $model->price = $price;
            print_r($model);

if($model->save()){
    return $this->redirect('index');
}else{
    echo 'fail';
}

        }

    }

}
