<?php

namespace app\controllers;

use Yii;
use app\models\TimeSlotsConfiguration;
use app\models\TimeSlotsConfigurationSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;

/**
 * TimeSlotsConfigurationController implements the CRUD actions for TimeSlotsConfiguration model.
 */
class TimeSlotsConfigurationController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all TimeSlotsConfiguration models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new TimeSlotsConfigurationSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single TimeSlotsConfiguration model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TimeSlotsConfiguration model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TimeSlotsConfiguration();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing TimeSlotsConfiguration model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionPreviewslots(){

        $session = Yii::$app->session;
        $get = Yii::$app->request->get();
        $post = Yii::$app->request->post();
        //var_dump($post);
        //exit();

        $effective_date=$post['effective_date'];
        $date_range_start=$post['date_range_start'];
        $date_range_end=$post['date_range_end'];
        $test_type=$post['test_type'];
        $duration=$post['duration'];
        $calendar_admin=$post['calendar_admin'];
        $start_time=$post['start_time'];
        $end_time=$post['end_time'];
        if(@$post['third_party_users']){
            $third_party_users=$post['third_party_users'];
            $third_party_users=implode(',', $third_party_users);
        }

        //$no_slots=$post['no_slots'];


        $to_time = strtotime($end_time);
        $from_time = strtotime($start_time);
        $total_minutes = round(abs($to_time - $from_time) / 60,2). " minute";
        $total_slots=floor($total_minutes/$duration);
        $loop_time=$total_slots/2;


        ob_start();
        //var_dump($post);

        ?>
        <?php $form = ActiveForm::begin(
            [
                'action' => Yii::getAlias("@web") . '/index.php/slots/createslots',
                'options' => [
                    'target' => '_blank',
                ]


            ]
        );


        ?>

            <input type="hidden" name="effective_date" value="<?=@$effective_date?>">
            <input type="hidden" name="date_range_start" value="<?=@$date_range_start?>">
            <input type="hidden" name="date_range_end" value="<?=@$date_range_end?>">
            <input type="hidden" name="calendar_type" value="<?=@$test_type?>">
            <input type="hidden" name="third_party_users" value="<?=@$third_party_users?>">
            <input type="hidden" name="duration" value="<?=@$duration?>">
            <input type="hidden" name="total_slots" value="<?=@$total_slots?>">

        <div id="sitem1">
            <h3>Slots Preview</h3>
        </div>
        <table id="sitem2">
            <tr>
                <th>
                   Slot Time
                </th>
                <th>
                    Total Slots
                </th>
                <th>
                    Slot Time
                </th>
                <th>
                    Total Slots
                </th>
            </tr>

            <tr>
                <td colspan="4">
            <?
            $i=0;
            while($i < $total_slots){
               ?>
                <div class="sitem10">
                    <input type="hidden" name="start_time[]" value="<?=$start_time;?>">
                    <?=date('h:i A', strtotime($start_time));?> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <input type="text" size="2" value="1" name="repeat_times[]">
                </div>
                <?

                $start_time = date("H:i", strtotime("+$duration minutes", strtotime($start_time)));
                $i++;
            }
            ?>
                </td>
            </tr>

        </table>
        <div id="sitem3">
            <input type="submit" class="btn btn-success" value="Generate Slots">
        </div>
        <?php ActiveForm::end(); ?>

        <?
        $return_str = ob_get_clean();
        return $return_str;
    }

    /**
     * Deletes an existing TimeSlotsConfiguration model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TimeSlotsConfiguration model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TimeSlotsConfiguration the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TimeSlotsConfiguration::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
