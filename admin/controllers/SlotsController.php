<?php

namespace app\controllers;

use app\models\Batches;
use app\models\Users;
use Yii;
use app\models\Slots;
use app\models\SlotsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SlotsController implements the CRUD actions for Slots model.
 */
class SlotsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Slots models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new SlotsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionCreateslots()
    {
        $session = Yii::$app->session;
        $get = Yii::$app->request->get();
        $post = Yii::$app->request->post();

        $effective_date = $post['effective_date'];
        $date_range_start = $post['date_range_start'];
        $date_range_end = $post['date_range_end'];
        $calendar_type = $post['calendar_type'];
        $duration = $post['duration'];
        $third_party_users = @$post['third_party_users'];
        $total_slots = $post['total_slots'];
        $start_time = $post['start_time'];
        $repeat_times = $post['repeat_times'];



        /* check if there is already calendar within that date range */
        $temp_date_range_start=explode('-', $date_range_start);
        $temp_date_range_end=explode('-', $date_range_end);
        $temp_date_range_start=$temp_date_range_start[2].'-'.$temp_date_range_start[0].'-'.$temp_date_range_start[1];
        $temp_date_range_end=$temp_date_range_end[2].'-'.$temp_date_range_end[0].'-'.$temp_date_range_end[1];
        //look in the batches if there are records for the same date range already
        $match_batches=Batches::find()
            ->andFilterWhere(
                [
                    'between','start_date', $temp_date_range_start,$temp_date_range_end
                ]
            )
        ->orFilterWhere(
            [
                'between','end_date', $temp_date_range_start, $temp_date_range_end
            ]
        );
        $match_batches->andFilterWhere(
            ['=', 'calendar_type', $calendar_type]
        );

        //echo $match_batches->createCommand()->getRawSql();


        if($match_batches->one()){
            Yii::$app->session->setFlash('error', 'There is already a batch of "'.$calendar_type.'" type within this date range');
            $this->redirect(Yii::$app->request->referrer);
            return;
        }



        $new_batch=new Batches();
        $new_batch->calendar_type=$calendar_type;


        $new_batch->start_date=$temp_date_range_start;
        $new_batch->end_date=$temp_date_range_end;
        $new_batch->save();

        if ($calendar_type == 'Third party cdl' || $calendar_type == 'Third party standard') {


            if ($calendar_type == 'Third party cdl') {
                $third_party_users = Users::find()->where(['user_group'=>'thirdparty-cdl'])->all();
            } else if ($calendar_type == 'Third party standard') {
                $third_party_users = Users::find()->where(['user_group'=>'thirdparty-ncdl'])->all();
            }




            foreach ($third_party_users as $third_party_user) {
                if ($effective_date != '') {
                    $effective_date1 = explode("-", $effective_date);
                    $effective_date1 = $effective_date1[2] . '-' . $effective_date1[0] . '-' . $effective_date1[1];

                    $i = 0;
                    while ($i < $total_slots) {
                        $j = 0;
                        while ($j < $repeat_times[$i]) {
                            $model = new Slots();
                            $model->calendar_type = $calendar_type;
                            $model->duration = $duration;
                            $model->slot_date = $effective_date1;
                            $model->start_time = $start_time[$i];
                            $model->third_party_user_id = $third_party_user->id;
                            if (!$model->save())
                                print_r($model->getErrors());
                            $j++;
                        }

                        $i++;
                    }


                } else if ($date_range_start != '') {

                    // Start date
                    $start_date = date("Y-m-d", strtotime(str_ireplace('-', '/', $date_range_start)));
                    // End date
                    $end_date = date("Y-m-d", strtotime(str_ireplace('-', '/', $date_range_end)));

                    while (strtotime($start_date) <= strtotime($end_date)) {


                        $effective_date1 = $start_date;


                        $i = 0;
                        while ($i < $total_slots) {
                            $j = 0;
                            while ($j < $repeat_times[$i]) {
                                $model = new Slots();
                                $model->calendar_type = $calendar_type;
                                $model->duration = $duration;
                                $model->slot_date = $effective_date1;
                                $model->start_time = $start_time[$i];
                                $model->third_party_user_id = $third_party_user->id;
                                if (!$model->save())
                                    print_r($model->getErrors());
                                $j++;
                            }

                            $i++;
                        }

                        $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));

                    }


                }
            }


            $this->redirect(['index']);

        } else {


            if ($effective_date != '') {
                $effective_date = explode("-", $effective_date);
                $effective_date = $effective_date[2] . '-' . $effective_date[0] . '-' . $effective_date[1];

                $i = 0;
                while ($i < $total_slots) {
                    $j = 0;
                    while ($j < $repeat_times[$i]) {
                        $model = new Slots();
                        $model->calendar_type = $calendar_type;
                        $model->duration = $duration;
                        $model->slot_date = $effective_date;
                        $model->start_time = $start_time[$i];
                        if (!$model->save())
                            print_r($model->getErrors());
                        $j++;
                    }

                    $i++;
                }


            } else if ($date_range_start != '') {

                // Start date
                $start_date = date("Y-m-d", strtotime(str_ireplace('-', '/', $date_range_start)));
                // End date
                $end_date = date("Y-m-d", strtotime(str_ireplace('-', '/', $date_range_end)));

                while (strtotime($start_date) <= strtotime($end_date)) {


                    $effective_date = $start_date;


                    $i = 0;
                    while ($i < $total_slots) {
                        $j = 0;
                        while ($j < $repeat_times[$i]) {
                            $model = new Slots();
                            $model->calendar_type = $calendar_type;
                            $model->duration = $duration;
                            $model->slot_date = $effective_date;
                            $model->start_time = $start_time[$i];
                            if (!$model->save())
                                print_r($model->getErrors());
                            $j++;
                        }

                        $i++;
                    }

                    $start_date = date("Y-m-d", strtotime("+1 day", strtotime($start_date)));

                }


            }
        }

        $this->redirect(['index']);


    }

    public function actionChangestatus()
    {
        $post = Yii::$app->request->post();
        $action = Yii::$app->request->post('action');
        $all_pages = Yii::$app->request->post('checkbox-check-all-records');
        $selection = (array)Yii::$app->request->post('selection');//typecasting

        if ($action != 'mark') {
            if ($all_pages == 'all_pages') {
                $slots = Slots::find()->all();

                foreach ($slots as $slot) {
                    $model = $this->findModel($slot->id);
                    $model->status = $action;
                    if (!$model->save()) {
                        print_r($model->getErrors());
                        exit();
                    }
                }
            } else {

                if ($action == 'Auto Reschedule') {
                    foreach ($selection as $id) {
                        $model = $this->findModel($id);
                        if ($model->status == 'Reserved') {
                            $new_slot_date = date('Y-m-d', strtotime($model->slot_date . ' +2 day'));
                            $next_available_slot = Slots::find()->select(['*'])
                                ->andFilterWhere([
                                    'and',
                                    ['>', 'slot_date', $new_slot_date],
                                    ['=', 'start_time', $model->start_time],
                                    ['=', 'calendar_type', $model->calendar_type],
                                    ['!=', 'status', 'Reserved'],
                                    ['=', 'third_party_user_id', $model->third_party_user_id],


                                ])->one();
                            if ($next_available_slot) {

                                $next_available_slot->status = 'Reserved';
                                $next_available_slot->reserved_by_customer_id = $model->reserved_by_customer_id;
                                $next_available_slot->third_party_user_id = $model->third_party_user_id;
                                if (!$next_available_slot->save()) {
                                    print_r($model->getErrors());
                                    exit();
                                }


                                $model->status = 'Inactive';
                                $model->reserved_by_customer_id = 0;
                                $model->third_party_user_id = 0;
                                if (!$model->save()) {
                                    print_r($model->getErrors());
                                    exit();
                                }
                                Yii::$app->session->setFlash('success', 'Slot(s) rescheduled successfuly!');
                                $this->redirect(Yii::$app->request->referrer);

                            } else {
                                Yii::$app->session->setFlash('warning', 'Some/All of the slots you selected could not be reschuduled because there is no empty slot!');
                                $this->redirect(Yii::$app->request->referrer);
                            }


                        } else {
                            Yii::$app->session->setFlash('warning', 'Some/All of the slots you selected are not reserved. Only reserved slots can be rescheduled!');
                            $this->redirect(Yii::$app->request->referrer);
                        }


                    }
                } else if ($action == 'Reschedule') {

                    foreach ($selection as $id) {

                        if ($post['r_date-' . $id] != '0') {
                            $model = $this->findModel($id);
                            $next_available_slot = $this->findModel($post['r_date-' . $id]);
                            $next_available_slot->status = 'Reserved';
                            $next_available_slot->reserved_by_customer_id = $model->reserved_by_customer_id;
                            $next_available_slot->third_party_user_id = $model->third_party_user_id;
                            if (!$next_available_slot->save()) {
                                print_r($model->getErrors());
                                exit();
                            }


                            $model->status = 'Inactive';
                            $model->reserved_by_customer_id = 0;
                            $model->third_party_user_id = 0;
                            if (!$model->save()) {
                                print_r($model->getErrors());
                                exit();
                            }
                            Yii::$app->session->setFlash('success', 'Slot(s) rescheduled successfuly!');
                            $this->redirect(Yii::$app->request->referrer);

                        }
                    }

                } else if ($action == 'Active' OR $action == 'Inactive') {
                    foreach ($selection as $id) {
                        $model = $this->findModel($id);
                        $model->status = $action;
                        if (!$model->save()) {
                            print_r($model->getErrors());
                            exit();
                        } else {
                            Yii::$app->session->setFlash('success', 'Changes saved');
                            $this->redirect(Yii::$app->request->referrer);
                        }
                    }
                }

            }
        }


    }

    /**
     * Displays a single Slots model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Slots model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Slots();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Slots model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Slots model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Slots model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Slots the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Slots::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
