<?php

namespace app\controllers;

use app\models\ChangeEmail;
use app\models\SiteSettings;
use app\models\User;
use app\models\Users;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SignupForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\Customers;
use app\models\CustomersSearch;
use app\models\CustomerReservedTimeslots;
use app\models\SiteIndexCustomerReservedTimeslotsSearch;
use app\models\SiteIndexTab2CustomerReservedTimeslotsSearch;
use app\models\SiteIndexTab3CustomerReservedTimeslotsSearch;
use app\models\SiteIndexTab4CustomerReservedTimeslotsSearch;
use app\models\SiteIndexTab5CustomerReservedTimeslotsSearch;
use app\models\PasswordForm;
use app\models\BusinessInformation;
use app\models\ServicesCommonName;
use app\models\StaffCommonName;
use app\models\Countries;
use app\models\ProfessionsList;






class SignupController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?','@'],
                    ],
                    [
                        'actions' => ['logout','changepassword'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'language' => ['post'],
                    'customerportalstatus'=>['post'],
                ],
            ],
        ];
    }
    public function beforeAction($action)
    {
        if ($action->id == 'signup') {
            $this->enableCsrfValidation = false;
        }

        return parent::beforeAction($action);
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {


        if(@$id=Yii::$app->request->get('id')){

            $model = User::findOne($id);
        } else {
            $model = new SignupForm();
        }

      // print_r($model);
       // exit();
        $business_information = new BusinessInformation();
        if ($model->load(Yii::$app->request->post())) {
            $model->user_group = 'admin';
            if ($business_information->load(Yii::$app->request->post())) {

                $business_information->business_subdomain = $model->username;
                if($business_information->save(false)){

                    $business_id  = $business_information->business_id;
                    $model->business_id = $business_information->business_id;

                }
             //   \Yii::$app->user->login($model);
              //  return $this->redirect(['/site/index']);

            }

            if(@$user_id=Yii::$app->request->post('user_id')){

                $user=Users::findOne($user_id);

                return $this->redirect(Yii::getAlias('@web').'/index.php/users/index',302);

            } else {

                if ($user = $model->signup()) {
                    $service_common_model = new ServicesCommonName();
                    $service_common_model->business_id=$business_id;
                    $service_common_model->singular_name='Service';
                    $service_common_model->plural_name='Services';
                    $service_common_model->save();
                    $staff_common_model = new StaffCommonName();
                    $staff_common_model->business_id=$business_id;
                    $staff_common_model->staff_singular='Staff';
                    $staff_common_model->staff_plural='Staffs';
                    $staff_common_model->save();

                    // $service_common_model = new ServicesCommonName();
                   // $staff_common_model = new StaffCommonName();
                      //\Yii::$app->user->login($user);
                     // return $this->redirect(['/site/login?id=signup']);
                    if (Yii::$app->getUser()->login($user)) {
                    return $this->redirect(Yii::getAlias('@web').'/index.php',302);
                     }
                }
            }

        }
    $countries = Countries::find()->all();
    $profession_list = ProfessionsList::find()->all();
        unset($profession_list[0]);
        unset($profession_list[1]);

        $this->layout = 'noheader';
        return $this->render('signup', [
            'model' => $model,
            'business_information'=>$business_information,
            'professions_list'=>$profession_list,
            'countries'=>$countries,
        ]);
    }

    public function actionRoutes()
    {
        return $this->render('routes');
    }

    /**
     * Ajax handler for language change dropdown list. Sets cookie ready for next request
     */
    public function actionLanguage()
    {
        if ( Yii::$app->request->post('_lang') !== NULL && array_key_exists(Yii::$app->request->post('_lang'), Yii::$app->params['languages']))
        {
            Yii::$app->language = Yii::$app->request->post('_lang');
            $cookie = new yii\web\Cookie([
                'name' => '_lang',
                'value' => Yii::$app->request->post('_lang'),
            ]);
            Yii::$app->getResponse()->getCookies()->add($cookie);
        }
        Yii::$app->end();
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $this->layout = 'noheader';
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

   










    public function updatePassword($new_password) {
        return $password_hash = Yii::$app->security->generatePasswordHash($new_password);
    }

}
