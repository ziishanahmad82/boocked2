<?php

namespace app\controllers;

use app\models\ResoucesService;
use Yii;
use app\models\Resources;
use app\models\Services;
use yii\data\ActiveDataProvider;
use app\models\ResourcesCommonName;
use yii\web\Controller;
use yii\web\Response;
use yii\web\NotFoundHttpException;
use yii\widgets\ActiveForm;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * ResourceController implements the CRUD actions for Resources model.
 */
class ResourceController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Resources models.
     * @return mixed
     */
    public function actionIndex()
    {   $resourcesCommonModel = new ResourcesCommonName();
        $Services = new Services();
        $ServicesList = $Services->getLinkedActiveServices();

        //exit();
        $business_id = Yii::$app->user->identity->business_id;
        $resources = Resources::find()->where(['business_id'=>$business_id])->all();
        $resourcesCommonData = $resourcesCommonModel->commonRelatedNames();
      //  $avc = Resources::find();
       // print_r($avc);
     ////   $dataProvider = new ActiveDataProvider([
      //      'query' => Resources::find(),
       // ]);
        return $this->render('index', [
            'dataProvider' => $resources,
            'resourcesCommonModel' => $resourcesCommonModel,
            'resourcesCommonData' => $resourcesCommonData,
            'ServicesList' => $ServicesList,
        ]);
    }

    /**
     * Displays a single Resources model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Resources model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Resources;
        if($model->load(Yii::$app->request->post())){
            $model->business_id = Yii::$app->user->identity->business_id;
            if($model->save(false)){
                return $this->redirect(['index']);

            }else {
               return 2;
            }



        }
    }

    /**
     * Updates an existing Resources model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
      
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            echo 'saved';
           // return $this->redirect(['view', 'id' => $model->resource_id]);
        } else {
            echo 'not';
            //return $this->render('update', [
            //    'model' => $model,
           // ]);
        }
    }
    public function actionValidate()
    {

        $model = new ResourcesCommonName();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;

        }
        return ActiveForm::validate($model);
    }
    public function actionSavecommon($id=1)
    {
     
     //   $serviceCommonNameModel = new ResourcesCommonName();
        $model = ResourcesCommonName::findOne($id);
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            if($model->save()){
                echo 1;
            }else {
                echo 2;

            }
            
        }
       
    }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
return 1;
       // return $this->redirect(['index']);
    }

    /**
     * Finds the Resources model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Resources the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */

    protected function findModel($id)
    {
        if (($model = Resources::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
