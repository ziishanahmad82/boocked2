<?php

namespace app\controllers;

use app\models\Customers;
use app\models\Users;
use Yii;
use app\models\Reports;
use app\models\ReportsSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Services;
use app\models\Appointments;
use app\models\AppointmentsPayments;


/**
 * ReportsController implements the CRUD actions for Reports model.
 */
class ReportsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Reports models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ReportsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    public function actionAppointmentsreports()
    {
        $business_id = Yii::$app->user->identity->business_id;
        $searchModel = new ReportsSearch();
        $model = Appointments::findOne(['business_id'=>$business_id]);
        $services = Services::find()->where(['business_id'=>$business_id])->all();
        $staffs = Users::find()->where(['business_id'=>$business_id])->all();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('appointment_reports', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'services'=>$services,
            'staffs'=>$staffs,
            'model'=>$model,
        ]);
    }
 //Appointment Search
    public function actionAppointmentsearch(){

  //  List Type
        if($_POST['list_type']==0){
            $list_type='date_created';
        }else {
            $list_type='appointment_date';
           // echo 'booking date';
        }
        if($_POST['start_date']){
           $start_date = $_POST['start_date']; 
        }else {

            $start_date = date('Y-m-d');
        }
        if($_POST['end_date']){
            $end_date = $_POST['end_date'];
        }else {

            $end_date = date('Y-m-d');
        }

  // Report type
        if($_POST['report_type']==0){

        }else if($_POST['report_type']==1){

        } else if($_POST['report_type']==2){


        }
// Service
        if($_POST['service_type']==0){
            $service_type = '';
        }else {
            $service_type = $_POST['service_type'];
        }
// Staff
        if($_POST['staff_type']==0){
            $staff_type='';
        }else {
         $staff_type = $_POST['staff_type'];

        }
// Appointment type
        if($_POST['appointment_type']==0){
            $appointment_type ='payment_status';
            $appointment_type_value ='';
        }else if($_POST['appointment_type']==1){
            $appointment_type ='payment_status';
            $appointment_type_value ='paid';
        }else if($_POST['appointment_type']==2) {
            $appointment_type = 'payment_status';
            $appointment_type_value = 'unpaid';
        }else if($_POST['appointment_type']==3) {
            $appointment_type = 'approval';
            $appointment_type_value = '1';
        } else if($_POST['appointment_type']==4) {
            $appointment_type = 'approval';
            $appointment_type_value = '0';
        }
// Appointment Status
        if($_POST['appointment_status']==0){
            $appointment_status ='';

        }else if($_POST['appointment_status']==1){
            $appointment_status ='As Schedule';

        }else if($_POST['appointment_status']==2) {
            $appointment_status="Arrived Late";
        }else if($_POST['appointment_status']==3) {
            $appointment_status="Gift Certificates";
        } else if($_POST['appointment_status']==4) {
            $appointment_status="cancel";
        } else if($_POST['appointment_status']==5) {
            $appointment_status="No Show";
        }

//Query

        if($_POST['report_type']==0) {
            $query = Appointments::find()->joinWith(['services','users','customers']);
            $appointmentReports = $query->select(['*'])->where(['appointments.business_id' => Yii::$app->user->identity->business_id])
                ->andFilterWhere(['and', ['=', 'appointments.user_id', $staff_type]])
                ->andFilterWhere(['and', ['=', 'appointments.service_id', $service_type]])
                ->andFilterWhere(['or', ['=', $list_type, $start_date], ['>', $list_type, $start_date]])
                ->andFilterWhere(['or', ['=', $list_type, $end_date], ['<', $list_type, $end_date]])
                ->andFilterWhere(['and', ['=', 'appointments.' . $appointment_type, $appointment_type_value]])
                ->andFilterWhere(['and', ['=', 'appointments.appointment_status', $appointment_status]])
                ->all();

            return $this->renderAjax('reports_results', [
                'appointmentReports' => $appointmentReports,
                'grouped'=>'detail',

            ]);
        }else  if($_POST['report_type']==2) {
           // $query = Appointments::find()->joinWith(['services','users']);
            $appointmentReports = Appointments::find()->select(['COUNT(appointment_id) AS cnt','COUNT(DISTINCT user_id) AS cntuser','DATE_FORMAT(appointment_date, "%m-%Y") as m_date'])
                ->where(['appointments.business_id' => Yii::$app->user->identity->business_id])
                ->andFilterWhere(['and', ['=', 'appointments.user_id', $staff_type]])
                ->andFilterWhere(['and', ['=', 'appointments.service_id', $service_type]])
                ->andFilterWhere(['or', ['=', $list_type, $start_date], ['>', $list_type, $start_date]])
                ->andFilterWhere(['or', ['=', $list_type, $end_date], ['<', $list_type, $end_date]])
                ->andFilterWhere(['and', ['=', 'appointments.' . $appointment_type, $appointment_type_value]])
                ->andFilterWhere(['and', ['=', 'appointments.appointment_status', $appointment_status]])
                ->groupBy ('m_date')->all();

            return $this->renderAjax('reports_results', [
                'appointmentReports' => $appointmentReports,
                'grouped'=>'month',

            ]);
            

        }else  if($_POST['report_type']==1) {
            // $query = Appointments::find()->joinWith(['services','users']);
            $appointmentReports = Appointments::find()->select(['COUNT(appointment_id) AS cnt','COUNT(DISTINCT user_id) AS cntuser','appointment_date'])
                ->where(['appointments.business_id' => Yii::$app->user->identity->business_id])
                ->andFilterWhere(['and', ['=', 'appointments.user_id', $staff_type]])
                ->andFilterWhere(['and', ['=', 'appointments.service_id', $service_type]])
                ->andFilterWhere(['or', ['=', $list_type, $start_date], ['>', $list_type, $start_date]])
                ->andFilterWhere(['or', ['=', $list_type, $end_date], ['<', $list_type, $end_date]])
                ->andFilterWhere(['and', ['=', 'appointments.' . $appointment_type, $appointment_type_value]])
                ->andFilterWhere(['and', ['=', 'appointments.appointment_status', $appointment_status]])
                ->groupBy ('appointment_date')->all();

            return $this->renderAjax('reports_results', [
                'appointmentReports' => $appointmentReports,
                'grouped'=>'month',

            ]);



        }

    }

    public function actionSalesreports()
    {
        $business_id = Yii::$app->user->identity->business_id;
        $searchModel = new ReportsSearch();
        $model = Appointments::findOne(['business_id'=>$business_id]);
        $services = Services::find()->where(['business_id'=>$business_id])->all();
        $staffs = Users::find()->where(['business_id'=>$business_id])->all();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('sales_reports', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'services'=>$services,
            'staffs'=>$staffs,
            'model'=>$model,
        ]);
    }


    //Appointment Search
    public function actionSalessearch(){

        //Dates
        if($_POST['start_date']){
            $start_date = $_POST['start_date'];
        }else {

            $start_date = date('Y-m-d');
        }
        if($_POST['end_date']){
            $end_date = $_POST['end_date'];
        }else {

            $end_date = date('Y-m-d');
        }

        // Report type

// Service
        if($_POST['service_type']==0){
            $service_type = '';
        }else {
            $service_type = $_POST['service_type'];
        }
// Staff
        if($_POST['staff_type']==0){
            $staff_type='';
        }else {
            $staff_type = $_POST['staff_type'];

        }
// Appointment type
        if($_POST['appointment_type']==0){
            $appointment_type ='payment_status';
            $appointment_type_value ='';
        }else if($_POST['appointment_type']==3) {
            $appointment_type = 'appointments.approval';
            $appointment_type_value = '1';
        } else if($_POST['appointment_type']==4) {
            $appointment_type = 'appointments.approval';
            $appointment_type_value = '0';
        }
       // print_r($start_date);
//print_r($end_date);
      //  exit();
        if($_POST['report_type']==0) {

//Query
            //   $query = AppointmentsPayments::find()->joinWith(['customers','appointments'])->joinWith(['services', 'services.appointments']);
            $query = AppointmentsPayments::find()->joinwith('customers')->joinWith(['appointments', 'appointments.services', 'appointments.users']);
            //  if($_POST['report_type']==0) {
            $appointmentReports = $query->select(['*','appointments_payments.created_at as payment_date'])//->where(['appointments.business_id' => Yii::$app->user->identity->business_id])
               ->andFilterWhere(['and', ['=', 'appointments.user_id', $staff_type]])
               ->andFilterWhere(['and', ['=', 'appointments.service_id', $service_type]])
               ->andFilterWhere(['or', ['=', 'appointments_payments.created_at', $start_date], ['>', 'appointments_payments.created_at', $start_date]])
               ->andFilterWhere(['or', ['=', 'appointments_payments.created_at', $end_date.' 23:59:59'], ['<', 'appointments_payments.created_at', $end_date.' 23:59:59']])
               ->andFilterWhere(['and', ['=', $appointment_type , $appointment_type_value]])

            ->all();

            return $this->renderAjax('reports_results', [
                'appointmentReports' => $appointmentReports,
                'grouped' => 'sales_detail',

            ]);
//        }else


        }
else if($_POST['report_type']==2) {
             $query = AppointmentsPayments::find()->joinWith(['appointments']);
                $appointmentReports = $query->select(['COUNT(ap_id) AS paymentcnt','COUNT(DISTINCT appointments.user_id) AS paymentuser','DATE_FORMAT(appointments_payments.created_at, "%m-%Y") as payment_date','sum(appointments_payments.total) as totalpayment'])
         ///  $appointmentReports = AppointmentsPayments::find()->select(['COUNT(appointment_id) AS cnt','COUNT(DISTINCT user_id) AS cntuser','DATE_FORMAT(appointment_date, "%m-%Y") as m_date'])

                    ->andFilterWhere(['and', ['=', 'appointments.user_id', $staff_type]])
                    ->andFilterWhere(['and', ['=', 'appointments.service_id', $service_type]])
                    ->andFilterWhere(['or', ['=', 'appointments_payments.created_at', $start_date], ['>', 'appointments_payments.created_at', $start_date]])
                    ->andFilterWhere(['or', ['=', 'appointments_payments.created_at',$end_date.' 23:59:59'], ['<', 'appointments_payments.created_at', $end_date.' 23:59:59']])
                    ->andFilterWhere(['and', ['=', $appointment_type , $appointment_type_value]])
                ->groupBy ('payment_date')->all();

            return $this->renderAjax('reports_results', [
                'appointmentReports' => $appointmentReports,
                'grouped'=>'salesmonth',

            ]);


        }else  if($_POST['report_type']==1) {
    $query = AppointmentsPayments::find()->joinWith(['appointments']);
            $appointmentReports = $query->select(['COUNT(ap_id) AS paymentcnt','COUNT(DISTINCT appointments.user_id) AS paymentuser','appointments_payments.created_at','sum(appointments_payments.total) as totalpayment'])
                ->andFilterWhere(['and', ['=', 'appointments.user_id', $staff_type]])
                ->andFilterWhere(['and', ['=', 'appointments.service_id', $service_type]])
                ->andFilterWhere(['or', ['=', 'appointments_payments.created_at', $start_date], ['>', 'appointments_payments.created_at', $start_date]])
               ->andFilterWhere(['or', ['=', 'appointments_payments.created_at', $end_date.' 23:59:59'], ['<', 'appointments_payments.created_at', $end_date.' 23:59:59']])
                ->andFilterWhere(['and', ['=', $appointment_type , $appointment_type_value]])
               ->groupBy ('appointments_payments.created_at')->all();

            return $this->renderAjax('reports_results', [
                'appointmentReports' => $appointmentReports,
                'grouped'=>'salesmonth',

            ]);



        }




//echo '<pre>';
        ////    print_r($appointmentReports);
        // echo '</pre>';
        //  echo $appointmentReports->createCommand()->getRawSql();

        //foreach($appointmentReports as $appointmentReport){
        //    echo  $appointmentReport->apntid.'<br/>';
        //  }

        //   }

        //  print_r($appointmentReports);
    }

    /**
     * Displays a single Reports model.
     * @param integer $id
     * @return mixed
     */
    public function actionCustomerreports()
    {
        $business_id = Yii::$app->user->identity->business_id;
        $searchModel = new ReportsSearch();
        $model = Appointments::findOne(['business_id'=>$business_id]);
        $services = Services::find()->where(['business_id'=>$business_id])->all();
        $staffs = Users::find()->where(['business_id'=>$business_id])->all();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('customer_reports', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'services'=>$services,
            'staffs'=>$staffs,
            'model'=>$model,
        ]);
    }

    public function actionCustomersearch(){

        //Dates
        if($_POST['start_date']){
            $start_date = $_POST['start_date'];
        }else {

            $start_date = date('Y-m-d');
        }
        if($_POST['end_date']){
            $end_date = $_POST['end_date'];
        }else {

            $end_date = date('Y-m-d');
        }

        // Report type

// Service
        if($_POST['verify_customer']==2){
            $verify_customer= '';
        }else {
            $verify_customer = $_POST['verify_customer'];
        }
// Staff
        if(isset($_POST['customer_tag'])){
            $customer_tag=$_POST['customer_tag'];
        }else {
            $customer_tag='';

        }



//Query
       if($_POST['advance_customer_radio']==1) {
           //   $query = AppointmentsPayments::find()->joinWith(['customers','appointments'])->joinWith(['services', 'services.appointments']);
           $query = Customers::find()->joinwith('appointments');
           //  if($_POST['report_type']==0) {
           $appointmentReports = $query->select(['*', 'customers.customer_id'])->where(['appointments.customer_id' => NULL, 'customers.business_id' => Yii::$app->user->identity->business_id])
               ->andFilterWhere(['and', ['=', 'customers.verify_status', $verify_customer]])
               ->andFilterWhere(['or', ['=', 'customers.createdat', $start_date], ['>', 'customers.createdat', $start_date]])
               ->andFilterWhere(['or', ['=', 'customers.createdat', $end_date . ' 23:59:59'], ['<', 'customers.createdat', $end_date . ' 23:59:59']])
               ->all();
         

           return $this->renderAjax('reports_results', [
               'appointmentReports' => $appointmentReports,
               'grouped' => 'customer_group',

           ]);
//        }else


           //    }
       }else if($_POST['advance_customer_radio']==0) {
            $query = Customers::find()->joinWith(['appointments']);
           $appointmentReports = $query->select(['*', 'customers.customer_id'])->where([ 'customers.business_id' => Yii::$app->user->identity->business_id])
               ->andFilterWhere(['and', ['=', 'customers.verify_status', $verify_customer]])
               ->andFilterWhere(['or', ['=', 'customers.createdat', $start_date], ['>', 'customers.createdat', $start_date]])
               ->andFilterWhere(['or', ['=', 'customers.createdat', $end_date . ' 23:59:59'], ['<', 'customers.createdat', $end_date . ' 23:59:59']])
               ->all();

           return $this->renderAjax('reports_results', [
               'appointmentReports' => $appointmentReports,
               'grouped' => 'customer_group',

           ]);


        }else  if($_POST['advance_customer_radio']==2) {
           if($_POST['booked_start_date']){
               $booked_start_date = $_POST['booked_start_date'];
           }else {

               $booked_start_date = date('Y-m-d');
           }
           if($_POST['booked_end_date']){
               $booked_end_date = $_POST['booked_end_date'];
           }else {

               $booked_end_date = date('Y-m-d');
           }
           $query = Customers::find()->joinWith(['appointments']);

           //['customers.business_id' => Yii::$app->user->identity->business_id]
           $appointmentReports = $query->select(['*', 'customers.customer_id'])->where(['not between', 'appointments.date_created', $booked_start_date, $booked_end_date ])
               ->andWhere([ 'customers.business_id' => Yii::$app->user->identity->business_id])
               ->andFilterWhere(['and', ['=', 'customers.verify_status', $verify_customer]])
              ->orFilterWhere(['and', ['=', 'appointments.customer_id', NULL],[ 'customers.business_id' => Yii::$app->user->identity->business_id],['or', ['=', 'customers.createdat', $start_date], ['>', 'customers.createdat', $start_date]],['or', ['=', 'customers.createdat', $end_date . ' 23:59:59'], ['<', 'customers.createdat', $end_date . ' 23:59:59']],['=', 'customers.verify_status', $verify_customer]  ])
               ->andFilterWhere(['or', ['=', 'customers.createdat', $start_date], ['>', 'customers.createdat', $start_date]])
               ->andFilterWhere(['or', ['=', 'customers.createdat', $end_date . ' 23:59:59'], ['<', 'customers.createdat', $end_date . ' 23:59:59']])

             //  ->andFilterWhere(['or', ['=', 'appointments.date_created', $booked_start_date], ['>', 'appointments.date_created', $booked_start_date]])
             //  ->andFilterWhere(['or', ['=', 'appointments.date_created', $booked_end_date . ' 23:59:59'], ['<', 'appointments.date_created', $booked_end_date . ' 23:59:59']])
               ->all();

           return $this->renderAjax('reports_results', [
               'appointmentReports' => $appointmentReports,
               'grouped' => 'customer_group',

           ]);



        }




//echo '<pre>';
        ////    print_r($appointmentReports);
        // echo '</pre>';
        //  echo $appointmentReports->createCommand()->getRawSql();

        //foreach($appointmentReports as $appointmentReport){
        //    echo  $appointmentReport->apntid.'<br/>';
        //  }

        //   }

        //  print_r($appointmentReports);
    }


    public function actionUnapprovedappointments()
    {
        $business_id = Yii::$app->user->identity->business_id;
        $searchModel = new ReportsSearch();
        $model = Appointments::findOne(['business_id'=>$business_id]);
        $services = Services::find()->where(['business_id'=>$business_id])->all();
        $staffs = Users::find()->where(['business_id'=>$business_id])->all();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('unapproved_reports', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'services'=>$services,
            'staffs'=>$staffs,
            'model'=>$model,
        ]);
    }



    public function actionUnapprovedsearch(){

        //  List Type
        if($_POST['list_type']==0){
            $list_type='appointments.date_created';
        }else {
            $list_type='appointments.appointment_date';
            // echo 'booking date';
        }
        if($_POST['start_date']){
            $start_date = $_POST['start_date'];
        }else {

            $start_date = date('Y-m-d');
        }
        if($_POST['end_date']){
            $end_date = $_POST['end_date'];
        }else {

            $end_date = date('Y-m-d');
        }

        // Report type
        if($_POST['report_type']==0){

        }else if($_POST['report_type']==1){

        } else if($_POST['report_type']==2){


        }
// Service
        if($_POST['service_type']==0){
            $service_type = '';
        }else {
            $service_type = $_POST['service_type'];
        }
// Staff
        if($_POST['staff_type']==0){
            $staff_type='';
        }else {
            $staff_type = $_POST['staff_type'];

        }


//Query

        if($_POST['report_type']==0) {
            $query = Appointments::find()->joinWith(['services','users','customers']);
            $appointmentReports = $query->select(['*'])->where(['appointments.business_id' => Yii::$app->user->identity->business_id,'appointments.approval'=>'0'])
                ->andFilterWhere(['and', ['=', 'appointments.user_id', $staff_type]])
                ->andFilterWhere(['and', ['=', 'appointments.service_id', $service_type]])
               ->andFilterWhere(['or', ['=', $list_type, $start_date], ['>', $list_type, $start_date]])
               ->andFilterWhere(['or', ['=', $list_type, $end_date], ['<', $list_type, $end_date]])
                ->andFilterWhere(['and', ['=', 'appointments.approval','0' ]])
                ->all();
         //   foreach($appointmentReports as $appointmentReport){
          //     echo  $appointmentReport->approval.'<br/>';

         //   }

            return $this->renderAjax('reports_results', [
                'appointmentReports' => $appointmentReports,
                'grouped'=>'detail',

            ]);
        }else  if($_POST['report_type']==2) {
            // $query = Appointments::find()->joinWith(['services','users']);
            $appointmentReports = Appointments::find()->select(['COUNT(appointment_id) AS cnt','COUNT(DISTINCT user_id) AS cntuser','DATE_FORMAT(appointment_date, "%m-%Y") as m_date'])
                ->where(['appointments.business_id' => Yii::$app->user->identity->business_id,'appointments.approval'=>'0'])
                ->andFilterWhere(['and', ['=', 'appointments.user_id', $staff_type]])
                ->andFilterWhere(['and', ['=', 'appointments.service_id', $service_type]])
                ->andFilterWhere(['or', ['=', $list_type, $start_date], ['>', $list_type, $start_date]])
                ->andFilterWhere(['or', ['=', $list_type, $end_date], ['<', $list_type, $end_date]])
                ->groupBy ('m_date')->all();

            return $this->renderAjax('reports_results', [
                'appointmentReports' => $appointmentReports,
                'grouped'=>'month',

            ]);


        }else  if($_POST['report_type']==1) {
            // $query = Appointments::find()->joinWith(['services','users']);
            $appointmentReports = Appointments::find()->select(['COUNT(appointment_id) AS cnt','COUNT(DISTINCT user_id) AS cntuser','appointment_date'])
                ->where(['appointments.business_id' => Yii::$app->user->identity->business_id,'appointments.approval'=>'0'])
                ->andFilterWhere(['and', ['=', 'appointments.user_id', $staff_type]])
                ->andFilterWhere(['and', ['=', 'appointments.service_id', $service_type]])
                ->andFilterWhere(['or', ['=', $list_type, $start_date], ['>', $list_type, $start_date]])
                ->andFilterWhere(['or', ['=', $list_type, $end_date], ['<', $list_type, $end_date]])
                ->groupBy ('appointment_date')->all();

            return $this->renderAjax('reports_results', [
                'appointmentReports' => $appointmentReports,
                'grouped'=>'month',

            ]);



        }

    }













    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Reports model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Reports();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Reports model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Reports model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Reports model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Reports the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Reports::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}