<?php

namespace app\controllers;

use Yii;
use app\models\Services;
use app\models\ServiceCategory;
use app\models\ServicesSearch;
use app\models\ServicesCommonName;
use app\models\WeeklyRecurringTime;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\db\Query;
use yii\web\View;
use app\models\Users;
use yii\web\UploadedFile;
use app\models\ServiceOffer;

$connection  = \Yii::$app->db;

/**
 * ServicesController implements the CRUD actions for Services model.
 */
class ServicesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],


        ];
    }

    /**
     * Lists all Services models.
     * @return mixed
     */
    public function actionIndex()
    {
        $business_id = Yii::$app->user->identity->business_id;
        $searchModel = new ServicesSearch();
        $serviceCommonNameModel = new ServicesCommonName();
        $servicesCommonNameData = $serviceCommonNameModel->commonRelatedNames();
        $query = new Query;

        $query->select(['*'])
            ->from('service_category')->where(['business_id'=>$business_id]);
        $command = $query->createCommand();
// $command->sql returns the actual SQL
        $services = $command->queryAll();
       // echo '<pre>';
       // print_r($services);
       // echo '</pre>';
      //  exit();
        // print_r($rows);
       // exit();

        $userCreateModel = new Users();
        $staffList = Users::find()->where(['business_id'=> $business_id])->all();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'services_categories' => $services,
            'serviceCommonNameModel' => $serviceCommonNameModel,
            'servicesCommonNameData' => $servicesCommonNameData,
            'userCreateModel' =>  $userCreateModel,
            'staffList' => $staffList
        ]);
    }
    public function actionCategories()
    {
        //print_r($_FILES);
        // print_r(Yii::$app->request->post('pname'));
        if(Yii::$app->request->post())

            $name=Yii::$app->request->post('pname');
        $sub=Yii::$app->request->post('psub');
        $img_link="";
        $uploads = \yii\web\UploadedFile::getInstanceByName('pimage');
        if (!empty($uploads)) {
            $img = 'uploads/categories/' . date("Y-m-d_H_i_s.") . $uploads->name;
            $uploads->saveAs($img);
            $img_link = "/admin/web/" . $img;
        }
        $img_link1="";
        $uploads1 = \yii\web\UploadedFile::getInstanceByName('pimage');
        if (!empty($uploads1)) {
            $img1 = 'uploads/categories/pressed_' . date("Y-m-d_H_i_s.") . $uploads->name;
            $uploads1->saveAs($img1);
            $img_link1 = "/admin/web/" . $img1;
        }
        Yii::$app->db->createCommand()
            ->insert('professions_list', [
                'profession_name' => $name,
                'image' => $img_link,
                'image_pressed' => $img_link1,
                'pro_parent'=>$sub
            ])->execute();


        return $this->redirect(['../mycategories']);
    }
    /**
     * Displays a single Services model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
    public function actionOffer()
    {
        //print_r($_FILES);
        // print_r(Yii::$app->request->post('pname'));
        if (Yii::$app->request->post()) {

             $id = Yii::$app->request->post('o_id');
             $desc = Yii::$app->request->post('o_desc');
             $sdate = Yii::$app->request->post('offer_start');
             $edate = Yii::$app->request->post('offer_end');
             $discount = Yii::$app->request->post('discount');
             $price = Yii::$app->request->post('o_price');
            //try to load model with available id i.e. unique key

           // $model = ServiceOffer::getservicebyid($id);

            //if(!$model){
                $model = new ServiceOffer();
                $model->service_id = $id;
            //}

            $model->offer_desc = $desc;
            $model->offer_start = $sdate;
            $model->offer_end = $edate;
            $model->discount = $discount;
            $model->price = $price;

            $model->save();

        }
        return $this->redirect('index');
    }
    /**
     * Creates a new Services model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate(){
        $model = new Services();
        $business_id = Yii::$app->user->identity->business_id;

        if(Yii::$app->request->post()) {

            //   print_r($_POST);
            //   exit();
            // service_category_name


            if ($model->load(Yii::$app->request->post())) {
              //  service_car_image1
             //   $imageFile1

                $model->google_address = Yii::$app->request->post("google_ad");
                $model->lat = Yii::$app->request->post("lat");
                $model->lng = Yii::$app->request->post("lng");

                $model->service_video  = UploadedFile::getInstance($model, 'service_video');
                $random_digit = rand(0000, 9999999);
                if($model->service_video->saveAs('uploads/' .str_replace(' ', '', $model->service_video->baseName).$random_digit.'.' . $model->service_video->extension)){
                    $model->service_video = 'uploads/' .str_replace(' ', '', $model->service_video->baseName).$random_digit. '.' . $model->service_video->extension;

                }


                $model->imageFile1 = UploadedFile::getInstance($model, 'imageFile1');
                $model->imageFile2 = UploadedFile::getInstance($model, 'imageFile2');
                $model->imageFile3 = UploadedFile::getInstance($model, 'imageFile3');



               // $service_model =  new Services;
                if ($model->validate()) {
                    if(!empty($model->imageFile1)) {
                        $random_digit = rand(0000, 9999999);
                        if ($model->imageFile1->saveAs('uploads/' . $model->imageFile1->baseName . $random_digit . '.' . $model->imageFile1->extension)) {
                            $model->service_car_image1 = 'uploads/' . $model->imageFile1->baseName . $random_digit . '.' . $model->imageFile1->extension;

                        }
                    }
                    if(!empty($model->imageFile2)) {
                        $random_digit = rand(0000, 9999999);
                        if ($model->imageFile2->saveAs('uploads/' . $model->imageFile2->baseName . $random_digit . '.' . $model->imageFile2->extension)) {
                            $model->service_car_image2 = 'uploads/' . $model->imageFile2->baseName . $random_digit . '.' . $model->imageFile2->extension;

                        }
                    }
                    if(!empty($model->imageFile3)) {
                    $random_digit = rand(0000, 9999999);
                    if($model->imageFile3->saveAs('uploads/' . $model->imageFile3->baseName.$random_digit. '.' . $model->imageFile3->extension)){
                        $model->service_car_image3  = 'uploads/'.$model->imageFile3->baseName.$random_digit. '.' . $model->imageFile3->extension;

                    }
                    }

                }

               // if($model->upload('service_car_image1')){
                //    echo 'heree1';

              //  }
             //   if($model->upload('imageFile2')){
               //     echo 'heree2';

            //    }


                if (!empty($_POST['service_category_name'])) {

                    $query = new Query;
                    if ($query->createCommand()->insert('service_category', [
                        'service_category_name' => $_POST['service_category_name'],
                        'business_id' =>$business_id,
                    ])->execute()
                    ) {

                        $model->service_cat_id = Yii::$app->db->getLastInsertID();
                        $model->business_id = $business_id;
                        $model->service_cat_id = Yii::$app->db->getLastInsertID();
                        if($model->save(false)){
                            return 1;

                        }
                    }
                }else {
                    $model->service_cat_id = 0;
                    $model->business_id = $business_id;
                    if($model->save(false)){
                        return 1;

                    }
                }



                   // return $this->redirect(['index', 'id' => $model->service_id]);
            }
        }else {
           // $s_Category_model = new ServiceCategory();
            $Category_model = ServiceCategory::find()->where(['business_id'=>Yii::$app->user->identity->business_id])->all();

            
          //  echo 'hereee';fdgdg
            return $this->renderAjax('create', ['model'=>$model,'Category_model'=>$Category_model]);
            //renderAjax('create', [
              //  'model' => $model,
           // ]);
        }

    }

    /**
     * Updates an existing Services model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {

        $model = $this->findModel($id);


        if ($model->load(Yii::$app->request->post())) {
            if($model->service_video!="") {
                $model->service_video = UploadedFile::getInstance($model, 'service_video');
                $random_digit = rand(0000, 9999999);
                if ($model->service_video->saveAs('uploads/' . str_replace(' ', '', $model->service_video->baseName) . $random_digit . '.' . $model->service_video->extension)) {
                    $model->service_video = 'uploads/' . str_replace(' ', '', $model->service_video->baseName) . $random_digit . '.' . $model->service_video->extension;

                }
            }


            $model->imageFile1 = UploadedFile::getInstance($model, 'imageFile1');
            $model->imageFile2 = UploadedFile::getInstance($model, 'imageFile2');
            $model->imageFile3 = UploadedFile::getInstance($model, 'imageFile3');


            // $service_model =  new Services;
            if ($model->validate()) {

                if(!empty($model->imageFile1)){
                $random_digit = rand(0000, 9999999);
                if ($model->imageFile1->saveAs('uploads/' . $model->imageFile1->baseName . $random_digit . '.' . $model->imageFile1->extension)) {
                    $model->service_car_image1 = 'uploads/' . $model->imageFile1->baseName . $random_digit . '.' . $model->imageFile1->extension;

                }
                }
                if(!empty($model->imageFile2)) {
                    $random_digit = rand(0000, 9999999);
                    if ($model->imageFile2->saveAs('uploads/' . $model->imageFile2->baseName . $random_digit . '.' . $model->imageFile2->extension)) {
                        $model->service_car_image2 = 'uploads/' . $model->imageFile2->baseName . $random_digit . '.' . $model->imageFile2->extension;

                    }
                }

                if(!empty($model->imageFile3)) {
                    $random_digit = rand(0000, 9999999);
                    if ($model->imageFile3->saveAs('uploads/' . $model->imageFile3->baseName . $random_digit . '.' . $model->imageFile3->extension)) {
                        $model->service_car_image3 = 'uploads/' . $model->imageFile3->baseName . $random_digit . '.' . $model->imageFile3->extension;

                    }
                }


                if ($model->save()) {
                    //  return $this->redirectAjax(['view', 'id' => $model->service_id]);
                    return 2;
                }
                // return $this->renderAjax('index', ['success'=>'Success']);
            }
        }else {
                $Category_model = ServiceCategory::find()->where(['business_id' => Yii::$app->user->identity->business_id])->all();
                return $this->renderAjax('update', [
                    'model' => $model, 'Category_model' => $Category_model

                ]);
            }

    }

    /**
     * Deletes an existing Services model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    public function actionCommonNames($id)
    {


    }
    public function actionValidate()
    {

        $model = new ServicesCommonName();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;

        }
        return ActiveForm::validate($model);
    }

    public function actionSavecommon()
    {
       // $model = new ServicesCommonName();
        $business_id = Yii::$app->user->identity->business_id;
        $serviceCommonNameModel = new ServicesCommonName();
         $model = ServicesCommonName::findOne(['business_id'=>$business_id]);
       //  ->where(['=','business_id',1]);
      //  $model = $this->findModel($id);
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
         //   \Yii::$app->response->format = Response::FORMAT_JSON;
        //   return ['success' => $model->save()];
            if($model->save()){
            //  echo  $model->singular_name;
            //  echo  $model->plural_name;
                Yii::$app->session->set('bk_singular_service', $model->singular_name);
                Yii::$app->session->set('bk_plural_service', $model->plural_name);
               echo 1;
            }else {
             echo 2;

           }
           // print_r('in iff');
        }
     //   print_r($this->actionIndex());
       // return $this->renderAjax('index');
      // return $this->renderAjax('index', [
        //    'model' => $model,
         //   'servicesCommonNameData' => $servicesCommonNameData,
        //]);
    }
    public function getCategory_services($id,$status){
        $query = new Query;
        $business_id = Yii::$app->user->identity->business_id;
        $services_cat = Services::find()->where(['service_cat_id' => $id , 'service_status'=>$status,'business_id'=>$business_id])->all();
      //  $query->select(['services.*'])
      //      ->from('services')
      //      ->where(['service_cat_id' => $id , 'service_status'=>$status]);
     //   $command = $query->createCommand();
// $command->sql returns the actual SQL
       // $services_cat = $command->queryAll();
      //  $this->findModel()->delete();
        return $services_cat;


    }
   public function actionUpdatecategoryname($id){
       //print_r(Yii::$app->request->post());

        if (($model = ServiceCategory::findOne($id)) !== null) {
         //   if(Yii::$app->db->createCommand()->update('service_category', ['service_category_name' => 1], 'service_cat_id = '.$id)->execute()){
            if(Yii::$app->db->createCommand()->update('service_category', Yii::$app->request->post(), 'service_cat_id = '.$id)->execute()){
                echo 1;
                   }else {
               echo 2;

            }
        }


    }
    public function actionCategorydelete()
    {
    $cat_id = $_POST['id'];

       if((ServiceCategory::findOne($_POST['id'])->delete()) &&(Services::deleteAll("service_cat_id =".$cat_id))){


           return 1;
       }

       // $this->findModel($id)->delete();

        //return $this->redirect(['index']);
    }
    public function actionSchedule(){
       // $business_id = Yii::$app->user->identity->business_id;  $staff_id , $service_id
    //  yRecurringTime
      //  exit();

         $service_id = $_POST['service_id'];
         $staff_id =  $_POST['staff_id'];
        $staffsun = json_encode($_POST['staffsun']);
        $staffmon = json_encode($_POST['staffmon']);
        $stafftue = json_encode($_POST['stafftue']);
        $staffwed = json_encode($_POST['staffwed']);
        $staffthurs = json_encode($_POST['staffthurs']);
        $stafffri = json_encode($_POST['stafffri']);
        $staffsat = json_encode($_POST['staffsat']);
        $query = new Query;
        if ($query->createCommand()->insert('weekly_recurring_time', [
            'sunday' => $staffsun,
            'monday' => $staffmon,
            'tuesday' =>$stafftue,
            'wednesday' =>$staffwed,
            'thursday' => $staffthurs,
            'friday' => $stafffri,
            'saturday' => $staffsat,
            'staff_id' => $staff_id,
            'service_id' => $service_id,
        ])->execute()
        ) {
         return 1;

        }


    }
    public function actionCreateschedule($service_id){
        $business_id = Yii::$app->user->identity->business_id;
        $userCreateModel = new Users();
        $staffList = Users::find()->where(['business_id'=> $business_id])->all();
        return $this->renderAjax('scedule', [
           'service_id'=>$service_id,
           'staffList'=>$staffList

        ]);


    }
    public function actionUpdatestaff_formslots($service_id, $staff_id){
        $weeklytime = WeeklyRecurringTime::find()->where(['service_id'=> $service_id, 'staff_id'=>$staff_id])->all();
        return $this->renderAjax('scedule', [
            'weeklytime'=> $weeklytime,
            'service_id'=> $service_id,
            'staff_id'=> $staff_id

        ]);
       // print_r($weeklytime);

    }
    public function actionUpdaterecurslots($id){
        $staffsun = json_encode($_POST['staffsun']);
        $staffmon = json_encode($_POST['staffmon']);
        $stafftue = json_encode($_POST['stafftue']);
        $staffwed = json_encode($_POST['staffwed']);
        $staffthurs = json_encode($_POST['staffthurs']);
        $stafffri = json_encode($_POST['stafffri']);
        $staffsat = json_encode($_POST['staffsat']);
        if (Yii::$app->db->createCommand()->update('weekly_recurring_time', [
            'sunday' => $staffsun,
            'monday' => $staffmon,
            'tuesday' =>$stafftue,
            'wednesday' =>$staffwed,
            'thursday' => $staffthurs,
            'friday' => $stafffri,
            'saturday' => $staffsat
        ], 'wrt_id = ' . $id)->execute()) {
          return 1;
        }
    }
    /**
     * Finds the Services model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Services the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionAddworkinghours($service_id){
        $business_id = Yii::$app->user->identity->business_id;
        $staffList = Users::find()->where(['business_id'=> $business_id])->all();
        $staff_id = $staffList[0]->id;
        $weeklytime = WeeklyRecurringTime::find()->where(['service_id'=> $service_id, 'staff_id'=>$staff_id])->all();
        return $this->renderAjax('add_working_hours', [
            'weeklytime'=> $weeklytime,
            'service_id'=> $service_id,
            'staff_id'=> $staff_id,
            'staffList'=> $staffList

        ]);

    }
    public function actionDisableservice($service_id,$status){

        if (Yii::$app->db->createCommand()->update('services', ['service_status' => $status], 'service_id = ' . $service_id)->execute()) {

           return 1;
        }

    }
    public function actionDisablefeatured($service_id,$status){

        if (Yii::$app->db->createCommand()->update('services', ['featured' => 0], 'service_id = ' . $service_id)->execute()) {

            return 1;
        }

    }
    public function actionEnablefeatured($service_id,$status){

        if (Yii::$app->db->createCommand()->update('services', ['featured' => 1], 'service_id = ' . $service_id)->execute()) {

            return 1;
        }

    }
    public function actionDisablepkgs($service_id,$status){

        if (Yii::$app->db->createCommand()->update('services', ['pkgs' => 0], 'service_id = ' . $service_id)->execute()) {

            return 1;
        }

    }
    public function actionEnablepkgs($service_id,$status){

        if (Yii::$app->db->createCommand()->update('services', ['pkgs' => 1], 'service_id = ' . $service_id)->execute()) {

            return 1;
        }

    }
    public function actionUploadservice_image($id)
    {
        if (($_FILES['profile_image']['type'] == 'image/jpeg') || ($_FILES['profile_image']['type'] == 'image/png')) {

            $uploaddir = 'uploads/';
            $random_digit = rand(0000, 9999);
            $uploadfile = $uploaddir . $random_digit . basename($_FILES['profile_image']['name']);

            if (move_uploaded_file($_FILES['profile_image']['tmp_name'], $uploadfile)) {
                if (Yii::$app->db->createCommand()->update('services', ['service_image' => $uploadfile], 'service_id = ' . $id)->execute()) {

                    echo $uploadfile;
                }
            } else {
                echo "Possible file upload attack!\n";
            }


        }else {

            return 0;
        }


    }
    public function actionUpdatelinkedresource($id){
        if(isset($_POST['linked_status'])) {
            if (Yii::$app->db->createCommand()->update('services', ['linked_status' => $_POST['linked_status']], 'service_id = ' . $id)->execute()) {
                echo 1;
            }
        }

    }
    protected function findModel($id)
    {
        if (($model = Services::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
