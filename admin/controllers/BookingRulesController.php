<?php

namespace app\controllers;

use Yii;
use app\models\SettingBookingRules;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\RestrictedDomains;
use yii\web\Response;
use yii\widgets\ActiveForm;
use app\models\BoockedPaymentGateways;

/**
 * BookingRulesController implements the CRUD actions for SettingBookingRules model.
 */
class BookingRulesController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all SettingBookingRules models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => SettingBookingRules::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionAdvance()
    {
       $business_id = Yii::$app->user->identity->business_id;
        $domain_model =  new RestrictedDomains();
        $restricted_domains = RestrictedDomains::find()->where(['business_id'=>$business_id])->all();
        $paypal_payment     = BoockedPaymentGateways::find()->where(['business_id'=>$business_id,'gateway_type'=>'paypal'])->one();
        if(!$paypal_payment){$paypal_payment = new BoockedPaymentGateways();}

        $authorize_payment     = BoockedPaymentGateways::find()->where(['business_id'=>$business_id,'gateway_type'=>'authorize'])->one();
        if(!$authorize_payment){$authorize_payment = new BoockedPaymentGateways();}
        $dataProvider = new ActiveDataProvider([
            'query' => SettingBookingRules::find(),
        ]);

        return $this->render('advance', [
            'dataProvider' => $dataProvider,
            'restricted_domains'=>$restricted_domains,
            'domain_model'=>$domain_model,
            'paypal_payment'=> $paypal_payment,
            'authorize_payment'=> $authorize_payment,

        ]);
    }
    public function actionPrivacy()
    {
        return $this->render('privacy');

    }
    public function actionNotification(){
        return $this->render('notification');

    }
    /**
     * Displays a single SettingBookingRules model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new SettingBookingRules model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new SettingBookingRules();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->bkrules_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing SettingBookingRules model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->bkrules_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing SettingBookingRules model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdaterulessetting(){
       $bkrule_name = $_POST['SettingBookingRules']['bkrules_name'];
       $bkrule_value = $_POST['SettingBookingRules']['bkrules_value'];
        $business_id = Yii::$app->user->identity->business_id;
        $model = SettingBookingRules::find()->where(['business_id'=>$business_id,'bkrules_name'=>$bkrule_name])->one();

        if(empty($model)){
            $models = new SettingBookingRules();
            $models->business_id = $business_id;

            if ($models->load(Yii::$app->request->post()) && $models->save()) {
                Yii::$app->session->set('bk_'.$bkrule_name, $bkrule_value);
            return 1;
            }

        }else {

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                Yii::$app->session->set('bk_'.$bkrule_name, $bkrule_value);
               return 1;
            }

        }


    }
    public function actionDomainvalidate(){
        $model = new RestrictedDomains();
        $request = \Yii::$app->getRequest();
        $model->business_id = Yii::$app->user->identity->business_id;
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;

        }
        return ActiveForm::validate($model);

    }
    
    public function actionSavedomains()
    {

       $model = new RestrictedDomains();

        $request = \Yii::$app->getRequest();
        $model->business_id = Yii::$app->user->identity->business_id;
        if ($request->isPost && $model->load($request->post())) {
            $model->business_id = Yii::$app->user->identity->business_id;
            if($model->save()){
                echo 1;
            }else {
                echo 2;

            }

        }

    }
    public function actionPaypalvalidate(){
        $model = new BoockedPaymentGateways();
        $request = \Yii::$app->getRequest();
        $model->business_id = Yii::$app->user->identity->business_id;
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;

        }
        return ActiveForm::validate($model);

    }
    public function actionPaypalsubmission(){
        $business_id = Yii::$app->user->identity->business_id;
        $model = BoockedPaymentGateways::find()->where(['business_id'=>$business_id,'gateway_type'=> $_POST['BoockedPaymentGateways']['gateway_type']])->one();
        if(!$model) {
            $model = new BoockedPaymentGateways();
        }
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            $model->business_id = Yii::$app->user->identity->business_id;
          //  $model->gateway_type='paypal';
            if($model->save(false)){
                echo 1;
            }else {
                echo 2;

            }


    } }

    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the SettingBookingRules model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return SettingBookingRules the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionTrash_tracking(){
        $business_id = Yii::$app->user->identity->business_id;
        $model = RestrictedDomains::find()->where(['business_id'=>$business_id,'rd_id'=> $_POST['domain']])->one();
    if($model->delete()){
    return 1;

    }
    }
    protected function findModel($id)
    {
        if (($model = SettingBookingRules::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
