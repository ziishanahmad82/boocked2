<?php

namespace app\controllers;

use Yii;
use app\models\CustomerReservedTimeslots;
use app\models\CustomerReservedTimeslotsSearch;
use app\models\Holidays;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use \DateTime;

/**
 * CustomerReservedTimeslotsController implements the CRUD actions for CustomerReservedTimeslots model.
 */
class CustomerReservedTimeslotsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomerReservedTimeslots models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerReservedTimeslotsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionIndex2()
    {
        $searchModel = new CustomerReservedTimeslotsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);


        return $this->render('index2', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }


    public function actionForm2()
    {
        
            $this->layout = 'noheader';
            return $this->render('_form2', [

            ]);

    }



    public function actionCellcolor(){
        $request = Yii::$app->request;
        $get = $request->get();
        $post = $request->post();
        $session = Yii::$app->session;

        $res_slots_count_normal=CustomerReservedTimeslots::find()
            ->select(['COUNT(timeslot_id) AS cnt'])
            ->where([
                'test_date'=>$get["date"],
                'confirmed'=>'1',
                'third_user_id'=>'0'
            ])
            ->one()->cnt;

        $res_slots_count_third=CustomerReservedTimeslots::find()
            ->select(['COUNT(timeslot_id) AS cnt'])
            ->where([
                'test_date'=>$get["date"],
                'confirmed'=>'1'
            ])->andWhere(['>', 'third_user_id', '0'])
            ->one()->cnt;

        //echo $res_slots_count->cnt;
        //$total_slots_per_day=$_REQUEST["total_slots_per_day"];
        $total_slots_per_day=8;
//        if($res_slots_count->cnt>=$total_slots_per_day)
//            $returnthis=['bgcolor'=>'coral', 'date'=>$get["date"], 'total_slots_per_day'=>$total_slots_per_day, 'day_status'=>'<h4>Third party : 0</h4><h4>Nomral : 0</h4>'];
//        else
            $returnthis=['bgcolor'=>'#5cb85c', 'date'=>$get["date"], 'total_slots_per_day'=>$total_slots_per_day, 'day_status'=>"<br/><br/><h4>Third party : $res_slots_count_third</h4><h4>DMV : $res_slots_count_normal</h4>"];

        $holidays=Holidays::find()
            ->where('holiday_date>=CURDATE()')
            ->all();

        //var_dump($holidays);



        foreach($holidays as $holiday){
            $holiday->holiday_name;
            $holiday->holiday_span;
            $holiday->holiday_date;
            $i=0;
            while($i < $holiday->holiday_span){


                $holiday_date = new DateTime($holiday->holiday_date);

                $holiday_date->modify("+{$i} day");
                $holiday_date = $holiday_date->format('Y-m-d') . "\n";

                $all_holidays[]=['holiday_name'=>'<br/><br/><h4>'.$holiday->holiday_name.'</h4>', 'holiday_date'=>$holiday_date];
                $i++;
            }

        }

        $returnthis['all_holidays']=$all_holidays;
//        echo "<pre>";
//        var_dump($returnthis);
//        echo "</pre>";

        $returnthis=json_encode($returnthis);
        return $returnthis;
    }


    public function actionSavestatus(){
        $request = Yii::$app->request;
        $get = $request->get();
        $post = $request->post();
        $session = Yii::$app->session;
        $model = $this->findModel($post['timeslot_id']);
        if($post['assign_toughbook']=='yes'){


            $model->tough_book_id=$post['toughbook-select1'];
            $model->status='Assigned to toughbook';
            if($model->save()){
                $session->setFlash('success', 'Test assigned to tough book.');
                $this->redirect(Yii::getAlias("@web").'/index.php');
            }

        } elseif($post['assign_toughbook']=='no'){
            $model->status=$post['status'];
            if($model->save()){
                $session->setFlash('success', 'Status Changed.');
                $this->redirect(Yii::getAlias("@web").'/index.php');
            }
        }



    }
    /**
     * Displays a single CustomerReservedTimeslots model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CustomerReservedTimeslots model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CustomerReservedTimeslots();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->timeslot_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CustomerReservedTimeslots model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->timeslot_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing CustomerReservedTimeslots model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CustomerReservedTimeslots model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomerReservedTimeslots the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CustomerReservedTimeslots::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
