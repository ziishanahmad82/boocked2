<?php

namespace app\controllers;

use Yii;
use app\models\Customers;
use app\models\CustomersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Appointments;
use app\models\AppointmentsPayments;
use yii\db\Expression;
use app\models\BusinessInformation;
use app\models\GiftCertificates;
use app\models\SoldGiftCertificates;
use app\models\Users;
use app\models\SentEmails;

/**
 * CustomersController implements the CRUD actions for Customers model.
 */
class CustomersController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Customers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomersSearch();
        $business_id = Yii::$app->user->identity->business_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $CustomerList = Customers::find()->where(['business_id'=>$business_id])->all();
        $NewCustomer = new Customers();
        $staffList = Users::find()->where(['business_id'=>$business_id])->all();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'NewCustomer' => $NewCustomer,
            'CustomerList'=> $CustomerList,
            'staffList'=>$staffList
        ]);
    }

    public function actionNew()
    {
        $searchModel = new CustomersSearch();
        $business_id = Yii::$app->user->identity->business_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $CustomerList = Customers::find()->where(['business_id'=>$business_id])
           ->andWhere(['between', 'createdat',  date('Y-m-d', strtotime('-30 days')),  date('Y-m-d') ])->orderBy('sort_order')->all();
        $staffList = Users::find()->where(['business_id'=>$business_id])->all();
        $NewCustomer = new Customers();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'NewCustomer' => $NewCustomer,
            'CustomerList'=> $CustomerList,
            'staffList'=>$staffList
        ]);
    }
    public function actionActive()
    {
        $searchModel = new CustomersSearch();
        $business_id = Yii::$app->user->identity->business_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $query = Customers::find()->joinWith(['appointments']);
        $CustomerList = $query->select(['*'])
            ->andFilterWhere(['<', 'appointments.appointment_date', date('Y-m-d', strtotime('-30 days')) ])
            ->andFilterWhere(['=', 'customers.business_id', $business_id])
            ->groupBy(['customers.customer_id'])
        ->all();
        $staffList = Users::find()->where(['business_id'=>$business_id])->all();
     //   $CustomerList = Customers::find()->where(['business_id'=>Yii::$app->user->identity->business_id])
        //    ->andWhere(['<', 'createdat',  date('Y-m-d')-30 ])->all();
       // print_r($CustomerList[0]->customer_id);
       // exit();
        $NewCustomer = new Customers();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'NewCustomer' => $NewCustomer,
            'CustomerList'=> $CustomerList,
            'staffList'=>$staffList,
        ]);
    }
    public function actionInactive()
    {
        $searchModel = new CustomersSearch();
        $business_id = Yii::$app->user->identity->business_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $query = Customers::find()->joinWith(['appointments']);
        $CustomerList = $query->select(['*'])
            ->andFilterWhere(['>', 'appointments.appointment_date',  date('Y-m-d', strtotime('-30 days')) ])
            ->andFilterWhere(['=', 'customers.business_id', $business_id ])
            ->groupBy(['customers.customer_id'])
            ->all();
        //   $CustomerList = Customers::find()->where(['business_id'=>Yii::$app->user->identity->business_id])
        //    ->andWhere(['<', 'createdat',  date('Y-m-d')-30 ])->all();
        // print_r($CustomerList[0]->customer_id);
        // exit();
        $staffList = Users::find()->where(['business_id'=>$business_id])->all();
        $NewCustomer = new Customers();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'NewCustomer' => $NewCustomer,
            'CustomerList'=> $CustomerList,
            'staffList'=>$staffList,
        ]);
    }

    public function actionHappy()
    {
        $searchModel = new CustomersSearch();
        $business_id = Yii::$app->user->identity->business_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $query = Customers::find()->joinWith(['appointments']);
        $CustomerList = $query->select(['*'])
            ->andFilterWhere(['<', 'appointments.appointment_date',  date('Y-m-d', strtotime('-30 days')) ])
            ->andFilterWhere(['=', 'customers.business_id', $business_id])
            ->groupBy(['customers.customer_id'])
            ->all();
        $staffList = Users::find()->where(['business_id'=>$business_id])->all();
        //   $CustomerList = Customers::find()->where(['business_id'=>Yii::$app->user->identity->business_id])
        //    ->andWhere(['<', 'createdat',  date('Y-m-d')-30 ])->all();
        // print_r($CustomerList[0]->customer_id);
        // exit();
        $NewCustomer = new Customers();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'NewCustomer' => $NewCustomer,
            'CustomerList'=> $CustomerList,
            'staffList'=>$staffList,
        ]);
    }
    public function actionUnhappy()
    {
        $searchModel = new CustomersSearch();
        $business_id = Yii::$app->user->identity->business_id;
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $query = Customers::find()->joinWith(['appointments']);
        $CustomerList = $query->select(['*'])
            ->andFilterWhere(['>', 'appointments.appointment_date',  date('Y-m-d', strtotime('-30 days')) ])
            ->andFilterWhere(['=', 'customers.business_id', $business_id ])
            ->groupBy(['customers.customer_id'])
            ->all();
        //   $CustomerList = Customers::find()->where(['business_id'=>Yii::$app->user->identity->business_id])
        //    ->andWhere(['<', 'createdat',  date('Y-m-d')-30 ])->all();
        // print_r($CustomerList[0]->customer_id);
        // exit();
        $staffList = Users::find()->where(['business_id'=>$business_id])->all();
        $NewCustomer = new Customers();

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'NewCustomer' => $NewCustomer,
            'CustomerList'=> $CustomerList,
            'staffList'=>$staffList,
        ]);
    }

    /**
     * Displays a single Customers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        $this->layout = 'noheader';
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Customers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Customers();
        $model->business_id = Yii::$app->user->identity->business_id;
        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            //return $this->redirect(['view', 'id' => $model->customer_id]);
            return 1;
        }
    }
    public function actionAdd()
    {
        $business_information = BusinessInformation::findOne(Yii::$app->user->identity->business_id);
        $model = new Customers();
        $model->business_id = Yii::$app->user->identity->business_id;
        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            //return $this->redirect(['view', 'id' => $model->customer_id]);
            return 1;
        }

        return $this->render('_form2', [
            'model' => $model,
            'business_information'=>$business_information
        ]);
    }

    /**
     * Updates an existing Customers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->customer_id]);
        } else {
            return $this->renderAjax('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Customers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    public function actionCustomerdetails()
    {
      $customer_id  = $_POST['csr_id'];
        $appointments_payments =  AppointmentsPayments::find()->where(['customer_id'=>$customer_id])->all();
        $new_appointments_payments =   new AppointmentsPayments();

        $query = Appointments::find()->joinWith(['services','users','appointments_payments']);

      //  $items = $query->select(['*'])->where(['appointments.appointment_id'=>$id])->one();
      $model = Customers::find()->where(['customer_id'=>$_POST['csr_id'],'business_id'=>Yii::$app->user->identity->business_id])->all();
      $modelban = Customers::findOne(['customer_id'=>$_POST['csr_id'],'business_id'=>Yii::$app->user->identity->business_id]);

        $past_appointments = $query->select(['*'])
            ->andFilterWhere([
          'and',
          ['=', 'appointments.appointment_date', new Expression('NOW()')],
          ['<', 'appointments.appointment_start_time', new Expression('NOW()')],
          ['appointments.customer_id'=>$customer_id],
      ]) ->orFilterWhere([
          'and',
          ['<', 'appointments.appointment_date', date('Y-m-d')],
                ['appointments.customer_id'=>$customer_id],
      ])
          ->all();
        $upcoming_appointments = Appointments::find()
        ->andFilterWhere([
            'and',
            ['=', 'appointment_date', date('Y-m-d')],
            ['>', 'appointment_start_time', new Expression('NOW()')],
            ['appointments.customer_id'=>$customer_id],
        ])->orFilterWhere([
                'and',
                ['>', 'appointment_date', date('Y-m-d')],
                ['appointments.customer_id'=>$customer_id],
            ])
            ->all();

        return $this->renderAjax('customer_details',[
            'model'=>$model,
            'past_appointments'=>$past_appointments,
            'upcoming_appointments'=>$upcoming_appointments,
            'appointments_payments'=>$appointments_payments,
            'new_appointments_payments'=>$new_appointments_payments,
            'modelban'=>$modelban,
        ]);
    }

    /**
     * Finds the Customers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Customers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionSearchcustomer(){
        $search = $_POST['searchtxt'];
        if($search=='All'){
            $results = Customers::find()->andFilterWhere(['=', 'business_id', Yii::$app->user->identity->business_id])->all();

        }else {
            $results = Customers::find()->andFilterWhere(['like', 'first_name', $search . '%', false])
                ->andFilterWhere(['=', 'business_id', Yii::$app->user->identity->business_id])->all();
        }
        return $this->renderAjax('customer_search_results',[
            'CustomerList'=>$results,
        ]);
       
      //  $searchtxt =

    }
    public function actionVerify_customer(){
        $customer_id = $_POST['cstr_id'];
        $model = Customers::findOne(['business_id'=>Yii::$app->user->identity->business_id,'customer_id'=>$customer_id]);

        $model->verify_status = "1";
        if($model->save(false)){
            return 1;

        }

    }
    public function actionDelete_customer(){
        $customer_id = $_POST['cstr_id'];
        $model = Customers::findOne(['business_id'=>Yii::$app->user->identity->business_id,'customer_id'=>$customer_id]);
        if($model->delete()){
            return 1;
        }

    }
    public function actionAddmembershippayment(){
        $model = new AppointmentsPayments();

        $customer_id = $_POST['customer_id'];
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            $model->customer_id = $customer_id;
            if ($model->save(false)) {
                return 1;
            }
        }
    }
    public function actionUpdateban()
    {
        $customer_id =  $_POST['Customers']['customer_id'];
        $model = Customers::findOne(['business_id' => Yii::$app->user->identity->business_id, 'customer_id' => $customer_id]);

        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            return 1;
        }
    }
    public function actionUpdateinformation()
    {
        $customer_id =  $_POST['cstr_id'];
        $model = Customers::findOne(['business_id' => Yii::$app->user->identity->business_id, 'customer_id' => $customer_id]);
            $model->customer_information = $_POST['cstr_inf'];
        if ($model->save(false)) {
            return 1;
        }
    }



    public function actionNewcount()
    {
        $searchModel = new CustomersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $CustomerList = Customers::find()->where(['business_id'=> Yii::$app->user->identity->business_id])
            ->andWhere(['between', 'createdat',  date('Y-m-d')-30,  date('Y-m-d') ])->all();

        return count($CustomerList);
    }
    public function actionSendcustomeremail()
    {
       $sentemails =  new SentEmails();
        $to = $_POST['to'];
        $subject = $_POST['subject'];
        $message = $_POST['message'];

        if(Yii::$app->mailer->compose()
            ->setFrom('from@domain.com')
            ->setTo('sikandar.maqbool@tabsusa.com')
            ->setSubject($subject)->setHtmlBody($message)
            ->send()){
            $sentemails->subject = $subject;
            $sentemails->content = $message;
            $sentemails->to_email = $to;
            if($sentemails->save(false)) {
                return 1;
            }
        }

        print_r($_POST);
    }
    public function actionActivecount()
    {

        $query = Customers::find()->joinWith(['appointments']);
        $CustomerList = $query->select(['*'])
            ->andFilterWhere(['<', 'appointments.appointment_date',  date('Y-m-d')-30 ])
            ->andFilterWhere(['=', 'customers.business_id', Yii::$app->user->identity->business_id ])
            ->groupBy(['customers.customer_id'])->all();

        //   $CustomerList = Customers::find()->where(['business_id'=>Yii::$app->user->identity->business_id])
        //    ->andWhere(['<', 'createdat',  date('Y-m-d')-30 ])->all();
        // print_r($CustomerList[0]->customer_id);
        // exit();


        return count($CustomerList);
    }
    public function actionInactivecount()
    {

      //  $query = Customers::find()->joinWith(['appointments']);
        $query = Customers::find()->joinWith(['appointments']);
        $CustomerList = $query->select(['*'])
            ->andFilterWhere(['>', 'appointments.appointment_date',  date('Y-m-d')-30 ])
            ->andFilterWhere(['=', 'customers.business_id', Yii::$app->user->identity->business_id ])
            ->groupBy(['customers.customer_id'])->all();

            //->count();
        //   $CustomerList = Customers::find()->where(['business_id'=>Yii::$app->user->identity->business_id])
        //    ->andWhere(['<', 'createdat',  date('Y-m-d')-30 ])->all();
        // print_r($CustomerList[0]->customer_id);
        // exit();
        return count($CustomerList);
    }






    public function actionAddtag(){
        $customer_id = $_POST['cust_d'];
        $customer_tag = $_POST['tag'];
         $customers = Customers::find()->where(['customer_id'=>$customer_id,'business_id'=>Yii::$app->user->identity->business_id])->one();
        $customers->customer_tags = $customer_tag;
        if($customers->save(false)){
            return 1;
        }else{
            return 2;
        }
            


    }

    public function actionCustomer_sort($ids){
     //   $list = explode(',' , $ids);
    //    $i = 1 ;
        $customer_id= explode(',' , $ids);

        $ids_array = explode(',' , $ids);
        sort($ids_array);
        $count=count($ids_array);

        for ($i = 0; $i < $count; $i++)
        {
          //  echo '   this  '.$ids_array[$i];
           $model = Customers::findOne($customer_id[$i]);
           $model->sort_order = $ids_array[$i];
            $model->save(false);
           // echo '"'.$ids_array[$i].'     '.$idsss[$i].'"';

        }

    }
    public function actionUploadcustomer_image($id)
    {
        if (($_FILES['profile_image']['type'] == 'image/jpeg') || ($_FILES['profile_image']['type'] == 'image/png')) {

            $uploaddir = 'uploads/';
            $random_digit = rand(000000, 999999);
            $uploadfile = $uploaddir . $random_digit . basename($_FILES['profile_image']['name']);


            if (move_uploaded_file($_FILES['profile_image']['tmp_name'], $uploadfile)) {
                $customer = Customers::findOne(['customer_id' => $id, 'business_id' => Yii::$app->user->identity->business_id]);
                $customer->profile_image = $uploadfile;
                if ($customer->save(false)) {
                    //    if (Yii::$app->db->createCommand()->update('customers', ['service_image' => $uploadfile], 'service_id = ' . $id)->execute()) {

                    echo $uploadfile;
                    //    }
                } else {
                    echo "Possible file upload attack!\n";
                }


            } else {

                return 0;
            }

        }
    }
    public function actionGift_certificates_for_customer(){
        $business_id = Yii::$app->user->identity->business_id;
        $gift_certificates = GiftCertificates::find()->where(['business_id'=>$business_id])->all();
        return $this->renderAjax('gift_certificates_for_customer',[
            'gift_certificates'=>$gift_certificates,
            'type'=>'list'
        ]);


    }

    public function actionSell_certificate($id){
        $business_id = Yii::$app->user->identity->business_id;
        $new_gift_certificate = GiftCertificates::findOne(['business_id' => $business_id, 'gc_id' => $id]);


        $model = new SoldGiftCertificates();
        return $this->renderAjax('gift_certificates_for_customer', [
                'model' => $model,
                'new_gift_certificate'=>$new_gift_certificate,
                'type'=>'sell',
      ]
        );
    }

    protected function findModel($id)
    {
        if (($model = Customers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


}
/*->select(['COUNT(timeslot_id) AS cnt'])
    ->where([
        'test_date' => $today_date,
        'confirmed' => '1',
        //'third_user_id' => '0'
    ])
    ->andFilterWhere([
        'and',
        ['!=', 'status', 'Checked-in'],
        ['!=', 'status', 'Waiting for check in'],
        ['!=', 'status', 'Authenticated'],
        //['!=', 'status', 'Assigned to toughbook'],
    ])
    ->one()->cnt;
*/