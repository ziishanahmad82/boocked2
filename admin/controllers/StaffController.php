<?php

namespace app\controllers;


use Yii;
use app\models\Services;
use app\models\ServicesSearch;
use app\models\StaffCommonName;
use app\models\UploadForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;
use app\models\Users;

/**
 * ServicesController implements the CRUD actions for Services model.
 */
class StaffController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Services models.
     * @return mixed
     */
    public function actionIndex()
    {
        $business_id = Yii::$app->user->identity->business_id;
        $searchModel = new ServicesSearch();
        $staffCommonNameModel = new StaffCommonName();
        $staffCommonNameData = $staffCommonNameModel->commonRelatedNames();
        $userCreateModel = new Users();
        $staffList = Users::find()->where(['business_id'=> $business_id,'staff_status'=>1])->all();
        $inactivestaffList = Users::find()->where(['business_id'=> $business_id,'staff_status'=>0])->all();
        $services_list = Services::find()->where(['business_id'=> $business_id,'service_status'=>'active'])->all();


        $model = StaffCommonName::findOne(['business_id'=> $business_id]);


        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'staffCommonNameModel' => $staffCommonNameModel,
            'staffCommonNameData' => $staffCommonNameData,
            'userCreateModel' =>  $userCreateModel,
            'staffList' => $staffList,
            'inactivestaffList'=>$inactivestaffList,
            'services_list'=> $services_list
        ]);
    }

    /**
     * Displays a single Services model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Services model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Services();

        if ($model->load(Yii::$app->request->post()) ) {
            $model->business_id=Yii::$app->user->id;
            $model->save();
            return $this->redirect(['index', 'id' => $model->service_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Services model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->service_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Services model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    public function actionCommonNames($id)
    {


    }
    public function actionValidate()
    {

        $model = new StaffCommonName();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;

        }
        return ActiveForm::validate($model);
    }
    public function actionValidatecreatestaff()
    {

        $model = new Users();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;
          // if() model->save(false)

        }
        return ActiveForm::validate($model);
    }

    public function actionActivate_status(){
        $business_id = Yii::$app->user->identity->business_id;

        if($_POST['activate']=='inactive'){
        $new_staff = Users::findOne(['id'=>$_POST['staff'],'business_id'=>$business_id]);
        $new_staff->staff_status='0';
            if($new_staff->save(false)){
                return 1;

            }

        }else  if($_POST['activate']=='active'){
            $new_staff = Users::findOne(['id'=>$_POST['staff'],'business_id'=>$business_id]);
            $new_staff->staff_status='1';
            if($new_staff->save(false)){
                return 1;

            }

        }

    }
    public function actionSavecommon()
    {
       // $model = new ServicesCommonName();
        $business_id = Yii::$app->user->identity->business_id;
        $staffCommonNameModel = new StaffCommonName();
        $model = StaffCommonName::findOne(['business_id'=> $business_id]);
       // $model = $this->findModel($id);
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
        //    \Yii::$app->response->format = Response::FORMAT_JSON;
          //  return ['success' => $model->save()];
            if($model->save()){
                Yii::$app->session->set('bk_singular_staff', $model->singular_name);
                Yii::$app->session->set('bk_plural_staffs', $model->plural_name);
                return 1;
            }
        }
        return $this->renderAjax('index', [
            'model' => $model,
        ]);
    }
    public function actionStaff_user_create(){
        $model = new Users();
        $request = \Yii::$app->getRequest();
        $model->business_id = Yii::$app->user->identity->business_id;
        if ($request->isPost && $model->load($request->post())) {
            if ($model->save(false)){

                return 1;
            }else{

                return 2;
            }
            }
            }

public function actionUploaduserprofile($id)
{
    if (($_FILES['profile_image']['type'] == 'image/jpeg') || ($_FILES['profile_image']['type'] == 'image/png')) {

    $uploaddir = 'uploads/';
    $random_digit = rand(0000, 9999);
    $uploadfile = $uploaddir . $random_digit . basename($_FILES['profile_image']['name']);

    if (move_uploaded_file($_FILES['profile_image']['tmp_name'], $uploadfile)) {
        if (Yii::$app->db->createCommand()->update('user', ['user_profile_image' => $uploadfile], 'id = ' . $id)->execute()) {

            echo $uploadfile;
        }
    } else {
        echo "Possible file upload attack!\n";
    }


    }else {

        return 0;
    }


}

//}

    /**
     * Finds the Services model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Services the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionGet_service_name($service_id){
        $model = Services::findOne($service_id);
        return $model->service_name;
    }
    protected function findModel($id)
    {
        if (($model = Services::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
