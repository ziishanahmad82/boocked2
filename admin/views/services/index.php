<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use app\models\ProfessionsList;
use yii\helpers\Url;
use app\models\Services;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ServicesSearch */
/* @var $servicesCommonNameData app\models\ServicesCommonName */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\widgets\Pjax;

$this->title = 'Services';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php // print_r($services_categories); ?>
<?php
 if(Yii::$app->session->get('bk_singular_service')!=""){   $singular_service_name = Yii::$app->session->get('bk_singular_service'); }else { $singular_service_name = 'service'; }
if(Yii::$app->session->get('bk_plural_service')!=""){   $plural_service_name = Yii::$app->session->get('bk_plural_service'); }else { $plural_service_name = 'services'; }

?>
<div class="col-lg-3 col-md-3 col-sm-3 no-padding-l media-xs-full">
    <div class="panel panel-default">
        <div class="panel-heading full">
            <ul class="list-inline list-staff">
                <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web') ?>/img/settings.png"> <span class="staff-head">Manage Services</span></h5></li>
            </ul>
        </div>
        <div class="panel-body panel-body-nav-tabs-sidebar" >
            <div class="row">

            </div>
            <div class="panel panel-default" id="updatecreateform" style="margin-top: 16px;">
                <div class="panel-heading full">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><i class="fa fa-plus" style="color: #FC7600; position: relative; top: 3px;"></i><span class="staff-head">Add <?= $plural_service_name ?></span></h5></li>
                    </ul>
                </div>
                <div class="panel-body panel-body-nav-tabs-sidebar">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 top">
                            <p>Category</p>
                            <div class="form-group">
                                <select class="form-control">
                                    <option selected>Default Category</option>
                                    <option>1</option>
                                    <option>2</option>
                                </select>
                            </div>
                            <small><a href="" style="color: #FC7600 !important;text-decoration: underline !important;">New Category | Delete Category</a></small>

                        </div>

                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 top">
                            <p>Design</p>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Design">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <p>Description</p>
                            <div class="form-group">
                                <textarea class="form-control group" cols="30"  placeholder="Description"></textarea>

                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <p>Duration</p>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Text here...">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <p>Price</p>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Rs....">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <p>Capacity</p>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="1">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-warning ad1">Add <?= $singular_service_name ?></button></li>
                            <li><button type="button" class="btn btn-warning ad1">Cancel</button></li>
                        </ul>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


<div class="col-lg-9 col-md-9 col-sm-9 media-xs-full">
    <div class="panel panel-default">



        <div class="panel-body">
            <div class="row">
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="service">
                        <div class="panel-collapse collapse in active">
                            <div class="panel-body" style="border: none;">
                                <div class="panel panel-default">
                                    <div class="panel-heading full">
                                        <ul class="list-inline list-staff">
                                            <li><h5 class="panel-title"><i class="fa fa-file" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">General name for services</span></h5></li>
                                            <li class="pull-right li-right hidden-xs">General name for service</li>
                                        </ul>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p>Common name for service (or first resource)</p>
                                                <p style="margin-top: -11px;"><small class="text-muted">Example service, Message, Tour etc</small></p>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 20px;">
                                            <div class="col-lg-10 col-lg-offset-1">
                                                <div class="row">
                                                    <?php Pjax::begin(); ?>
                                                    <?php  $form = ActiveForm::begin(['id'=>'sevicecommonform', 'action' => 'savecommon',
                                                        'enableAjaxValidation' => true, 'validationUrl' => 'validate',
                                                        'fieldConfig' => ['options' => ['class' => 'col-lg-6 col-md-6 col-sm-6']],]); ?>

                                                    <?= $form->field($servicesCommonNameData[0], 'singular_name',[ 'labelOptions' => [ 'class' => 'small text-muted']])->textInput(['maxlength' => true]) ?>

                                                    <?= $form->field($servicesCommonNameData[0], 'plural_name',[ 'labelOptions' => [ 'class' => 'small text-muted']])->textInput(['maxlength' => true]) ?>
                                                    <?php ActiveForm::end();
                                                    Pjax::end(); ?>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 field-servicescommonname-singular_name required" ><span class="savedMess" id="singularsaved" style="display:none;">saved</span></div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <div class="col-sm-12">
                                                        <p><small class="text-muted">This will be shown t your clients at various places while booking. Sample sentences: "Please select (service)", Number of (services)."</small></p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                </div>
                            </div>
                        <?php Pjax::begin(['id' => 'srvicereloadsec']); ?>



                        <?php    $total_services =  Services::find()->where(['business_id'=>Yii::$app->user->identity->business_id])->all();
                            $inactive_services =  Services::find()->where(['business_id'=>Yii::$app->user->identity->business_id,'service_status'=>'inactqwive'])->all();
                        ?>

                             <div class="panel-body" style="border:none">
                                <div class="panel panel-default">
                                    <div class="panel-heading full">
                                        <ul class="list-inline list-staff">
                                            <li><h5 class="panel-title text-uppercase"><i class="fa fa-clock-o" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head"><?= $plural_service_name ?></span></h5></li>

                                            <li class="pull-right li-right hidden-xs">Inactive: <?= count($inactive_services) ?></li>
                                            <li class="pull-right li-right hidden-xs">Total <?= $plural_service_name ?>: <?= count($total_services) ?></li>
                                        </ul>
                                    </div>







                                   <?php  $services_cat_id = 0; ?>


                                    <div class="panel-body">

                                    <div class="panel-body" style="border:none">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p id="editcatname<?= $services_cat_id ?>"><span>Default</span></p>
                                                <p class="col-sm-4" id="editcatid<?= $services_cat_id ?>" style="display: none;"><input type="text" value="Default" class="form-control" /><small><a href="#" class="servicecatSave" onclick="servicecatSave(event,this)"  data-servicecat="<?= $services_cat_id ?>" style="color: #FC7600 !important;">Save</a></small> / <small><a href="#editcatname<?= $services_cat_id ?>" class="showinputname" onclick="showinputname(event,this)" style="color: #FC7600 !important;">Cancel</a></small></p>
                                                <div class="clearfix"></div>
                                                <p style="margin-top: -11px;"><small class="text-muted">Example service, Message, Tour etc</small></p>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 20px;">
                                            <div class="col-lg-12">
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a href="#active<?= $services_cat_id ?>" data-toggle="tab">Active</a></li>
                                                    <li><a href="#inactive<?= $services_cat_id ?>" data-toggle="tab" style="border-top-right-radius: 4px;">Inactive</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane fade in active" id="active<?= $services_cat_id ?>">
                                                        <div class="panel-collapse collapse in active">
                                                            <div class="panel-body panel-table">
                                                                <table class="table table-responsive table-striped">
                                                                    <tbody>

                                                                    <?php  $Activeservices = $this->context->getCategory_services($services_cat_id,'active');
                                                                    //print_r($servicedat);
                                                                    if(!empty($Activeservices)){
                                                                    foreach($Activeservices as $Activeservice ){
                                                                    ?>
                                                                    <tr>
                                                                        <td>
                                                                            <ul class="list-inline list-action">
                                                                                <li style="position:relative">
                                                                                    <?php if($Activeservice['service_image']){ ?>
                                                                                    <img src="<?= Yii::getAlias('@web').'/'.$Activeservice['service_image'] ?>" style="width:58px;height: 57px">
                                                                                    <?php }else { ?>
                                                                                    <img src="<?= Yii::getAlias('@web') ?>/img/user.png" style="width:58px;height: 57px">
                                                                                    <?php } ?>
                                                                                    <form enctype="multipart/form-data"   method="post" class="staffimageform" action="uploadservice_image?id=<?= $Activeservice['service_id'] ?>" id="staffimageform<?= $Activeservice['service_id'] ?>" >
                                                                                        <input type="hidden" id="resource_csrf" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />

                                                                                        <input type="file" name="profile_image" style="display: none;" class="staffimagelog">
                                                                                        <a href="#" onclick="changeProfile(event,this)" class="changeProfile">Change</a>
                                                                                    </form>
                                                                                </li>
                                                                                <li>
                                                                                    <ul class="list-unstyled list-name">
                                                                                        <li><?= $Activeservice['service_name'] ?></li>
                                                                                        <li><small><span class="text-muted">Time Slot : </span><span class="txt-theme"><?= $Activeservice['service_duration'] ?> Min</span></small></li>
                                                                                    </ul>
                                                                                </li>
                                                                                <div class="pull-right div-desc">
                                                                                    <ul class="list-inline">
                                                                                        <li><?= $Activeservice['service_duration'] ?><small class="text-muted">mins</small></li>
                                                                                        <li><span class="text-muted" style="font-size:12px"> <?= Yii::$app->session->get('bk_currency') ?> </span><?= $Activeservice['service_price'] ?></li>
                                                                                        <li><span class="text-muted">Capacity : </span><small><?= $Activeservice['service_capacity'] ?></small></li>
                                                                                        <li><a href="<?= Yii::getAlias('@web') ?>/index.php/services/update?id=<?= $Activeservice['service_id'] ?>" onclick="textthemeeditservice(event,this)" class="txt-theme">Edit</a></li>
                                                                                        <li class="dropdown"><a href="#" onclick="event.preventDefault();" class="dropdown-toggle" data-target="#demo<?= $Activeservice['service_id'] ?>" data-toggle="collapse"><i class="fa fa-chevron-circle-down"></i></a>
                                                                                            <!-- <ul class="dropdown-menu drop">
                                                                                                 <li><a href="#">Activate</a></li>
                                                                                                 <li><a href="#">Deactivate</a></li>
                                                                                             </ul> -->
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </ul>
                                                                        </td>
                                                                    </tr>

                                                                    <tr id="demo<?= $Activeservice['service_id'] ?>" class="collapse">
                                                                        <td>
                                                                            <ul class="nav nav-tabs">
                                                                                <li class="active"><a data-toggle="tab" href="#home<?= $Activeservice['service_id'] ?>">Who & When</a></li>
                                                                                <li><a data-toggle="tab" href="#men<?= $Activeservice['service_id'] ?>">Status</a></li>
                                                                                <li><a data-toggle="tab" href="#menu2<?= $Activeservice['service_id'] ?>">Resources</a></li>
                                                                                <li><a data-toggle="tab" href="#menu3<?= $Activeservice['service_id'] ?>">Featured</a></li>
                                                                                <li><a data-toggle="tab" href="#menu4<?= $Activeservice['service_id'] ?>">Regular Packages</a></li>

                                                                            </ul>

                                                                            <div class="tab-content">
                                                                                <div id="home<?= $Activeservice['service_id'] ?>" class="tab-pane fade in active">
                                                                                    <div id="staffserviceform<?= $Activeservice['service_id'] ?>">

                                                                                    </div>
                                                                                    <br/>
                                                                                    <a href="addworkinghours?service_id=<?= $Activeservice['service_id'] ?>" class="addupdate_allworking"> Add or Update working hours for this service.</a>
                                                                                    <p><?php foreach($staffList as $staff_name){
                                                                                        ?> <a href="<?= Yii::getAlias('@web') ?>/index.php/services/updatestaff_formslots?service_id=<?= $Activeservice['service_id'] ?>&staff_id=<?php echo $staff_name->id; ?>" onclick="staff_serviceslots(event,this)" class="btn btn-info staff_serviceslots"> <?php echo $staff_name->username; ?></a> <?php


                                                                                        }?></p>
                                                                                </div>
                                                                                <div id="men<?= $Activeservice['service_id'] ?>"" class="tab-pane fade">

                                                                                <p><br/>
                                                                                    <a href="<?= Yii::getAlias('@web') ?>/index.php/services/disableservice?service_id=<?= $Activeservice['service_id'] ?>&status=inactive" onclick="EnableDisableService(event,this)" class="diableenableservice btn btn-danger">Disable</a> </p>
                                                                            </div>
                                                                            <div id="menu2<?= $Activeservice['service_id'] ?>" class="tab-pane fade">
                                                                                <h5>Resources that <?= $Activeservice['service_name'] ?> can use</h5> <a href="#">Update</a>
                                                                                <form action="<?= Yii::getAlias('@web') ?>/index.php/services/updatelinkedresource?id=<?= $Activeservice['service_id'] ?>" action="post" id="resourceform<?= $Activeservice['service_id'] ?>">
                                                                                    <p><input type="radio" value="yes" name="linked_status" <?php if($Activeservice['linked_status']=="yes"){echo "checked";} ?>> All Resources</p>
                                                                                    <p><input type="radio" value="no" name="linked_status" <?php if($Activeservice['linked_status']=="no"){echo "checked";} ?>> No Resources</p>
                                                                                    <a href="#" class="btn btn-info" onclick="updatelinkedresource(event,this);">Update</a>
                                                                                    <div class="alert alert-success sucalert" style="display: none;">
                                                                                        <strong>Updated !</strong>
                                                                                    </div>
                                                                                </form>
                                                                            </div>
                                                                            <div id="menu3<?= $Activeservice['service_id'] ?>" class="tab-pane fade">

                                                                            <br/>
                                                                                <?php
                                                                                if ($Activeservice['featured']==0){
                                                                                    ?>
                                                                                <a href="<?= Yii::getAlias('@web') ?>/index.php/services/enablefeatured?service_id=<?= $Activeservice['service_id'] ?>&status=active" onclick="EnableDisableService(event,this)" class="diableenableservice btn btn-success">Enable</a>
                                                                                <?php
                                                                                }else if ($Activeservice['featured']==1){
                                                                                    ?>
                                                                                    <a href="<?= Yii::getAlias('@web') ?>/index.php/services/disablefeatured?service_id=<?= $Activeservice['service_id'] ?>&status=inactive" onclick="EnableDisableService(event,this)" class="diableenableservice btn btn-danger">Disable</a>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                                  </div>
                                                                            <div id="menu4<?= $Activeservice['service_id'] ?>" class="tab-pane fade">

                                                                                <br/>
                                                                                <?php
                                                                                if ($Activeservice['pkgs']==0){
                                                                                    ?>
                                                                                    <a href="<?= Yii::getAlias('@web') ?>/index.php/services/enablepkgs?service_id=<?= $Activeservice['service_id'] ?>&status=active" onclick="EnableDisableService(event,this)" class="diableenableservice btn btn-success">Enable</a>
                                                                                    <?php
                                                                                }else if ($Activeservice['pkgs']==1){
                                                                                    ?>
                                                                                    <a href="<?= Yii::getAlias('@web') ?>/index.php/services/disablepkgs?service_id=<?= $Activeservice['service_id'] ?>&status=inactive" onclick="EnableDisableService(event,this)" class="diableenableservice btn btn-danger">Disable</a>
                                                                                    <?php
                                                                                }
                                                                                ?>
                                                                            </div>
                                                            </div>
                                                            </td>
                                                            </tr>

                                                            <?php } ?>
                                                                    <?php }else { ?>
                                                                        <tr>
                                                                            <td>
                                                                                <ul class="list-inline list-action">
                                                                                    <li>
                                                                                        No Post to show
                                                                                    </li>

                                                                                </ul>
                                                                            </td>
                                                                        </tr>
                                                            <?php } ?>

                                                            </tbody>
                                                            </table>
                                                        </div>
                                                    </div>

</div>
                                                <div class="tab-pane fade" id="inactive<?= $services_cat_id ?>">
                                                    <div class="panel-collapse collapse in active">
                                                        <div class="panel-body panel-table">
                                                            <table class="table table-responsive table-striped">
                                                                <tbody>
                                                                <?php  $inActiveservices = $this->context->getCategory_services($services_cat_id,'inactive');
                                                                //print_r($servicedat);
                                                                if(!empty($inActiveservices)){
                                                                foreach ($inActiveservices as $inActiveservice){
                                                                ?>
                                                                <tr>
                                                                    <td>
                                                                        <ul class="list-inline list-action">
                                                                            <li style="position:relative">
                                                                                <?php if ($inActiveservice['service_image']) { ?>
                                                                                    <img
                                                                                        src="<?= Yii::getAlias('@web') . '/' . $inActiveservice['service_image'] ?>"
                                                                                        style="width:58px;height: 57px">
                                                                                <?php } else { ?>
                                                                                    <img
                                                                                        src="<?= Yii::getAlias('@web') ?>/img/user.png"
                                                                                        style="width:58px;height: 57px">
                                                                                <?php } ?>
                                                                                <form enctype="multipart/form-data"
                                                                                      method="post"
                                                                                      class="staffimageform"
                                                                                      action="<?= Yii::getAlias('@web') ?>/index.php/services/uploadservice_image?id=<?= $inActiveservice['service_id'] ?>"
                                                                                      id="staffimageform<?= $inActiveservice['service_id'] ?>">
                                                                                    <input type="hidden"
                                                                                           id="resource_csrf"
                                                                                           name="<?= Yii::$app->request->csrfParam; ?>"
                                                                                           value="<?= Yii::$app->request->csrfToken; ?>"/>

                                                                                    <input type="file"
                                                                                           name="profile_image"
                                                                                           style="display: none;"
                                                                                           class="staffimagelog">
                                                                                    <a href="#"
                                                                                       onclick="changeProfile(event,this)"
                                                                                       class="changeProfile">Change</a>
                                                                                </form>
                                                                            </li>
                                                                            <li>
                                                                                <ul class="list-unstyled list-name">
                                                                                    <li><?= $inActiveservice['service_name'] ?></li>
                                                                                    <li>
                                                                                        <small><span class="text-muted">Time Slot : </span><span
                                                                                                class="txt-theme"><?= $inActiveservice['service_duration'] ?>
                                                                                                Min</span></small>
                                                                                    </li>
                                                                                </ul>
                                                                            </li>
                                                                            <div class="pull-right div-desc">
                                                                                <ul class="list-inline">
                                                                                    <li><?= $inActiveservice['service_duration'] ?>
                                                                                        <small class="text-muted">mins
                                                                                        </small>
                                                                                    </li>
                                                                                    <li><span
                                                                                            class="text-muted" style="font-size:12px;"><?= Yii::$app->session->get('bk_currency') ?> </span><?= $inActiveservice['service_price'] ?>
                                                                                    </li>
                                                                                    <li><span class="text-muted">Capacity : </span>
                                                                                        <small><?= $inActiveservice['service_capacity'] ?></small>
                                                                                    </li>
                                                                                    <li>
                                                                                        <a href="<?= Yii::getAlias('@web') ?>/index.php/services/update?id=<?= $inActiveservice['service_id'] ?>"
                                                                                           onclick="textthemeeditservice(event,this)"
                                                                                           class="txt-theme">Edit</a>
                                                                                    </li>
                                                                                    <li class="dropdown"><a href="#"
                                                                                                            onclick="event.preventDefault();"
                                                                                                            class="dropdown-toggle"
                                                                                                            data-target="#demo<?= $inActiveservice['service_id'] ?>"
                                                                                                            data-toggle="collapse"><i
                                                                                                class="fa fa-chevron-circle-down"></i></a>

                                                                                    </li>
                                                                                </ul>
                                                                            </div>
                                                                        </ul>
                                                                    </td>
                                                                </tr>
                                                                <tr id="demo<?= $inActiveservice['service_id'] ?>"
                                                                    class="collapse">
                                                                    <td>
                                                                        <ul class="nav nav-tabs">
                                                                            <li class="active"><a data-toggle="tab"
                                                                                                  href="#home<?= $inActiveservice['service_id'] ?>">Who
                                                                                    & When</a></li>
                                                                            <li><a data-toggle="tab"
                                                                                   href="#men<?= $inActiveservice['service_id'] ?>">Status</a>
                                                                            </li>
                                                                            <?php /*  <li><a data-toggle="tab" href="#menu2<?= $inActiveservice['service_id'] ?>">Menu 2</a></li> */ ?>
                                                                        </ul>

                                                                        <div class="tab-content">
                                                                            <div
                                                                                id="home<?= $inActiveservice['service_id'] ?>"
                                                                                class="tab-pane fade in active">
                                                                                <div
                                                                                    id="staffserviceform<?= $inActiveservice['service_id'] ?>">

                                                                                </div>
                                                                                <br/>
                                                                                <a href="<?= Yii::getAlias('@web') ?>/index.php/services/addworkinghours?service_id=<?= $inActiveservice['service_id'] ?>"
                                                                                   class="addupdate_allworking"> Add or
                                                                                    Update working hours for this
                                                                                    service.</a>
                                                                                <p><?php foreach ($staffList as $staff_name) {
                                                                                        ?> <a
                                                                                            href="<?= Yii::getAlias('@web') ?>/index.php/services/updatestaff_formslots?service_id=<?= $inActiveservice['service_id'] ?>&staff_id=<?php echo $staff_name->id; ?>"
                                                                                            onclick="staff_serviceslots(event,this)"
                                                                                            class="btn btn-info staff_serviceslots"> <?php echo $staff_name->username; ?></a> <?php


                                                                                    } ?></p>
                                                                            </div>
                                                                            <div
                                                                                id="men<?= $inActiveservice['service_id'] ?>"
                                                                            " class="tab-pane fade">

                                                                            <p><br/><a
                                                                                    href="<?= Yii::getAlias('@web') ?>/index.php/services/disableservice?service_id=<?= $inActiveservice['service_id'] ?>&status=active"
                                                                                    onclick="EnableDisableService(event,this)"
                                                                                    class="diableenableservice btn btn-success">Enable</a>
                                                                            </p>
                                                                        </div>

                                                        </div>
                                                        </td>
                                                        </tr>
                                                        <?php }
                                                        }else {?>
                                                        <tr>
                                                            <td>
                                                                <ul class="list-inline list-action">
                                                                    <li>
                                                                        No Post to show
                                                                    </li>

                                                                </ul>
                                                            </td>
                                                        </tr>
                                                        <? } ?>
                                                        </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                 </div>































                                    <?php foreach($services_categories as $services_category){
                                        $services_cat_id = $services_category['service_cat_id'];

                                        ?>
                                    <div class="panel-body" style="border:none">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p id="editcatname<?= $services_cat_id ?>"><span><?= $services_category['service_category_name'] ?></span> <small><a href="#editcatid<?= $services_cat_id ?>" onclick="showinputname(event,this)" class="showinputname" style="color: #FC7600 !important;">Edit</a></small></p>
                                                <p class="col-sm-4" id="editcatid<?= $services_cat_id ?>" style="display: none;"><input type="text" value="<?= $services_category['service_category_name'] ?>" class="form-control" /><small><a href="#" class="servicecatSave" onclick="servicecatSave(event,this)" data-servicecat="<?= $services_cat_id ?>" style="color: #FC7600 !important;">Save</a></small> / <small><a href="#editcatname<?= $services_cat_id ?>" class="showinputname" onclick="showinputname(event,this)" style="color: #FC7600 !important;">Cancel</a></small></p>
                                                <div class="clearfix"></div>
                                                <p style="margin-top: -11px;"><small class="text-muted">Example service, Message, Tour etc</small></p>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 20px;">
                                            <div class="col-lg-12">
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a href="#active<?= $services_cat_id ?>" data-toggle="tab">Active</a></li>
                                                    <li><a href="#inactive<?= $services_cat_id ?>" data-toggle="tab" style="border-top-right-radius: 4px;">Inactive</a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane fade in active" id="active<?= $services_cat_id ?>">
                                                        <div class="panel-collapse collapse in active">
                                                            <div class="panel-body panel-table">
                                                                <table class="table table-responsive table-striped">
                                                                    <tbody>

                                                                  <?php  $Activeservices = $this->context->getCategory_services($services_cat_id,'active');
                                                                  //print_r($servicedat);
                                                                  if(!empty($Activeservices)){
                                                                  foreach($Activeservices as $Activeservice ){
                                                                  ?>
                                                                    <tr>
                                                                        <td>
                                                                            <ul class="list-inline list-action">
                                                                                <li style="position:relative">
                                                                                    <?php if($Activeservice['service_image']){ ?>
                                                                                        <img src="<?= Yii::getAlias('@web').'/'.$Activeservice['service_image'] ?>" style="width:58px;height: 57px">
                                                                                    <?php }else { ?>
                                                                                        <img src="<?= Yii::getAlias('@web') ?>/img/user.png" style="width:58px;height: 57px">
                                                                                    <?php } ?>
                                                                                    <form enctype="multipart/form-data"   method="post" class="staffimageform" action="<?= Yii::getAlias('@web') ?>/index.php/services/uploadservice_image?id=<?= $Activeservice['service_id'] ?>" id="staffimageform<?= $Activeservice['service_id'] ?>" >
                                                                                        <input type="hidden" id="resource_csrf" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />

                                                                                        <input type="file" name="profile_image" style="display: none;" class="staffimagelog">
                                                                                        <a href="#" onclick="changeProfile(event,this)" class="changeProfile">Change</a>
                                                                                    </form>
                                                                                </li>
                                                                                <li>
                                                                                    <ul class="list-unstyled list-name">
                                                                                        <li><?= $Activeservice['service_name'] ?></li>
                                                                                        <li><small><span class="text-muted">Time Slot : </span><span class="txt-theme"><?= $Activeservice['service_duration'] ?> Min</span></small></li>
                                                                                    </ul>
                                                                                </li>
                                                                                <div class="pull-right div-desc">
                                                                                    <ul class="list-inline">
                                                                                        <li><?= $Activeservice['service_duration'] ?><small class="text-muted">mins</small></li>
                                                                                        <li><span class="text-muted" style="font-size:12px"><?= Yii::$app->session->get('bk_currency') ?> </span><?= $Activeservice['service_price'] ?></li>
                                                                                        <li><span class="text-muted">Capacity : </span><small><?= $Activeservice['service_capacity'] ?></small></li>
                                                                                        <li><a href="<?= Yii::getAlias('@web') ?>/index.php/services/update?id=<?= $Activeservice['service_id'] ?>" onclick="textthemeeditservice(event,this)" class="txt-theme">Edit</a></li>
                                                                                        <li class="dropdown"><a href="#" onclick="event.preventDefault();" class="dropdown-toggle" data-target="#demo<?= $Activeservice['service_id'] ?>" data-toggle="collapse"><i class="fa fa-chevron-circle-down"></i></a>
                                                                                           <!-- <ul class="dropdown-menu drop">
                                                                                                <li><a href="#">Activate</a></li>
                                                                                                <li><a href="#">Deactivate</a></li>
                                                                                            </ul> -->
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </ul>
                                                                        </td>
                                                                    </tr>

                                                                      <tr id="demo<?= $Activeservice['service_id'] ?>" class="collapse">
                                                                          <td>
                                                                              <ul class="nav nav-tabs">
                                                                                  <li class="active"><a data-toggle="tab" href="#home<?= $Activeservice['service_id'] ?>">Who & When</a></li>
                                                                                  <li><a data-toggle="tab" href="#men<?= $Activeservice['service_id'] ?>">Status</a></li>
                                                                                  <li><a data-toggle="tab" href="#menu2<?= $Activeservice['service_id'] ?>">Resources</a></li>
                                                                              </ul>

                                                                              <div class="tab-content">
                                                                                  <div id="home<?= $Activeservice['service_id'] ?>" class="tab-pane fade in active">
                                                                                      <div id="staffserviceform<?= $Activeservice['service_id'] ?>">

                                                                                      </div>
                                                                                      <br/>
                                                                                      <a href="addworkinghours?service_id=<?= $Activeservice['service_id'] ?>" class="addupdate_allworking"> Add or Update working hours for this service.</a>
                                                                                      <p><?php foreach($staffList as $staff_name){
                                                                                              ?> <a href="<?= Yii::getAlias('@web') ?>/index.php/services/updatestaff_formslots?service_id=<?= $Activeservice['service_id'] ?>&staff_id=<?php echo $staff_name->id; ?>" onclick="staff_serviceslots(event,this)" class="btn btn-info staff_serviceslots"> <?php echo $staff_name->username; ?></a> <?php


                                                                                          }?></p>
                                                                                  </div>
                                                                                  <div id="men<?= $Activeservice['service_id'] ?>"" class="tab-pane fade">

                                                                                      <p><br/>
                                                                                              <a href="<?= Yii::getAlias('@web') ?>/index.php/services/disableservice?service_id=<?= $Activeservice['service_id'] ?>&status=inactive" onclick="EnableDisableService(event,this)" class="diableenableservice btn btn-danger">Disable</a> </p>
                                                                                  </div>
                                                                              <div id="menu2<?= $Activeservice['service_id'] ?>" class="tab-pane fade">
                                                                                      <h5>Resources that <?= $Activeservice['service_name'] ?> can use</h5> <a href="#">Update</a>
                                                                                            <form action="<?= Yii::getAlias('@web') ?>/index.php/services/updatelinkedresource?id=<?= $Activeservice['service_id'] ?>" action="post" id="resourceform<?= $Activeservice['service_id'] ?>">
                                                                                      <p><input type="radio" value="yes" name="linked_status" <?php if($Activeservice['linked_status']=="yes"){echo "checked";} ?>> All Resources</p>
                                                                                     <p><input type="radio" value="no" name="linked_status" <?php if($Activeservice['linked_status']=="no"){echo "checked";} ?>> No Resources</p>
                                                                                                <a href="#" class="btn btn-info" onclick="updatelinkedresource(event,this);">Update</a>
                                                                                                <div class="alert alert-success sucalert" style="display: none;">
                                                                                                    <strong>Updated !</strong> 
                                                                                                </div>
                                                                                            </form>
                                                                                  </div>
                                                                              </div>
                                                                          </td>
                                                                      </tr>

                                                                  <?php } ?>
                                                              <?php  }else { ?>

                                                                  <tr>
                                                                      <td>
                                                                          <ul class="list-inline list-action">
                                                                              <li>
                                                                                  No Post to show
                                                                              </li>

                                                                          </ul>
                                                                      </td>
                                                                  </tr>
                                                            <?php } ?>

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="inactive<?= $services_cat_id ?>">
                                                        <div class="panel-collapse collapse in active">
                                                            <div class="panel-body panel-table">
                                                                <table class="table table-responsive table-striped">
                                                                    <tbody>
                                                                    <?php  $inActiveservices = $this->context->getCategory_services($services_cat_id,'inactive');
                                                                    //print_r($servicedat);
                                                                    if(!empty($inActiveservices)){
                                                                    foreach($inActiveservices as $inActiveservice ){
                                                                    ?>
                                                                    <tr>
                                                                        <td>
                                                                            <ul class="list-inline list-action">
                                                                                <li style="position:relative">
                                                                                    <?php if($inActiveservice['service_image']){ ?>
                                                                                        <img src="<?= Yii::getAlias('@web').'/'.$inActiveservice['service_image'] ?>" style="width:58px;height: 57px">
                                                                                    <?php }else { ?>
                                                                                        <img src="<?= Yii::getAlias('@web') ?>/img/user.png" style="width:58px;height: 57px">
                                                                                    <?php } ?>
                                                                                    <form enctype="multipart/form-data"   method="post" class="staffimageform" action="<?= Yii::getAlias('@web') ?>/index.php/services/uploadservice_image?id=<?= $inActiveservice['service_id'] ?>" id="staffimageform<?= $inActiveservice['service_id'] ?>" >
                                                                                        <input type="hidden" id="resource_csrf" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />

                                                                                        <input type="file" name="profile_image" style="display: none;" class="staffimagelog">
                                                                                        <a href="#" onclick="changeProfile(event,this)" class="changeProfile">Change</a>
                                                                                    </form>
                                                                                </li>
                                                                                <li>
                                                                                    <ul class="list-unstyled list-name">
                                                                                        <li><?= $inActiveservice['service_name'] ?></li>
                                                                                        <li><small><span class="text-muted">Time Slot : </span><span class="txt-theme"><?= $inActiveservice['service_duration'] ?> Min</span></small></li>
                                                                                    </ul>
                                                                                </li>
                                                                                <div class="pull-right div-desc">
                                                                                    <ul class="list-inline">
                                                                                        <li><?= $inActiveservice['service_duration'] ?><small class="text-muted">mins</small></li>
                                                                                        <li><span class="text-muted" style="font-size:12px;"><?= Yii::$app->session->get('bk_currency') ?> </span><?= $inActiveservice['service_price'] ?></li>
                                                                                        <li><span class="text-muted">Capacity : </span><small><?= $inActiveservice['service_capacity'] ?></small></li>
                                                                                        <li><a href="<?= Yii::getAlias('@web') ?>/index.php/services/update?id=<?= $inActiveservice['service_id'] ?>" onclick="textthemeeditservice(event,this)" class="txt-theme">Edit</a></li>
                                                                                        <li class="dropdown"><a href="#" onclick="event.preventDefault();" class="dropdown-toggle" data-target="#demo<?= $inActiveservice['service_id'] ?>" data-toggle="collapse"><i class="fa fa-chevron-circle-down"></i></a>

                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </ul>
                                                                        </td>
                                                                    </tr>
                                                                    <tr id="demo<?= $inActiveservice['service_id'] ?>" class="collapse">
                                                                        <td>
                                                                            <ul class="nav nav-tabs">
                                                                                <li class="active"><a data-toggle="tab" href="#home<?= $inActiveservice['service_id'] ?>">Who & When</a></li>
                                                                                <li><a data-toggle="tab" href="#men<?= $inActiveservice['service_id'] ?>">Status</a></li>
                                                                              <?php /*  <li><a data-toggle="tab" href="#menu2<?= $inActiveservice['service_id'] ?>">Menu 2</a></li> */ ?>
                                                                            </ul>

                                                                            <div class="tab-content">
                                                                                <div id="home<?= $inActiveservice['service_id'] ?>" class="tab-pane fade in active">
                                                                                    <div id="staffserviceform<?= $inActiveservice['service_id'] ?>">

                                                                                    </div>
                                                                                    <br/>
                                                                                    <a href="addworkinghours?service_id=<?= $inActiveservice['service_id'] ?>" class="addupdate_allworking"> Add or Update working hours for this service.</a>
                                                                                    <p><?php foreach($staffList as $staff_name){
                                                                                            ?> <a href="<?= Yii::getAlias('@web') ?>/index.php/services/updatestaff_formslots?service_id=<?= $inActiveservice['service_id'] ?>&staff_id=<?php echo $staff_name->id; ?>" onclick="staff_serviceslots(event,this)" class="btn btn-info staff_serviceslots"> <?php echo $staff_name->username; ?></a> <?php


                                                                                        }?></p>
                                                                                </div>
                                                                                <div id="men<?= $inActiveservice['service_id'] ?>"" class="tab-pane fade">

                                                                                <p><br/><a href="<?= Yii::getAlias('@web') ?>/index.php/services/disableservice?service_id=<?= $inActiveservice['service_id'] ?>&status=active" onclick="EnableDisableService(event,this)"  class="diableenableservice btn btn-success">Enable</a> </p>
                                                                            </div>

                                                        </div>
                                                        </td>
                                                        </tr>
                                                                    <?php } ?>
                                                            <?php }else { ?>
                                                                    <tr>
                                                                        <td>
                                                                            <ul class="list-inline list-action">
                                                                                <li>
                                                                                 No Post to show
                                                                                </li>

                                                                            </ul>
                                                                        </td>
                                                                    </tr>
                                                            <?php } ?>

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                </div>
                                                </div>
                                        </div>
                                        </div>
                        </div>

                                    <?php  } ?>


                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>



        </div>
    </div>









<?php Pjax::end(); ?>
































    <?php // ActiveForm::end();
     // Pjax::end(); ?>


<div class="clearfix"></div>



<?php
$script = "


$('#updatecreateform').load('".Yii::getAlias('@web')."/index.php/services/create');

$('form#sevicecommonform :text').change(function(){
    $('form#sevicecommonform').submit();
   
});

$('body').on('beforeSubmit', 'form#sevicecommonform', function () {
var form = $(this);
// return false if form still have some validation errors
if (form.find('.has-error').length) {
return false;
}
// submit form
$.ajax({
url: form.attr('action'),
type: 'post',
data: form.serialize(),
success: function (response) {
    if(response==1){
       $('#singularsaved').css('display','inline-block');
       setTimeout(function(){
         $('#singularsaved').hide('slow');
            },2000);
    }

    else {
        alert('not updated');

    }
}
});
return false;
});

$('body').on('beforeSubmit', '#updatecreateform form', function () {
var form = $(this);
// return false if form still have some validation errors
if (form.find('.has-error').length) {
return false;
}
//alert('aaa');
  var form_data = form.serialize();
  var formData = new FormData(this);
// submit form
$.ajax({
url: form.attr('action'),
type: 'post',
//data: form.serialize(),
 data:formData,
  contentType: false,
        processData: false,
success: function (response) {
    if(response==1){
   document.getElementById('servicecreatedform').reset();
$('#updatecreateform .alert.alert-success').show();
setTimeout(function () {
$('#updatecreateform .alert.alert-success').hide();
}, 3000);
     $.pjax.reload({container:'#srvicereloadsec',timeout:60000});

    }else if(response==2) {
    $('#updatecreateform').load('".Yii::getAlias('@web')."/index.php/services/create');

     $.pjax.reload({container:'#srvicereloadsec',timeout:60000});
   }
}
});
return false;
});



$('.service_updatesubmit').click(function(event){

});
$('.timepicker').timepicker({
        showPeriod: true
      
    });
 $('.staffrecurngtime').on('submit', function( event ) {

    event.preventDefault();
    var formdate = $(this).serialize();
    $.ajax({
url:'schedule',
type: 'post',
data: formdate,
success: function (response) {
    if(response==1){
     //   alert('changed');

    }else {
       // alert('not updated');

    }
}
});
    console.log(formdate);
 }); 
   $('.addupdate_allworking').click(function(event){
  event.preventDefault();
var linkattr =  $(this).attr('href');
var divid =  $(this).siblings('div').attr('id');

$('#'+divid).load(linkattr);

   });
   
   
 

    $('.staffimageform').on('submit',(function(e) {
        e.preventDefault();
        
        var formData = new FormData(this);
        var formid = $(this).attr('id');

        $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
               if(data!= '0'){
               alert('dfasf');
               $('#'+formid).siblings('img').attr('src','".Yii::getAlias('@web')."/'+data)
               }
            },
            error: function(data){
                console.log('error');
                console.log(data);
            }
        });
    }));

    $('.staffimagelog').on('change', function() {
   
        $(this).parent('form').submit();
    });
";
$this->registerJs($script); ?>

<?php
$script2 ="jQuery(document).on('click', '.showModalButton', function(){
        if ($('#modal').data('bs.modal').isShown) {
            $('#modal').find('#modalContent')
                .load($(this).attr('value'));
            document.getElementById('modalHeader').innerHTML ='<h4>' + $(this).attr('title')+'</h4>';
        } else {
            $('#modal').modal('show').find('#modalContent').load($(this).attr('value'));
            document.getElementById('modalHeader').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
        }
    });
   
" ;
    $this->registerJs($script2);
?>


<script type="text/javascript">

</script >
<script>
    function EnableDisableService(event,xur) {
        event.preventDefault();
        $.ajax({
            url:$(xur).attr('href'),
            type: 'post',
            success: function (response) {
                if(response==1){
                    $.pjax.reload({container:'#srvicereloadsec',timeout:60000});

                }
            }
        });
    }
    function serviceCatboxShow(event,hideitem,showitem){
        event.preventDefault();
        $(hideitem).hide();
        $(showitem).show();

    }
    function deletecategory(event,selectbox){
        event.preventDefault();
        var optionRe = $(selectbox+' option:selected');
        var option_value = $(selectbox+' option:selected').val();
        var option_name = $(selectbox+' option:selected').text();
        if (confirm("Are you sure you want "+option_name+" delete?All services related to categories also deleted") == true) {

            $.ajax({
                url:'categorydelete',
                type: 'post',
                data:{'id':option_value},
                success: function (response) {
                    if(response==1){

                        $(optionRe).remove();
                        $.pjax.reload({container:'#srvicereloadsec',timeout:60000});



                    }else {
                       // alert('not updated');

                    }
                }
            });
            

        } else {
            x = "You pressed Cancel!";
        }
    }
    function updatelinkedresource(event,xor){
        event.preventDefault();
        var formid = $(xor).parents('form').attr('id');
        $.ajax({
            url: $('#'+formid).attr('action'),
            type: 'post',
            data:$('#'+formid).serialize(),
            success: function (response) {
                if(response==1){

                   $('#'+formid+' .sucalert').show();
                    setTimeout(function () {
                        $('#'+formid+' .sucalert').hide();
                    }, 3000);



                }else {
                    // alert('not updated');

                }
            }
        });

    }

 //   $('.staff_serviceslots').click(function(event){
    function staff_serviceslots(event,xor){
        event.preventDefault();
        var linkattr =  $(xor).attr('href');
        var divid =  $(xor).parent('p').siblings('div').attr('id');
        $('#'+divid).load(linkattr);


        //$('#scdemodal').modal('show').find('#modalscedule').load($(this).attr('href'));
    }
    function changeProfile(event,xor){
        event.preventDefault();
        $(xor).siblings().click();
    }
    function showinputname(event,xor){
        event.preventDefault();
        $(xor).parents('p').hide();
        var show_catid = $(xor).attr('href');
        $(show_catid).show();

    }
    function servicecatSave(event,xor){
        event.preventDefault();
        var id = $(xor).attr('data-servicecat');
        var catname = $('#editcatid'+id+' input').val();
        if(catname!=''){
            $.ajax({
                url: 'updatecategoryname?id='+id,
                type: 'post',
                data: {service_category_name : catname},
                success: function (response) {
                    if(response==1){
                        $('#editcatname'+id+' span').text(catname);
                        $('#editcatname'+id+'').show();
                        $('#editcatid'+id+'').hide();

                    }else {


                    }
                }
            });
        }
        //  alert(id);

        return false;
    }

    function textthemeeditservice(event,xor){
        event.preventDefault();

        $('#updatecreateform').load($(xor).attr('href'));
        //$('#modal').modal('show').find('#modalContent').load($(this).attr('href'));
    }

</script>

<?php
/*$script0 = "
$(document).ready(function(){
$('#example').DataTable( {
dom: 'Bfrtip'
} );
});

";

$this->registerJs($script0);*/
/*$script2 = "
$(document).ready(function(){
$('#example').DataTable( {
dom: 'Bfrtip'
} );
});

";

$this->registerJs($script2);
Pjax::begin();
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-sm',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);
echo "<div id='modalContent'></div>";
yii\bootstrap\Modal::end();
Pjax::end(); */?><!--
<?php /*Pjax::begin();
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'scdemodal',
    'size' => 'modal-md',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);
echo "<div id='modalscedule'></div>";
yii\bootstrap\Modal::end();
Pjax::end(); */?>











-->