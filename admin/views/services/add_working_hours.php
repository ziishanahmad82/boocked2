<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Services */
/* @var $form yii\widgets\ActiveForm */


if($weeklytime) {
    ?>
    <?php foreach ($weeklytime as $weeklytimes) { ?>

            <div class="col-sm-6">

                <select  class="form-control" id="staff_id">
                    <?php foreach($staffList as $staff_name){
                ?><option value="<?php echo $staff_name->id; ?>" <?php if($staff_id==$staff_name->id){echo 'selected';} ?>><?php echo $staff_name->username; ?></option> <?php


            }  ?>
                </select>
                <input type="hidden" value="<?php  print_r($service_id)?>" name="service_id" id="service_id">
            </div>
        <div class="workinload">
        <form class="staffrecurngtime12" action="updaterecurslots?id=<?= $weeklytimes->wrt_id ?>" id="wrtform<?= $weeklytimes->wrt_id ?>">
            <input type="hidden" value="<?php  print_r($service_id)?>" name="service_id">
            <?php $sunday = json_decode($weeklytimes->sunday, true);
            $monday = json_decode($weeklytimes->monday, true);
            $tuesday = json_decode($weeklytimes->tuesday, true);
            $wednesday = json_decode($weeklytimes->wednesday, true);
            $thursday = json_decode($weeklytimes->thursday, true);
            $friday = json_decode($weeklytimes->friday, true);
            $saturday = json_decode($weeklytimes->saturday, true);
            $sunday_size = sizeof($sunday['from']);
            $monday_size = sizeof($monday['from']);
            $tuesday_size = sizeof($tuesday['from']);
            $wednesday_size = sizeof($wednesday['from']);
            $thursday_size = sizeof($thursday['from']);
            $friday_size = sizeof($friday['from']);
            $saturday_size = sizeof($saturday['from']);
            ?>

            <?php for ($x = 0; $x != $sunday_size; $x++) {

                ?>
                <div class="col-sm-12">
                    <div class="col-sm-2">Sun</div>
                    <div class="col-sm-2"><input type="text" value="<?= $sunday['from'][$x] ?>" name="staffsun[from][]"
                                                 class="timepicker1 form-control"></div>
                    <div class="col-sm-1"> to</div>
                    <div class="col-sm-2"><input type="text" value="<?= $sunday['to'][$x] ?>" name="staffsun[to][]"
                                                 class="timepicker1 form-control"></div>
                </div>
            <?php }
            ?>
            <?php for ($x = 0; $x != $monday_size; $x++) { ?>

                <div class="col-sm-12">
                    <div class="col-sm-2">Mon</div>
                    <div class="col-sm-2"><input type="text" value="<?= $monday['from'][$x] ?>" name="staffmon[from][]"
                                                 class="timepicker1 form-control"></div>
                    <div class="col-sm-1"> to</div>
                    <div class="col-sm-2"><input type="text" name="staffmon[to][]" value="<?= $monday['to'][$x] ?>"
                                                 class="timepicker1 form-control"></div>
                </div>
            <?php } ?>
            <?php for ($x = 0; $x != $tuesday_size; $x++) { ?>
                <div class="col-sm-12">
                    <div class="col-sm-2">Tue</div>
                    <div class="col-sm-2"><input type="text" value="<?= $tuesday['from'][$x] ?>" name="stafftue[from][]"
                                                 class="timepicker1 form-control"></div>
                    <div class="col-sm-1"> to</div>
                    <div class="col-sm-2"><input type="text" name="stafftue[to][]" value="<?= $tuesday['to'][$x] ?>"
                                                 class="timepicker1 form-control"></div>
                </div>
            <?php } ?>
            <?php for ($x = 0; $x != $wednesday_size; $x++) { ?>
                <div class="col-sm-12">
                    <div class="col-sm-2">Wed</div>
                    <div class="col-sm-2"><input type="text" value="<?= $wednesday['from'][$x] ?>"
                                                 name="staffwed[from][]" class="timepicker1 form-control"></div>
                    <div class="col-sm-1"> to</div>
                    <div class="col-sm-2"><input type="text" name="staffwed[to][]" value="<?= $wednesday['to'][$x] ?>"
                                                 class="timepicker1 form-control"></div>
                </div>
            <?php }
            for ($x = 0; $x != $thursday_size; $x++) { ?>
                <div class="col-sm-12">
                    <div class="col-sm-2">Thurs</div>
                    <div class="col-sm-2"><input type="text" value="<?= $thursday['from'][$x] ?>"
                                                 name="staffthurs[from][]" class="timepicker1 form-control"></div>
                    <div class="col-sm-1"> to</div>
                    <div class="col-sm-2"><input type="text" name="staffthurs[to][]" value="<?= $thursday['to'][$x] ?>"
                                                 class="timepicker1 form-control"></div>
                </div>
            <?php }
            for ($x = 0; $x != $friday_size; $x++) { ?>
                <div class="col-sm-12">
                    <div class="col-sm-2">Fri</div>
                    <div class="col-sm-2"><input type="text" value="<?= $friday['from'][$x] ?>" name="stafffri[from][]"
                                                 class="timepicker1 form-control"></div>
                    <div class="col-sm-1"> to</div>
                    <div class="col-sm-2"><input type="text" name="stafffri[to][]" value="<?= $friday['to'][$x] ?>"
                                                 class="timepicker1 form-control"></div>
                </div>
            <?php }
            for ($x = 0; $x != $saturday_size; $x++) {
                ?>
                <div class="col-sm-12">
                    <div class="col-sm-2">Sat</div>
                    <div class="col-sm-2"><input type="text" value="<?= $saturday['from'][$x] ?>"
                                                 name="staffsat[from][]" class="timepicker1 form-control"></div>
                    <div class="col-sm-1"> to</div>
                    <div class="col-sm-2"><input type="text" name="staffsat[to][]" value="<?= $saturday['to'][$x] ?>"
                                                 class="timepicker1 form-control"></div>
                </div>
            <?php } ?>
            <div class="col-sm-12">
                <div class="col-sm-6"><input type="submit" class="btn btn-success btn btn-warning ad1 " value="Update">
                </div>
            </div>

        </form>
        </div>
    <? }
    $scriptq ="
    $( '#staff_id' ).change(function () {
   
    var staff_id = $( '#staff_id option:selected' ).val();
    var service_id = $('#service_id').val();
    $('.workinload').load('updatestaff_formslots?service_id='+service_id+'&staff_id='+staff_id)
     });
    
    $('.staffrecurngtime12').on('submit', function( event ) {

event.preventDefault();
var formdate = $(this).serialize();
var fromid = $(this).attr('id');
$.ajax({
url:$(this).attr('action'),
type: 'post',
data: formdate,
success: function (response) {
alert(response);
if(response==1){
$('#'+fromid).remove();
}else {
// alert('not updated');

}
}
});
});
";
    $this->registerJs($scriptq);
} else {?>
    <div class="workinload">
    <form class="staffrecurngtime12" id="staff_recurform12">
        <div class="col-sm-6">

            <input type="hidden" value="<?= $service_id ?>" name="service_id" id="service_id">
            <select  class="form-control" id="staff_id">
            <?php foreach($staffList as $staff_name){
                ?><option value="<?php echo $staff_name->id; ?>" <?php if($staff_id==$staff_name->id){echo 'selected';} ?>><?php echo $staff_name->username; ?></option> <?php


            }  ?>
</select>
        </div>

        <div class="col-sm-12">
            <div class="col-sm-2">Sun</div><div class="col-sm-2"><input type="text" name="staffsun[from][]" class="timepicker2 form-control"> </div><div class="col-sm-1"> to </div><div class="col-sm-2"><input type="text" name="staffsun[to][]" class="timepicker2 form-control"></div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-2">Sun</div><div class="col-sm-2"><input type="text" name="staffsun[from][]" class="timepicker2 form-control"> </div><div class="col-sm-1"> to </div><div class="col-sm-2"><input type="text" name="staffsun[to][]" class="timepicker2 form-control"></div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-2">Mon</div><div class="col-sm-2"><input type="text" name="staffmon[from][]" class="timepicker2 form-control"> </div><div class="col-sm-1"> to </div><div class="col-sm-2"><input type="text" name="staffmon[to][]" class="timepicker2 form-control"></div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-2">Tue</div><div class="col-sm-2"><input type="text" name="stafftue[from][]" class="timepicker2 form-control"> </div><div class="col-sm-1"> to </div><div class="col-sm-2"><input type="text" name="stafftue[to][]" class="timepicker2 form-control"></div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-2">Wed</div><div class="col-sm-2"><input type="text" name="staffwed[from][]" class="timepicker2 form-control"> </div><div class="col-sm-1"> to </div><div class="col-sm-2"><input type="text" name="staffwed[to][]" class="timepicker2 form-control"></div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-2">Thurs</div><div class="col-sm-2"><input type="text" name="staffthurs[from][]" class="timepicker2 form-control"> </div><div class="col-sm-1"> to </div><div class="col-sm-2"><input type="text" name="staffthurs[to][]" class="timepicker2 form-control"></div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-2">Fri</div><div class="col-sm-2"><input type="text" name="stafffri[from][]" class="timepicker2 form-control"> </div><div class="col-sm-1"> to </div><div class="col-sm-2"><input type="text" name="stafffri[to][]" class="timepicker2 form-control"></div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-2">Sat</div><div class="col-sm-2"><input type="text" name="staffsat[from][]" class="timepicker2 form-control"> </div><div class="col-sm-1"> to </div><div class="col-sm-2"><input type="text" name="staffsat[to][]" class="timepicker2 form-control"></div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6"><input type="submit" class="btn btn-success btn btn-warning ad1 " value="Update"></div>
        </div>


    </form>
    </div>
    <?php
  $script_second ="$('.timepicker2').timepicker({
        showPeriod: true
      
    });
      $( '#staff_id' ).change(function () {
   
    var staff_id = $( '#staff_id option:selected' ).val();
    var service_id = $('#service_id').val();
    $('.workinload').load('updatestaff_formslots?service_id='+service_id+'&staff_id='+staff_id)
     });
  $('.staffrecurngtime12').on('submit', function( event ) {

    event.preventDefault();
    var formdate = $(this).serialize();
    var fromid = $(this).attr('id');
    $.ajax({
    url:'schedule',
    type: 'post',
    data: formdate,
    success: function (response) {
    if(response==1){
   $('#'+fromid).remove();

    }else {
    // alert('not updated');

    }
    }
    });
    });
    ";
    $this->registerJs($script_second);
}


