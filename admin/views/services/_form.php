<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Services */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="col-lg-12 col-md-12 col-sm-12 top">

    <?php $form = ActiveForm::begin(['id'=> 'servicecreatedform','options' => ['enctype' => 'multipart/form-data']]); ?>

        <p>Category</p>
        <div id="catselctbox">
        <div class="form-group">

            <select class="form-control" name="Services[service_cat_id]" id="category_createselectebox">
              <?php $i=1; ?>
                <?php foreach($Category_model as $category){ ?>
                <option value="<?=$category->service_cat_id?>" <?php if($model->service_cat_id==$category->service_cat_id){?> selected="" <?php } ?>><?=$category->service_category_name?></option>
                <?php $i++;
                } ?>
               
            </select>
            <small>
                <a style="color: #FC7600 !important;text-decoration: underline !important;" href="#" onclick="serviceCatboxShow(event,'#catselctbox','#catinputbox')"  id="addNewServiceCat">New Category</a> |<a style="color: #FC7600 !important;text-decoration: underline !important;" href="#" onclick="deletecategory(event,'#category_createselectebox')"> Delete Category</a>
            </small>
        </div>

            </div>
        <div id="catinputbox" style="display:none">
            <div class="form-group">
            <input type="text" name="service_category_name" class="form-control" >
                <small>
                    <a style="color: #FC7600 !important;text-decoration: underline !important;" href="#" onclick="serviceCatboxShow(event,'#catinputbox','#catselctbox')" id="addNewServiceCat">Show List</a>
                </small>

            </div>

        </div>

    <?= $form->field($model, 'service_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'service_description')->textarea(['rows' => 6]) ?>
    <div class="col-lg-12 col-md-12 col-sm-12" style="padding: 0">
        <div class="map_canvas"></div>
        <div class="clearfix"> </div>

        <input id="geocomplete" type="text" class="form-control" placeholder="Type a address" name="google_ad" value="" />
        <a id="find" class="btn btn-primary" style="margin-top:10px">Find</a>
        <div class="clearfix"> </div> <div class="clearfix"> </div>
        <br>
        <div class="form-group1 group-mail">
            <label class="control-label">Latitude</label>
            <input name="lat" class="form-control"  type="text"  disabled><input name="lat" type="text" hidden>

        </div>
        <div class="clearfix"> </div>
        <div class=" form-group1 group-mail ">
            <label class="control-label">Longitude</label>
            <input name="lng" class="form-control" type="text" disabled>
            <input name="lng" class="form-control" type="text" hidden>


        </div>

        <a id="reset" href="#" style="display:none;">Reset Marker</a><br>

    </div>
<div class="col-lg-6 col-md-6 col-sm-6" style="padding-left: 0">
    <?= $form->field($model, 'service_duration')->textInput() ?>
</div>
    <div class="col-lg-6 col-md-6 col-sm-6" style="padding-right: 0">
    <?= $form->field($model, 'service_price')->textInput(['maxlength' => true]) ?>
        </div>
    <div class="col-lg-6 col-md-6 col-sm-6" style="padding-left: 0">
    <?= $form->field($model, 'service_capacity')->textInput() ?>
     </div>
<div class="clearfix"></div>
    <?= $form->field($model, 'imageFile1')->fileInput() ?>
    <?= $form->field($model, 'imageFile2')->fileInput() ?>
    <?= $form->field($model, 'imageFile3')->fileInput() ?>
    <?= $form->field($model, 'service_video')->fileInput() ?>

<!--    --><?//= $form->field($model, 'business_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn btn-warning ad1' : 'btn btn-primary btn btn-warning ad1 service_updatesubmit']) ?>
    </div>
        <div class="alert alert-success" style="display:none">
            <strong>Successfully Created!</strong>.
        </div>
    <?php ActiveForm::end(); ?>

</div>
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyDgHqFXsJ-zVGOZ_wMk-cL9sqViJ47kxcA&sensor=false&amp;libraries=places"></script>


<script src="/admin/js/jquery.geocomplete.min.js"></script>

<script>
    $(function(){
        $("#geocomplete").geocomplete({
            map: ".map_canvas",
            details: "form ",
            markerOptions: {
                draggable: true
            }
        });

        $("#geocomplete").bind("geocode:dragged", function(event, latLng){
            $("input[name=lat]").val(latLng.lat());
            $("input[name=lng]").val(latLng.lng());
            $("#reset").show();
        });


        $("#reset").click(function(){
            $("#geocomplete").geocomplete("resetMarker");
            $("#reset").hide();
            return false;
        });

        $("#find").click(function(){
            $("#geocomplete").trigger("geocode");
        }).click();
    });
</script>
<style>
    .map_canvas {
        width: 250px !important;
        height: 400px !important;
        margin: 10px 20px 10px 0;
    }
    #geocomplete {
        display: -webkit-inline-box;
    }
</style>