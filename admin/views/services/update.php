<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Services */

//$this->title = 'Edit Services: ' . ' ' . $model->service_id;
$this->title = 'Edit Services';
$this->params['breadcrumbs'][] = ['label' => 'Services', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->service_id, 'url' => ['view', 'id' => $model->service_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="panel-heading full">
    <ul class="list-inline list-staff">
        <li><h5 class="panel-title text-uppercase"><i class="fa fa-pencil" style="color: #FC7600; position: relative; top: 3px;"></i><span class="staff-head"><?= Html::encode($this->title) ?></span></h5></li>
    </ul>
</div>
<div class="panel-body panel-body-nav-tabs-sidebar">
    <div class="row">
        

    <?= $this->render('_form', [
        'model' => $model,
        'Category_model'=>$Category_model,
    ]) ?>
    </div>
</div>
