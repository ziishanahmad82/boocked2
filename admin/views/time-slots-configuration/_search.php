<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TimeSlotsConfigurationSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="time-slots-configuration-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'day_starts_time') ?>

    <?= $form->field($model, 'day_ends_time') ?>

    <?= $form->field($model, 'slot_period') ?>

    <?= $form->field($model, 'lunch_start_time') ?>

    <?php // echo $form->field($model, 'lunch_end_time') ?>

    <?php // echo $form->field($model, 'weekdays') ?>

    <?php // echo $form->field($model, 'test_type') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
