<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use kartik\time\TimePicker;
//use kartik\select2\Select2;
use app\models\ThirdPartyUsers;
use app\models\Users;

/* @var $this yii\web\View */
/* @var $model app\models\TimeSlotsConfiguration */
/* @var $form yii\widgets\ActiveForm */

$ajax_path=Yii::getAlias('@web');


?>

<div class="time-slots-configuration-form">

    <?php $form = ActiveForm::begin(); ?>


    <?
    if(@$_REQUEST["test_type"]){
        $test_type=$_REQUEST["test_type"];
    }
    ?>

    <div id="just_hide">
    <b>Select an option:</b> <select name="date_range" id="date_range">
        <option value="single_date">One date</option>
        <option value="date_range" selected>Date range</option>
    </select>
    <br/>
    <br/>
    <div id="div_single_date">
        <?=
        $form->field($model, 'applicable_from_date')->widget(\yii\jui\DatePicker::classname(), [
            'dateFormat' => 'MM-dd-yyyy',
            //'dateFormat' => 'yyyy-MM-dd',
            'clientOptions' => [
                //'dateFormat' => 'yyyy-MM-dd',
                'showAnim' => 'fold',
                'yearRange' => 'c:c+10',
                'changeMonth' => true,
                'changeYear' => true,
                'autoSize' => true,
                //'defaultDate' => '01-01-1990',
                //'showOn'=> "button",
                //'buttonText' => 'clique aqui',
                //'buttonImage'=> "images/calendar.gif",
            ]
        ])
        ?>
    </div>
        </div>

    <div id="div_date_range">
        <?=
        $form->field($model, 'date_range_start')->widget(\yii\jui\DatePicker::classname(), [
            'dateFormat' => 'MM-dd-yyyy',
            'clientOptions' => [
                //'dateFormat' => 'yyyy-MM-dd',
                'showAnim' => 'fold',
                'yearRange' => 'c:c+10',
                'changeMonth' => true,
                'changeYear' => true,
                'autoSize' => true,
                //'defaultDate' => '01-01-1990',
                //'showOn'=> "button",
                //'buttonText' => 'clique aqui',
                //'buttonImage'=> "images/calendar.gif",
            ]
        ])
        ?>

        <?=
        $form->field($model, 'date_range_end')->widget(\yii\jui\DatePicker::classname(), [
            'dateFormat' => 'MM-dd-yyyy',
            'clientOptions' => [
                //'dateFormat' => 'yyyy-MM-dd',
                'showAnim' => 'fold',
                'yearRange' => 'c:c+10',
                'changeMonth' => true,
                'changeYear' => true,
                'autoSize' => true,
                //'defaultDate' => '01-01-1990',
                //'showOn'=> "button",
                //'buttonText' => 'clique aqui',
                //'buttonImage'=> "images/calendar.gif",
            ]
        ])
        ?>

    </div>




    <b>Select calendar type</b>&nbsp;<select name="calendar_admin" id="calendar_admin">
        <option value="NCDL">Standard</option>
        <option value="CDL">CDL</option>
        <option value="Third party cdl">Third Party CDL</option>
        <option value="Third party standard">Third Party Standard</option>


    </select>


    <br/><br/>
    <div id="third_party_users_div">
        <?
        $third_party_users=Users::find()->andFilterWhere([
            'and',
            [
                'like', 'user_group', 'thirdparty'
            ]
        ])->all();
        $third_party_html='';
        foreach($third_party_users as $third_party_user){



            $third_party_html.="<input class=\"third_party_users\" type=\"checkbox\" name=\"third_party_users[]\" value=\"$third_party_user->id\"> $third_party_user->displayname &nbsp;&nbsp;";



        }
        $third_party_html.="<br/>";
        $third_party_html.="<br/>";
        echo $third_party_html;
        ?>

    </div>

    <?
    if($test_type=='CDL')
        echo "<b>Duration</b>&nbsp;<input type='text' name='duration' id='duration' value='90' size='2'>Minutes";
    else
        echo "<b>Duration</b>&nbsp;<input type='text' name='duration' id='duration' value='30' size='2'>Minutes";
    ?>

    <br/><br/>


    <b>Start Time</b> <input type="text" name="start_time" id="start_time" size="6" >
    <br/>
    <br/>
    <b>End Time</b> <input type="text" name="end_time" id="end_time" size="6" >
    <br/>
    <br/>
    <!--    <b>Number of slots</b> <input type="text" name="no_slots" id="no_slots" size="3">&nbsp;-->







    <!--    --><?//
    //    echo "<label class=\"control-label\">{$model->attributeLabels()['weekdays']}</label>";
    //    ?>
    <!--    --><?//
    //    $weekdays=['Monday'=>'Monday', 'Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday','Friday'=>'Friday','Saturday'=>'Saturday','Sunday'=>'Sunday'];
    //    echo Select2::widget([
    //        'model'=>$model,
    //        'attribute' => 'weekdays',
    //
    //        'data' => $weekdays,
    //        'size' => Select2::MEDIUM,
    //        'options' => ['placeholder' => 'Select weekdays...', 'multiple' => true],
    //        'pluginOptions' => [
    //            'allowClear' => true
    //        ],
    //    ]) ;
    //
    //    ?>



    <br/>
    <br/>

    <div id="sitem4">
        <input type="button" class="btn btn-success" value="Preview slots" id="btn-preview-slots">
    </div>


    <?php ActiveForm::end(); ?>

</div>

<div id="time-slot-configuraiton-right-div">
    <div id="preview_div">

    </div>
</div>



<?php
$this->registerJs("
var test_type=$('#test_type').val();
$('#date_range').change(function(){
    if($(this).val()=='single_date'){
        $('#div_date_range').hide();
        $('#div_single_date').show();
        $('#timeslotsconfiguration-date_range_start').val('');
        $('#timeslotsconfiguration-date_range_end').val('');
    }else{
        $('#div_date_range').show();
        $('#div_single_date').hide();
        $('#timeslotsconfiguration-applicable_from_date').val('');

    }
});

$(document).ready(function(){
        //$('#div_date_range').hide();
        $('#just_hide').hide();
        $('#third_party_users_div').hide();
        if($('#test_type').val()=='NCDL'){
            $('#duration').val('30');
            $('#start_time').val('08:30');
            $('#end_time').val('16:00');

        } else if($('#test_type').val()=='CDL'){
            $('#duration').val('90');
            $('#start_time').val('07:30');
            $('#end_time').val('16:00');
        }

        if($('#calendar_admin').val()!='third-party'){
            $('#duration').val('60');
            $('#start_time').val('09:00');
            $('#end_time').val('16:00');
        }
    });

    $('#calendar_admin').change(function(){


            if($(this).val()=='CDL'){
                $('#start_time').val('07:30');
                $('#duration').val('90');
                $('#end_time').val('16:00');
                $('#third_party_users_div').hide();
            } else if($(this).val()=='NCDL') {
                $('#start_time').val('08:30');
                $('#duration').val('30');
                $('#end_time').val('16:00');
                $('#third_party_users_div').hide();

            } else if($(this).val()=='Third party') {
                $('#start_time').val('08:30');
                $('#duration').val('60');
                $('#end_time').val('16:00');
                $('#third_party_users_div').show();

            }


    });

    $('#btn-preview-slots').click(function(){
        var effective_date=$('#timeslotsconfiguration-applicable_from_date').val();
        var date_range_start=$('#timeslotsconfiguration-date_range_start').val();
        var date_range_end=$('#timeslotsconfiguration-date_range_end').val();
        var third_party_users=$('.third_party_users');
        var test_type=$('#calendar_admin').val();
        var duration=$('#duration').val();
        var calendar_admin=$('#calendar_admin').val();
        var start_time=$('#start_time').val();
        var end_time=$('#end_time').val();
        //var no_slots=$('#no_slots').val();

        var third_party_users_arr={};
        var i=0;
        $('.third_party_users').each(function(){
            if(this.checked)
                third_party_users_arr[i]=this.value;
            i++;
        });




        $('#preview_div').load('$ajax_path/index.php/time-slots-configuration/previewslots',
        {
            'effective_date': effective_date,
            'date_range_start': date_range_start,
            'date_range_end': date_range_end,
            'third_party_users' : third_party_users_arr,
            'test_type':test_type,
            'duration':duration,
            'calendar_admin':calendar_admin,
            'start_time':start_time,
            'end_time':end_time,
            //'no_slots':no_slots,
        }
        );
    })

");
?>

