<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\time\TimePicker;
use kartik\select2\Select2;
use app\models\ThirdPartyUsers;

/* @var $this yii\web\View */
/* @var $model app\models\TimeSlotsConfiguration */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="time-slots-configuration-form">

    <?php $form = ActiveForm::begin(); ?>


    <?
    if(@$_REQUEST["test_type"]){
        $test_type=$_REQUEST["test_type"];
    }
    ?>
    <?= $form->field($model, 'test_type')->hiddenInput(['maxlength' => true, 'value' => $_REQUEST["test_type"]])->label(false) ?>
    <?=
    $form->field($model, 'applicable_from_date')->widget(\yii\jui\DatePicker::classname(), [
        'dateFormat' => 'MM-dd-yyyy',
        'clientOptions' => [
            //'dateFormat' => 'yyyy-MM-dd',
            'showAnim' => 'fold',
            'yearRange' => 'c:c+10',
            'changeMonth' => true,
            'changeYear' => true,
            'autoSize' => true,
            //'defaultDate' => '01-01-1990',
            //'showOn'=> "button",
            //'buttonText' => 'clique aqui',
            //'buttonImage'=> "images/calendar.gif",
        ]
    ])
    ?>


    <b>Select party for calendar</b>&nbsp;<select name="calender_admin" id="calender_admin">
        <option value="DMV">DMV</option>
        <?
        $thirdpartyusers=ThirdPartyUsers::findAll(['status'=>'active']);
        foreach($thirdpartyusers as $thirdpartyuser){
            ?>
            <option value="<?=$thirdpartyuser->id?>"><?=$thirdpartyuser->user_name;?></option>
            <?
        }
        ?>

    </select>


    <br/><br/>

    <?
    if($test_type=='CDL')
        echo "<b>Duration</b>&nbsp;<input type='text' name='duration' id='duration' value='90'>Mins";
    else
        echo "<b>Duration</b>&nbsp;<input type='text' name='duration' id='duration' value='30'>Mins";
    ?>

    <br/><br/>


    <div style="width:100px">

        <?
        echo "<label class=\"control-label\">{$model->attributeLabels()['slot_start_time']}</label>";
        ?>
        <?
        echo TimePicker::widget([
            'model'=>$model,
            'attribute' => 'slot_start_time',
            'pluginOptions' => [
                'showSeconds' => false,
                'showMeridian' => false,
                'minuteStep' => 1,
                'secondStep' => 5,
            ]
        ]);
        ?>

        <?
        echo "<label class=\"control-label\">{$model->attributeLabels()['slot_end_time']}</label>";
        ?>
        <?
        echo TimePicker::widget([
            'model'=>$model,
            'attribute' => 'slot_end_time',
            'pluginOptions' => [
                'showSeconds' => false,
                'showMeridian' => false,
                'minuteStep' => 1,
                'secondStep' => 5,
            ]
        ]);
        ?>


    </div>




    <!--    --><?//
    //    echo "<label class=\"control-label\">{$model->attributeLabels()['weekdays']}</label>";
    //    ?>
    <!--    --><?//
    //    $weekdays=['Monday'=>'Monday', 'Tuesday'=>'Tuesday','Wednesday'=>'Wednesday','Thursday'=>'Thursday','Friday'=>'Friday','Saturday'=>'Saturday','Sunday'=>'Sunday'];
    //    echo Select2::widget([
    //        'model'=>$model,
    //        'attribute' => 'weekdays',
    //
    //        'data' => $weekdays,
    //        'size' => Select2::MEDIUM,
    //        'options' => ['placeholder' => 'Select weekdays...', 'multiple' => true],
    //        'pluginOptions' => [
    //            'allowClear' => true
    //        ],
    //    ]) ;
    //
    //    ?>





    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<div id="time-slot-configuraiton-right-div">
    Hello
</div>

