<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TimeSlotsConfiguration */

$this->title = 'Setup Calendar';
$this->params['breadcrumbs'][] = ['label' => 'Setup Calendar', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="time-slots-configuration-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>



</div>
