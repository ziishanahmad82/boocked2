<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TimeSlotsConfigurationSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Time Slots Configurations';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="time-slots-configuration-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Time Slots', ['create', 'test_type' => 'NCDL'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'slot_start_time',
            'slot_end_time',
            'test_type',

//            [
//                'class' => 'yii\grid\ActionColumn',
//                'template' => '{update}{delete}',
//            ],
        ],
    ]); ?>

</div>
