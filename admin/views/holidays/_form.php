<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Holidays */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="holidays-form">

    <?php $form = ActiveForm::begin(); ?>


    <?=
    $form->field($model, 'holiday_date')->widget(\yii\jui\DatePicker::classname(), [
        'dateFormat' => 'MM-dd-yyyy',
        'clientOptions' =>[
            //'dateFormat' => 'yyyy-MM-dd',
            'showAnim'=>'fold',
            'yearRange' => 'c-25:c+10',
            'changeMonth'=> true,
            'changeYear'=> true,
            'autoSize'=>true,
            //'defaultDate'=>'01-01-1990',
            //'showOn'=> "button",
            //'buttonText' => 'clique aqui',
            //'buttonImage'=> "images/calendar.gif",
        ]
    ])
    ?>

    <?= $form->field($model, 'holiday_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'holiday_span')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
