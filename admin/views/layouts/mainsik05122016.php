<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\bootstrap\Modal;
use app\models\SiteSettings;

$customer_portal_status = SiteSettings::find()->where(['id'=>'1'])->one()->value;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>


<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>


</head>
<body>
<?php $this->beginBody() ?>


<header class="t_header">
    <div class="container">
        <div class="row">
            <div class="col-sm-3 pad">
                <div class="logo"><img src="<?= Yii::getAlias('@web') ?>/img/logo.png" class="img-responsive"></div>
            </div>
            <div class="col-sm-8">
<!--                <h1 id="fittext1" class="heading2">Road Test Scheduling System</h1>-->
            </div>
        </div>
    </div>
</header>

<?php
NavBar::begin([
    //'brandLabel' => 'VP',
    //'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar navbar-inverse',
    ],
]);




echo Nav::widget([
    'options' => ['class' => 'nav navbar-right nav-tabs'],
    'items' => [
        //['label' => 'Home', 'url' => ['/site/index']],

        Yii::$app->user->isGuest ?
            ['label' => 'Login', 'url' => ['/site/login']] :
            ['label' => 'Logout (' . Yii::$app->user->identity->username . ')',
                'url' => ['/site/logout'],
                'linkOptions' => ['data-method' => 'post']],
    ],
]);





if (!Yii::$app->user->isGuest && Yii::$app->user->identity->user_group=='admin') {


    if($customer_portal_status=='active'){
        echo Nav::widget([
            'options' => ['class' => 'nav navbar-right nav-tabs'],
            'items' => [
                [
                    'label' => 'Disable Customer Portal',
                    'url' => ['/site/customerportalstatus?action=disable'],
                    'linkOptions' => ['data-method' => 'post'],
                ]

            ],

        ]);
    } else {
        echo Nav::widget([
            'options' => ['class' => 'nav navbar-right nav-tabs'],
            'items' => [
                [
                    'label' => 'Enable Customer Portal',
                    'url' => ['/site/customerportalstatus?action=enable'],
                    'linkOptions' => ['data-method' => 'post'],
                ]

            ],

        ]);
    }

//

    echo Nav::widget([
        'options' => ['class' => 'nav navbar-left nav-tabs'],
        'items' => [

            ['label' => 'Dashboard / Today', 'url' => ['/']],
            ['label' => 'Appointment Calendar', 'url'=> ['/customer-reserved-timeslots/index2'],

            ],
            //['label' => 'Calendar Summary', 'url' => ['/customer-reserved-timeslots/index']],
            //['label' => 'Examiners To Locations', 'url' => ['/examinertolocation/index']],
            ['label' => 'Administrative Setup ',
                'items' => [
                    ['label' => 'Services', 'url' => ['/services/index']],
                    ['label' => 'Initial Calendar Setup', 'url' => ['/time-slots-configuration/create?test_type=NCDL']],
                    ['label' => 'Calendar Maintenance', 'url' => ['/slots/index']],
                    ['label' => 'Calendar Batches', 'url' => ['/batches/index']],
                    ['label' => 'Locations', 'url' => ['/locations/index']],
                    ['label' => 'Holidays', 'url' => ['/holidays/index']],
                    ['label' => 'Emergency days', 'url' => ['/emergency-days/index']],
                    ['label' => 'User Roles', 'url' => ['/user-roles/index']],
                    ['label' => 'Third Party Users', 'url' => ['/users/index']],
                    ['label' => 'Tough Books', 'url' => ['/toughbooks/index']],
            ],
            ],
            //['label' => 'Schedule Test', 'url' => ['/scheduletest/index']],
            //['label' => 'Examiners', 'url' => ['/examiners/index']],


        ],
    ]);

    echo Nav::widget([
        'options' => ['class' => 'nav navbar-left nav-tabs'],
        'items' => [



            ['label' => 'Reports', 'url' => ['/reports/index']],
            //['label' => 'Schedule An Appointment', 'url' => 'http://lh/dmv/customer/web/index.php',  'linkOptions' => ['target' => '_blank']],

        ],
    ]);
} else if(@stristr(Yii::$app->user->identity->user_group, 'thirdparty-')){

    echo Nav::widget([
        'options' => ['class' => 'nav navbar-left nav-tabs'],
        'items' => [

            ['label' => 'Dashboard / Today', 'url' => ['/']],
            ['label' => 'Calendar Maintainance', 'url' => ['/slots/index']],




        ],
    ]);

}
NavBar::end();
?>

<div class="container">

    <?php
    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
        Modal::begin([
            'id' => 'modal_flash',
            'header' => '<h3 class="alert alert-' . $key . '">'.ucfirst($key).' : '.$message.'</h3>',
            'size' => 'modal-md',
        ]);

        Modal::end();
        $flasjJs= <<<EOF
        $('#modal_flash .modal-body').remove();
        $('#modal_flash').modal('show');
EOF;




        $this->registerJs($flasjJs);


    }
    ?>


    <?= $content ?>

</div>


<footer class="footer">


    <div class="container"><p>&copy; Boocked.com <?= date('Y') ?></p></div>

</footer>


<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>

