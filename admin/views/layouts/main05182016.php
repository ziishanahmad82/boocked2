<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\bootstrap\Modal;
use app\models\SiteSettings;

$customer_portal_status = SiteSettings::find()->where(['id'=>'1'])->one()->value;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>


<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>


</head>
<body>
<?php $this->beginBody() ?>
<div class="menu-wrap">
            <nav class="menu">
                <div class="icon-list">
                    <ul class="list-unstyled">
                        <li>
                            <a href="#" class="menu-db">
                                <img src="<?= Yii::getAlias('@web') ?>/img/dashboard-hover.png"> 
                                <span class="menu-caption menu-media">Dashboard</span></a>
                        </li>
                        <li>
                            <a href="#" class="menu-cal">
                                <img src="<?= Yii::getAlias('@web') ?>/img/calendar-hover.png">
                                <span class="menu-caption menu-media">Calendar</span></a>
                        </li>
                        <li class="dropdown dropdown-large">
                            <a href="#" class="menu-cust dropdown-toggle" data-toggle="dropdown">
                                <img src="<?= Yii::getAlias('@web') ?>/img/customers.png"> 
                                <span class="menu-caption-cust">Customers <span class="caret"></span></span></a>
                            <ul class="dropdown-menu dropdown-menu-large row">
                                <li class="col-sm-2">
                                    <ul>
                                        <li class="dropdown-header"><a href="#">Sub Menu</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-2">
                                    <ul>
                                        <li class="dropdown-header"><a href="#">Sub Menu</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-2">
                                    <ul>
                                        <li class="dropdown-header"><a href="#">Sub Menu</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-2">
                                    <ul>
                                        <li class="dropdown-header"><a href="#">Sub Menu</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-2">
                                    <ul>
                                        <li class="dropdown-header"><a href="#">Sub Menu</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-2">
                                    <ul>
                                        <li class="dropdown-header"><a href="#">Sub Menu</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="menu-mark marketing">
                                <img src="<?= Yii::getAlias('@web') ?>/img/marketing-hover.png"> 
                                <span class="menu-caption menu-media">Marketing</span></a>
                        </li>
                        <li>
                            <a href="#" class="menu-report">
                                <img src="<?= Yii::getAlias('@web') ?>/img/reports-hover.png"> 
                                <span class="menu-caption menu-media">Reports</span></a>
                        </li>
                        <li>
                            <a href="#" class="menu-setting">
                                <img src="<?= Yii::getAlias('@web') ?>/img/settings-hover.png"> 
                                <span class="menu-caption menu-media">Settings</span></a>
                        </li>
                    </ul>
                </div>
            </nav>
            <button class="close-button" id="close-button">Close Menu</button>
            <div class="morph-shape" id="morph-shape" data-morph-open="M-1,0h101c0,0,0-1,0,395c0,404,0,405,0,405H-1V0z">
                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 100 800" preserveAspectRatio="none">
                <path d="M-1,0h101c0,0-97.833,153.603-97.833,396.167C2.167,627.579,100,800,100,800H-1V0z"/>
                </svg>
            </div>
        </div>
    
    <div class="container-fluid container-main-wrapper">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 media-col1">
                    <a href="index.html">
                        <img src="<?= Yii::getAlias('@web') ?>/img/logo.png" class="logo">
                    </a>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5 text-center media-col2">
                    <input id="foo" name="foo" class="form-control" type="text" value="" size="55" disabled=""/>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 media-col3">
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="list-inline pull-right list-menu-top">
                                <li>
                                    <a href="#" class="link-img-view">
                                        <img src="<?= Yii::getAlias('@web') ?>/img/customer-view.png" class="img-view"> 
                                        <img src="<?= Yii::getAlias('@web') ?>/img/customer-view-hover.png" class="img-view-hover">
                                        Customer View</a>
                                </li>
                                <li>
                                    <a href="#" class="link-img-help">
                                        <img src="<?= Yii::getAlias('@web') ?>/img/help.png" class="img-help"> 
                                        <img src="<?= Yii::getAlias('@web') ?>/img/help-hover.png" class="img-help-hover"> 
                                        Help</a>
                                </li>
                            </ul>
                        </div>
                        <div class="col-lg-12 media-col4">
                            <ul class="list-inline pull-right list-icons-top">
                                <li class="li-icon-first" data-toggle="tooltip" data-placement="left" title="New Appointment">
                                    <a href="#">
                                        <img src="<?= Yii::getAlias('@web') ?>/img/pen-hover.png" class="img-rect-pen">
                                        <img src="<?= Yii::getAlias('@web') ?>/img/pen.png" class="img-rect-pen-hover">
                                    </a>
                                </li>
                                <li class="li-icon-sec">
                                    <a href="#">
                                        <img src="<?= Yii::getAlias('@web') ?>/img/hands.png" class="img-rect-hand">
                                        <img src="<?= Yii::getAlias('@web') ?>/img/hands-hover.png" class="img-rect-hand-hover">
                                    </a>
                                </li>
                                <li class="li-icon-third">
                                    <a href="#">
                                        <img src="<?= Yii::getAlias('@web') ?>/img/mike.png" class="img-rect-mike">
                                        <img src="<?= Yii::getAlias('@web') ?>/img/mike-hover.png" class="img-rect-mike-hover">
                                    </a>
                                </li>
                                <li class="li-icon-forth">
                                    <a href="#">
                                        <img src="<?= Yii::getAlias('@web') ?>/img/sigs.png" class="img-rect-sigs">
                                        <img src="<?= Yii::getAlias('@web') ?>/img/sigs-hover.png" class="img-rect-sigs-hover">
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php
NavBar::begin([
    //'brandLabel' => 'VP',
    //'brandUrl' => Yii::$app->homeUrl,
    'options' => [
        'class' => 'navbar',
       
         
    ],
    'innerContainerOptions' => ['class'=>'container-fluid navbar-bg'],
]);
 echo Nav::widget([
        'options' => ['class' => 'nav navbar-nav nav-primary'],
     'encodeLabels' => false,
        'items' => [

            ['label' => '<img src="'.Yii::getAlias('@web').'/img/dashboard.png" class="icon-menu"> 
                                <img src="'. Yii::getAlias('@web').'/img/dashboard-hover.png" class="icon-menu-hover-db"> 
                                <span class="menu-caption">Dashboard</span>', 'url' => ['/'], 'options'=>['class'=>'menu-db']],
            ['label' => '<img src="'.Yii::getAlias('@web').'/img/calendar.png" class="icon-menu"> 
                           <img src="'. Yii::getAlias('@web').'/img/calendar-hover.png" class="icon-menu-hover-cal"> 
                           <span class="menu-caption">Calendar</span>', 'url' => ['/customer-reserved-timeslots/index2'], 'options'=>['class'=>'menu-cal']],
            
            //['label' => 'Calendar Summary', 'url' => ['/customer-reserved-timeslots/index']],
            //['label' => 'Examiners To Locations', 'url' => ['/examinertolocation/index']],
            ['label' =>  '<img src="'.Yii::getAlias('@web').'/img/customers-hover.png" class="icon-menu"> 
                           <img src="'. Yii::getAlias('@web').'/img/customers.png" class="icon-menu-hover-cust"> 
                           <span class="menu-caption">Customers</span>',  'options'=>['class'=>'menu-cust'],
                'items' => [
                    ['label' => 'Services', 'url' => ['/services/index']],
                    ['label' => 'Initial Calendar Setup', 'url' => ['/time-slots-configuration/create?test_type=NCDL']],
                    ['label' => 'Calendar Maintenance', 'url' => ['/slots/index']],
                    ['label' => 'Calendar Batches', 'url' => ['/batches/index']],
                    ['label' => 'Locations', 'url' => ['/locations/index']],
                    ['label' => 'Holidays', 'url' => ['/holidays/index']],
                    ['label' => 'Emergency days', 'url' => ['/emergency-days/index']],
                    ['label' => 'User Roles', 'url' => ['/user-roles/index']],
                    ['label' => 'Third Party Users', 'url' => ['/users/index']],
                    ['label' => 'Tough Books', 'url' => ['/toughbooks/index']],
                   
            ],
            
            ],
              ['label' => '<img src="'.Yii::getAlias('@web').'/img/marketing.png" class="icon-menu"> 
                           <img src="'. Yii::getAlias('@web').'/img/marketing-hover.png" class="icon-menu-hover-mark"> 
                           <span class="menu-caption">Marketing</span>', 'url' => ['/'], 'options'=>['class'=>'menu-mark marketing']],
             ['label' => '<img src="'.Yii::getAlias('@web').'/img/reports.png" class="icon-menu"> 
                           <img src="'. Yii::getAlias('@web').'/img/reports-hover.png" class="icon-menu-hover-report"> 
                           <span class="menu-caption">Reports</span>', 'url' => ['/reports/index'], 'options'=>['class'=>'menu-report']],
             ['label' => '<img src="'.Yii::getAlias('@web').'/img/settings.png" class="icon-menu"> 
                           <img src="'. Yii::getAlias('@web').'/img/settings-hover.png" class="icon-menu-hover-setting"> 
                           <span class="menu-caption">Settings</span>', 'url' => ['/reports/index'], 'options'=>['class'=>'menu-setting']],
            //['label' => 'Schedule Test', 'url' => ['/scheduletest/index']],
            //['label' => 'Examiners', 'url' => ['/examiners/index']],


        ],
     //'submenuTemplate' => '\n<ul class="dropdown-menu dropdown-menu-large row">\n <li class="col-sm-2"><ul><li class="dropdown-header">{items}</li></ul></li>\n</ul>\n',
    ]);
 ?>
   
    
     
                    <ul class="nav navbar-nav nav-primary">
                       
                        <li class="dropdown dropdown-large">
                            <a href="#" class="menu-cust dropdown-toggle" data-toggle="dropdown">
                                <img src="<?= Yii::getAlias('@web') ?>/img/customers-hover.png" class="icon-menu"> 
                                <img src="<?= Yii::getAlias('@web') ?>/img/customers.png" class="icon-menu-hover-cust"> 
                                <span class="menu-caption-cust">Customers <span class="caret"></span></span></a>
                            <ul class="dropdown-menu dropdown-menu-large row">
                                <li class="col-sm-2">
                                    <ul>
                                        <li class="dropdown-header"><a href="#">Sub Menu</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-2">
                                    <ul>
                                        <li class="dropdown-header"><a href="#">Sub Menu</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-2">
                                    <ul>
                                        <li class="dropdown-header"><a href="#">Sub Menu</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-2">
                                    <ul>
                                        <li class="dropdown-header"><a href="#">Sub Menu</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-2">
                                    <ul>
                                        <li class="dropdown-header"><a href="#">Sub Menu</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-2">
                                    <ul>
                                        <li class="dropdown-header"><a href="#">Sub Menu</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                       
                     
                      
                    </ul>
 <?php   

 $gravatarImageUrl = Yii::getAlias('@web').'/img/user.png';

// Then later:


    echo Nav::widget([
    'options' => ['class' => 'nav navbar-nav navbar-right nav-user'],
        'encodeLabels' => false,
    'items' => [
        //['label' => 'Home', 'url' => ['/site/index']],

        Yii::$app->user->isGuest ?
            ['label' => 'Login', 'url' => ['/site/login']] :
            ['label' =>  Yii::$app->user->identity->username, 'options'=>['class'=>'li-username'],
                'url' => ['/site/logout'],
                'linkOptions' => ['data-method' => 'post']],
    
         ['label' =>  Html::img($gravatarImageUrl), 'options'=>['class'=>'li-userimg'],
                'url' => ['/site/logout'],
                'linkOptions' => ['data-method' => 'post']],
    ],
]);
    ?>
                 <?php /*   <ul class="nav navbar-nav navbar-right nav-user">
                        <li class="li-username"><a href="#">User Name</a></li>
                        <li class="li-userimg"><img src="<?= Yii::getAlias('@web') ?>/img/user.png" class="image-user"></li>
                    </ul>
                <?php */ ?>
    
    
    
    
    
<?php NavBar::end(); ?>    
    <nav class="navbar">
            <div class="container-fluid navbar-bg">
                <div class="navbar-header">
                    <button class="menu-button" id="open-button">Open Menu</button>
                </div>
                
                <?php /* ?>
                <div class="collapse navbar-collapse">
                    <ul class="nav navbar-nav nav-primary">
                        <li>
                            <a href="#" class="menu-db">
                                <img src="<?= Yii::getAlias('@web') ?>/img/dashboard.png" class="icon-menu"> 
                                <img src="<?= Yii::getAlias('@web') ?>/img/dashboard-hover.png" class="icon-menu-hover-db"> 
                                <span class="menu-caption">Dashboard</span></a>
                        </li>
                        <li>
                            <a href="#" class="menu-cal">
                                <img src="<?= Yii::getAlias('@web') ?>/img/calendar.png" class="icon-menu">
                                <img src="<?= Yii::getAlias('@web') ?>/img/calendar-hover.png" class="icon-menu-hover-cal">
                                <span class="menu-caption">Calendar</span></a>
                        </li>
                        <li class="dropdown dropdown-large">
                            <a href="#" class="menu-cust dropdown-toggle" data-toggle="dropdown">
                                <img src="<?= Yii::getAlias('@web') ?>/img/customers-hover.png" class="icon-menu"> 
                                <img src="<?= Yii::getAlias('@web') ?>/img/customers.png" class="icon-menu-hover-cust"> 
                                <span class="menu-caption-cust">Customers <span class="caret"></span></span></a>
                            <ul class="dropdown-menu dropdown-menu-large row">
                                <li class="col-sm-2">
                                    <ul>
                                        <li class="dropdown-header"><a href="#">Sub Menu</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-2">
                                    <ul>
                                        <li class="dropdown-header"><a href="#">Sub Menu</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-2">
                                    <ul>
                                        <li class="dropdown-header"><a href="#">Sub Menu</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-2">
                                    <ul>
                                        <li class="dropdown-header"><a href="#">Sub Menu</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-2">
                                    <ul>
                                        <li class="dropdown-header"><a href="#">Sub Menu</a></li>
                                    </ul>
                                </li>
                                <li class="col-sm-2">
                                    <ul>
                                        <li class="dropdown-header"><a href="#">Sub Menu</a></li>
                                    </ul>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="#" class="menu-mark marketing">
                                <img src="<?= Yii::getAlias('@web') ?>/img/marketing.png" class="icon-menu"> 
                                <img src="<?= Yii::getAlias('@web') ?>/img/marketing-hover.png" class="icon-menu-hover-mark"> 
                                <span class="menu-caption">Marketing</span></a>
                        </li>
                        <li>
                            <a href="#" class="menu-report">
                                <img src="<?= Yii::getAlias('@web') ?>/img/reports.png" class="icon-menu"> 
                                <img src="<?= Yii::getAlias('@web') ?>/img/reports-hover.png" class="icon-menu-hover-report"> 
                                <span class="menu-caption">Reports</span></a>
                        </li>
                        <li>
                            <a href="#" class="menu-setting">
                                <img src="<?= Yii::getAlias('@web') ?>/img/settings.png" class="icon-menu"> 
                                <img src="<?= Yii::getAlias('@web') ?>/img/settings-hover.png" class="icon-menu-hover-setting"> 
                                <span class="menu-caption">Settings</span></a>
                        </li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right nav-user">
                        <li class="li-username"><a href="#">User Name</a></li>
                        <li class="li-userimg"><img src="<?= Yii::getAlias('@web') ?>/img/user.png" class="image-user"></li>
                    </ul>
                </div>
                <?php */ ?> 
                
            </div>
        </nav>
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    





<div class="container-fluid">

    <?php
    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
        Modal::begin([
            'id' => 'modal_flash',
            'header' => '<h3 class="alert alert-' . $key . '">'.ucfirst($key).' : '.$message.'</h3>',
            'size' => 'modal-md',
        ]);

        Modal::end();
        $flasjJs= <<<EOF
        $('#modal_flash .modal-body').remove();
        $('#modal_flash').modal('show');
EOF;




        $this->registerJs($flasjJs);


    }
    ?>


    <?= $content ?>

</div>



    <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <p class="pull-right">Boocked <?= date('Y') ?> &copy; Copyrights. All Rights Reserved</p>
                </div>
            </div>
        </div>


<?php $this->endBody() ?>

</body>
</html>
<?php $this->endPage() ?>

