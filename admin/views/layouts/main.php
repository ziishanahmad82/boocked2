<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\bootstrap\Modal;
use app\models\SiteSettings;
use yii\helpers\Url;

$customer_portal_status = SiteSettings::find()->where(['id'=>'1'])->one()->value;


AppAsset::register($this);
?>
<?php $this->beginPage() ?>


<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>


</head>
<body>
<?php $this->beginBody() ?>
<div class="menu-wrap">
            <nav class="menu">
                <div class="icon-list">
                    <ul class="list-unstyled">
                        <li>
                            <a href="<?= Yii::getAlias('@web') ?>/index.php/" class="menu-db">
                                <img src="<?= Yii::getAlias('@web') ?>/img/dashboard-hover.png">
                                <span class="menu-caption menu-media">Dashboard</span></a>
                        </li>
                        <li>
                            <a href="<?= Yii::getAlias('@web') ?>/index.php/customer-reserved-timeslots/index2" class="menu-cal">
                                <img src="<?= Yii::getAlias('@web') ?>/img/calendar-hover.png">
                                <span class="menu-caption menu-media">Calendar</span></a>
                        </li>
                        <li class="dropdown dropdown-large">
                            <a href="#" class="menu-cust dropdown-toggle" data-toggle="modal" data-target='#modal_mobile_customer'>
                                <img src="<?= Yii::getAlias('@web') ?>/img/customers.png">
                                <span class="menu-caption-cust">Customers <span class="caret"></span></span></a>

                        </li>
                        <li>
                            <a href="#" class="menu-mark marketing" data-toggle="modal" data-target='#modal-marketing'>
                                <img src="<?= Yii::getAlias('@web') ?>/img/marketing-hover.png">
                                <span class="menu-caption menu-media">Marketing <span class="caret caret-sidebar"></span></span></a>
                        </li>
                        <li>
                            <a href="#" class="menu-report" data-toggle="modal" data-target='#modal-reports'>
                                <img src="<?= Yii::getAlias('@web') ?>/img/reports-hover.png">
                                <span class="menu-caption menu-media">Reports <span class="caret caret-sidebar"></span></span></a>
                        </li>
                        <li>
                            <a href="#" class="menu-setting" data-toggle='modal' data-target='#modal-settings'>
                                <img src="<?= Yii::getAlias('@web') ?>/img/settings-hover.png">
                                <span class="menu-caption menu-media">Settings <span class="caret caret-sidebar"></span></span></a>
                        </li>
                        <li>
                            <a href="<?= Yii::getAlias('@web') ?>/index.php/offers" class="menu-db">
                                <img src="<?= Yii::getAlias('@web') ?>/img/dashboard-hover.png">
                                <span class="menu-caption menu-media">Offers</span></a>
                        </li>

                        <li>
                            <a href="#" class="menu-categories" data-toggle='modal' data-target='#modal-categories'>
                                <img src="<?= Yii::getAlias('@web') ?>/img/mike-hover-olf.png">
                                <span class="menu-caption menu-media">Categories <span class="caret caret-sidebar"></span></span></a>
                        </li>
                    </ul>
                </div>
            </nav>
            <button class="close-button" id="close-button">Close Menu</button>
            <div class="morph-shape" id="morph-shape" data-morph-open="M-1,0h101c0,0,0-1,0,395c0,404,0,405,0,405H-1V0z">
                <svg xmlns="http://www.w3.org/2000/svg" width="100%" height="100%" viewBox="0 0 100 800" preserveAspectRatio="none">
                <path d="M-1,0h101c0,0-97.833,153.603-97.833,396.167C2.167,627.579,100,800,100,800H-1V0z"/>
                </svg>
            </div>
        </div>

<div class="modal fade" id='modal_mobile_customer'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss='modal'><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <ul class="list-unstyled list-modal text-center">
                    <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/customers/add">Add</a></li>
                    <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/customers/new">New</a></li>
                    <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/customers/active">Active</a></li>
                    <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/customers/inactive">Inactive</a></li>
                    <li class="dropdown-header"><a href="#">Add</a></li>
                    <li class="dropdown-header"><a href="#">Happy</a></li>
                    <li class="dropdown-header"><a href="#">UnHappy</a></li>
                  <!--  <li class="dropdown-header"><a href="#">Fans</a></li>
                    <li class="dropdown-header"><a href="">Restricted</a></li>-->
                    <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/customers/">All</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id='modal-marketing'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss='modal'><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <ul class="list-unstyled list-modal text-center">
                   <!-- <li class="dropdown-header"><a href="marketing_sub_menu.html">Marketing</a></li>-->
                    <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/marketing/">Wizard</a></li>
                  <!--  <li class="dropdown-header"><a href="#">Web Integration</a></li>-->
                    <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/marketing/coupans">Coupons</a></li>
                    <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/marketing/giftcertificates">Gift Certificates</a></li>
                    <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/marketing/emailmarketing">Email Marketing</a></li>
                   <!-- <li class="dropdown-header"><a href="#">Social Media</a></li>-->
                </ul>
            </div>
        </div>
    </div>
</div>


        <div class="modal fade" id='modal-reports'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss='modal'><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <ul class="list-unstyled list-modal text-center">
                    <li class="dropdown-header"><a href="reports.html">Reports</a></li>
                    <li class="dropdown-header"><a href="#">Sales Reports</a></li>
                    <li class="dropdown-header"><a href="#">Customer Reports</a></li>
                   <!-- <li class="dropdown-header dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle='dropdown'>Marketing Reports</a>
                        <ul class="dropdown-menu dropdown-menu-reports">
                            <li><a href='#'>Marketing Report 1</a></li>
                            <li><a href='#'>Marketing Report 2</a></li>
                            <li><a href='#'>Marketing Report 3</a></li>
                            <li class="site-margins">
                                <div class="form-group">
                                    <select class="form-control site-margins">
                                        <option selected>Start Date</option>
                                        <option>1</option>
                                        <option>2</option>
                                    </select>
                                </div>
                            </li>
                            <li class="site-margins">
                                <div class="form-group">
                                    <select class="form-control site-margins">
                                        <option selected>End Date</option>
                                        <option>1</option>
                                        <option>2</option>
                                    </select>
                                </div>
                            </li>
                            <li class="current-reports"><a href="#"><i class="fa fa-circle-o"></i> Current Report</a></li>
                            <li class="btn-site-margins"><button class="btn btn-default btn-block">Search</button></li>
                        </ul>
                    </li>-->
                 <!--   <li class="dropdown-header"><a href="#">Financial Reports</a></li>
                    <li class="dropdown-header"><a href="#">Communication Reports</a></li>
                    <li class="dropdown-header"><a href="#">Appointment Reports</a></li>
                    <li class="dropdown-header"><a href="#">Alert Reports</a></li>-->
                </ul>
            </div>
        </div>
    </div>
</div>





<div class="modal fade" id='modal-settings'>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss='modal'><i class="fa fa-times"></i></button>
            </div>
            <div class="modal-body">
                <ul class="list-unstyled list-modal text-center">
                    <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/business-information/">Business</a></li>
                    <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/services/index">Services</a></li>
                    <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/staff/index">Staff</a></li>
                    <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/resource/index">Resources</a></li>
                    <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/booking-rules">Rules</a></li>
                    <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/booking-rules/advance">Advance</a></li>
                    <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/booking-rules/notification">Notification</a></li>
                 <!--   <li class="dropdown-header"><a href="interface.html">Interface</a></li>-->
                    <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/customize/index/">Customize</a></li>
                   <!-- <li class="dropdown-header"><a href="integration.html">Integration</a></li>-->
                    <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/booking-rules/privacy">Privacy</a></li>
                   <!-- <li class="dropdown-header"><a href="subscription.html"><i class="fa fa-gears"></i></a></li>-->
                </ul>
            </div>
        </div>
    </div>
</div>
    <div class="container-fluid container-main-wrapper">
            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 media-col1">
                    <a href="<?= Yii::getAlias('@web') ?>/index.php/">
                        <img src="<?= Yii::getAlias('@web') ?>/img/logo.png" class="logo">
                    </a>
                </div>

                <div class="col-lg-5 col-md-5 col-sm-5 text-center media-col2">
                 <!--   <input id="foo" name="foo" class="form-control" type="text" value="" size="55" disabled=""/>-->
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 media-col3">
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="list-inline pull-right list-menu-top">
                                <li style="visibility: hidden">
                                    <a href="#" id="customer_view_interface" class="link-img-view" data-toggle="modal" data-target="#customer_view" >
                                        <img src="<?= Yii::getAlias('@web') ?>/img/customer-view.png" class="img-view">
                                        <img src="<?= Yii::getAlias('@web') ?>/img/customer-view-hover.png" class="img-view-hover">
                                        Customer View</a>
                                </li>
                            <!--    <li>
                                    <a href="#" class="link-img-help">
                                        <img src="<?= Yii::getAlias('@web') ?>/img/help.png" class="img-help">
                                        <img src="<?= Yii::getAlias('@web') ?>/img/help-hover.png" class="img-help-hover">
                                        Help</a>
                                </li>-->
                            </ul>
                        </div>
                        <div class="col-lg-12 media-col4">
                            <ul class="list-inline pull-right list-icons-top">
                                <li class="li-icon-first" data-toggle="tooltip" data-placement="right" title="Appointment">
                                    <span class="badge badge-notify" id="appointment_notification" style="background:red;position:absolute;top:-10px"></span>
                                    <a href="#" class="dropdown-toggle" id="appointment_notification_link">

                                        <img src="<?= Yii::getAlias('@web') ?>/img/pen-hover.png" class="img-rect-pen">
                                        <img src="<?= Yii::getAlias('@web') ?>/img/pen.png" class="img-rect-pen-hover">
                                    </a>
                                </li>
                                <li class="li-icon-sec">
                                    <a href="#" class="dropdown-toggle" id="customer_notification_link" data-toggle="tooltip" data-placement="left" title="New Customers">
                                        <span class="badge badge-notify" id="customer_notification" style="background:red;position:absolute;top:-10px"></span>
                                        <img src="<?= Yii::getAlias('@web') ?>/img/hands.png" class="img-rect-hand">
                                        <img src="<?= Yii::getAlias('@web') ?>/img/hands-hover.png" class="img-rect-hand-hover">
                                    </a>
                                </li>
                                <li class="li-icon-third">
                                    <a href="#" id="appointment_top_review_link" style="position:relative" data-toggle="tooltip" data-placement="left" title="Reviews">
                                        <img src="<?= Yii::getAlias('@web') ?>/img/mike.png" class="img-rect-mike">
                                        <img src="<?= Yii::getAlias('@web') ?>/img/mike-hover.png" class="img-rect-mike-hover">
                                        <div id="review_notification_list" class="top_dashboard_notification dropdown-menu" style="position:absolute">
                                        </div>
                                    </a>
                                </li>
                             <!--   <li class="li-icon-forth">
                                    <a href="#">
                                        <img src="<?= Yii::getAlias('@web') ?>/img/sigs.png" class="img-rect-sigs">
                                        <img src="<?= Yii::getAlias('@web') ?>/img/sigs-hover.png" class="img-rect-sigs-hover">
                                    </a>
                                </li>-->
                            </ul>
                        </div>
                        <div id="appointment_notification_list" class="top_dashboard_notification dropdown-menu" style="display:none;padding:0">


                        </div>
                        <div id="customer_notification_list" class="top_dashboard_notification dropdown-menu">
                         </div>

                    </div>
                </div>
            </div>
        </div>
<nav class="navbar">
    <div class="container-fluid navbar-bg">
        <div class="navbar-header">
            <button class="menu-button" id="open-button">Open Menu</button>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav nav-primary">
                <li>
                    <a href="<?= Yii::getAlias('@web') ?>/index.php" class="menu-db <? if($this->context->route == 'site/index'){ echo 'active';} ?>">
                        <img src="<?= Yii::getAlias('@web') ?>/img/dashboard.png" class="icon-menu">
                        <img src="<?= Yii::getAlias('@web') ?>/img/dashboard-hover.png" class="icon-menu-hover-db">
                        <span class="menu-caption">Dashboard</span></a>
                </li>
                <li>
                    <a href="<?= Yii::getAlias('@web') ?>/index.php/customer-reserved-timeslots/index2" class="menu-cal <? if($this->context->route == 'customer-reserved-timeslots/index2'){ echo 'active';} ?>">
                        <img src="<?= Yii::getAlias('@web') ?>/img/calendar.png" class="icon-menu">
                        <img src="<?= Yii::getAlias('@web') ?>/img/calendar-hover.png" class="icon-menu-hover-cal">
                        <span class="menu-caption">Calendar</span></a>
                </li>
                <? if(($this->context->route == 'customers/add') || ($this->context->route == 'customers/new') || ($this->context->route == 'customers/active') || ($this->context->route == 'customers/inactive') )
                { $customer_men_actclass = 'active'; $customer_men_li = 'activelimen'; }else { $customer_men_li = '';$customer_men_actclass=''; } ?>
                <li class="dropdown dropdown-large dropdown-customer <?= $customer_men_li ?>">
                    <a href="#" id="customers" class="menu-cust dropdown-toggle <?= $customer_men_actclass ?> " data-toggle="dropdown">
                        <img src="<?= Yii::getAlias('@web') ?>/img/customers-hover.png" class="icon-menu">
                        <img src="<?= Yii::getAlias('@web') ?>/img/customers.png" class="icon-menu-hover-cust">
                        <span class="menu-caption-cust">Customers <span class="caret"></span></span></a>
                    <ul class="dropdown-menu dropdown-menu-large row dropdown-large-customer">
                        <li class="col-sm-2 media-column-sm">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/customers/add" <? if($this->context->route == 'customers/add'){ echo 'class="active"';} ?> ><i class="fa fa-plus"></i> Add</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-2 media-column-sm">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/customers/new"  <? if($this->context->route == 'customers/new'){ echo 'class="active"';} ?>><i class="fa fa-pencil"></i> New</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-2 media-column-sm">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/customers/active"  <? if($this->context->route == 'customers/active'){ echo 'class="active"';} ?>><i class="fa fa-circle"></i> Active</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-2 media-column-sm">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/customers/inactive"  <? if($this->context->route == 'customers/inactive'){ echo 'class="active"';} ?>><i class="fa fa-circle-o"></i> Inactive</a></li>
                            </ul>
                        </li>
                        <!--<li class="col-sm-2 media-column-sm">
                            <ul>
                                <li class="dropdown-header"><a href="#"><i class="fa fa-plus"></i> Add</a></li>
                            </ul>
                        </li>-->
                        <li class="col-sm-2 media-column-sm">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/customers/happy">&#9786; Happy</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-2 media-column-sm">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/customers/unhappy">&#9785; UnHappy</a></li>
                            </ul>
                        </li>
                      <!--  <li class="col-sm-2 media-column-sm">
                            <ul>
                                <li class="dropdown-header"><a href="#"><i class="fa fa-users"></i> Fans</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-2 media-column-sm">
                            <ul>
                                <li class="dropdown-header"><a href="#"><i class="fa fa-ban"></i> Restricted</a></li>
                            </ul>
                        </li> -->
                        <li class="col-sm-2 media-column-sm">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/customers/">All</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <? if(($this->context->route == 'marketing/index') || ($this->context->route == 'marketing/coupans') || ($this->context->route == 'marketing/giftcertificates') || ($this->context->route == 'marketing/emailmarketing'))
                { $customer_men_actclass = 'active'; $customer_men_li = 'activelimen'; }else { $customer_men_li = '';$customer_men_actclass=''; } ?>
                <li class="dropdown dropdown-large dropdown-marketing <?= $customer_men_li ?>">
                    <a href="#" id="marketing" class="menu-cust dropdown-toggle <?= $customer_men_actclass ?>" data-toggle="dropdown">
                        <img src="<?= Yii::getAlias('@web') ?>/img/marketing.png" class="icon-menu">
                        <img src="<?= Yii::getAlias('@web') ?>/img/marketing-hover.png" class="icon-menu-hover-cust icon-menu-hover-mark">
                        <span class="menu-caption-mark">Marketing <span class="caret"></span></span></a>
                    <ul class="dropdown-menu dropdown-menu-large row dropdown-large-marketing">
                        <li class="col-sm-2">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/marketing/" <? if($this->context->route == 'marketing/index'){ echo 'class="active"';} ?> ><i class="fa fa-magic"></i> Wizard</a></li>
                            </ul>
                        </li>
                        <!--<li class="col-sm-2">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/"><i class="fa fa-globe"></i> Web Integration</a></li>
                            </ul>
                        </li>-->
                        <li class="col-sm-2">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/marketing/coupans" <? if($this->context->route == 'marketing/coupans'){ echo 'class="active"';} ?> ><i class="fa fa-tag"></i> Coupons</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-2">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/marketing/giftcertificates" <? if($this->context->route == 'marketing/giftcertificates'){ echo 'class="active"';} ?> ><i class="fa fa-tag"></i> Gift Certificates</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-2">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/marketing/emailmarketing" <? if($this->context->route == 'marketing/emailmarketing'){ echo 'class="active"';} ?> ><i class="fa fa-envelope-o"></i> Email Marketing</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-2">
                            <ul>
                                <li class="dropdown-header"><a href="#"><i class="fa fa-signal  "></i> Social Media</a></li>
                            </ul>
                        </li>

                    </ul>
                </li>
                <? if(($this->context->route == 'reports/salesreports') || ($this->context->route == 'reports/customerreports') || ($this->context->route == 'reports/customerreports') || ($this->context->route == 'reports/unapprovedappointments'))
                { $customer_men_actclass = 'active'; $customer_men_li = 'activelimen'; }else { $customer_men_li = '';$customer_men_actclass=''; } ?>
                <li class="dropdown dropdown-large dropdown-reports <?= $customer_men_li ?>">
                    <a href="reports.html" id="reports" class="menu-cust dropdown-toggle <?= $customer_men_actclass ?>" data-toggle="dropdown">
                        <img src="<?= Yii::getAlias('@web') ?>/img/reports.png" class="icon-menu">
                        <img src="<?= Yii::getAlias('@web') ?>/img/reports-hover.png" class="icon-menu-hover-report icon-menu-hover-cust">
                        <span class="menu-caption">Reports <span class="caret"></span></span></a>
                    <ul class="dropdown-menu dropdown-menu-large row dropdown-large-reports">
                        <li class="col-sm-2 media-rep-sm">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/reports/salesreports" <? if($this->context->route == 'reports/salesreports'){ echo 'class="active"';} ?> ><i class="fa fa-file-text"></i> Sales Reports</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-2 media-rep-sm">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/reports/customerreports" <? if($this->context->route == 'reports/customerreports'){ echo 'class="active"';} ?> ><i class="fa fa-users"></i> Customer Reports</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-2 media-rep-sm">
                            <ul>
                                <li class="dropdown-header dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle='dropdown'><i class="fa fa-pie-chart"></i> Marketing Reports
                                      <!--  <ul class="dropdown-menu dropdown-menu-reports">
                                            <li><a href='#'>Marketing Report 1</a></li>
                                            <li><a href='#'>Marketing Report 2</a></li>
                                            <li><a href='#'>Marketing Report 3</a></li>
                                            <li class="site-margins">
                                                <div class="form-group">
                                                    <select class="form-control site-margins">
                                                        <option selected>Start Date</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                    </select>
                                                </div>
                                            </li>
                                            <li class="site-margins">
                                                <div class="form-group">
                                                    <select class="form-control site-margins">
                                                        <option selected>End Date</option>
                                                        <option>1</option>
                                                        <option>2</option>
                                                    </select>
                                                </div>
                                            </li>
                                            <li class="current-reports"><a href="#"><i class="fa fa-circle-o"></i> Current Report</a></li>
                                            <li class="btn-site-margins"><button class="btn btn-default btn-block">Search</button></li>
                                        </ul>-->
                                    </a>
                                </li>
                            </ul>
                        </li>
                      <!--  <li class="col-sm-2 media-rep-sm">
                            <ul>
                                <li class="dropdown-header"><a href="#"><i class="fa fa-signal"></i> Financial Reports</a></li>
                            </ul>
                        </li>-->
                        <li class="col-sm-2 media-rep-comm-sm">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/reports/unapprovedappointments" <? if($this->context->route == 'reports/unapprovedappointments'){ echo 'class="active"';} ?> ><i class="fa fa-comment"></i>Unapproved Appointments</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-2 media-app-rep-sm">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/reports/appointmentsreports" <? if($this->context->route == 'reports/appointmentsreports'){ echo 'class="active"';} ?> ><i class="fa fa-calendar"></i> Appointment Reports</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-2 media-rep-sm">
                            <ul>
                                <li class="dropdown-header"><a href="#"><i class="fa fa-bell"></i> Alert Reports</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <? if(($this->context->route == 'business-information/index') || ($this->context->route == 'services/index') || ($this->context->route == 'staff/index') || ($this->context->route == 'resource/index')
                    || ($this->context->route == 'booking-rules/index') || ($this->context->route == 'booking-rules/advance') || ($this->context->route == 'booking-rules/notification') || ($this->context->route == 'customize/index') || ($this->context->route == 'booking-rules/privacy'))
                { $customer_men_actclass = 'active'; $customer_men_li = 'activelimen'; }else { $customer_men_li = '';$customer_men_actclass=''; } ?>
                <li class="dropdown dropdown-large dropdown-reports <?= $customer_men_li ?>">
                    <a href="#"  id="settings" class="menu-setting dropdown-toggle <?= $customer_men_actclass ?> " data-toggle="dropdown">
                        <img src="<?= Yii::getAlias('@web') ?>/img/settings.png" class="icon-menu">
                        <img src="<?= Yii::getAlias('@web') ?>/img/settings-hover.png" class="icon-menu-hover-setting">
                        <span class="menu-caption">Settings</span></a>
                    <ul class="dropdown-menu dropdown-menu-large row dropdown-large-reports">

                        <li class="col-sm-2 media-set-sm">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/business-information/" <? if($this->context->route == 'business-information/index'){ echo 'class="active"';} ?> ><i class="fa fa-briefcase"></i> Business</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-2 media-set-sm">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/services/index" <? if($this->context->route == 'services/index'){ echo 'class="active"';} ?> ><i class="fa fa-gears"></i> Services</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-2 media-set-sm">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/offers/index" <? if($this->context->route == 'offers/index'){ echo 'class="active"';} ?> ><i class="fa fa-gears"></i> Offers</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-2 media-set-sm">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/staff/index" <? if($this->context->route == 'staff/index'){ echo 'class="active"';} ?> ><i class="fa fa-user"></i> Staff</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-2 media-set-sm">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/resource/index" <? if($this->context->route == 'resource/index'){ echo 'class="active"';} ?>><i class="fa fa-share-alt"></i> Resources</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-2 media-set-sm">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/booking-rules" <? if($this->context->route == 'booking-rules/index'){ echo 'class="active"';} ?> ><i class="fa fa-file-code-o"></i> Rules</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-2 media-set-sm">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/booking-rules/advance" <? if($this->context->route == 'booking-rules/advance'){ echo 'class="active"';} ?> ><i class="fa fa-file-code-o"></i> Advance</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-2 media-set-sm">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/booking-rules/notification" <? if($this->context->route == 'booking-rules/notification'){ echo 'class="active"';} ?> ><i class="fa fa-bell"></i> Notification</a></li>
                            </ul>
                        </li>
                        <!--<li class="col-sm-2 media-set-sm">
                            <ul>
                                <li class="dropdown-header"><a href="#"><i class="fa fa-square"></i> Interface</a></li>
                            </ul>
                        </li>-->
                        <li class="col-sm-2 media-set-sm">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/customize/index/" <? if($this->context->route == 'customize/index'){ echo 'class="active"';} ?> ><i class="fa fa-clipboard"></i> Customize</a></li>
                            </ul>
                        </li>
                        <!--<li class="col-sm-2 media-set-sm">
                            <ul>
                                <li class="dropdown-header"><a href="integration.html"><i class="fa fa-align-justify"></i> Integration</a></li>
                            </ul>
                        </li>-->
                        <li class="col-sm-2 media-set-sm">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/booking-rules/privacy" <? if($this->context->route == 'booking-rules/privacy'){ echo 'class="active"';} ?> ><i class="fa fa-lock"></i> Privacy</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <?php if (Yii::$app->user->identity->role ==1){?>
                <? if(($this->context->route == 'mycategories/add') || ($this->context->route == 'mycategories') )
                { $customer_men_actclass1 = 'active'; $customer_men_li1 = 'activelimen'; }else { $customer_men_li1 = '';$customer_men_actclass1=''; } ?>

                <li class="dropdown dropdown-large dropdown-reports <?= $customer_men_li1 ?>">
                    <a href="#"  id="categories" class="menu-setting dropdown-toggle <?= $customer_men_actclass1 ?> " data-toggle="dropdown">
                        <img src="<?= Yii::getAlias('@web') ?>/img/settings.png" class="icon-menu">
                        <img src="<?= Yii::getAlias('@web') ?>/img/settings-hover.png" class="icon-menu-hover-setting">
                        <span class="menu-caption">Categories</span></a>
                    <ul class="dropdown-menu dropdown-menu-large row dropdown-large-reports">


                        <li class="col-sm-2">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/mycategories/add"><i class="fa fa-align-justify"></i> Add Categories</a></li>
                            </ul>
                        </li>
                        <li class="col-sm-2">
                            <ul>
                                <li class="dropdown-header"><a href="<?= Yii::getAlias('@web') ?>/index.php/mycategories"<i class=""></i> View Categories</a></li>
                            </ul>
                        </li>
                    </ul>
                </li>
                <?php } ?>
            </ul>
            <ul class="nav navbar-nav navbar-right nav-user">
               <?php
               if (Yii::$app->user->isGuest) {
                   ?>
                   <li class="li-username"><a href="<?= Url::to(['site/login']) ?>" data-method="post">Login</a></li>
                   <?php }
               else { ?>
                   <li class="li-username" style="padding-top:25px "><?= Yii::$app->user->identity->username ?></a></li>
                <li class="li-username"> <a style="font-size: 29px;
    padding-top: 22px;
    padding-bottom: 21px;" title="Logout" href="<?= Url::to(['site/logout']) ?>" data-method="post"><i class="fa fa-sign-out" aria-hidden="true"></i></a></li>
               <!-- <li class="li-userimg"><img src="<?= Yii::getAlias('@web') ?>/img/user.png" class="image-user"></li>-->
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>










<div class="modal fade" id="customer_view">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="list-inline list-modal-header">
                    <li class="text-uppercase"><img src="<?= Yii::getAlias('@web') ?>/img/customer-view.png" alt="view" />
                        Here is the preview of your customer booking interface.</li>

                </ul>
            </div>
            <div class="modal-body">




                <iframe src="<?= str_replace('/admin/web', "/customer/web/index.php",Yii::getAlias('@web') );?>" style="height: 550px;width:100%"></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Oka Got it</button>
            </div>

        </div>
    </div>
</div>









<div class="container-fluid section-main">
    <div class="row">

    <?php
    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
        Modal::begin([
            'id' => 'modal_flash',
            'header' => '<h3 class="alert alert-' . $key . '">'.ucfirst($key).' : '.$message.'</h3>',
            'size' => 'modal-md',
        ]);

        Modal::end();
        $flasjJs= <<<EOF
        $('#modal_flash .modal-body').remove();
        $('#modal_flash').modal('show');
EOF;




        $this->registerJs($flasjJs);


    }
    ?>


    <?= $content ?>

</div>
</div>


    <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <p class="pull-right">Boocked <?= date('Y') ?> &copy; Copyrights. All Rights Reserved</p>
                </div>
            </div>
        </div>


<?php $this->endBody() ?>

</body>

</html>
<style>
    #geocomplete { width: 200px}
    .map_canvas {
        width: 600px;
        height: 400px;
        margin: 10px 20px 10px 0;
    }
    #multiple li {
        cursor: pointer;
        text-decoration: underline;
    }


</style>

<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBukxLQv96jNjV0b1VBaqKegksC6foPGRI&sensor=false&amp;libraries=places"></script>


<script src="http://boocked.com/admin/js/jquery.geocomplete.min.js"></script>

<script>
    $(function(){
        $("#geocomplete").geocomplete({
            map: ".map_canvas",
            details: "form ",
            markerOptions: {
                draggable: true
            }
        });

        $("#geocomplete").bind("geocode:dragged", function(event, latLng){
            $("input[name=lat]").val(latLng.lat());
            $("input[name=lng]").val(latLng.lng());
            $("#reset").show();
        });


        $("#reset").click(function(){
            $("#geocomplete").geocomplete("resetMarker");
            $("#reset").hide();
            return false;
        });

        $("#find").click(function(){
            $("#geocomplete").trigger("geocode");
        }).click();
    });
</script>
<style>
    .top_dashboard_notification{
        z-index: 11110;
    }
    .top_dashboard_notification h5 {
        margin: 0;
        padding: 10px 0;
    }
</style>
<?php $this->endPage() ?>

