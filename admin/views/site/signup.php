<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

if(@$id=Yii::$app->request->get('id')){
    $this->title = 'Update User';
}else{
    $this->title = 'Sign Up';
}
if (!\Yii::$app->user->isGuest) { ?>
<div class="site-signup">
    

<?php
        }else {
?>
<div class="site-signup">
    

    <div class="row col-sm-6" style="float: none; margin: 10px auto;">
        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
            <fieldset>
                <a href="#" style="display:block;text-align:center;margin-bottom: 50px;">
                <img src="<?= Yii::getAlias('@web') ?>/img/logo.png" class="logo">
                </a>
  

   
<!--                <legend>--><?//= Yii::t('app', 'User')?><!--</legend>-->
                <?= $form->field($model, 'username') ?>
                <?= $form->field($model, 'displayname') ?>
                <?= $form->field($model, 'email') ?>
              
                <div class="form-group field-businessinformation-country ">
                    <label for="businessinformation-country" class="control-label">Profession</label>
                        <select class="form-control" name="BusinessInformation[profession]" id="prosessions_list" >
                            <?php foreach($professions_list as $professions){?>
                                <option value="<?= $professions->profession_id ?>"><?= $professions->profession_name ?></option>
                            <?php } ?>
                        </select>
                    </div>

                
                

                    <div class="form-group field-businessinformation-country ">
                    <label for="businessinformation-country" class="control-label">Country</label>
                    <select name="BusinessInformation[country]" class="form-control" id="businessinformation-country">
                        <?php foreach ($countries as $country){ ?>
                            <option value="<?= $country->id ?>">
                                <?= $country->name ?>
                            </option>
                        <?php } ?>
                    </select>

                    <div class="help-block"></div>
                </div>
                <? //= $form->field($business_information, 'country') ?>

                <?
//                if(@$id=Yii::$app->request->get('id')){
//                    Html::activeTextInput()
//                    echo $form->field('', 'password')->passwordInput();
//                    echo $form->field('', 'password_repeat')->passwordInput();
//                } else {
//                    ?>
                    <?= $form->field($model, 'password')->passwordInput() ?>
                    <?= $form->field($model, 'password_repeat')->passwordInput() ?>
<!--                    --><?//= $form->field($model, 'user_group')->dropDownList(Array('thirdparty-cdl'=> 'Third party CDL', 'thirdparty-ncdl'=> 'Third party Standard' )) ?>
<!--                    --><?//
//                }
//                ?>



                <div class="col12">
                    <?
                    if(@$id=Yii::$app->request->get('id')){
                        echo "<input type='hidden' name='user_id' value='{$id}'>";
                        echo Html::submitButton('Update User', ['class' => 'btn btn-primary', 'name' => 'signup-button']);
                    } else {
                        echo Html::submitButton('Sign Up', ['class' => 'btn btn-primary', 'name' => 'signup-button']);
                    }
                    ?>

                </div>
                <p style="margin: 15px;">Already signup? Please <a style="color:orangered" href="<?= Yii::getAlias('@web') ?>/index.php/site/login">Click here</a> for Login</p>
            </fieldset>
            <?php ActiveForm::end(); ?>
        </div>

</div>

<script>

</script>
<!-- Button trigger modal -->
<?php } ?>