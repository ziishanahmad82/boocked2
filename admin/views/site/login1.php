<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Boocked Login';

?>

<div class="login col-sm-6" style="margin:0 auto;float:none;margin-top:10%">
    <?php
    $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
        'fieldConfig' => [
            'template' => "<div class=\"row\"><section><label class=\"col-sm-4 txt\">{label}:</label><div class=\"col-sm-8\"><label class=\"input\">{input}</label></div></section></div>\n<div class=\"col-lg-8\">{error}</div>"

            ,
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]);
    ?>
    <div class="form">
       <a href="#" style="display:block;text-align:center;margin-bottom: 20px;"><img src="<?= Yii::getAlias('@web') ?>/img/logo.png" class="logo"></a>
        <h1>Majid <?= $msg ?></h1>
        <!--<header id="log">Login</header>-->
        <fieldset>

            <?= $form->field($model, 'username') ?>

            <?= $form->field($model, 'password')->passwordInput() ?>




            <div class="col12"><?= Html::submitButton('Login', ['class' => 'bt btn btn-primary', 'name' => 'login-button']) ?></div>

            <p>You have no Account? Please <a style="color:orangered" href="<?= Yii::getAlias('@web') ?>/index.php/signup">Click here</a> for Signup</p>

        </fieldset>


    </div>

    <?php ActiveForm::end(); ?>

</div>
<?php

?>