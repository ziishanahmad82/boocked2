<?php
 /*@var $this yii\web\View */
$this->title = 'Dashboard';
use app\models\CustomerReservedTimeslots;
use app\models\Customers;
use app\models\Locations;
use app\models\Services;
use app\models\ThirdPartyUsers;
use app\models\Toughbooks;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;

?>


    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading header-border">
                    <h5 class="text-uppercase panel-title panel-body-title">appointments</h5>
                </div>
                <div class="panel-body text-center panel-center">
                    <h1 class="center-top"><?= $appointments_count ?></h1>
                    <p>appointments</p>
                    <div class="section-panel-footer">
                        <p>From Last Month</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading header-border">
                    <h5 class="text-uppercase panel-title panel-body-title">estimated sales</h5>
                </div>
                <div class="panel-body text-center panel-center">
                    <h1 class="center-top"><? if($estimated_sales){ echo $estimated_sales; }else { echo '0'; } ?></h1>
                    <p>sales</p>
                    <div class="section-panel-footer">
                        <p>From Last Month</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading header-border">
                    <h5 class="text-uppercase panel-title panel-body-title">new customers</h5>
                </div>
                <div class="panel-body panel-no-padding">
                    <div class="div-scroller">
                        <table class="table table-responsive table-striped">
                            <?php foreach($new_customers as $customers){ ?>
                                <tr><td><?= $customers->first_name ?></td></tr>
                            <?php } ?>

                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row row-custom">
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading header-border">
                    <h5 class="text-uppercase panel-title panel-body-title">satisfaction</h5>
                </div>
                <div class="panel-body text-center panel-center">
                    <h1 class="center-top"><?= $satisfaction ?>%</h1>
                    <p>satisfaction</p>
                    <div class="section-panel-footer">
                        <p>From <?= $total_reviews ?> Reviews</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading header-border">
                    <h5 class="text-uppercase panel-title panel-body-title">total appntmnt / month</h5>
                </div>
                <div class="panel-body text-center panel-center">
                    <h1 class="center-top">0</h1>
                    <p>appointments</p>
                    <div class="section-panel-footer">
                        <p>From Last Month</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading header-border">
                    <h5 class="text-uppercase panel-title panel-body-title">extra appointments</h5>
                </div>
                <div class="panel-body text-center panel-center">
                    <h1 class="center-top">5%</h1>
                    <p>appointments</p>
                    <div class="section-panel-footer">
                        <p>From Last Month</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
   <!-- <div class="row row-custom">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading header-border">
                    <h5 class="text-uppercase panel-title panel-body-title">appointments</h5>
                </div>
                <div class="panel-body panel-no-padding">
                    <div class="table-scroller">
                        <table class="table table-responsive table-striped table-months">
                            <tr>
                                <th>Mon</th>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                            </tr>
                            <tr>
                                <th>Tue</th>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                            </tr>
                            <tr>
                                <th>Wed</th>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                            </tr>
                            <tr>
                                <th>Thur</th>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                            </tr>
                            <tr>
                                <th>Fri</th>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                            </tr>
                            <tr>
                                <th>Sat</th>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                            </tr>
                            <tr>
                                <th>Sun</th>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                                <td>xx</td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>-->

   
<?php

   $sum_script ="$('h1.center-top').each(function () {
        $(this).css('visibility','visible');
        $(this).prop('Counter',0).animate({
                Counter: $(this).text()
            }, {
            duration: 400,
                easing: 'swing',
                step: function (now) {
                $(this).text(Math.ceil(now));
            }
            });
        }); ";
$this->registerJs($sum_script);
?>
       
       
