<?php
 /*@var $this yii\web\View */

use app\models\CustomerReservedTimeslots;
use app\models\Customers;
use app\models\Locations;
use app\models\Services;
use app\models\ThirdPartyUsers;
use app\models\Toughbooks;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;

?>
<style>
    #appointment_notification_list.dropdown-menu , #customer_notification_list.dropdown-menu  {

        background: #000 none repeat scroll 0 0;
        border-radius: 0;
        padding: 4px 5px !important;
        border-color:#000;
    }

    }
    #appointment_notification_list p , #customer_notification_list p{
          margin-bottom:0;
      }
    #appointment_notification_list small , #customer_notification_list small {
        font-size:12px !important;
        color:#777;
    }
    .notification_email{
        color:#6c6c6c;
        margin-bottom:4px;

    }
    #appointment_notification_list i , #customer_notification_list i {
        color:#FE7500;
        margin-right:5px;

    }
    .notification_head {
        background:#000;
        padding:0 5px;
    }
    .notification_head h5 {
        font-size:12px;
    }
    #customer_notification_list.dropdown-menu{
        padding:0;

    }
    .notification_content_row {
        max-width:330px;
        background:#fff;
        margin: 0px;
    }
   .notificatin_cover {
        padding:10px 8px;border-bottom:1px solid #cccccc
    }
    .notification_profile_image{
        float:left;
        width:20%;
    }
    .notification_profile_image img {
        margin-top:8px;
    }
    .notification_right_cont{
        float: left;
        width:80%;
        padding-left:10px;

    }
    .notification_boocked_on {
        color:#000000;
        font-weight:700;
        margin-bottom:0;
        font-size:12px;
    }
    .notification_booked_name {
        margin-bottom:0px;

    }
</style>
  <!--  profile_image123-->

              <div class="row notification_content_row" >
                  <?php if($type=='1'){ ?>
                  <div class="col-sm-12 notification_head">
                  <h5 style="color:#fff;">Booking Notfications</h5>
                  </div>
                  <div style="max-height:300px;overflow-y:scroll;padding:0" class="col-sm-12 mCustomScrollbar" id="notification_scroll">
                  <?php
                  if(count($model) > 0) {
                      foreach ($model as $appointments_noti) { ?>
                          <div class="col-sm-12 notificatin_cover" style="">
                              <p>
                                  <?php if (isset($appointments_noti['customers']['email'])) {
                                      echo '  ' . $appointments_noti['customers']['first_name'] . ' ' . $appointments_noti['customers']['last_name'];
                                  } else {
                                      echo 'Unknown';
                                  } ?></p>
                              <p class="notification_email"><i class="fa fa-envelope" aria-hidden="true"></i>
                                  <?php if (isset($appointments_noti['customers']['email'])) {
                                      echo '  ' . $appointments_noti['customers']['email'];
                                  } else {
                                      echo 'Unknown';
                                  } ?></p>
                              <p><i class="fa fa-user" aria-hidden="true"
                                    style="padding-left:2px;margin-right:8px;"></i><?php echo 'Booked for ' . $appointments_noti['services']['service_name'] . ' with ' . $appointments_noti['users']['username'] ?>
                              </p>
                              <?php $date = $appointments_noti->appointment_date;
                              ?><p><i class="fa fa-clock-o" aria-hidden="true"
                                      style="padding-left:2px;margin-right:8px;"></i><span
                                          style="font-size: 12px"><?php echo date('D, M d, Y', strtotime($date));
                                      echo '  ' . date('h:i A', strtotime($appointments_noti->appointment_start_time)); ?></span>
                              </p>
                              <p>
                                  <small><?php echo ' Booking Date ' . date('D, M d, Y @ h:i A', strtotime($appointments_noti->date_created)); ?></small>
                              </p>
                          </div>
                      <?php }
                  }else{
                  ?>
                      <div class="col-sm-12 notificatin_cover">No Notification Available</div>
                      <?php } ?>
                  </div>
                  <?php }else if($type=='2'){ ?>

                      <div class="col-sm-12 notification_head" >
                          <h5 style="color:#fff;">New Customers</h5>
                      </div>
                      <div style="max-height:300px;overflow-y:scroll;padding:0" class="col-sm-12" id="notification_scroll12">
                          <?php
                         if(count($model) > 0) {
                          foreach($model as $customer ){
                              if(isset($customer->email)){ ?>
                              <div class="col-sm-12 notificatin_cover">
                                  <div class="notification_profile_image" style="width:18%;float:left">
                                    <img src="<?= Yii::getAlias('@web') ?>/img/profile_image123.png" style="width:100%" />
                                  </div>
                                  <div class="notification_right_cont" style="width:80%;float:left">
                                      <p class="notification_booked_name"><?php echo $customer->first_name.'  '.$customer->last_name ?></p>
                                  <p class="notification_email"><!--<i class="fa fa-envelope" aria-hidden="true" style="margin-right:8px"></i>--><?php echo $customer->email; ?></p>
                                  <p class="notification_boocked_on"><!--<i class="fa fa-clock-o" aria-hidden="true" style="padding-left:1px;margin-right:6px;"></i>--><?php  echo ' Registered On '.date('D, M d, Y  h:i A', strtotime($customer->createdat)); ?></p>
                              </div>
                                  </div>
                          <?php }
                              } ?>
                           <?php }  else{
                             ?>
                             <div class="col-sm-12 notificatin_cover">No Notification Available</div>
                         <?php } ?>
                      </div>



                  <?php } else if($type=='3'){ ?>

                  <div class="col-sm-12 notification_head" >
                      <h5 style="color:#fff;">Reviews</h5>
                  </div>
                  <div style="max-height:300px;overflow-y:scroll;padding:0" class="col-sm-12" id="notification_scroll12">
                      <?php if(count($model) > 0) {
                          foreach ($model as $review) {

                              ?>
                              <div class="col-sm-12 notificatin_cover">
                                  <div class="notification_profile_image" style="width:18%;float:left">
                                      <img src="<?= Yii::getAlias('@web') ?>/img/profile_image123.png"
                                           style="width:100%"/>
                                  </div>
                                  <?php
                                  $customer = Customers::findOne($review->customer_id);
                                  //  print_r($customer);
                                  ?>
                                  <?php if ($review->review_status == '1') {
                                      $img_src = Yii::getAlias('@web') . '/img/poor.png';
                                  } else if ($review->review_status == '2') {
                                      $img_src = Yii::getAlias('@web') . '/img/bad.png';
                                  } else if ($review->review_status == '3') {
                                      $img_src = Yii::getAlias('@web') . '/img/fair.png';
                                  } else if ($review->review_status == '4') {
                                      $img_src = Yii::getAlias('@web') . '/img/good.png';
                                  } else if ($review->review_status == '5') {
                                      $img_src = Yii::getAlias('@web') . '/img/excelant.png';
                                  } ?>
                                  <div class="notification_right_cont" style="width:80%;float:left">
                                      <p class="notification_booked_name"><?php echo $customer->first_name . '  ' . $customer->last_name ?>
                                          <img src="<?= $img_src ?>" alt="<?= $customer->first_name ?>"
                                               style="border-radius:15px;width:30px"/></p>
                                      <p class="notification_email">
                                          <!--<i class="fa fa-envelope" aria-hidden="true" style="margin-right:8px"></i>--><?php echo $review->review_text; ?></p>
                                      <p class="notification_boocked_on">
                                          <!--<i class="fa fa-clock-o" aria-hidden="true" style="padding-left:1px;margin-right:6px;"></i>--><?php echo 'Review On ' . date('D, M d, Y  h:i A', strtotime($review->created_at)); ?></p>
                                  </div>
                              </div>
                              <?php // }
                          }
                      }else{
                          ?>
                          <div class="col-sm-12 notificatin_cover">No Review Available</div>
                      <?php }?>

                  </div>



                  <?php } ?>
              </div>
<?php

$script12 = '$("#notification_scroll").mCustomScrollbar({
                theme:"minimal"
            });
            $("#notification_scroll12").mCustomScrollbar({
                theme:"minimal"
            });
            ';

$this->registerJs($script12); ?>