<?php
 /*@var $this yii\web\View */
$this->title = 'Dashboard';
use app\models\CustomerReservedTimeslots;
use app\models\Customers;
use app\models\Locations;
use app\models\Services;
use app\models\ThirdPartyUsers;
use app\models\Toughbooks;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use app\models\CurrencyList;
use miloschuman\highcharts\Highcharts;
//use yii\jui\DatePicker;

?>
<style>
    #calendar {
        width: 200px;
        margin: 0 auto;
        font-size: 10px;
    }
    .fc-header-title h2 {
        font-size: .9em;
        white-space: normal !important;
    }
    .fc-view-month .fc-event, .fc-view-agendaWeek .fc-event {
        font-size: 0;
        overflow: hidden;
        height: 2px;
    }
    .fc-view-agendaWeek .fc-event-vert {
        font-size: 0;
        overflow: hidden;
        width: 2px !important;
    }
    .fc-agenda-axis {
        width: 20px !important;
        font-size: .7em;
    }

    .fc-button-content {
        padding: 0;
    }


</style>
<style>
    .datepicker.datepicker-inline{
        width:100%;

    }
    .datepicker.datepicker-inline .table-condensed {
        width:100%;

    }
    .datepicker th {
        font-size: 12px;

    }
    .datepicker td {
        font-size: 12px;
        border:1px solid #ebebeb;
        text-align:left;
        vertical-align:top;
        height: 50px;
    }
    .datepicker tr td:last-child {
        border-right:none;
    }
    .datepicker tr td:first-child{
        border-left:none;
    }
    .datepicker tr td.new, .datepicker tr td.old {
      background:#f3f3f5;
    }
    .table-condensed span.month, .table-condensed span.year{
        text-align: center;

    }
</style>

    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 no-padding-l media-xs-full">
            <div class="panel panel-default" style="border:none;">
                <div class="panel-body section-calendar">
                    <div class="page">
                        <div style="width:100%; max-width:600px; display:inline-block;">
                           <span style="display:none"><a href="month" class="btn btn-success" id="slecteddateshow"><?= date('m-Y') ?></a></span>
                            <div class="monthly" id="mycalendar"></div>
                            <? /*= yii2fullcalendar\yii2fullcalendar::widget(array(

                                'header' => [
                                    'left' => 'prev,next today',
                                    'center' => 'title',
                                    'right' => ' '
                                ],

                                'clientOptions' => [
                                    'selectable' => true,
                                    'selectHelper' => true,
                                    'droppable' => true,
                                    'editable' => true,
                                    //'drop' => new JsExpression($JSDropEvent),
                                    //'select' => new JsExpression($JSCode),
                               //     'dayRender' => new JsExpression($JSDayRender),
                               //     'dayClick' => new JsExpression($JSEventClick),
                               //     'eventClick'=> new JsExpression($JSslotClick),
                                    // 'viewRender' => new JsExpression($JSviewRender),
                                    'defaultDate' => date('Y-m-d'),
                                    'weekends' => false,
                                    'slotDuration' => '00:30:00',

                                    //'eventRender' => new JsExpression($JSEventRender),
                                ],
                            //    'events'=> $events,
                                //'ajaxEvents' => Url::toRoute(['/customer-reserved-timeslots/jsoncalendar'])
                            ));
                                 */   ?>
                            </div>
                    </div>
                    <style>
                        .hidepicker{
                            display:none;

                        }

                    </style>

                    <div id="datepicker_week" class="datepiclall hidepicker Weekdatepick" ></div>
                    <div id="datepicker_months" class="datepiclall Monthdatepick" ></div>
                    <div id="datepicker_year" class="datepiclall hidepicker Yeardatepick"></div>
                    <input type="hidden" value="" id="dates">
                    <div style="display:none">
                    <!--<input class="span2 changedatepicvalue"  id="datepicker_months"  type="text"  size="16">
                    <input class="span2 changedatepicvalue" id="datepicker_year"  type="text"  size="16">-->
                    </div>
                        <ul class="list-inline list-wmy">
                        <li><a href="#" class="changepicker">Week</a></li>
                        <li><a href="#" class="changepicker" >Month</a></li>
                        <li><a href="#" class="changepicker">Year</a></li>
                      

                    </ul>

                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/staff-icon.png"> <span class="staff-head">Staff</span></h5></li>
                        <li class="pull-right li-right"><i class="fa fa-plus plus-square"></i></li>
                    </ul>
                </div>
                <div class="panel-body staff-div">
                    <div class="row">
                        <?php foreach($staffList as $staff){ ?>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5 staffdp_cont">
                            <a href="#" class="dashboard_staff_list" data-staff="<?= $staff->id ?>">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm"><?= $staff->username ?>  </small></p>
                            </a>
                        </div>
                        <?php } ?>
                       <!-- <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff2.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff2.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff2.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>-->
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/analytics.png"> <span class="staff-head">analytics</span></h5></li>
                        <li class="pull-right li-right"><i class="fa fa-plus plus-square"></i></li>
                    </ul>
                </div>
                <div class="panel-body section-analytics">
                    <div class="row">
                        <div class="div-analytics">
                            <canvas id="canvas" height="450" width="600"></canvas>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 div-relative pull-right">
            <ul class="list-inline pull-right" style="display:none;">
                <li><a href="<?= Yii::getAlias('@web') ?>/index.php/services/index" data-toggle="tooltip" data-placement="left" data-original-title="Add Service"><i class="fa fa-plus fa-add-services"></i></a></li>
                <li><a href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="Help"><i class="fa fa-question fa-question-mark"></i></a></li>
            </ul>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9 media-xs-full" id="dashboardcontentload">
            <div style="overflow:hidden;">
                <div class="form-group" style="display: none;">
                    <div class="row">
                        <div class="col-md-8">
                            <div id="datetimepicker12"></div>
                        </div>
                    </div>
                </div>
            <ul class="nav nav-tabs nav-main dashboard_services_list">
                <?php $services = Services::find()->asArray()->where(['business_id'=> Yii::$app->user->identity->business_id])->all();
                //print_r($service_name); ?>
                <li class="active" style="border-left:1px solid #c4c4c4">
                         <a href="#"  class="dashboardapoints service_report"
                            data-service-id="0">All</a>
                </li>
                <?php
                $i=1;
                $closediv='';
                foreach($services as $service ){
                 if($i<4) { ?>
                     <li>
                         <a href="#<?= $service['service_id'] ?>"  class="dashboardapoints service_report"
                            data-service-id="<?= $service['service_id'] ?>"><?= $service['service_name'] ?></a>
                     </li>
                     <?php
                 }else {
                     if($i==4){
                         ?>
                <li><ul class="dropdown-menu" id="demo12" style="background: #fff; margin-left: 40px;">
                <?php
                     $closediv = '</ul></li>';
                }
            if($i>4){
                ?>
                <li><a href="#first_2" class="service_report"  data-service-id="<?= $service['service_id'] ?>"><?= $service['service_name'] ?></a></li>
              <?php  }
                 }
                    $i++; }?>
                <?php echo $closediv; ?>


                <li class="caret-dropdown">
                    <a href="#"  class="dropdown-toggle" data-target="#demo12" data-toggle="collapse" >
                        <i class="fa fa-caret-down"></i>
                    </a>

                </li>

               <!-- <li><ul class="dropdown-menu" id="demo12">
                    <li><a href="#">HTML</a></li>
                    <li><a href="#">CSS</a></li>
                    <li><a href="#">JavaScript</a></li>
                </ul></li>-->

            </ul>




<style>
    .selected{
        background:#fc7600;

    }
</style>

<?php
                $script = " $('.dashboardapoints').onclick(function () {
                var acv = $(this).href();
          
                });";

              //  $this->registerJs($script, Yii::POS_END, 'my-options');
?>

            <div class="tab-content" style="border:1px solid #C4C4C4;border-top:none">
                <div class="tab-pane fade in active" id="first_1">
                    <div class="panel-collapse collapse in active">
                        <div class="panel-body panel-main" id="service_stats" >
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">Total appointments</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top"><?= $appointments_count ?></h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">estimated sales</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top"><?php if($estimated_sales){ echo $estimated_sales; }else { echo '0'; } ?></h1>
                                            <p>sales</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">new customers</h5>
                                        </div>
                                 <?php
                                    if(!empty($new_customers)) { ?>
                                        <div class="panel-body panel-no-padding">
                                        <div class="div-scroller">
                                                <table class="table table-responsive table-striped">
                                       <?php  foreach ($new_customers as $customers) { ?>
                                            <tr>
                                                <td><?= $customers->first_name ?></td>
                                            </tr>
                                        <?php } ?>
                                                </table>
                                        </div>
                                        </div>
                                        <?php }else { ?>
                                       <div class="panel-body text-center panel-center">
                                         <h1 class="center-top">0</h1><p>Customers</p>
                                       </div>


                                  <?php  }?>
                                                  


                                    </div>
                                </div>
                            </div>
                            <div class="row row-custom">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">satisfaction</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">0%</h1>
                                            <p>satisfaction</p>
                                            <div class="section-panel-footer">
                                                <p>From 0 Reviews</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">Today Appointment </h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top"><?= $today_appointments_count ?></h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p style="color:transparent;">From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">extra appointments</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">0%</h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <?php
                $first_month_users = intval($first_month_user);
                $second_month_users = intval($second_month_user);
                $third_month_users = intval($third_month_user);
                $four_month_users = intval($four_month_user);
                $five_month_users = intval($five_month_user);
                $six_month_users = intval($six_month_user);
              //  $first_month.'", "'.$second_month.'", "'.$third_month.'", "'.$four_month.'", "'.$five_month.'", "'.$six_month
                
              $acaas  = array($first_month_user,$first_month_user,30,40,50);
                ?>
       <div class="col-sm-4"><?php       echo Highcharts::widget([
             'scripts' => [
                 'highcharts-3d',
             ],
                'options' => [
                    'chart' => ['type' => 'column',
              'options3d'=>['enabled'=>true,
                            'alpha'=>12,  //adjust for tilt
                            'beta'=>14,  // adjust for turn
                            'depth'=>70,
                            ]
            ],
                    'xAxis' => [
                        'categories' => [date("M", strtotime("-5 months")),date("M", strtotime("-4 months")),date("M", strtotime("-3 months")),date("M", strtotime("-2 months")),date("M", strtotime("-1 months")),date("M")],
                    ],

            'title' => ['text' => 'Appointment/Users'],

              'series' => [[
                           'type'=>'column',
                           'name'=>'Users',
                           'data'=>[$six_month_users,$five_month_users, $four_month_users,$third_month_users,$second_month_users,$first_month_users],
                          ],
                  [
                      'type'=>'column',
                      'name'=>'Sales',
                      'data'=>[intval($six_month),intval($five_month),intval($four_month),intval($third_month),intval($second_month),intval($first_month)],
                  ]
              ],
                ]
                ]);
         ?>
           </div>
               
            </div>
        </div>
    </div>
<div class="clearfix"></div>
        <style>
            h1.center-top {
                visibility:hidden;

            }
        </style>
   <!-- <button type="button" class="btn" data-toggle="modal" data-target="#customer_view" data-backdrop="static" id="customer_viewbut" >
        Player 1
    </button>-->
   <!-- <div class="modal fade" id="customer_view">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <ul class="list-inline list-modal-header">
                        <li class="text-uppercase"><i class="fa fa-dollar" style="color: #FC7600;"></i>
                            Here is the preview of your customer booking interface.</li>

                    </ul>
                </div>
                <div class="modal-body">
                <iframe src="http://localhost/boocked/customer/web/index.php" style="height: 550px;width:100%"></iframe>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Oka Got it</button>
                </div>

            </div>
        </div>
    </div>-->
  <?php  if($information_required=='yes'){ ?>
    <button type="button" class="btn" data-toggle="modal" data-target="#pay1" data-backdrop="static" id="blocked" style="display: none;">
        Player 1
    </button>
<div class="modal fade" id="pay1">
    <div class="modal-dialog modal-dialog-customer">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="list-inline list-modal-header">
                    <li class="text-uppercase"><i class="fa fa-dollar" style="color: #FC7600;"></i>Account Settings</li>
                    <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                        <a href=""><i class="fa fa-reply"></i></a>
                    </li>
                   <!-- <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                        <a href=""><i class="fa fa-plus"></i></a>
                    </li>
                    <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban"><i class="fa fa-ban"></i></a></li>
                    <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay"><i class="fa fa-dollar"></i></a></li>-->
                </ul>
            </div>
            <div class="modal-body">
                <?php Pjax::begin(); ?>

                <?php $form = ActiveForm::begin(['id'=>'signup_currency_timezone','action'=>Yii::getAlias('@web').'/index.php/site/signup_timezone_currency']); ?>
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <p>Currency</p>
                        <?php $currency_list =  CurrencyList::find()->all(); ?>
                        <div class="form-group">
                            <select class="form-control" name="Timezoneandcurrency[currency]">
                                <?php foreach($currency_list as $currencylist){ ?>
                                    <option value="<?= $currencylist->currency_id ?>"  ><?= $currencylist->country  ?> <?= $currencylist->code.'('.$currencylist->symbol.')'  ?></option>

                                <?php  }?>
                            </select>

                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <p>Profession</p>

                            <div class="form-group">
                            <select class="form-control" name="BusinessInformation[profession]" id="prosessions_list" >
                                <?php foreach($professions_list as $professions){?> 
                                <option <?php if($business_information->profession==$professions->profession_id){ echo 'selected'; }  ?> value="<?= $professions->profession_id ?>"><?= $professions->profession_name ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <p>Timezone</p>
                        <div class="form-group">
                            <select class="form-control" name="Timezoneandcurrency[timezone]">
                            <option value="9">Midway Island (GMT -11:20)</option>
                            <option value="1">Hawai (GMT -10:00)</option>
                            <option value="11">Tahiti (GMT -10:00)</option>
                            <option value="2">Alaska (GMT -09:00)</option>
                            <option value="180">GAMT - Gambier Time(GMT -9:00)</option>
                            <option value="3">Pacific Time (US & Canada) (GMT -08:00)</option>
                            <option value="12">Tijuana (GMT -08:00)</option>
                            <option value="13">Chihuahua (GMT -07:00)</option>
                            <option value="14">Baja Sur, Mexico(GMT -07:00)</option>
                            <option value="15">Mazatlan(GMT -07:00)</option>
                            <option value="4">Arizona (GMT -07)</option>
                            <option value="5">Mountain Time (US & Canada)(GMT -07:00)</option>
                            <option value="167">Pacific Daylight Time (US & Canada) (GMT -07:00)</option>
                            <option value="168">Mountain Daylight Time(GMT -06:00)</option>
                            <option value="165">San Jose(GMT -06:00)</option>
                            <option value="6">Central Time (US & Canada) (GMT -06:00)</option>
                            <option value="16">Mexico City (GMT -06:00)</option>
                            <option value="17">Monterrey(GMT -06:00)</option>
                            <option value="26">Saskatchewan, Canada(GMT -06:00)</option>
                            <option value="191">Central Standard Time (GMT -06:00)</option>
                            <option value="188">Cayman Islands time (GMT -05:00)</option>
                            <option value="172">Central Daylight Time(GMT -05:00)</option>
                            <option value="18">Bogota (GMT -05:00)</option>
                            <option value="19">Lima, Quito(GMT -05:00)</option>
                            <option value="7">Eastern Time (US & Canada) (GMT -05:00)</option>
                            <option value="8">Indiana (East)(GMT -05:00)</option>
                            <option value="150">Eastern Standard Time(EST)(GMT -05:00)</option>
                            <option value="21">Caracas(GMT -04:30)</option>
                            <option value="173">Venezuelan Standard Time(GMT -04:30)</option>
                            <option value="187">Atlantic Standard Time (GMT -04:00)</option>
                            <option value="194">Guyana Time(GMY -04:00)</option>
                            <option value="22">La Paz, Bolivia(GMT -04:00)</option>
                            <option value="23">Santiago(GMT -04:00)</option>
                            <option value="151">Atlantic Standard Time(AST)(GMT -04:00)</option>
                            <option value="171">Eastern Daylight Savings(GMT -04:00)</option>
                            <option value="176">Atlantic Standard Time (Philipsburg) (GMT -04:00)</option>
                            <option value="177">Atlantic Standard Time (Curacao)(GMT -04:00)</option>
                            <option value="163">Saint Kitts and Nevis(GMT -04:00)</option>
                            <option value="20">Atlantic Time (Canada) (GMT -04:00)</option>
                            <option value="24">Newfoundland (GMT -03:30)</option>
                            <option value="25">Buenos Aires, Georgetown (GMT -03:00)</option>
                            <option value="149">Montevideo (GMT -03:00)</option>
                            <option value="175">Brasilia, Sao Paulo (GMT -03:00)</option>
                            <option value="27">Azores (GMT -01:00)</option>
                            <option value="28">Cape Verde Is.(GMT -01:00)</option>
                            <option value="29">Casablanca (GMT 00:00)</option>
                            <option value="30">Dublin (GMT 00:00)</option>
                            <option value="31">Lisbon (GMT 00:00)</option>
                            <option value="32">London, Edinburgh (GMT 00:00)</option>
                            <option value="33">Monrovia (GMT 00:00)</option>
                            <option value="164">Western European Standard Time(WET)(GMT 00:00)</option>
                            <option value="166">Gambia Standard Time(GMT 00:00)</option>
                            <option value="148">Central European Time (CET) (GMT +01:00)</option>
                            <option value="152">West Africa Time(WAT)(GMT +01:00)</option>
                            <option value="155">British Summer Time(BST)(GMT +01:00)</option>
                            <option value="34">Amsterdam (GMT +01:00)</option>
                            <option value="35">Belgrade(GMT +01:00)</option>
                            <option value="36">Berlin, Bern(GMT +01:00)</option>
                            <option value="37">Bratislava(GMT +01:00)</option>
                            <option value="38">Brussels(GMT +01:00)</option>
                            <option value="39">Budapest(GMT +01:00)</option>
                            <option value="40">Copenhagen(GMT +01:00)</option>
                            <option value="41">Ljubljana(GMT +01:00)</option>
                            <option value="42">Madrid(GMT +01:00)</option>
                            <option value="43">Paris(GMT +01:00)</option>
                            <option value="44">Prague(GMT +01:00)</option>
                            <option value="45">Rome(GMT +01:00)</option>
                            <option value="46">Sarajevo(GMT +01:00)</option>
                            <option value="47">Skopje(GMT +01:00)</option>
                            <option value="48">Stockholm(GMT +01:00)</option>
                            <option value="49">Vienna(GMT +01:00)</option>
                            <option value="50">Warsaw(GMT +01:00)</option>
                            <option value="51">Zagreb(GMT +01:00)</option>
                            <option value="190">Oslo (GMT +01:00)</option>
                            <option value="186">Eastern European Summer Time(GMT +2:00)</option>
                            <option value="183">Amman(GMT +02:00)</option>
                            <option value="184">Beirut(GMT +02:00)</option>
                            <option value="181">Nicosia (GMT +2:00)</option>
                            <option value="52">Athens (GMT +02:00)</option>
                            <option value="53">Bucharest(GMT +02:00)</option>
                            <option value="54">Cairo(GMT +02:00)</option>
                            <option value="55">Johannesburg, Harare(GMT +02:00)</option>
                            <option value="56">Helsinki(GMT +02:00)</option>
                            <option value="57">Istanbul(GMT +02:00)</option>
                            <option value="58">Jerusalem(GMT +02:00)</option>
                            <option value="59">Kyiv(GMT +02:00)</option>
                            <option value="61">Riga(GMT +02:00)</option>
                            <option value="62">Sofia(GMT +02:00)</option>
                            <option value="63">Tallinn(GMT +02:00)</option>
                            <option value="64">Vilnius(GMT +02:00)</option>
                            <option value="147">Central European Summer Time (CEST) (GMT +02:00)</option>
                            <option value="178">South Africa Standard Time(GMT +02:00)</option>
                            <option value="179">Jordan Standard Time(Amman)(GMT +3:00)</option>
                            <option value="169">Eastern European Summer Time(GMT +3:00)</option>
                            <option value="156">Kaliningrad Time(GMT +03:00)</option>
                            <option value="153">East Africa Time(EAT)(GMT +03:00)</option>
                            <option value="154">Manama(GMT +03:00)</option>
                            <option value="65">Baghdad (GMT +03:00)</option>
                            <option value="66">Kuwait(GMT +03:00)</option>
                            <option value="67">Moscow, St. Petersburg, Volgograd(GMT +03:00)</option>
                            <option value="68">Nairobi(GMT +03:00)</option>
                            <option value="69">Riyadh(GMT +03:00)</option>
                            <option value="60">Minsk(GMT +03:00)</option>
                            <option value="174">Doha(GMT +03:00)</option>
                            <option value="70">Tehran (GMT +03:30)</option>
                            <option value="71">Abu Dhabi (GMT +04:00)</option>
                            <option value="72">Baku(GMT +04:00)</option>
                            <option value="73">Muscat(GMT +04:00)</option>
                            <option value="74">Tbilisi(GMT +04:00)</option>
                            <option value="75">Yerevan(GMT +04:00)</option>
                            <option value="157">Samara Time(GMT +04:00)</option>
                            <option value="170">Moscow Daylight Time(GMT +4:00)</option>
                            <option value="185">Port Louis(GMT +04:00)</option>
                            <option value="189">Paraguay Standard Time (GMT -04:00)</option>
                            <option value="76">Kabul (GMT +04:30)</option>
                            <option value="78">Karachi, Islamabad(GMT +05:00)</option>
                            <option value="79">Tashkent(GMT +05:00)</option>
                            <option value="80">Calcutta, Mumbai, Delhi (GMT +05:30)</option>
                            <option value="162">Sri Lanka Time(SLT)(GMT +05:30)</option>
                            <option value="161">Nepal Time (NPT)(GMT +05:45)</option>
                            <option value="81">Almaty (GMT +06:00)</option>
                            <option value="82">Dhaka, Astana(GMT +06:00)</option>
                            <option value="83">Novosibirsk(GMT +06:00)</option>
                            <option value="77">Ekaterinburg (GMT +06:00)</option>
                            <option value="84">Rangoon (GMT +06:30)</option>
                            <option value="85">Bangkok, Hanoi (GMT +07:00)</option>
                            <option value="86">Jakarta(GMT +07:00)</option>
                            <option value="87">Krasnoyarsk(GMT +07:00)</option>
                            <option value="192">Indochina Time (GMT +07:00)</option>
                            <option value="193">China Time Zone</option>
                            <option value="182">Hong Kong (GMT +08:00)</option>
                            <option value="88">Beijing, Chongqing (GMT +08:00)</option>
                            <option value="89">Hong Kong, Manila(GMT +08:00)</option>
                            <option value="90">Irkutsk(GMT +08:00)</option>
                            <option value="91">Kuala Lumpur(GMT +08:00)</option>
                            <option value="92">Perth(GMT +08:00)</option>
                            <option value="93">Singapore(GMT +08:00)</option>
                            <option value="94">Taipei(GMT +08:00)</option>
                            <option value="95">Ulaan Bataar(GMT +08:00)</option>
                            <option value="96">Urumqi(GMT +08:00)</option>
                            <option value="97">Osaka, Sapporo, Japan (GMT +09:00)</option>
                            <option value="98">Seoul(GMT +09:00)</option>
                            <option value="99">Tokyo(GMT +09:00)</option>
                            <option value="100">Yakutsk(GMT +09:00)</option>
                            <option value="101">Adelaide (GMT +09:30)</option>
                            <option value="102">Darwin(GMT +09:30)</option>
                            <option value="103">Brisbane (GMT +10:00)</option>
                            <option value="104">Canberra(GMT +10:00)</option>
                            <option value="105">Guam, Port Moresby(GMT +10:00)</option>
                            <option value="106">Hobart(GMT +10:00)</option>
                            <option value="107">Melbourne(GMT +10:00)</option>
                            <option value="108">Sydney(GMT +10:00)</option>
                            <option value="158">Vladivostok Time(GMT +10:00)</option>
                            <option value="159">Magadan Time(GMT +10:00)</option>
                            <option value="160">Kamchatka Time(GMT +12:00)</option>
                            <option value="109">Auckland, Wellington (GMT +12:00)</option>
                            <option value="110">Fiji(GMT +12:00)</option>
                            <option value="10">Samoa(GMT +13:00)</option>
                            </select>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="modal-body-inner-tabs">

                        <div class="row">
                            <div class="col-lg-12">
                                <ul class="list-inline pull-right">
                                    <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;">Cancel</button></li>
                                    <li><input type="submit" class="btn btn-main"  style="padding-right: 19px; padding-left: 19px;" value="Next"></li>
                                    <button class="btn btn-default btn-next" id="business_currency_next" style="display:none" type="button">Next</button>


                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php ActiveForm::end(); ?>
            <?php Pjax::end(); ?>


        </div>
    </div>
</div>
<div class="modal fade" id="pay2">
    <div class="modal-dialog modal-dialog-customer">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="list-inline list-modal-header" style="margin-bottom:0">
                    <li class="text-uppercase"><i class="fa fa-briefcase" style="color: #FC7600;"></i> Business Information</li>
                </ul>
            </div>
            <div class="modal-body">
                <?php Pjax::begin(); ?>

                <?php $form = ActiveForm::begin(['id'=>'business_information_signup','enableAjaxValidation' => true, 'validationUrl' => Yii::getAlias('@web').'/index.php/business-information/validate','action'=>Yii::getAlias('@web').'/index.php/business-information/update']); ?>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <?= $form->field($business_information, 'business_name')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-lg-6 col-md-6">
            <?= $form->field($business_information, 'business_phone')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <!--   <div class="row">
           <div class="col-lg-12">
               <p class="text-muted pull-right" style="margin-top: -9px;"><small>Customer restrictions of the selected membership will be applied automatically.</small></p>
           </div>
       </div> -->
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <!--   <textarea class="form-control" rows="2" placeholder="Note against payment"></textarea>-->
                <?= $form->field($business_information, 'business_description')->textarea(['rows' => 2]) ?>
                <p class="text-danger pull-right"><small>Please do not enter html tags</small></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <?= $form->field($business_information, 'address')->textInput(['maxlength' => true]) ?>
        </div>

    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <div class="form-group field-businessinformation-country required has-success">
                <label for="businessinformation-country" class="control-label">Country</label>
                <?= $business_information->country ?>
                <select name="BusinessInformation[country]" class="form-control" id="businessinformation-country">
                    <?php foreach ($countries as $country){ ?>
                        <option <?php if($business_information->country==$country->id){ echo 'selected'; }?> value="<?= $country->id ?>">
                            <?= $country->name ?>
                        </option>
                    <?php } ?>
                </select>

                <div class="help-block"></div>
            </div>
            <? //= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
            <div class="col-lg-6 col-md-6 col-sm-6" style="padding:0">
                <div class="form-group field-businessinformation-state ">
                    <label for="businessinformation-state" class="control-label">State</label>
                    <select name="BusinessInformation[state]" class="form-control" id="businessinformation-state">
                        <?php foreach ($states as $state){ ?>
                            <option <?php if($business_information->state==$state->id){ echo 'selected'; }?> value="<?= $state->id ?>">
                                <?= $state->name ?>
                            </option>
                        <?php } ?>
                    </select>

                    <div class="help-block"></div>
                </div>

            </div>
            <? //= $form->field($business_information, 'state')->textInput(['maxlength' => true]) ?>

    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6" >
            <? //= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
            <div class="form-group field-businessinformation-city">
                <label for="businessinformation-state" class="control-label">City</label>
                <select name="BusinessInformation[city]" class="form-control" id="businessinformation-city" >
                    <?php foreach ($cities as $city){ ?>
                        <option <?php if($business_information->city==$city->id){ echo 'selected'; }?> value="<?= $city->id ?>">
                            <?= $city->name ?>
                        </option>
                    <?php } ?>
                </select>

                <div class="help-block"></div>
            </div>


        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
            <?= $form->field($business_information, 'zip')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <?= Html::submitButton($business_information->isNewRecord ? 'Next' : 'Next', ['class' => $business_information->isNewRecord ? 'btn btn-main pull-right' : 'btn btn-main pull-right']) ?>
                <div class="clearfix"></div>
                <?php ActiveForm::end(); ?>
                <?php Pjax::end(); ?>
                <button class="btn btn-default btn-next" type="button" id="business_information_next" style="display:none ">Next</button>
    </div>

    </div>
    </div>
   <!-- </div>-->

    <div class="modal fade" id="pay3">
        <div class="modal-dialog modal-dialog-customer">
            <div class="modal-content">
                <div class="modal-header">
                    <ul class="list-inline list-modal-header">
                        <li class="text-uppercase"><i class="fa fa-dollar" style="color: #FC7600;"></i> Services Offered</li>

                    </ul>
                </div>
                <div class="modal-body">
                    <?php Pjax::begin(); ?>

                    <?php $form = ActiveForm::begin(['id'=>'services_signup','action'=>Yii::getAlias('@web').'/index.php/site/signup_service_create']); ?>
                    <div class="row">
                       <div class="col-sm-12"><p style="color: #FC7600;">General name for service (Example Service, Class, Tour etc)</p></div>
                        <div class="col-lg-6 col-md-6">

                            <?= $form->field($serviceCommonNameModel, 'singular_name')->textInput(['maxlength' => true]) ?>

                        </div>
                        <div class="col-lg-6 col-md-6">
                            <?= $form->field($serviceCommonNameModel, 'plural_name')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="row">

                            <div class="col-sm-4 reset_field_cont">
                                <?= $form->field($NewService, 'service_name')->textInput(['maxlength' => true]) ?>
                            </div>
                        <div class="col-sm-2 reset_field_cont">
                                <?= $form->field($NewService, 'service_price')->textInput(['maxlength' => true]) ?>
                            </div>
                        <div class="col-sm-2 reset_field_cont">
                            <?= $form->field($NewService, 'service_duration')->textInput(['maxlength' => true])->label('Duration') ?>
                        </div>
                        <div class="col-sm-2" style="padding-top: 25px;">
                            <?= Html::submitButton($NewService->isNewRecord ? 'Add' : 'Update', ['class' => $business_information->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>



                    </div>
                    <?php ActiveForm::end(); ?>
                    <?php Pjax::end(); ?>

                    <?php Pjax::begin(['id'=>'srvices_signuplist']); ?>
                    <div class="row">
                    <?php    if(!empty($allServices)){ ?>
                        <div class="col-lg-12" style="height:160px;overflow-y:scroll">
                        <?php foreach($allServices as $allService){
                        ?>
                        <div class="col-lg-12" style="padding:0;border-bottom:1px solid">
                            <div class="col-xs-4">
                                <?= $allService->service_name ?>
                            </div>
                            <div class="col-xs-3"><?= $allService->service_price ?></div>
                            <div class="col-xs-3"><?= $allService->service_duration ?></div>
                            <div class="col-xs-2"><span onclick="signupservice_Delete(this,<?= $allService->service_id ?>)" class="glyphicon glyphicon-trash"></span></div>
                        </div>
                       <?php }
                        ?>
                        </div>
                       <?php } ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="alert alert-danger" style="display: none;" id="service_error">
                        Please Add atleast one service.
                    </div>
                    <?php Pjax::end(); ?>
                    <br/>
                    <button class="btn btn-main" type="button" id="service_signupcountcheck" style="float:right">Next</button>
                    <button class="btn btn-default btn-next" id="service_signup_next" type="button" style="display:none;">Next</button>
                    <div class="clearfix"></div>

                </div>

            </div>
        </div>
    </div>
    <div class="modal fade" id="pay4">
        <div class="modal-dialog modal-dialog-customer">
            <div class="modal-content">
                <div class="modal-header">
                    <ul class="list-inline list-modal-header">
                        <li class="text-uppercase"><i class="fa fa-dollar" style="color: #FC7600;"></i> Staff</li>

                    </ul>
                </div>
                <div class="modal-body">
                    <?php Pjax::begin(); ?>

                    <?php $form = ActiveForm::begin(['id'=>'staff_signup','action'=>Yii::getAlias('@web').'/index.php/site/signup_staff_create']); ?>
                    <div class="row">
                        <div class="col-sm-12"><p style="color: #FC7600;">General name for Staff (Example Staff, Provider, Teacher, Chiropractor, trainer, Therapist etc)</p></div>
                        <div class="col-lg-6 col-md-6">

                            <?= $form->field($staffCommonNameModel, 'staff_singular')->textInput(['maxlength' => true]) ?>

                        </div>
                        <div class="col-lg-6 col-md-6">
                            <?= $form->field($staffCommonNameModel, 'staff_plural')->textInput(['maxlength' => true]) ?>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-sm-4 reset_field_cont">
                            <?= $form->field($NewStaff, 'username')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-2 reset_field_cont">
                            <?= $form->field($NewStaff, 'mobile_phone')->textInput(['maxlength' => true])->label('Phone') ?>
                        </div>
                        <div class="col-sm-4 reset_field_cont">
                            <?= $form->field($NewStaff, 'email')->textInput(['maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-2" style="padding-top: 25px;">
                            <?= Html::submitButton($NewService->isNewRecord ? 'Add' : 'Update', ['class' => $business_information->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                        </div>



                    </div>
                    <?php ActiveForm::end(); ?>
                    <?php Pjax::end(); ?>
                    <?php Pjax::begin(['id'=>'staff_listbox']); ?>
                    <br/>
                    <div class="row">
                        <div class="col-lg-12" style="max-height:150px;overflow-y:scroll">
                            <?php foreach($staffList as $staffone){
                                ?>
                                <div class="col-lg-12" style="padding:0;border-bottom:1px solid">
                                    <div class="col-xs-4">
                                        <?= $staffone->username ?>
                                    </div>
                                    <div class="col-xs-3"><?= $staffone->mobile_phone ?></div>
                                    <div class="col-xs-3"><?= $staffone->email ?></div>
                                    <div class="col-xs-2" style="text-align:right"><?php if(Yii::$app->user->identity->id != $staffone->id){ ?><span onclick="signupStaff_Delete(this,<?= $staffone->id ?>)" class="glyphicon glyphicon-trash"></span><?php } ?></div>
                                </div>
                            <?php }
                            ?>
                        </div>
                    </div>
                    <?php Pjax::end(); ?>
                    <br/>
                    <div class="col-lg-12"><p><small>You can always add staff later. Click next to add your business hours.</small></p></div>
                    <br/>
                    <button class="btn btn-main btn-next" type="button" style="float:right">Next</button>
                    <div class="clearfix"></div>
                </div>


            </div>
        </div>
    </div>
    <div class="modal fade" id="pay5">
        <div class="modal-dialog modal-dialog-customer">
            <div class="modal-content">
                <div class="modal-header">
                    <ul class="list-inline list-modal-header">
                        <li class="text-uppercase"><i class="fa fa-dollar" style="color: #FC7600;"></i> Business Hours</li>

                    </ul>
                </div>
                <div class="modal-body">
                    <form action="#" id="signup_business_hours">
                    <div class="col-sm-12"><div class="col-sm-12"><input type="checkbox" id="signup_same_all"> Same For all</div></div>
                    <div class="col-sm-12">
                        <div class="col-sm-2">Sun</div><div class="col-sm-3"><input type="text" value="09:00 AM" required name="staffsun[from][]" class="timepicker3 form-control" id="fromsamefirst"> </div><div class="col-sm-1"> to </div><div class="col-sm-3"><input type="text" value="05:00 PM"  required  name="staffsun[to][]" class="timepicker3 form-control" id="tosamefirst"></div>
                    </div>

                    <div class="col-sm-12">
                        <div class="col-sm-2">Mon</div><div class="col-sm-3"><input type="text" required value="09:00 AM" name="staffmon[from][]" class="timepicker3 form-control fromsamerem"> </div><div class="col-sm-1"> to </div><div class="col-sm-3"><input type="text" value="05:00 PM" required name="staffmon[to][]" class="timepicker3 form-control tosamerem"></div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-2">Tue</div><div class="col-sm-3"><input type="text" required value="09:00 AM" name="stafftue[from][]" class="timepicker3 form-control fromsamerem"> </div><div class="col-sm-1"> to </div><div class="col-sm-3"><input type="text" value="05:00 PM" required name="stafftue[to][]" class="timepicker3 form-control tosamerem"></div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-2">Wed</div><div class="col-sm-3"><input type="text" required value="09:00 AM" name="staffwed[from][]" class="timepicker3 form-control fromsamerem"> </div><div class="col-sm-1"> to </div><div class="col-sm-3"><input type="text" value="05:00 PM" required name="staffwed[to][]" class="timepicker3 form-control tosamerem"></div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-2">Thurs</div><div class="col-sm-3"><input type="text" required value="09:00 AM" name="staffthurs[from][]" class="timepicker3 form-control fromsamerem"> </div><div class="col-sm-1"> to </div><div class="col-sm-3"><input type="text" value="05:00 PM" required name="staffthurs[to][]" class="timepicker3 form-control tosamerem"></div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-2">Fri</div><div class="col-sm-3"><input type="text" required value="09:00 AM" name="stafffri[from][]" class="timepicker3 form-control fromsamerem"> </div><div class="col-sm-1"> to </div><div class="col-sm-3"><input type="text" value="05:00 PM" required name="stafffri[to][]" class="timepicker3 form-control tosamerem"></div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-2">Sat</div><div class="col-sm-3"><input type="text" required value="09:00 AM" name="staffsat[from][]" class="timepicker3 form-control fromsamerem"> </div><div class="col-sm-1"> to </div><div class="col-sm-3"><input type="text" value="05:00 PM" required  name="staffsat[to][]" class="timepicker3 form-control tosamerem"></div>
                    </div>
                    <div class="col-sm-12">
                        <div class="col-sm-6 pull-right"><input type="submit" class="btn btn-main" required style="float:right" value="Next"></div>
                        <button class="btn btn-default btn-next" id="business_hour_next" style="display:none" type="button">Next</button>
                    </div>

                    <div class="clearfix"></div>
                    </form>
                </div>

            </div>
        </div>
    </div>

<?php    $script2 = "


$('body').on('beforeSubmit', '#business_information_signup', function () {

    var form = $(this);
    var sc = form.attr('action');
    alert(sc);
    // return false if form still have some validation errors
    if (form.find('.has-error').length) {
    return false;
    }
    // submit form
    $.ajax({
    url: form.attr('action'),
    type: 'post',
    data: form.serialize(),
    success: function (response) {
    if(response==1){
  $('#business_information_next').click();

    }else {
    alert('not updated');

    }
    }
    });
    return false;
    });
    $('body').on('beforeSubmit', '#services_signup', function () {

    var form = $(this);
    var sc = form.attr('action');
    alert(sc);
    // return false if form still have some validation errors
    if (form.find('.has-error').length) {
    return false;
    }
    // submit form
    $.ajax({
    url: form.attr('action'),
    type: 'post',
    data: form.serialize(),
    success: function (response) {
    if(response==1){
  $.pjax.reload({container:'#srvices_signuplist',timeout:60000});
  $('.reset_field_cont input').val('');

    }
    }
    });
    return false;
    });
    
     $('body').on('beforeSubmit', '#staff_signup', function () {

    var form = $(this);
    var sc = form.attr('action');
    alert(sc);
    // return false if form still have some validation errors
    if (form.find('.has-error').length) {
    return false;
    }
    // submit form
    $.ajax({
    url: form.attr('action'),
    type: 'post',
    data: form.serialize(),
    success: function (response) {
    if(response==1){
  $.pjax.reload({container:'#staff_listbox',timeout:60000});
  $('.reset_field_cont input').val('');

    }else {
    alert('not updated');

    }
    }
    });
    return false;
    });
    $('#service_signupcountcheck').click(function(){
    
    $.ajax({
            url: '".Yii::getAlias('@web')."/index.php/site/signup_service_count',
           
            success: function (response) {
                if(response==1){
                  $('#service_signup_next').click();


                }else {
                $('#service_error').css('display','block');
                  setTimeout(function () {
                        $('#service_error').hide();
                    }, 3000);
                }
            }
        });
    
    });
     $('.timepicker3').timepicker({
        showPeriod: true

    });
    
    $('#signup_same_all').change(function(){
    if(this.checked){
            var fromhour = $('#fromsamefirst').val();
            var tohour = $('#tosamefirst').val();
            $('.fromsamerem').val(fromhour);
            $('.tosamerem').val(tohour);
            $('.fromsamerem').attr('readonly',true);
            $('.tosamerem').attr('readonly',true);
    }else {
            $('.fromsamerem').attr('readonly', false)
            $('.tosamerem').attr('readonly', false);
    }
});
 $('#signup_business_hours').submit(function () {
 var form = $(this);
 $.ajax({
    url:'".Yii::getAlias('@web')."/index.php/site/signupbusiness_hours',
    type: 'post',
    data: form.serialize(),
    success: function (response) {
    if(response==1){
  $('#business_hour_next').click();

    }
    }
    });

 return false;
 event.preventDefault();
 });

$('body').on('beforeSubmit', '#signup_currency_timezone', function () {

    var form = $(this);
    var sc = form.attr('action');
    alert(sc);
    // return false if form still have some validation errors
    if (form.find('.has-error').length) {
    return false;
    }
    // submit form
    $.ajax({
    url: form.attr('action'),
    type: 'post',
    data: form.serialize(),
    success: function (response) {
    if(response==1){
  $('#business_currency_next').click();

    }else {
    alert('not updated');

    }
    }
    });
    return false;
    });
    
     $('#mycalendar').monthly({
        mode: 'event',
      
    });
    
  
  
  
  
  $('#dashboardcontentload).load('".Yii::getAlias('@web')."/index.php/customer-reserved-timeslots/dashboardCalendar');
  
  
  
  
  
  
  
 

  
  
  
  
    
    
    ";
    
    $this->registerJs($script2);
echo 'data: ["'.$first_month.'", "'.$second_month.'", "'.$third_month.'", "'.$four_month.'", "'.$five_month.'", "'.$six_month.'"]';
    ?>

    <button class="btn btn-main" type="button" id="service_signupcountcheck">Next</button>
    <button class="btn btn-default btn-next" id="service_signup_next" type="button" style="display:none;">Next</button>
<script>
    function signupservice_Delete(xor,service_id){
        $.ajax({
            url: '<?= Yii::getAlias('@web')?>/index.php/site/signup_service_delete',
            type: 'post',
            data: {'sr_id':service_id},
            success: function (response) {
                if(response==1){
                    $.pjax.reload({container:'#srvices_signuplist',timeout:60000});


                }
            }
        });

        
    }
    function signupStaff_Delete(xor,service_id){
        $.ajax({
            url: '<?= Yii::getAlias('@web')?>/index.php/site/signup_staff_delete',
            type: 'post',
            data: {'sr_id':service_id},
            success: function (response) {
                if(response==1){
                    $.pjax.reload({container:'#staff_listbox',timeout:60000});


                }
            }
        });


    }

   
</script>
    <style>
        #signup_business_hours .col-sm-12{
            padding-top:2px;
            padding-bottom:2px;

        }

    </style>
 <?php }
    ?>

<script>
    function dateajaxcall(selectdate,type) {

        st_id = $('.staffdp_cont.selected .dashboard_staff_list').attr('data-staff');
        if(!st_id){
            st_id =0;
        }
        service_id = $('.dashboard_services_list li.active a').attr('data-service-id');
        $.ajax({
            url: '<?=Yii::getAlias('@web') ?>/index.php/site/dasboardservicesummary',
            type: 'post',
            data:{'service_id':service_id,'st_id':st_id,'selectdate':selectdate,'type':type},
        success: function (response) {
            if(response){
                $('#service_stats').html(response)

            }else {
                alert('not updated');

            }
        }
    });

    }

</script>

  <?php  $script =  "
$('#demo12 li').click(function() {
    $(this).addClass('active').siblings().removeClass('active');
    });
    $('#blocked').click();
    
      $('.service_report').click(function(event){
     event.preventDefault();
     $('.dashboard_services_list li').removeClass('active');
     $(this).parent('li').addClass('active');
     service_id = $(this).attr('data-service-id');
     st_id = $('.staffdp_cont.selected .dashboard_staff_list').attr('data-staff');
      selectdate = $('#slecteddateshow').text();
      type = $('#slecteddateshow').attr('href');
     if(!st_id){
     st_id=0;
     }
      $.ajax({
    url: '".Yii::getAlias('@web')."/index.php/site/dasboardservicesummary',
    type: 'post',
    data:{'service_id':service_id,'st_id':st_id,'selectdate':selectdate,'type':type},
    success: function (response) {
    if(response){
  $('#service_stats').html(response)

    }else {
    alert('not updated');

    }
    }
    });
    
    });
    
    $('.changepicker').click(function(event){
    event.preventDefault();
      textcont =  $(this).text();
    $('.datepiclall').addClass('hidepicker');
    $('.'+textcont+'datepick').removeClass('hidepicker');
    });
    
    $('.dashboard_staff_list').click(function(event){
    event.preventDefault();
    $('.staffdp_cont').removeClass('selected');
     $(this).parent('.staffdp_cont').addClass('selected');
     st_id = $(this).attr('data-staff');
     selectdate = $('#slecteddateshow').text();
      type = $('#slecteddateshow').attr('href');
     service_id = $('.dashboard_services_list li.active a').attr('data-service-id');
    $.ajax({
    url: '".Yii::getAlias('@web')."/index.php/site/dasboardservicesummary',
    type: 'post',
    data:{'service_id':service_id,'st_id':st_id,'selectdate':selectdate,'type':type},
    success: function (response) {
    if(response){
  $('#service_stats').html(response)

    }else {
    alert('not updated');

    }
    }
    });
    
    
      });
    //   st_id = $('.staffdp_cont.selected .dashboard_staff_list').attr('data-staff');
    //     service_id = $('.dashboard_services_list li.active a').attr('data-service-id');

   $('#datepicker_week').on('changeDate', function() {
     value =  $('#datepicker_week').datepicker('getFormattedDate');
     firstDate = moment(value, 'DD-MM-YYYY').day(1).format('DD-MM-YYYY');
    lastDate =  moment(value, 'DD-MM-YYYY').day(7).format('DD-MM-YYYY');
    $('#slecteddateshow').text(firstDate + '   -    '+ lastDate);
    $('#slecteddateshow').attr('href','week');
    
      dateajaxcall(firstDate,'week'); 
       
        
    
});



$('#datepicker_year').on('changeDate', function() {
     value =  $('#datepicker_year').datepicker('getFormattedDate');
   
    $('#slecteddateshow').text(value);
     $('#slecteddateshow').attr('href','year');
    
      dateajaxcall(value,'year'); 
    
    
});
$('#datepicker_months').on('changeDate', function() {
     value =  $('#datepicker_months').datepicker('getFormattedDate');
    $('#slecteddateshow').text(value);
        $('#slecteddateshow').attr('href','month');
    
      dateajaxcall(value,'month'); 
    
});
   
  $('#businessinformation-country').change(function(){
  var country_id =  $('#businessinformation-country option:selected').val();
 $.ajax({
url:'".Yii::getAlias('@web')."/index.php/business-information/address_states',
type: 'post',
data: {'country_id':country_id,'type':'country'},
success: function (response) {
    if(response){
    //    alert('changed');
        $('#businessinformation-state').html(response);
        $('#businessinformation-city').html('');

    }else {
        alert('not updated');

    }
}
});
 });
 
  $('#businessinformation-state').change(function(){
  var state_id =  $('#businessinformation-state option:selected').val();
 $.ajax({
url:'".Yii::getAlias('@web')."/index.php/business-information/address_states',
type: 'post',
data: {'state_id':state_id,'type':'state'},
success: function (response) {
    if(response){
    //    alert('changed');
        $('#businessinformation-city').html(response);

    }else {
        alert('not updated');

    }
}
});
 }); 
  $('h1.center-top').each(function () {
        $(this).css('visibility','visible');
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        }, {
            duration: 3000,
            easing: 'swing',
            step: function (now) {
                $(this).text(Math.ceil(now));
            }
        });
    });
 
  
    ";

  $this->registerJsFile('https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.6.4/js/bootstrap-datepicker.min.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
        $this->registerJs($script); ?>
<?php $dependjs = '
 
 var monthNames = [ "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" ];

        var str=new Array();
        var varDate = new Date();
        var month = varDate.getMonth()+1;
        var currentDay = varDate.getDate();


        for ( var i = 5; i >= 0; i--) {

            var now = new Date();
            now.setDate(1);
            var date = new Date(now.setMonth(now.getMonth() - i));
            var datex = ("0" + date.getDate()).slice(-2) + "-" + ("0" + (date.getMonth() + 1)).slice(-2) + "-" + date.getFullYear();

            str[i]= monthNames[date.getMonth()];
        }
//alert(str);
 
 
 var randomScalingFactor = function () {
        return Math.round(Math.random() * 100)
        };
        var lineChartData = {
       // labels: ["January", "February", "March", "April", "May", "June", "July"],
       labels:str,
        datasets: [
        {
        label: "Appointments",
        fillColor: "rgba(220,220,220,0.2)",
        strokeColor: "rgba(220,220,220,1)",
        pointColor: "rgba(220,220,220,1)",
        pointStrokeColor: "#fff",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(220,220,220,1)",
        data: ["'.$first_month.'", "'.$second_month.'", "'.$third_month.'", "'.$four_month.'", "'.$five_month.'", "'.$six_month.'"]
        },
       {
        label: "New Customers",
        fillColor: "rgba(151,187,205,0.2)",
        strokeColor: "rgba(151,187,205,1)",
        pointColor: "rgba(151,187,205,1)",
        pointStrokeColor: "#fff",
        pointHighlightFill: "#fff",
        pointHighlightStroke: "rgba(151,187,205,1)",
          data: ["'.$first_month_user.'", "'.$second_month_user.'", "'.$third_month_user.'", "'.$four_month_user.'", "'.$five_month_user.'", "'.$six_month_user.'"]
        } 
        ]

        }

        window.onload = function () {
        var ctx = document.getElementById("canvas").getContext("2d");
        window.myLine = new Chart(ctx).Line(lineChartData, {
        responsive: true
        });
        }'

;
$this->registerJs($dependjs); ?>


      




  <?php    $asdasdas='
        $("#datepicker_months").datepicker( {
	    format: "mm-yyyy",
	    viewMode: "months", 
	    minViewMode: "months"
	    });
	    
	     $("#datepicker_year").datepicker( {
	    format: "yyyy",
	    viewMode: "years", 
	    minViewMode: "years"
	    });
	     $("#datepicker_week").datepicker( {
	    format: "dd-mm-yyyy",
	    viewMode: "0", 
	    minViewMode: "0"
	    });
	    
        
        
        
        
        
        $(".changedatepicvalue").change(function(){
     //  alert("aaaa");
        curentdate = $(this).val()
        $("#datevalue").val(curentdate);
        });
        
        
        
        
        
       ' ;


$this->registerJs($asdasdas);
?>

