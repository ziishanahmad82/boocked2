<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;


$this->title = 'Change Password';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="site-changepassword">
   <style>
       .smallfont {
           font-size:10px;
           padding: 6px 9px

       }
       </style>


    <?php $form = ActiveForm::begin(['id'=>'changepassword-form'
        , 'options'=>['class'=>'form-horizontal'],
        'fieldConfig'=>[
            'template'=>"<p>{label}</p>\n<div class=\"col-lg-12\">
                        {input}</div>\n<div class=\"col-lg-12\">
                        {error}</div>",
           'labelOptions'=>['class'=>'col-lg-2 control-label'],
        ],
    ]); ?>

    <?= $form->field($model,'oldpass',['inputOptions'=>[
        'placeholder'=>'Old Password'
    ]])->passwordInput()->label(false) ?>

    <?= $form->field($model,'newpass',['inputOptions'=>[
        'placeholder'=>'New Password'
    ]])->passwordInput()->label(false) ?>

    <?= $form->field($model,'repeatnewpass',['inputOptions'=>[
        'placeholder'=>'Repeat New Password'
    ]])->passwordInput()->label(false) ?>

    <div class="form-group">
        <div class="col-lg-12">
            <a href="#" style="font-size:10px" onclick="hidepaswdform(event)" >Cancel</a>
            <?= Html::submitButton('Change password',[
               'class'=>'btn btn-primary smallfont'
             ]) ?>

        </div>
        <span id="passwrd_response"></span>
    </div>
    <?php ActiveForm::end(); ?>
</div>