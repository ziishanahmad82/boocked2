<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Boocked Login';

?>

<div class="login col-sm-6" style="margin:0 auto;float:none;margin-top:10%;">
    <?php
    $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'col-lg-12 '],
        'fieldConfig' => [
            'template' => "<div class=\"row\"><section><label class=\"col-lg-12 txt\">{label}:</label><div class=\"col-lg-12 \">{input}</div></section></div>\n<div class=\"col-lg-12\">{error}</div>"

            ,
            'labelOptions' => ['class' => 'control-label'],
        ],
    ]);
    ?>
    <div class="form">
      
<?php
if (isset($_GET['id'])){
    echo "<h1>Please check your email</h1>";
}
?>



        <!--<header id="log">Login</header>-->
        <fieldset>
 <a href="#" style="display:block;text-align:center;margin-bottom: 40px;"><img src="<?= Yii::getAlias('@web') ?>/img/logo.png" class="logo"></a>
 

            <?= $form->field($model, 'username') ?>

            <?= $form->field($model, 'password')->passwordInput() ?>




            <div class="col12"><?= Html::submitButton('Login', ['class' => 'bt btn btn-primary', 'name' => 'login-button']) ?></div>

            <p style="margin: 15px;">You have no Account? Please <a style="color:orangered;" href="<?= Yii::getAlias('@web') ?>/index.php/signup">Click here</a> for Signup</p>



    <?php ActiveForm::end(); ?>

</div>
<?php

?>