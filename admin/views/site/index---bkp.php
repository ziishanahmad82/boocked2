<?php
/* @var $this yii\web\View */
$this->title = 'Dashboard';
use app\models\CustomerReservedTimeslots;
use app\models\Customers;
use app\models\Locations;
use app\models\ThirdPartyUsers;
use app\models\Toughbooks;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;



$this->registerJs(
'
$(document).ready( function(){
        $("#div-select-toughbook1").hide();
    });
    $("#status1").change(function(){
        if($("#status1").val()=="assign-toughbook"){
            $("#div-select-toughbook1").show();
        }
    });


    $(document).ready( function(){
        $("#div-select-toughbook2").hide();
    });
    $("#status2").change(function(){
        if($("#status2").val()=="assign-toughbook"){
            $("#div-select-toughbook2").show();
        }
    });
'
);
?>

<div class="site-index">






    <div class="body-content">
        <div style="float: right;">

            <span class="cdl1 cdl_btn3"><a href="http://lh/dmv/customer/web/index.php?customer_action=new" target="_blank">New Appointment</a></span>
            <span class="cdl1 cdl_btn3"><a href="http://lh/dmv/customer/web/index.php?customer_action=edit" target="_blank">Change/Cancel Appointment</a></span>


        </div>
        <br/><br/><br/>

        <h3>Dashboard</h3>


    <?php ob_start(); ?>























        <?
        $today_date = date("Y-m-d");


        $reservations = CustomerReservedTimeslots::find()
            ->select(['*'])
            ->where([
                'test_date' => $today_date,
                'confirmed' => '1',
                'third_user_id' => '0'
            ])
            ->andFilterWhere([
                'or',
                ['like', 'status', 'checked in'],
                ['like', 'status', 'Waiting for check in'],
                ['like', 'status', 'Assigned to toughbook'],
            ])
            ->orderBy([
           'status'=>SORT_ASC,
            ])
            ->all();
        if ($reservations) {
            ?>
            <div style="width: 100%;background-color: #ddd;">
            Total : 60<br>
            Completed : 40<br>
            Untested : 20<br>
            </div>

            <div class="grid-view">
                <table class="table table-striped table-bordered dashboard-table">
                    <thead>
                    <tr>
<!--                        <th>-->
<!--                            Customer Id-->
<!--                        </th>-->
                        <th>
                            DL#
                        </th>
                        <th>
                            First Name
                        </th>
                        <th>
                            Last Name
                        </th>
                        <th>
                            Test Type
                        </th>
                        <th>
                            Time
                        </th>
<!--                        <th>-->
<!--                            Test-->
<!--                        </th>-->
                        <th>
                            Status
                        </th>
<!--                        <th>-->
<!--                            Change Status-->
<!--                        </th>-->
                        <th style="width: 100px;">
                            Actions
                        </th>

                    </tr>

                    </thead>

                    <tbody>

                    <?
                    foreach ($reservations as $reservation) {
                        $customer = Customers::find()
                            ->select(['*'])
                            ->where([
                                'customer_id' => $reservation->customer_id,
                                //'confirmed' => '1',
                                //'third_user_id' => '0'
                            ])
                            ->one();

                        $location = Locations::find()
                            ->select(['location_name'])
                            ->where([
                                'location_id' => $reservation->location_id,
                            ])
                            ->one();

                        $mysqldate = strtotime( $reservation->test_date );
                        $ourdate = date( 'm-d-Y', $mysqldate );
                        ?>

                            <tr>
<!--                                <td>-->
<!--                                    --><?//= $customer->customer_id ?>
<!--                                </td>-->
                                <td>
                                    <?= $customer->learners_permit_number ?>
                                </td>
                                <td>
                                    <?= $customer->first_name ?>
                                </td>
                                <td>
                                    <?= $customer->last_name ?>
                                </td>
                                <td>
                                    <?= $reservation->test_type; ?>
                                </td>
                                <td>
                                    <?=$reservation->test_start_time?>
                                </td>
<!--                                <td>-->
<!--                                    --><?//=$customer->test_type?><!----><?//if($customer->sub_test_type){ echo "&gt;&gt; ".$customer->sub_test_type;}?>
<!--                                </td>-->
                                <td>
                                    <?=$reservation->status?>

                                </td>
<!--                                <td>-->
<!--                                    --><?//=CustomerReservedTimeslots::customer_status_dropdown($customer->status, 'status1')?>
<!--                                    <div id="div-select-toughbook1">-->
<!---->
<!--                                    </div>-->
<!---->
<!--                                </td>-->
                                <td>

                                    <?php $form = ActiveForm::begin(
                                        [
                                            'action' => Yii::getAlias("@web") . '/index.php/customer-reserved-timeslots/savestatus',

                                        ]
                                    ); ?>

                                    <input type="hidden" name="timeslot_id" value="<?=$reservation->timeslot_id?>">
                                    <?
                                    if($reservation->status=="Checked in"){
                                        ?>
                                        <select name="toughbook-select1" id="toughbook-select1">
                                            <option value="0">Select a toughbook</option>
                                            <?
                                            $toughbooks=Toughbooks::find()
                                                ->select(['*'])
                                                ->where([
                                                    'toughbook_status' => 'active',
                                                ])
                                                ->all();
                                            foreach($toughbooks as $toughbook){
                                                ?>
                                                <option value="<?=$toughbook->toughbook_id?>"><?=$toughbook->toughbook_code?></option>
                                                <?
                                            }
                                            ?>
                                        </select>

                                        <input type="hidden" name="assign_toughbook" value="yes">
                                        <input type="submit" class="btn-success" value="Go"/>

                                        <?
                                    } else if($reservation->status=="Waiting for check in"){
                                        ?>
                                        <?=CustomerReservedTimeslots::customer_status_dropdown($customer->status, 'status1')?>
                                        <input type="hidden" name="assign_toughbook" value="no">
                                        <input type="submit" class="btn-success" value="Go"/>

                                        <?
                                    } else if($reservation->status=="Assigned to toughbook"){
                                        print "";
                                    } else {
                                        print "";
                                    }
                                    ?>

                                    <?php ActiveForm::end(); ?>

                                </td>
                            </tr>

                        <?
                    }
                    ?>
                    </tbody>

                </table>
            </div>
            <?
        } else {
            ?>
            <h4>No records found!</h4>
            <?
        }

        ?>

        <?php $var1 = ob_get_clean(); ?>





        <?php ob_start(); ?>




        <?
        $today_date = date("Y-m-d");


        $reservations = CustomerReservedTimeslots::find()
            ->select(['*'])
            ->where([
                'test_date' => $today_date,
                'confirmed' => '1',

            ])
            ->andWhere(['>', 'third_user_id', '0'])
            ->andFilterWhere([
                'or',
                ['like', 'status', 'checked in'],
                ['like', 'status', 'Waiting for check in'],
                ['like', 'status', 'Assigned to toughbook'],
            ])
            ->all();
        if ($reservations) {
            ?>
            <div class="grid-view">
                <table class="table table-striped table-bordered dashboard-table">
                    <thead>
                    <tr>
<!--                        <th>-->
<!--                            Customer Id-->
<!--                        </th>-->
                        <th>
                            DL#
                        </th>
                        <th>
                            First Name
                        </th>
                        <th>
                            Last Name
                        </th>
                        <th>
                            Test Type
                        </th>
                        <th>
                            3rd party username
                        </th>
                        <th>
                            Time
                        </th>
                        <!--                        <th>-->
                        <!--                            Test-->
                        <!--                        </th>-->
                        <th>
                            Status
                        </th>
                        <!--                        <th>-->
                        <!--                            Change Status-->
                        <!--                        </th>-->
                        <th>
                            Actions
                        </th>

                    </tr>

                    </thead>

                    <tbody>

                    <?
                    foreach ($reservations as $reservation) {
                        $customer = Customers::find()
                            ->select(['*'])
                            ->where([
                                'customer_id' => $reservation->customer_id,
                                //'confirmed' => '1',
                                //'third_user_id' => '0'
                            ])
                            ->one();

                        $location = Locations::find()
                            ->select(['location_name'])
                            ->where([
                                'location_id' => $reservation->location_id,
                            ])
                            ->one();

                        $third_user = ThirdPartyUsers::find()
                            ->select(['user_name'])
                            ->where([
                                'id' => $reservation->third_user_id,
                            ])
                            ->one();

                        $mysqldate = strtotime( $reservation->test_date );
                        $ourdate = date( 'm-d-Y', $mysqldate );
                        ?>

                        <tr>
<!--                            <td>-->
<!--                                --><?//= $customer->customer_id ?>
<!--                            </td>-->
                            <td>
                                <?= $customer->learners_permit_number ?>
                            </td>
                            <td>
                                <?= $customer->first_name ?>
                            </td>
                            <td>
                                <?= $customer->last_name ?>
                            </td>
                            <td>
                                <?= $reservation->test_type ?>
                            </td>
                            <td>
                                <?= $third_user->user_name; ?>
                            </td>
                            <td>
                                <?=$reservation->test_start_time?>
                            </td>
                            <!--                                <td>-->
                            <!--                                    --><?//=$customer->test_type?><!----><?//if($customer->sub_test_type){ echo "&gt;&gt; ".$customer->sub_test_type;}?>
                            <!--                                </td>-->
                            <td>
                                <?=$reservation->status?>

                            </td>
                            <!--                                <td>-->
                            <!--                                    --><?//=CustomerReservedTimeslots::customer_status_dropdown($customer->status, 'status1')?>
                            <!--                                    <div id="div-select-toughbook1">-->
                            <!---->
                            <!--                                    </div>-->
                            <!---->
                            <!--                                </td>-->
                            <td>

                                <?php $form = ActiveForm::begin(
                                    [
                                        'action' => Yii::getAlias("@web") . '/index.php/customer-reserved-timeslots/savestatus',

                                    ]
                                ); ?>

                                <input type="hidden" name="timeslot_id" value="<?=$reservation->timeslot_id?>">
                                <?
                                if($reservation->status=="Checked in"){
                                    ?>
                                    <select name="toughbook-select1" id="toughbook-select1">
                                        <option value="0">Select a toughbook</option>
                                        <?
                                        $toughbooks=Toughbooks::find()
                                            ->select(['*'])
                                            ->where([
                                                'toughbook_status' => 'active',
                                            ])
                                            ->all();
                                        foreach($toughbooks as $toughbook){
                                            ?>
                                            <option value="<?=$toughbook->toughbook_id?>"><?=$toughbook->toughbook_code?></option>
                                            <?
                                        }
                                        ?>
                                    </select>

                                    <input type="hidden" name="assign_toughbook" value="yes">
                                    <input type="submit" class="btn-success" value="Go"/>

                                    <?
                                } else if($reservation->status=="Waiting for check in"){
                                    ?>
                                    <?=CustomerReservedTimeslots::customer_status_dropdown($customer->status, 'status1')?>
                                    <input type="hidden" name="assign_toughbook" value="no">
                                    <input type="submit" class="btn-success" value="Go"/>

                                    <?
                                } else if($reservation->status=="Assigned to toughbook"){
                                    print "";
                                } else {
                                    print "";
                                }
                                ?>

                                <?php ActiveForm::end(); ?>

                            </td>
                        </tr>

                        <?
                    }
                    ?>
                    </tbody>

                </table>
            </div>
            <?
        } else {
            ?>
            <h4>No records found!</h4>
            <?
        }

        ?>



        <?php $var2 = ob_get_clean(); ?>




        <?php ob_start(); ?>




        <?
        $today_date = date("Y-m-d");


        $reservations = CustomerReservedTimeslots::find()
            ->select(['*'])
            ->where([
                'test_date' => $today_date,
                'confirmed' => '1',
                'third_user_id' => '0'

            ])->andWhere(['!=', 'status', 'checked in'])
            ->andWhere(['!=', 'status', 'Waiting for check in'])
            ->andWhere(['!=', 'status', 'Assigned to toughbook'])
            ->all();
        if ($reservations) {
            ?>
            <div class="grid-view">
                <table class="table table-striped table-bordered dashboard-table">
                    <thead>
                    <tr>
<!--                        <th>-->
<!--                            Customer Id-->
<!--                        </th>-->
                        <th>
                            DL#
                        </th>
                        <th>
                            First Name
                        </th>
                        <th>
                            Last Name
                        </th>
                        <th>
                            Location
                        </th>
                        <th>
                            Time
                        </th>
                        <!--                        <th>-->
                        <!--                            Test-->
                        <!--                        </th>-->
                        <th>
                            Status
                        </th>
                        <!--                        <th>-->
                        <!--                            Change Status-->
                        <!--                        </th>-->


                    </tr>

                    </thead>

                    <tbody>

                    <?
                    foreach ($reservations as $reservation) {
                        $customer = Customers::find()
                            ->select(['*'])
                            ->where([
                                'customer_id' => $reservation->customer_id,
                                //'confirmed' => '1',
                                //'third_user_id' => '0'
                            ])
                            ->one();

                        $location = Locations::find()
                            ->select(['location_name'])
                            ->where([
                                'location_id' => $reservation->location_id,
                            ])
                            ->one();

                        $mysqldate = strtotime( $reservation->test_date );
                        $ourdate = date( 'm-d-Y', $mysqldate );
                        ?>

                        <tr>
<!--                            <td>-->
<!--                                --><?//= $customer->customer_id ?>
<!--                            </td>-->
                            <td>
                                <?= $customer->learners_permit_number ?>
                            </td>
                            <td>
                                <?= $customer->first_name ?>
                            </td>
                            <td>
                                <?= $customer->last_name ?>
                            </td>
                            <td>
                                <?= $location->location_name ?>
                            </td>
                            <td>
                                <?=$reservation->test_start_time?>
                            </td>
                            <!--                                <td>-->
                            <!--                                    --><?//=$customer->test_type?><!----><?//if($customer->sub_test_type){ echo "&gt;&gt; ".$customer->sub_test_type;}?>
                            <!--                                </td>-->
                            <td>
                                <?=$reservation->status?>

                            </td>
                            <!--                                <td>-->
                            <!--                                    --><?//=CustomerReservedTimeslots::customer_status_dropdown($customer->status, 'status1')?>
                            <!--                                    <div id="div-select-toughbook1">-->
                            <!---->
                            <!--                                    </div>-->
                            <!---->
                            <!--                                </td>-->

                        </tr>

                        <?
                    }
                    ?>
                    </tbody>

                </table>
            </div>
            <?
        } else {
            ?>
            <h4>No records found!</h4>
            <?
        }

        ?>



        <?php $var3 = ob_get_clean(); ?>





        <?php ob_start(); ?>




        <?
        $today_date = date("Y-m-d");


        $reservations = CustomerReservedTimeslots::find()
            ->select(['*'])
            ->where([
                'test_date' => $today_date,
                'confirmed' => '1',


            ])->andWhere(['!=', 'status', 'checked in'])
            ->andWhere(['!=', 'status', 'Waiting for check in'])
            ->andWhere(['!=', 'status', 'Assigned to toughbook'])
            ->andWhere(['>', 'third_user_id', '0'])
            ->all();
        if ($reservations) {
            ?>
            <div class="grid-view">
                <table class="table table-striped table-bordered dashboard-table">
                    <thead>
                    <tr>
<!--                        <th>-->
<!--                            Customer Id-->
<!--                        </th>-->
                        <th>
                            DL#
                        </th>
                        <th>
                            First Name
                        </th>
                        <th>
                            Last Name
                        </th>
                        <th>
                            Location
                        </th>
                        <th>
                            3rd party username
                        </th>
                        <th>
                            Time
                        </th>
                        <!--                        <th>-->
                        <!--                            Test-->
                        <!--                        </th>-->
                        <th>
                            Status
                        </th>
                        <!--                        <th>-->
                        <!--                            Change Status-->
                        <!--                        </th>-->


                    </tr>

                    </thead>

                    <tbody>

                    <?
                    foreach ($reservations as $reservation) {
                        $customer = Customers::find()
                            ->select(['*'])
                            ->where([
                                'customer_id' => $reservation->customer_id,
                                //'confirmed' => '1',
                                //'third_user_id' => '0'
                            ])
                            ->one();

                        $location = Locations::find()
                            ->select(['location_name'])
                            ->where([
                                'location_id' => $reservation->location_id,
                            ])
                            ->one();

                        $third_user = ThirdPartyUsers::find()
                            ->select(['user_name'])
                            ->where([
                                'id' => $reservation->third_user_id,
                            ])
                            ->one();

                        $mysqldate = strtotime( $reservation->test_date );
                        $ourdate = date( 'm-d-Y', $mysqldate );
                        ?>

                        <tr>
<!--                            <td>-->
<!--                                --><?//= $customer->customer_id ?>
<!--                            </td>-->
                            <td>
                                <?= $customer->learners_permit_number ?>
                            </td>
                            <td>
                                <?= $customer->first_name ?>
                            </td>
                            <td>
                                <?= $customer->last_name ?>
                            </td>
                            <td>
                                <?= $location->location_name ?>
                            </td>
                            <td>
                                <?= $third_user->user_name; ?>
                            </td>
                            <td>
                                <?=$reservation->test_start_time?>
                            </td>
                            <!--                                <td>-->
                            <!--                                    --><?//=$customer->test_type?><!----><?//if($customer->sub_test_type){ echo "&gt;&gt; ".$customer->sub_test_type;}?>
                            <!--                                </td>-->
                            <td>
                                <?=$reservation->status?>

                            </td>
                            <!--                                <td>-->
                            <!--                                    --><?//=CustomerReservedTimeslots::customer_status_dropdown($customer->status, 'status1')?>
                            <!--                                    <div id="div-select-toughbook1">-->
                            <!---->
                            <!--                                    </div>-->
                            <!---->
                            <!--                                </td>-->

                        </tr>

                        <?
                    }
                    ?>
                    </tbody>

                </table>
            </div>
            <?
        } else {
            ?>
            <h4>No records found!</h4>
            <?
        }

        ?>



        <?php $var4 = ob_get_clean(); ?>



<?
$normal_res_count=CustomerReservedTimeslots::find()
    ->select(['COUNT(timeslot_id) AS cnt'])
    ->where([
        'test_date' => $today_date,
        'confirmed' => '1',
        'third_user_id' => '0'
    ])
    ->andFilterWhere([
        'or',
        ['like', 'status', 'checked in'],
        ['like', 'status', 'Waiting for check in'],
        ['like', 'status', 'Assigned to toughbook'],
    ])
    ->one()->cnt;

$third_res_count=CustomerReservedTimeslots::find()
    ->select(['COUNT(timeslot_id) AS cnt'])
    ->where([
        'test_date' => $today_date,
        'confirmed' => '1',
    ])->andWhere(['>', 'third_user_id', '0'])
    ->andFilterWhere([
        'or',
        ['like', 'status', 'checked in'],
        ['like', 'status', 'Waiting for check in'],
        ['like', 'status', 'Assigned to toughbook'],
    ])
    ->one()->cnt;

$completed_normal_res_count=CustomerReservedTimeslots::find()
    ->select(['COUNT(timeslot_id) AS cnt'])
    ->where([
        'test_date' => $today_date,
        'confirmed' => '1',
        'third_user_id' => '0',
    ])->andWhere(['!=', 'status', 'checked in'])
    ->andWhere(['!=', 'status', 'Waiting for check in'])
    ->andWhere(['!=', 'status', 'Assigned to toughbook'])
    ->one()->cnt;

$completed_3rd_res_count=CustomerReservedTimeslots::find()
    ->select(['COUNT(timeslot_id) AS cnt'])
    ->where([
        'test_date' => $today_date,
        'confirmed' => '1',
    ])->andWhere(['!=', 'status', 'checked in'])
    ->andWhere(['!=', 'status', 'Waiting for check in'])
    ->andWhere(['!=', 'status', 'Assigned to toughbook'])
    ->andWhere(['>', 'third_user_id', '0'])
    ->one()->cnt;


?>





<div class="dashboard-tabs">
    <?

    echo Tabs::widget([
        'items' => [
            [
                //'label' => "Total Appointments ($normal_res_count)",
                'label' => "Total Appointments",
                'content' => $var1,
                'active' => true
            ],
            [
                'label' => "Standard Appointments ($completed_normal_res_count)",
                'content' => $var3,
            ],
            [
                'label' => "3rd Party Appointments ($third_res_count)",
                'content' => $var2,
            ],
            [
                'label' => "CDL Appointments ($completed_3rd_res_count)",
                'content' => $var4,
            ],


        ],
    ]);
    ?>
</div>





    </div>
</div>
