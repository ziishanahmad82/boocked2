<?php
 /*@var $this yii\web\View */
$this->title = 'Dashboard';
use app\models\CustomerReservedTimeslots;
use app\models\Customers;
use app\models\Locations;
use app\models\Services;
use app\models\ThirdPartyUsers;
use app\models\Toughbooks;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;


if(Yii::$app->user->identity->user_group=='admin' OR Yii::$app->user->identity->user_group=='csr') {

    $today_date = date("Y-m-d");

    $this->registerJs(
        '
$(document).ready( function(){
        $("#div-select-toughbook1").hide();
    });
    $("#status1").change(function(){
        if($("#status1").val()=="assign-toughbook"){
            $("#div-select-toughbook1").show();
        }
    });


    $(document).ready( function(){
        $("#div-select-toughbook2").hide();
    });
    $("#status2").change(function(){
        if($("#status2").val()=="assign-toughbook"){
            $("#div-select-toughbook2").show();
        }
    });
'
    );
    ?>

    <div class="site-index" style="display:none;">


        <div class="body-content">

            <div style="float: right;">

            <span class="cdl1 cdl_btn3"><a href="<?=Yii::getAlias('@web') . '../../../'?>customer/web/index.php?customer_action=new"
                    >New Appointment</a></span>
            <span class="cdl1 cdl_btn3"><a href="<?=Yii::getAlias('@web') . '../../../'?>customer/web/index.php?customer_action=edit"
                    >Change/Cancel Appointment</a></span>


            </div>

            <div id="sitem5">
                <h1>Dashboard <?= $today_date = date("m-d-Y"); ?></h1>
            </div>


            <?php ob_start(); ?>



            <?
            $today_date = date("Y-m-d");
            $total_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->one()->cnt;
            $completed_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'and',
                    ['!=', 'status', 'checked in'],
                    ['!=', 'status', 'Waiting for check in'],
                    ['!=', 'status', 'Assigned to toughbook'],
                ])
                ->one()->cnt;


            $untested_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'or',
                    ['like', 'status', 'checked in'],
                    ['like', 'status', 'Waiting for check in'],
                    ['like', 'status', 'Assigned to toughbook'],
                ])
                ->one()->cnt;

            ?>

            <div class="sitem8">
                <div class="sitem9">
                    <b>Total :</b> <?= $total_res_count ?>&nbsp;&nbsp;
                    <b>Completed :</b> <?= $completed_res_count ?>&nbsp;&nbsp;
                    <b>Untested :</b> <?= $untested_res_count ?>
                </div>
            </div>


            <?php Pjax::begin(['id' => 'p1']); ?>

            <?= GridView::widget([
                'id' => 'p1',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'dl_no',
                        'label' => 'Permit Number',
                        'value' => 'customer.learners_permit_number',
                    ],
                    [
                        'attribute' => 'full_name',
                        'label' => 'Name',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return "<span style=\"text-transform: capitalize\">{$data->customer->first_name} {$data->customer->last_name}</span>";

                        }
                    ],
                    //'location_id',
                    //'test_date',
                    [
                        'attribute' => 'test_start_time',
                        'label' => 'Test Time',
                        'value' => 'test_start_time',
                        'format' => ['time', 'php:h:i A'],
                    ],
                    [
                        'attribute' => 'third_user',
                        'label' => 'Third Party Examiner',
                        'format' => 'raw',
                        'value' => function ($data) {
                            $str = '<div class="sitem7">';
                            if ($data->third_user_id > 0) {
                                $str .= $data->thirdparty->displayname;
                            } else
                                $str .= 'DMV Examiner';
                            $str .= '</div>';
                            return $str;
                        },
                    ],
                    //'test_start_time',
                    // 'test_end_time',
                    // 'examiner_id',

                    [
                        'attribute' => 'test_type',
                        'label' => 'Test Type',
                        'format' => 'raw',
                        'value' => function ($data) {
                            if ($data->test_type == 'NCDL')
                                return 'Standard';
                            else
                                return 'CDL';
                        },
                    ],
                    // 'confirmed',
                    // 'third_user_id',
                    //'tough_book_id',
                    //'status',
                    [
                        'label' => 'Status',
                        'attribute' => 'status',
                        'format' => 'raw',
                        'value' => function($data){
                            if ($data->status == 'Waiting for check in') {
                                return "<a href='".Yii::getAlias('@web')."/index.php/customers/update?id=".$data->customer->customer_id."'>Waiting for check in</a>";
                            } else {
                                return $data->status;
                            }
                        }
                    ],
                    [

                        'label' => 'Actions',
                        'format' => 'raw',
                        //'filter' => '<a class="link_reset_filter">Reset All Filters</a>',
                        'filter' => Html::a('Reset All Filters', ['index'], ['class' => 'link_reset_filter']),
                        'value' => function ($data) {
                            $str = '<div class="sitem6">';
                            if ($data->status == 'Waiting for check in') {

                                $str .= CustomerReservedTimeslots::customer_status_dropdown($data->status, 'status1');
                                $str .= '

                                        <input type="hidden" name="assign_toughbook" value="no">
                                        <input type="submit" class="btn-success" value="Go"/>
                            ';

                            } else if ($data->status == 'Checked in') {
                                $str .= '<select name="toughbook-select1" id="toughbook-select1">
                                            <option value="0">Select a toughbook</option>';

                                $toughbooks = Toughbooks::find()
                                    ->select(['*'])
                                    ->where([
                                        'toughbook_status' => 'active',
                                    ])
                                    ->all();
                                foreach ($toughbooks as $toughbook) {

                                    $str .= '<option value="' . $toughbook->toughbook_id . '">' . $toughbook->toughbook_code . '</option>';

                                }
                                $str .= '
                            </select>

                                <input type="hidden" name="assign_toughbook" value="yes">
                                <input type="submit" class="btn-success" value="Go"/>';


                            } else {
                                $str .= '';
                                return '';
                            }
                            $str .= '</div>';
                            return $str;

                        },
                    ],

                    //['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <?php Pjax::end(); ?>





            <?php $var1 = ob_get_clean(); ?>





            <?php ob_start(); ?>




            <?
            $today_date = date("Y-m-d");
            $total_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'and',
                    ['<', 'third_user_id', 1],
                    ['=', 'test_type', 'NCDL'],
                ])
                ->one()->cnt;

            $completed_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'and',
                    ['!=', 'status', 'checked in'],
                    ['!=', 'status', 'Waiting for check in'],
                    ['!=', 'status', 'Assigned to toughbook'],
                    ['<', 'third_user_id', 1],
                    ['=', 'test_type', 'NCDL'],
                ])
                ->one()->cnt;


            $untested_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'or',
                    ['like', 'status', 'checked in'],
                    ['like', 'status', 'Waiting for check in'],
                    ['like', 'status', 'Assigned to toughbook'],
                ])
                ->andFilterWhere([
                    'and',
                    ['<', 'third_user_id', 1],
                    ['=', 'test_type', 'NCDL'],
                ])
                ->one()->cnt;

            ?>

            <div class="sitem8">
                <div class="sitem9">
                    <b>Total :</b> <?= $total_res_count ?>&nbsp;&nbsp;
                    <b>Completed :</b> <?= $completed_res_count ?>&nbsp;&nbsp;
                    <b>Untested :</b> <?= $untested_res_count ?>
                </div>
            </div>


            <?php Pjax::begin(['id' => 'p2']); ?>

            <?= GridView::widget([
                'id' => 'p2',
                'dataProvider' => $dataProvider2,
                'filterModel' => $searchModel2,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'dl_no',
                        'label' => 'Permit Number',
                        'value' => 'customer.learners_permit_number',
                    ],
                    [
                        'attribute' => 'full_name',
                        'label' => 'Name',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return "<span style=\"text-transform: capitalize\">{$data->customer->first_name} {$data->customer->last_name}</span>";

                        }
                    ],
                    //'location_id',
                    //'test_date',
                    [
                        'attribute' => 'test_start_time',
                        'label' => 'Test Time',
                        'value' => 'test_start_time',
                        'format' => ['time', 'php:h:i A'],
                    ],
                    //'test_start_time',
                    // 'test_end_time',
                    // 'examiner_id',

                    [
                        'attribute' => 'test_type',
                        'label' => 'Test Type',
                        'format' => 'raw',
                        'value' => function ($data) {
                            if ($data->test_type == 'NCDL')
                                return 'Standard';
                            else
                                return 'CDL';
                        },
                    ],
                    // 'confirmed',
                    // 'third_user_id',
                    //'tough_book_id',
                    'status',
                    [

                        'label' => 'Actions',
                        'format' => 'raw',
                        'filter' => Html::a('Reset All Filters', ['index'], ['class' => 'link_reset_filter']),
                        'value' => function ($data) {
                            $str = '<div class="sitem6">';
                            if ($data->status == 'Waiting for check in') {

                                $str .= CustomerReservedTimeslots::customer_status_dropdown($data->status, 'status1');
                                $str .= '

                                        <input type="hidden" name="assign_toughbook" value="no">
                                        <input type="submit" class="btn-success" value="Go"/>
                            ';

                            } else if ($data->status == 'Checked in') {
                                $str .= '<select name="toughbook-select1" id="toughbook-select1">
                                            <option value="0">Select a toughbook</option>';

                                $toughbooks = Toughbooks::find()
                                    ->select(['*'])
                                    ->where([
                                        'toughbook_status' => 'active',
                                    ])
                                    ->all();
                                foreach ($toughbooks as $toughbook) {

                                    $str .= '<option value="' . $toughbook->toughbook_id . '">' . $toughbook->toughbook_code . '</option>';

                                }
                                $str .= '
                            </select>

                                <input type="hidden" name="assign_toughbook" value="yes">
                                <input type="submit" class="btn-success" value="Go"/>';


                            } else {
                                $str .= '';
                                return '';
                            }
                            $str .= '</div>';
                            return $str;

                        },
                    ],

                    //['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <?php Pjax::end(); ?>


            <?php $var2 = ob_get_clean(); ?>




            <?php ob_start(); ?>





            <?
            $today_date = date("Y-m-d");
            $total_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'and',
                    ['>', 'third_user_id', 0],
                    ['=', 'test_type', 'NCDL'],
                ])
                ->one()->cnt;

            $completed_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'and',
                    ['!=', 'status', 'checked in'],
                    ['!=', 'status', 'Waiting for check in'],
                    ['!=', 'status', 'Assigned to toughbook'],
                    ['>', 'third_user_id', 0],
                    ['=', 'test_type', 'NCDL'],
                ])
                ->one()->cnt;


            $untested_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'or',
                    ['like', 'status', 'checked in'],
                    ['like', 'status', 'Waiting for check in'],
                    ['like', 'status', 'Assigned to toughbook'],
                ])
                ->andFilterWhere([
                    'and',
                    ['>', 'third_user_id', 0],
                    ['=', 'test_type', 'NCDL'],
                ])
                ->one()->cnt;

            ?>

            <div class="sitem8">
                <div class="sitem9">
                    <b>Total :</b> <?= $total_res_count ?>&nbsp;&nbsp;
                    <b>Completed :</b> <?= $completed_res_count ?>&nbsp;&nbsp;
                    <b>Untested :</b> <?= $untested_res_count ?>
                </div>
            </div>


            <?php Pjax::begin(['id' => 'p3']); ?>

            <?= GridView::widget([
                'id' => 'p3',
                'dataProvider' => $dataProvider3,
                'filterModel' => $searchModel3,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'dl_no',
                        'label' => 'Permit Number',
                        'value' => 'customer.learners_permit_number',
                    ],
                    [
                        'attribute' => 'full_name',
                        'label' => 'Name',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return "<span style=\"text-transform: capitalize\">{$data->customer->first_name} {$data->customer->last_name}</span>";

                        }
                    ],
                    //'location_id',
                    //'test_date',
                    [
                        'attribute' => 'test_start_time',
                        'label' => 'Test Time',
                        'value' => 'test_start_time',
                        'format' => ['time', 'php:h:i A'],
                    ],
                    [
                        'attribute' => 'third_user',
                        'label' => 'Third Party Examiner',
                        'format' => 'raw',
                        'value' => function ($data) {
                            $str = '<div class="sitem7">';
                            if ($data->third_user_id > 0) {
                                $str .= $data->thirdparty->displayname;
                            } else
                                $str .= 'DMV Examiner';
                            $str .= '</div>';
                            return $str;
                        },
                    ],
                    //'test_start_time',
                    // 'test_end_time',
                    // 'examiner_id',

                    [
                        'attribute' => 'test_type',
                        'label' => 'Test Type',
                        'format' => 'raw',
                        'value' => function ($data) {
                            if ($data->test_type == 'NCDL')
                                return 'Standard';
                            else
                                return 'CDL';
                        },
                    ],
                    // 'confirmed',
                    // 'third_user_id',
                    //'tough_book_id',
                    'status',
                    [

                        'label' => 'Actions',
                        'format' => 'raw',
                        'filter' => Html::a('Reset All Filters', ['index'], ['class' => 'link_reset_filter']),
                        'value' => function ($data) {
                            $str = '<div class="sitem6">';
                            if ($data->status == 'Waiting for check in') {

                                $str .= CustomerReservedTimeslots::customer_status_dropdown($data->status, 'status1');
                                $str .= '

                                        <input type="hidden" name="assign_toughbook" value="no">
                                        <input type="submit" class="btn-success" value="Go"/>
                            ';

                            } else if ($data->status == 'Checked in') {
                                $str .= '<select name="toughbook-select1" id="toughbook-select1">
                                            <option value="0">Select a toughbook</option>';

                                $toughbooks = Toughbooks::find()
                                    ->select(['*'])
                                    ->where([
                                        'toughbook_status' => 'active',
                                    ])
                                    ->all();
                                foreach ($toughbooks as $toughbook) {

                                    $str .= '<option value="' . $toughbook->toughbook_id . '">' . $toughbook->toughbook_code . '</option>';

                                }
                                $str .= '
                            </select>

                                <input type="hidden" name="assign_toughbook" value="yes">
                                <input type="submit" class="btn-success" value="Go"/>';


                            } else {
                                $str .= '';
                                return '';
                            }
                            $str .= '</div>';
                            return $str;

                        },
                    ],

                    //['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <?php Pjax::end(); ?>





            <?php $var3 = ob_get_clean(); ?>





            <?php ob_start(); ?>




            <?
            $today_date = date("Y-m-d");
            $total_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'and',
                    ['<', 'third_user_id', 1],
                    ['=', 'test_type', 'CDL'],
                ])
                ->one()->cnt;

            $completed_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'and',
                    ['!=', 'status', 'checked in'],
                    ['!=', 'status', 'Waiting for check in'],
                    ['!=', 'status', 'Assigned to toughbook'],
                    ['<', 'third_user_id', 1],
                    ['=', 'test_type', 'CDL'],
                ])
                ->one()->cnt;


            $untested_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'or',
                    ['like', 'status', 'checked in'],
                    ['like', 'status', 'Waiting for check in'],
                    ['like', 'status', 'Assigned to toughbook'],
                ])
                ->andFilterWhere([
                    'and',
                    ['<', 'third_user_id', 1],
                    ['=', 'test_type', 'CDL'],
                ])
                ->one()->cnt;

            ?>

            <div class="sitem8">
                <div class="sitem9">
                    <b>Total :</b> <?= $total_res_count ?>&nbsp;&nbsp;
                    <b>Completed :</b> <?= $completed_res_count ?>&nbsp;&nbsp;
                    <b>Untested :</b> <?= $untested_res_count ?>
                </div>
            </div>


            <?php Pjax::begin(['id' => 'p4']); ?>

            <?= GridView::widget([
                'id' => 'p4',
                'dataProvider' => $dataProvider4,
                'filterModel' => $searchModel4,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'dl_no',
                        'label' => 'Permit Number',
                        'value' => 'customer.learners_permit_number',
                    ],
                    [
                        'attribute' => 'full_name',
                        'label' => 'Name',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return "<span style=\"text-transform: capitalize\">{$data->customer->first_name} {$data->customer->last_name}</span>";

                        }
                    ],
                    //'location_id',
                    //'test_date',
                    [
                        'attribute' => 'test_start_time',
                        'label' => 'Test Time',
                        'value' => 'test_start_time',
                        'format' => ['time', 'php:h:i A'],
                    ],
//                [
//                    'attribute' => 'third_user',
//                    'label' => 'Third Party User',
//                    'format' => 'raw',
//                    'value' => function($data){
//                        $str='<div class="sitem7">';
//                        if($data->third_user_id>0)
//                        {
//                            $str.= $data->thirdparty->displayname;
//                        }
//                        else
//                            $str.='';
//                        $str.='</div>';
//                        return $str;
//                    },
//                ],
                    //'test_start_time',
                    // 'test_end_time',
                    // 'examiner_id',

                    [
                        'attribute' => 'test_type',
                        'label' => 'Test Type',
                        'format' => 'raw',
                        'value' => function ($data) {
                            if ($data->test_type == 'NCDL')
                                return 'Standard';
                            else
                                return 'CDL';
                        },
                    ],
                    // 'confirmed',
                    // 'third_user_id',
                    //'tough_book_id',
                    'status',
                    [

                        'label' => 'Actions',
                        'format' => 'raw',
                        'filter' => Html::a('Reset All Filters', ['index'], ['class' => 'link_reset_filter']),
                        'value' => function ($data) {
                            $str = '<div class="sitem6">';
                            if ($data->status == 'Waiting for check in') {

                                $str .= CustomerReservedTimeslots::customer_status_dropdown($data->status, 'status1');
                                $str .= '

                                        <input type="hidden" name="assign_toughbook" value="no">
                                        <input type="submit" class="btn-success" value="Go"/>
                            ';

                            } else if ($data->status == 'Checked in') {
                                $str .= '<select name="toughbook-select1" id="toughbook-select1">
                                            <option value="0">Select a toughbook</option>';

                                $toughbooks = Toughbooks::find()
                                    ->select(['*'])
                                    ->where([
                                        'toughbook_status' => 'active',
                                    ])
                                    ->all();
                                foreach ($toughbooks as $toughbook) {

                                    $str .= '<option value="' . $toughbook->toughbook_id . '">' . $toughbook->toughbook_code . '</option>';

                                }
                                $str .= '
                            </select>

                                <input type="hidden" name="assign_toughbook" value="yes">
                                <input type="submit" class="btn-success" value="Go"/>';


                            } else {
                                $str .= '';
                                return '';
                            }
                            $str .= '</div>';
                            return $str;

                        },
                    ],

                    //['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <?php Pjax::end(); ?>



            <?php $var4 = ob_get_clean(); ?>



            <?
            $normal_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'or',
                    ['like', 'status', 'checked in'],
                    ['like', 'status', 'Waiting for check in'],
                    ['like', 'status', 'Assigned to toughbook'],
                ])
                ->one()->cnt;

            $third_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                ])->andWhere(['>', 'third_user_id', '0'])
                ->andFilterWhere([
                    'or',
                    ['like', 'status', 'checked in'],
                    ['like', 'status', 'Waiting for check in'],
                    ['like', 'status', 'Assigned to toughbook'],
                ])
                ->one()->cnt;

            $completed_normal_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    'third_user_id' => '0',
                ])->andWhere(['!=', 'status', 'checked in'])
                ->andWhere(['!=', 'status', 'Waiting for check in'])
                ->andWhere(['!=', 'status', 'Assigned to toughbook'])
                ->one()->cnt;

            $completed_3rd_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                ])->andWhere(['!=', 'status', 'checked in'])
                ->andWhere(['!=', 'status', 'Waiting for check in'])
                ->andWhere(['!=', 'status', 'Assigned to toughbook'])
                ->andWhere(['>', 'third_user_id', '0'])
                ->one()->cnt;


            ?>


            <div class="dashboard-tabs">
                <?

                //            echo Tabs::widget([
                //                'items' => [
                //                    [
                //                        //'label' => "Total Appointments ($normal_res_count)",
                //                        'label' => "All DMV ($count1)",
                //                        'content' => $var1,
                //                        'active' => true
                //                    ],
                //                    [
                //                        'label' => "Standard ($count2)",
                //                        'content' => $var2,
                //                    ],
                //                    [
                //                        'label' => "Third Party ($count3)",
                //                        'content' => $var3,
                //                    ],
                //                    [
                //                        'label' => "CDL ($count4)",
                //                        'content' => $var4,
                //                    ],
                //
                //
                //                ],
                //            ]);


                echo Tabs::widget([
                    'items' => [
                        [
                            //'label' => "Total Appointments ($normal_res_count)",
                            'label' => "All Appointments",
                            'content' => $var1,
                            'active' => true
                        ],
                        [
                            'label' => "Standard",
                            'content' => $var2,
                        ],
                        [
                            'label' => "Third Party",
                            'content' => $var3,
                        ],
                        [
                            'label' => "CDL",
                            'content' => $var4,
                        ],


                    ],
                ]);
                ?>
            </div>


        </div>
    </div>


    <?
    $this->registerJs("
    $('.link_reset_filter').click(function(){
        $('#p1-filters :input').val('');
        //p1-filters
    })
");


} else {



    $today_date = date("Y-m-d");

    $this->registerJs(
        '
$(document).ready( function(){
        $("#div-select-toughbook1").hide();
    });
    $("#status1").change(function(){
        if($("#status1").val()=="assign-toughbook"){
            $("#div-select-toughbook1").show();
        }
    });


    $(document).ready( function(){
        $("#div-select-toughbook2").hide();
    });
    $("#status2").change(function(){
        if($("#status2").val()=="assign-toughbook"){
            $("#div-select-toughbook2").show();
        }
    });
'
    );
    ?>

    <div class="site-index">


        <div class="body-content">

            <div style="float: right;">

            <span class="cdl1 cdl_btn3"><a href="<?=Yii::getAlias('@web') . '../../../'?>customer/web/index.php?customer_action=new"
                    >New Appointment</a></span>
            <span class="cdl1 cdl_btn3"><a href="<?=Yii::getAlias('@web') . '../../../'?>customer/web/index.php?customer_action=edit"
                    >Change/Cancel Appointment</a></span>


            </div>

            <div id="sitem5">
                <h1>Dashboard <?= $today_date = date("m-d-Y"); ?></h1>
            </div>


            <?php ob_start(); ?>



            <?
            $today_date = date("Y-m-d");
            $total_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    'third_user_id' => Yii::$app->user->identity->id
                ])
                ->one()->cnt;
            $completed_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    'third_user_id' => Yii::$app->user->identity->id
                ])
                ->andFilterWhere([
                    'and',
                    ['!=', 'status', 'checked in'],
                    ['!=', 'status', 'Waiting for check in'],
                    ['!=', 'status', 'Assigned to toughbook'],
                ])
                ->one()->cnt;


            $untested_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    'third_user_id' => Yii::$app->user->identity->id
                ])
                ->andFilterWhere([
                    'or',
                    ['like', 'status', 'checked in'],
                    ['like', 'status', 'Waiting for check in'],
                    ['like', 'status', 'Assigned to toughbook'],
                ])
                ->one()->cnt;

            ?>

            <div class="sitem8">
                <div class="sitem9">
                    <b>Total :</b> <?= $total_res_count ?>&nbsp;&nbsp;
                    <b>Completed :</b> <?= $completed_res_count ?>&nbsp;&nbsp;
                    <b>Untested :</b> <?= $untested_res_count ?>
                </div>
            </div>


            <?php Pjax::begin(['id' => 'p1']); ?>

            <?= GridView::widget([
                'id' => 'p1',
                'dataProvider' => $dataProvider5,
                'filterModel' => $searchModel5,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'dl_no',
                        'label' => 'Permit Number',
                        'value' => 'customer.learners_permit_number',
                    ],
                    [
                        'attribute' => 'full_name',
                        'label' => 'Name',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return "<span style=\"text-transform: capitalize\">{$data->customer->first_name} {$data->customer->last_name}</span>";

                        }
                    ],
                    //'location_id',
                    //'test_date',
                    [
                        'attribute' => 'test_start_time',
                        'label' => 'Test Time',
                        'value' => 'test_start_time',
                        'format' => ['time', 'php:h:i A'],
                    ],
//                    [
//                        'attribute' => 'third_user',
//                        'label' => 'Third Party Examiner',
//                        'format' => 'raw',
//                        'value' => function ($data) {
//                            $str = '<div class="sitem7">';
//                            if ($data->third_user_id > 0) {
//                                $str .= $data->thirdparty->displayname;
//                            } else
//                                $str .= 'DMV Examiner';
//                            $str .= '</div>';
//                            return $str;
//                        },
//                    ],
                    //'test_start_time',
                    // 'test_end_time',
                    // 'examiner_id',

                    [
                        'attribute' => 'test_type',
                        'label' => 'Test Type',
                        'format' => 'raw',
                        'value' => function ($data) {
                            if ($data->test_type == 'NCDL')
                                return 'Standard';
                            else
                                return 'CDL';
                        },
                    ],
                    // 'confirmed',
                    // 'third_user_id',
                    //'tough_book_id',
                    'status',
                    [

                        'label' => 'Actions',
                        'format' => 'raw',
                        //'filter' => '<a class="link_reset_filter">Reset All Filters</a>',
                        'filter' => Html::a('Reset All Filters', ['index'], ['class' => 'link_reset_filter']),
                        'value' => function ($data) {
                            $str = '<div class="sitem6">';

                            $str.=Html::a('Change/Cancel Appointment', ['index'], ['class' => 'sitem12']);

                            $str .= '</div>';
                            return $str;

                        },
                    ],

                    //['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <?php Pjax::end(); ?>





            <?php $var1 = ob_get_clean(); ?>















            <div class="dashboard-tabs">
               <?

                echo Tabs::widget([
                    'items' => [
                        [
                            //'label' => "Total Appointments ($normal_res_count)",
                            'label' => "All Appointments",
                            'content' => $var1,
                            'active' => true
                        ],



                    ],
                ]);
                ?>
            </div>


        </div>
    </div>


    <?
    $this->registerJs("
    $('.link_reset_filter').click(function(){
        $('#p1-filters :input').val('');
        //p1-filters
    })
");


}
?>

    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 no-padding-l media-xs-full">
            <div class="panel panel-default">
                <div class="panel-body section-calendar">
                    <div class="page">
                        <div style="width:100%; max-width:600px; display:inline-block;">
                            <div class="monthly" id="mycalendar"></div>
                        </div>
                    </div>
                    <ul class="list-inline list-wmy">
                        <li><a href="">Week</a></li>
                        <li><a href="">Month</a></li>
                        <li><a href="">Year</a></li>
                    </ul>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/staff-icon.png"> <span class="staff-head">Staff</span></h5></li>
                        <li class="pull-right li-right"><i class="fa fa-plus plus-square"></i></li>
                    </ul>
                </div>
                <div class="panel-body staff-div">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff2.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff2.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff2.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/analytics.png"> <span class="staff-head">analytics</span></h5></li>
                        <li class="pull-right li-right"><i class="fa fa-plus plus-square"></i></li>
                    </ul>
                </div>
                <div class="panel-body section-analytics">
                    <div class="row">
                        <div class="div-analytics">
                            <canvas id="canvas" height="450" width="600"></canvas>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 div-relative pull-right">
            <ul class="list-inline pull-right">
                <li><a href="<?= Yii::getAlias('@web') ?>/index.php/services/index" data-toggle="tooltip" data-placement="left" data-original-title="Add Service"><i class="fa fa-plus fa-add-services"></i></a></li>
                <li><a href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="Help"><i class="fa fa-question fa-question-mark"></i></a></li>
            </ul>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9 media-xs-full">
            <div style="overflow:hidden;">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-8">
                            <div id="datetimepicker12"></div>
                        </div>
                    </div>
                </div>
            <ul class="nav nav-tabs nav-main">
                <?php $services = Services::find()->asArray()->all();
                //print_r($service_name);
                $i=1;
                $closediv='';
                foreach($services as $service ){
                 if($i<4) { ?>
                     <li class="active">
                         <a href="#first_<?= $service['service_id'] ?>" class="dashboardapoints" data-toggle="tab"
                            data-service-id="<?= $service['service_id'] ?>"><?= $service['service_name'] ?></a>
                     </li>
                     <?php
                 }else {
                     if($i==4){
                         ?>
                <li><ul class="dropdown-menu" id="demo12" style="background: #fff; margin-left: 40px;">
                <?php
                     $closediv = '</ul></li>';
                }
            if($i>4){
                ?>
                <li><a href="#first_2" data-toggle="tab"><?= $service['service_name'] ?></a></li>
              <?php  }
                 }
                    $i++; }?>
                <?php echo $closediv; ?>


                <li class="caret-dropdown">
                    <a href="#"  class="dropdown-toggle" data-target="#demo12" data-toggle="collapse" >
                        <i class="fa fa-caret-down"></i>
                    </a>

                </li>

               <!-- <li><ul class="dropdown-menu" id="demo12">
                    <li><a href="#">HTML</a></li>
                    <li><a href="#">CSS</a></li>
                    <li><a href="#">JavaScript</a></li>
                </ul></li>-->

            </ul>






<?php
                $script = " $('.dashboardapoints').onclick(function () {
                var acv = $(this).href();
                alert('acv');
                });";

              //  $this->registerJs($script, Yii::POS_END, 'my-options');
?>

            <div class="tab-content">
                <div class="tab-pane fade in active" id="first_1">
                    <div class="panel-collapse collapse in active">
                        <div class="panel-body panel-main">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">appointments</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">0</h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">estimated sales</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">0</h1>
                                            <p>sales</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">new customers</h5>
                                        </div>
                                        <div class="panel-body panel-no-padding">
                                            <div class="div-scroller">
                                                <table class="table table-responsive table-striped">
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-custom">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">satisfaction</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">10%</h1>
                                            <p>satisfaction</p>
                                            <div class="section-panel-footer">
                                                <p>From 0 Reviews</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">total appntmnt / month</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">0</h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">extra appointments</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">5%</h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-custom">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">appointments</h5>
                                        </div>
                                        <div class="panel-body panel-no-padding">
                                            <div class="table-scroller">
                                                <table class="table table-responsive table-striped table-months">
                                                    <tr>
                                                        <th>Mon</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Tue</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Wed</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Thur</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Fri</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Sat</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Sun</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="first_2">
                    <div class="panel-collapse collapse in active">
                        <div class="panel-body panel-main">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">appointments</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">1</h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">estimated sales</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">2</h1>
                                            <p>sales</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">new customers</h5>
                                        </div>
                                        <div class="panel-body panel-no-padding">
                                            <div class="div-scroller">
                                                <table class="table table-responsive table-striped">
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-custom">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">satisfaction</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">10%</h1>
                                            <p>satisfaction</p>
                                            <div class="section-panel-footer">
                                                <p>From 0 Reviews</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">total appointments / month</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">0</h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">extra appointments</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">5%</h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-custom">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">appointments</h5>
                                        </div>
                                        <div class="panel-body panel-no-padding">
                                            <div class="table-scroller">
                                                <table class="table table-responsive table-striped table-months">
                                                    <tr>
                                                        <th>Mon</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Tue</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Wed</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Thur</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Fri</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Sat</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Sun</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="third">
                    <div class="panel-collapse collapse in active">
                        <div class="panel-body panel-main">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">appointments</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">3</h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">estimated sales</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">4</h1>
                                            <p>sales</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">new customers</h5>
                                        </div>
                                        <div class="panel-body panel-no-padding">
                                            <div class="div-scroller">
                                                <table class="table table-responsive table-striped">
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-custom">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">satisfaction</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">10%</h1>
                                            <p>satisfaction</p>
                                            <div class="section-panel-footer">
                                                <p>From 0 Reviews</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">total appointments / month</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">0</h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">extra appointments</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">5%</h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-custom">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">appointments</h5>
                                        </div>
                                        <div class="panel-body panel-no-padding">
                                            <div class="table-scroller">
                                                <table class="table table-responsive table-striped table-months">
                                                    <tr>
                                                        <th>Mon</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Tue</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Wed</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Thur</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Fri</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Sat</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Sun</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="forth">
                    <div class="panel-collapse collapse in active">
                        <div class="panel-body panel-main">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">appointments</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">0</h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">estimated sales</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">0</h1>
                                            <p>sales</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">new customers</h5>
                                        </div>
                                        <div class="panel-body panel-no-padding">
                                            <div class="div-scroller">
                                                <table class="table table-responsive table-striped">
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-custom">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">satisfaction</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">10%</h1>
                                            <p>satisfaction</p>
                                            <div class="section-panel-footer">
                                                <p>From 0 Reviews</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">total appointments / month</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">0</h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">extra appointments</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">5%</h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-custom">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">appointments</h5>
                                        </div>
                                        <div class="panel-body panel-no-padding">
                                            <div class="table-scroller">
                                                <table class="table table-responsive table-striped table-months">
                                                    <tr>
                                                        <th>Mon</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Tue</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Wed</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Thur</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Fri</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Sat</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Sun</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="clearfix"></div>
  <?php  if($information_required=='yes'){ ?>
    <button type="button" class="btn" data-toggle="modal" data-target="#pay2" id="blocked">
        Player 1
    </button>
<div class="modal fade" id="pay1">
    <div class="modal-dialog modal-dialog-customer">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="list-inline list-modal-header">
                    <li class="text-uppercase"><i class="fa fa-dollar" style="color: #FC7600;"></i>Account Settings</li>
                    <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                        <a href=""><i class="fa fa-reply"></i></a>
                    </li>
                   <!-- <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                        <a href=""><i class="fa fa-plus"></i></a>
                    </li>
                    <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban"><i class="fa fa-ban"></i></a></li>
                    <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay"><i class="fa fa-dollar"></i></a></li>-->
                </ul>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 col-md-12">
                        <p>Currency</p>
                        <div class="form-group">
                            <select class="form-control">
                                <option value="98">ALL (ALL)</option>
                                <option value="73">ANG (ANG)</option>
                                <option value="26">ARS ($)</option>
                                <option value="9">AUD ($)</option>
                                <option value="28">BSD ($)</option>
                                <option value="35">BHD (BD)</option>
                                <option value="82">BDT (Tk)</option>
                                <option value="31">BBD ($)</option>
                                <option value="102">BZD (BZD)</option>
                                <option value="30">BMD ($)</option>
                                <option value="68">VEF (Bs.F)</option>
                                <option value="76">BOB (Bs)</option>
                                <option value="43">BGN (BGN)</option>
                                <option value="99">KHR (KHR)</option>
                                <option value="6">CAD (Can$)</option>
                                <option value="93">KYD (CI$)</option>
                                <option value="61">XAF (XAF)</option>
                                <option value="81">XPF (XPF)</option>
                                <option value="25">CLP ($)</option>
                                <option value="55">CRC (CRC)</option>
                                <option value="39">CZK (Kc)</option>
                                <option value="13">DKK (kr)</option>
                                <option value="1">USD ($)</option>
                                <option value="65">TTD (TT$)</option>
                                <option value="95">DOP (RD$)</option>
                                <option value="71">AMD (AMD)</option>
                                <option value="53">XCD ($)</option>
                                <option value="29">XCD (EC$)</option>
                                <option value="79">EGP (E£)</option>
                                <option value="8">EUR (€)</option>
                                <option value="85">FJD ($)</option>
                                <option value="52">HUF (Ft)</option>
                                <option value="56">GMD (GMD)</option>
                                <option value="89">GEL (GEL)</option>
                                <option value="84">GIP (£)</option>
                                <option value="103">GYD (GYD)</option>
                                <option value="94">HTG (HTG)</option>
                                <option value="104">HNL (HNL)</option>
                                <option value="17">HKD ($)</option>
                                <option value="57">UAH (UAH)</option>
                                <option value="49">IDR (Rp)</option>
                                <option value="37">ILS (NIS)</option>
                                <option value="70">JMD (J$)</option>
                                <option value="15">JPY (YEN)</option>
                                <option value="87">JOD (JOD)</option>
                                <option value="54">ISK (kr)</option>
                                <option value="18">HRK (Kuna)</option>
                                <option value="51">KWD (KWD)</option>
                                <option value="63">LVL (LVL)</option>
                                <option value="88">LBP (LBP)</option>
                                <option value="80">SZL (SZL)</option>
                                <option value="58">LTL (Lt)</option>
                                <option value="101">MOP (MOP$)</option>
                                <option value="78">MKD (MKD)</option>
                                <option value="92">AZN (AZN)</option>
                                <option value="90">MUR (Rs)</option>
                                <option value="91">MDL (MDL)</option>
                                <option value="83">MAD (MAD)</option>
                                <option value="96">NGN (NGN)</option>
                                <option value="32">NAD ($)</option>
                                <option value="47">NPR (NPR)</option>
                                <option value="14">NZD ($)</option>
                                <option value="23">NOK (NOK)</option>
                                <option value="59">PEN (PEN)</option>
                                <option value="60">PKR (Rs)</option>
                                <option value="66">PAB (BL)</option>
                                <option value="62">COP ($)</option>
                                <option value="44">MXN ($)</option>
                                <option value="20">PHP (piso)</option>
                                <option value="21">PLN (zl)</option>
                                <option value="97">GTQ (GTQ)</option>
                                <option value="11">ZAR (R)</option>
                                <option value="10">BRL (R$)</option>
                                <option value="75">OMR (OMR)</option>
                                <option value="16">MYR (RM)</option>
                                <option value="69">QAR (QAR)</option>
                                <option value="48">RON (lei)</option>
                                <option value="100">BYR (BYR)</option>
                                <option value="50">LKR (Rs)</option>
                                <option value="2">INR (Rs.)</option>
                                <option value="38">RUB (RUB)</option>
                                <option value="45">SAR (SR)</option>
                                <option value="72">RSD (RSD)</option>
                                <option value="64">KES (Ksh)</option>
                                <option value="22">SGD ($)</option>
                                <option value="36">KRW (KRW)</option>
                                <option value="12">SEK (kr)</option>
                                <option value="46">CHF (CHF)</option>
                                <option value="67">TWD (NT$)</option>
                                <option value="33">TZS (TZS)</option>
                                <option value="77">KZT (KZT)</option>
                                <option value="19">THB (baht)</option>
                                <option value="24">TRY (TL)</option>
                                <option value="3">EUR (€)</option>
                                <option value="4">GBP (₤)</option>
                                <option value="34">AED (AED)</option>
                                <option value="27">UYU (UYU)</option>
                                <option value="86">VND (VND)</option>
                                <option value="5">CNY (CNY)</option>
                            </select>

                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <p>Profession</p>
                        <div class="form-group">
                            <select class="form-control">
                            <option value="4"> Accountant</option>
                            <option value="9"> Acupuncture</option>
                            <option value="81"> Administrative Professional</option>
                            <option value="242"> Advertising</option>
                            <option value="239"> Agile Build-a-Thon</option>
                            <option value="202"> Air Charter Services</option>
                            <option value="28"> Alternative Medicine</option>
                            <option value="181"> Animal Communicator</option>
                            <option value="103"> Aquatic Fitness</option>
                            <option value="229"> Art and Activities</option>
                            <option value="169"> Art Classes</option>
                            <option value="170"> Art School</option>
                            <option value="124"> Artist</option>
                            <option value="163"> Astrologer</option>
                            <option value="17"> Athletic Fields</option>
                            <option value="70"> Attorney</option>
                            <option value="227"> Audio Consultant</option>
                            <option value="175"> Audiologist</option>
                            <option value="78"> Auto Broker</option>
                            <option value="177"> Automotive</option>
                            <option value="233"> Baby Sitter</option>
                            <option value="232"> Babysitting</option>
                            <option value="105"> Barber</option>
                            <option value="1"> Beauty Salon</option>
                            <option value="228"> BeeKeeper</option>
                            <option value="172"> Bicycle Fitting</option>
                            <option value="110"> Bicycle Service and Repair</option>
                            <option value="152"> Birth</option>
                            <option value="153"> Birth and Postpartum Doula</option>
                            <option value="223"> Body Composition Analysis</option>
                            <option value="12"> Body Piercing</option>
                            <option value="22"> Body Waxing</option>
                            <option value="111"> Body-Centered Coaching</option>
                            <option value="112"> Breath-Work Instructor</option>
                            <option value="6"> Bridal Services</option>
                            <option value="158"> Business</option>
                            <option value="19"> Business Consultant</option>
                            <option value="43"> Car Service</option>
                            <option value="113"> Career Coach</option>
                            <option value="243"> Career Consultancy</option>
                            <option value="51"> Carpet Cleaning</option>
                            <option value="168"> Cartomancy /Tarot</option>
                            <option value="123"> Certified Tennis Professional</option>
                            <option value="146"> Child Care</option>
                            <option value="145"> Childrens Resale</option>
                            <option value="161"> Chiropody</option>
                            <option value="13"> Chiropractor</option>
                            <option value="65"> Circus School</option>
                            <option value="164"> Clairvoyant</option>
                            <option value="32"> Clinical Psychologist</option>
                            <option value="187"> Coach</option>
                            <option value="134"> Coach and Psychotherapist</option>
                            <option value="62"> Colon Therapist</option>
                            <option value="207"> Computer and Mobile Repair</option>
                            <option value="205"> Computer Repair</option>
                            <option value="230"> Consciousness Coaching</option>
                            <option value="174"> Consulate</option>
                            <option value="115"> Consultant</option>
                            <option value="29"> Cosmetic/Beauty Services</option>
                            <option value="108"> Counselor</option>
                            <option value="74"> CPR First Aid Training</option>
                            <option value="98"> Dance, Yoga and Aerial Arts Instruction</option>
                            <option value="237"> Dental Supply and Equipment Distributor</option>
                            <option value="77"> Dentist</option>
                            <option value="198"> Designer</option>
                            <option value="119"> Dietitian</option>
                            <option value="236"> Doctors Office</option>
                            <option value="38"> Dog Groomer</option>
                            <option value="238"> Dog Trainer</option>
                            <option value="37"> Driving School</option>
                            <option value="101"> Duct and Vent Cleaning</option>
                            <option value="212"> Editor</option>
                            <option value="72"> Education</option>
                            <option value="214"> Electrician</option>
                            <option value="194"> Employment Agency</option>
                            <option value="56"> Energetic Healing</option>
                            <option value="71"> Energy Healing</option>
                            <option value="178"> Entertainment</option>
                            <option value="86"> Equestrian Centre</option>
                            <option value="241"> Event</option>
                            <option value="240"> Event Build-a-Thon</option>
                            <option value="18"> Event Organizer</option>
                            <option value="118"> Family Entertainment Center</option>
                            <option value="102"> Feldenkrais Teacher</option>
                            <option value="114"> Fencing Club</option>
                            <option value="36"> Financial Advisor</option>
                            <option value="150"> Financial Coach</option>
                            <option value="197"> Fingerprinting Services</option>
                            <option value="182"> Fishing Charter</option>
                            <option value="53"> Fitness Bootcamps</option>
                            <option value="162"> Foot Care Clinic</option>
                            <option value="95"> Freelance Film Crew</option>
                            <option value="193"> Freelance Flight Instructor</option>
                            <option value="46"> Freelance Photographer</option>
                            <option value="204"> Gardener</option>
                            <option value="147"> Gemlight Practitioner</option>
                            <option value="218"> Glassblowing</option>
                            <option value="80"> Golf Clinic</option>
                            <option value="143"> Golf Club</option>
                            <option value="138"> Government Institution</option>
                            <option value="199"> Graphic Designer</option>
                            <option value="45"> Hair Stylist</option>
                            <option value="216"> Handyman</option>
                            <option value="24"> Health Clinic</option>
                            <option value="14"> Health Club</option>
                            <option value="185"> Health Coach</option>
                            <option value="117"> Helicopters</option>
                            <option value="130"> Home E-retailer</option>
                            <option value="84"> Home Security Service</option>
                            <option value="226"> Home Theater Consultant</option>
                            <option value="155"> Homeopath</option>
                            <option value="127"> Hospitality Services</option>
                            <option value="42"> Hot Air Ballooning</option>
                            <option value="234"> HVAC Contractor</option>
                            <option value="73"> Hypnotherapist</option>
                            <option value="92"> Hypnotist</option>
                            <option value="82"> Independent Contractor</option>
                            <option value="131"> Indoor Cricket</option>
                            <option value="224"> Indoor Cycling</option>
                            <option value="85"> Indoor Rock Climbing</option>
                            <option value="94"> Insurance Broker</option>
                            <option value="203"> Integrated Physical Medicine</option>
                            <option value="55"> Intuitive Readings</option>
                            <option value="48"> Intuitive Readings</option>
                            <option value="40"> IT Services</option>
                            <option value="217"> Lactation Consultant</option>
                            <option value="31"> Laser Hair Removal</option>
                            <option value="93"> Laser Tattoo Removal</option>
                            <option value="183"> Lavender Farming</option>
                            <option value="122"> Lawn Service</option>
                            <option value="180"> Legal Document Assistant</option>
                            <option value="191"> Legal Services</option>
                            <option value="83"> Librarian</option>
                            <option value="47"> Life Coaching</option>
                            <option value="213"> Locksmith</option>
                            <option value="89"> Maid Service</option>
                            <option value="57"> Marriage & Family Therapist</option>
                            <option value="61"> Martial Arts</option>
                            <option value="2"> Massage Therapist</option>
                            <option value="100"> Mediator</option>
                            <option value="235"> Medical Clinic</option>
                            <option value="190"> Medical Device Distributor</option>
                            <option value="129"> Medical Intuitive</option>
                            <option value="157"> Medical Records Repository</option>
                            <option value="58"> Meditation Instructor</option>
                            <option value="215"> MindBodyWise Therapy</option>
                            <option value="116"> Mobile Car Wash</option>
                            <option value="121"> Mobile Notary Public Service</option>
                            <option value="206"> Mobile Repair</option>
                            <option value="75"> Mortgage Broker</option>
                            <option value="125"> Moving Company</option>
                            <option value="35"> Music School</option>
                            <option value="104"> Music Studio</option>
                            <option value="15"> Nail Salon</option>
                            <option value="26"> Natural Medicine</option>
                            <option value="139"> Non Profit Organization</option>
                            <option value="176"> Notary Services</option>
                            <option value="165"> Numerologist</option>
                            <option value="63"> Nutrition & Pilates Centre</option>
                            <option value="136"> Nutritional Consultant</option>
                            <option value="69"> Nutritionist</option>
                            <option value="120"> Occupational Therapy</option>
                            <option value="87"> Offshore Watersports Instructor</option>
                            <option value="192"> Online Counselling Service</option>
                            <option value="156"> Online Marketing</option>
                            <option value="50"> Optician </option>
                            <option value="184"> Optometrist</option>
                            <option value="27"> Oriental Medicine</option>
                            <option value="64"> Osteopath</option>
                            <option value="33"> Other</option>
                            <option value="220"> Outdoor Training</option>
                            <option value="79"> Pastoral Counselor </option>
                            <option value="39"> Personal Trainer</option>
                            <option value="189"> Pest Control</option>
                            <option value="21"> Pet Services</option>
                            <option value="160"> Pharmacy</option>
                            <option value="188"> Photographer</option>
                            <option value="8"> Photography Studio</option>
                            <option value="20"> Physical Therapy</option>
                            <option value="96"> Physician</option>
                            <option value="141"> Piano Tuning and Repair</option>
                            <option value="10"> Pilates</option>
                            <option value="154"> Plumber</option>
                            <option value="41"> Pole Dancing</option>
                            <option value="231"> Private Yoga and Yoga Therapy</option>
                            <option value="44"> Professional Organizer</option>
                            <option value="210"> Proofreader</option>
                            <option value="171"> Property Services</option>
                            <option value="90"> Psychiatry Practice</option>
                            <option value="166"> Psychic</option>
                            <option value="128"> Psychologist</option>
                            <option value="60"> Psychotherapist</option>
                            <option value="208"> R.V Technician</option>
                            <option value="34"> Real Estate</option>
                            <option value="225"> Reflexology</option>
                            <option value="173"> Reiki</option>
                            <option value="52"> Renewable Energy Installer</option>
                            <option value="133"> Repair/Resale</option>
                            <option value="54"> Romance Coach</option>
                            <option value="59"> Salon</option>
                            <option value="201"> Schools</option>
                            <option value="135"> Scuba</option>
                            <option value="91"> Singing Telegram Services</option>
                            <option value="23"> Skin Care</option>
                            <option value="209"> Slaughter Facility</option>
                            <option value="99"> Social Media Manager</option>
                            <option value="3"> Spa</option>
                            <option value="167"> Spirit Art and Color Therapy</option>
                            <option value="68"> Spiritual Relationship Therapist</option>
                            <option value="179"> Spiritual Teacher</option>
                            <option value="186"> sports coaching</option>
                            <option value="142"> Tailoring</option>
                            <option value="16"> Tanning Salon</option>
                            <option value="11"> Tattooing</option>
                            <option value="5"> Tax Advisor</option>
                            <option value="222"> Television Studio</option>
                            <option value="109"> Therapist</option>
                            <option value="219"> Thermographer</option>
                            <option value="221"> Translation and Interpreting services</option>
                            <option value="126"> Transportation</option>
                            <option value="76"> Transportation Shuttle Service</option>
                            <option value="49"> Tutor</option>
                            <option value="7"> Tutor or Trainer</option>
                            <option value="140"> Veteran Services</option>
                            <option value="97"> Veterinarian</option>
                            <option value="137"> Video Chat</option>
                            <option value="88"> Video Producer</option>
                            <option value="159"> Virtual Assistant</option>
                            <option value="107"> Vocal Coach</option>
                            <option value="148"> Watersport Company</option>
                            <option value="30"> Waxing Services</option>
                            <option value="200"> Web Designer</option>
                            <option value="195"> Wedding</option>
                            <option value="196"> Wedding and Event Venue</option>
                            <option value="144"> Weight Control Therapist</option>
                            <option value="151"> Window Cleaning</option>
                            <option value="149"> Window Tinting</option>
                            <option value="211"> Writer</option>
                            <option value="25"> Yoga</option>
                            <option value="132"> Yoga, Meditation and Life Coach</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12">
                        <p>Timezone</p>
                        <div class="form-group">
                            <select class="form-control">
                            <option value="9">Midway Island (GMT -11:20)</option>
                            <option value="1">Hawai (GMT -10:00)</option>
                            <option value="11">Tahiti (GMT -10:00)</option>
                            <option value="2">Alaska (GMT -09:00)</option>
                            <option value="180">GAMT - Gambier Time(GMT -9:00)</option>
                            <option value="3">Pacific Time (US & Canada) (GMT -08:00)</option>
                            <option value="12">Tijuana (GMT -08:00)</option>
                            <option value="13">Chihuahua (GMT -07:00)</option>
                            <option value="14">Baja Sur, Mexico(GMT -07:00)</option>
                            <option value="15">Mazatlan(GMT -07:00)</option>
                            <option value="4">Arizona (GMT -07)</option>
                            <option value="5">Mountain Time (US & Canada)(GMT -07:00)</option>
                            <option value="167">Pacific Daylight Time (US & Canada) (GMT -07:00)</option>
                            <option value="168">Mountain Daylight Time(GMT -06:00)</option>
                            <option value="165">San Jose(GMT -06:00)</option>
                            <option value="6">Central Time (US & Canada) (GMT -06:00)</option>
                            <option value="16">Mexico City (GMT -06:00)</option>
                            <option value="17">Monterrey(GMT -06:00)</option>
                            <option value="26">Saskatchewan, Canada(GMT -06:00)</option>
                            <option value="191">Central Standard Time (GMT -06:00)</option>
                            <option value="188">Cayman Islands time (GMT -05:00)</option>
                            <option value="172">Central Daylight Time(GMT -05:00)</option>
                            <option value="18">Bogota (GMT -05:00)</option>
                            <option value="19">Lima, Quito(GMT -05:00)</option>
                            <option value="7">Eastern Time (US & Canada) (GMT -05:00)</option>
                            <option value="8">Indiana (East)(GMT -05:00)</option>
                            <option value="150">Eastern Standard Time(EST)(GMT -05:00)</option>
                            <option value="21">Caracas(GMT -04:30)</option>
                            <option value="173">Venezuelan Standard Time(GMT -04:30)</option>
                            <option value="187">Atlantic Standard Time (GMT -04:00)</option>
                            <option value="194">Guyana Time(GMY -04:00)</option>
                            <option value="22">La Paz, Bolivia(GMT -04:00)</option>
                            <option value="23">Santiago(GMT -04:00)</option>
                            <option value="151">Atlantic Standard Time(AST)(GMT -04:00)</option>
                            <option value="171">Eastern Daylight Savings(GMT -04:00)</option>
                            <option value="176">Atlantic Standard Time (Philipsburg) (GMT -04:00)</option>
                            <option value="177">Atlantic Standard Time (Curacao)(GMT -04:00)</option>
                            <option value="163">Saint Kitts and Nevis(GMT -04:00)</option>
                            <option value="20">Atlantic Time (Canada) (GMT -04:00)</option>
                            <option value="24">Newfoundland (GMT -03:30)</option>
                            <option value="25">Buenos Aires, Georgetown (GMT -03:00)</option>
                            <option value="149">Montevideo (GMT -03:00)</option>
                            <option value="175">Brasilia, Sao Paulo (GMT -03:00)</option>
                            <option value="27">Azores (GMT -01:00)</option>
                            <option value="28">Cape Verde Is.(GMT -01:00)</option>
                            <option value="29">Casablanca (GMT 00:00)</option>
                            <option value="30">Dublin (GMT 00:00)</option>
                            <option value="31">Lisbon (GMT 00:00)</option>
                            <option value="32">London, Edinburgh (GMT 00:00)</option>
                            <option value="33">Monrovia (GMT 00:00)</option>
                            <option value="164">Western European Standard Time(WET)(GMT 00:00)</option>
                            <option value="166">Gambia Standard Time(GMT 00:00)</option>
                            <option value="148">Central European Time (CET) (GMT +01:00)</option>
                            <option value="152">West Africa Time(WAT)(GMT +01:00)</option>
                            <option value="155">British Summer Time(BST)(GMT +01:00)</option>
                            <option value="34">Amsterdam (GMT +01:00)</option>
                            <option value="35">Belgrade(GMT +01:00)</option>
                            <option value="36">Berlin, Bern(GMT +01:00)</option>
                            <option value="37">Bratislava(GMT +01:00)</option>
                            <option value="38">Brussels(GMT +01:00)</option>
                            <option value="39">Budapest(GMT +01:00)</option>
                            <option value="40">Copenhagen(GMT +01:00)</option>
                            <option value="41">Ljubljana(GMT +01:00)</option>
                            <option value="42">Madrid(GMT +01:00)</option>
                            <option value="43">Paris(GMT +01:00)</option>
                            <option value="44">Prague(GMT +01:00)</option>
                            <option value="45">Rome(GMT +01:00)</option>
                            <option value="46">Sarajevo(GMT +01:00)</option>
                            <option value="47">Skopje(GMT +01:00)</option>
                            <option value="48">Stockholm(GMT +01:00)</option>
                            <option value="49">Vienna(GMT +01:00)</option>
                            <option value="50">Warsaw(GMT +01:00)</option>
                            <option value="51">Zagreb(GMT +01:00)</option>
                            <option value="190">Oslo (GMT +01:00)</option>
                            <option value="186">Eastern European Summer Time(GMT +2:00)</option>
                            <option value="183">Amman(GMT +02:00)</option>
                            <option value="184">Beirut(GMT +02:00)</option>
                            <option value="181">Nicosia (GMT +2:00)</option>
                            <option value="52">Athens (GMT +02:00)</option>
                            <option value="53">Bucharest(GMT +02:00)</option>
                            <option value="54">Cairo(GMT +02:00)</option>
                            <option value="55">Johannesburg, Harare(GMT +02:00)</option>
                            <option value="56">Helsinki(GMT +02:00)</option>
                            <option value="57">Istanbul(GMT +02:00)</option>
                            <option value="58">Jerusalem(GMT +02:00)</option>
                            <option value="59">Kyiv(GMT +02:00)</option>
                            <option value="61">Riga(GMT +02:00)</option>
                            <option value="62">Sofia(GMT +02:00)</option>
                            <option value="63">Tallinn(GMT +02:00)</option>
                            <option value="64">Vilnius(GMT +02:00)</option>
                            <option value="147">Central European Summer Time (CEST) (GMT +02:00)</option>
                            <option value="178">South Africa Standard Time(GMT +02:00)</option>
                            <option value="179">Jordan Standard Time(Amman)(GMT +3:00)</option>
                            <option value="169">Eastern European Summer Time(GMT +3:00)</option>
                            <option value="156">Kaliningrad Time(GMT +03:00)</option>
                            <option value="153">East Africa Time(EAT)(GMT +03:00)</option>
                            <option value="154">Manama(GMT +03:00)</option>
                            <option value="65">Baghdad (GMT +03:00)</option>
                            <option value="66">Kuwait(GMT +03:00)</option>
                            <option value="67">Moscow, St. Petersburg, Volgograd(GMT +03:00)</option>
                            <option value="68">Nairobi(GMT +03:00)</option>
                            <option value="69">Riyadh(GMT +03:00)</option>
                            <option value="60">Minsk(GMT +03:00)</option>
                            <option value="174">Doha(GMT +03:00)</option>
                            <option value="70">Tehran (GMT +03:30)</option>
                            <option value="71">Abu Dhabi (GMT +04:00)</option>
                            <option value="72">Baku(GMT +04:00)</option>
                            <option value="73">Muscat(GMT +04:00)</option>
                            <option value="74">Tbilisi(GMT +04:00)</option>
                            <option value="75">Yerevan(GMT +04:00)</option>
                            <option value="157">Samara Time(GMT +04:00)</option>
                            <option value="170">Moscow Daylight Time(GMT +4:00)</option>
                            <option value="185">Port Louis(GMT +04:00)</option>
                            <option value="189">Paraguay Standard Time (GMT -04:00)</option>
                            <option value="76">Kabul (GMT +04:30)</option>
                            <option value="78">Karachi, Islamabad(GMT +05:00)</option>
                            <option value="79">Tashkent(GMT +05:00)</option>
                            <option value="80">Calcutta, Mumbai, Delhi (GMT +05:30)</option>
                            <option value="162">Sri Lanka Time(SLT)(GMT +05:30)</option>
                            <option value="161">Nepal Time (NPT)(GMT +05:45)</option>
                            <option value="81">Almaty (GMT +06:00)</option>
                            <option value="82">Dhaka, Astana(GMT +06:00)</option>
                            <option value="83">Novosibirsk(GMT +06:00)</option>
                            <option value="77">Ekaterinburg (GMT +06:00)</option>
                            <option value="84">Rangoon (GMT +06:30)</option>
                            <option value="85">Bangkok, Hanoi (GMT +07:00)</option>
                            <option value="86">Jakarta(GMT +07:00)</option>
                            <option value="87">Krasnoyarsk(GMT +07:00)</option>
                            <option value="192">Indochina Time (GMT +07:00)</option>
                            <option value="193">China Time Zone</option>
                            <option value="182">Hong Kong (GMT +08:00)</option>
                            <option value="88">Beijing, Chongqing (GMT +08:00)</option>
                            <option value="89">Hong Kong, Manila(GMT +08:00)</option>
                            <option value="90">Irkutsk(GMT +08:00)</option>
                            <option value="91">Kuala Lumpur(GMT +08:00)</option>
                            <option value="92">Perth(GMT +08:00)</option>
                            <option value="93">Singapore(GMT +08:00)</option>
                            <option value="94">Taipei(GMT +08:00)</option>
                            <option value="95">Ulaan Bataar(GMT +08:00)</option>
                            <option value="96">Urumqi(GMT +08:00)</option>
                            <option value="97">Osaka, Sapporo, Japan (GMT +09:00)</option>
                            <option value="98">Seoul(GMT +09:00)</option>
                            <option value="99">Tokyo(GMT +09:00)</option>
                            <option value="100">Yakutsk(GMT +09:00)</option>
                            <option value="101">Adelaide (GMT +09:30)</option>
                            <option value="102">Darwin(GMT +09:30)</option>
                            <option value="103">Brisbane (GMT +10:00)</option>
                            <option value="104">Canberra(GMT +10:00)</option>
                            <option value="105">Guam, Port Moresby(GMT +10:00)</option>
                            <option value="106">Hobart(GMT +10:00)</option>
                            <option value="107">Melbourne(GMT +10:00)</option>
                            <option value="108">Sydney(GMT +10:00)</option>
                            <option value="158">Vladivostok Time(GMT +10:00)</option>
                            <option value="159">Magadan Time(GMT +10:00)</option>
                            <option value="160">Kamchatka Time(GMT +12:00)</option>
                            <option value="109">Auckland, Wellington (GMT +12:00)</option>
                            <option value="110">Fiji(GMT +12:00)</option>
                            <option value="10">Samoa(GMT +13:00)</option>
                            </select>

                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="modal-body-inner-tabs">

                        <div class="row">
                            <div class="col-lg-12">
                                <ul class="list-inline pull-right">
                                    <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;">Cancel</button></li>
                                    <li><button class="btn btn-main" style="padding-right: 19px; padding-left: 19px;">Save</button></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-default btn-prev" type="button">Prev</button>
                <button class="btn btn-default btn-next" type="button">Next</button>
                <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="pay2">
    <div class="modal-dialog modal-dialog-customer">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="list-inline list-modal-header">
                    <li class="text-uppercase"><i class="fa fa-briefcase" style="color: #FC7600;"></i>Business Information</li>
                <!--    <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                        <a href=""><i class="fa fa-reply"></i></a>
                    </li>
                    <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                        <a href=""><i class="fa fa-plus"></i></a>
                    </li>
                    <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban"><i class="fa fa-ban"></i></a></li>
                    <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay"><i class="fa fa-dollar"></i></a></li>-->
                </ul>
            </div>
            <div class="modal-body">
                <?php Pjax::begin(); ?>

                <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-lg-6 col-md-6">
            <?= $form->field($business_information, 'business_name')->textInput(['maxlength' => true]) ?>

        </div>
        <div class="col-lg-6 col-md-6">
            <?= $form->field($business_information, 'business_phone')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <!--   <div class="row">
           <div class="col-lg-12">
               <p class="text-muted pull-right" style="margin-top: -9px;"><small>Customer restrictions of the selected membership will be applied automatically.</small></p>
           </div>
       </div> -->
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <!--   <textarea class="form-control" rows="2" placeholder="Note against payment"></textarea>-->
                <?= $form->field($business_information, 'business_description')->textarea(['rows' => 2]) ?>
                <p class="text-danger pull-right"><small>Please do not enter html tags</small></p>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12 col-md-12">
            <?= $form->field($business_information, 'address')->textInput(['maxlength' => true]) ?>
        </div>
        <!--  <div class="modal-body-inner-tabs">
              <div class="row">
                  <div class="col-lg-4">
                      <p class="txt-theme">Amount</p>
                      <div class="form-group">
                          <input type="text" class="form-control" placeholder="Additional Charges">
                      </div>
                  </div>
                  <div class="col-lg-4">
                      <div class="form-group">
                          <span style="position: absolute; left: -2px;">$</span> <input type="text" class="form-control" placeholder="$$$">
                      </div>
                      <div class="form-group">
                          <input type="text" class="form-control" placeholder="Discount">
                      </div>
                  </div>
                  <div class="col-lg-4">
                      <div class="form-group">
                          <span style="position: absolute; left: -6px;">by</span>
                          <select class="form-control" placeholder="Cash">
                              <option selected="">Cash</option>
                              <option>d</option>
                          </select>
                      </div>
                      <div class="form-group">
                          <input type="text" class="form-control" placeholder="Total">
                      </div>
                  </div>
              </div>
              <div class="row">
                  <div class="col-lg-12">
                      <ul class="list-inline pull-right">
                          <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;">Cancel</button></li>
                          <li><button class="btn btn-main" style="padding-right: 19px; padding-left: 19px;">Save</button></li>
                      </ul>
                  </div>
              </div>
          </div>-->
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <?= $form->field($business_information, 'country')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
            <?= $form->field($business_information, 'state')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6">
            <?= $form->field($business_information, 'city')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
            <?= $form->field($business_information, 'zip')->textInput(['maxlength' => true]) ?>
        </div>
    </div>
    <?= Html::submitButton($business_information->isNewRecord ? 'Create' : 'Update', ['class' => $business_information->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
                <?php ActiveForm::end(); ?>
                <?php Pjax::end(); ?>

    </div>
    <div class="modal-footer">
        <button class="btn btn-default btn-prev" type="button">Prev</button>
        <button class="btn btn-default btn-next" type="button">Next</button>
        <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
    </div>
    </div>
    </div>
    </div>

    <div class="modal fade" id="pay3">
        <div class="modal-dialog modal-dialog-customer">
            <div class="modal-content">
                <div class="modal-header">
                    <ul class="list-inline list-modal-header">
                        <li class="text-uppercase"><i class="fa fa-dollar" style="color: #FC7600;"></i> add membership payment</li>
                        <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                            <a href=""><i class="fa fa-reply"></i></a>
                        </li>
                        <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                            <a href=""><i class="fa fa-plus"></i></a>
                        </li>
                        <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban"><i class="fa fa-ban"></i></a></li>
                        <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay"><i class="fa fa-dollar"></i></a></li>
                    </ul>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6 col-md-6">
                            <p>Payment Date</p>
                            <div class="form-group">
                                <input type="text" placeholder="4-17-2016" class="form-control">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <p>Payment For</p>
                            <div class="form-group">
                                <input type="text" placeholder="Add New Payment" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p class="text-muted pull-right" style="margin-top: -9px;"><small>Customer restrictions of the selected membership will be applied automatically.</small></p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <textarea class="form-control" rows="2" placeholder="Note against payment"></textarea>
                                <p class="text-danger pull-right"><small>Please do not enter html tags</small></p>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="modal-body-inner-tabs">
                            <div class="row">
                                <div class="col-lg-4">
                                    <p class="txt-theme">Amount</p>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Additional Charges">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <span style="position: absolute; left: -2px;">$</span> <input type="text" class="form-control" placeholder="$$$">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Discount">
                                    </div>
                                </div>
                                <div class="col-lg-4">
                                    <div class="form-group">
                                        <span style="position: absolute; left: -6px;">by</span>
                                        <select class="form-control" placeholder="Cash">
                                            <option selected="">Cash</option>
                                            <option>d</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Total">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="list-inline pull-right">
                                        <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;">Cancel</button></li>
                                        <li><button class="btn btn-main" style="padding-right: 19px; padding-left: 19px;">Save</button></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-default btn-prev" type="button">Prev</button>
                    <button class="btn btn-default btn-next" type="button">Next</button>
                    <button class="btn btn-default" data-dismiss="modal" type="button">Close</button>
                </div>
            </div>
        </div>
    </div>

 <?php }
    ?>




  <?php  $script =  "$('#demo12 li').click(function() {
    $(this).addClass('active').siblings().removeClass('active');
    });
    $('#blocked').click();
    ";
        $this->registerJs($script); ?>