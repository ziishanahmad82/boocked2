<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Toughbooks */

$this->title = 'Update Toughbooks: ' . ' ' . $model->toughbook_id;
$this->params['breadcrumbs'][] = ['label' => 'Toughbooks', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->toughbook_id, 'url' => ['view', 'id' => $model->toughbook_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="toughbooks-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
