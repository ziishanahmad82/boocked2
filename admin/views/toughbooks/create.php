<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Toughbooks */

$this->title = 'Create Toughbooks';
$this->params['breadcrumbs'][] = ['label' => 'Toughbooks', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="toughbooks-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
