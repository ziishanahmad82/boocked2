<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Toughbooks */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="toughbooks-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'toughbook_code')->textInput(['maxlength' => true]) ?>


    <?= $form->field($model, 'toughbook_status')->dropDownList(Array('active' => 'Active', 'inactive' => 'In-Active' )) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
