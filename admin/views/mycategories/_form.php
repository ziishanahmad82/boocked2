<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Services */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="col-lg-12 col-md-12 col-sm-12 top">

    <?php $form = ActiveForm::begin(['id'=> 'servicecreatedform','options' => ['enctype' => 'multipart/form-data']]); ?>

        <p>Category</p>
        <div id="catselctbox">
        <div class="form-group">

            <select class="form-control" name="Services[service_cat_id]" id="category_createselectebox">
              <?php $i=1; ?>
                <?php foreach($Category_model as $category){ ?>
                <option value="<?=$category->service_cat_id?>" <?php if($model->service_cat_id==$category->service_cat_id){?> selected="" <?php } ?>><?=$category->service_category_name?></option>
                <?php $i++;
                } ?>
               
            </select>
            <small>
                <a style="color: #FC7600 !important;text-decoration: underline !important;" href="#" onclick="serviceCatboxShow(event,'#catselctbox','#catinputbox')"  id="addNewServiceCat">New Category</a> |<a style="color: #FC7600 !important;text-decoration: underline !important;" href="#" onclick="deletecategory(event,'#category_createselectebox')"> Delete Category</a>
            </small>
        </div>

            </div>
        <div id="catinputbox" style="display:none">
            <div class="form-group">
            <input type="text" name="service_category_name" class="form-control" >
                <small>
                    <a style="color: #FC7600 !important;text-decoration: underline !important;" href="#" onclick="serviceCatboxShow(event,'#catinputbox','#catselctbox')" id="addNewServiceCat">Show List</a>
                </small>

            </div>

        </div>

    <?= $form->field($model, 'service_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'service_description')->textarea(['rows' => 6]) ?>
<div class="col-lg-6 col-md-6 col-sm-6" style="padding-left: 0">
    <?= $form->field($model, 'service_duration')->textInput() ?>
</div>
    <div class="col-lg-6 col-md-6 col-sm-6" style="padding-right: 0">
    <?= $form->field($model, 'service_price')->textInput(['maxlength' => true]) ?>
        </div>
    <div class="col-lg-6 col-md-6 col-sm-6" style="padding-left: 0">
    <?= $form->field($model, 'service_capacity')->textInput() ?>
     </div>
<div class="clearfix"></div>
    <?= $form->field($model, 'imageFile1')->fileInput() ?>
    <?= $form->field($model, 'imageFile2')->fileInput() ?>
    <?= $form->field($model, 'imageFile3')->fileInput() ?>
    <?= $form->field($model, 'service_video')->fileInput() ?>

<!--    --><?//= $form->field($model, 'business_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success btn btn-warning ad1' : 'btn btn-primary btn btn-warning ad1 service_updatesubmit']) ?>
    </div>
        <div class="alert alert-success" style="display:none">
            <strong>Successfully Created!</strong>.
        </div>
    <?php ActiveForm::end(); ?>

</div>
    