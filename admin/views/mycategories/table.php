<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use app\models\ProfessionsList;
use yii\helpers\Url;
use app\models\Services;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ServicesSearch */
/* @var $servicesCommonNameData app\models\ServicesCommonName */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\widgets\Pjax;

$this->title = 'Categories';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php // print_r($services_categories); ?>
<?php
 if(Yii::$app->session->get('bk_singular_service')!=""){   $singular_service_name = Yii::$app->session->get('bk_singular_service'); }else { $singular_service_name = 'service'; }
if(Yii::$app->session->get('bk_plural_service')!=""){   $plural_service_name = Yii::$app->session->get('bk_plural_service'); }else { $plural_service_name = 'services'; }

?>



<div class="col-lg-12 col-md-12 col-sm-12 media-xs-full">
    <div class="panel panel-default">
        <div class="panel-heading full">
            <ul class="list-inline list-staff">
                <li><h5 class="panel-title text-uppercase"><i class="fa fa-clock-o" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Categories</span></h5></li>
            </ul>
        </div>
        <?php if (Yii::$app->user->identity->role ==1){?>
            <div class="panel-body">
                <div class="row">
                    <div class="tab-content">
                        <div class="tab-pane fade active in" id="service">
                            <div class="panel-collapse collapse in active">
                                <div class="panel-body" style="border: none;">
                                    <div class="panel panel-default">
                                        <div class="panel-heading full">
                                            <ul class="list-inline list-staff">
                                                <li><h5 class="panel-title"><i class="fa fa-file" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Professionals Categories</span></h5></li>
                                                <li class="pull-right li-right hidden-xs">Add professionals Categories</li>
                                            </ul>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <table class="table table-responsive table-striped tab-reports" id="example">
                                                    <thead>
                                                    <tr class="table-panel-head row-panel-head">
                                                        <th>ID</th>
                                                        <th>Name</th>
                                                        <th>Image </th>
                                                        <th>Pressed Image </th>
                                                        <th>Parent ID </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php
                                                    $lists = ProfessionsList::find()->all();
                                                    foreach($lists as $list){ ?>
                                                        <tr>
                                                            <td><?= $list->profession_id ?></td>
                                                            <td><?= $list->profession_name ?></td>
                                                            <td><img src="<?= $list->image ?>" width="120px" height="120px"></td>
                                                            <td><img src="<?= $list->image_pressed ?>" width="120px" height="120px"></td>
                                                            <td><?= $list->pro_parent ?></td>
                                                        </tr>
                                                    <?php } ?>
                                                    </tbody>


                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php
        }
        ?>

                    </div>

                </div>









































    <?php // ActiveForm::end();
     // Pjax::end(); ?>


<div class="clearfix"></div>



<?php
$script = "


$('#updatecreateform').load('".Yii::getAlias('@web')."/index.php/services/create');

$('form#sevicecommonform :text').change(function(){
    $('form#sevicecommonform').submit();
   
});

$('body').on('beforeSubmit', 'form#sevicecommonform', function () {
var form = $(this);
// return false if form still have some validation errors
if (form.find('.has-error').length) {
return false;
}
// submit form
$.ajax({
url: form.attr('action'),
type: 'post',
data: form.serialize(),
success: function (response) {
    if(response==1){
       $('#singularsaved').css('display','inline-block');
       setTimeout(function(){
         $('#singularsaved').hide('slow');
            },2000);
    }

    else {
        alert('not updated');

    }
}
});
return false;
});

$('body').on('beforeSubmit', '#updatecreateform form', function () {
var form = $(this);
// return false if form still have some validation errors
if (form.find('.has-error').length) {
return false;
}
alert('aaa');
  var form_data = form.serialize();
  var formData = new FormData(this);
// submit form
$.ajax({
url: form.attr('action'),
type: 'post',
//data: form.serialize(),
 data:formData,
  contentType: false,
        processData: false,
success: function (response) {
    if(response==1){
   document.getElementById('servicecreatedform').reset();
$('#updatecreateform .alert.alert-success').show();
setTimeout(function () {
$('#updatecreateform .alert.alert-success').hide();
}, 3000);
     $.pjax.reload({container:'#srvicereloadsec',timeout:60000});

    }else if(response==2) {
    $('#updatecreateform').load('".Yii::getAlias('@web')."/index.php/services/create');

     $.pjax.reload({container:'#srvicereloadsec',timeout:60000});
   }
}
});
return false;
});



$('.service_updatesubmit').click(function(event){

});
$('.timepicker').timepicker({
        showPeriod: true
      
    });
 $('.staffrecurngtime').on('submit', function( event ) {

    event.preventDefault();
    var formdate = $(this).serialize();
    $.ajax({
url:'schedule',
type: 'post',
data: formdate,
success: function (response) {
    if(response==1){
     //   alert('changed');

    }else {
       // alert('not updated');

    }
}
});
    console.log(formdate);
 }); 
   $('.addupdate_allworking').click(function(event){
  event.preventDefault();
var linkattr =  $(this).attr('href');
var divid =  $(this).siblings('div').attr('id');

$('#'+divid).load(linkattr);

   });
   
   
 

    $('.staffimageform').on('submit',(function(e) {
        e.preventDefault();
        
        var formData = new FormData(this);
        var formid = $(this).attr('id');

        $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
               if(data!= '0'){
               alert('dfasf');
               $('#'+formid).siblings('img').attr('src','".Yii::getAlias('@web')."/'+data)
               }
            },
            error: function(data){
                console.log('error');
                console.log(data);
            }
        });
    }));

    $('.staffimagelog').on('change', function() {
   
        $(this).parent('form').submit();
    });
";
$this->registerJs($script);

$script2 = "
$('#example').DataTable( {
dom: 'Bfrtip',
buttons: [
'copy', 'csv', 'excel', 'pdf', 'print'
]
} );




";

$this->registerJs($script2);
?>

<?php
$script2 ="jQuery(document).on('click', '.showModalButton', function(){
        if ($('#modal').data('bs.modal').isShown) {
            $('#modal').find('#modalContent')
                .load($(this).attr('value'));
            document.getElementById('modalHeader').innerHTML ='<h4>' + $(this).attr('title')+'</h4>';
        } else {
            $('#modal').modal('show').find('#modalContent').load($(this).attr('value'));
            document.getElementById('modalHeader').innerHTML = '<h4>' + $(this).attr('title') + '</h4>';
        }
    });
" ;
    $this->registerJs($script2);
?>


<script type="text/javascript">

</script >
<script>
    function EnableDisableService(event,xur) {
        event.preventDefault();
        $.ajax({
            url:$(xur).attr('href'),
            type: 'post',
            success: function (response) {
                if(response==1){
                    $.pjax.reload({container:'#srvicereloadsec',timeout:60000});

                }
            }
        });
    }
    function serviceCatboxShow(event,hideitem,showitem){
        event.preventDefault();
        $(hideitem).hide();
        $(showitem).show();

    }
    function deletecategory(event,selectbox){
        event.preventDefault();
        var optionRe = $(selectbox+' option:selected');
        var option_value = $(selectbox+' option:selected').val();
        var option_name = $(selectbox+' option:selected').text();
        if (confirm("Are you sure you want "+option_name+" delete?All services related to categories also deleted") == true) {

            $.ajax({
                url:'categorydelete',
                type: 'post',
                data:{'id':option_value},
                success: function (response) {
                    if(response==1){

                        $(optionRe).remove();
                        $.pjax.reload({container:'#srvicereloadsec',timeout:60000});



                    }else {
                       // alert('not updated');

                    }
                }
            });
            

        } else {
            x = "You pressed Cancel!";
        }
    }
    function updatelinkedresource(event,xor){
        event.preventDefault();
        var formid = $(xor).parents('form').attr('id');
        $.ajax({
            url: $('#'+formid).attr('action'),
            type: 'post',
            data:$('#'+formid).serialize(),
            success: function (response) {
                if(response==1){

                   $('#'+formid+' .sucalert').show();
                    setTimeout(function () {
                        $('#'+formid+' .sucalert').hide();
                    }, 3000);



                }else {
                    // alert('not updated');

                }
            }
        });

    }

 //   $('.staff_serviceslots').click(function(event){
    function staff_serviceslots(event,xor){
        event.preventDefault();
        var linkattr =  $(xor).attr('href');
        var divid =  $(xor).parent('p').siblings('div').attr('id');
        $('#'+divid).load(linkattr);


        //$('#scdemodal').modal('show').find('#modalscedule').load($(this).attr('href'));
    }
    function changeProfile(event,xor){
        event.preventDefault();
        $(xor).siblings().click();
    }
    function showinputname(event,xor){
        event.preventDefault();
        $(xor).parents('p').hide();
        var show_catid = $(xor).attr('href');
        $(show_catid).show();

    }
    function servicecatSave(event,xor){
        event.preventDefault();
        var id = $(xor).attr('data-servicecat');
        var catname = $('#editcatid'+id+' input').val();
        if(catname!=''){
            $.ajax({
                url: 'updatecategoryname?id='+id,
                type: 'post',
                data: {service_category_name : catname},
                success: function (response) {
                    if(response==1){
                        $('#editcatname'+id+' span').text(catname);
                        $('#editcatname'+id+'').show();
                        $('#editcatid'+id+'').hide();

                    }else {


                    }
                }
            });
        }
        //  alert(id);

        return false;
    }

    function textthemeeditservice(event,xor){
        event.preventDefault();

        $('#updatecreateform').load($(xor).attr('href'));
        //$('#modal').modal('show').find('#modalContent').load($(this).attr('href'));
    }

</script>

<?php Pjax::begin();
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'modal',
    'size' => 'modal-sm',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);
echo "<div id='modalContent'></div>";
yii\bootstrap\Modal::end();
Pjax::end(); ?>
<?php Pjax::begin();
yii\bootstrap\Modal::begin([
    'headerOptions' => ['id' => 'modalHeader'],
    'id' => 'scdemodal',
    'size' => 'modal-md',
    //keeps from closing modal with esc key or by clicking out of the modal.
    // user must click cancel or X to close
    'clientOptions' => ['backdrop' => 'static', 'keyboard' => FALSE]
]);
echo "<div id='modalscedule'></div>";
yii\bootstrap\Modal::end();
Pjax::end(); ?>











