<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model app\models\Services */
/* @var $form yii\widgets\ActiveForm */


if($weeklytime) {
    ?>
    <?php foreach ($weeklytime as $weeklytimes) {
        $weekly_id = $weeklytimes->wrt_id;
        ?>
        <form class="staffrecurngtime12" action="<?= Url::to(['services/updaterecurslots', 'id' =>$weekly_id ]);?>" id="wrtform<?= $weeklytimes->wrt_id ?>">
            <div class="col-sm-6">
                <input type="hidden" value="<?php // print_r($service_id)?>" name="service_id">

            </div>
            <?php $sunday = json_decode($weeklytimes->sunday, true);
            $monday = json_decode($weeklytimes->monday, true);
            $tuesday = json_decode($weeklytimes->tuesday, true);
            $wednesday = json_decode($weeklytimes->wednesday, true);
            $thursday = json_decode($weeklytimes->thursday, true);
            $friday = json_decode($weeklytimes->friday, true);
            $saturday = json_decode($weeklytimes->saturday, true);
            $sunday_size = sizeof($sunday['from']);
            $monday_size = sizeof($monday['from']);
            $tuesday_size = sizeof($tuesday['from']);
            $wednesday_size = sizeof($wednesday['from']);
            $thursday_size = sizeof($thursday['from']);
            $friday_size = sizeof($friday['from']);
            $saturday_size = sizeof($saturday['from']);
            ?>

            <?php for ($x = 0; $x != $sunday_size; $x++) {

                ?>
                <div class="col-sm-12">
                    <div class="col-sm-2">Sun</div>
                    <div class="col-sm-2"><input type="text" value="<?= $sunday['from'][$x] ?>" name="staffsun[from][]"
                                                 class="timepicker1 form-control"></div>
                    <div class="col-sm-1"> to</div>
                    <div class="col-sm-2"><input type="text" value="<?= $sunday['to'][$x] ?>" name="staffsun[to][]"
                                                 class="timepicker1 form-control"></div>
                    <?php if($x==0){ ?><a onclick="add_more(event,this)" href="#">Add more</a><?php  } else { ?>
                        <a onclick="remove_more(event,this)" href="#">Remove</a> <?php } ?>
                </div>
            <?php }
            ?>
            <?php for ($x = 0; $x != $monday_size; $x++) { ?>

                <div class="col-sm-12">
                    <div class="col-sm-2">Mon</div>
                    <div class="col-sm-2"><input type="text" value="<?= $monday['from'][$x] ?>" name="staffmon[from][]"
                                                 class="timepicker1 form-control"></div>
                    <div class="col-sm-1"> to</div>
                    <div class="col-sm-2"><input type="text" name="staffmon[to][]" value="<?= $monday['to'][$x] ?>"
                                                 class="timepicker1 form-control"></div>
                    <?php if($x==0){ ?><a onclick="add_more(event,this)" href="#">Add more</a><?php  } else { ?>
                        <a onclick="remove_more(event,this)" href="#">Remove</a> <?php } ?>
                </div>
            <?php } ?>
            <?php for ($x = 0; $x != $tuesday_size; $x++) { ?>
                <div class="col-sm-12">
                    <div class="col-sm-2">Tue</div>
                    <div class="col-sm-2"><input type="text" value="<?= $tuesday['from'][$x] ?>" name="stafftue[from][]"
                                                 class="timepicker1 form-control"></div>
                    <div class="col-sm-1"> to</div>
                    <div class="col-sm-2"><input type="text" name="stafftue[to][]" value="<?= $tuesday['to'][$x] ?>"
                                                 class="timepicker1 form-control"></div>
                    <?php if($x==0){ ?><a onclick="add_more(event,this)" href="#">Add more</a><?php  } else { ?>
                        <a onclick="remove_more(event,this)" href="#">Remove</a> <?php } ?>
                </div>
            <?php } ?>
            <?php for ($x = 0; $x != $wednesday_size; $x++) { ?>
                <div class="col-sm-12">
                    <div class="col-sm-2">Wed</div>
                    <div class="col-sm-2"><input type="text" value="<?= $wednesday['from'][$x] ?>"
                                                 name="staffwed[from][]" class="timepicker1 form-control"></div>
                    <div class="col-sm-1"> to</div>
                    <div class="col-sm-2"><input type="text" name="staffwed[to][]" value="<?= $wednesday['to'][$x] ?>"
                                                 class="timepicker1 form-control"></div>
                    <?php if($x==0){ ?><a onclick="add_more(event,this)" href="#">Add more</a><?php  } else { ?>
                        <a onclick="remove_more(event,this)" href="#">Remove</a> <?php } ?>
                </div>
            <?php }
            for ($x = 0; $x != $thursday_size; $x++) { ?>
                <div class="col-sm-12">
                    <div class="col-sm-2">Thurs</div>
                    <div class="col-sm-2"><input type="text" value="<?= $thursday['from'][$x] ?>"
                                                 name="staffthurs[from][]" class="timepicker1 form-control"></div>
                    <div class="col-sm-1"> to</div>
                    <div class="col-sm-2"><input type="text" name="staffthurs[to][]" value="<?= $thursday['to'][$x] ?>"
                                                 class="timepicker1 form-control"></div>

                   <?php if($x==0){ ?><a onclick="add_more(event,this)" href="#">Add more</a><?php  } else { ?>
                    <a onclick="remove_more(event,this)" href="#">Remove</a> <?php } ?>
                </div>
            <?php }
            for ($x = 0; $x != $friday_size; $x++) { ?>
                <div class="col-sm-12">
                    <div class="col-sm-2">Fri</div>
                    <div class="col-sm-2"><input type="text" value="<?= $friday['from'][$x] ?>" name="stafffri[from][]"
                                                 class="timepicker1 form-control"></div>
                    <div class="col-sm-1"> to</div>
                    <div class="col-sm-2"><input type="text" name="stafffri[to][]" value="<?= $friday['to'][$x] ?>"
                                                 class="timepicker1 form-control"></div>
                    <?php if($x==0){ ?><a onclick="add_more(event,this)" href="#">Add more</a><?php  } else { ?>
                        <a onclick="remove_more(event,this)" href="#">Remove</a> <?php } ?>
                </div>
            <?php }
            for ($x = 0; $x != $saturday_size; $x++) {
                ?>
                <div class="col-sm-12">
                    <div class="col-sm-2">Sat</div>
                    <div class="col-sm-2"><input type="text" value="<?= $saturday['from'][$x] ?>"
                                                 name="staffsat[from][]" class="timepicker1 form-control"></div>
                    <div class="col-sm-1"> to</div>
                    <div class="col-sm-2"><input type="text" name="staffsat[to][]" value="<?= $saturday['to'][$x] ?>"
                                                 class="timepicker1 form-control"></div>
                    <?php if($x==0){ ?><a onclick="add_more(event,this)" href="#">Add more</a><?php  } else { ?>
                        <a onclick="remove_more(event,this)" href="#">Remove</a> <?php } ?>
                </div>
            <?php } ?>
            <div class="col-sm-12">
                <div class="col-sm-6"><input type="submit" class="btn btn-success btn btn-warning ad1 " value="Update">
                </div>
            </div>

        </form>
        <div class="clearfix"></div>
    <? }
    $scriptq ="
    $('.timepicker1').timepicker({
        showPeriod: true
      
    });
    $('.staffrecurngtime12').on('submit', function( event ) {

event.preventDefault();
var formdate = $(this).serialize();
var fromid = $(this).attr('id');
$.ajax({
url:$(this).attr('action'),
type: 'post',
data: formdate,
success: function (response) {
alert(response);
if(response==1){
$('#'+fromid).remove();
}else {
// alert('not updated');

}
}
});
});
";
    $this->registerJs($scriptq);
} else {?>

    <form class="staffrecurngtime12" id="staff_recurform12">
        <div class="col-sm-6">
            <input type="hidden" value="<?= $service_id ?>" name="service_id">
            <input type="hidden" value="<?= $staff_id ?>" name="staff_id">

        </div>
        <div class="col-sm-12">
            <div class="col-sm-2">Sun</div><div class="col-sm-2"><input type="text" name="staffsun[from][]" class="timepicker2 form-control"> </div><div class="col-sm-1"> to </div><div class="col-sm-2"><input type="text" name="staffsun[to][]" class="timepicker2 form-control"></div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-2">Sun</div><div class="col-sm-2"><input type="text" name="staffsun[from][]" class="timepicker2 form-control"> </div><div class="col-sm-1"> to </div><div class="col-sm-2"><input type="text" name="staffsun[to][]" class="timepicker2 form-control"></div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-2">Mon</div><div class="col-sm-2"><input type="text" name="staffmon[from][]" class="timepicker2 form-control"> </div><div class="col-sm-1"> to </div><div class="col-sm-2"><input type="text" name="staffmon[to][]" class="timepicker2 form-control"></div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-2">Tue</div><div class="col-sm-2"><input type="text" name="stafftue[from][]" class="timepicker2 form-control"> </div><div class="col-sm-1"> to </div><div class="col-sm-2"><input type="text" name="stafftue[to][]" class="timepicker2 form-control"></div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-2">Wed</div><div class="col-sm-2"><input type="text" name="staffwed[from][]" class="timepicker2 form-control"> </div><div class="col-sm-1"> to </div><div class="col-sm-2"><input type="text" name="staffwed[to][]" class="timepicker2 form-control"></div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-2">Thurs</div><div class="col-sm-2"><input type="text" name="staffthurs[from][]" class="timepicker2 form-control"> </div><div class="col-sm-1"> to </div><div class="col-sm-2"><input type="text" name="staffthurs[to][]" class="timepicker2 form-control"></div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-2">Fri</div><div class="col-sm-2"><input type="text" name="stafffri[from][]" class="timepicker2 form-control"> </div><div class="col-sm-1"> to </div><div class="col-sm-2"><input type="text" name="stafffri[to][]" class="timepicker2 form-control"></div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-2">Sat</div><div class="col-sm-2"><input type="text" name="staffsat[from][]" class="timepicker2 form-control"> </div><div class="col-sm-1"> to </div><div class="col-sm-2"><input type="text" name="staffsat[to][]" class="timepicker2 form-control"></div>
        </div>
        <div class="col-sm-12">
            <div class="col-sm-6"><input type="submit" class="btn btn-success btn btn-warning ad1 " value="Update"></div>
        </div>

    </form>
    <div class="clearfix"></div>
    <?php
  $script_second ="$('.timepicker2').timepicker({
        showPeriod: true
      
    });
  $('.staffrecurngtime12').on('submit', function( event ) {

    event.preventDefault();
    var formdate = $(this).serialize();
    var fromid = $(this).attr('id');
    $.ajax({
    url:'schedule',
    type: 'post',
    data: formdate,
    success: function (response) {
    if(response==1){
   $('#'+fromid).remove();

    }else {
    // alert('not updated');

    }
    }
    });
    });
    ";
    $this->registerJs($script_second);
}


