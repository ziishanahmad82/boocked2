<?php

/* @var $this yii\web\View */
$this->title = 'Dashboard';
use app\models\CustomerReservedTimeslots;
use app\models\Customers;
use app\models\Locations;
use app\models\ThirdPartyUsers;
use app\models\Toughbooks;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use app\models\Countries;
use app\models\Cities;
use app\models\States;

$countries = Countries::find()->all();
?>
<style>
    .container {
        max-width: 100%;

    }

</style>
<? Pjax::begin(['id'=>'calendar_reload']); ?>
    <div class="site-index">


        <div class="body-content">
            <div id="errorrr"></div>
            <div class="col-sm-12">
                <form action="appointmentsave" id="appointmentsaving" >
                <?//= date('m-d-Y', strtotime($today_date));  ?>
                <input type="hidden" id="resource_csrf" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
               <div class="row">
                <fieldset class="col-xs-6 form-group">
                    <label>Services </label>
                <select class="form-control" name="Appointments[service_id]">
                  <?php  foreach($service_list as $services){ ?>
                      <option value="<?= $services->service_id ?>" data-time="<?= $services->service_duration ?>"><?= $services->service_name ?></option>
                    <?php } ?>
                </select>
                    </fieldset>
                    <fieldset class="col-xs-6 form-group">
                        <label>Staff</label>
                        <select class="form-control" name="Appointments[user_id]">
                        <?php
                        foreach($active_staff as $active_staffs){
                       print_r($active_staffs);    ?>
                            <option value="<?= $active_staffs->id; ?>"><?= $active_staffs->username; ?></option>
                            <?php
                        }

                        ?>
                        </select>
                    </fieldset>
                    </div>
                <div class="row">
                <fieldset class="form-group col-xs-6">
                    <input type="text" class="form-control" name="Appointments[appointment_start_time]" readonly="readonly" value="<?= $time ?>" />
                </fieldset>
                <fieldset class="form-group col-xs-6">
                    <div style="display:none">
                        <input type="text" class="form-control" name="Appointments[appointment_date]" readonly="readonly" value="<?= $date ?>" />
                    </div>
                    <input type="text" class="form-control"  readonly="readonly" value="<?= date('m-d-Y',strtotime($date)) ?>" />
                </fieldset>
                 </div>

                    <div class="row">
                    <fieldset class="form-group col-xs-12">
                        <select class="form-control" name="customer_type" onchange="customer_typeform(this)">
                            <option value="new_cust">New Customer</option>
                            <option value="exi_cust">Existing Customer</option>
                            <option value="3" >Unknown Customer</option>
                        </select>
                     </fieldset>
                    </div>

                <div id="customer_select_cont">
                    <div class="row">
                    <fieldset class="form-group col-xs-6">
                        <input type="text" name="Customers[first_name]" class="form-control col-sm-6" placeholder="First Name" >
                    </fieldset>
                    <fieldset class="form-group col-xs-6">
                        <input type="text" name="Customers[last_name]" class="form-control col-sm-6" placeholder="Last Name" >
                    </fieldset>
                    </div>
                    <div class="row">
                    <fieldset class="form-group col-xs-12">
                        <input type="email" name="Customers[email]" required class="form-control col-sm-12" placeholder="Email" >
                    </fieldset>
                    </div>
                    <div class="row">
                    <fieldset class="form-group col-xs-6">
                        <input type="text" name="Customers[cell_phone]" class="form-control col-sm-4" placeholder="Mobile" >
                    </fieldset>
                    <fieldset class="form-group col-xs-6">
                        <input type="text" name="Customers[work_phone]" class="form-control col-sm-4" placeholder="Work Phone" >
                    </fieldset>
                    </div>
                    <div class="row">
                    <fieldset class="form-group col-xs-6">
                        <input type="text" name="Customers[home_phone]" class="form-control col-sm-4" placeholder="Home Phone" >
                    </fieldset>
                    <fieldset class="form-group col-xs-6">
                        <input type="text" name="Customers[customer_address]" class="form-control col-sm-12" placeholder="Address" >
                    </fieldset>
                    </div>

                   <div class="row">
                    <fieldset class="form-group col-xs-12">
                        <select name="Customers[customer_country]" class="form-control col-sm-6" id="businessinformation-country">
                            <?php foreach ($countries as $country){ ?>
                                <option  value="<?= $country->id ?>">
                                    <?= $country->name ?>
                                </option>
                            <?php } ?>
                        </select>
                        <!--<input type="text" name="Customers[customer_country]" class="form-control col-sm-6" placeholder="Country" >-->
                    </fieldset>
                    </div>

                    <div class="row">
                        <fieldset class="form-group col-xs-6">
                            <select  name="Customers[customer_region]" class="form-control col-sm-6" id="businessinformation-state" >
                                <option>Region</option>
                            </select>

                        </fieldset>
                        <fieldset class="form-group col-xs-6">
                            <select  name="Customers[customer_city]" class="form-control col-sm-6" id="businessinformation-city" >
                                <option>City</option>
                            </select>
                        </fieldset>
                    </div>

                </div>

                <input type="submit" value="Book" class="btn btn-main pull-right" onclick="saving_appointment(event)" />
                </form>
            </div>


        </div>
    </div>
<?php Pjax::end(); ?>
<style>
    fieldset{
        border:none;
        margin-bottom:0 !important;

    }

</style>

<script>
    function customer_typeform(xor){
       var cstmr = $(xor).val();
        $('#customer_select_cont').load('<?= Url::to(['customer-reserved-timeslots/loadcustomertype']);?>?cust='+cstmr);
        

    }
    function saving_appointment(xor){
        xor.preventDefault();
        var form = $('#appointmentsaving');
        var inpObj = document.getElementById("appointmentsaving");
        if(inpObj.checkValidity()== false){
          //  alert(inpObj.validationMessage);
         //   document.getElementById("errorrr").innerHTML = "Please Enter valid email Address";
            alert('Please Enter valid email Address');
            return false;

        }else{

            var aptcust = jQuery('input[name="Appointments[customer_id]"]').val();
            var search_cust =   $('#search_custinput').val();
            if((aptcust!=undefined) || (search_cust!=undefined)  ){
                var aptcustval = jQuery('input[name="Appointments[customer_id]"]:checked').val();
                if(aptcustval==undefined){
                        alert('Please select atleast one customer');
                    return false;

                }

            }



        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: form.serialize(),
            success: function (response) {
                if(response==1){
                   $('button.close').click();
                $.pjax.reload({container:'#calendar_reload'});

                }else {
                    alert('not updated');

                }
            }
        });
        }

    }
</script>