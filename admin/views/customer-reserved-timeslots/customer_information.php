<?php

/* @var $this yii\web\View */
$this->title = 'Dashboard';
use app\models\CustomerReservedTimeslots;
use app\models\Customers;
use app\models\Users;
use app\models\ThirdPartyUsers;
use app\models\Toughbooks;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;

?>
<style xmlns="http://www.w3.org/1999/html">
    .container {
        max-width: 100%;

    }
    .option_nothours{
        color:red;
        font-weight:400;
    }
    .optionhour{
        font-weight:400;
    }

</style>
    <div class="site-index">



        <div class="body-content">
            <div id="errorrr"></div>
            <div id="model_appointment_detail_content" class="col-sm-12">
                <?php if($items['appointment_status']!= 'cancel'){ ?>    <div style="position: relative" id="cancelapnt_cont">
                    <div class="customer_informationboxer_cont">
                 <a href="#" onclick="event.preventDefault();" class="dropdown-toggle btn btn-warning" data-target="#status" data-toggle="collapse"  data-parent="#model_appointment_detail_content" style="margin-bottom:10px;float:right"><i class="fa fa-trash" ></i>Cancel Appointment</a>

                                             <div class="collapse dropdown-menu drop" id="status" >
                                                 <div>
                                                     <span style="    border-color: red red red transparent;border-style: solid;
    border-width: 0px 8px 8px;
    font-size: 0;
    height: 0;
    left: 50%;
    margin-left: -6px;
    margin-top: -3px;
    padding: 0;
    position: absolute;
    top: -4px;
    width: 0;"></span>
                                                     <input id="isSendMailDltGroup" type="checkbox"  checked="true">
                                                     Send email to client.
                                                 </div>
                                                   <button class="btn btn-warning"  style="margin-top:20px" onclick="cancel_appointment(<?= $items['appointment_id'] ?>)" >Cancel</button>
                                                 </div>
                     </div>
                    <div class="customer_informationboxer_cont">
                    <a href="#" onclick="event.preventDefault();" class="dropdown-toggle btn btn-warning" data-parent="#model_appointment_detail_content" data-target="#rescedule" data-toggle="collapse"  style="margin-bottom:10px;float:right"><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Reschedule</a>
                    <div class="collapse dropdown-menu drop" id="rescedule" >
                        <form id="reschedule_form">
                        <div><input type="hidden" name="srID" value="<?= $items['services']['service_id'] ?>" />
                            <p><span>Services: <strong style="color:#FE7500;" id="re_srid" data-srid="<?= $items['services']['service_id'] ?>"><?= $items['services']['service_name'] ?></strong></span></p>
                            <p><span>Who: <strong style="color:#FE7500; width:120px;display:inline-block ">
                                        <select class="form-control" id="re_staffs_list" name="stID">
                                            <?php  foreach($staffList as $staffs){ ?>
                                            <option value="<?= $staffs->id ?>"><?= $staffs->username ?></option>
                                            <?php } ?>
                                        </select>
                                    </strong></span>
                            </p>
                            <p><span>When:<strong style="color:#FE7500;display:inline-block">
                                        <input  name="reschedule_date" type="text" class="form-control" id="reschedule_datepicker" style="max-width:100px;display:inline-block" readonly='true'>
                                        <span style="display:inline-block" id="reschedule_mod_time_cont">
                                            <select name="reschedule_time"  type="text" class="form-control" id="reschedule_time" style="max-width:80px;display:inline-block">
                                            <option>-NA-</option>
                                        </select>
                                        </span>
                                    </strong>
                                </span>
                            </p>

                            <input id="isSendMailReschedule" name="sendemail" value="1" type="checkbox" checked="true">
                            Send email to client.
                        </div>
                            <input type="hidden" value="<?= $items['appointment_id'] ?>" name="apntID" >
                        </form>
                        <button class="btn btn-warning"  style="margin-top:20px" id="appointment_reschedule_fin" > Reschedule</button>

                    </div>
                    </div>
                    <div class="customer_informationboxer_cont">
                    <a href="#" onclick="event.preventDefault();" data-parent="#model_appointment_detail_content" class="dropdown-toggle btn btn-warning" data-target="#review_box" data-toggle="collapse"  style="margin-bottom:10px;float:right"><i class="fa fa-bullhorn" aria-hidden="true"></i>Request a Review</a>
                    <div class="collapse dropdown-menu drop" id="review_box" >
                        <div>
                            <p>An email would be sent to client to leave a review.</p>
                        </div>
                        <button class="btn btn-warning" id="send_review_email_cust"  style="margin-top:20px" data-cust="<?= $items['customer_id'] ?>" data-apnt="<?= $items['appointment_id'] ?>" >Send</button>
                    </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
                <?php } ?>
                <div class="clearfix"></div>

                <fieldset class="appointment_detail_fielset">
                    <p><span>Services: <strong style="color:#FE7500;"><?= $items['services']['service_name'] ?></strong></span></p>
                    <p><span>Staff:<strong></strong><?= Users::getusername($items['user_id']); ?></span></p>
                    <p><span>Time:<strong style="color:#FE7500;"><?= $items['appointment_start_time'] ?> @ <?= date('M,d,Y',strtotime($items['appointment_date'])); ?></strong></span></p>

                </fieldset>
                <h4>Customer Info</h4>


                <table class="table table-responsive">
                    <tbody>

                        <tr>
                            <td>
                                <ul class="list-inline list-action">

                                    <li style="position:relative">



                                            <img src="<?= Yii::getAlias('@web') ?>/img/user.png" style="width:58px;height: 57px">



                                    </li>
                                    <li>
                                        <ul class="list-unstyled list-name">
                                            <li><? if($items['customer_id']=="0"){ echo 'Unknown User'; } else { echo $items['customers']['first_name'];} ?></li>
                                           <!-- <li><small><span class="text-muted">Time Slot : </span><span class="txt-theme">30 Min</span></small></li> -->
                                        </ul>
                                    </li>
                                    <div class="pull-right div-desc">
                                        <ul class="list-inline" id="staus_contul">
                                            <?php if($items['appointment_status']== 'cancel'){ ?>
                                            <li class="alert alert-danger" style="padding:7px;" id="cancelpopupnti">
                                                <strong>Cancelled</strong>
                                          </li>
                                            <?php }else {?>
                                            <li style="position: relative" id="status_slectboxcont">
                                            <select id="appointment_status" class="form-control" onchange="appointmentstatus_changed('#appointment_status','<?= $items['appointment_id'] ?>')">
                                                <option value="As Schedule" <?php if($items['appointment_status']== 'As Schedule'){ echo "selected"; } ?> >As Schedule</option>
                                                <option value="Arrived Late" <?php if($items['appointment_status']== 'Arrived Late'){ echo "selected"; } ?>>Arrived Late</option>
                                                <option value="No Show" <?php if($items['appointment_status']== 'No Show'){ echo "selected"; } ?>>No Show</option>
                                                <option value="Gift Certificates" <?php if($items['appointment_status']== 'Gift Certificates'){ echo "selected"; } ?>>Gift Certificates</option>
                                            </select>
                                             <!--- <a href="#" onclick="event.preventDefault();" class="dropdown-toggle" data-target="#status" data-toggle="collapse" ><?//= $items['appointment_status'] ?></a>
                                             <ul class="collapse dropdown-menu drop" id="status" style="position:absolute;left:0 !important;">
                                                     <li><a href="#">Arrived Late </a></li>
                                                     <li><a href="#">No Show</a></li>
                                                  <li><a href="#">Gift Certificates</a></li>
                                                 </ul> -->
                                            </li>
                                            <?php } ?>
                                           <!-- <li class="dropdown"><a href="#" onclick="event.preventDefault();" class="dropdown-toggle" data-target="#staffdemo" data-toggle="collapse" ><i class="fa fa-chevron-circle-down"></i></a>

                                            </li>-->
                                        </ul>
                                    </div>
                                </ul>
                            </td>

                        </tr>



                    </tbody>
                </table>









            </div>

            <div class="clearfix"></div>

            <div  id="staffdemo" class="collapse body-content">
                <td>
                    <div class="col-sm-12">
                        <div class="col-sm-12">
                        <label>Appointment Note </label>
                        <textarea class="form-control" id="apnt_note" name="appointment_note"><?= $items['appointment_note'] ?></textarea>
                        <br/>
                        <input type="button" class="btn btn-main pull-right" value="Update Note" onclick="updatenote('<?= $items['appointment_id'] ?>')">
                          <div style="clear:both;line-height:0;padding:0;margin-bottom:10px; "></div>

                        <div class="alert alert-success updatenotemsg" style="display:none">
                            <strong>Updated!</strong>.
                        </div>
                            </div>

                    </div>
                    <?php Pjax::begin(['id' => 'payment_reload']); ?>
                    <div class="col-sm-12" id="payment_process">
                       <?php if($items['payment_status']=='unpaid'){} ?>
                        <div class="col-sm-12"> <div class="alert alert-warning">Process Payment <?= $items['payment_status'] ?></div></div>

                        <?php if($items['payment_status']=='paid'){
                            foreach($appointment_payment as $payment) { ?>
                                <div class="col-sm-12">
                                <p><span>To Pay: <strong><?= $payment['to_pay'] ?></strong> <?= Yii::$app->session->get('bk_currency') ?> by <strong><?= $payment['payment_method'] ?></strong></span><a href="#" style="font-size:12px" class="btn btn-danger pull-right">Delete</a> </p>
                                <p><span>Additional Charges: <strong><?= $payment['additional_charges'] ?></strong></span></p>
                                <p><span>Discount: <strong><?= $payment['discount'] ?></strong></span></p>
                                <p><span>Total: <strong><?= $payment['total'] ?></strong> <?= Yii::$app->session->get('bk_currency') ?></span></p>
                                </div>


                            <?php }
                        }else { ?>
                            <form action="appointmentspayments" id="paymentprocess">
                                <input type="hidden" id="resource_csrf" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                                <input type="hidden" name="AppointmentsPayments[appointment_id]" value="<?= $items['appointment_id'] ?>" />
                                <p class="col-xs-6"><label>To Pay</label></label><input type="text" name="AppointmentsPayments[to_pay]"  class="form-control payment_pro_vals" id="to_pay" value="<?= $items['services']['service_price'] ?>" ></p>
                                <p class="col-xs-6"><label>By</label>
                                    <select name="AppointmentsPayments[payment_method]" class="form-control">
                                        <option value="Cash">Cash</option>
                                        <option value="Credit Card">Credit Card</option>
                                        <option value="Cheque">Cheque</option>
                                        <option value="Echeck">Echeck</option>
                                        <option value="Debit Card">Debit Card</option>
                                        <option value="Credit Card and Cash">Credit Card and Cash</option>
                                        <option value="Gift Certificates">Gift Certificates</option>
                                        <option value="Gift Certificates and Cash">Gift Certificates and Cash</option>

                                    </select></p>
                                <p class="col-xs-6"><label>Additional Charges</label><input type="text" name="AppointmentsPayments[additional_charges]" class="form-control payment_pro_vals" id="additional_payment" value="0"></p>
                                <p class="col-xs-6"><label>Discount</label> <input type="text" name="AppointmentsPayments[discount]" class="form-control payment_pro_vals" value="0" id="discount_payment"></p>
                                <p class="col-xs-12"><textarea name="AppointmentsPayments[payment_note]" class="form-control"></textarea></p>
                                <p class="col-xs-6"><label>Total</label> <input type="text" name="AppointmentsPayments[total]" id="total_payment" class="form-control"  value="<?= $items['services']['service_price'] ?>"  ></p>
                                <div class="clearfix"></div>
                                <p><input type="button" class="btn btn-main pull-right" onclick="paymentsubmit()" id="add_paymentbut" value="Add Payment" </p>

                            </form>
                        <?php } ?>
                    </div>
                    <?php Pjax::end(); ?>


                </td>
                <div class="clearfix"></div>
            </div>
        </div>


       
<?php   $script = "
$('.payment_pro_vals').change(function(){
var pay = Number($('#to_pay').val());
var discount = Number($('#discount_payment').val());
var addtional = Number($('#additional_payment').val());
var total = pay+addtional-discount;
$('#total_payment').val(total);
});
 //  $.pjax.reload({container:'#payment_reload'});
 
 var myGroup = $('#model_appointment_detail_content');
myGroup.on('show.bs.collapse','.collapse', function() {
    myGroup.find('.collapse.in').collapse('hide');
});
  $('#reschedule_datepicker').datepicker({
    minDate: 0,
    setDate: new Date(),
  });
  
 
  
";
$this->registerJs($script);
?>
        <style>
           /* #datepicker{z-index:12000 !important;} */
            .ui-datepicker { position: relative; z-index: 10000 !important; }
        </style>
<script>
    function customer_typeform(xor){
       var cstmr = $(xor).val();
        $('#customer_select_cont').load('<?= Url::to(['customer-reserved-timeslots/loadcustomertype']);?>?cust='+cstmr);
        

    }
    function updatenote(apnt_id){
       var apnt_note = $('#apnt_note').val();

        $.ajax({
            url: 'updateappointnote',
            type: 'post',
            data:{'id':apnt_id,'value':apnt_note},
            success: function (response) {
                if(response==1){
$('.updatenotemsg').show();
                    hidesucmessage('.updatenotemsg');

                }else {
                  

                }
            }
        });

    }
    function saving_appointment(xor){
        xor.preventDefault();
        var form = $('#appointmentsaving');
        var inpObj = document.getElementById("appointmentsaving");
        if(inpObj.checkValidity()== false){
          //  alert(inpObj.validationMessage);
            document.getElementById("errorrr").innerHTML = "Please Enter valid email Address";
            exit();

        }else{





        $.ajax({
            url: form.attr('action'),
            type: 'post',
            data: form.serialize(),
            success: function (response) {
                if(response==1){
                   $('button.close').click();

                }else {
                    alert('not updated');

                }
            }
        });
        }

    }
    function hidesucmessage(xor){
        setTimeout(function () {
            $(xor).hide();
        }, 3000);

    }
    function paymentsubmit(){


        $.ajax({
            url: $('#paymentprocess').attr('action'),
            type: 'post',
            data: $('#paymentprocess').serialize(),
            success: function (response) {
                if(response==1){
                    $('#payment_process').html('<p class="alert alert-success"> <strong>Paid</strong> </p>');

                }else {
                    alert('not updated');

                }
            }
        });

    }
    function appointmentstatus_changed(xor,apnt_id){

        //alert(xor);
        var status = $(xor+' option:selected').val();

        $.ajax({
            url:'appointmentstatusupdate/?status='+status+'&apnt_id='+apnt_id,

            success: function (response) {
                if(response==1){
                    //  $('button.close').click();

                }else {
                    alert('not updated');

                }
            }
        });

    }
    function cancel_appointment(apnt_id){
        $.ajax({
            url:'<?= Url::toRoute(['/customer-reserved-timeslots/appointmentstatuscancel']) ?>/?apnt_id='+apnt_id,

            success: function (response) {
                if(response==1){
                  $('#add_paymentbut').remove();
                   $('#cancelapnt_cont').hide();
                  $('#status_slectboxcont').remove();
                  $('#staus_contul').prepend('<li class="alert alert-danger" style="padding:7px;" id="cancelpopupnti"> <strong>Cancelled</strong> </li>');
                    $.pjax.reload({container:'#calendar_reload'});

                }else {
                   // alert('not updated');

                }
            }
        });

    }
</script>
        <style>
            .payment_process p {
                margin-top: 30px;
            }
        </style>
