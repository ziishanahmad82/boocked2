<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\web\JsExpression;
use yii\bootstrap\Modal;

use app\models\Locations;
use app\models\Customers;
use app\models\TimeSlotsConfiguration;
use app\models\CustomerReservedTimeslots;
use app\models\Users;



/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerReservedTimeslotsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Calendar';
$this->params['breadcrumbs'][] = $this->title;



//$this->registerJs($DragJS);


$ajax_path=Yii::getAlias('@web');


?>


















































<?php

$JSDayRender = <<<EOF
function (date, cell) {

    var check = date._d.toJSON().slice(0,10);
    var today = new Date().toJSON().slice(0,10);



    $.ajax({
    url: "$ajax_path/index.php/customer-reserved-timeslots/cellcolor/?date="+check,
    dataType: 'json',
    success: function(result){

        //if(check >= today){
            cell.css("background-color", '#228B22');
            cell.html(result['day_status']);
            console.log(result['day_status']);

        //}
//        if(check < today){
//            cell.css("background-color", "lightgray");
//            cell.html("<span style='display:none;'>past</span>");
//        }
        if(check == today){
            cell.css("background-color", "#228B22");

        }





        $.each(result.all_holidays, function(i, item) {



             if(check == result.all_holidays[i].holiday_date.trim()){
                cell.css("background-color", "#cc0000");
                cell.html("<br/><br/><h4>Unavailable</h4><span style='display:none;'>holiday</span>");

             }
        })



    }});




}
EOF;


$JSEventClick = <<<EOF
function(date,jsEvent,view) {

  var check = date._d.toJSON().slice(0,10);
    var today = new Date().toJSON().slice(0,10);
 
if(view.name=='month'){
  
  //
    //alert(check);
   // $('.fc-view-container').load('$ajax_path/index.php/customer-reserved-timeslots/cellcolor/?date='+check);
//GET http://localhost/boocked/admin/web/index.php/customer-reserved-timeslots/cellcolor/?date=2016-06-13
	   view.calendar.gotoDate(date);
    view.calendar.changeView('agendaDay');



}else {
var slottime = date._d.toJSON().slice(11,19);
 jQuery("#loadingstatus").show();
        jQuery("#preloader").show();
$('#modal-form2').find('#modalContent').load('$ajax_path/index.php/customer-reserved-timeslots/form2/?time='+slottime+'&date='+check,function(){
  jQuery("#loadingstatus").hide();
        jQuery("#preloader").hide();
$('#modal-form2').modal('show');
});
}
}

EOF;

$JSslotClick = <<<EOF
function(calEvent, jsEvent, view) {
//alert(calEvent.id)
//.modal('show')
 jQuery("#loadingstatus").show();
        jQuery("#preloader").show();
 $('#appointment_detail2').find('#modalContent').load('$ajax_path/index.php/customer-reserved-timeslots/appointmentinformation/?id='+calEvent.id,function(){
  jQuery("#loadingstatus").hide();
        jQuery("#preloader").hide();
$('#appointment_detail2').modal('show');
});
 

}
EOF;


?>
<?php

?>




    <div class="customer-reserved-timeslots-form inner-form" style="width: 80%">


        <?php $form = ActiveForm::begin(); ?>











    </div>


 <?php   /*   $Event = new \yii2fullcalendar\models\Event();
    $Event->id = 1;
    $Event->title = 'Available';
    $Event->start = date('Y-m-d\Th:m:s\Z');
    $events[] = $Event; */
?>


<?php //\T \Z
//print_r(date('Y-m-d\Th:m:s\Z', strtotime('tomorrow 6am')));
//print_r($reserved_timeslot);

//.' T0 '.reserved_timeslots->test_start_time.' Z');
//$selectedTime = "23:45:00";
//$endTime = strtotime("+30 minutes", strtotime($selectedTime));
//echo date('h:i:s', $endTime);
$events = array();
//Testing

foreach($appointments as $appointment) {

    $test_date = $appointment['appointment_date'];
    $test_time = $appointment['appointment_start_time'];
    $endTime = strtotime("+30 minutes", strtotime($test_time));
    $endTime= date('h:i:s', $endTime);






    $Event = new \yii2fullcalendar\models\Event();
    $Event->id = $appointment['appointment_id'];
    $Event->title = Users::getusername($appointment['user_id']);


  $Event->start = $test_date.'T'.$test_time.'Z';
    $Event->end = $test_date.'T'.$endTime.'Z';
    if($appointment['appointment_status']=='cancel'){
        $Event->backgroundColor = 'red';
        $Event->borderColor = 'red';

    }
    $events[] = $Event;

}

?>
    
<?= yii2fullcalendar\yii2fullcalendar::widget(array(

    'header' => [
        'left' => 'prev,next today',
        'center' => 'title',
       'right' => 'agendaDay,agendaWeek,month'
    ],

    'clientOptions' => [
        'selectable' => true,
        'selectHelper' => true,
        'droppable' => true,
        'editable' => true,
        //'drop' => new JsExpression($JSDropEvent),
        //'select' => new JsExpression($JSCode),
        'dayRender' => new JsExpression($JSDayRender),
        'dayClick' => new JsExpression($JSEventClick),
        'eventClick'=> new JsExpression($JSslotClick),
        // 'viewRender' => new JsExpression($JSviewRender),
        'defaultDate' => date('Y-m-d'),
        'weekends' => false,
        'slotDuration' => '00:30:00',
       
        //'eventRender' => new JsExpression($JSEventRender),
    ],
    'events'=> $events,
    //'ajaxEvents' => Url::toRoute(['/customer-reserved-timeslots/jsoncalendar'])
));

$newappointmenthead = '<ul class="list-inline list-modal-header" style="margin-bottom:0">
    <li class="text-uppercase"><i style="margin-right:6px;color: #FC7600;" class="fa fa-user"></i>New Appointment</li>
   <!-- <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
        <a href=""><i class="fa fa-reply"></i></a>
    </li>
    <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
        <a href="#" onclick="detailaddnewcustomer()"><i class="fa fa-plus"></i></a>
    </li>
    <li class="pull-right"><a href="#ban1" data-toggle="tab" id="ban-btn-toggle"><i class="fa fa-ban"></i></a></li>
    <li class="pull-right"><a href="#pay1" data-toggle="tab" id="ban-btn-toggle"><i class="fa fa-dollar"></i></a></li>
    <li class="pull-right" style="font-size: 12px;">Gift Certificates / Discount Coupons</li>-->
</ul>';


Modal::begin([
    'header' =>$newappointmenthead,
    'id' => 'modal-form2',
    //'size' => 'modal-lg',
    //'htmlOptions' => ['style' => 'width:800px;']
]);
echo "<div id='modalContent' ></div>";
Modal::end();
?>
<?php $customer_Detailhead = '<ul class="list-inline list-modal-header" style="margin-bottom:0">
    <li class="text-uppercase"><i style="margin-right:6px;color: #FC7600;" class="fa fa-user"></i>Appointment Details</li>
   
    <li class="pull-right"><a href="#ban1" data-toggle="tab" id="model_appointment_detail_link"><i class="fa fa-clock-o"></i></a></li>
    <li class="pull-right"><a href="#staffdemo" id="model_calendar_payment" ><i class="fa fa-dollar"></i></a></li>
    <!--<li class="pull-right" style="font-size: 12px;">Gift Certificates / Discount Coupons</li>-->
</ul>'; ?>
<?php
Modal::begin([
    'header' => $customer_Detailhead,
    'id' => 'appointment_detail2',
  //  'size' => 'modal-lg',
    //'htmlOptions' => ['style' => 'width:800px;']
]);
echo "<div id='modalContent'></div>";
Modal::end();



























/*$selectedTime = "23:45:00";
$endTime = strtotime("+30 minutes", strtotime($selectedTime));

$Event = new \yii2fullcalendar\models\Event();
$Event->id = 1;
$Event->title = 'Available';
$Event->start = date('Y-m-d\Th:m:s\Z');
//$Event->end = date('Y-m-d\Th:m:s\Z',strtotime($selectedTime));
$events[] = $Event;

*/

?>


