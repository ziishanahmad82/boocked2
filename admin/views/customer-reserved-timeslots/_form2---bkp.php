<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\CustomerReservedTimeslots;
use app\models\CustomerReservedTimeslotsSearch;
use yii\grid\GridView;
use yii\helpers\Url;

$request = Yii::$app->request;
$get = $request->get();
$post = $request->post();
$session = Yii::$app->session;

$searchModel = new CustomerReservedTimeslotsSearch(['confirmed'=>'1', 'third_user_id'=>1, 'test_date'=>$get['check']]);
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);


?>

<h3>Selected Date: <?=date("d M Y",strtotime($get['check']));?></h3>
<h4>Third Party Appointments</h4>
<div class="customer-reserved-timeslots-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <!--    <p>-->
    <!--        --><?//= Html::a('Create Customer Reserved Timeslots', ['create'], ['class' => 'btn btn-success']) ?>
    <!--    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            //'timeslot_id',
            //'customer_id',
            //'location_id',
            //'test_date',
            'test_start_time',
            //'test_end_time',
           // 'examiner_id',
            'test_type',
            // 'confirmed',
            'status',

            //['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
<h4>Normal Appointments</h4>
<?
$searchModel = new CustomerReservedTimeslotsSearch(['confirmed'=>'1', 'third_user_id'=>0, 'test_date'=>$get['check']]);
$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
?>
<div class="customer-reserved-timeslots-index">


    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>-->
<!--        --><?//= Html::a('Create Customer Reserved Timeslots', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->

<?= GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'columns' => [
        //['class' => 'yii\grid\SerialColumn'],

        //'timeslot_id',
       // 'customer_id',
       // 'location_id',
        //'test_date',
        'test_start_time',
        //'test_end_time',
        //'examiner_id',
        'test_type',
        // 'confirmed',
        'status',

        //['class' => 'yii\grid\ActionColumn'],
    ],
]); ?>

</div>
