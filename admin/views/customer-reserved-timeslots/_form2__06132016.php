<?php
/* @var $this yii\web\View */
$this->title = 'Dashboard';
use app\models\CustomerReservedTimeslots;
use app\models\Customers;
use app\models\Locations;
use app\models\ThirdPartyUsers;
use app\models\Toughbooks;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;

$get=Yii::$app->request->get();
@$today_date = $get["check"];

if(Yii::$app->user->identity->user_group=='admin' OR Yii::$app->user->identity->user_group=='csr') {



    $this->registerJs(
        '
$(document).ready( function(){
        $("#div-select-toughbook1").hide();
    });
    $("#status1").change(function(){
        if($("#status1").val()=="assign-toughbook"){
            $("#div-select-toughbook1").show();
        }
    });


    $(document).ready( function(){
        $("#div-select-toughbook2").hide();
    });
    $("#status2").change(function(){
        if($("#status2").val()=="assign-toughbook"){
            $("#div-select-toughbook2").show();
        }
    });
'
    );
    ?>

    <div class="site-index">


        <div class="body-content">

<!--            <div style="float: right;">-->
<!---->
<!--            <span class="cdl1 cdl_btn3"><a href="--><?//=Yii::getAlias('@web') . '../../../'?><!--customer/web/index.php?customer_action=new"-->
<!--                    >New Appointment</a></span>-->
<!--            <span class="cdl1 cdl_btn3"><a href="--><?//=Yii::getAlias('@web') . '../../../'?><!--customer/web/index.php?customer_action=edit"-->
<!--                    >Change/Cancel Appointment</a></span>-->
<!---->
<!---->
<!--            </div>-->

            <div id="sitem5">
                <h1>Dashboard <?= date('m-d-Y', strtotime($today_date));  ?></h1>
            </div>


            <?php ob_start(); ?>



            <?
            //$today_date = date("Y-m-d");
            $total_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->one()->cnt;
            $completed_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'and',
                    ['!=', 'status', 'checked in'],
                    ['!=', 'status', 'Waiting for check in'],
                    ['!=', 'status', 'Assigned to toughbook'],
                ])
                ->one()->cnt;


            $untested_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'or',
                    ['like', 'status', 'checked in'],
                    ['like', 'status', 'Waiting for check in'],
                    ['like', 'status', 'Assigned to toughbook'],
                ])
                ->one()->cnt;

            ?>

            <div class="sitem8">
                <div class="sitem9">
                    <b>Total :</b> <?= $total_res_count ?>&nbsp;&nbsp;
                    <b>Completed :</b> <?= $completed_res_count ?>&nbsp;&nbsp;
                    <b>Untested :</b> <?= $untested_res_count ?>
                </div>
            </div>


            <?php Pjax::begin(['id' => 'p1']); ?>

            <?= GridView::widget([
                'id' => 'p1',
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'dl_no',
                        'label' => 'Permit Number',
                        'value' => 'customer.learners_permit_number',
                    ],
                    [
                        'attribute' => 'full_name',
                        'label' => 'Name',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return "<span style=\"text-transform: capitalize\">{$data->customer->first_name} {$data->customer->last_name}</span>";

                        }
                    ],
                    //'location_id',
                    //'test_date',
                    [
                        'attribute' => 'test_start_time',
                        'label' => 'Test Time',
                        'value' => 'test_start_time',
                        'format' => ['time', 'php:h:i A'],
                    ],
                    [
                        'attribute' => 'third_user',
                        'label' => 'Third Party Examiner',
                        'format' => 'raw',
                        'value' => function ($data) {
                            $str = '<div class="sitem7">';
                            if ($data->third_user_id > 0) {
                                $str .= $data->thirdparty->displayname;
                            } else
                                $str .= 'DMV Examiner';
                            $str .= '</div>';
                            return $str;
                        },
                    ],
                    //'test_start_time',
                    // 'test_end_time',
                    // 'examiner_id',

                    [
                        'attribute' => 'test_type',
                        'label' => 'Test Type',
                        'format' => 'raw',
                        'value' => function ($data) {
                            if ($data->test_type == 'NCDL')
                                return 'Standard';
                            else
                                return 'CDL';
                        },
                    ],
                    // 'confirmed',
                    // 'third_user_id',
                    //'tough_book_id',
                    'status',
//                    [
//
//                        'label' => 'Actions',
//                        'format' => 'raw',
//                        //'filter' => '<a class="link_reset_filter">Reset All Filters</a>',
//                        'filter' => Html::a('Reset All Filters', ['index'], ['class' => 'link_reset_filter']),
//                        'value' => function ($data) {
//                            $str = '<div class="sitem6">';
//                            if ($data->status == 'Waiting for check in') {
//
//                                $str .= CustomerReservedTimeslots::customer_status_dropdown($data->status, 'status1');
//                                $str .= '
//
//                                        <input type="hidden" name="assign_toughbook" value="no">
//                                        <input type="submit" class="btn-success" value="Go"/>
//                            ';
//
//                            } else if ($data->status == 'Checked in') {
//                                $str .= '<select name="toughbook-select1" id="toughbook-select1">
//                                            <option value="0">Select a toughbook</option>';
//
//                                $toughbooks = Toughbooks::find()
//                                    ->select(['*'])
//                                    ->where([
//                                        'toughbook_status' => 'active',
//                                    ])
//                                    ->all();
//                                foreach ($toughbooks as $toughbook) {
//
//                                    $str .= '<option value="' . $toughbook->toughbook_id . '">' . $toughbook->toughbook_code . '</option>';
//
//                                }
//                                $str .= '
//                            </select>
//
//                                <input type="hidden" name="assign_toughbook" value="yes">
//                                <input type="submit" class="btn-success" value="Go"/>';
//
//
//                            } else {
//                                $str .= '';
//                                return '';
//                            }
//                            $str .= '</div>';
//                            return $str;
//
//                        },
//                    ],

                    //['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <?php Pjax::end(); ?>





            <?php $var1 = ob_get_clean(); ?>





            <?php ob_start(); ?>




            <?
            //$today_date = date("Y-m-d");
            $total_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'and',
                    ['<', 'third_user_id', 1],
                    ['=', 'test_type', 'NCDL'],
                ])
                ->one()->cnt;

            $completed_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'and',
                    ['!=', 'status', 'checked in'],
                    ['!=', 'status', 'Waiting for check in'],
                    ['!=', 'status', 'Assigned to toughbook'],
                    ['<', 'third_user_id', 1],
                    ['=', 'test_type', 'NCDL'],
                ])
                ->one()->cnt;


            $untested_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'or',
                    ['like', 'status', 'checked in'],
                    ['like', 'status', 'Waiting for check in'],
                    ['like', 'status', 'Assigned to toughbook'],
                ])
                ->andFilterWhere([
                    'and',
                    ['<', 'third_user_id', 1],
                    ['=', 'test_type', 'NCDL'],
                ])
                ->one()->cnt;

            ?>

            <div class="sitem8">
                <div class="sitem9">
                    <b>Total :</b> <?= $total_res_count ?>&nbsp;&nbsp;
                    <b>Completed :</b> <?= $completed_res_count ?>&nbsp;&nbsp;
                    <b>Untested :</b> <?= $untested_res_count ?>
                </div>
            </div>


            <?php Pjax::begin(['id' => 'p2']); ?>

            <?= GridView::widget([
                'id' => 'p2',
                'dataProvider' => $dataProvider2,
                'filterModel' => $searchModel2,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'dl_no',
                        'label' => 'Permit Number',
                        'value' => 'customer.learners_permit_number',
                    ],
                    [
                        'attribute' => 'full_name',
                        'label' => 'Name',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return "<span style=\"text-transform: capitalize\">{$data->customer->first_name} {$data->customer->last_name}</span>";

                        }
                    ],
                    //'location_id',
                    //'test_date',
                    [
                        'attribute' => 'test_start_time',
                        'label' => 'Test Time',
                        'value' => 'test_start_time',
                        'format' => ['time', 'php:h:i A'],
                    ],
                    //'test_start_time',
                    // 'test_end_time',
                    // 'examiner_id',

                    [
                        'attribute' => 'test_type',
                        'label' => 'Test Type',
                        'format' => 'raw',
                        'value' => function ($data) {
                            if ($data->test_type == 'NCDL')
                                return 'Standard';
                            else
                                return 'CDL';
                        },
                    ],
                    // 'confirmed',
                    // 'third_user_id',
                    //'tough_book_id',
                    'status',
//                    [
//
//                        'label' => 'Actions',
//                        'format' => 'raw',
//                        'filter' => Html::a('Reset All Filters', ['index'], ['class' => 'link_reset_filter']),
//                        'value' => function ($data) {
//                            $str = '<div class="sitem6">';
//                            if ($data->status == 'Waiting for check in') {
//
//                                $str .= CustomerReservedTimeslots::customer_status_dropdown($data->status, 'status1');
//                                $str .= '
//
//                                        <input type="hidden" name="assign_toughbook" value="no">
//                                        <input type="submit" class="btn-success" value="Go"/>
//                            ';
//
//                            } else if ($data->status == 'Checked in') {
//                                $str .= '<select name="toughbook-select1" id="toughbook-select1">
//                                            <option value="0">Select a toughbook</option>';
//
//                                $toughbooks = Toughbooks::find()
//                                    ->select(['*'])
//                                    ->where([
//                                        'toughbook_status' => 'active',
//                                    ])
//                                    ->all();
//                                foreach ($toughbooks as $toughbook) {
//
//                                    $str .= '<option value="' . $toughbook->toughbook_id . '">' . $toughbook->toughbook_code . '</option>';
//
//                                }
//                                $str .= '
//                            </select>
//
//                                <input type="hidden" name="assign_toughbook" value="yes">
//                                <input type="submit" class="btn-success" value="Go"/>';
//
//
//                            } else {
//                                $str .= '';
//                                return '';
//                            }
//                            $str .= '</div>';
//                            return $str;
//
//                        },
//                    ],

                    //['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <?php Pjax::end(); ?>


            <?php $var2 = ob_get_clean(); ?>




            <?php ob_start(); ?>





            <?
            //$today_date = date("Y-m-d");
            $total_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'and',
                    ['>', 'third_user_id', 0],
                    ['=', 'test_type', 'NCDL'],
                ])
                ->one()->cnt;

            $completed_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'and',
                    ['!=', 'status', 'checked in'],
                    ['!=', 'status', 'Waiting for check in'],
                    ['!=', 'status', 'Assigned to toughbook'],
                    ['>', 'third_user_id', 0],
                    ['=', 'test_type', 'NCDL'],
                ])
                ->one()->cnt;


            $untested_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'or',
                    ['like', 'status', 'checked in'],
                    ['like', 'status', 'Waiting for check in'],
                    ['like', 'status', 'Assigned to toughbook'],
                ])
                ->andFilterWhere([
                    'and',
                    ['>', 'third_user_id', 0],
                    ['=', 'test_type', 'NCDL'],
                ])
                ->one()->cnt;

            ?>

            <div class="sitem8">
                <div class="sitem9">
                    <b>Total :</b> <?= $total_res_count ?>&nbsp;&nbsp;
                    <b>Completed :</b> <?= $completed_res_count ?>&nbsp;&nbsp;
                    <b>Untested :</b> <?= $untested_res_count ?>
                </div>
            </div>


            <?php Pjax::begin(['id' => 'p3']); ?>

            <?= GridView::widget([
                'id' => 'p3',
                'dataProvider' => $dataProvider3,
                'filterModel' => $searchModel3,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'dl_no',
                        'label' => 'Permit Number',
                        'value' => 'customer.learners_permit_number',
                    ],
                    [
                        'attribute' => 'full_name',
                        'label' => 'Name',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return "<span style=\"text-transform: capitalize\">{$data->customer->first_name} {$data->customer->last_name}</span>";

                        }
                    ],
                    //'location_id',
                    //'test_date',
                    [
                        'attribute' => 'test_start_time',
                        'label' => 'Test Time',
                        'value' => 'test_start_time',
                        'format' => ['time', 'php:h:i A'],
                    ],
                    [
                        'attribute' => 'third_user',
                        'label' => 'Third Party Examiner',
                        'format' => 'raw',
                        'value' => function ($data) {
                            $str = '<div class="sitem7">';
                            if ($data->third_user_id > 0) {
                                $str .= $data->thirdparty->displayname;
                            } else
                                $str .= 'DMV Examiner';
                            $str .= '</div>';
                            return $str;
                        },
                    ],
                    //'test_start_time',
                    // 'test_end_time',
                    // 'examiner_id',

                    [
                        'attribute' => 'test_type',
                        'label' => 'Test Type',
                        'format' => 'raw',
                        'value' => function ($data) {
                            if ($data->test_type == 'NCDL')
                                return 'Standard';
                            else
                                return 'CDL';
                        },
                    ],
                    // 'confirmed',
                    // 'third_user_id',
                    //'tough_book_id',
                    'status',
//                    [
//
//                        'label' => 'Actions',
//                        'format' => 'raw',
//                        'filter' => Html::a('Reset All Filters', ['index'], ['class' => 'link_reset_filter']),
//                        'value' => function ($data) {
//                            $str = '<div class="sitem6">';
//                            if ($data->status == 'Waiting for check in') {
//
//                                $str .= CustomerReservedTimeslots::customer_status_dropdown($data->status, 'status1');
//                                $str .= '
//
//                                        <input type="hidden" name="assign_toughbook" value="no">
//                                        <input type="submit" class="btn-success" value="Go"/>
//                            ';
//
//                            } else if ($data->status == 'Checked in') {
//                                $str .= '<select name="toughbook-select1" id="toughbook-select1">
//                                            <option value="0">Select a toughbook</option>';
//
//                                $toughbooks = Toughbooks::find()
//                                    ->select(['*'])
//                                    ->where([
//                                        'toughbook_status' => 'active',
//                                    ])
//                                    ->all();
//                                foreach ($toughbooks as $toughbook) {
//
//                                    $str .= '<option value="' . $toughbook->toughbook_id . '">' . $toughbook->toughbook_code . '</option>';
//
//                                }
//                                $str .= '
//                            </select>
//
//                                <input type="hidden" name="assign_toughbook" value="yes">
//                                <input type="submit" class="btn-success" value="Go"/>';
//
//
//                            } else {
//                                $str .= '';
//                                return '';
//                            }
//                            $str .= '</div>';
//                            return $str;
//
//                        },
//                    ],

                    //['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <?php Pjax::end(); ?>





            <?php $var3 = ob_get_clean(); ?>





            <?php ob_start(); ?>




            <?
            //$today_date = date("Y-m-d");
            $total_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'and',
                    ['<', 'third_user_id', 1],
                    ['=', 'test_type', 'CDL'],
                ])
                ->one()->cnt;

            $completed_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'and',
                    ['!=', 'status', 'checked in'],
                    ['!=', 'status', 'Waiting for check in'],
                    ['!=', 'status', 'Assigned to toughbook'],
                    ['<', 'third_user_id', 1],
                    ['=', 'test_type', 'CDL'],
                ])
                ->one()->cnt;


            $untested_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    //'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'or',
                    ['like', 'status', 'checked in'],
                    ['like', 'status', 'Waiting for check in'],
                    ['like', 'status', 'Assigned to toughbook'],
                ])
                ->andFilterWhere([
                    'and',
                    ['<', 'third_user_id', 1],
                    ['=', 'test_type', 'CDL'],
                ])
                ->one()->cnt;

            ?>

            <div class="sitem8">
                <div class="sitem9">
                    <b>Total :</b> <?= $total_res_count ?>&nbsp;&nbsp;
                    <b>Completed :</b> <?= $completed_res_count ?>&nbsp;&nbsp;
                    <b>Untested :</b> <?= $untested_res_count ?>
                </div>
            </div>


            <?php Pjax::begin(['id' => 'p4']); ?>

            <?= GridView::widget([
                'id' => 'p4',
                'dataProvider' => $dataProvider4,
                'filterModel' => $searchModel4,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'dl_no',
                        'label' => 'Permit Number',
                        'value' => 'customer.learners_permit_number',
                    ],
                    [
                        'attribute' => 'full_name',
                        'label' => 'Name',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return "<span style=\"text-transform: capitalize\">{$data->customer->first_name} {$data->customer->last_name}</span>";

                        }
                    ],
                    //'location_id',
                    //'test_date',
                    [
                        'attribute' => 'test_start_time',
                        'label' => 'Test Time',
                        'value' => 'test_start_time',
                        'format' => ['time', 'php:h:i A'],
                    ],
//                [
//                    'attribute' => 'third_user',
//                    'label' => 'Third Party User',
//                    'format' => 'raw',
//                    'value' => function($data){
//                        $str='<div class="sitem7">';
//                        if($data->third_user_id>0)
//                        {
//                            $str.= $data->thirdparty->displayname;
//                        }
//                        else
//                            $str.='';
//                        $str.='</div>';
//                        return $str;
//                    },
//                ],
                    //'test_start_time',
                    // 'test_end_time',
                    // 'examiner_id',

                    [
                        'attribute' => 'test_type',
                        'label' => 'Test Type',
                        'format' => 'raw',
                        'value' => function ($data) {
                            if ($data->test_type == 'NCDL')
                                return 'Standard';
                            else
                                return 'CDL';
                        },
                    ],
                    // 'confirmed',
                    // 'third_user_id',
                    //'tough_book_id',
                    'status',
//                    [
//
//                        'label' => 'Actions',
//                        'format' => 'raw',
//                        'filter' => Html::a('Reset All Filters', ['index'], ['class' => 'link_reset_filter']),
//                        'value' => function ($data) {
//                            $str = '<div class="sitem6">';
//                            if ($data->status == 'Waiting for check in') {
//
//                                $str .= CustomerReservedTimeslots::customer_status_dropdown($data->status, 'status1');
//                                $str .= '
//
//                                        <input type="hidden" name="assign_toughbook" value="no">
//                                        <input type="submit" class="btn-success" value="Go"/>
//                            ';
//
//                            } else if ($data->status == 'Checked in') {
//                                $str .= '<select name="toughbook-select1" id="toughbook-select1">
//                                            <option value="0">Select a toughbook</option>';
//
//                                $toughbooks = Toughbooks::find()
//                                    ->select(['*'])
//                                    ->where([
//                                        'toughbook_status' => 'active',
//                                    ])
//                                    ->all();
//                                foreach ($toughbooks as $toughbook) {
//
//                                    $str .= '<option value="' . $toughbook->toughbook_id . '">' . $toughbook->toughbook_code . '</option>';
//
//                                }
//                                $str .= '
//                            </select>
//
//                                <input type="hidden" name="assign_toughbook" value="yes">
//                                <input type="submit" class="btn-success" value="Go"/>';
//
//
//                            } else {
//                                $str .= '';
//                                return '';
//                            }
//                            $str .= '</div>';
//                            return $str;
//
//                        },
//                    ],

                    //['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <?php Pjax::end(); ?>



            <?php $var4 = ob_get_clean(); ?>



            <?
            $normal_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    'third_user_id' => '0'
                ])
                ->andFilterWhere([
                    'or',
                    ['like', 'status', 'checked in'],
                    ['like', 'status', 'Waiting for check in'],
                    ['like', 'status', 'Assigned to toughbook'],
                ])
                ->one()->cnt;

            $third_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                ])->andWhere(['>', 'third_user_id', '0'])
                ->andFilterWhere([
                    'or',
                    ['like', 'status', 'checked in'],
                    ['like', 'status', 'Waiting for check in'],
                    ['like', 'status', 'Assigned to toughbook'],
                ])
                ->one()->cnt;

            $completed_normal_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    'third_user_id' => '0',
                ])->andWhere(['!=', 'status', 'checked in'])
                ->andWhere(['!=', 'status', 'Waiting for check in'])
                ->andWhere(['!=', 'status', 'Assigned to toughbook'])
                ->one()->cnt;

            $completed_3rd_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                ])->andWhere(['!=', 'status', 'checked in'])
                ->andWhere(['!=', 'status', 'Waiting for check in'])
                ->andWhere(['!=', 'status', 'Assigned to toughbook'])
                ->andWhere(['>', 'third_user_id', '0'])
                ->one()->cnt;


            ?>


            <div class="dashboard-tabs">
                <?

                //            echo Tabs::widget([
                //                'items' => [
                //                    [
                //                        //'label' => "Total Appointments ($normal_res_count)",
                //                        'label' => "All DMV ($count1)",
                //                        'content' => $var1,
                //                        'active' => true
                //                    ],
                //                    [
                //                        'label' => "Standard ($count2)",
                //                        'content' => $var2,
                //                    ],
                //                    [
                //                        'label' => "Third Party ($count3)",
                //                        'content' => $var3,
                //                    ],
                //                    [
                //                        'label' => "CDL ($count4)",
                //                        'content' => $var4,
                //                    ],
                //
                //
                //                ],
                //            ]);


                echo Tabs::widget([
                    'items' => [
                        [
                            //'label' => "Total Appointments ($normal_res_count)",
                            'label' => "All Appointments",
                            'content' => $var1,
                            'active' => true
                        ],
                        [
                            'label' => "Standard",
                            'content' => $var2,
                        ],
                        [
                            'label' => "Third Party",
                            'content' => $var3,
                        ],
                        [
                            'label' => "CDL",
                            'content' => $var4,
                        ],


                    ],
                ]);
                ?>
            </div>


        </div>
    </div>


    <?
    $this->registerJs("
    $('.link_reset_filter').click(function(){
        $('#p1-filters :input').val('');
        //p1-filters
    })
");


} else {



    //$today_date = date("Y-m-d");

    $this->registerJs(
        '
$(document).ready( function(){
        $("#div-select-toughbook1").hide();
    });
    $("#status1").change(function(){
        if($("#status1").val()=="assign-toughbook"){
            $("#div-select-toughbook1").show();
        }
    });


    $(document).ready( function(){
        $("#div-select-toughbook2").hide();
    });
    $("#status2").change(function(){
        if($("#status2").val()=="assign-toughbook"){
            $("#div-select-toughbook2").show();
        }
    });
'
    );
    ?>

    <div class="site-index">


        <div class="body-content">

            <div style="float: right;">

            <span class="cdl1 cdl_btn3"><a href="<?=Yii::getAlias('@web') . '../../../'?>customer/web/index.php?customer_action=new"
                    >New Appointment</a></span>
            <span class="cdl1 cdl_btn3"><a href="<?=Yii::getAlias('@web') . '../../../'?>customer/web/index.php?customer_action=edit"
                    >Change/Cancel Appointment</a></span>


            </div>

            <div id="sitem5">
                <h1>Dashboard <?= $today_date = date("m-d-Y"); ?></h1>
            </div>


            <?php ob_start(); ?>



            <?
            //$today_date = date("Y-m-d");
            $total_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    'third_user_id' => Yii::$app->user->identity->id
                ])
                ->one()->cnt;
            $completed_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    'third_user_id' => Yii::$app->user->identity->id
                ])
                ->andFilterWhere([
                    'and',
                    ['!=', 'status', 'checked in'],
                    ['!=', 'status', 'Waiting for check in'],
                    ['!=', 'status', 'Assigned to toughbook'],
                ])
                ->one()->cnt;


            $untested_res_count = CustomerReservedTimeslots::find()
                ->select(['COUNT(timeslot_id) AS cnt'])
                ->where([
                    'test_date' => $today_date,
                    'confirmed' => '1',
                    'third_user_id' => Yii::$app->user->identity->id
                ])
                ->andFilterWhere([
                    'or',
                    ['like', 'status', 'checked in'],
                    ['like', 'status', 'Waiting for check in'],
                    ['like', 'status', 'Assigned to toughbook'],
                ])
                ->one()->cnt;

            ?>

            <div class="sitem8">
                <div class="sitem9">
                    <b>Total :</b> <?= $total_res_count ?>&nbsp;&nbsp;
                    <b>Completed :</b> <?= $completed_res_count ?>&nbsp;&nbsp;
                    <b>Untested :</b> <?= $untested_res_count ?>
                </div>
            </div>


            <?php Pjax::begin(['id' => 'p1']); ?>

            <?= GridView::widget([
                'id' => 'p1',
                'dataProvider' => $dataProvider5,
                'filterModel' => $searchModel5,
                'columns' => [
                    //['class' => 'yii\grid\SerialColumn'],

                    [
                        'attribute' => 'dl_no',
                        'label' => 'Permit Number',
                        'value' => 'customer.learners_permit_number',
                    ],
                    [
                        'attribute' => 'full_name',
                        'label' => 'Name',
                        'format' => 'raw',
                        'value' => function ($data) {
                            return "<span style=\"text-transform: capitalize\">{$data->customer->first_name} {$data->customer->last_name}</span>";

                        }
                    ],
                    //'location_id',
                    //'test_date',
                    [
                        'attribute' => 'test_start_time',
                        'label' => 'Test Time',
                        'value' => 'test_start_time',
                        'format' => ['time', 'php:h:i A'],
                    ],
//                    [
//                        'attribute' => 'third_user',
//                        'label' => 'Third Party Examiner',
//                        'format' => 'raw',
//                        'value' => function ($data) {
//                            $str = '<div class="sitem7">';
//                            if ($data->third_user_id > 0) {
//                                $str .= $data->thirdparty->displayname;
//                            } else
//                                $str .= 'DMV Examiner';
//                            $str .= '</div>';
//                            return $str;
//                        },
//                    ],
                    //'test_start_time',
                    // 'test_end_time',
                    // 'examiner_id',

                    [
                        'attribute' => 'test_type',
                        'label' => 'Test Type',
                        'format' => 'raw',
                        'value' => function ($data) {
                            if ($data->test_type == 'NCDL')
                                return 'Standard';
                            else
                                return 'CDL';
                        },
                    ],
                    // 'confirmed',
                    // 'third_user_id',
                    //'tough_book_id',
                    'status',
//                    [
//
//                        'label' => 'Actions',
//                        'format' => 'raw',
//                        //'filter' => '<a class="link_reset_filter">Reset All Filters</a>',
//                        'filter' => Html::a('Reset All Filters', ['index'], ['class' => 'link_reset_filter']),
//                        'value' => function ($data) {
//                            $str = '<div class="sitem6">';
//
//                            $str.=Html::a('Change/Cancel Appointment', ['index'], ['class' => 'sitem12']);
//
//                            $str .= '</div>';
//                            return $str;
//
//                        },
//                    ],

                    //['class' => 'yii\grid\ActionColumn'],
                ],
            ]); ?>
            <?php Pjax::end(); ?>





            <?php $var1 = ob_get_clean(); ?>















            <div class="dashboard-tabs">
                <?

                echo Tabs::widget([
                    'items' => [
                        [
                            //'label' => "Total Appointments ($normal_res_count)",
                            'label' => "All Appointments",
                            'content' => $var1,
                            'active' => true
                        ],



                    ],
                ]);
                ?>
            </div>


        </div>
    </div>


    <?
    $this->registerJs("
    $('.link_reset_filter').click(function(){
        $('#p1-filters :input').val('');
        //p1-filters
    })
");


}
?>