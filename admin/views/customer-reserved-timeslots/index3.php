<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\web\JsExpression;
use yii\bootstrap\Modal;

use app\models\Locations;
use app\models\Customers;
use app\models\TimeSlotsConfiguration;
use app\models\CustomerReservedTimeslots;
use app\models\Examiners;


/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerReservedTimeslotsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Calendar';
$this->params['breadcrumbs'][] = $this->title;



//$this->registerJs($DragJS);



$ajax_path=Yii::getAlias('@web');


$JSDayRender = <<<EOF
function (date, cell) {

    var check = date._d.toJSON().slice(0,10);
    var today = new Date().toJSON().slice(0,10);



    $.ajax({
    url: "$ajax_path/index.php/customer-reserved-timeslots/cellcolor/?date="+check,
    dataType: 'json',
    success: function(result){

        //if(check >= today){
            cell.css("background-color", result['bgcolor']);
            cell.html(result['day_status']);
            console.log(result['day_status']);

        //}
//        if(check < today){
//            cell.css("background-color", "lightgray");
//            cell.html("<span style='display:none;'>past</span>");
//        }
        if(check == today){
            cell.css("background-color", "lightblue");

        }





        $.each(result.all_holidays, function(i, item) {



             if(check == result.all_holidays[i].holiday_date.trim()){
                cell.css("background-color", "#80FF00");
                cell.html(result.all_holidays[i].holiday_name+"<span style='display:none;'>holiday</span>");

             }
        })



    }});




}
EOF;


$JSEventClick = <<<EOF
function(date, allDay, jsEvent, view) {


    var check = date._d.toJSON().slice(0,10);
    var today = new Date().toJSON().slice(0,10);

    $('#modal-form2').modal('show').find('#modalContent').load('$ajax_path/index.php/customer-reserved-timeslots/form2/?date='+date+'&check='+check);


}

EOF;









?>




<div class="customer-reserved-timeslots-form inner-form" style="width: 80%">


    <?php $form = ActiveForm::begin(); ?>











</div>





<?php

$events = array();
//Testing
$Event = new \yii2fullcalendar\models\Event();
$Event->id = 1;
$Event->title = 'Available';
$Event->start = date('Y-m-d\Th:m:s\Z');
$events[] = $Event;

$Event = new \yii2fullcalendar\models\Event();
$Event->id = 2;
$Event->title = 'Available';
$Event->start = date('Y-m-d\Th:m:s\Z', strtotime('tomorrow 6am'));
$events[] = $Event;

?>

<?= yii2fullcalendar\yii2fullcalendar::widget(array(
    'header' => [
        'left' => 'prev,next today',
        'center' => 'title',
        'right' => ''
    ],

    'clientOptions' => [
        'selectable' => true,
        'selectHelper' => true,
        'droppable' => true,
        'editable' => true,
        //'drop' => new JsExpression($JSDropEvent),
        //'select' => new JsExpression($JSCode),
        'dayRender' => new JsExpression($JSDayRender),
        'dayClick' => new JsExpression($JSEventClick),
       // 'viewRender' => new JsExpression($JSviewRender),
        'defaultDate' => date('Y-m-d'),
        'weekends' => false,
        'slotDuration' => '00:30:00',
        //'eventRender' => new JsExpression($JSEventRender),
    ],
    //'ajaxEvents' => Url::toRoute(['/customer-reserved-timeslots/jsoncalendar'])
));


Modal::begin([
    'header' => '<h1>Appointments for selected date</h1>',
    'id' => 'modal-form2',
    //'size' => 'modal-lg',
    //'htmlOptions' => ['style' => 'width:800px;']
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>