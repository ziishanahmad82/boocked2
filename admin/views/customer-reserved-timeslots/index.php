<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomerReservedTimeslotsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customer Reserved Timeslots';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-reserved-timeslots-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Customer Reserved Timeslots', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            'timeslot_id',
            'customer_id',
            'location_id',
            'test_date',
            'test_start_time',
            // 'test_end_time',
            // 'examiner_id',
             'test_type',
            // 'confirmed',
            // 'third_user_id',
             'tough_book_id',
             'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
