<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerReservedTimeslots */

//$this->title = $model->timeslot_id;
$this->params['breadcrumbs'][] = ['label' => 'Customer Reserved Timeslots', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="gdview" style=" height: 200px;
    overflow-x: scroll;">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Firstname</th>
            <th>Email</th>
            <th></th>
        </tr>
        </thead>
        <tbody>

      <?php
    if(empty($results)){
echo ' <tr>
              <td>No user found</td><td></td></td><td></td></td></tr>';

    }
      else{
      foreach($results as $data) {
          ?>


          <tr>
              <td><?= $data->first_name ?></td>
              <td><?= $data->email ?></td>
              <td><input type="radio" name="Appointments[customer_id]" value="<?= $data->customer_id ?>"/></td>
          </tr>
          <?php
      }
      }?>

        </tbody>
    </table>
<? /*= GridView::widget([
    'dataProvider' => $dataProvider,
    'columns' => [
        [
            'class' => 'yii\grid\CheckboxColumn',
            'checkboxOptions' => function ($dataProvider, $key, $index, $column) {
                return ['value' => $dataProvider->customer_id];
            },
            'multiple'=> false,
        ],
        'first_name',
        'email',


    ],
]); */
?>
</div>
<?php
$script= "
$('ul.pagination > li > a').click(function(event){
event.preventDefault();
var loadingpage = $(this).attr('href');
$('#gdview').load(loadingpage);
});

";
$this->registerJs($script);
