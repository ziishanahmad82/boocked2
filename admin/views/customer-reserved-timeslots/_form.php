<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerReservedTimeslots */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-reserved-timeslots-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'customer_id')->textInput() ?>

    <?= $form->field($model, 'location_id')->textInput() ?>

    <?= $form->field($model, 'test_date')->textInput() ?>

    <?= $form->field($model, 'test_start_time')->textInput() ?>

    <?= $form->field($model, 'test_end_time')->textInput() ?>

    <?= $form->field($model, 'examiner_id')->textInput() ?>

    <?= $form->field($model, 'test_type')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'confirmed')->textInput() ?>

    <?= $form->field($model, 'third_user_id')->textInput() ?>

    <?= $form->field($model, 'tough_book_id')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
