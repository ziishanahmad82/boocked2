<?php

/* @var $this yii\web\View */
use app\models\CustomerReservedTimeslots;
use app\models\Customers;
use app\models\Locations;
use app\models\ThirdPartyUsers;
use app\models\Toughbooks;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;


?>

<?php if($customer=='new_cust'){ ?>
  <?php
    $countries = \app\models\Countries::find()->all();
    ?>
<div class="row">
    <fieldset class="form-group col-xs-6">
        <input type="text" name="Customers[first_name]" class="form-control col-sm-6" placeholder="First Name" >
    </fieldset>
    <fieldset class="form-group col-xs-6">
        <input type="text" name="Customers[last_name]" class="form-control col-sm-6" placeholder="Last Name" >
    </fieldset>
</div>
    <div class="row">
        <fieldset class="form-group col-xs-12">
            <input type="email" name="Customers[email]" required class="form-control col-sm-12" placeholder="Email" >
        </fieldset>
    </div>
    <div class="row">
    <fieldset class="form-group col-xs-6">
        <input type="text" name="Customers[cell_phone]" class="form-control col-sm-4" placeholder="Mobile" >
    </fieldset>

    <fieldset class="form-group col-xs-6">
        <input type="text" name="Customers[work_phone]" class="form-control col-sm-4" placeholder="Work Phone" >
    </fieldset>
    </div>
    <div class="row">
    <fieldset class="form-group col-xs-6">
        <input type="text" name="Customers[home_phone]" class="form-control col-sm-4" placeholder="Home Phone" >
    </fieldset>
    <fieldset class="form-group col-xs-6">
        <input type="text" name="Customers[customer_address]" class="form-control col-sm-12" placeholder="Address" >
    </fieldset>
     </div>
    <div class="row">
        <fieldset class="form-group col-xs-12">
            <select name="Customers[customer_country]" class="form-control col-sm-6" id="businessinformation-country">
                <?php foreach ($countries as $country){ ?>
                    <option  value="<?= $country->id ?>">
                        <?= $country->name ?>
                    </option>
                <?php } ?>
            </select>
            <!--<input type="text" name="Customers[customer_country]" class="form-control col-sm-6" placeholder="Country" >-->
        </fieldset>
    </div>

    <div class="row">
        <fieldset class="form-group col-xs-6">
            <select  name="Customers[customer_region]" class="form-control col-sm-6" id="businessinformation-state" >
                <option>Region</option>
            </select>

        </fieldset>
        <fieldset class="form-group col-xs-6">
            <select  name="Customers[customer_city]" class="form-control col-sm-6" id="businessinformation-city" >
                <option>City</option>
            </select>
        </fieldset>
    </div>

<?php } else if($customer == 'exi_cust'){ ?>
    <fieldset class="form-group" style="padding-left:0; padding-right:0"><div class="col-sm-6"></div><input type="text" class="form-control" style="width: 92%; display: inline;" id="search_custinput"><i class="glyphicon glyphicon-search" id="search_customer" onclick="search_existing_customer(this)" style="display: inline;
    padding: 10px;"></i></div>
   </fieldset>
    <div id="search_results" >
        
        
    </div>


    <script>
       function search_existing_customer(xor){
           var searchinput =  $('#search_custinput').val();
          if(searchinput){
            //  searchcustomer
             $('#search_results').load('searchcustomer?search='+searchinput);

          }else{
              alert('Please enter First name,Email or Phone')

          }

        }

    </script>

<? }
