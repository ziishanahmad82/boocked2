<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;


$this->title = 'Notification';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>

</style>


    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 no-padding-l media-xs-full">
            <div class="panel panel-default">
                <div class="panel-heading full2">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web') ?>/img/settings.png"> <span class="staff-head">Manage Notification</span></h5></li>
                    </ul>
                </div>
                <div class="panel-body panel-body-nav-tabs-sidebar sid">
                    <p style="padding-top:20px">You can manage email and SMS notification from here.Email notification is Free of Charge for appropriate package.SMS notification require additional messaging charges. </p>
                </div>
            </div>
        </div>

        <div class="col-lg-9 col-md-9 col-sm-9 media-xs-full sed">
            <div class="panel panel-default">
                <div class="panel-heading full2">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><i class="fa fa-bell" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Notification</span></h5></li>
                        <li class="pull-right li-right">General name for service</li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="service">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="panel panel-default">
                                            <div class="panel-heading full2">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">Email Notification</span></h5></li>

                                                </ul>
                                            </div>
                                            <div class="panel-body">

                                                <div class="row" style="margin-top: 20px;">
                                                    <div class="col-lg-10 col-md-10 col-sm-10 col-lg-offset-1 col-xs-12">
                                                        <div class="row">
                                                            <label class="checkbox box1" style="top:-10px; ">
                                                                <input type="checkbox" name="customer_email_notification" <?php if(Yii::$app->session->get('bk_customer_email_notification')=='1'){ echo 'checked';} ?> value="1" class="top2 booking_rules_checkbox" style="top:6px"><small>Send an email to customer.</small> <span class="savedMess" style="display:none;">saved</span>

                                                                <input type="text" name="customer_email_notification_min" style=" display: inline;max-width: 40px;" class="form-control bkr_textinput" value="<?php if(Yii::$app->session->get('bk_customer_email_notification_min')){ echo Yii::$app->session->get('bk_customer_email_notification_min');} ?> " />
                                                                <small>hours prior to their appointment.(This will help to minimize no-shows).</small> <span class="savedMess" style="display:none;">saved</span>
                                                            </label>

                                                        </div>


                                                    </div>


                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="panel-body" style="border: none;">
                                        <div class="panel panel-default">
                                            <div class="panel-heading full2">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">SMS Notification</span></h5></li>

                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12 top">
                                                        <a href="" class="font" style="color: #FC7600; top: 10px;">Send an sms when a appointment is booked.(Requires purchase of SMS credits.)</a>
                                                        <p style="margin-top:15px"><strong>When to send</strong></p>
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top: 0px;">
                                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">


                                                        <label class="radio" style="left:20px">
                                                            <input type="radio" name="admin_sms_notification" <?php if(Yii::$app->session->get('bk_admin_sms_notification')=='never'){ echo 'checked';} ?> value="never" class="booking_rules_checkbox" > <small>Never send an SMS</small> <span class="savedMess" style="display:none;">saved</span>
                                                        </label>

                                                        <label class="radio" style="left:20px">
                                                            <input type="radio" name="admin_sms_notification" <?php if(Yii::$app->session->get('bk_admin_sms_notification')=='on_require_approval'){ echo 'checked';} ?> value="on_require_approval" class="booking_rules_checkbox"> <small>Whenever an appointment requires approval</small> <span class="savedMess" style="display:none;">saved</span>
                                                        </label>

                                                        <label class="radio" style="left:20px">
                                                            <input type="radio"  name="admin_sms_notification" <?php if(Yii::$app->session->get('bk_admin_sms_notification')=='appointment_book'){ echo 'checked';} ?> value="appointment_book" class="booking_rules_checkbox"><small>Each time an appointment is booked</small> <span class="savedMess" style="display:none;">saved</span>
                                                        </label>




                                                        <div class="row">
                                                            <div class="col-lg-12 top">
                                                                <p><strong>When to send an SMS to</strong></p>

                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                                                    <label class="radio" style="left:20px">
                                                                        <input type="checkbox" name="send_admin_sms" value="1" class="booking_rules_checkbox" <?php if(Yii::$app->session->get('bk_send_admin_sms')=='1'){ echo 'checked';} ?> > <small>Admin</small> <span class="savedMess" style="display:none;">saved</span>
                                                                    </label>
                                                                </div>
                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
                                                                    <label class="radio" style="left:20px">
                                                                        <input  type="checkbox" name="send_staff_sms" value="1" class="booking_rules_checkbox" <?php if(Yii::$app->session->get('bk_send_staff_sms')=='1'){ echo 'checked';} ?> > <small>Staff</small> <span class="savedMess" style="display:none;">saved</span>
                                                                    </label>
                                                                </div>

                                                            </div>

                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 start">
                                                                <span class="wid">Note one sms alert in</span><select name="group" class="form-control wid" style="width:110px;display: -moz-box;margin-left: 10px;margin-right: 10px;">
                                                                    <option value="group1">Pakistan</option>
                                                                    <option value="group2">Pakistan</option>
                                                                    <option value="group3">Pakistan</option>
                                                                </select><span class="wid">Cost 11.20 cents(approx).</span>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                                                                <p style="margin-top:15px"><strong>Send an SMS alert to your customer(Requires the purchase of SMS credits).</strong></p>
                                                                <label class="radio" style="left:20px">
                                                                    <input type="checkbox" name="customer_alert_sms" value="1" class="booking_rules_checkbox" <?php if(Yii::$app->session->get('bk_customer_alert_sms')=='1'){ echo 'checked';} ?>  > <small>Send an SMS alert to customer &nbsp &nbsp  &nbsp &nbsp  hours prior to their appointment.(This will help to minimize no-shows).</small> <span class="savedMess" style="display:none;">saved</span>
                                                                </label>

                                                                <label class="radio" style="left:20px">
                                                                    <input type="checkbox"  name="customer_coutesy_sms" value="1" class="booking_rules_checkbox" <?php if(Yii::$app->session->get('bk_customer_coutesy_sms')=='1'){ echo 'checked';} ?>  > <small>Send an courtesy"Thank You" text after appointment.</small> <span class="savedMess" style="display:none;">saved</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- last extra divs  class="tab-pane fade" id="preference"  -->

                        </div>
                    </div>
                </div>
                <!-- last extra divs <div class="modal fade" id="modal-customer-details">  -->


            </div>


































<input type="hidden" id="booking_csrf" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />





































<?php
$script = "
$('body').on('beforeSubmit', 'form#resourcecommonform', function () {
var form = $(this);

if (form.find('.has-error').length) {
return false;
}
$.ajax({
url: form.attr('action'),
type: 'post',
data: form.serialize(),
success: function (response) {
    if(response==1){
        alert('changed');
         $.pjax.reload({container:'#domainnamesection'});

    }else {
        alert('not updated');

    }
}
});
return false
});
$('body').on('beforeSubmit', 'form.paypalform', function () {
var form = $(this);
var savedmess = $(this).find('.savedMess');

if (form.find('.has-error').length) {
return false;
}
$.ajax({
url: form.attr('action'),
type: 'post',
data: form.serialize(),
success: function (response) {
    if(response==1){
       // alert('changed');
       savedmess.css('display','inline');
     setTimeout(function(){
 
    savedmess.hide('slow');
},2000);
    }else {
        alert('not updated');

    }
}
});
return false;
});
$('.booking_rules_checkbox').change(function(){
   var chkbox_name =  $(this).attr('name');
   var savedmess = $(this).siblings('.savedMess');
   
if($(this).is(':checked')){
  var chkbox_val = 1;
  }else {
    var chkbox_val = 0;
  }
   var csrf_name = $('booking_csrf').attr('name');
   var csrf_val = $('booking_csrf').val();
$.ajax({
url: 'updaterulessetting',
type: 'post',
data:{csrf_name:csrf_val,
'SettingBookingRules[bkrules_name]':chkbox_name,
'SettingBookingRules[bkrules_value]':chkbox_val},
success: function (response) {
    if(response==1){
     savedmess.css('display','inline');
     setTimeout(function(){
 
    savedmess.hide('slow');
}, 2000);

    }else {
        alert('not updated');

    }
}
});

});
$('.bkr_selectbox').change(function(){

   var optionval =  $('option:selected',this).val();
   var selectbox_name =  $(this).attr('name');
   alert(optionval);
   var csrf_name = $('booking_csrf').attr('name');
   var csrf_val = $('booking_csrf').val();
$.ajax({
url: 'updaterulessetting',
type: 'post',
data:{csrf_name:csrf_val,
'SettingBookingRules[bkrules_name]':selectbox_name,
'SettingBookingRules[bkrules_value]':optionval},
success: function (response) {
    if(response==1){
      // $('#business_informationform .alert').show(0).delay(2000).hide(0);
alert('updated');
    }else {
        alert('not updated');

    }
}
});

});

$('.bkr_textinput').change(function(){

var savedmess = $(this).siblings('.savedMess');
   var optionval =  $(this).val();
   var selectbox_name =  $(this).attr('name');
   var csrf_name = $('booking_csrf').attr('name');
   var csrf_val = $('booking_csrf').val();
$.ajax({
url: 'updaterulessetting',
type: 'post',
data:{csrf_name:csrf_val,
'SettingBookingRules[bkrules_name]':selectbox_name,
'SettingBookingRules[bkrules_value]':optionval},
success: function (response) {
    if(response==1){
    
 savedmess.css('display','inline');
     setTimeout(function(){
 
    savedmess.hide('slow');
}, 2000);
    }else {
        alert('not updated');

    }
}
});

});

";

$this->registerJs($script);
?>
<script>
function setTimezoneSetting(currentEle){
    var itemvalue = currentEle.value;
    var itemname = currentEle.name;
    var curformaction = $('#timezoneform').attr('action');
    $.ajax({
        url: curformaction,
        type: 'post',
        data:{'item_name':itemname, 'item_value':itemvalue},
        success: function (response) {
            if(response==1){
                alert('Updated');

            }else {
                alert('not updated');

            }
        }
    });



 //'var abc = ad.attr('id');
  //  alert(abc);
}
function trash_tracking_domain(event,xor){
    event.preventDefault();
    var link_href = $(xor).attr('href');
    $.ajax({
        url: 'trash_tracking',
        type: 'post',
        data:{'domain':link_href},
        success: function (response) {
            if(response==1){
     $.pjax.reload({container:'#domainnamesection'});

            }else {
                alert('not updated');

            }
        }
    });
}
</script>
