<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\BusinessInformation */

$this->title = 'Create Business Information';
$this->params['breadcrumbs'][] = ['label' => 'Business Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="business-information-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
