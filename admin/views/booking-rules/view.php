<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\BusinessInformation */

$this->title = $model->business_id;
$this->params['breadcrumbs'][] = ['label' => 'Business Informations', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="business-information-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->business_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->business_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'business_id',
            'business_name',
            'business_description:ntext',
            'address',
            'country',
            'state',
            'city',
            'zip',
            'logo',
            'business_phone',
        ],
    ]) ?>

</div>
