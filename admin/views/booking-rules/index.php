<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Url;


$this->title = 'Booking Rules';
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 no-padding-l media-xs-full">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web') ?>/img/settings.png"> <span class="staff-head">Manage Rules</span></h5></li>
                    </ul>
                </div>
                <div class="panel-body panel-body-nav-tabs-sidebar">
                    <div class="row">
                        <ul class="nav nav-stacked nav-tabs nav-tabs-sidebar nav-rules">
                            <li class="active"><a href="#tab1" data-toggle="tab">1 Allow customers to schedule</a></li>
                            <li><a href="#tab2" data-toggle="tab">2 Who can schedule</a></li>
                            <li><a href="#tab3" class="last" data-toggle="tab">3 Strike-out-options</a></li>
                            <li><a href="#tab4" class="last" data-toggle="tab">4 Layout settings</a></li>
                            <li><a href="#tab5" class="last" data-toggle="tab">5 Services Options</a></li>
                            <li><a href="#tab6" class="last" data-toggle="tab">6 Staff options</a></li>
                            <li><a href="#tab7" class="last" data-toggle="tab">7 Login options</a></li>
                            <li><a href="#tab8" class="last" data-toggle="tab">8 Appointment leads & cancel...</a></li>
                            <li><a href="#tab9" class="last" data-toggle="tab">9 Info required from customer</a></li>
                            <li><a href="#tab10" class="last" data-toggle="tab">10 Tracking Code</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-9 col-md-9 col-sm-9 media-xs-full">
            <div class="panel panel-default panel-rules2">
                <div class="panel-heading">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><i class="fa fa-file-code-o" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Rules</span></h5></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="tab1">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step1.png" class="img-responsive" style="margin-top: 0; margin-bottom: 28px; display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">Allow customers to schedule themselves?</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label class="checkbox" style="margin-left: 21px;">
                                                            <input type="checkbox" name="rules_allow_customer_booking" <?php if(Yii::$app->session->get('bk_rules_allow_customer_booking')=='1'){ echo 'checked';} ?>  value="1" class="booking_rules_checkbox"> Check this box if you want to allow customers to book appointments directly from the site.  <span class="savedMess" style="display:none;">saved</span>
                                                        </label>
                                                        <p style="margin-top: -7px; margin-left: 23px;">
                                                            <small class="text-muted">
                                                                If left unchecked, your customers will be able to see your availability and a message directing them to contact you by phone or in person to make an appointment. This option will also be applied to your mini-website and the widget integrated on your website.
                                                            </small>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row btn-row" style="margin-top: 8pc;">
                                            <div class="col-lg-12">
                                                <button  onclick="clickTab('#tab2')"  class="btn btn-main pull-right" style="padding: 11px 36px 13px;">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab2">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-collapse collapse in active">
                                        <div class="panel-body" style="border: none;">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <img src="<?= Yii::getAlias('@web') ?>/img/step2.png" class="img-responsive" style="margin-top: 0; margin-bottom: 28px; display: block; margin-left: auto; margin-right: auto;">
                                                </div>
                                            </div>
                                            <div class="panel panel-default panel-rules1">
                                                <div class="panel-heading">
                                                    <ul class="list-inline list-staff">
                                                        <li><h5 class="panel-title text-uppercase"><span class="staff-head">Who can schedule?</span></h5></li>
                                                    </ul>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                                            <ul class="list-inline">
                                                                <li>
                                                                    <label class="checkbox" style="margin-left: 21px;">
                                                                        <input type="checkbox" name="phone_verified_approvel" value="1" <?php if(Yii::$app->session->get('bk_phone_verified_approvel')=='1'){ echo 'checked';} ?> class="booking_rules_checkbox"> Phone verified members <span class="savedMess" style="display:none;">saved</span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <div class="form-group">
                                                                        <select class="form-control bkr_selectbox" name="phone_require_approvel">
                                                                            <option value="0" <?php if(Yii::$app->session->get('bk_phone_require_approvel')=='0'){ echo 'selected';} ?> >Doesn't require approval</option>
                                                                            <option value="1" <?php if(Yii::$app->session->get('bk_phone_require_approvel')=='1'){ echo 'selected';} ?>>Requires Approval</option>
                                                                        </select> <span class="savedMess" style="display:none;">saved</span>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                            <ul class="list-inline">
                                                                <li>
                                                                    <p>Non email verified members</p>
                                                                </li>
                                                                <li>
                                                                    <div class="form-group" name="nonemail_require_approvel">
                                                                        <select class="form-control bkr_selectbox" name="nonemail_require_approvel">
                                                                            <option value="0" <?php if(Yii::$app->session->get('bk_nonemail_require_approvel')=='0'){ echo 'selected';} ?> >Doesn't require approval</option>
                                                                            <option value="1" <?php if(Yii::$app->session->get('bk_nonemail_require_approvel')=='1'){ echo 'selected';} ?>>Requires Approval</option>
                                                                        </select> <span class="savedMess" style="display:none;">saved</span>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                                            <ul class="list-inline pull-right media-li">
                                                                <li>
                                                                    <label class="checkbox" style="margin-left: 21px;">
                                                                        <input type="checkbox" name="email_verified_approvel" value="1" <?php if(Yii::$app->session->get('bk_email_verified_approvel')=='0'){ echo 'checked';} ?> class="booking_rules_checkbox">Email verified members <span class="savedMess" style="display:none;">saved</span>
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <div class="form-group">
                                                                        <select class="form-control bkr_selectbox" name="email_require_approvel">
                                                                            <option value="0" <?php if(Yii::$app->session->get('bk_email_require_approvel')=='0'){ echo 'selected';} ?> >Doesn't require approval</option>
                                                                            <option value="1" <?php if(Yii::$app->session->get('bk_email_require_approvel')=='1'){ echo 'selected';} ?> >Requires Approval</option>
                                                                        </select>
                                                                        <span class="savedMess" style="display:none;">saved</span>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                           <!-- <ul class="list-inline">
                                                                <li class="pull-right"><button class="btn btn-main" style='padding: 10px 25px;'>Save</button></li>
                                                            </ul>-->
                                                        </div>
                                                    </div>
                                                    <hr>
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-6 col-sm-12">
                                                            <p>Note:</p>
                                                            <ul class="list-inline">
                                                                <li>
                                                                    <p>One call verification in</p>
                                                                </li>
                                                                <li>
                                                                    <div class="form-group">
                                                                        <select class="form-control">
                                                                            <option selected="">Pakistan</option>
                                                                            <option>Pakistan</option>
                                                                        </select>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                        <div class="col-lg-6 col-md-6">
                                                            <ul class="list-inline pull-right media-li">
                                                                <li>
                                                                    <label class="checkbox" style="margin-left: 21px;">
                                                                        <input type="checkbox"> One SMS verification in
                                                                    </label>
                                                                </li>
                                                                <li>
                                                                    <div class="form-group">
                                                                        <select class="form-control">
                                                                            <option selected="">Pakistan</option>
                                                                            <option>Pakistan</option>
                                                                        </select>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <button onclick="clickTab('#tab3')" class="btn btn-main pull-right" style="padding: 11px 36px 13px; margin-top: 4px;">Continue</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab3">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step3.png" class="img-responsive" style="margin-top: 0; margin-bottom: 28px; display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1" style="margin-top: 10px;">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">Strike-Out-Options</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label class="checkbox" style="margin-left: 21px;">
                                                            <input type="checkbox" name="strike_out_unavailable_times" <?php if(Yii::$app->session->get('bk_strike_out_unavailable_times')=='1'){ echo 'checked';} ?> value="1" class="booking_rules_checkbox"  > Check this box to strike out unavailable times instead of hiding them.  <span class="savedMess" style="display:none;">saved</span>
                                                        </label>
                                                        <p style="margin-top: -7px; margin-left: 23px;">
                                                            <small class="text-muted">
                                                                By showing your bookings and blocked times as struck out rather than hiding them, you can indicate to your customers that you are busy. This will encourage them to book early to avoid dissapointment.
                                                            </small>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row btn-row">
                                            <div class="col-lg-12">
                                                <button onclick="clickTab('#tab4')" class="btn btn-main pull-right" style="padding: 11px 36px 13px;">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab4">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step4.png" class="img-responsive" style="margin-top: 0; margin-bottom: 28px; display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">Layout Settings</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <ul class="list-inline">
                                                            <li class="li-media-auto" style="width: 300px;">
                                                                Select which schedule view should be shown by default on admin
                                                            </li>
                                                            <li>
                                                                <div class="form-group media-sel" style="position: relative; top: 0px;">
                                                                    <select class="form-control bkr_selectbox"  name="schedule_view_shown_by_default">
                                                                        <option value="week" <?php if(Yii::$app->session->get('bk_strike_out_unavailable_times')=='week'){ echo 'selected';} ?> >Week</option>
                                                                        <option value="month" <?php if(Yii::$app->session->get('bk_strike_out_unavailable_times')=='month'){ echo 'selected';} ?> >Month</option>
                                                                    </select>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                        <ul class="list-inline">
                                                            <li class="li-media-auto" style="width: 300px;">
                                                                On what day would you like your calendar to start?
                                                            </li>
                                                            <li>
                                                                <div class="form-group media-sel" style="position: relative; top: -13px;">
                                                                    <select class="form-control bkr_selectbox" name="calendar_start_day">
                                                                        <option value="today" <?php if(Yii::$app->session->get('bk_calendar_start_day')=='today'){ echo 'today';} ?> >Today</option>
                                                                        <option value="sunday" <?php if(Yii::$app->session->get('bk_calendar_start_day')=='sunday'){ echo 'selected';} ?> >Sunday</option>
                                                                        <option value="monday" <?php if(Yii::$app->session->get('bk_calendar_start_day')=='monday'){ echo 'selected';} ?> >Monday</option>
                                                                        <option value="tuesday" <?php if(Yii::$app->session->get('bk_calendar_start_day')=='tuesday'){ echo 'selected';} ?> >Tuesday</option>
                                                                        <option value="wednesday" <?php if(Yii::$app->session->get('bk_calendar_start_day')=='wednesday'){ echo 'selected';} ?> >Wednesday</option>
                                                                        <option value="thursday" <?php if(Yii::$app->session->get('bk_calendar_start_day')=='thursday'){ echo 'selected';} ?> >Thursday</option>
                                                                        <option value="friday" <?php if(Yii::$app->session->get('bk_calendar_start_day')=='friday'){ echo 'selected';} ?> >Friday</option>
                                                                        <option value="saturday" <?php if(Yii::$app->session->get('bk_calendar_start_day')=='saturday'){ echo 'selected';} ?> >Saturday</option>
                                                                    </select>
                                                                    <span class="savedMess" style="display:none;">saved</span>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-12">
                                                        <ul class="list-inline">
                                                            <li class="li-media-auto" style="width: 300px;">
                                                                What tab should be shown by default?
                                                            </li>
                                                            <li>
                                                                <div class="form-group media-sel">
                                                                    <select class="form-control bkr_selectbox" name="tab_shown_by_default">
                                                                        <option value="schedule" <?php if(Yii::$app->session->get('bk_tab_shown_by_default')=='schedule'){ echo 'selected';} ?>  >Schedule</option>
                                                                        <option value="about" <?php if(Yii::$app->session->get('bk_tab_shown_by_default')=='about'){ echo 'selected';} ?>  >About</option>
                                                                        <option value="reviews" <?php if(Yii::$app->session->get('bk_tab_shown_by_default')=='reviews'){ echo 'selected';} ?>  >Reviews</option>
                                                                    </select>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                        <ul class="list-inline">
                                                            <li class="li-media-auto" style="width: 300px;">
                                                                On what date would you like your calendar to start?
                                                            </li>
                                                            <li>
                                                                <div class="form-group media-sel" style="">
                                                                   <input type="text" class="form-control" />
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label class="checkbox" style="margin-left: 21px;">
                                                            <input type="checkbox" value="1" name="boocked_branding_hide" class="booking_rules_checkbox" <?php if(Yii::$app->session->get('bk_boocked_branding_hide')=='1'){ echo 'checked';} ?> > Check this box if you want to hide the Boocked branding on your customer interface  <span class="savedMess" style="display:none;">saved</span>
                                                        </label>
                                                        <p style="margin-top: -7px; margin-left: 23px;">
                                                            <small class="text-muted">
                                                                If you leave this box unchecked, the Boockd logo will appear on your customer calendar.
                                                            </small>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row btn-row" style="margin-top: 62px; margin-bottom: 2pc;">
                                            <div class="col-lg-12">
                                                <button onclick="clickTab('#tab5')" class="btn btn-main pull-right" style="padding: 11px 36px 13px;">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab5">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step5.png" class="img-responsive" style="margin-top: 0; margin-bottom: 28px; display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1" style="margin-top: 10px;">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">Service Options</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label class="checkbox" style="margin-left: 21px;">
                                                            <input type="checkbox" value="1" name="show_service_time_customer" class="booking_rules_checkbox" <?php if(Yii::$app->session->get('bk_show_service_time_customer')!='0'){ echo 'checked';} ?> > Check this box if you want to show the service time to your customers.  <span class="savedMess" style="display:none;">saved</span>
                                                        </label>
                                                        <label class="checkbox" style="margin-left: 21px;">
                                                            <input type="checkbox" value="1" name="show_service_cost_customer" class="booking_rules_checkbox" <?php if(Yii::$app->session->get('bk_show_service_cost_customer')!='0'){ echo 'checked';} ?> > Check this box if you want to show the service cost to your customers.  <span class="savedMess" style="display:none;">saved</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row btn-row" style="margin-top: 105px;">
                                            <div class="col-lg-12">
                                                <button class="btn btn-main pull-right" onclick="clickTab('#tab6')" style="padding: 11px 36px 13px;">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab6">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step6.png" class="img-responsive" style="margin-top: 0; margin-bottom: 28px; display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1" style="margin-top: 10px;">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">Staff Options</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label class="checkbox" style="margin-left: 21px;">
                                                            <input type="checkbox" value="1" name="staff_member_customer_visibility" <?php if(Yii::$app->session->get('bk_staff_member_customer_visibility')!='0'){ echo 'checked';} ?> class="booking_rules_checkbox"> Check this box if you want your staff members to be visible to customers.  <span class="savedMess" style="display:none;">saved</span>
                                                        </label>
                                                        <label class="checkbox" style="margin-left: 21px;">
                                                            <input type="checkbox" value="1" name="staff_member_selection_mandatory" <?php if(Yii::$app->session->get('bk_staff_member_selection_mandatory')=='1'){ echo 'checked';} ?> class="booking_rules_checkbox"> Check this box o make selection of staff mandatory.  <span class="savedMess" style="display:none;">saved</span>
                                                        </label>
                                                        <!--<ul class="list-inline">
                                                            <li><p>In what order should appointments be allocated to staff?</p></li>
                                                            <li class="pull-right">
                                                                <div class="form-group">
                                                                    <select class="form-control">
                                                                        <option selected="">Week</option>
                                                                        <option>week</option>
                                                                    </select>
                                                                </div>
                                                            </li>
                                                        </ul>-->
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row btn-row" style="margin-top: 81px;">
                                            <div class="col-lg-12">
                                                <button class="btn btn-main pull-right" onclick="clickTab('#tab7')" style="padding: 11px 36px 13px;">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab7">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step7.png" class="img-responsive" style="margin-top: 0; margin-bottom: 28px; display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1" style="margin-top: 10px;">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">Login Options</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <p>Select the Login method(s) you would like to use</p>
                                                        <div class="row">
                                                            <div class="col-sm-4">
                                                                <label class="checkbox" style="margin-left: 21px;">
                                                                    <input type="checkbox" value="1" name="boocked_login" <?php if(Yii::$app->session->get('bk_boocked_login')=='1'){ echo 'checked';} ?>  class="booking_rules_checkbox" > Boocked Login  <span class="savedMess" style="display:none;">saved</span>
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label class="checkbox" style="margin-left: 21px;">
                                                                    <input type="checkbox" value="1" name="facebook_login" class="booking_rules_checkbox" <?php if(Yii::$app->session->get('bk_facebook_login')=='1'){ echo 'checked';} ?>  > Facebook Login  <span class="savedMess" style="display:none;">saved</span>
                                                                </label>
                                                            </div>
                                                            <div class="col-sm-4">
                                                                <label class="checkbox" style="margin-left: 21px;">
                                                                    <input type="checkbox" value="1" name="google_login" class="booking_rules_checkbox" <?php if(Yii::$app->session->get('bk_google_login')=='1'){ echo 'checked';} ?>  > Google Login  <span class="savedMess" style="display:none;">saved</span>
                                                                </label>
                                                            </div>
                                                          <!--  <div class="col-lg-3 col-md-3 col-sm-3">
                                                                <label class="checkbox" style="margin-left: 21px;">
                                                                    <input type="checkbox" value="1" name="guest_login" class="booking_rules_checkbox" <?php if(Yii::$app->session->get('bk_guest_login')=='1'){ echo 'checked';} ?> > Guest Login  <span class="savedMess" style="display:none;">saved</span>
                                                                </label>
                                                            </div>-->
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row btn-row" style="margin-top: 125px;">
                                            <div class="col-lg-12">
                                                <button onclick="clickTab('#tab8')" class="btn btn-main pull-right" style="padding: 11px 36px 13px;">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab8">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body panel-tab8-main" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step8.png" class="img-responsive" style="margin-top: 0; margin-bottom: 28px; display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1" style="margin-top: 10px;">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">Appointment lead and cancellation times</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body panel-tab8">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <?= Yii::$app->session->get('bk_apointment_time_slot');?>
                                                        <label class="radio" style="margin-left: 21px;">
                                                            <input type="radio" class="booking_appointment_radio" name="apointment_time_slot" <?php if((Yii::$app->session->get('bk_apointment_time_slot')=='auto') || ((Yii::$app->session->get('bk_apointment_time_slot')=='fixed') && (Yii::$app->session->get('bk_apointment_time_slot')=='specific') ) ){ echo 'checked';} ?>  value="auto"> Allow Boocked to automatically calculate time slots on your calendar?
                                                        </label>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <ul class="list-inline">
                                                            <li>
                                                                <label class="radio booking_rules_checkbox" style="margin-left: 21px;">
                                                                    <input type="radio" name="apointment_time_slot" value="fixed" class="booking_appointment_radio" <?php if(Yii::$app->session->get('bk_apointment_time_slot')=='fixed'){ echo 'checked';} ?> />
                                                                    Set a fixed time interval to show on the calendar
                                                                </label>
                                                            </li>
                                                            <li class="pull-right">
                                                                <div class="form-group">
                                                                    <input type="text" style="max-width:50px;display:inline-block" value="<?php if(Yii::$app->session->get('bk_fixed_interval_customer')!=''){ echo Yii::$app->session->get('bk_fixed_interval_customer');}else { echo '30'; } ?>"  name="fixed_interval_customer" placeholder="30" class="form-control bkr_textinput"><span style="display:inline-block"> Mins</span>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <label class="radio" style="margin-left: 21px;">
                                                            <input type="radio" class="booking_appointment_radio" name="apointment_time_slot" value="specific" <?php if(Yii::$app->session->get('bk_apointment_time_slot')=='specific'){ echo 'checked';} ?> > Show only specific times on the calendar (separated by a comma)
                                                        </label>
                                                    </div>
                                                    <div class="col-lg-7 col-md-7 col-sm-7">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" placeholder="Enter text here">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-12 col-md-12 col-sm-12">
                                                        <p>
                                                            <small class="text-muted">Add times separated by comma(,) For example - 9:00 AM, 12:00 PM, 3:00 PM, 6:00 PM. If you want to open times in an ordered manner then use hash(#) as a separator. For example: If you dont to open at 3:00 PM unless 9:00 AM and 12:00 PM are boocked, then enter 9:00 AM, 12:00 PM & 3:00 PM.</small>
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="row" style="margin-top: 3%;">
                                                    <div class="col-lg-8 col-md-8 col-sm-8">
                                                        <p>Minimum advance notice required to book an appointment</p>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                                        <ul class="list-inline pull-right">
                                                            <li>
                                                                <div class="form-group li-right-item">
                                                                    <input type="text" class="form-control bkr_textinput" placeholder="30" name="advance_notice_time_book" value="<?php if(Yii::$app->session->get('bk_advance_notice_time_book')){ echo Yii::$app->session->get('bk_advance_notice_time_book');} ?> " style="width: 75px;">  <span class="savedMess" style="display:none;">saved</span>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="form-group media-select">
                                                                    <select class="form-control">
                                                                        <option selected="">Mins</option>
                                                                        <option>mins</option>
                                                                    </select>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="row" style="position: relative; top: -35px;">
                                                    <div class="col-lg-8 col-md-8 col-sm-8">
                                                        <p>Minimum notice required for canceling an appointment</p>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                                        <ul class="list-inline pull-right">
                                                            <li>
                                                                <div class="form-group li-right-item">
                                                                    <input type="text" class="form-control bkr_textinput" value="<?php if(Yii::$app->session->get('bk_advance_notice_time_cancel')){ echo Yii::$app->session->get('bk_advance_notice_time_cancel');} ?>" name="advance_notice_time_cancel" placeholder="30" style="width: 75px;" >  <span class="savedMess" style="display:none;">saved</span>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="form-group media-select">
                                                                    <select class="form-control" class="form-control">
                                                                        <option selected="">Mins</option>
                                                                        <option>mins</option>
                                                                    </select>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="row" style="position: relative; top: -72px;">
                                                    <div class="col-lg-8 col-md-8 col-sm-8">
                                                        <p>Minimum notice required for rescheduling an appointment</p>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                                        <ul class="list-inline pull-right">
                                                            <li>
                                                                <div class="form-group li-right-item">
                                                                    <input type="text" class="form-control bkr_textinput" name="advance_notice_time_reschedule" placeholder="30" style="width: 75px;" value="<?php if(Yii::$app->session->get('bk_advance_notice_time_reschedule')){ echo Yii::$app->session->get('bk_advance_notice_time_reschedule');} ?>" ><span class="savedMess" style="display:none;">
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="form-group media-select">
                                                                    <select class="form-control" class="form-control">
                                                                        <option selected="">Mins</option>
                                                                        <option>mins</option>
                                                                    </select>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="row" style="position: relative; top: -108px;">
                                                    <div class="col-lg-8 col-md-8 col-sm-8">
                                                        <p>How many days in advance can appointmets be booked?</p>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                                        <ul class="list-inline pull-right">
                                                            <li>
                                                                <div class="form-group li-right-item-rel">
                                                                    <input  class="form-control bkr_textinput" name="advance_days_booking"  value="<?php if(Yii::$app->session->get('bk_advance_days_booking')){ echo Yii::$app->session->get('bk_advance_days_booking');} ?>"  placeholder="30" style="width: 75px;"><span class="savedMess" style="display:none;">
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="row" style="position: relative; top: -123px;">
                                                    <div class="col-lg-8 col-md-8 col-sm-8">
                                                        <p>Minimum time interval required between appointmenst</p>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                                        <ul class="list-inline pull-right">
                                                            <li>
                                                                <div class="form-group li-right-item">
                                                                    <input type="text" class="form-control bkr_textinput" name="min_interval_between_appointment" value="<?php if(Yii::$app->session->get('bk_min_interval_between_appointment')){ echo Yii::$app->session->get('bk_min_interval_between_appointment');} ?>" placeholder="30" style="width: 75px;"><span class="savedMess" style="display:none;">
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="form-group media-select">
                                                                    <select class="form-control" class="form-control">
                                                                        <option selected="">Mins</option>
                                                                        <option>mins</option>
                                                                    </select>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="row" style="position: relative; top: -160px;">
                                                    <div class="col-lg-8 col-md-8 col-sm-8">
                                                        <p>Set time interval for Administrator</p>
                                                    </div>
                                                    <div class="col-lg-4 col-md-4 col-sm-4">
                                                        <ul class="list-inline pull-right">
                                                            <li>
                                                                <div class="form-group li-right-item-rel">
                                                                    <input type="text" class="form-control bkr_textinput" name="time_interval_admin" value="<?php if(Yii::$app->session->get('bk_time_interval_admin')){ echo Yii::$app->session->get('bk_time_interval_admin');} ?>" placeholder="30" style="width: 75px;"><span class="savedMess" style="display:none;">
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row btn-row" style="margin-top: 0pc; margin-bottom: 0pc;">
                                            <div class="col-lg-12">
                                                <button onclick="clickTab('#tab9')" class="btn btn-main pull-right" style="padding: 11px 36px 13px;">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab9">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step9.png" class="img-responsive" style="margin-top: 0; margin-bottom: 28px; display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1" style="margin-top: 10px;">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">What information do you require from the customer at the time of registration?</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <p>Do you require the following information from customer at the time of signup?</p>
                                                        <div class="row">
                                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                                <label class="checkbox" style="margin-left: 21px;">
                                                                    <input type="checkbox" value="1" name="address_field" class="booking_rules_checkbox" <?php if(Yii::$app->session->get('bk_address_field')=='1'){ echo 'checked';} ?>  > Address  <span class="savedMess" style="display:none;">saved</span>
                                                                </label>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                                <label class="checkbox" style="margin-left: 21px;">
                                                                    <input type="checkbox" value="1" name="zip_field" class="booking_rules_checkbox" <?php if(Yii::$app->session->get('bk_zip_field')=='1'){ echo 'checked';} ?> > Zip  <span class="savedMess" style="display:none;">saved</span>
                                                                </label>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                                <label class="checkbox" style="margin-left: 21px;">
                                                                    <input type="checkbox" value="1" name="city_state_field" class="booking_rules_checkbox" <?php if(Yii::$app->session->get('bk_city_state_field')!='0'){ echo 'checked';} ?> > City & State  <span class="savedMess" style="display:none;">saved</span>
                                                                </label>
                                                            </div>
                                                            <div class="col-lg-3 col-md-3 col-sm-3">
                                                                <label class="checkbox" style="margin-left: 21px;">
                                                                    <input type="checkbox" value="1" name="mobile_field" class="booking_rules_checkbox" <?php if(Yii::$app->session->get('bk_mobile_field')=='1'){ echo 'checked';} ?> > Mobile Phone  <span class="savedMess" style="display:none;">saved</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <p class="text-muted">
                                                                    <small>By default, the customer is asked for their first name last name, email and to create the password check the relevant boxes to select any other information that you require at registration.</small>
                                                                </p>
                                                                <!--<p>
                                                                    <small>Need more information? <a href="" style="color: #F27B21; text-decoration: underline;">Create a custom form</a></small>
                                                                </p>--
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row btn-row" style="margin-top: 65px;">
                                            <div class="col-lg-12">
                                                <button class="btn btn-main pull-right" style="padding: 11px 36px 13px;">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab10">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step10.png" class="img-responsive" style="margin-top: 0; margin-bottom: 28px; display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1" style="margin-top: 10px;">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">Tracking Code</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <p>Enter URL of page on ehich you have included Google adwords script</p>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <div class="form-group">
                                                                    <input type="text" placeholder="Enter text here..." name="google_tracking_url" class="form-control bkr_textinput" <?php if(Yii::$app->session->get('google_tracking_url')){ echo Yii::$app->session->get('google_tracking_url');} ?> ><span class="savedMess" style="display:none;">saved</span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-12">
                                                                <label class="checkbox" style="margin-left: 21px;">
                                                                    <input type="checkbox" value="1" name="redirect_tracking_on_payment" class="booking_rules_checkbox" <?php if(Yii::$app->session->get('bk_redirect_tracking_on_payment')=='1'){ echo 'checked';} ?> > Run script when user is redirected to payment gateway.  <span class="savedMess" style="display:none;">saved</span>
                                                                </label>
                                                                <p>
                                                                    <small class="text-muted">You can track all appointments that are boocked through your Google advertisements by placing the Google Adword script on your page and entering the page URL here. Boocked will then automatically call this page in the background providing you with accurate data on how well your advertisement is performing.</small>
                                                                </p>
                                                                <!--<button class="btn btn-main" style="padding-left: 30px; padding-right: 30px;">Save</button>-->
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row btn-row" style="margin-top: 42px;margin-bottom: 1pc;">
                                            <div class="col-lg-12">
                                                <button class="btn btn-main pull-right" style="padding: 11px 36px 13px;">Save</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-customer-details">
            <div class="modal-dialog modal-dialog-customer">
                <div class="modal-content">
                    <div class="modal-header">
                        <ul class="list-inline list-modal-header">
                            <li class="text-uppercase"><i class="fa fa-user" style="color: #FC7600;"></i> customer</li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                                <a href=""><i class="fa fa-reply"></i></a>
                            </li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                                <a href=""><i class="fa fa-plus"></i></a>
                            </li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban" id="ban-btn-toggle"><i class="fa fa-ban"></i></a></li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay" id="pay-btn-toggle"><i class="fa fa-dollar"></i></a></li>
                            <li class="pull-right" style="font-size: 12px;">Gift Certificates / Discount Coupons</li>
                        </ul>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body-inner-contents">
                            <div class="row">
                                <div class="col-lg-7 col-md-7 col-sm-7">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <i class="fa fa-plus upload-image" data-toggle="tooltip" data-placement="top" title="Upload Image"></i>
                                            <img src="<?= Yii::getAlias('@web') ?>/img/staff2.png">
                                        </div>
                                        <div class="col-lg-8">
                                            <h4 class="txt-theme" style="position: relative; top: 9px;">John Doe</h4>
                                            <p><small>Johndoe@gmail.com</small></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <ul class="list-inline" style="margin-top: 15px;">
                                                <li><small><a href="" class="txt-theme">Invite to schedule online</a></small></li><li class="txt-theme"><small>|</small></li>
                                                <li><small><a href="" class="txt-theme">Verify</a></small></li><li class="txt-theme"><small>|</small></li>
                                                <li><small><a href="" class="txt-theme">Edit</a></small></li><li class="txt-theme"><small>|</small></li>
                                                <li><small><a href="" class="txt-theme">Delete</a></small></li>
                                            </ul>
                                            <button class="btn btn-main" style="padding: 4px 9px;"><i class="fa fa-plus"></i> Add Tags</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 media-col6">
                                    <div class="row">
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                            <i class="fa fa-map-marker txt-theme"></i>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                            <p><small>145 strait street, CT.No City, No Region United States(01234)</small></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                            <i class="fa fa-mobile txt-theme"></i>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                            <p><small>098-879-46548</small></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12" style="margin-top: 8px;"><button class="btn btn-main" style="padding: 3px 4px;"><small>Send Gift Certificates</small></button></div>
                                        <div class="col-lg-12" style="margin-top: 5px;"><button class="btn btn-main" style="padding: 3px 4px;"><small>Send Discount Coupons</small></button></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px;">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="2" placeholder="Add Information"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="modal-body-inner-tabs">
                                <ul class="nav nav-tabs nav-main nav-customer-details">
                                    <li class="active">
                                        <a href="#up-app" data-toggle="tab">Upcoming Appointments</a>
                                    </li>
                                    <li>
                                        <a href="#past-app" data-toggle="tab">Past Appointments</a>
                                    </li>
                                    <li>
                                        <a href="#payments" data-toggle="tab">Payments</a>
                                    </li>
                                    <li>
                                        <a href="#promotion" data-toggle="tab">Promotion</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="up-app">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="past-app">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="payments">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="promotion">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="ban">
            <div class="modal-dialog modal-dialog-customer">
                <div class="modal-content">
                    <div class="modal-header">
                        <ul class="list-inline list-modal-header">
                            <li class="text-uppercase"><i class="fa fa-user" style="color: #FC7600;"></i> customer booking restrictions</li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                                <a href=""><i class="fa fa-reply"></i></a>
                            </li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                                <a href=""><i class="fa fa-plus"></i></a>
                            </li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban"><i class="fa fa-ban"></i></a></li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay"><i class="fa fa-dollar"></i></a></li>
                        </ul>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body-inner-contents">
                            <div class="alert alert-warning">
                                <i class="fa fa-warning"></i> <small>Client Advance setting is a premium feature available with PRO and above packages. <a href="#" class="txt-theme">Upgrade your account </a> or <a href="" class="txt-theme">Compare various Packages</a></small>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>This Customer is</p>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option selected>Post-Paying</option>
                                            <option>Post-Paying</option>
                                            <option>Post-Paying</option>
                                        </select>
                                    </div>
                                    <p class="text-muted">
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                        </small>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>Future Bookings Status</p>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option selected>As per settings</option>
                                            <option>Post-Paying</option>
                                            <option>Post-Paying</option>
                                        </select>
                                    </div>
                                    <p class="text-muted">
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                        </small>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>With Restriction</p>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option selected>As per settings</option>
                                            <option>Post-Paying</option>
                                            <option>Post-Paying</option>
                                        </select>
                                    </div>
                                    <p class="text-muted">
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                        </small>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="list-inline pull-right">
                                        <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;">Cancel</button></li>
                                        <li><button class="btn btn-main" style="padding-right: 19px; padding-left: 19px;">Save</button></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="pay">
            <div class="modal-dialog modal-dialog-customer">
                <div class="modal-content">
                    <div class="modal-header">
                        <ul class="list-inline list-modal-header">
                            <li class="text-uppercase"><i class="fa fa-dollar" style="color: #FC7600;"></i> add membership payment</li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                                <a href=""><i class="fa fa-reply"></i></a>
                            </li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                                <a href=""><i class="fa fa-plus"></i></a>
                            </li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban"><i class="fa fa-ban"></i></a></li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay"><i class="fa fa-dollar"></i></a></li>
                        </ul>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <p>Payment Date</p>
                                <div class="form-group">
                                    <input type="text" placeholder="4-17-2016" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <p>Payment For</p>
                                <div class="form-group">
                                    <input type="text" placeholder="Add New Payment" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="text-muted pull-right" style="margin-top: -9px;"><small>Customer restrictions of the selected membership will be applied automatically.</small></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <textarea class="form-control" rows="2" placeholder="Note against payment"></textarea>
                                    <p class="text-danger pull-right"><small>Please do not enter html tags</small></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="modal-body-inner-tabs">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <p class="txt-theme">Amount</p>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Additional Charges">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <span style="position: absolute; left: -2px;">$</span> <input type="text" class="form-control" placeholder="$$$">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Discount">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <span style="position: absolute; left: -6px;">by</span>
                                            <select class="form-control" placeholder="Cash">
                                                <option selected="">Cash</option>
                                                <option>d</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Total">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <ul class="list-inline pull-right">
                                            <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;">Cancel</button></li>
                                            <li><button class="btn btn-main" style="padding-right: 19px; padding-left: 19px;">Save</button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>





<input type="hidden" id="booking_csrf" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />





































<?php
$csrf_ttoken = '"_csrf-token"';

$script = "
$('.booking_rules_checkbox').change(function(){
var savedmess = $(this).siblings('.savedMess');
   var chkbox_name =  $(this).attr('name');
if($(this).is(':checked')){
  var chkbox_val = 1;
  }else {
 
    var chkbox_val = 0;
  }
   var csrf_name = $('booking_csrf').attr('name');
   var csrf_val = $('booking_csrf').val();
$.ajax({
url: '".Url::toRoute(['/booking-rules/updaterulessetting'])."',
type: 'post',
data:{csrf_name:csrf_val,
'SettingBookingRules[bkrules_name]':chkbox_name,
'SettingBookingRules[bkrules_value]':chkbox_val},
success: function (response) {
    if(response==1){
 savedmess.css('display','inline');
     setTimeout(function(){
        savedmess.hide('slow');     
},2000);
    }else {
        alert('not updated');

    }
}
});

});
$('.bkr_selectbox').change(function(){
var savedmess = $(this).siblings('.savedMess');
   var optionval =  $('option:selected',this).val();
   var selectbox_name =  $(this).attr('name');
   var csrf_name = $('booking_csrf').attr('name');
   var csrf_val = $('booking_csrf').val();
$.ajax({
url: '".Url::toRoute(['/booking-rules/updaterulessetting'])."',
type: 'post',
data:{csrf_name:csrf_val,
'SettingBookingRules[bkrules_name]':selectbox_name,
'SettingBookingRules[bkrules_value]':optionval},
success: function (response) {
    if(response==1){
    
 savedmess.css('display','inline');
     setTimeout(function(){
 
    savedmess.hide('slow');
}, 2000);
    }else {
        alert('not updated');

    }
}
});

});


$('.bkr_textinput').change(function(){

var savedmess = $(this).siblings('.savedMess');
   var optionval =  $(this).val();
   var selectbox_name =  $(this).attr('name');
   var csrf_name = $('booking_csrf').attr('name');
   var csrf_val = $('booking_csrf').val();
$.ajax({
url: '".Url::toRoute(['/booking-rules/updaterulessetting'])."',
type: 'post',
data:{csrf_name:csrf_val,
'SettingBookingRules[bkrules_name]':selectbox_name,
'SettingBookingRules[bkrules_value]':optionval},
success: function (response) {
    if(response==1){
    
 savedmess.css('display','inline');
     setTimeout(function(){
 
    savedmess.hide('slow');
}, 2000);
    }else {
        alert('not updated');

    }
}
});

});

$('.booking_appointment_radio').change(function(){
alert('sdasda');
var savedmess = $(this).siblings('.savedMess');
   var chkbox_name =  $(this).attr('name');
if($(this).is(':checked')){
  var chkbox_val = $(this).val();
  }else {
 
    var chkbox_val = 0;
  }
   var csrf_name = $('booking_csrf').attr('name');
   var csrf_val = $('booking_csrf').val();
$.ajax({
url: '".Url::toRoute(['/booking-rules/updaterulessetting'])."',
type: 'post',
data:{csrf_name:csrf_val,
'SettingBookingRules[bkrules_name]':chkbox_name,
'SettingBookingRules[bkrules_value]':chkbox_val},
success: function (response) {
    if(response==1){
 savedmess.css('display','inline');
     setTimeout(function(){
        savedmess.hide('slow');     
},2000);
    }else {
        alert('not updated');

    }
}
});

});
";

$this->registerJs($script);
?>
<script>
    function clickTab(link){
        $('a[href="'+link+'"]').click();
    }
function setTimezoneSetting(currentEle){
    var itemvalue = currentEle.value;
    var itemname = currentEle.name;
    var curformaction = $('#timezoneform').attr('action');
    $.ajax({
        url: curformaction,
        type: 'post',
        data:{'item_name':itemname, 'item_value':itemvalue},
        success: function (response) {
            if(response==1){
                alert('Updated');

            }else {
                alert('not updated');

            }
        }
    });



 //'var abc = ad.attr('id');
  //  alert(abc);
}
</script>
