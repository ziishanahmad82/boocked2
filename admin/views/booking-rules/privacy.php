<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;


$this->title = 'Privacy';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>

</style>

    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 no-padding-l media-xs-full">
            <div class="panel panel-default">
                <div class="panel-heading full">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><i class="fa fa-square" style="color:#FC7600"></i> <span class="staff">Manage Privacy</span></h5></li>
                    </ul>
                </div>
                <div class="panel-body panel-body-nav-tabs-sidebar side1">
                    <p style="padding-top:20px">Easily manage your privacy policy and terms and conditions from this page.You can edit or update your policies at anytime. </p>
                </div>
            </div>
        </div>

        <div class="col-lg-9 col-md-9 col-sm-9 media-xs-full full1">
            <div class="panel panel-default">
                <div class="panel-heading full">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><i class="fa fa-lock" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Privacy</span></h5></li>
                        <li class="pull-right li-right hidden-xs">Control how your information is publicly displayed</li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="service">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="panel panel-default">
                                            <div class="panel-heading full">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title "><span class="staff-head">Reviews</span></h5></li>

                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 top">
                                                        <div class="form-group">
                                                            <label class="checkbox" style="left:30px">
                                                                <input type="checkbox" name="show_customer_name_reviews" value="1" class="booking_rules_checkbox" <?php if(Yii::$app->session->get('bk_show_customer_name_reviews')!='0'){ echo 'checked';} ?>  ><span style="padding-left:10px;font-weight: normal;">Check this box if you want to show your customer's name with their reviews</span> <span class="savedMess" style="display:none;">saved</span>
                                                            </label>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="panel panel-default" style="margin-top: 2%;">
                                                <div class="panel-heading full">
                                                    <ul class="list-inline list-staff">
                                                        <li><h5 class="panel-title "><span class="staff-head"></span>Terms & Conditions</h5></li>

                                                    </ul>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-lg-12 top">
                                                           <textarea class="bkr_textinput form-control well hei" id="textareacharacter_limit" name="privacy_terms" maxlength="2000"><?php if(Yii::$app->session->get('bk_privacy_terms')){ echo Yii::$app->session->get('bk_privacy_terms');} ?>  </textarea>
                                                            <span class="savedMess" style="display:none;">saved</span>

                                                            <p class="textarea_characters">2000 characters remaining</p>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <div class="panel panel-default">
                                                <div class="panel-heading full">
                                                    <ul class="list-inline list-staff">
                                                        <li><h5 class="panel-title "><span class="staff-head">Social Promotions</span></h5></li>

                                                    </ul>
                                                </div>
                                                <div class="panel-body">
                                                    <div class="row">
                                                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 top">
                                                            <div class="form-group">
                                                                <label class="checkbox" style="left:30px">
                                                                    <input type="checkbox" name="allow_customer_promote_social" value="1" class="booking_rules_checkbox" <?php if(Yii::$app->session->get('bk_allow_customer_promote_social')=='1'){ echo 'checked';} ?>  ><span style="padding-left:10px;font-weight: normal;">Check this box to allow your customers to promote there appointment on social networks </span><span class="savedMess" style="display:none;">saved</span>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>

                        </div>

                    </div>


                </div>
            </div>
        </div>
    </div>

































<input type="hidden" id="booking_csrf" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />





































<?php
$script = "
$('body').on('beforeSubmit', 'form#resourcecommonform', function () {
var form = $(this);

if (form.find('.has-error').length) {
return false;
}
$.ajax({
url: form.attr('action'),
type: 'post',
data: form.serialize(),
success: function (response) {
    if(response==1){
        alert('changed');
         $.pjax.reload({container:'#domainnamesection'});

    }else {
        alert('not updated');

    }
}
});
return false
});
$('.booking_rules_checkbox').change(function(){
   var chkbox_name =  $(this).attr('name');
   var savedmess = $(this).siblings('.savedMess');
      
if($(this).is(':checked')){
  var chkbox_val = 1;
  }else {
    var chkbox_val = 0;
  }
   var csrf_name = $('booking_csrf').attr('name');
   var csrf_val = $('booking_csrf').val();
$.ajax({
url: 'updaterulessetting',
type: 'post',
data:{csrf_name:csrf_val,
'SettingBookingRules[bkrules_name]':chkbox_name,
'SettingBookingRules[bkrules_value]':chkbox_val},
success: function (response) {
    if(response==1){
     savedmess.css('display','inline');
     setTimeout(function(){
 
    savedmess.hide('slow');
}, 2000);

    }else {
        alert('not updated');

    }
}
});

});
$('.bkr_selectbox').change(function(){

   var optionval =  $('option:selected',this).val();
   var selectbox_name =  $(this).attr('name');

   var csrf_name = $('booking_csrf').attr('name');
   var csrf_val = $('booking_csrf').val();
$.ajax({
url: 'updaterulessetting',
type: 'post',
data:{csrf_name:csrf_val,
'SettingBookingRules[bkrules_name]':selectbox_name,
'SettingBookingRules[bkrules_value]':optionval},
success: function (response) {
    if(response==1){
      // $('#business_informationform .alert').show(0).delay(2000).hide(0);
       savedmess.css('display','inline');
     setTimeout(function(){
 
    savedmess.hide('slow');
}, 2000);
alert('updated');
    }else {
        alert('not updated');

    }
}
});
});
$('.bkr_textinput').change(function(){

var savedmess = $(this).siblings('.savedMess');
   var optionval =  $(this).val();
   var selectbox_name =  $(this).attr('name');
   var csrf_name = $('booking_csrf').attr('name');
   var csrf_val = $('booking_csrf').val();
$.ajax({
url: 'updaterulessetting',
type: 'post',
data:{csrf_name:csrf_val,
'SettingBookingRules[bkrules_name]':selectbox_name,
'SettingBookingRules[bkrules_value]':optionval},
success: function (response) {
    if(response==1){
    
 savedmess.css('display','inline');
     setTimeout(function(){
 
    savedmess.hide('slow');
}, 2000);
    }else {
        alert('not updated');

    }
}
});
});
});

 var text_max = 2000;
    $('.textarea_characters').html(text_max + ' characters remaining');

    $('#textareacharacter_limit').keyup(function() {
        var text_length = $('#textareacharacter_limit').val().length;
        var text_remaining = text_max - text_length;

        $('.textarea_characters').html(text_remaining + ' characters remaining');

";

$this->registerJs($script);
?>
<script>
function setTimezoneSetting(currentEle){
    var itemvalue = currentEle.value;
    var itemname = currentEle.name;
    var curformaction = $('#timezoneform').attr('action');
    $.ajax({
        url: curformaction,
        type: 'post',
        data:{'item_name':itemname, 'item_value':itemvalue},
        success: function (response) {
            if(response==1){
                alert('Updated');

            }else {
                alert('not updated');

            }
        }
    });



 //'var abc = ad.attr('id');
  //  alert(abc);
}


</script>
