<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;


$this->title = 'Advance Setting';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>

</style>


    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 no-padding-l media-xs-full">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web') ?>/img/settings.png"> <span class="staff-head">Manage Rules</span></h5></li>
                    </ul>
                </div>
                <div class="panel-body panel-body-nav-tabs-sidebar">
                    <div class="row">
                        <ul class="nav nav-stacked nav-tabs nav-tabs-sidebar nav-rules">
                            <li class="active"><a href="#tab1" data-toggle="tab">1 Back to Back Service Booking</a></li>
                            <li><a href="#tab2" data-toggle="tab">2 Recurring / Repeat Booking</a></li>
                            <li><a href="#tab3" class="last" data-toggle="tab">3 Quantity Booking</a></li>
                            <li><a href="#tab4" class="last" data-toggle="tab">4 International Appointment...</a></li>
                            <li><a href="#tab5" class="last" data-toggle="tab">5 Booking restriction</a></li>
                            <li><a href="#tab6" class="last" data-toggle="tab">6 Domain Restriction for Booking</a></li>
                            <li><a href="#tab7" class="last" data-toggle="tab">7 Payment Options</a></li>
                            <li><a href="#tab8" class="last" data-toggle="tab">8 Tax</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-9 col-md-9 col-sm-9 media-xs-full">
            <div class="panel panel-default panel-rules2">
                <div class="panel-heading">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><i class="fa fa-file-code-o" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Rules</span></h5></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="tab1">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step1-adv.png" class="img-responsive" style="margin-top: 0; margin-bottom: 22px; display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">Back to Back Service Booking</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label class="checkbox" style="margin-left: 21px;">

                                                            <input type="checkbox" name="allow_customer_multiple_service" value="1"  <?php if(Yii::$app->session->get('bk_allow_customer_multiple_service')=='1'){ echo 'checked';} ?> class="booking_rules_checkbox"> Allow your customers to select multiple services?
                                                            <span class="savedMess" style="display:none;">saved</span>
                                                        </label>
                                                        <p style="margin-top: -7px; margin-left: 23px;">
                                                            <small class="text-muted">
                                                                By checking this box, you will allow your customers to select multiple services at the time of booking. Only times when the selected services can be performed consecutively will be shown.
                                                            </small>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row btn-row" style="margin-top: 53px; margin-bottom: 35px;">
                                            <div class="col-lg-12">
                                                <button class="btn btn-main pull-right" style="padding: 11px 36px 13px;">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab2">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step2-adv.png" class="img-responsive" style="margin-top: 0; margin-bottom: 22px; display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">Recurring / Repeat Booking</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label class="checkbox" style="margin-left: 21px;">
                                                            <input type="checkbox" name="allow_customer_recurring_appointments" <?php if(Yii::$app->session->get('bk_allow_customer_recurring_appointments')=='1'){ echo 'checked';} ?> value="1" class="booking_rules_checkbox"> Allow customers to book recurring appointments   <span class="savedMess" style="display:none;">saved</span>
                                                        </label>
                                                    </div>
                                                    <div class="col-lg-12">
                                                        <label class="checkbox" style="margin-left: 21px;">
                                                            <input type="checkbox" name="allow_admin_show_recurring" value="1" <?php if(Yii::$app->session->get('bk_allow_admin_show_recurring')=='1'){ echo 'checked';} ?> class="booking_rules_checkbox">Show recurring option for Admin <span class="savedMess" style="display:none;">saved</span>
                                                        </label>
                                                        <p style="margin-top: -7px; margin-left: 23px;">
                                                            <small class="text-muted">
                                                                Boost your sales by allowing your customers to book repeat appointments in a single login session
                                                            </small>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row btn-row" style="margin-top: 2pc; margin-bottom: 16px;">
                                            <div class="col-lg-12">
                                                <button class="btn btn-main pull-right" style="padding: 11px 36px 13px;">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab3">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step3-adv.png" class="img-responsive" style="margin-top: 0; margin-bottom: 22px; display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">Quantity Booking</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label class="checkbox" style="margin-left: 21px;">
                                                            <input type="checkbox" name="allow_customer_quantity_booking" <?php if(Yii::$app->session->get('bk_allow_customer_quantity_booking')=='1'){ echo 'checked';} ?>  value="1" class="booking_rules_checkbox"> Allow customers to select quantity for their appointment  <span class="savedMess" style="display:none;">saved</span>
                                                        </label>
                                                        <p style="margin-top: -7px; margin-left: 23px;">
                                                            <small class="text-muted">
                                                                Check this box to allow your customers to reserve an appointment for more than one person. The quantity will be shown based on the number of staff performing a service at the time the customer wants to make a reservation or the capacity available.
                                                            </small>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row btn-row" style="margin-top: 22px; margin-bottom: 20px;">
                                            <div class="col-lg-12">
                                                <button class="btn btn-main pull-right" style="padding: 11px 36px 13px;">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab4">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step4-adv.png" class="img-responsive" style="margin-top: 0; margin-bottom: 22px; display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">International Appointment & timezone</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label class="checkbox" style="margin-left: 21px;">
                                                            <input type="checkbox" name="allow_international_user" value="1" <?php if(Yii::$app->session->get('bk_allow_international_user')=='1'){ echo 'checked';} ?> class="booking_rules_checkbox"> Allow international users (outside your country) to book appointments  <span class="savedMess" style="display:none;">saved</span>
                                                            
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row btn-row" style="margin-top: 22px; margin-bottom: 63px;">
                                            <div class="col-lg-12">
                                                <button class="btn btn-main pull-right" style="padding: 11px 36px 13px;">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab5">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step5-adv.png" class="img-responsive" style="margin-top: 0; margin-bottom: 22px; display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">booking restriction</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label class="checkbox" style="margin-left: 21px;">
                                                            <input type="checkbox" name="restriction_on_booking_number" value="1" <?php if(Yii::$app->session->get('bk_restriction_on_booking_number')=='1'){ echo 'checked';} ?> class="booking_rules_checkbox" > Check this box if you want to apply restriction on the number of bookings a customer can make  <span class="savedMess" style="display:none;">saved</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row btn-row" style="margin-top: 22px; margin-bottom: 63px;">
                                            <div class="col-lg-12">
                                                <button class="btn btn-main pull-right" style="padding: 11px 36px 13px;">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab6">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step6-adv.png" class="img-responsive" style="margin-top: 0; margin-bottom: 22px; display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">Domain Restriction for Booking</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <?php Pjax::begin(['id'=>'domainnamesection']); ?>
                                                        <p>Enter domain you want to allow for booking</p>
                                                        <div class="row">
                                                            <?php  $form = ActiveForm::begin(['id'=>'resourcecommonform', 'action' => 'savedomains',
                                                                'enableAjaxValidation' => true, 'validationUrl' => 'domainvalidate',]); ?>
                                                            <div class="col-lg-10">
                                                                <div class="form-group">

                                                                    <?= $form->field($domain_model, 'domain_name',[ 'labelOptions' => [ 'class' => 'small text-muted']])->textInput(['maxlength' => true])->label(false) ?>



                                                                   <!-- <input type="text" placeholder="Enter text here" class="form-control">-->
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-2">
                                                                <?= Html::submitButton($domain_model->isNewRecord ? 'Add' : 'Update', ['class' => $domain_model->isNewRecord ? 'btn btn-main' : 'btn btn-main']) ?>
                                                               <!-- <button class="btn btn-main" style="padding: 8px 36px;">Add</button> -->
                                                            </div>
                                                            <?php ActiveForm::end(); ?>
                                                        </div>
                                                        <p>
                                                            <?php foreach($restricted_domains as $restricted_domain){ ?>
                                                            <a href="<?= $restricted_domain->rd_id;?>" style="color:#fff" onclick="trash_tracking_domain(event,this)" class="btn btn-main"><?= $restricted_domain->domain_name; ?> <span class="glyphicon glyphicon-trash" ></span></a>
                                                            <?php } ?>
                                                        </p>
                                                        <p><small class="text-muted">You can restrict bookings to a specified domain. IF you want anyone to be able to book, leave this field blank</small></p>
                                                        <?php Pjax::end(); ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row btn-row" style="margin-top: 2pc; margin-bottom: 10px;">
                                            <div class="col-lg-12">
                                                <button class="btn btn-main pull-right" style="padding: 11px 36px 13px;">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab7">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step7-adv.png" class="img-responsive" style="margin-top: 0; margin-bottom: 22px; display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">Pre-Payment</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <label class="checkbox" style="margin-left: 21px;">
                                                            <input type="checkbox" name="restriction_limit_customer_booking" <?php if(Yii::$app->session->get('bk_restriction_limit_customer_booking')=='1'){ echo 'checked';} ?>  value="1" class="booking_rules_checkbox"> Check this box if you want to apply restriction on the number of bookings a customer can make  <span class="savedMess" style="display:none;">saved</span>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1" style="margin-top: 1pc;">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">Payment Setting</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body" style="padding-bottom: 0;">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <p class="txt-theme">Payment Gateways</p>
                                                    </div>
                                                </div>
                                                <div class="row">
                                                    <?php Pjax::begin(['id'=>'paypalsection']); ?>
                                                    <div class="content-inner-top">
                                                        <?php  $form = ActiveForm::begin([ 'action' => 'paypalsubmission',
                                                            'enableAjaxValidation' => true, 'validationUrl' => 'paypalvalidate','options' => ['class' => 'paypalform']]); ?>
                                                        <p class="text-uppercase"><b>paypal</b></p>
                                                        <label class="checkbox" style="">
                                                            <?= $form->field($paypal_payment, 'gateway_type')->hiddenInput(['value'=>'paypal'])->label(false); ?>
                                                            <?= $form->field($paypal_payment, 'pg_status')->checkBox(['label' =>'Enable Paypal','uncheck' => 0, 'check' => 1])->label(false) ?>
                                                        </label>
                                                        <div class="row">
                                                            <div class="col-lg-2"><p>Username</p></div>
                                                            <div class="col-lg-10">
                                                                <div class="form-group">
                                                                    <?= $form->field($paypal_payment, 'api_login')->textInput(['maxlength' => true])->label(false) ?>
                                                                </div>
                                                                <span class="savedMess" style="display:none;">saved</span>
                                                                <?= Html::submitButton($paypal_payment->isNewRecord ? 'Save' : 'Save', ['class' => $paypal_payment->isNewRecord ? 'btn btn-main pull-right' : 'btn btn-main pull-right']) ?>
                                                            </div>
                                                        </div><hr>
                                                        <p><b>Enable</b> Check this box to accept payments through your <b>Paypal</b> account.</p>
                                                        <p><b>Username</b> Enter your Paypal email address through which you want to accept payments <span class="txt-theme"><small>e.g. payment@bookings.com</small></span></p>
                                                        <?php ActiveForm::end(); ?>
                                                    </div>
                                                    <?php Pjax::end(); ?>
                                                    <?php Pjax::begin(['id'=>'paypalsection']); ?>
                                                    <?php  $form = ActiveForm::begin([ 'action' => 'paypalsubmission',
                                                        'enableAjaxValidation' => true, 'validationUrl' => 'paypalvalidate', 'options' => ['class' => 'paypalform']]); ?>
                                                    <div class="content-inner-bottom">
                                                        <p class="text-uppercase"><b>authorize.net</b></p>
                                                        <label class="checkbox" style="margin-left: 21px;">
                                                            <?= $form->field($authorize_payment, 'pg_status')->checkBox(['label' =>'Enable authorize.net','uncheck' => 0, 'check' => 1])->label(false) ?>   <span class="savedMess" style="display:none;">saved</span>
                                                        </label>
                                                        <?= $form->field($paypal_payment, 'gateway_type')->hiddenInput(['value'=>'authorize'])->label(false); ?>
                                                        <div class="row">
                                                            <div class="col-lg-2"><p>API Login</p></div>
                                                            <div class="col-lg-10">
                                                                <div class="form-group">
                                                                    <?= $form->field($authorize_payment, 'api_login')->textInput(['maxlength' => true])->label(false) ?>

                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-2"><p>Transaction Key</p></div>
                                                            <div class="col-lg-10">
                                                                <div class="form-group">
                                                                    <?= $form->field($authorize_payment, 'transaction_key')->textInput(['maxlength' => true])->label(false) ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-2"><p>MD5-Hash Value</p></div>
                                                            <div class="col-lg-10">
                                                                <div class="form-group">
                                                                    <?= $form->field($authorize_payment, 'hash_value')->textInput(['maxlength' => true])->label(false) ?>
                                                                </div>
                                                                <span class="savedMess" style="display:none;">saved</span>
                                                                <?= Html::submitButton($authorize_payment->isNewRecord ? 'Save' : 'Save', ['class' => $authorize_payment->isNewRecord ? 'btn btn-main pull-right' : 'btn btn-main pull-right']) ?>
                                                            </div>
                                                        </div><hr>
                                                        <p><small><b>Enable</b> Check this box to accept payments through your <b>Paypal</b> account.</small></p>
                                                        <p><small><b>API Login: </b> Login to your <b>Authorize.NET</b> account.</small></p>
                                                        <p><small>Click on settings from the left panel. Under security settings find API Login and Transaction key. Click to generate your transaction key. </small></p>
                                                        <p><small><b>MD5_Hash Value: </b> This is your secret key. Login to your Authorize.net account. Click on settings from the left panel. Under security settings find MD5 Hash. Enter new hash value. Enter that same value here e.g. you can set Boocked as the secret key on authorize.net and enter the same value here. </small></p>
                                                        <p><small><b>Response/Receipt URLs: </b> Login to your Authorize.net account. Click on Settings from the left panel. Select "Response/Receipts URLs". Add new URL and enter your unique Boocked</small> <small class="txt-theme">URL http://User.boocked.com/</small></p>
                                                    </div>
                                                    <?php ActiveForm::end(); ?>
                                                    <?php Pjax::end(); ?>

                                                </div>
                                            </div>
                                        </div>
                                        <div class="row btn-row" style="margin-top: 2pc; margin-bottom: 0px;">
                                            <div class="col-lg-12">
                                                <button class="btn btn-main pull-right" style="padding: 11px 36px 13px;">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab8">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step8-adv.png" class="img-responsive" style="margin-top: 0; margin-bottom: 22px; display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">tax</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <p><b>Add Tax</b> Taxes that will be applied to the cost of the service</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row btn-row" style="margin-top: 22px; margin-bottom: 73px;">
                                            <div class="col-lg-12">
                                                <button class="btn btn-main pull-right" style="padding: 11px 36px 13px;">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-customer-details">
            <div class="modal-dialog modal-dialog-customer">
                <div class="modal-content">
                    <div class="modal-header">
                        <ul class="list-inline list-modal-header">
                            <li class="text-uppercase"><i class="fa fa-user" style="color: #FC7600;"></i> customer</li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                                <a href=""><i class="fa fa-reply"></i></a>
                            </li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                                <a href=""><i class="fa fa-plus"></i></a>
                            </li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban" id="ban-btn-toggle"><i class="fa fa-ban"></i></a></li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay" id="pay-btn-toggle"><i class="fa fa-dollar"></i></a></li>
                            <li class="pull-right" style="font-size: 12px;">Gift Certificates / Discount Coupons</li>
                        </ul>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body-inner-contents">
                            <div class="row">
                                <div class="col-lg-7 col-md-7 col-sm-7">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <i class="fa fa-plus upload-image" data-toggle="tooltip" data-placement="top" title="Upload Image"></i>
                                            <img src="<?= Yii::getAlias('@web') ?>/img/staff2.png">
                                        </div>
                                        <div class="col-lg-8">
                                            <h4 class="txt-theme" style="position: relative; top: 9px;">John Doe</h4>
                                            <p><small>Johndoe@gmail.com</small></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <ul class="list-inline" style="margin-top: 15px;">
                                                <li><small><a href="" class="txt-theme">Invite to schedule online</a></small></li><li class="txt-theme"><small>|</small></li>
                                                <li><small><a href="" class="txt-theme">Verify</a></small></li><li class="txt-theme"><small>|</small></li>
                                                <li><small><a href="" class="txt-theme">Edit</a></small></li><li class="txt-theme"><small>|</small></li>
                                                <li><small><a href="" class="txt-theme">Delete</a></small></li>
                                            </ul>
                                            <button class="btn btn-main" style="padding: 4px 9px;"><i class="fa fa-plus"></i> Add Tags</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 media-col6">
                                    <div class="row">
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                            <i class="fa fa-map-marker txt-theme"></i>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                            <p><small>145 strait street, CT.No City, No Region United States(01234)</small></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                            <i class="fa fa-mobile txt-theme"></i>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                            <p><small>098-879-46548</small></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12" style="margin-top: 8px;"><button class="btn btn-main" style="padding: 3px 4px;"><small>Send Gift Certificates</small></button></div>
                                        <div class="col-lg-12" style="margin-top: 5px;"><button class="btn btn-main" style="padding: 3px 4px;"><small>Send Discount Coupons</small></button></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px;">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="2" placeholder="Add Information"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="modal-body-inner-tabs">
                                <ul class="nav nav-tabs nav-main nav-customer-details">
                                    <li class="active">
                                        <a href="#up-app" data-toggle="tab">Upcoming Appointments</a>
                                    </li>
                                    <li>
                                        <a href="#past-app" data-toggle="tab">Past Appointments</a>
                                    </li>
                                    <li>
                                        <a href="#payments" data-toggle="tab">Payments</a>
                                    </li>
                                    <li>
                                        <a href="#promotion" data-toggle="tab">Promotion</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="up-app">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="past-app">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="payments">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="promotion">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="ban">
            <div class="modal-dialog modal-dialog-customer">
                <div class="modal-content">
                    <div class="modal-header">
                        <ul class="list-inline list-modal-header">
                            <li class="text-uppercase"><i class="fa fa-user" style="color: #FC7600;"></i> customer booking restrictions</li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                                <a href=""><i class="fa fa-reply"></i></a>
                            </li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                                <a href=""><i class="fa fa-plus"></i></a>
                            </li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban"><i class="fa fa-ban"></i></a></li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay"><i class="fa fa-dollar"></i></a></li>
                        </ul>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body-inner-contents">
                            <div class="alert alert-warning">
                                <i class="fa fa-warning"></i> <small>Client Advance setting is a premium feature available with PRO and above packages. <a href="#" class="txt-theme">Upgrade your account </a> or <a href="" class="txt-theme">Compare various Packages</a></small>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>This Customer is</p>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option selected>Post-Paying</option>
                                            <option>Post-Paying</option>
                                            <option>Post-Paying</option>
                                        </select>
                                    </div>
                                    <p class="text-muted">
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                        </small>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>Future Bookings Status</p>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option selected>As per settings</option>
                                            <option>Post-Paying</option>
                                            <option>Post-Paying</option>
                                        </select>
                                    </div>
                                    <p class="text-muted">
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                        </small>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>With Restriction</p>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option selected>As per settings</option>
                                            <option>Post-Paying</option>
                                            <option>Post-Paying</option>
                                        </select>
                                    </div>
                                    <p class="text-muted">
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                        </small>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="list-inline pull-right">
                                        <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;">Cancel</button></li>
                                        <li><button class="btn btn-main" style="padding-right: 19px; padding-left: 19px;">Save</button></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="pay">
            <div class="modal-dialog modal-dialog-customer">
                <div class="modal-content">
                    <div class="modal-header">
                        <ul class="list-inline list-modal-header">
                            <li class="text-uppercase"><i class="fa fa-dollar" style="color: #FC7600;"></i> add membership payment</li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                                <a href=""><i class="fa fa-reply"></i></a>
                            </li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                                <a href=""><i class="fa fa-plus"></i></a>
                            </li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban"><i class="fa fa-ban"></i></a></li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay"><i class="fa fa-dollar"></i></a></li>
                        </ul>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <p>Payment Date</p>
                                <div class="form-group">
                                    <input type="text" placeholder="4-17-2016" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <p>Payment For</p>
                                <div class="form-group">
                                    <input type="text" placeholder="Add New Payment" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="text-muted pull-right" style="margin-top: -9px;"><small>Customer restrictions of the selected membership will be applied automatically.</small></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <textarea class="form-control" rows="2" placeholder="Note against payment"></textarea>
                                    <p class="text-danger pull-right"><small>Please do not enter html tags</small></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="modal-body-inner-tabs">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <p class="txt-theme">Amount</p>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Additional Charges">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <span style="position: absolute; left: -2px;">$</span> <input type="text" class="form-control" placeholder="$$$">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Discount">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <span style="position: absolute; left: -6px;">by</span>
                                            <select class="form-control" placeholder="Cash">
                                                <option selected="">Cash</option>
                                                <option>d</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Total">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <ul class="list-inline pull-right">
                                            <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;">Cancel</button></li>
                                            <li><button class="btn btn-main" style="padding-right: 19px; padding-left: 19px;">Save</button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>

































<input type="hidden" id="booking_csrf" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />





































<?php
$script = "
$('body').on('beforeSubmit', 'form#resourcecommonform', function () {
var form = $(this);

if (form.find('.has-error').length) {
return false;
}
$.ajax({
url: form.attr('action'),
type: 'post',
data: form.serialize(),
success: function (response) {
    if(response==1){
        alert('changed');
         $.pjax.reload({container:'#domainnamesection'});

    }else {
        alert('not updated');

    }
}
});
return false
});
$('body').on('beforeSubmit', 'form.paypalform', function () {
var form = $(this);
var savedmess = $(this).find('.savedMess');

if (form.find('.has-error').length) {
return false;
}
$.ajax({
url: form.attr('action'),
type: 'post',
data: form.serialize(),
success: function (response) {
    if(response==1){
       // alert('changed');
       savedmess.css('display','inline');
     setTimeout(function(){
 
    savedmess.hide('slow');
},2000);
    }else {
        alert('not updated');

    }
}
});
return false;
});
$('.booking_rules_checkbox').change(function(){
   var chkbox_name =  $(this).attr('name');
   var savedmess = $(this).siblings('.savedMess');
   
if($(this).is(':checked')){
  var chkbox_val = 1;
  }else {
    var chkbox_val = 0;
  }
   var csrf_name = $('booking_csrf').attr('name');
   var csrf_val = $('booking_csrf').val();
$.ajax({
url: 'updaterulessetting',
type: 'post',
data:{csrf_name:csrf_val,
'SettingBookingRules[bkrules_name]':chkbox_name,
'SettingBookingRules[bkrules_value]':chkbox_val},
success: function (response) {
    if(response==1){
     savedmess.css('display','inline');
     setTimeout(function(){
 
    savedmess.hide('slow');
}, 2000);

    }else {
        alert('not updated');

    }
}
});

});
$('.bkr_selectbox').change(function(){

   var optionval =  $('option:selected',this).val();
   var selectbox_name =  $(this).attr('name');
   alert(optionval);
   var csrf_name = $('booking_csrf').attr('name');
   var csrf_val = $('booking_csrf').val();
$.ajax({
url: 'updaterulessetting',
type: 'post',
data:{csrf_name:csrf_val,
'SettingBookingRules[bkrules_name]':selectbox_name,
'SettingBookingRules[bkrules_value]':optionval},
success: function (response) {
    if(response==1){
      // $('#business_informationform .alert').show(0).delay(2000).hide(0);
alert('updated');
    }else {
        alert('not updated');

    }
}
});

});


";

$this->registerJs($script);
?>
<script>
function setTimezoneSetting(currentEle){
    var itemvalue = currentEle.value;
    var itemname = currentEle.name;
    var curformaction = $('#timezoneform').attr('action');
    $.ajax({
        url: curformaction,
        type: 'post',
        data:{'item_name':itemname, 'item_value':itemvalue},
        success: function (response) {
            if(response==1){
                alert('Updated');

            }else {
                alert('not updated');

            }
        }
    });



 //'var abc = ad.attr('id');
  //  alert(abc);
}
function trash_tracking_domain(event,xor){
    event.preventDefault();
    var link_href = $(xor).attr('href');
    $.ajax({
        url: 'trash_tracking',
        type: 'post',
        data:{'domain':link_href},
        success: function (response) {
            if(response==1){
     $.pjax.reload({container:'#domainnamesection'});

            }else {
                alert('not updated');

            }
        }
    });
}
</script>
