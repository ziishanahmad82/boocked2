<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ThirdPartyUsers */

$this->title = 'Create Third Party Users';
$this->params['breadcrumbs'][] = ['label' => 'Third Party Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="third-party-users-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
