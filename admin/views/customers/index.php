<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use app\models\Customers;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    #demo12 li.active{
        background: ;

    }

</style>
<?php
$query = Customers::find()->joinWith(['appointments']);
$activecustomers= $query->select(['*'])
    ->andFilterWhere(['<', 'appointments.appointment_date',  date('Y-m-d')  ]) //-30
    ->andFilterWhere(['=', 'customers.business_id', Yii::$app->user->identity->business_id ])
    ->groupBy(['customers.customer_id'])
    ->all();
$all_customers = Customers::find()->where(['business_id'=>Yii::$app->user->identity->business_id])->all();
$all_customers = count($all_customers);

$activecustomers = count($activecustomers);
$inactivecustomers = $all_customers-$activecustomers;
?>
    <div class="row">
       <div class="col-lg-3 col-md-3 col-sm-3 no-padding-l media-xs-full">
     <?php
     echo Highcharts::widget([

         'scripts' => [
        //     'highcharts-3d',
        //     'modules/exporting',
       //      'themes/sand-signika',
         ],
         'options' => [
          //   'credits' => ['enabled' => false],
             'chart' => ['type' => 'pie','height'=>350,'plotBackgroundColor'=> null,'plotShadow'=>false,
                /* 'options3d' => ['enabled' => true,
                     'alpha' => 55, //adjust for tilt
                     'beta' => 0,
                     'depth' => 50,// adjust for turn
                 ]  */
             ],
             'plotOptions' => [ // it is important here is code for change depth and use pie as donut
                 'pie' => [
                     'allowPointSelect' => true,
                     'cursor' => 'pointer',
                     'innerSize' => 0,
                      'depth' => 30
                 ]
             ],
             'title' => ['text' => 'Customer Report'],
             'series' => [[// mind the [[ instead of [
                 'type' => 'pie',
                 'name' => 'customer',
                 'colors'=>['yellow','#00F4AE'],
                 'data' => [
                     ['Active', 25],
                        ['Inactive', 75],
                  //   ['Active', $activecustomers],
                  //   ['Inactive', $inactivecustomers],
                 ],
             ]], //mind the ]] instead of ]
         ]
     ]);
     ?>
           <?php /*       echo Highcharts::widget([
               'options' => [
                   'title' => ['text' => 'Active/Inactive Customers'],
                   'options3d'=>['enabled'=>true,
                       'alpha'=>100,  //adjust for tilt
                       'beta'=>100,  // adjust for turn
                       'depth'=>100,
                   ],
                   'plotOptions' => [
                       'pie' => [
                           'cursor' => 'pointer',
                       ],
                   ],
                   'series' => [
                       [ // new opening bracket
                           'type' => 'pie',
                           'name' => 'Customers',
                           'data' => [
                               ['January', 30.0],
                               ['Febueary', 2.8],
                               ['March', 8.5],
                               ['April', 0.2],
                               ['May', 0],
                               ['June', 0],
                               ['July', 0],
                               ['August', 0],
                               ['Sep', 0],
                               ['October', 0],
                               ['November', 7],
                                ['December', 0],

                           ],
                       ] // new closing bracket
                   ],
               ],
           ]); */ ?>

            <div class="panel panel-default" style="margin-top:25px;">
                <div class="panel-heading">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/staff-icon.png"> <span class="staff-head">Staff</span></h5></li>
                    </ul>
                </div>
                <div class="panel-body staff-div">
                    <div class="row">
                        <?php
                        $i=1;
                        foreach($staffList as $staff){ ?>

                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5 staffdp_cont">
                            <a href="#" class="dashboard_staff_list" data-staff="<?= $staff->id ?>">
                                <?php if($staff->user_profile_image!=""){ ?>
                                    <img src="<?= Yii::getAlias('@web').'/'.$staff->user_profile_image ?>" alt="<?= $staff->username ?>">
                                <?php }else { ?>
                                    <img src="<?= Yii::getAlias('@web')?>/img/staff12.png">
                                <?php } ?>
                                <p><small class="title-sm"><?= $staff->username ?>  </small></p>
                            </a>
                        </div>
                        <?php
                        if($i==3){ ?>
                    </div>
                </div>
                <div class="col-sm-12">
                    <div class="row">
                        <?php $i=0; }  ?>
                        <?php
                        $i++;
                        } ?>
                    </div>
                </div>
            </div>

           <!-- <div class="panel panel-default">
                <div class="panel-heading">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/analytics.png"> <span class="staff-head">analytics</span></h5></li>
                        <li class="pull-right li-right"><i class="fa fa-plus plus-square"></i></li>
                    </ul>
                </div>
                <div class="panel-body section-analytics">
                    <div class="row">
                        <div class="div-analytics">
                            <canvas id="canvas" height="450" width="600"></canvas>
                        </div>
                    </div>
                </div>
            </div>-->

        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 div-relative pull-right">
            <ul class="list-inline pull-right">
                <li><a href="#new_customer" data-toggle="tooltip" data-original-title="Add new Customer" data-placement="left" id="add_new_customer" ><i class="fa fa-plus fa-add-services"></i></a></li>
              <!--  <li><a href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="Help"><i class="fa fa-question fa-question-mark"></i></a></li>-->
            </ul>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9 media-xs-full">
            <ul class="nav nav-tabs nav-main alphabetstabs">
                <li>
                    <a href="#" onclick="searchalphabets(event,this)" data-toggle="tab">All</a>
                </li>
                <li class="active">
                    <a href="#" onclick="searchalphabets(event,this)" data-toggle="tab">A</a>
                </li>
                <li>
                    <a href="#" onclick="searchalphabets(event,this)" data-toggle="tab">B</a>
                </li>
                <li>
                    <a href="#" onclick="searchalphabets(event,this)" data-toggle="tab" >C</a>
                </li>
                <li>
                    <a href="#" onclick="searchalphabets(event,this)" data-toggle="tab" >D</a>
                </li>
                <li>
                    <a href="#" onclick="searchalphabets(event,this)" data-toggle="tab">E</a>
                </li>
                <li>
                    <a href="#" onclick="searchalphabets(event,this)" data-toggle="tab" >F</a>
                </li>
                <li>
                    <a href="#" onclick="searchalphabets(event,this)" data-toggle="tab">G</a>
                </li>
                <li>
                    <a href="#" onclick="searchalphabets(event,this)" data-toggle="tab">H</a>
                </li>

                <li class="caret-dropdown">
                     <a href="#"  class="dropdown-toggle" data-target="#demo12" data-toggle="collapse" ><i class="fa fa-caret-down"></i></a>
                    <ul class="dropdown-menu" id="demo12" style="background: #fff; margin-left: 40px;max-height: 188px;overflow-y: scroll;">
                       <?php
                       foreach (range('I', 'Z') as $char) {
                        ?>   <li><a href="#" onclick="searchalphabets(event,this)"  data-toggle="tab" class="dropdownlistcustomer" ><?= $char ?></a></li>

                      <?php  } ?>





                    </ul>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade in active panel" id="first" style="border-top:none">
                    <div class="panel-collapse collapse in active">
                        <div class="panel-body panel-main">
                            <div class="row row-custom">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body panel-no-padding" style="border-bottom-right-radius: 0; border-bottom-left-radius: 0;">
                                            <div class="scroller-tab">
                                                <div id="customerlistload">
                                                    <?php Pjax::begin(['id' => 'customer_list_reload']); ?>
                                                <table class="table table-responsive table-striped" >
                                                    <tr class="table-panel-head row-panel-head">
                                                       <!-- <th></th>-->
                                                        <th><input type='checkbox'></th>
                                                        <th>Customer Name</th>
                                                        <th>Status</th>
                                                        <th>Action Items</th>
                                                        <th>
                                                            <div class="form-group search-table-main-div">
                                                                <input type="text" class="form-control search-table" placeholder="Search" onchange="searchalphabets(event,this)">
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    <tbody id="sortable">
                                                     <?php if(!empty($CustomerList)) { ?>
                                                         <?php foreach ($CustomerList as $customers) { ?>
                                                             <tr id="<?= $customers->customer_id ?>">
                                                                <!-- <td style="cursor:pointer;"><span></span>1</td>-->
                                                                 <td><input type='checkbox'></td>
                                                                 <td><?= $customers->first_name ?></td>
                                                                 <td class="td-status"><i class="fa fa-smile-o"></i>
                                                                 </td>
                                                                 <td>
                                                                     <ul class="list-inline list-action">
                                                                         <li onclick="customer_detailspopup(<?= $customers->customer_id ?>)">
                                                                    <span>
                                                                                <i class="fa fa-exclamation-circle" data-placement="bottom" title="Details" data-toggle="tooltip"></i>
                                                                            </span>
                                                                             <!--  <span data-toggle="modal" data-target="#modal-customer-details">
                                                                                <i class="fa fa-exclamation-circle" data-placement='top' title="Details" data-toggle='tooltip' onclick="customer_detailspopup(<?= $customers->customer_id ?>)"></i>
                                                                            </span>-->
                                                                         </li>
                                                                         <li><i class="fa fa-trash" data-placement="bottom" title="Delete" data-toggle='tooltip' onclick="customer_delete(event,this,<?= $customers->customer_id ?>)"></i>
                                                                         </li>
                                                                         <li><i class="fa fa-crosshairs" data-placement="right" title="Move" data-toggle="tooltip"></i></li>
                                                                         <li><i class="send_customrt_email fa fa-envelope" data-placement='left' title="Send Msg" data-custemail="<?= $customers->email ?>" data-toggle='tooltip'></i></li>
                                                                     </ul>
                                                                 </td>
                                                                 <td></td>
                                                             </tr>
                                                         <?php }
                                                     }else { ?>
                                                         <tr>
                                                             <td></td>
                                                             <td></td>
                                                             <td colspan="3"> No customer For this Criteria  </td>
                                                             <td></td>
                                                         </tr>

                                                     <?php }?>
                                                    </tbody>

                                                </table>
                                                    <?php Pjax::end(); ?>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                          <!--  <div class="row">
                                <div class="col-lg-12 text-center">
                                    <ol class="pagination">
                                        <li><a href="#" class="go-prev"><i class="fa fa-chevron-left"></i></a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">5</a></li>
                                        <li><a href="#" class="go-next"><i class="fa fa-chevron-right"></i></a></li>
                                    </ol>
                                </div>
                            </div> -->
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>


<div class="modal fade" id="update_customer">
    <div class="modal-dialog modal-dialog-customer">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="list-inline list-modal-header">
                    <li class="text-uppercase"><i class="fa fa-user" style="color: #FC7600;"></i> New Customer</li>

                </ul>
            </div>
            <div class="modal-body">
                <div class="modal-body-inner-contents"></div>

            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="new_customer">
    <div class="modal-dialog modal-dialog-customer">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="list-inline list-modal-header">
                    <li class="text-uppercase"><i class="fa fa-user" style="color: #FC7600;"></i> New Customer</li>

                </ul>
            </div>
            <div class="modal-body">
                <div class="modal-body-inner-contents">

                    <?php $form = ActiveForm::begin(['id'=>'new_customerform','action'=>Yii::getAlias('@web').'/index.php/customers/create']); ?>


                    <div class="col-sm-6">
                        <?= $form->field($NewCustomer, 'first_name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($NewCustomer, 'last_name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-12">
                        <?= $form->field($NewCustomer, 'email')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($NewCustomer, 'cell_phone')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($NewCustomer, 'home_phone')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($NewCustomer, 'work_phone')->textInput(['maxlength' => true]) ?>
                    </div>              <div class="col-sm-12">
                        <?= $form->field($NewCustomer, 'customer_address')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($NewCustomer, 'customer_region')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($NewCustomer, 'customer_city')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($NewCustomer, 'customer_zip')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="list-inline pull-right">
                                <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;" onclick="hidemodel('#new_customer')">Cancel</button></li>
                                <li> <?= Html::submitButton($NewCustomer->isNewRecord ? 'Save' : 'Save', ['class' => $NewCustomer->isNewRecord ? 'btn btn-main' : 'btn btn-main']) ?></li>
                            </ul>
                        </div>
                    </div>


                    <?php ActiveForm::end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="send_customer_emailmodel">
    <div class="modal-dialog modal-dialog-customer">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="list-inline list-modal-header" style="margin-bottom:0">
                    <li class="text-uppercase"><i class="fa fa-user" style="color: #FC7600;"></i> Send Email</li>

                </ul>
            </div>
            <div class="modal-body">
                <div class="modal-body-inner-contents">

                    <?php $form = ActiveForm::begin(['id'=>'send_customerform','action'=>Yii::getAlias('@web').'/index.php/customers/sendcustomeremail']); ?>


                    <div class="col-sm-6" style="margin-bottom: 10px;">
                        <div class="form-group">
                            <label>To</label>
                            <input type="email" id="sendtoemail" name="to" class="form-control" required />
                        </div>
                    </div>
                    <div class="col-sm-6" style="margin-bottom: 10px;">
                        <div class="form-group">
                            <label>Subject</label>
                            <input type="text" name="subject" class="form-control" required />
                        </div>
                    </div>

                    <div class="col-sm-12" style="margin-bottom: 10px;">
                        <div class="form-group">
                            <label>Message</label>
                            <textarea name="message" class="form-control" required></textarea>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="list-inline pull-right">
                                <li><a class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;padding:5px 8px" onclick="hidemodel('#send_customer_emailmodel')">Cancel</a></li>
                                <li> <?= Html::submitButton($NewCustomer->isNewRecord ? 'Send' : 'Send', ['class' => $NewCustomer->isNewRecord ? 'btn btn-main' : 'btn btn-main']) ?></li>
                            </ul>
                        </div>
                    </div>


                    <?php ActiveForm::end(); ?>
                    <div class="alert alert-success" id="sentemail_scus" style="display:none;">
                        <strong>Sent!</strong> Your message sent Successfully.
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="gift_certificate_cont_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Modal title</h4>
            </div>
            <div class="modal-body">
                ...
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
    </div>
</div>


<?php $customer_Detailhead = '<ul class="list-inline list-modal-header">
    <li class="text-uppercase"><i class="fa fa-user" style="color: #FC7600;"></i> customer</li>
    <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Customer">
        <a href="#custmer_det" data-toggle="tab" ><i class="fa fa-reply"></i></a>
    </li>
    <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
        <a href="#" onclick="detailaddnewcustomer()"><i class="fa fa-plus"></i></a>
    </li>
    <li class="pull-right"><a href="#ban1" data-toggle="tab" id="ban-btn-toggle"><i class="fa fa-ban"></i></a></li>
    <li class="pull-right"><a href="#pay1" data-toggle="tab" id="ban-btn-toggle"><i class="fa fa-dollar"></i></a></li>
    <li class="pull-right" style="font-size: 12px;">Gift Certificates / Discount Coupons</li>
</ul>'; ?>
<?php
Modal::begin([
 'header'=> $customer_Detailhead,
'id' => 'customer_details',

//'htmlOptions' => ['style' => 'width:800px;']
]);
//'size' => 'modal-lg',
echo "<div id='modaleContent'></div>";
Modal::end();
$script ="
$('#add_new_customer').click(function(event){
event.preventDefault();
$('#new_customer').modal('show');
});

 
$('body').on('beforeSubmit', '#new_customerform', function () {
 var form = $(this);
$.ajax({
    url:form.attr('action'),
    type: 'post',
    data: form.serialize(),
    success: function (response) {
    if(response==1){
    
    $('#new_customer').modal('hide');
  $.pjax.reload({container: '#customer_list_reload',timeout:60000});

    }
    }
    }); 
 return false;
 
 });

$('.dropdownlistcustomer').click(function(){

$('.dropdownlistcustomer').not(this).parent('li').removeClass('active');
});

$(document).on('click', '#add_tag_button', function(event) { 
      addtag_texarea = $('#addtag_texarea').val();
      cust_d = $(this).attr('data-customer');
     $.ajax({
            url: '".Yii::getAlias('@web')."/index.php/customers/addtag',
            type: 'post',
            data: {'cust_d': cust_d,'tag':addtag_texarea},
            success: function (response) {
                if (response==1) {
                $('#created_tag').html('<button class=\"btn btn-main\" style=\"padding: 4px 9px;\">'+addtag_texarea+'</button>');
                $('#popup_add_tags').show();
                $('#addtagbox').hide();
                 

                }
            }
        });
    
});

$(document).on('click', '#send_gift_certificate_button', function(event) {
        
       
     //   $('#gift_certificate_cont_modal').modal('show');
        $.ajax({
            url:'".Yii::getAlias('@web')."/index.php/customers/gift_certificates_for_customer',
            type: 'post',
           // data: form.serialize(),
            success: function (response) {
           
          
            $('#tab_gift_certificate_cont').html(response);
          
        
          
            }
            }); 
        
    }); 
    
 $(document).on('click', '.one_gift_certificate', function(event) {
        
       $('.one_gift_certificate').not(this).removeClass('selected');
       $(this).addClass('selected');
     
        
    });    
 $(document).on('click', '#sendcertificate_but', function(event) {
        
       gid = $('.one_gift_certificate.selected').attr('data-atrid');
       if(gid){
         cust_email = $('#customer_email_cert').text();
         cust_name  = $('#custmer_det h4.txt-theme').text();
            alert(cust_email);
            $('#pay4').load('".Yii::getAlias('@web')."/index.php/customers/sell_certificate/?id='+gid,function(){
            
            $('#soldgiftcertificates-to_name').val(cust_name);
            $('#soldgiftcertificates-to_email').val(cust_email);
          $('#pay3').removeClass('active');
            $('#pay4').addClass('active');
          });
        
          
           
         
        }else {
        alert('Please select Certificate')
        }
     
        
    });   
 
    $('body').on('beforeSubmit', '#sells_giftcertificate_form', function () {
     var form = $(this);
    $.ajax({
    url: form.attr('action'),
    type: 'post',
    data: form.serialize(),
    success: function (response) {
    if(response==1){
    
  

    }else {
    console.log(response);

    }
    }
    });
    return false;
    });
$('body').on('beforeSubmit', '#membership_payment', function () {
 
 
 var form = $(this);
$.ajax({
    url:'".Yii::getAlias('@web')."/index.php/customers/addmembershippayment',
    type: 'post',
    data: form.serialize(),
    success: function (response) {
    if(response==1){
    $('#ban-btn-toggle1').click();
  

    }
    }
    }); 
 return false;
 
 });



   
    
    $('#sortable').sortable({
     handle: '.fa.fa-crosshairs',
			
			  update : function () 
			  { 
	              //  alert('aaaaaaaaaaaaaaaaaaaac');
	                
					var order = $(this).sortable('toArray');
				    console.log(order);
					jQuery(document).load('".Yii::getAlias('@web')."/index.php/customers/customer_sort?ids='+order); 
			//console.log(order);
			
			  }
		}); 

//$('.staffimageform').on('submit',(function(e) {
$('body').on('submit', '.staffimageform', function () {
//	alert('sdsadas');
//	return false;
//	exit();
     //   e.preventDefault();
        
        var formData = new FormData(this);
        var formid = $(this).attr('id');

        $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
               if(data!= '0'){
               alert('dfasf');
               $('#'+formid).siblings('img').attr('src','".Yii::getAlias('@web')."/'+data)
               }
            },
            error: function(data){
                console.log('error');
                console.log(data);
            }
        });
        
        return false;
        
    });
		
	$('body').on('change', '.staffimagelog', function () {	
//$('.staffimagelog').on('change', function() {
   alert('change');
        $(this).parent('form').submit();
        
    });	
    $('.send_customrt_email').click(function(event){
        event.preventDefault();
         custemail  = $(this).attr('data-custemail');
        $('#sendtoemail').val(custemail);
        $('#send_customer_emailmodel').modal('show');
       
    });	
    
    $('body').on('beforeSubmit', '#send_customerform', function () {
   
      var form = $(this);
        $.ajax({
            url:form.attr('action'),
            type: 'post',
            data: form.serialize(),
            success: function (response) {
            if(response==1){
            
            $('#send_customerform').trigger('reset');
            $('#sentemail_scus').show();
             $('#sentemail_scus').delay(2000).fadeOut();
            }
            }
            }); 
        return false;
      
    });

";
$this->registerJs($script);
?>
<style>
    .fa.fa-plus.upload-image{
        cursor:pointer;
    }
</style>
<script>

    function changeProfile(event,xor){
        $('.staffimagelog').click();

    }
    function searchalphabets(event,xor) {

        if($(xor).is("input")){
            var searchtext = $(xor).val();

        }else {
            var searchtext = $(xor).text();
            event.preventDefault();
        }



        $.ajax({
            url: '<?= Yii::getAlias('@web') ?>/index.php/customers/searchcustomer',
            type: 'post',
            data: {'searchtxt': searchtext},
            success: function (response) {
                if (response) {
                    $('#customerlistload').html(response);

                }
            }
        });

    }

   function customer_detailspopup(csr_id){

       $.ajax({
           url: '<?= Yii::getAlias('@web') ?>/index.php/customers/customerdetails',
           type: 'post',
           data: {'csr_id':csr_id},
           success: function (response) {
               if(response){
                   $('#customer_details').modal('show').find('#modaleContent').html(response);

               }else {
                   alert('not updated');

               }
           }
       });

       
   }
   function hidemodel(model){
       $(model).modal('hide');
   }
   function verify_customer(event,xor,cstr_id){

       event.preventDefault();
       $.ajax({
           url: '<?= Yii::getAlias('@web') ?>/index.php/customers/verify_customer',
           type: 'post',
           data: {'cstr_id':cstr_id},
           success: function (response) {
               if(response){
                   $(xor).replaceWith('<span>Verified</span>')
                 //  $('#customer_details').modal('show').find('#modaleContent').html(response);

               }else {
                   alert('not updated');

               }
           }
       });


   }
    function customer_delete(event,xor,cstr_id){
        event.preventDefault();
        if(confirm("Are you sure you want to delete it?")) {
            $.ajax({
                url: '<?= Yii::getAlias('@web') ?>/index.php/customers/delete_customer',
                type: 'post',
                data: {'cstr_id': cstr_id},
                success: function (response) {
                    if (response) {
                        $('#customer_details').modal('hide');
                        $.pjax.reload({container: '#customer_list_reload',timeout:60000});
                        //  $('#customer_details').modal('show').find('#modaleContent').html(response);

                    } else {


                    }
                }
            });
        }

    }
    function detailaddnewcustomer(){
        $('#customer_details').modal('hide');
        $('#new_customer').modal('show');

    }
    function customer_informationsave(cstr_id,xor){
       var cstr_inf = $(xor).val();

        $.ajax({
            url: '<?= Yii::getAlias('@web')?>/index.php/customers/updateinformation',
            type: 'post',
            data: {'cstr_id':cstr_id,'cstr_inf':cstr_inf},
            success: function (response) {
                if(response==1){
                    alert('saved');


                }
            }
        });



    }
    function editcustomer(event,xor){
        event.preventDefault();
        $('#custmer_det').removeClass('active');
        $('#pay2').addClass('active');
        //$('#update_customer').modal('show').find('.modal-body').load(update_action);
      //  $('#update_customer .modal-body').css('overflow-y', 'auto');
      //  $('#update_customer .modal-body').css('max-height', $(window).height() * 0.7);



    }
    function popup_add_tags(id){
        $('#popup_add_tags').hide();
        $('#addtagbox').show();

    }
    function canceltags(event){
        event.preventDefault();
        $('#addtagbox').hide();
        $('#popup_add_tags').show();

    }
    function invitation_email(id,event){
        event.preventDefault();
        alert('email will send');


    }
</script>
<style>
    #new_customer label, #new_customer .help-block{
        font-size:12px;

    }
    #new_customer .form-group{
        margin-bottom:2px;;

    }

</style>
<?php $this->registerJsFile('https://code.jquery.com/ui/1.12.1/jquery-ui.js', ['depends' => [yii\web\JqueryAsset::className()]]); ?>
<script>




</script>
