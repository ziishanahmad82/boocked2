<?php

/* @var $this yii\web\View */
$this->title = 'Add new Customer';
use app\models\CustomerReservedTimeslots;
use app\models\Customers;
use app\models\Locations;
use app\models\ThirdPartyUsers;
use app\models\Toughbooks;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use app\models\Countries;
use app\models\States;
use app\models\Cities;
use miloschuman\highcharts\Highcharts;

$countries = Countries::find()->all();

?>
<style>
    .container {
        max-width: 100%;

    }

</style>
<?php


?>
    <div class="site-index">


        <div class="body-content">
            <div id="errorrr"></div>

                <div class="col-lg-3 col-md-3 col-sm-3 no-padding-l media-xs-full">

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <ul class="list-inline list-staff">
                                <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/staff-icon.png"> <span class="staff-head">New Users</span></h5></li>
                                <li class="pull-right li-right"></li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <?php       echo Highcharts::widget([

                                    //  'scripts' => [
                                    //       'highcharts-3d',
                                    //   ],
                                    'options' => [
                                        'chart' => ['type' => 'spline', 'height'=>250
                                            /* 'options3d'=>['enabled'=>true,
                                                           'alpha'=>12,  //adjust for tilt
                                                           'beta'=>14,  // adjust for turn
                                                           'depth'=>70,
                                                           ]  */
                                        ],
                                        'xAxis' => [
                                            'categories' => [date("M", strtotime("-5 months")),date("M", strtotime("-4 months")),date("M", strtotime("-3 months")),date("M", strtotime("-2 months")),date("M", strtotime("-1 months")),date("M")],
                                        ],
                                        /* 'plotOptions'=>[
                                            'series'=> [
                                                'borderWidth'=>0,
                                                // 'color'=>'rgb(0,220,158)',
                                                'color'=>'#fc7600',
                                                'pointWidth'=>27,


                                            ]
                                        ], */
                                        'plotLines'=>[[
                                            'value'=>0,
                                            'width'=>3,
                                            'color'=> 'orange'
                                        ]],

                                        'title' => ['text' => ''],

                                        'series' => [[
                                            //   'type'=>'column',
                                            'name'=>'Users',
                                            //  'data'=>[$six_month_users,$five_month_users, $four_month_users,$third_month_users,$second_month_users,$first_month_users],
                                            'data'=>[2,5,3,4,5,10],
                                            'color'=> 'orange',
                                            //   'lineWidth'=>'2'
                                        ],
                                            [
                                                //   'type'=>'column',
                                                'name'=>'Users',
                                                //  'data'=>[$six_month_users,$five_month_users, $four_month_users,$third_month_users,$second_month_users,$first_month_users],
                                                'data'=>[9,8,12,8,5,9],
                                                'color'=> 'red',
                                                //   'lineWidth'=>'2'
                                            ],

                                            /*   [
                                                   'type'=>'column',
                                                   'name'=>'Sales',
                                                   'data'=>[intval($six_month),intval($five_month),intval($four_month),intval($third_month),intval($second_month),intval($first_month)],
                                               ] */
                                        ],

                                    ]
                                ]);
                                ?>


                            </div>
                        </div>
                    </div>



                </div>

          <div class="col-sm-9" >
              <div class="panel panel-default">

                  <div class="panel-heading full">
                      <ul class="list-inline list-staff">
                          <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/coupons.png"><span class="staff-head">Add Customer</span></h5></li>
                      </ul>
                  </div>
                  <div class="panel-body panel-main reports_filter_panel" style="border-top: 1px solid #c4c4c4;">
                      <div class="col-sm-12">
                      <div class="row row-custom">
                      <?php Pjax::begin(['id' => 'customer_list_reload']); ?>
                      <?php $form = ActiveForm::begin(['id'=>'new_customerform','action'=>Yii::getAlias('@web').'/index.php/customers/create']); ?>

                      <div class="row">
                          <div class="col-lg-6 mid">
                              <?= $form->field($model, 'first_name')->textInput(['placeholder' => "First Name"])->label(false) ?>
                          </div>
                          <!--<div class="form-group col-lg-6 last field-signupform-first_name required">
                              <input type="text" name="SignupForm[first_name]" class="form-control" id="signupform-first_name">
                              <p class="help-block"></p>
                          </div>-->
                          <div class="col-lg-6 last">
                              <?= $form->field($model, 'last_name')->textInput(['placeholder' => "Last Name"])->label(false) ?>
                          </div>
                          <!-- <div class="last field-signupform-last_name required">
                              <label for="signupform-last_name" class="control-label"></label>
                              <input type="text" name="SignupForm[last_name]" class="form-control" id="signupform-last_name">

                              <div class="help-block"></div>
                          </div>-->
                          <? //= $form->field($model, 'last_name')?>
                          <!-- <div class="form-group col-lg-6 last">
                               <input type="text" class="form-control" placeholder="Last Name">
                           </div>-->
                      </div>
                      <? //= $form->field($model, 'first_name') ?>
                          <div class="row">
                              <div class="col-lg-6 mid">
                                <?= $form->field($model, 'email')->input('email',['placeholder' => "Email"])->label(false) ?>
                              </div>
                              <div class="col-lg-6 last">
                               <?= $form->field($model, 'customer_address')->textInput(['placeholder'=>'Postal Address'])->label(false) ?>
                               </div>
                           </div>
                      <div class="row">
                          <div class="form-group col-lg-6 mid">
                              <select class="form-control" id="businessinformation-country" name="SignupForm[customer_country]" onchange="buisness_country(this)">
                                  <option value="">Select Country</option>
                                  <?php  foreach($countries as $country){ ?>
                                      <option <?php if($business_information->country==$country->id){ echo 'selected'; }?> value="<?= $country->id ?>">
                                          <?= $country->name ?>
                                      </option>
                                  <?php }  ?>
                              </select>
                              <!--   <input type="text" class="form-control" placeholder="State">-->
                          </div>
                          <div class="form-group col-lg-6 last">
                              <select class="form-control" id="businessinformation-state" name="SignupForm[customer_region]">
                                  <option value="">Select States</option>

                              </select>
                          </div>
                      </div>
                      <div class="row">
                          <div class="form-group col-lg-6 mid">
                              <select class="form-control" id="businessinformation-city" name="SignupForm[customer_city]" >
                                  <option value="">Select City</option>
                              </select>
                          </div>
                          <div class="col-lg-6 last">
                              <?= $form->field($model, 'customer_zip')->textInput(['placeholder'=>'Zip Code'])->label(false) ?>
                              <!--  <input type="text" class="form-control" placeholder="Zip Code">-->
                          </div>
                      </div>
                      <div class="row">


                       <!--   <div class="form-group col-lg-6 mid">
                              < //= $form->field($model, 'password')->passwordInput(['placeholder'=>'Password'])->label(false) ?>
                          </div>
                          <div class="form-group col-lg-6 mid">
                              <? //= $form->field($model, 'password_repeat')->passwordInput(['placeholder'=>'Confirm Password'])->label(false) ?>
                          </div>-->
                      </div>
                      <!-- <div class="form-group">
                           <input type="text" class="form-control" placeholder="Card Number">
                       </div>
                       <ul class="list-inline list-payments">
                           <li>Payment Types</li>
                           <div class="list-inner">
                               <li class="radio pull-right cc"><input type="radio" data-toggle="modal" data-target="#card-options" class="radio-payment"> <small>Credit Card</small></li>
                               <li class="radio pull-right"><input type="radio" data-toggle="modal" data-target="#paypal-options"> <small>Paypal</small></li>
                           </div>
                       </ul>
                       <div class="row">
                           <div class="col-lg-4 exp"><p>Expiration Date</p></div>
                           <div class="col-lg-8">
                               <div class="form-group">
                                   <input type="text" class="form-control" placeholder="mm/dd/yy">
                               </div>
                           </div>
                       </div>
                       <div class="form-group">
                           <input type="text" class="form-control" placeholder="CSV Code">
                       </div> -->
                      <div class="clearfix"></div>
                      <?= Html::submitButton('Submit', ['class' => 'btn btn-main pull-right', 'name' => 'signup-button']) ?>
                      <div class="clearfix"></div>
                          <?php ActiveForm::end(); ?>

                          <?php Pjax::end(); ?>
                          <div class="alert alert-success" style="margin-top:25px;display:none" id="sucess_message">
                              <strong>Success!</strong>Customer added Successfully.
                          </div>

                          </div>
                      </div>
                      </div>




                  </div>
              </div>
              
              



        </div>
    </div>

<style>
    fieldset{
        border:none;
        margin-bottom:0 !important;

    }

</style>

<?php
$script ="$('body').on('beforeSubmit', '#new_customerform', function () {
    var form = $(this);
    $.ajax({
    url:form.attr('action'),
    type: 'post',
    data: form.serialize(),
    success: function (response) {
    if(response==1){
    $('#sucess_message').show();
    $.pjax.reload({container: '#customer_list_reload'});
    setTimeout(function(){ $('#sucess_message').hide() }, 5000);
    
    }
    }
    });
    return false;
    
    });
    $('#businessinformation-country').change(function(){
  var country_id =  $('#businessinformation-country option:selected').val();
 $.ajax({
url:'".Yii::getAlias('@web')."/index.php/business-information/address_states',
type: 'post',
data: {'country_id':country_id,'type':'country'},
success: function (response) {
    if(response){
    //    alert('changed');
        $('#businessinformation-state').html(response);
        $('#businessinformation-city').html('');

    }else {
        alert('No State Available');

    }
}
});
 });
 
  $('#businessinformation-state').change(function(){
  var state_id =  $('#businessinformation-state option:selected').val();
 $.ajax({
url:'".Yii::getAlias('@web')."/index.php/business-information/address_states',
type: 'post',
data: {'state_id':state_id,'type':'state'},
success: function (response) {
    if(response){
    //    alert('changed');
        $('#businessinformation-city').html(response);

    }else {
        alert('No City Available');

    }
}
});
 });
    
    
    
    
    
    
    ";
$this->registerJs($script);