<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid section-main">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 no-padding-l media-xs-full">
            <div class="panel panel-default">
                <div class="panel-body section-calendar">
                    <div class="page">
                        <div style="width:100%; max-width:600px; display:inline-block;">
                            <div class="monthly" id="mycalendar"></div>
                        </div>
                    </div>
                    <ul class="list-inline list-wmy">
                        <li><a href="">Week</a></li>
                        <li><a href="">Month</a></li>
                        <li><a href="">Year</a></li>
                    </ul>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="images/staff-icon.png"> <span class="staff-head">Staff</span></h5></li>
                        <li class="pull-right li-right"><i class="fa fa-plus plus-square"></i></li>
                    </ul>
                </div>
                <div class="panel-body staff-div">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="images/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="images/staff2.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="images/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="images/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="images/staff2.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="images/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="images/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="images/staff2.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="images/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="images/analytics.png"> <span class="staff-head">analytics</span></h5></li>
                        <li class="pull-right li-right"><i class="fa fa-plus plus-square"></i></li>
                    </ul>
                </div>
                <div class="panel-body section-analytics">
                    <div class="row">
                        <div class="div-analytics">
                            <canvas id="canvas" height="450" width="600"></canvas>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 div-relative pull-right">
            <ul class="list-inline pull-right">
                <li><a href="#" data-toggle="tooltip" data-placement="left" data-original-title="Add Service"><i class="fa fa-plus fa-add-services"></i></a></li>
                <li><a href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="Help"><i class="fa fa-question fa-question-mark"></i></a></li>
            </ul>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9 media-xs-full">
            <ul class="nav nav-tabs nav-main">
                <li class="active">
                    <a href="#first" data-toggle="tab">A</a>
                </li>
                <li>
                    <a href="#second" data-toggle="tab">B</a>
                </li>
                <li>
                    <a href="#third" data-toggle="tab">C</a>
                </li>
                <li>
                    <a href="#forth" data-toggle="tab">D</a>
                </li>
                <li>
                    <a href="#fifth" data-toggle="tab">E</a>
                </li>
                <li>
                    <a href="#sixth" data-toggle="tab">F</a>
                </li>
                <li>
                    <a href="#seventh" data-toggle="tab">G</a>
                </li>
                <li>
                    <a href="#eightht" data-toggle="tab">H</a>
                </li>
                <li class="caret-dropdown">
                    <a href="" data-toggle="tab"><i class="fa fa-caret-down"></i></a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="first">
                    <div class="panel-collapse collapse in active">
                        <div class="panel-body panel-main">
                            <div class="row row-custom">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-body panel-no-padding" style="border-bottom-right-radius: 0; border-bottom-left-radius: 0;">
                                            <div class="scroller-tab">
                                                <table class="table table-responsive table-striped">
                                                    <tr class="table-panel-head row-panel-head">
                                                        <th></th>
                                                        <th><input type='checkbox'></th>
                                                        <th>Customer Name</th>
                                                        <th>Status</th>
                                                        <th>Action Items</th>
                                                        <th>
                                                            <div class="form-group search-table-main-div">
                                                                <input type="text" class="form-control search-table" placeholder="Search">
                                                            </div>
                                                        </th>
                                                    </tr>
                                                   <?php foreach($CustomerList as $customers){?>
                                                    <tr>
                                                        <td>1</td>
                                                        <td><input type='checkbox'></td>
                                                        <td><?= $customers->first_name ?></td>
                                                        <td class="td-status"><i class="fa fa-smile-o"></i></td>
                                                        <td>
                                                            <ul class="list-inline list-action">
                                                                <li>
                                                                            <span data-toggle="modal" data-target="#modal-customer-details">
                                                                                <i class="fa fa-exclamation-circle" data-placement='top' title="Details" data-toggle='tooltip'></i>
                                                                            </span>
                                                                </li>
                                                                <li><i class="fa fa-trash" data-placement='top' title="Delete" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-crosshairs" data-placement='top' title="Move" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-envelope" data-placement='top' title="Send Msg" data-toggle='tooltip'></i></li>
                                                            </ul>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <?php } ?>
                                               <!--     <tr>
                                                        <td>1</td>
                                                        <td><input type='checkbox'></td>
                                                        <td>John Doe</td>
                                                        <td class="td-status"><i class="fa fa-smile-o"></i></td>
                                                        <td>
                                                            <ul class="list-inline list-action">
                                                                <li><i class="fa fa-exclamation-circle" data-placement='top' title="Details" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-trash" data-placement='top' title="Delete" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-crosshairs" data-placement='top' title="Move" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-envelope" data-placement='top' title="Send Msg" data-toggle='tooltip'></i></li>
                                                            </ul>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td><input type='checkbox'></td>
                                                        <td>John Doe</td>
                                                        <td class="td-status"><i class="fa fa-smile-o"></i></td>
                                                        <td>
                                                            <ul class="list-inline list-action">
                                                                <li><i class="fa fa-exclamation-circle" data-placement='top' title="Details" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-trash" data-placement='top' title="Delete" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-crosshairs" data-placement='top' title="Move" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-envelope" data-placement='top' title="Send Msg" data-toggle='tooltip'></i></li>
                                                            </ul>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td><input type='checkbox'></td>
                                                        <td>John Doe</td>
                                                        <td class="td-status"><i class="fa fa-smile-o"></i></td>
                                                        <td>
                                                            <ul class="list-inline list-action">
                                                                <li><i class="fa fa-exclamation-circle" data-placement='top' title="Details" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-trash" data-placement='top' title="Delete" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-crosshairs" data-placement='top' title="Move" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-envelope" data-placement='top' title="Send Msg" data-toggle='tooltip'></i></li>
                                                            </ul>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td><input type='checkbox'></td>
                                                        <td>John Doe</td>
                                                        <td class="td-status"><i class="fa fa-smile-o"></i></td>
                                                        <td>
                                                            <ul class="list-inline list-action">
                                                                <li><i class="fa fa-exclamation-circle" data-placement='top' title="Details" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-trash" data-placement='top' title="Delete" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-crosshairs" data-placement='top' title="Move" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-envelope" data-placement='top' title="Send Msg" data-toggle='tooltip'></i></li>
                                                            </ul>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td><input type='checkbox'></td>
                                                        <td>John Doe</td>
                                                        <td class="td-status"><i class="fa fa-smile-o"></i></td>
                                                        <td>
                                                            <ul class="list-inline list-action">
                                                                <li><i class="fa fa-exclamation-circle" data-placement='top' title="Details" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-trash" data-placement='top' title="Delete" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-crosshairs" data-placement='top' title="Move" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-envelope" data-placement='top' title="Send Msg" data-toggle='tooltip'></i></li>
                                                            </ul>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td><input type='checkbox'></td>
                                                        <td>John Doe</td>
                                                        <td class="td-status"><i class="fa fa-smile-o"></i></td>
                                                        <td>
                                                            <ul class="list-inline list-action">
                                                                <li><i class="fa fa-exclamation-circle" data-placement='top' title="Details" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-trash" data-placement='top' title="Delete" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-crosshairs" data-placement='top' title="Move" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-envelope" data-placement='top' title="Send Msg" data-toggle='tooltip'></i></li>
                                                            </ul>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td><input type='checkbox'></td>
                                                        <td>John Doe</td>
                                                        <td class="td-status"><i class="fa fa-smile-o"></i></td>
                                                        <td>
                                                            <ul class="list-inline list-action">
                                                                <li><i class="fa fa-exclamation-circle" data-placement='top' title="Details" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-trash" data-placement='top' title="Delete" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-crosshairs" data-placement='top' title="Move" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-envelope" data-placement='top' title="Send Msg" data-toggle='tooltip'></i></li>
                                                            </ul>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td><input type='checkbox'></td>
                                                        <td>John Doe</td>
                                                        <td class="td-status"><i class="fa fa-smile-o"></i></td>
                                                        <td>
                                                            <ul class="list-inline list-action">
                                                                <li><i class="fa fa-exclamation-circle" data-placement='top' title="Details" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-trash" data-placement='top' title="Delete" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-crosshairs" data-placement='top' title="Move" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-envelope" data-placement='top' title="Send Msg" data-toggle='tooltip'></i></li>
                                                            </ul>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td><input type='checkbox'></td>
                                                        <td>John Doe</td>
                                                        <td class="td-status"><i class="fa fa-smile-o"></i></td>
                                                        <td>
                                                            <ul class="list-inline list-action">
                                                                <li><i class="fa fa-exclamation-circle" data-placement='top' title="Details" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-trash" data-placement='top' title="Delete" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-crosshairs" data-placement='top' title="Move" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-envelope" data-placement='top' title="Send Msg" data-toggle='tooltip'></i></li>
                                                            </ul>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td><input type='checkbox'></td>
                                                        <td>John Doe</td>
                                                        <td class="td-status"><i class="fa fa-smile-o"></i></td>
                                                        <td>
                                                            <ul class="list-inline list-action">
                                                                <li><i class="fa fa-exclamation-circle" data-placement='top' title="Details" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-trash" data-placement='top' title="Delete" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-crosshairs" data-placement='top' title="Move" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-envelope" data-placement='top' title="Send Msg" data-toggle='tooltip'></i></li>
                                                            </ul>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td><input type='checkbox'></td>
                                                        <td>John Doe</td>
                                                        <td class="td-status"><i class="fa fa-smile-o"></i></td>
                                                        <td>
                                                            <ul class="list-inline list-action">
                                                                <li><i class="fa fa-exclamation-circle" data-placement='top' title="Details" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-trash" data-placement='top' title="Delete" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-crosshairs" data-placement='top' title="Move" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-envelope" data-placement='top' title="Send Msg" data-toggle='tooltip'></i></li>
                                                            </ul>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td><input type='checkbox'></td>
                                                        <td>John Doe</td>
                                                        <td class="td-status"><i class="fa fa-smile-o"></i></td>
                                                        <td>
                                                            <ul class="list-inline list-action">
                                                                <li><i class="fa fa-exclamation-circle" data-placement='top' title="Details" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-trash" data-placement='top' title="Delete" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-crosshairs" data-placement='top' title="Move" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-envelope" data-placement='top' title="Send Msg" data-toggle='tooltip'></i></li>
                                                            </ul>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td><input type='checkbox'></td>
                                                        <td>John Doe</td>
                                                        <td class="td-status"><i class="fa fa-smile-o"></i></td>
                                                        <td>
                                                            <ul class="list-inline list-action">
                                                                <li><i class="fa fa-exclamation-circle" data-placement='top' title="Details" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-trash" data-placement='top' title="Delete" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-crosshairs" data-placement='top' title="Move" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-envelope" data-placement='top' title="Send Msg" data-toggle='tooltip'></i></li>
                                                            </ul>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td><input type='checkbox'></td>
                                                        <td>John Doe</td>
                                                        <td class="td-status"><i class="fa fa-smile-o"></i></td>
                                                        <td>
                                                            <ul class="list-inline list-action">
                                                                <li><i class="fa fa-exclamation-circle" data-placement='top' title="Details" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-trash" data-placement='top' title="Delete" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-crosshairs" data-placement='top' title="Move" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-envelope" data-placement='top' title="Send Msg" data-toggle='tooltip'></i></li>
                                                            </ul>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td><input type='checkbox'></td>
                                                        <td>John Doe</td>
                                                        <td class="td-status"><i class="fa fa-smile-o"></i></td>
                                                        <td>
                                                            <ul class="list-inline list-action">
                                                                <li><i class="fa fa-exclamation-circle" data-placement='top' title="Details" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-trash" data-placement='top' title="Delete" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-crosshairs" data-placement='top' title="Move" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-envelope" data-placement='top' title="Send Msg" data-toggle='tooltip'></i></li>
                                                            </ul>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td>1</td>
                                                        <td><input type='checkbox'></td>
                                                        <td>John Doe</td>
                                                        <td class="td-status"><i class="fa fa-smile-o"></i></td>
                                                        <td>
                                                            <ul class="list-inline list-action">
                                                                <li><i class="fa fa-exclamation-circle" data-placement='top' title="Details" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-trash" data-placement='top' title="Delete" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-crosshairs" data-placement='top' title="Move" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-envelope" data-placement='top' title="Send Msg" data-toggle='tooltip'></i></li>
                                                            </ul>
                                                        </td>
                                                        <td></td>
                                                    </tr>-->
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <ol class="pagination">
                                        <li><a href="#" class="go-prev"><i class="fa fa-chevron-left"></i></a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">5</a></li>
                                        <li><a href="#" class="go-next"><i class="fa fa-chevron-right"></i></a></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="second">
                    <div class="panel-collapse collapse in active">
                        <div class="panel-body panel-main">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">appointments</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">1</h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">estimated sales</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">2</h1>
                                            <p>sales</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">new customers</h5>
                                        </div>
                                        <div class="panel-body panel-no-padding">
                                            <div class="div-scroller">
                                                <table class="table table-responsive table-striped">
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-custom">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">satisfaction</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">10%</h1>
                                            <p>satisfaction</p>
                                            <div class="section-panel-footer">
                                                <p>From 0 Reviews</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">total appointments / month</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">0</h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">extra appointments</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">5%</h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-custom">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">appointments</h5>
                                        </div>
                                        <div class="panel-body panel-no-padding">
                                            <div class="table-scroller">
                                                <table class="table table-responsive table-striped table-months">
                                                    <tr>
                                                        <th>Mon</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Tue</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Wed</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Thur</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Fri</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Sat</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Sun</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="third">
                    <div class="panel-collapse collapse in active">
                        <div class="panel-body panel-main">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">appointments</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">3</h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">estimated sales</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">4</h1>
                                            <p>sales</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">new customers</h5>
                                        </div>
                                        <div class="panel-body panel-no-padding">
                                            <div class="div-scroller">
                                                <table class="table table-responsive table-striped">
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-custom">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">satisfaction</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">10%</h1>
                                            <p>satisfaction</p>
                                            <div class="section-panel-footer">
                                                <p>From 0 Reviews</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">total appointments / month</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">0</h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">extra appointments</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">5%</h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-custom">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">appointments</h5>
                                        </div>
                                        <div class="panel-body panel-no-padding">
                                            <div class="table-scroller">
                                                <table class="table table-responsive table-striped table-months">
                                                    <tr>
                                                        <th>Mon</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Tue</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Wed</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Thur</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Fri</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Sat</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Sun</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane fade" id="forth">
                    <div class="panel-collapse collapse in active">
                        <div class="panel-body panel-main">
                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">appointments</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">0</h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">estimated sales</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">0</h1>
                                            <p>sales</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">new customers</h5>
                                        </div>
                                        <div class="panel-body panel-no-padding">
                                            <div class="div-scroller">
                                                <table class="table table-responsive table-striped">
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                    <tr><td>Customer Name</td></tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-custom">
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">satisfaction</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">10%</h1>
                                            <p>satisfaction</p>
                                            <div class="section-panel-footer">
                                                <p>From 0 Reviews</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">total appointments / month</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">0</h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-4">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">extra appointments</h5>
                                        </div>
                                        <div class="panel-body text-center panel-center">
                                            <h1 class="center-top">5%</h1>
                                            <p>appointments</p>
                                            <div class="section-panel-footer">
                                                <p>From Last Month</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row row-custom">
                                <div class="col-lg-12">
                                    <div class="panel panel-default">
                                        <div class="panel-heading header-border">
                                            <h5 class="text-uppercase panel-title panel-body-title">appointments</h5>
                                        </div>
                                        <div class="panel-body panel-no-padding">
                                            <div class="table-scroller">
                                                <table class="table table-responsive table-striped table-months">
                                                    <tr>
                                                        <th>Mon</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Tue</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Wed</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Thur</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Fri</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Sat</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                    <tr>
                                                        <th>Sun</th>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                        <td>xx</td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-customer-details">
    <div class="modal-dialog modal-dialog-customer">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="list-inline list-modal-header">
                    <li class="text-uppercase"><i class="fa fa-user" style="color: #FC7600;"></i> customer</li>
                    <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                        <a href=""><i class="fa fa-reply"></i></a>
                    </li>
                    <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                        <a href=""><i class="fa fa-plus"></i></a>
                    </li>
                    <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban" id="ban-btn-toggle"><i class="fa fa-ban"></i></a></li>
                    <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay" id="pay-btn-toggle"><i class="fa fa-dollar"></i></a></li>
                    <li class="pull-right" style="font-size: 12px;">Gift Certificates / Discount Coupons</li>
                </ul>
            </div>
            <div class="modal-body">
                <div class="modal-body-inner-contents">
                    <div class="row">
                        <div class="col-lg-7 col-md-7 col-sm-7">
                            <div class="row">
                                <div class="col-lg-4">
                                    <i class="fa fa-plus upload-image" data-toggle="tooltip" data-placement="top" title="Upload Image"></i>
                                    <img src="images/staff2.png">
                                </div>
                                <div class="col-lg-8">
                                    <h4 class="txt-theme" style="position: relative; top: 9px;">John Doe</h4>
                                    <p><small>Johndoe@gmail.com</small></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="list-inline" style="margin-top: 15px;">
                                        <li><small><a href="" class="txt-theme">Invite to schedule online</a></small></li><li class="txt-theme"><small>|</small></li>
                                        <li><small><a href="" class="txt-theme">Verify</a></small></li><li class="txt-theme"><small>|</small></li>
                                        <li><small><a href="" class="txt-theme">Edit</a></small></li><li class="txt-theme"><small>|</small></li>
                                        <li><small><a href="" class="txt-theme">Delete</a></small></li>
                                    </ul>
                                    <button class="btn btn-main" style="padding: 4px 9px;"><i class="fa fa-plus"></i> Add Tags</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 media-col6">
                            <div class="row">
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                    <i class="fa fa-map-marker txt-theme"></i>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                    <p><small>145 strait street, CT.No City, No Region United States(01234)</small></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                    <i class="fa fa-mobile txt-theme"></i>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                    <p><small>098-879-46548</small></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12" style="margin-top: 8px;"><button class="btn btn-main" style="padding: 3px 4px;"><small>Send Gift Certificates</small></button></div>
                                <div class="col-lg-12" style="margin-top: 5px;"><button class="btn btn-main" style="padding: 3px 4px;"><small>Send Discount Coupons</small></button></div>
                            </div>
                        </div>
                    </div>
                    <div class="row" style="margin-top: 20px;">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <textarea class="form-control" rows="2" placeholder="Add Information"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="modal-body-inner-tabs">
                        <ul class="nav nav-tabs nav-main nav-customer-details">
                            <li class="active">
                                <a href="#up-app" data-toggle="tab">Upcoming Appointments</a>
                            </li>
                            <li>
                                <a href="#past-app" data-toggle="tab">Past Appointments</a>
                            </li>
                            <li>
                                <a href="#payments" data-toggle="tab">Payments</a>
                            </li>
                            <li>
                                <a href="#promotion" data-toggle="tab">Promotion</a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="up-app">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body">
                                        <div class="row row-custom">
                                            <div class="col-lg-12">
                                                <div class="panel panel-default">
                                                    <p>No Upcoming Appointments</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="past-app">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body">
                                        <div class="row row-custom">
                                            <div class="col-lg-12">
                                                <div class="panel panel-default">
                                                    <p>No Upcoming Appointments</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="payments">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body">
                                        <div class="row row-custom">
                                            <div class="col-lg-12">
                                                <div class="panel panel-default">
                                                    <p>No Upcoming Appointments</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="promotion">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body">
                                        <div class="row row-custom">
                                            <div class="col-lg-12">
                                                <div class="panel panel-default">
                                                    <p>No Upcoming Appointments</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="ban">
    <div class="modal-dialog modal-dialog-customer">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="list-inline list-modal-header">
                    <li class="text-uppercase"><i class="fa fa-user" style="color: #FC7600;"></i> customer booking restrictions</li>
                    <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                        <a href=""><i class="fa fa-reply"></i></a>
                    </li>
                    <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                        <a href=""><i class="fa fa-plus"></i></a>
                    </li>
                    <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban"><i class="fa fa-ban"></i></a></li>
                    <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay"><i class="fa fa-dollar"></i></a></li>
                </ul>
            </div>
            <div class="modal-body">
                <div class="modal-body-inner-contents">
                    <div class="alert alert-warning">
                        <i class="fa fa-warning"></i> <small>Client Advance setting is a premium feature available with PRO and above packages. <a href="#" class="txt-theme">Upgrade your account </a> or <a href="" class="txt-theme">Compare various Packages</a></small>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p>This Customer is</p>
                            <div class="form-group">
                                <select class="form-control">
                                    <option selected>Post-Paying</option>
                                    <option>Post-Paying</option>
                                    <option>Post-Paying</option>
                                </select>
                            </div>
                            <p class="text-muted">
                                <small>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                </small>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p>Future Bookings Status</p>
                            <div class="form-group">
                                <select class="form-control">
                                    <option selected>As per settings</option>
                                    <option>Post-Paying</option>
                                    <option>Post-Paying</option>
                                </select>
                            </div>
                            <p class="text-muted">
                                <small>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                </small>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p>With Restriction</p>
                            <div class="form-group">
                                <select class="form-control">
                                    <option selected>As per settings</option>
                                    <option>Post-Paying</option>
                                    <option>Post-Paying</option>
                                </select>
                            </div>
                            <p class="text-muted">
                                <small>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                </small>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="list-inline pull-right">
                                <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;">Cancel</button></li>
                                <li><button class="btn btn-main" style="padding-right: 19px; padding-left: 19px;">Save</button></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="pay">
    <div class="modal-dialog modal-dialog-customer">
        <div class="modal-content">
            <div class="modal-header">
                <ul class="list-inline list-modal-header">
                    <li class="text-uppercase"><i class="fa fa-dollar" style="color: #FC7600;"></i> add membership payment</li>
                    <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                        <a href=""><i class="fa fa-reply"></i></a>
                    </li>
                    <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                        <a href=""><i class="fa fa-plus"></i></a>
                    </li>
                    <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban"><i class="fa fa-ban"></i></a></li>
                    <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay"><i class="fa fa-dollar"></i></a></li>
                </ul>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-6">
                        <p>Payment Date</p>
                        <div class="form-group">
                            <input type="text" placeholder="4-17-2016" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-6">
                        <p>Payment For</p>
                        <div class="form-group">
                            <input type="text" placeholder="Add New Payment" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <p class="text-muted pull-right" style="margin-top: -9px;"><small>Customer restrictions of the selected membership will be applied automatically.</small></p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                        <div class="form-group">
                            <textarea class="form-control" rows="2" placeholder="Note against payment"></textarea>
                            <p class="text-danger pull-right"><small>Please do not enter html tags</small></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="modal-body-inner-tabs">
                        <div class="row">
                            <div class="col-lg-4">
                                <p class="txt-theme">Amount</p>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Additional Charges">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <span style="position: absolute; left: -2px;">$</span> <input type="text" class="form-control" placeholder="$$$">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Discount">
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="form-group">
                                    <span style="position: absolute; left: -6px;">by</span>
                                    <select class="form-control" placeholder="Cash">
                                        <option selected="">Cash</option>
                                        <option>d</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Total">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <ul class="list-inline pull-right">
                                    <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;">Cancel</button></li>
                                    <li><button class="btn btn-main" style="padding-right: 19px; padding-left: 19px;">Save</button></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="customers-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <?php $form = ActiveForm::begin(); ?>



    <?= $form->field($NewCustomer, 'first_name')->textInput(['maxlength' => true]) ?>



    <?= $form->field($NewCustomer, 'last_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($NewCustomer, 'date_of_birth')->textInput() ?>

    <?= $form->field($NewCustomer, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($NewCustomer, 'cell_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($NewCustomer, 'home_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($NewCustomer, 'work_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($NewCustomer, 'customer_address')->textInput(['maxlength' => true]) ?>
    <?= $form->field($NewCustomer, 'customer_region')->textInput(['maxlength' => true]) ?>
    <?= $form->field($NewCustomer, 'customer_city')->textInput(['maxlength' => true]) ?>
    <?= $form->field($NewCustomer, 'customer_zip')->textInput(['maxlength' => true]) ?>


    <div class="form-group">
        <?= Html::submitButton($NewCustomer->isNewRecord ? 'Create' : 'Update', ['class' => $NewCustomer->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

    <p>
        <?= Html::a('Create Customers', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'customer_id',
            'learners_permit_number',
            'first_name',
            'middle_name',
            'last_name',
            // 'date_of_birth',
            // 'email:email',
            // 'cell_phone',
            // 'test_type',
            // 'sub_test_type',
            // 'confirmed',
            // 'third_user_id',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
