<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;
use app\models\Customers;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if($type=='list'){
     ?>
<style>
.one_gift_certificate {
    cursor:pointer;
    padding:14px 0;
}
.one_gift_certificate.selected{
    background:#EAEAEA;
}
</style>
<?php
    foreach($gift_certificates as $gift_certificate){ ?>
    <div class="col-sm-12 one_gift_certificate" style="margin-bottom:20px;" data-atrid="<?= $gift_certificate->gc_id ?>" >
        <div class="col-sm-6">
            <?php
            echo '<strong>Gift value :</strong> '.$gift_certificate->gift_value.'<br/>';
            echo '<strong>Gift Selling Price :</strong>'.$gift_certificate->gift_selling_price;
            ?>
        </div>
        <div class="col-sm-6">
            <?php
            echo '<img src="'.$gift_certificate->gift_image.'" style="max-width:100%" />';
            ?>
        </div>
    </div>
    <div class="clearfix"></div>
<?php

}
?>
<div class="clearfix"></div>
<?php }else if($type='sell'){
    $form = ActiveForm::begin(['id' => 'sells_giftcertificate_form', 'action' => Yii::getAlias('@web') . '/index.php/marketing/create_sell_gift_certificate?id=' . $new_gift_certificate->gc_id,
    'options' => ['class' => 'generate_email_template']]); ?>
    <div class="col-sm-12">
        <?= $form->field($model, 'amount')->textInput(['maxlength' => true, 'class' => 'form-control','value'=>$new_gift_certificate->gift_selling_price])->label('Amount') ?>
    </div>
    <div class="col-sm-12">
        <label>To</label>
        <?= $form->field($model, 'to_name')->textInput(['maxlength' => true, 'class' => 'form-control','placeholder' => "Name"])->label(false) ?>
        <?= $form->field($model, 'to_email')->textInput(['maxlength' => true, 'class' => 'form-control','placeholder' => "Email"])->label(false) ?>
    </div>
    <div class="col-sm-12">

        <?= $form->field($model, 'message')->textInput(['maxlength' => true, 'class' => 'form-control'])->label('Message') ?>
    </div>
    <div class="col-sm-12">
        <label>From</label>
        <?= $form->field($model, 'from_name')->textInput(['maxlength' => true, 'class' => 'form-control','placeholder' => "Name"])->label(false) ?>
        <?= $form->field($model, 'from_email')->textInput(['maxlength' => true, 'class' => 'form-control','placeholder' => "Email"])->label(false) ?>
    </div>
    <div class="col-sm-12"><?= Html::submitButton($model->isNewRecord ? 'Send' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-main pull-right' : 'btn btn-main pull-right']) ?><a class="btn btn-default pull-right" data-toggle="tab" href="#pay3" style="font-size: 13px; color: #FC7600 !important;margin-right:10px" onclick="hidemodel('#new_customer')">Cancel</a></div>
    <div class="clearfix"></div>
    <?php  ActiveForm::end(); ?>

<?php } ?>