<?php

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\widgets\Pjax;


/* @var $this yii\web\View */
/* @var $model app\models\Customers */
?>


            <div class="modal-body" style="padding: 0">
                <?php Pjax::begin(['id' => 'customer_detail_reload']); ?>
                <div class="tab-content">
                    <div class="tab-pane active" id="custmer_det">
                <div class="modal-body-inner-contents">
                    <?php foreach($model as $customer){
                        $customerID =  $customer->customer_id?>
                    <div class="row">
                        <div class="col-lg-7 col-md-7 col-sm-7">
                            <div class="row">
                                <div class="col-lg-4">
                                    <form enctype="multipart/form-data"   method="post" class="staffimageform" action="<?= Yii::getAlias('@web') ?>/index.php/customers/uploadcustomer_image?id=<?= $customer->customer_id ?>" id="staffimageform<?= $customer->customer_id ?>" >
                                        <input type="hidden" id="resource_csrf" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />

                                        <input type="file" name="profile_image" style="display: none;" class="staffimagelog">


                                    </form>
                                    <i class="fa fa-plus upload-image" onclick="changeProfile(event,this)" data-toggle="tooltip" data-placement="top" title="Upload Image"></i>
                                   <?php if($customer->profile_image){?>
                                    <img src="<?= Yii::getAlias('@web') ?>/<?= $customer->profile_image ?>" style="max-width:75px;border-radius:39px">
                                   <?php  }else { ?>
                                       <img src="<?= Yii::getAlias('@web') ?>/img/staff2.png" style="max-width:75px;border-radius:39px">
                                    <?php } ?>
                                </div>
                                <div class="col-lg-8">
                                    <h4 class="txt-theme" style="position: relative; top: 9px;"><?= $customer->first_name ?> <?= $customer->last_name ?></h4>
                                    <p><small id="customer_email_cert"><?= $customer->email ?></small></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="list-inline" style="margin-top: 15px;">
                                        <li><small><a href="#" onclick="invitation_email('<?= $customer->customer_id ?>',event)" class="txt-theme">Invite</a></small></li><li class="txt-theme"><small>|</small></li>
                                        <li><small><?php if($customer->verify_status==1){ ?><span>Verified</span><?php } else {?><a href="#" class="txt-theme" onclick="verify_customer(event,this,<?= $customer->customer_id ?>)">Verify</a><?php } ?></small></li><li class="txt-theme"><small>|</small></li>
                                        <li><small><a href="<?= Yii::getAlias('@web')?>/index.php/customers/update?id=<?= $customer->customer_id ?>" class="txt-theme" onclick="editcustomer(event,this)">Edit</a></small></li><li class="txt-theme"><small>|</small></li>
                                        <li><small><a href="" class="txt-theme" onclick="customer_delete(event,this,<?= $customer->customer_id ?>)">Delete</a></small></li>
                                    </ul>
                                    <div id="created_tag">
                                       <?php if($customer->customer_tags!=""){ ?>
                                       <button class="btn btn-main" style="padding: 4px 9px;"><?= $customer->customer_tags ?></button>
                                        <?php } ?>
                                    </div>
                                    <button class="btn btn-main" id="popup_add_tags" style="display:none;padding: 4px 9px;margin-top:7px;margin-bottom:10px;" onclick="popup_add_tags(<?= $customer->customer_id ?>)"><i class="fa fa-plus"></i> Add Tags</button>

                                    <div id="addtagbox" style="display:none">
                                        <textarea class="form-control" id="addtag_texarea" ></textarea>
                                        <input type="button" data-customer="<?= $customer->customer_id ?>" class="btn btn-main" id="add_tag_button" value="Add" style="padding: 4px 9px;" > <a href="#" onclick="canceltags(event)" class="txt-theme">Cancel</a>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <div class="col-lg-5 col-md-5 col-sm-5 media-col6">
                            <div class="row">
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                    <i class="fa fa-map-marker txt-theme"></i>
                                </div>
                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                    <p><small><?= $customer->customer_address ?> <?= $customer->customer_city ?> <?= $customer->customer_region ?> <?= $customer->customer_country ?> <?php if($customer->customer_zip){echo '('.$customer->customer_zip.')';} ?></small></p>
                                </div>
                            </div>
                            <div class="row">
                                <?php if($customer->cell_phone!=''){ ?>
                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                    <i class="fa fa-mobile txt-theme"></i>


                                <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                    <p><small><?= $customer->cell_phone ?></small></p>
                                </div>

                            </div>  <?php } ?>
                            <div class="row">
                               <!-- href="#pay3" data-toggle="tab" -->
                              <!--  <div class="col-lg-12" style="margin-top: 8px;"><a class="btn btn-main" href="#pay3" data-toggle="tab"  id="send_gift_certificate_button" style="padding: 3px 4px;"><small>Send Gift Certificates</small></a></div>
                                <div class="col-lg-12" style="margin-top: 5px;"><button class="btn btn-main" style="padding: 3px 4px;" id="send_discount_coupans"><small>Send Discount Coupons</small></button></div>-->
                            </div>
                        </div>
                    </div>

                    <div class="row" style="margin-top: 20px;">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <textarea class="form-control" rows="2" onchange="customer_informationsave('<?= $customer->customer_id ?>',this)" placeholder="Add Information"><?= $customer->customer_information ?></textarea>
                            </div>
                        </div>
                    </div>

                </div>  <?php }   ?>

                <div class="row">
                    <div class="modal-body-inner-tabs">
                        <ul class="nav nav-tabs nav-main nav-customer-details">
                            <li class="active">
                                <a href="#up-app1" data-toggle="tab">Upcoming Appointments</a>
                            </li>
                            <li >
                                <a href="#past-app1" data-toggle="tab" >Past Appointments</a>
                            </li>
                            <li>
                                <a href="#payments1" data-toggle="tab">Payments</a>
                            </li>
                            <!--<li>
                                <a href="#promotion1" data-toggle="tab">Promotion</a>
                            </li>-->
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="up-app1" >
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body">
                                        <div class="row row-custom">
                                            <div class="col-lg-12">
                                                <div class="row">
                                                <div class="panel panel-default">
                                                <table class="table table-responsive table-striped">
                                                    <?php if(!empty($upcoming_appointments)){ ?>
                                                        <?php foreach($upcoming_appointments as $upcoming_appointment){
                                                            ?><tr><td>
                                                                <?php
                                                                echo '<strong style="color:#fc7600">'.$upcoming_appointment['services']['service_name'].'</strong><span style="font-size: 12px"> with '.$upcoming_appointment['users']['displayname'].'</span> <br/>';
                                                                $date = $upcoming_appointment->appointment_date;
                                                                ?><span class="pull-left" style="font-size: 12px"><?php echo date('D, M d, Y', strtotime($date));   echo '  '.$upcoming_appointment->appointment_start_time; ?></span>

                                                        <span class="pull-right"><?php print_r($upcoming_appointment['appointments_payments']['total']);  //  print_r();
                                                            ?></span>
                                                                <div class="clearfix"></div>
                                                            </td>
                                                            </tr>
                                                            <?php

                                                        }
                                                    }else { ?>
                                                    <tr><td><p>No Upcoming Appointments</p></td></tr>
                                                    <?php }?>
                                                    </table>

                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div  >
                            <div class="tab-pane fade " id="past-app1">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body">
                                        <div class="row row-custom">
                                            <div class="col-lg-12">
                                                <div class="row">
                                                <div class="panel panel-default">
                                                   <table class="table table-responsive table-striped">
                                                       <?php if(!empty($past_appointments)){ ?>
                                                    <?php foreach($past_appointments as $past_appointment){
                                                    ?><tr><td>
                                                        <?php
                                                        echo '<strong style="color:#fc7600">'.$past_appointment['services']['service_name'].'</strong><span style="font-size: 12px"> with '.$past_appointment['users']['displayname'].'</span> <br/>';
                                                        $date = $past_appointment->appointment_date;
                                                        ?><span class="pull-left" style="font-size: 12px"><?php echo date('D, M d, Y', strtotime($date));   echo '  '.$past_appointment->appointment_start_time; ?></span>

                                                        <span class="pull-right"><?php isset($past_appointment['appointments_payments'])?$past_appointment['appointments_payments']['total']:'';  ?></span>
                                                        <div class="clearfix"></div>
                                                        </td>
                                                        </tr>
                                                        <?php

                                                    } }else {
                                                           echo '<tr><td><p>No Past Appointments</p></td></tr>';
                                                       }?>
                                                   </table>

                                                </div>
                                                    </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="payments1">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body">
                                        <div class="row row-custom">
                                            <div class="col-lg-12">
                                                <div class="row">
                                                <div class="panel panel-default">

                                                    <table class="table table-responsive table-striped">
                                                    <!--    <tr class="table-panel-head row-panel-head">
                                                            <th></th>
                                                            <th>Payment Type</th>
                                                            <th>Amount</th>
                                                            <th>Payment Mode</th>
                                                            <th></th>
                                                            </tr>-->
                                                        <?php //if(!empty($appointments_payments)){
                                                        if($appointments_payments=='111111ahgd'){
                                                            ?>
                                                            <?php foreach($appointments_payments as $appointments_payment){
                                                                ?>
                                                                <tr>
                                                                    <td></td>
                                                                    <td>
                                                                    <?php if(!empty($appointments_payment->membership_name)){
                                                                        echo $appointments_payment->membership_name;

                                                                    }else {

                                                                    }
                                                                    ?>
                                                                    </td>
                                                                <td><?= $appointments_payment->total ?></td>

                                                                <td></td>
                                                                    <td></td>
                                                                </tr>
                                                                <?php

                                                            }
                                                        }else { ?>
                                                            <tr><td><p>No Payment Available</p></td></tr>
                                                        <?php }?>
                                                    </table>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                           <!-- <div class="tab-pane fade" id="promotion1">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body">
                                        <div class="row row-custom">
                                            <div class="col-lg-12">
                                                <div class="panel panel-default">
                                                    <p>No Upcoming Appointments</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>
                    </div>
                    </div>
              <!--customer Restriction--->
                <div class="modal-body-inner-contents tab-pane" id="ban1">
                    <div class="alert alert-warning">
                        <i class="fa fa-warning"></i> <small>Client Advance setting is a premium feature available with PRO and above packages. <a href="#" class="txt-theme">Upgrade your account </a> or <a href="" class="txt-theme">Compare various Packages</a></small>
                    </div>
                    <?php $form = ActiveForm::begin(['id' => 'ban_customer']); ?>
                    <div class="row">
                        <div class="col-lg-12">
                            <p>This Customer is</p>
                            <div class="form-group">
                                <?php    echo $form->field($modelban, 'customer_id')->hiddenInput()->label(false); ?>
                                 <?php    echo $form->field($modelban, 'payment_terms')->dropDownList(['2' => 'Post Paying','1' => 'Pre Paying',])->label(false); ?>

                            </div>
                            <p class="text-muted">
                                <small>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                </small>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p>Future Bookings Status</p>
                            <div class="form-group">
                                <?php    echo $form->field($modelban, 'future_booking')->dropDownList(['0' => 'As per Setting','1' => 'Approved','2' => 'Unapproved'])->label(false); ?>
                            </div>
                            <p class="text-muted">
                                <small>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                </small>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <p>With Restriction</p>
                            <div class="form-group">
                                <select class="form-control">
                                    <option selected>As per settings</option>
                                    <option>Post-Paying</option>
                                    <option>Post-Paying</option>
                                </select>
                            </div>
                            <p class="text-muted">
                                <small>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                </small>
                            </p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="list-inline pull-right">
                                <li><a href="#custmer_det" class="btn btn-default" data-toggle="tab" id="ban-btn-toggle">Cancel</a></li>-->
                                <li><?php echo Html::submitButton('Save', ['class' => 'btn btn-main']); ?></li>
                            </ul>
                        </div>
                    </div>
                    <?php ActiveForm::end(); ?>

                </div>



                    <div class="modal-body-inner-contents tab-pane" id="pay1">
                        <?php $form = ActiveForm::begin(['id' => 'membership_payment']); ?>

                            <!--                <legend>--><?//= Yii::t('app', 'User')?><!--</legend>-->


                        <div class="row">
                            <div class="col-lg-6">
                                <p>Payment Date</p>
                                <div class="form-group">
                                    <input type="text" placeholder="4-17-2016" class="form-control">
                                </div>
                            </div>
                          <?php // print_r($customerID);
                          echo Html::hiddenInput('customer_id', $customerID); ?>
                            <div class="col-lg-6">
                                <p>Payment For</p>
                                <div class="form-group">
                                    <?= $form->field($new_appointments_payments, 'membership_name')->label(false) ?>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <?= $form->field($new_appointments_payments, 'payment_note')->textarea() ?>
                                <p class="text-muted pull-right" style="margin-top: -9px;"><small>Customer restrictions of the selected membership will be applied automatically.</small></p>
                            </div>
                        </div>

                        <div class="row">
                            <div class="modal-body-inner-tabs">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <p class="txt-theme">Amount</p>
                                        <div class="form-group">
                                            <?= $form->field($new_appointments_payments, 'additional_charges')->input('additional_charges', ['placeholder' => "Additional Charges"])->label(false) ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <span style="position: absolute; left: -2px;">$</span> <?= $form->field($new_appointments_payments, 'to_pay')->label(false) ?>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($new_appointments_payments, 'discount')->input('discount', ['placeholder' => "Discount"])->label(false) ?>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <span style="position: absolute; left: -6px;">by</span>
                                            <select name="AppointmentsPayments[payment_method]" class="form-control">
                                                <option value="Cash">Cash</option>
                                                <option value="Credit Card">Credit Card</option>
                                                <option value="Cheque">Cheque</option>
                                                <option value="Echeck">Echeck</option>
                                                <option value="Debit Card">Debit Card</option>
                                                <option value="Credit Card and Cash">Credit Card and Cash</option>

                                            </select>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <?= $form->field($new_appointments_payments, 'total')->input('total', ['placeholder' => "Total"])->label(false)?>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <ul class="list-inline pull-right">
                                            <li><a href="#custmer_det" class="btn btn-default" data-toggle="tab" id="ban-btn-toggle1" style="font-size:12px;">Cancel</a></li>
                                            <li><?php echo Html::submitButton('Save', ['class' => 'btn btn-main', 'name' => 'submit']); ?></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                            <?php ActiveForm::end(); ?>
                    </div>
                    <div class="modal-body-inner-contents tab-pane" id="pay2">


                        <!--                <legend>--><?//= Yii::t('app', 'User')?><!--</legend>-->


                        <?php $form = ActiveForm::begin(['id'=>'update_customerfrom']); ?>

                        <?= $form->field($modelban, 'customer_id')->hiddenInput()->label(false) ?>
                        <div class="col-sm-6">
                            <?= $form->field($modelban, 'first_name')->input(['maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($modelban, 'last_name')->input(['maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-12">
                            <?= $form->field($modelban, 'email')->input(['maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($modelban, 'cell_phone')->input(['maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($modelban, 'home_phone')->input(['maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($modelban, 'work_phone')->input(['maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-12">
                            <?= $form->field($modelban, 'customer_address')->input(['maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($modelban, 'customer_region')->input(['maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($modelban, 'customer_city')->input(['maxlength' => true]) ?>
                        </div>
                        <div class="col-sm-6">
                            <?= $form->field($modelban, 'customer_zip')->input(['maxlength' => true]) ?>
                        </div>
                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-lg-12">
                                <ul class="list-inline pull-right">
                                    <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;" onclick="hidemodel('#new_customer')">Cancel</button></li>
                                    <li><?php echo Html::submitButton('Save', ['class' => 'btn btn-main', 'name' => 'submit']); ?></li>
                                </ul>
                            </div>
                        </div>



                        <?php ActiveForm::end(); ?>
                    </div>
                    <div class="modal-body-inner-contents tab-pane" id="pay3" style="padding:0">
                            <div id="tab_gift_certificate_cont">

                            </div>















                        <div class="clearfix"></div>
                        <div class="row">
                            <div class="col-lg-12">
                                <ul class="list-inline pull-right">
                                    <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;" onclick="hidemodel('#new_customer')">Cancel</button></li>
                                    <li><button class="btn btn-main" id="sendcertificate_but">Send Certificate </button></li>
                                </ul>
                            </div>
                        </div>




                    </div>
                    <div class="modal-body-inner-contents tab-pane" id="pay4">

                    </div>



</div>
                <?php Pjax::end(); ?>

            </div>
<?php
$scriptjs = "$('#membership_payment1').submit(function () {
var form = $(this);
alert('membership_payment');
$.ajax({
url:'".Yii::getAlias('@web')."/index.php/customers/addmembershippayment',
type: 'post',
data: form.serialize(),
success: function (response) {
if(response==1){
$('#business_hour_next').click();

}
}
}); 

return false;
event.preventDefault();
});
$('#ban_customer').submit(function () {
var form = $(this);
$.ajax({
url:'".Yii::getAlias('@web')."/index.php/customers/updateban',
type: 'post',
data: form.serialize(),
success: function (response) {
if(response==1){
$('#ban_customer #ban-btn-toggle').click();

}
}
}); 

return false;
event.preventDefault();
});
$('body').on('beforeSubmit', '#update_customerfrom', function () {
var form = $('#update_customerfrom');
$.ajax({
url:'".Yii::getAlias('@web')."/index.php/customers/updateban',
type: 'post',
data: form.serialize(),
success: function (response) {
if(response==1){
 $.pjax.reload({container: '#customer_detail_reload'});


}
}
}); 
return false;
})
";
$this->registerJs($scriptjs);
    ?>