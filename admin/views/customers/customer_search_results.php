<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
?>

                                                <table class="table table-responsive table-striped">
                                                    <tr class="table-panel-head row-panel-head">
                                                        <th></th>
                                                        <th><input type='checkbox'></th>
                                                        <th>Customer Name</th>
                                                        <th>Status</th>
                                                        <th>Action Items</th>
                                                        <th>
                                                            <div class="form-group search-table-main-div">
                                                                <input type="text" class="form-control search-table" placeholder="Search" onchange="searchalphabets(event,this)">
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    <div id="customerlistload">
                                                        <?php if(!empty($CustomerList)){ ?>
                                                   <?php foreach($CustomerList as $customers){?>
                                                    <tr>
                                                        <td>1</td>
                                                        <td><input type='checkbox'></td>
                                                        <td><?= $customers->first_name ?></td>
                                                        <td class="td-status"><i class="fa fa-smile-o"></i></td>
                                                        <td>
                                                            <ul class="list-inline list-action">
                                                                <li>
                                                                    <span>
                                                                                <i class="fa fa-exclamation-circle" data-placement='top' title="Details" data-toggle='tooltip' onclick="customer_detailspopup(<?= $customers->customer_id?>)"></i>
                                                                            </span>
                                                                          <!--  <span data-toggle="modal" data-target="#modal-customer-details">
                                                                                <i class="fa fa-exclamation-circle" data-placement='top' title="Details" data-toggle='tooltip' onclick="customer_detailspopup(<?= $customers->customer_id?>)"></i>
                                                                            </span>-->
                                                                </li>
                                                                <li><i class="fa fa-trash" data-placement='top' title="Delete" onclick="customer_delete(event,this,<?= $customers->customer_id ?>)"  data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-crosshairs" data-placement='top' title="Move" data-toggle='tooltip'></i></li>
                                                                <li><i class="fa fa-envelope" data-placement='top' title="Send Msg" data-toggle='tooltip'></i></li>
                                                            </ul>
                                                        </td>
                                                        <td></td>
                                                    </tr>
                                                    <?php } ?>
                                                        <?php }else { ?>
                                                       <tr>
                                                           <td></td>
                                                           <td></td>
                                                           <td colspan="4"> No customer For this Criteria  </td>
                                                       <td></td>
                                                       </tr>
                                                        <?php } ?>
                                                    </div>

                                                </table>
