<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\UsersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Third Party Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="users-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Users', ['site/signup'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'username',
            'displayname',
//            [
//                'attribute'=>'displayname',
//                'label' => 'Examiner'
//            ],
            //'user_group',
            [
                'attribute'=>'user_group',
                'format' => 'raw',
                'value'=>function($data){
                    if($data->user_group=='thirdparty-cdl'){
                        return "Third Party CDL";
                    } else {
                        return "Third Party Standard";
                    }
                }
            ],
            'email:email',
            //'auth_key',
            //'password_hash',
            // 'password_reset_token',
            // 'email:email',
            // 'role',
            // 'status',
            // 'created_at',
            // 'updated_at',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{update}{delete}',
                'buttons' => [
                    'update' => function ($url, $model) {

                            return Html::a(
                                '<span class="glyphicon glyphicon-pencil"></span>', yii\helpers\Url::to(['site/signup', 'id' => $model->id, 'action' => 'update']), [
                                    'title' => 'Update',
                                    'data-pjax' => '0',
                                ]
                            );

                    },

                ],
            ],
        ],
    ]); ?>

</div>
