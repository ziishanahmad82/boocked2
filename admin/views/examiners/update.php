<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Examiners */

$this->title = 'Update Examiners: ' . ' ' . $model->examiner_id;
$this->params['breadcrumbs'][] = ['label' => 'Examiners', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->examiner_id, 'url' => ['view', 'id' => $model->examiner_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="examiners-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
