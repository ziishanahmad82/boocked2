<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Locations;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Examiners */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="examiners-form">

    <?php $form = ActiveForm::begin(); ?>

    <?
    if(@$_REQUEST["test_type"]){
        $test_type=$_REQUEST["test_type"];
    } else {
        $test_type=$model->test_type;
    }



    ?>
    <?= $form->field($model, 'location_id')->dropDownList(ArrayHelper::map(Locations::find()->where(['test_type' => $test_type])->all(), 'location_id', 'location_name')); ?>

    <?= $form->field($model, 'examiner_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'examiner_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'examiner_phone')->textInput(['maxlength' => true]) ?>
    <?
    if($model->isNewRecord ) {
        ?>
        <?= $form->field($model, 'test_type')->hiddenInput(['maxlength' => true, 'value' => $_REQUEST["test_type"]])->label(false) ?>
        <?
    } else {
        ?>
        <?= $form->field($model, 'test_type')->hiddenInput(['maxlength' => true])->label(false) ?>
        <?
    }
    ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
