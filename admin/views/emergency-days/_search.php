<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\EmergencyDaysSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="emergency-days-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'holiday_id') ?>

    <?= $form->field($model, 'holiday_date') ?>

    <?= $form->field($model, 'holiday_name') ?>

    <?= $form->field($model, 'holiday_span') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
