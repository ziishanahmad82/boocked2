<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EmergencyDays */

$this->title = 'Update Emergency Days: ' . ' ' . $model->holiday_id;
$this->params['breadcrumbs'][] = ['label' => 'Emergency Days', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->holiday_id, 'url' => ['view', 'id' => $model->holiday_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="emergency-days-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
