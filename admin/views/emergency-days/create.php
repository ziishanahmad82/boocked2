<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EmergencyDays */

$this->title = 'Create Emergency Days';
$this->params['breadcrumbs'][] = ['label' => 'Emergency Days', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="emergency-days-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
