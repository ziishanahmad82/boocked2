<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\PaymentPlan */

$this->title = 'Update Payment Plan: ' . ' ' . $model->pp_id;
$this->params['breadcrumbs'][] = ['label' => 'Payment Plans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->pp_id, 'url' => ['view', 'id' => $model->pp_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="payment-plan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
