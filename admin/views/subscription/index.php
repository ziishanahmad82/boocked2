<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Subscription';
$this->params['breadcrumbs'][] = $this->title;
?>

<?php
/*
$ch = curl_init();
$clientId = "myId";
$secret = "mySecret";

curl_setopt($ch, CURLOPT_URL, "https://api.sandbox.paypal.com/v1/oauth2/token");
curl_setopt($ch, CURLOPT_HEADER, false);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_USERPWD, $clientId.":".$secret);
curl_setopt($ch, CURLOPT_POSTFIELDS, "grant_type=client_credentials");

$result = curl_exec($ch);

if(empty($result))die("Error: No response.");
else
{
    $json = json_decode($result);
   print_r($json);
}

curl_close($ch); */


// 1. Autoload the SDK Package. This will include all the files and classes to your autoloader
//require '/PayPal-PHP-SDK/autoload.php';
// 2. Provide your Secret Key. Replace the given one with your app clientId, and Secret
// https://developer.paypal.com/webapps/developer/applications/myapps
/*$apiContext = new \PayPal\Rest\ApiContext(
    new \PayPal\Auth\OAuthTokenCredential(
        'AYSq3RDGsmBLJE-otTkBtM-jBRd1TCQwFf9RGfwddNXWz0uFU9ztymylOhRS',     // ClientID
        'EGnHDxD_qRPdaLdZz8iCr8N7_MzF-YHPTkjs6NKYQvQSBngp4PTTVWkPZRbL'      // ClientSecret
    )
);
// 3. Lets try to save a credit card to Vault using Vault API mentioned here
// https://developer.paypal.com/webapps/developer/docs/api/#store-a-credit-card
$creditCard = new \PayPal\Api\CreditCard();
$creditCard->setType("visa")
    ->setNumber("4417119669820331")
    ->setExpireMonth("11")
    ->setExpireYear("2019")
    ->setCvv2("012")
    ->setFirstName("Joe")
    ->setLastName("Shopper");
// 4. Make a Create Call and Print the Card
try {
    $creditCard->create($apiContext);
    echo $creditCard.'fasdfasfasd';
}
catch (\PayPal\Exception\PayPalConnectionException $ex) {
    // This will print the detailed information on the exception.
    //REALLY HELPFUL FOR DEBUGGING
    echo $ex->getData();
} */?>

<div class="container-fluid section-main">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 no-padding-l media-xs-full">
            <div class="panel panel-default">
                <div class="panel-heading full">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web') ?>/img/settings.png"> <span class="staff-head">Additional Setting</span></h5></li>
                    </ul>
                </div>
                <div class="panel-body panel-body-nav-tabs-sidebar side3">

                    <p style="padding-top:20px">You can manage advance settings here including current subscription or upgrading your plan.</p>

                </div>
            </div>
        </div>

        <div class="col-lg-9 col-md-9 col-sm-9 media-xs-full">

            <div class="panel-body" style="border: none;">
                <div class="panel panel-default default">
                    <div class="panel-heading full">
                        <ul class="list-inline list-staff">
                            <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web') ?>/img/settings.png"></i> <span class="staff-head">Settings</span></h5></li>
                            <li class="pull-right li-right"><i class="fa fa-plus plus-square"></i></li>
                            <li class="pull-right li-right hidden-xs">Inactive: 0</li>
                            <li class="pull-right li-right hidden-xs">Total Designs: 1</li>
                        </ul>
                    </div>
                    <div class="panel-body">

                        <div class="row" style="margin-top: 20px;">
                            <div class="col-lg-12">
                                <ul class="nav nav-tabs">
                                    <li class="active"><a href="#subscription" data-toggle="tab">Subscription</a></li>
                                    <li><a href="#buycredits" data-toggle="tab" style="border-top-right-radius: 4px;">BuyCredits</a></li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="subscription">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body panel-table">

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <p style="padding-top:20px">YOUR CURRENT MEMBERSHIP</p>
                                                    <p>Free plan  <small><a href="" style="color: #FC7600 !important;">&nbsp Free</a></small></p>
                                                    <small><a href="">Billed monthly | Forum based support</a></small>
                                                </div>

                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <ul class="list-inline list1">
                                                        <li><p>Your Business trial membership expires in 5 days.After this trial Period your account will automatically be converted to a Free Membership,which is Ad supported and has limited features. Upgrading while in Trial Period will give you a 10% discount on your first bill.Use promocode <strong>TRIAL UPGRADE.</strong> <a href="" style="color: #FC7600 !important;text-decoration:underline !important;">Upgrade now</a>
                                                            </p></li>


                                                        <li><strong>Switch Membership</strong></li>


                                                        <li><div class="form-group">
                                                                <select class="form-control">
                                                                    <option selected>Business</option>
                                                                    <option>1</option>
                                                                    <option>2</option>
                                                                </select>
                                                            </div></li>
                                                    </ul>


                                                    <p class="last">During trial Period you can switch between varios memberships to try them.</p>
                                                </div>


                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                <p style="padding-top:20px;font-size:15px;">Member Suggestion</p>
                                                <div class="row text1">
                                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                        <p><small>Get priority and email delivery of confirmation emails.Hide ads from other businesses in your area from your client's public scheduling page and get Google calender linking.</small> <a href="" style="color: #FC7600 !important;text-decoration:underline !important;">Upgrade to Pro Membership</a></p>
                                                        <p><small>Get the most out of Appointy with all advanced scheduling features along with email customization,custom fields, Precision scheduling,fill gaps yb offering discount clubbed with powerful email marketing etc.All premium features are included.</small> <a href="" style="color: #FC7600 !important;text-decoration:underline !important;">Upgrade to Pro Membership</a></p>
                                                        <p><small>Allow your staff or Manager to login and manage their own account.All PRO features.</small> <a href="" style="color: #FC7600 !important;text-decoration:underline !important;">Upgrade to Business Membership</a> </p>
                                                        <p><small>Got multiple locations ? All locations will have business membership features.</small> <a href="" style="color: #FC7600 !important;text-decoration:underline !important;">Upgrade to Enterprise Membership</a></p>
                                                        <a href="" style="color: #FC7600 !important;text-decoration:underline !important;">Buy Credits</a>
                                                    </div>
                                                    <button type="button" class="btn btn-warning ad2">Change Plan</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="buycredits">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body panel-table">
                                                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12" style="margin-bottom: 15px;">
                                                    <p style="padding-top:20px">YOU HAVE 0 CREDIT(S) IN YOUR ACCOUNT.<a href="" style="color: #FC7600 !important;text-decoration:underline !important;">My Membership</a> </p>
                                                    <small>These credits will be used for text alerts,phone verification & bulk email marketing.<a href="" style="color: #FC7600 !important;text-decoration:underline !important;">Know me</a></small><br>
                                                    <small><a href="" style="color: #FC7600 !important;text-decoration:underline !important;">Know your credit usage report</a> or <a href="" style="color: #FC7600 !important;text-decoration:underline !important;">past credit purchase histroy</a></small>
                                                </div>

                                                <div class="strip">
                                                    <table class="table table-responsive table-striped" style="border: 1px solid rgb(221, 212, 212);">
                                                        <thead>
                                                        <tr style="background-color:#ddd;">
                                                            <th></th>
                                                            <th><strong>Packages</strong></th>
                                                            <th><strong>Amount</strong></th>
                                                            <th><strong>Credits</strong></th>
                                                            <th><strong>Descriptions</strong></th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>

                                                     <?php
                                                     $i=1;
                                                     foreach($payment_plan as $paymentPlan) {?>
                                                        <tr style="<?php if ($i % 2 == 0) {echo 'background-color:#f7f7f7';}else { echo 'background-color:#ffebdd';}?>"  >
                                                            <td><input  class="dropdown-toggle" data-target="#paypalbox<?= $i ?>" data-toggle="collapse" type="radio" name="radioGroup" value="<?= $i ?>"></td>
                                                            <td><?= $paymentPlan->plan_name ?></td>
                                                            <td>$<?= $paymentPlan->plan_amount ?></td>
                                                            <td><?= $paymentPlan->plan_credits ?></td>
                                                            <td><?= $paymentPlan->plan_description ?></td>
                                                            <td><a href="#" onclick="event.preventDefault();" class="dropdown-toggle" data-target="#paypalbox<?= $i ?>" data-toggle="collapse" style="color: #FC7600 !important;text-decoration:underline !important;">Buy Credits</a></td>
                                                         </tr>
                                                        <tr style="<?php if ($i % 2 == 0) {echo 'background-color:#f7f7f7';}else {echo 'background-color:#ffebdd';}?>" class="collapse" id="paypalbox<?= $i ?>" ><td colspan="6">
                                                         <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                             <form action="https://www.sandbox.paypal.com/cgi-bin/webscr" method="post" id="payment<?= $i ?>">
                                                                 <input type="hidden" name="cmd" value="_xclick">
                                                                 <input type="hidden" name="business" value="abdulrehmanphp2-facilitator@gmail.com">
                                                                 <input type="hidden" name="item_name" value="<?= $paymentPlan->plan_name ?>">
                                                                 <input type="hidden" name="item_number" value="MEM32507725">

                                                                 <input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                                                                 <input type="hidden" name="notify_url" value="http://localhost/boocked/admin/web/index.php/subscription/notify_url">
                                                                 <INPUT TYPE="hidden" NAME="return" value="http://localhost/boocked/admin/web/index.php/subscription/authorize_records">
                                                                 <input type="hidden" name="amount" value="<?= $paymentPlan->plan_amount ?>">
                                                                 <input type="hidden" name="quantity" value="1">
                                                                 <input type="hidden" name="no_note" value="1">
                                                                 <input type="hidden" name="currency_code" value="USD">



                                                             </form>
                                                             <p style="padding-top:20px">Pay through <a href="#" onClick="event.preventDefault();document.forms['payment<?= $i ?>'].submit();" style="color: #FC7600 !important;text-decoration:underline !important;">Paypal</a> or <a href="" style="color: #FC7600 !important;text-decoration:underline !important;">Link Credit Card</a></p>
                                                             <ul class="list-unstyled">
                                                                 <li><small>1.One call in Pakistan costs 35.01 cents(approx)</small></li>
                                                                 <li><small>2.One SMS in Pakistan costs 11.30 cents(approx)</small></li>
                                                             </ul>
                                                             <div class="row">
                                                                 <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 form">
                                                                     <div class="form-group">
                                                                         <select class="form-control">
                                                                             <option selected>Pakistan</option>
                                                                             <option>English</option>
                                                                         </select>
                                                                     </div>
                                                                 </div>
                                                             </div>
                                                         </div>
                                                             </td>
                                                         </tr>
                                                    <?php $i++; } ?>

                                                        </tbody>
                                                    </table>
                                                </div>
                                            <!--   <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                                    <p style="padding-top:20px">Pay through <a href="" style="color: #FC7600 !important;text-decoration:underline !important;">Paypal</a> or <a href="" style="color: #FC7600 !important;text-decoration:underline !important;">Link Credit Card</a></p>
                                                    <ul class="list-unstyled">
                                                        <li><small>1.One call in Pakistan costs 35.01 cents(approx)</small></li>
                                                        <li><small>2.One SMS in Pakistan costs 11.30 cents(approx)</small></li>
                                                    </ul>
                                                    <div class="row">
                                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 form">
                                                            <div class="form-group">
                                                                <select class="form-control">
                                                                    <option selected>Pakistan</option>
                                                                    <option>English</option>
                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>-->

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-customer-details">
            <div class="modal-dialog modal-dialog-customer">
                <div class="modal-content">
                    <div class="modal-header">
                        <ul class="list-inline list-modal-header">
                            <li class="text-uppercase"><i class="fa fa-user" style="color: #FC7600;"></i> customer</li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                                <a href=""><i class="fa fa-reply"></i></a>
                            </li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                                <a href=""><i class="fa fa-plus"></i></a>
                            </li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban" id="ban-btn-toggle"><i class="fa fa-ban"></i></a></li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay" id="pay-btn-toggle"><i class="fa fa-dollar"></i></a></li>
                            <li class="pull-right" style="font-size: 12px;">Gift Certificates / Discount Coupons</li>
                        </ul>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body-inner-contents">
                            <div class="row">
                                <div class="col-lg-7 col-md-7 col-sm-7">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <i class="fa fa-plus upload-image" data-toggle="tooltip" data-placement="top" title="Upload Image"></i>
                                            <img src="<?= Yii::getAlias('@web') ?>/img/staff2.png">
                                        </div>
                                        <div class="col-lg-8">
                                            <h4 class="txt-theme" style="position: relative; top: 9px;">John Doe</h4>
                                            <p><small>Johndoe@gmail.com</small></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <ul class="list-inline" style="margin-top: 15px;">
                                                <li><small><a href="" class="txt-theme">Invite to schedule online</a></small></li><li class="txt-theme"><small>|</small></li>
                                                <li><small><a href="" class="txt-theme">Verify</a></small></li><li class="txt-theme"><small>|</small></li>
                                                <li><small><a href="" class="txt-theme">Edit</a></small></li><li class="txt-theme"><small>|</small></li>
                                                <li><small><a href="" class="txt-theme">Delete</a></small></li>
                                            </ul>
                                            <button class="btn btn-main" style="padding: 4px 9px;"><i class="fa fa-plus"></i> Add Tags</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 media-col6">
                                    <div class="row">
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                            <i class="fa fa-map-marker txt-theme"></i>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                            <p><small>145 strait street, CT.No City, No Region United States(01234)</small></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                            <i class="fa fa-mobile txt-theme"></i>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                            <p><small>098-879-46548</small></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12" style="margin-top: 8px;"><button class="btn btn-main" style="padding: 3px 4px;"><small>Send Gift Certificates</small></button></div>
                                        <div class="col-lg-12" style="margin-top: 5px;"><button class="btn btn-main" style="padding: 3px 4px;"><small>Send Discount Coupons</small></button></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px;">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="2" placeholder="Add Information"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="modal-body-inner-tabs">
                                <ul class="nav nav-tabs nav-main nav-customer-details">
                                    <li class="active">
                                        <a href="#up-app" data-toggle="tab">Upcoming Appointments</a>
                                    </li>
                                    <li>
                                        <a href="#past-app" data-toggle="tab">Past Appointments</a>
                                    </li>
                                    <li>
                                        <a href="#payments" data-toggle="tab">Payments</a>
                                    </li>
                                    <li>
                                        <a href="#promotion" data-toggle="tab">Promotion</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="up-app">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="past-app">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="payments">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="promotion">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="ban">
            <div class="modal-dialog modal-dialog-customer">
                <div class="modal-content">
                    <div class="modal-header">
                        <ul class="list-inline list-modal-header">
                            <li class="text-uppercase"><i class="fa fa-user" style="color: #FC7600;"></i> customer booking restrictions</li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                                <a href=""><i class="fa fa-reply"></i></a>
                            </li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                                <a href=""><i class="fa fa-plus"></i></a>
                            </li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban"><i class="fa fa-ban"></i></a></li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay"><i class="fa fa-dollar"></i></a></li>
                        </ul>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body-inner-contents">
                            <div class="alert alert-warning">
                                <i class="fa fa-warning"></i> <small>Client Advance setting is a premium feature available with PRO and above packages. <a href="#" class="txt-theme">Upgrade your account </a> or <a href="" class="txt-theme">Compare various Packages</a></small>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>This Customer is</p>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option selected>Post-Paying</option>
                                            <option>Post-Paying</option>
                                            <option>Post-Paying</option>
                                        </select>
                                    </div>
                                    <p class="text-muted">
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                        </small>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>Future Bookings Status</p>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option selected>As per settings</option>
                                            <option>Post-Paying</option>
                                            <option>Post-Paying</option>
                                        </select>
                                    </div>
                                    <p class="text-muted">
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                        </small>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>With Restriction</p>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option selected>As per settings</option>
                                            <option>Post-Paying</option>
                                            <option>Post-Paying</option>
                                        </select>
                                    </div>
                                    <p class="text-muted">
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                        </small>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="list-inline pull-right">
                                        <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;">Cancel</button></li>
                                        <li><button class="btn btn-main" style="padding-right: 19px; padding-left: 19px;">Save</button></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="pay">
            <div class="modal-dialog modal-dialog-customer">
                <div class="modal-content">
                    <div class="modal-header">
                        <ul class="list-inline list-modal-header">
                            <li class="text-uppercase"><i class="fa fa-dollar" style="color: #FC7600;"></i> add membership payment</li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                                <a href=""><i class="fa fa-reply"></i></a>
                            </li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                                <a href=""><i class="fa fa-plus"></i></a>
                            </li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban"><i class="fa fa-ban"></i></a></li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay"><i class="fa fa-dollar"></i></a></li>
                        </ul>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <p>Payment Date</p>
                                <div class="form-group">
                                    <input type="text" placeholder="4-17-2016" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <p>Payment For</p>
                                <div class="form-group">
                                    <input type="text" placeholder="Add New Payment" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="text-muted pull-right" style="margin-top: -9px;"><small>Customer restrictions of the selected membership will be applied automatically.</small></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <textarea class="form-control" rows="2" placeholder="Note against payment"></textarea>
                                    <p class="text-danger pull-right"><small>Please do not enter html tags</small></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="modal-body-inner-tabs">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <p class="txt-theme">Amount</p>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Additional Charges">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <span style="position: absolute; left: -2px;">$</span> <input type="text" class="form-control" placeholder="$$$">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Discount">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <span style="position: absolute; left: -6px;">by</span>
                                            <select class="form-control" placeholder="Cash">
                                                <option selected="">Cash</option>
                                                <option>d</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Total">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <ul class="list-inline pull-right">
                                            <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;">Cancel</button></li>
                                            <li><button class="btn btn-main" style="padding-right: 19px; padding-left: 19px;">Save</button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

      </div>
