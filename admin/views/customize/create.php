<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\CustomizeEmail */

$this->title = 'Create Customize Email';
$this->params['breadcrumbs'][] = ['label' => 'Customize Emails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customize-email-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
