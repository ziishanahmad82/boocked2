<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CustomizeEmail */

$this->title = 'Update Customize Email: ' . ' ' . $model->custom_email_id;
$this->params['breadcrumbs'][] = ['label' => 'Customize Emails', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->custom_email_id, 'url' => ['view', 'id' => $model->custom_email_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="customize-email-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
