<?php

use yii\helpers\Html;
use yii\grid\GridView;
use dosamigos\ckeditor\CKEditor;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customize Emails';
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 no-padding-l media-xs-full">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web') ?>/img/settings.png"> <span class="staff-head">Additional Customizations</span></h5></li>
                    </ul>
                </div>
                <div class="panel-body panel-body-nav-tabs-sidebar">
                    <div class="row">
                        <ul class="nav nav-stacked nav-tabs nav-tabs-sidebar nav-rules">
                            <li class="active"><a href="#tab1" data-toggle="tab">1 Cancellation Policy</a></li>
                            <li><a href="#tab2" data-toggle="tab">2 Additional Information</a></li>
                            <li><a href="#tab3" class="last" data-toggle="tab">3 Customize Emails</a></li>
                            <li><a href="#tab4" class="last" data-toggle="tab">4 Customize Form</a></li>
                            <li><a href="#tab5" class="last" data-toggle="tab">5 Add-On</a></li>
                            <li><a href="#tab6" class="last" data-toggle="tab">6 Calendar</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-lg-9 col-md-9 col-sm-9 media-xs-full">
            <div class="panel panel-default panel-rules2">
                <div class="panel-heading">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><i class="fa fa-clipboard" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Customize</span></h5></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="tab-content">
                            <div class="tab-pane fade active in" id="tab1">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step1-custom.png" class="img-responsive" style="margin-top: 0; margin-bottom: 28px; display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1" style="margin-top: 0pc !important;">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">Cancellation Policy</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <p class="txt-theme">Select a Theme for your Calendar</p>
                                                        <p style="margin-top: -7px;">
                                                            <small class="text-muted">
                                                                We understand that unavoidable circumstances arise and appointments have to be cancelled or rescheduled. Remember, cancellation and/or rescheduling can be made {CancellationHour} prior to your appointment. Please call us at {yourphone} during our regular business hours to cancel or reschedule your appointment if you are unable to do this through our online booking system.
                                                            </small>
                                                        </p>
                                                        <p><b>Tip</b> Please use the following code to replace with your data</p>
                                                        <p><b>Your Phone</b> Replaces your phone number</p>
                                                        <p><b>Cancellation Hour</b> Replaces your cancellation hours</p>
                                                        <p><b>Mail</b> Replaces your email</p>
                                                        <textarea class="bkr_textinput form-control well hei textareacharacter_limit"  name="cancelation_policy" maxlength="2000"><?php if(Yii::$app->session->get('bk_cancelation_policy')){ echo Yii::$app->session->get('bk_cancelation_policy');} ?>  </textarea>
                                                        <span class="savedMess" style="display:none;">saved</span>

                                                        <p class="textarea_characters">2000 characters remaining</p></div>
                                                    </div>
                                  </div>

                                        </div>
                                        <div class="row btn-row" style="margin-top: 2pc; margin-bottom: 35px; margin-bottom: 0px;">
                                            <div class="col-lg-12">
                                                <button class="btn btn-main pull-right" style="padding: 11px 36px 13px;">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="tab2">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step2-custom.png" class="img-responsive" style="margin-top: 0; margin-bottom: 28px;display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1" style="margin-top: 0pc;">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">additional information</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <textarea class="bkr_textinput form-control well hei textareacharacter_limit"  name="customize_additional" maxlength="2000"><?php if(Yii::$app->session->get('bk_customize_additional')){ echo Yii::$app->session->get('bk_customize_additional');} ?>  </textarea>
                                                        <span class="savedMess" style="display:none;">saved</span>

                                                        <p class="textarea_characters">2000 characters remaining</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row btn-row" style="margin-top: 1pc; margin-bottom: 0px;">
                                            <div class="col-lg-12">
                                                <button class="btn btn-main pull-right" style="padding: 11px 36px 13px;">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                                </div>
                            <div class="tab-pane fade" id="tab3">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step3-custom.png" class="img-responsive" style="margin-top: 0; margin-bottom: 28px;display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1" style="margin-top: 0;">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">customize emails</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">



                                                        <?//= $form->field($model, 'email_content')->textInput(['maxlength' => true,'class' => 'form-control datepicker1'])->label('Block Dates') ?>

                                                        <p style="margin-top: -7px;">
                                                            <small class="text-muted">
                                                                Use {Smarty Tags} to replace tags with the value required from the database. e.g. to show first name use {FNAME}. You can get tags from the drop down in the editor.
                                                            </small>
                                                        </p>
                                                        <div class="row">
                                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"><p><b>1</b></p></div>
                                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                                                <p>
                                                                    <b>Booking Confirmation</b> <a onclick="event.preventDefault();" style="cursor: pointer;"  data-target="#customize_email1" data-toggle="collapse" class="txt-theme"><small>Edit</small></a>
                                                                </p>
                                                                <p style="margin-top: -10px;">This email will be sent to your customer after each confirmed booking. This email is triggered automatically.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row collapse" style="margin-top: 15px;" id="customize_email1">
                                                            <div class="col-sm-12">
                                                             <?php        $form = ActiveForm::begin(['action' => Yii::getAlias('@web').'/index.php/customize/update_customemail',
                                                                'enableAjaxValidation' => true, 'validationUrl' => Yii::getAlias('@web').'/index.php/customize/customize_emailvalidate','options' => ['class' => 'customize_email']]); ?>
                                                                <?= $form->field($booking_confirmation, 'email_type')->hiddenInput(['value'=>'booking_confirmation' ])->label(false) ?>
                                                                <?= $form->field($booking_confirmation, 'email_subject')->textInput(['maxlength' => true,'class' => 'form-control'])->label('Subject') ?>
                                                                <select onchange="insertAtCaret('booking_confirmation',this)" class="form-control" style="margin-bottom:12px" >
                                                                    <option value="smarty">Smarty Tags</option>
                                                                    <option value="{{business_name}}" >Business Name</option>
                                                                    <option value="{{businessphone}}" >Business Phone</option>
                                                                    <option value="{{businessemail}}" >Business Email</option>
                                                                    <option value="{{businessaddress}}" >Business Address</option>
                                                                    <option value="{{businessLink}}" >Business Link</option>
                                                                    <option value="{{additionalinformation}}" >Additional Information</option>
                                                                    <option value="{{fname}}" >First Name</option>
                                                                    <option value="{{lname}}" >Last Name</option>
                                                                    <option value="{{cancellationpolicy}}">Cancelattion Policy </option>
                                                                    <option value="{{AppointmentInfo}}">Appointment Info</option>
                                                                </select>
                                                                <?php
                                                                $booking_confirmation->email_content = preg_replace("/{{businesslogo}}/",Yii::getAlias('@web').'/'.$business_information->logo, $booking_confirmation->email_content);
                                                                ?>
                                                                <?= $form->field($booking_confirmation, 'email_content')->widget(CKEditor::className(), [
                                                                    'options' => ['rows' => 6,'id'=>'booking_confirmation'],

                                                                    'preset' => 'standard',
                                                                'clientOptions'=>[
                                                                    'enterMode' => 2,
                                                                    'forceEnterMode'=>false,
                                                                    'shiftEnterMode'=>1,
                                                                    'autoGrow_bottomSpace'=>0,
                                                                    'allowedContent'=>true,

                                                                ]
                                                            ])->label(false);
                                                               ?>
                                                                <?= Html::submitButton($booking_confirmation->isNewRecord ? 'Save' : 'Update', ['class' => $booking_confirmation->isNewRecord ? 'btn btn-main' : 'btn btn-main']) ?>
                                                            <?php ActiveForm::end(); ?>
                                                                </div>
                                                        </div>
                                                        <div class="row" style="margin-top: 15px;">
                                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"><p><b>2</b></p></div>
                                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                                                <p>
                                                                    <b>Waiting for Approval</b> <a onclick="event.preventDefault();" style="cursor: pointer;"  data-target="#customize_email2" data-toggle="collapse" class="txt-theme"><small>Edit</small></a>
                                                                </p>
                                                                <p style="margin-top: -10px;">This email will be sent to your customers who book appointments that need approval before final confirmation. This
                                                                    email is triggered automatically.</p>
                                                            </div>
                                                        </div>
                                                        <div class="row collapse" style="margin-top: 15px;" id="customize_email2">
                                                            <div class="col-sm-12">
                                                                <?php        $form = ActiveForm::begin(['action' => Yii::getAlias('@web').'/index.php/customize/update_customemail',
                                                                    'enableAjaxValidation' => true, 'validationUrl' => Yii::getAlias('@web').'/index.php/customize/customize_emailvalidate','options' => ['class' => 'customize_email']]); ?>
                                                                <?= $form->field($waiting_approval, 'email_type')->hiddenInput(['value'=>'waiting_approval'])->label(false) ?>
                                                                <?= $form->field($waiting_approval, 'email_subject')->textInput(['maxlength' => true,'class' => 'form-control'])->label('Subject') ?>
                                                                <select onchange="insertAtCaret('waiting_approval',this)" class="form-control" >
                                                                    <option value="smarty">Smarty Tags</option>
                                                                    <option value="{{business_name}}" >Business Name</option>
                                                                    <option value="{{businessphone}}" >Business Phone</option>
                                                                    <option value="{{businessemail}}" >Business Email</option>
                                                                    <option value="{{businessaddress}}" >Business Address</option>
                                                                    <option value="{{businessLink}}" >Business Link</option>
                                                                    <option value="{{additionalinformation}}" >Additional Information</option>
                                                                    <option value="{{fname}}" >First Name</option>
                                                                    <option value="{{lname}}" >Last Name</option>
                                                                    <option value="{{cancellationpolicy}}">Cancelattion Policy </option>
                                                                    <option value="{{AppointmentInfo}}">Appointment Info</option>
                                                                </select>
                                                                <?php
                                                                $waiting_approval->email_content = preg_replace("/{{businesslogo}}/",Yii::getAlias('@web').'/'.$business_information->logo, $waiting_approval->email_content);
                                                                ?>
                                                                <?= $form->field($waiting_approval, 'email_content')->widget(CKEditor::className(), [
                                                                    'options' => ['rows' => 6,'id'=>'waiting_approval'],

                                                                    'preset' => 'standard',
                                                                    'clientOptions'=>[
                                                                        'enterMode' => 2,
                                                                        'forceEnterMode'=>false,
                                                                        'shiftEnterMode'=>1,
                                                                        'autoGrow_bottomSpace'=>0,
                                                                        'allowedContent'=>true
                                                                    ]
                                                                ])->label(false);  ?>
                                                                <?= Html::submitButton($waiting_approval->isNewRecord ? 'Save' : 'Update', ['class' => $waiting_approval->isNewRecord ? 'btn btn-main' : 'btn btn-main']) ?>
                                                                <?php ActiveForm::end();  ?>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top: 15px;">
                                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"><p><b>3</b></p></div>
                                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                                                <p>
                                                                    <b>Waiting for Approval - Approved Appointment</b>  <a onclick="event.preventDefault();" style="cursor: pointer;"  data-target="#customize_email3" data-toggle="collapse" class="txt-theme"><small>Edit</small></a>
                                                                </p>
                                                                <p style="margin-top: -10px;"> This email will be sent to advise your customer after an administrator or staff member approves an appointment. This
                                                                    email is a follow-up notification to the "Waiting for Approval" email. It is not the same as the "Booking Confirmation".
                                                                    This email is triggered automatically.
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row collapse" style="margin-top: 15px;" id="customize_email3">
                                                            <div class="col-sm-12">
                                                                <?php        $form = ActiveForm::begin(['action' => Yii::getAlias('@web').'/index.php/customize/update_customemail',
                                                                    'enableAjaxValidation' => true, 'validationUrl' => Yii::getAlias('@web').'/index.php/customize/customize_emailvalidate','options' => ['class' => 'customize_email']]); ?>
                                                                <?= $form->field($approved_appointment, 'email_type')->hiddenInput(['value'=>'approved_appointment'])->label(false) ?>
                                                                <?= $form->field($approved_appointment, 'email_subject')->textInput(['maxlength' => true,'class' => 'form-control'])->label('Subject') ?>
                                                                <select onchange="insertAtCaret('approved_appointment',this)" class="form-control" >
                                                                    <option value="smarty">Smarty Tags</option>
                                                                    <option value="{{business_name}}" >Business Name</option>
                                                                    <option value="{{businessphone}}" >Business Phone</option>
                                                                    <option value="{{businessemail}}" >Business Email</option>
                                                                    <option value="{{businessaddress}}" >Business Address</option>
                                                                    <option value="{{businessLink}}" >Business Link</option>
                                                                    <option value="{{additionalinformation}}" >Additional Information</option>
                                                                    <option value="{{fname}}" >First Name</option>
                                                                    <option value="{{lname}}" >Last Name</option>
                                                                    <option value="{{cancellationpolicy}}">Cancelattion Policy </option>
                                                                    <option value="{{AppointmentInfo}}">Appointment Info</option>
                                                                </select>
                                                                <?php
                                                                $approved_appointment->email_content = preg_replace("/{{businesslogo}}/",Yii::getAlias('@web').'/'.$business_information->logo, $approved_appointment->email_content);
                                                                ?>
                                                                <?= $form->field($approved_appointment, 'email_content')->widget(CKEditor::className(), [
                                                                    'options' => ['rows' => 6,'id'=>'approved_appointment'],
                                                                    'preset' => 'standard',
                                                                    'clientOptions'=>[
                                                                        'enterMode' => 2,
                                                                        'forceEnterMode'=>false,
                                                                        'shiftEnterMode'=>1,
                                                                        'autoGrow_bottomSpace'=>0,
                                                                        'allowedContent'=>true
                                                                    ]
                                                                ])->label(false);   ?>
                                                                <?= Html::submitButton($approved_appointment->isNewRecord ? 'Save' : 'Update', ['class' => $approved_appointment->isNewRecord ? 'btn btn-main' : 'btn btn-main']) ?>
                                                                <?php ActiveForm::end(); ?>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top: 15px;">
                                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"><p><b>4</b></p></div>
                                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                                                <p>
                                                                    <b>Waiting for Approval - Appointment Denied</b>  <a onclick="event.preventDefault();" style="cursor: pointer;"  data-target="#customize_email4" data-toggle="collapse" class="txt-theme"><small>Edit</small></a>
                                                                </p>
                                                                <p style="margin-top: -10px;"> This email will be sent to advise your customer if an administrator or staff member denies an appointment. This email
                                                                    a a follow-up notification to the “Waiting for Approval” email. This email is triggered automatically.
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row collapse" style="margin-top: 15px;" id="customize_email4">
                                                            <div class="col-sm-12">
                                                                <?php        $form = ActiveForm::begin(['action' => Yii::getAlias('@web').'/index.php/customize/update_customemail',
                                                                    'enableAjaxValidation' => true, 'validationUrl' => Yii::getAlias('@web').'/index.php/customize/customize_emailvalidate','options' => ['class' => 'customize_email']]); ?>
                                                                <?= $form->field($appointment_denied, 'email_type')->hiddenInput(['value'=>'appointment_denied'])->label(false) ?>
                                                                <?= $form->field($appointment_denied, 'email_subject')->textInput(['maxlength' => true,'class' => 'form-control'])->label('Subject') ?>
                                                                <select onchange="insertAtCaret('approved_denied',this)" class="form-control" >
                                                                    <option value="smarty">Smarty Tags</option>
                                                                    <option value="{{business_name}}" >Business Name</option>
                                                                    <option value="{{businessphone}}" >Business Phone</option>
                                                                    <option value="{{businessemail}}" >Business Email</option>
                                                                    <option value="{{businessaddress}}" >Business Address</option>
                                                                    <option value="{{businessLink}}" >Business Link</option>
                                                                    <option value="{{additionalinformation}}" >Additional Information</option>
                                                                    <option value="{{fname}}" >First Name</option>
                                                                    <option value="{{lname}}" >Last Name</option>
                                                                    <option value="{{cancellationpolicy}}">Cancelattion Policy </option>
                                                                    <option value="{{AppointmentInfo}}">Appointment Info</option>
                                                                </select>
                                                                <?php
                                                                $appointment_denied->email_content = preg_replace("/{{businesslogo}}/",Yii::getAlias('@web').'/'.$business_information->logo, $appointment_denied->email_content);

                                                                ?>
                                                                <?= $form->field($appointment_denied, 'email_content')->widget(CKEditor::className(), [
                                                                    'options' => ['rows' => 6, 'id'=>'approved_denied'],
                                                                    'preset' => 'standard',
                                                                    'clientOptions'=>[
                                                                        'enterMode' => 2,
                                                                        'forceEnterMode'=>false,
                                                                        'shiftEnterMode'=>1,
                                                                        'autoGrow_bottomSpace'=>0,
                                                                        'allowedContent'=>true
                                                                    ]
                                                                ])->label(false);  ?>
                                                                <?= Html::submitButton($appointment_denied->isNewRecord ? 'Save' : 'Update', ['class' => $appointment_denied->isNewRecord ? 'btn btn-main' : 'btn btn-main']) ?>
                                                                <?php ActiveForm::end(); ?>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top: 15px;">
                                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"><p><b>5</b></p></div>
                                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                                                <p>
                                                                    <b>Appointment Rescheduled</b> <a onclick="event.preventDefault();" style="cursor: pointer;"  data-target="#customize_email5" data-toggle="collapse" class="txt-theme"><small>Edit</small></a>
                                                                </p>
                                                                <p style="margin-top: -10px;"> This email will be sent to alert your customer if an administrator or staff member reschedules an appointment.
                                                                    This email is triggered automatically.
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row collapse" style="margin-top: 15px;" id="customize_email5">
                                                            <div class="col-sm-12">
                                                                <?php        $form = ActiveForm::begin(['action' => Yii::getAlias('@web').'/index.php/customize/update_customemail',
                                                                    'enableAjaxValidation' => true, 'validationUrl' => Yii::getAlias('@web').'/index.php/customize/customize_emailvalidate','options' => ['class' => 'customize_email']]); ?>
                                                                <?= $form->field($appointment_reschedule, 'email_type')->hiddenInput(['value'=>'appointment_reschedule'])->label(false) ?>
                                                                <?= $form->field($appointment_reschedule, 'email_subject')->textInput(['maxlength' => true,'class' => 'form-control'])->label('Subject') ?>
                                                                <select onchange="insertAtCaret('appointment_reschedule',this)" class="form-control" style="margin-bottom:12px" >
                                                                    <option value="smarty">Smarty Tags</option>
                                                                    <option value="{{business_name}}" >Business Name</option>
                                                                    <option value="{{businessphone}}" >Business Phone</option>
                                                                    <option value="{{businessemail}}" >Business Email</option>
                                                                    <option value="{{businessaddress}}" >Business Address</option>
                                                                    <option value="{{businessLink}}" >Business Link</option>
                                                                    <option value="{{additionalinformation}}" >Additional Information</option>
                                                                    <option value="{{fname}}" >First Name</option>
                                                                    <option value="{{lname}}" >Last Name</option>
                                                                    <option value="{{cancellationpolicy}}">Cancelattion Policy </option>
                                                                    <option value="{{AppointmentInfo}}">Appointment Info</option>
                                                                </select>

                                                             <?php
                                                                $appointment_reschedule->email_content = preg_replace("/{{businesslogo}}/",Yii::getAlias('@web').'/'.$business_information->logo, $appointment_reschedule->email_content);
                                                             ?>
                                                                <?= $form->field($appointment_reschedule, 'email_content')->widget(CKEditor::className(), [
                                                                    'options' => ['rows' => 6, 'id'=>'appointment_reschedule'],
                                                                    'preset' => 'standard',
                                                                    'clientOptions'=>[
                                                                        'enterMode' => 2,
                                                                        'forceEnterMode'=>false,
                                                                        'shiftEnterMode'=>1,
                                                                        'autoGrow_bottomSpace'=>0,
                                                                        'allowedContent'=>true
                                                                    ]
                                                                ])->label(false);  ?>
                                                                <?= Html::submitButton($appointment_reschedule->isNewRecord ? 'Save' : 'Update', ['class' => $appointment_reschedule->isNewRecord ? 'btn btn-main' : 'btn btn-main']) ?>
                                                                <?php ActiveForm::end(); ?>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top: 15px;">
                                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"><p><b>6</b></p></div>
                                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                                                <p>
                                                                    <b>Appointment Cancellation</b> <a onclick="event.preventDefault();" style="cursor: pointer;"  data-target="#customize_email6" data-toggle="collapse" class="txt-theme"><small>Edit</small></a>
                                                                </p>
                                                                <p style="margin-top: -10px;"> This email will be sent to alert a customer if an administrator or staff member cancels an appointment. This email
                                                                    is triggered automatically.
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row collapse" style="margin-top: 15px;" id="customize_email6">
                                                            <div class="col-sm-12">
                                                                <?php        $form = ActiveForm::begin(['action' => Yii::getAlias('@web').'/index.php/customize/update_customemail',
                                                                    'enableAjaxValidation' => true, 'validationUrl' => Yii::getAlias('@web').'/index.php/customize/customize_emailvalidate','options' => ['class' => 'customize_email']]); ?>
                                                                <?= $form->field($appointment_cancel, 'email_type')->hiddenInput(['value'=>'appointment_cancel'])->label(false) ?>
                                                                <?= $form->field($appointment_cancel, 'email_subject')->textInput(['maxlength' => true,'class' => 'form-control'])->label('Subject') ?>
                                                                <select onchange="insertAtCaret('appointment_cancel',this)" class="form-control" >
                                                                    <option value="smarty">Smarty Tags</option>
                                                                    <option value="{{business_name}}" >Business Name</option>
                                                                    <option value="{{businessphone}}" >Business Phone</option>
                                                                    <option value="{{businessemail}}" >Business Email</option>
                                                                    <option value="{{businessaddress}}" >Business Address</option>
                                                                    <option value="{{businessLink}}" >Business Link</option>
                                                                    <option value="{{additionalinformation}}" >Additional Information</option>
                                                                    <option value="{{fname}}" >First Name</option>
                                                                    <option value="{{lname}}" >Last Name</option>
                                                                    <option value="{{cancellationpolicy}}">Cancelattion Policy </option>
                                                                    <option value="{{AppointmentInfo}}">Appointment Info</option>
                                                                </select>
                                                                <?php
                                                                $appointment_cancel->email_content = preg_replace("/{{businesslogo}}/",Yii::getAlias('@web').'/'.$business_information->logo, $appointment_cancel->email_content);

                                                                ?>
                                                                <?= $form->field($appointment_cancel, 'email_content')->widget(CKEditor::className(), [
                                                                    'options' => ['rows' => 6, 'id'=>'appointment_cancel'],
                                                                    'preset' => 'standard',
                                                                    'clientOptions'=>[
                                                                        'enterMode' => 2,
                                                                        'forceEnterMode'=>false,
                                                                        'shiftEnterMode'=>1,
                                                                        'autoGrow_bottomSpace'=>0,
                                                                        'allowedContent'=>true
                                                                    ]
                                                                ])->label(false);  ?>
                                                                <?= Html::submitButton($appointment_cancel->isNewRecord ? 'Save' : 'Update', ['class' => $appointment_cancel->isNewRecord ? 'btn btn-main' : 'btn btn-main']) ?>
                                                                <?php ActiveForm::end(); ?>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top: 15px;">
                                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"><p><b>7</b></p></div>
                                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                                                <p>
                                                                    <b>New User Registration by Administrator / Staff</b> <a onclick="event.preventDefault();" style="cursor: pointer;"  data-target="#customize_email7" data-toggle="collapse" class="txt-theme"><small>Edit</small></a>
                                                                </p>
                                                                <p style="margin-top: -10px;"> This email will be sent to new customers added by an administrator or a staff member and provides the customer
                                                                    with login details. This email is triggered automatically.
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row collapse" style="margin-top: 15px;" id="customize_email7">
                                                            <div class="col-sm-12">
                                                                <?php        $form = ActiveForm::begin(['action' => Yii::getAlias('@web').'/index.php/customize/update_customemail',
                                                                    'enableAjaxValidation' => true, 'validationUrl' => Yii::getAlias('@web').'/index.php/customize/customize_emailvalidate','options' => ['class' => 'customize_email']]); ?>
                                                                <?= $form->field($new_registration, 'email_type')->hiddenInput(['value'=>'new_registration'])->label(false) ?>
                                                                <?= $form->field($new_registration, 'email_subject')->textInput(['maxlength' => true,'class' => 'form-control'])->label('Subject') ?>
                                                                <select onchange="insertAtCaret('new_registration',this)" class="form-control" >
                                                                    <option value="smarty">Smarty Tags</option>
                                                                    <option value="{{business_name}}" >Business Name</option>
                                                                    <option value="{{businessphone}}" >Business Phone</option>
                                                                    <option value="{{businessemail}}" >Business Email</option>
                                                                    <option value="{{businessaddress}}" >Business Address</option>
                                                                    <option value="{{businessLink}}" >Business Link</option>
                                                                    <option value="{{additionalinformation}}" >Additional Information</option>
                                                                    <option value="{{fname}}" >First Name</option>
                                                                    <option value="{{lname}}" >Last Name</option>
                                                                    <option value="{{cancellationpolicy}}">Cancelattion Policy </option>
                                                                    <option value="{{AppointmentInfo}}">Appointment Info</option>
                                                                </select>
                                                                <?php
                                                                $new_registration->email_content = preg_replace("/{{businesslogo}}/",Yii::getAlias('@web').'/'.$business_information->logo, $new_registration->email_content);

                                                                ?>
                                                                <?= $form->field($new_registration, 'email_content')->widget(CKEditor::className(), [
                                                                    'options' => ['rows' => 6, 'id'=>'new_registration'],
                                                                    'preset' => 'standard',
                                                                    'clientOptions'=>[
                                                                        'enterMode' => 2,
                                                                        'forceEnterMode'=>false,
                                                                        'shiftEnterMode'=>1,
                                                                        'autoGrow_bottomSpace'=>0,
                                                                        'allowedContent'=>true
                                                                    ]
                                                                ])->label(false);  ?>
                                                                <?= Html::submitButton($new_registration->isNewRecord ? 'Save' : 'Update', ['class' => $new_registration->isNewRecord ? 'btn btn-main' : 'btn btn-main']) ?>
                                                                <?php ActiveForm::end(); ?>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top: 15px;">
                                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"><p><b>8</b></p></div>
                                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                                                <p>
                                                                    <b>Reminder Alert Prior to Appointment</b> <a onclick="event.preventDefault();" style="cursor: pointer;"  data-target="#customize_email8" data-toggle="collapse" class="txt-theme"><small>Edit</small></a>
                                                                </p>
                                                                <p style="margin-top: -10px;">This email will be sent to customers prior to their appointment. Sending an email alert prior to appointments helps to
                                                                    reduce customer no-shows. This email is triggered automatically.
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row collapse" style="margin-top: 15px;" id="customize_email8">
                                                            <div class="col-sm-12">
                                                                <?php        $form = ActiveForm::begin(['action' => Yii::getAlias('@web').'/index.php/customize/update_customemail',
                                                                    'enableAjaxValidation' => true, 'validationUrl' => Yii::getAlias('@web').'/index.php/customize/customize_emailvalidate','options' => ['class' => 'customize_email']]); ?>
                                                                <?= $form->field($reminder, 'email_type')->hiddenInput(['value'=>'reminder'])->label(false) ?>
                                                                <?= $form->field($reminder, 'email_subject')->textInput(['maxlength' => true,'class' => 'form-control'])->label('Subject') ?>
                                                                <select onchange="insertAtCaret('reminder',this)" class="form-control" >
                                                                    <option value="smarty">Smarty Tags</option>
                                                                    <option value="{{business_name}}" >Business Name</option>
                                                                    <option value="{{businessphone}}" >Business Phone</option>
                                                                    <option value="{{businessemail}}" >Business Email</option>
                                                                    <option value="{{businessaddress}}" >Business Address</option>
                                                                    <option value="{{businessLink}}" >Business Link</option>
                                                                    <option value="{{additionalinformation}}" >Additional Information</option>
                                                                    <option value="{{fname}}" >First Name</option>
                                                                    <option value="{{lname}}" >Last Name</option>
                                                                    <option value="{{cancellationpolicy}}">Cancelattion Policy </option>
                                                                    <option value="{{AppointmentInfo}}">Appointment Info</option>
                                                                </select>
                                                                <?php
                                                                $reminder->email_content = preg_replace("/{{businesslogo}}/",Yii::getAlias('@web').'/'.$business_information->logo, $reminder->email_content);

                                                                ?>
                                                                <?= $form->field($reminder, 'email_content')->widget(CKEditor::className(), [
                                                                    'options' => ['rows' => 6, 'id'=>'reminder'],
                                                                    'preset' => 'standard',
                                                                    'clientOptions'=>[
                                                                        'enterMode' => 2,
                                                                        'forceEnterMode'=>false,
                                                                        'shiftEnterMode'=>1,
                                                                        'autoGrow_bottomSpace'=>0,
                                                                        'allowedContent'=>true
                                                                    ]
                                                                ])->label(false);  ?>
                                                                <?= Html::submitButton($reminder->isNewRecord ? 'Save' : 'Update', ['class' => $reminder->isNewRecord ? 'btn btn-main' : 'btn btn-main']) ?>
                                                                <?php ActiveForm::end(); ?>
                                                            </div>
                                                        </div>
                                                        <div class="row" style="margin-top: 15px;">
                                                            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1"><p><b>9</b></p></div>
                                                            <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11">
                                                                <p>
                                                                    <b>Thank You</b> <a onclick="event.preventDefault();" style="cursor: pointer;"  data-target="#customize_email9" data-toggle="collapse" class="txt-theme"><small>Edit</small></a>
                                                                </p>
                                                                <p style="margin-top: -10px;">This email will be sent to customers after their appointment asking for their feedback. A customer can leave a bad,
                                                                    good or excellent rating with comments. This email is triggered manually.
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row collapse" style="margin-top: 15px;" id="customize_email9">
                                                            <div class="col-sm-12">

                                                                <?php        $form = ActiveForm::begin(['action' => Yii::getAlias('@web').'/index.php/customize/update_customemail',
                                                                    'enableAjaxValidation' => true, 'validationUrl' => Yii::getAlias('@web').'/index.php/customize/customize_email/validate','options' => ['class' => 'customize_email']]); ?>
                                                                <?= $form->field($thank_you, 'email_type')->hiddenInput(['value'=>'thank_you'])->label(false) ?>
                                                                <?= $form->field($thank_you, 'email_subject')->textInput(['maxlength' => true,'class' => 'form-control'])->label('Subject') ?>


                                                                <select onchange="insertAtCaret('thank_you',this)" class="form-control" >
                                                                    <option value="smarty">Smarty Tags</option>
                                                                    <option value="{{business_name}}" >Business Name</option>
                                                                    <option value="{{businessphone}}" >Business Phone</option>
                                                                    <option value="{{businessemail}}" >Business Email</option>
                                                                    <option value="{{businessaddress}}" >Business Address</option>
                                                                    <option value="{{businessLink}}" >Business Link</option>
                                                                    <option value="{{additionalinformation}}" >Additional Information</option>
                                                                    <option value="{{fname}}" >First Name</option>
                                                                    <option value="{{lname}}" >Last Name</option>
                                                                    <option value="{{cancellationpolicy}}">Cancelattion Policy </option>
                                                                    <option value="{{AppointmentInfo}}">Appointment Info</option>
                                                                </select>
                                                                <?= $form->field($thank_you, 'email_content')->widget(CKEditor::className(), [
                                                                    'options' => ['rows' => 6, 'id'=>'thank_you'],
                                                                    'preset' => 'standard',
                                                                    'clientOptions'=>[
                                                                        'enterMode' => 2,
                                                                        'forceEnterMode'=>false,
                                                                        'shiftEnterMode'=>1,
                                                                        'autoGrow_bottomSpace'=>0,
                                                                        'allowedContent'=>true
                                                                    ]
                                                                ])->label(false);  ?>
                                                                <?= Html::submitButton($thank_you->isNewRecord ? 'Save' : 'Update', ['class' => $thank_you->isNewRecord ? 'btn btn-main' : 'btn btn-main']) ?>
                                                                <?php ActiveForm::end(); ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row btn-row" style="margin-top: 1pc; margin-bottom: 0;">
                                            <div class="col-lg-12">
                                                <button class="btn btn-main pull-right" style="padding: 11px 36px 13px;">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab4">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step4-custom.png" class="img-responsive" style="margin-top: 0; margin-bottom: 28px;display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1" style="margin-top: 0;">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">Customize Form</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <p class="txt-theme">Customize Form</p>
                                                        <p style="margin-top: -7px;">
                                                            <small class="text-muted">
                                                                Create a customized form that the client will see at the time of making a booking. For example a dog grooming company can create fields asking for the dog's name, breed, weight etc when the appointment is booked.
                                                            </small>
                                                        </p>
                                                        <label class="checkbox" style="margin-left: 21px;">
                                                            <input type="checkbox"> Check this box if you require this aditional information with every appointment that is booked.
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row btn-row" style="margin-top: 1pc; margin-bottom: 0;">
                                            <div class="col-lg-12">
                                                <button class="btn btn-main pull-right" style="padding: 11px 36px 13px;">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab5">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step5-custom.png" class="img-responsive" style="margin-top: 0; margin-bottom: 28px;display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1" style="margin-top: 0;">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">Add - On</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <p class="txt-theme"><a href="">New Add-on</a></p>
                                                        <div class="addon-div">
                                                            <ul class="list-inline list-addon">
                                                                <li class="col-lg-3 col-md-3 col-sm-3 col-xs-6 text-center">Add-on Name</li>
                                                                <li class="col-lg-3 col-md-3 col-sm-3 col-xs-6 text-center">Depend On</li>
                                                                <li class="col-lg-3 col-md-3 col-sm-3 col-xs-6 text-center">Field Type</li>
                                                                <li class="col-lg-3 col-md-3 col-sm-3 col-xs-6 text-center">On Each</li>
                                                            </ul>
                                                        </div>
                                                        <p>
                                                            <small class="text-muted">
                                                                The Add-on option allows you to sell products, additional services or special offers at the time of check-out. Each service can have its own add-on. An example of and Add-on could be, a tour company can ask customers to include meal or a Salon can sell a product.
                                                            </small>
                                                        </p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row btn-row" style="margin-top: 1pc; margin-bottom: 0;">
                                            <div class="col-lg-12">
                                                <button class="btn btn-main pull-right" style="padding: 11px 36px 13px;">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="tab6">
                                <div class="panel-collapse collapse in active">
                                    <div class="panel-body" style="border: none;">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <img src="<?= Yii::getAlias('@web') ?>/img/step6-custom.png" class="img-responsive" style="margin-top: 0; margin-bottom: 28px;display: block; margin-left: auto; margin-right: auto;">
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1" style="margin-top: 0;">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">Calendar</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <p class="txt-theme">Set calendar background image</p>
                                                        <p><small class="text-muted">By default, the Appointy Logo is displayed as the background image of your calendar. You can add a different image by writing the full image path in the text box below. (e.g. http://www.boocked.com/images/logo.gif) </small></p>
                                                        <div class="form-group">
                                                            <input type="text" style="background-color: #f2f2f2;" class="form-control" placeholder="Enter text here..">
                                                        </div>
                                                        <p style="margin-top: -10px;"><small class="text-muted">eg: http://abc.com/imagenname.png</small></p>
                                                        <hr>
                                                        <p class="txt-theme">URL of calendar on your site</p>
                                                        <p><small class="text-muted">If you are integrating Appointy on your site please enter the URL of that page in the text box below. We will use this URL to redirect users to your desired page after payments, email verifications, ratings, etc. Leave empty if you are not integrating your calendar on your website. By default, users will be redirected to http://abc.boocked.com </small></p>
                                                        <div class="form-group">
                                                            <input type="text" style="background-color: #f2f2f2;" class="form-control" placeholder="Enter text here..">
                                                        </div>
                                                        <p style="margin-top: -10px;"><small class="text-muted">eg: http://www.inglesenmadrid.com/pride-hora/</small></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default panel-rules1" style="margin-top: 0;">
                                            <div class="panel-heading">
                                                <ul class="list-inline list-staff">
                                                    <li><h5 class="panel-title text-uppercase"><span class="staff-head">Calendar</span></h5></li>
                                                </ul>
                                            </div>
                                            <div class="panel-body">
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <p class="txt-theme">Your social channels.</p>
                                                        <div class="form-group">
                                                            <input type="text" style="background-color: #f2f2f2;" class="form-control" placeholder="Facebook Page URL">
                                                            <i class="fa fa-facebook" style="float: right; position: relative; right: 1px; top: -30px; color: rgb(58, 82, 130); background: #F2F2F2; padding: 6px;"></i>
                                                        </div>
                                                        <p style="margin-top: -10px;"><small class="text-muted">eg: http://abc.com/imagenname.png</small></p>
                                                        <hr>
                                                        <div class="form-group">
                                                            <input type="text" style="background-color: #f2f2f2;" class="form-control" placeholder="Twitter Page URL">
                                                            <i class="fa fa-twitter" style="float: right; position: relative; right: 1px; top: -30px; color: #3c9fcb; background: #F2F2F2; padding: 6px;"></i>
                                                        </div>
                                                        <p style="margin-top: -10px;"><small class="text-muted">eg: http://www.inglesenmadrid.com/pride-hora/</small></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row btn-row" style="margin-top: 1pc; margin-bottom: 0px;">
                                            <div class="col-lg-12">
                                                <button class="btn btn-main pull-right" style="padding: 11px 36px 13px;">Continue</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-customer-details">
            <div class="modal-dialog modal-dialog-customer">
                <div class="modal-content">
                    <div class="modal-header">
                        <ul class="list-inline list-modal-header">
                            <li class="text-uppercase"><i class="fa fa-user" style="color: #FC7600;"></i> customer</li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                                <a href=""><i class="fa fa-reply"></i></a>
                            </li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                                <a href=""><i class="fa fa-plus"></i></a>
                            </li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban" id="ban-btn-toggle"><i class="fa fa-ban"></i></a></li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay" id="pay-btn-toggle"><i class="fa fa-dollar"></i></a></li>
                            <li class="pull-right" style="font-size: 12px;">Gift Certificates / Discount Coupons</li>
                        </ul>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body-inner-contents">
                            <div class="row">
                                <div class="col-lg-7 col-md-7 col-sm-7">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <i class="fa fa-plus upload-image" data-toggle="tooltip" data-placement="top" title="Upload Image"></i>
                                            <img src="<?= Yii::getAlias('@web') ?>/img/staff2.png">
                                        </div>
                                        <div class="col-lg-8">
                                            <h4 class="txt-theme" style="position: relative; top: 9px;">John Doe</h4>
                                            <p><small>Johndoe@gmail.com</small></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <ul class="list-inline" style="margin-top: 15px;">
                                                <li><small><a href="" class="txt-theme">Invite to schedule online</a></small></li><li class="txt-theme"><small>|</small></li>
                                                <li><small><a href="" class="txt-theme">Verify</a></small></li><li class="txt-theme"><small>|</small></li>
                                                <li><small><a href="" class="txt-theme">Edit</a></small></li><li class="txt-theme"><small>|</small></li>
                                                <li><small><a href="" class="txt-theme">Delete</a></small></li>
                                            </ul>
                                            <button class="btn btn-main" style="padding: 4px 9px;"><i class="fa fa-plus"></i> Add Tags</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 media-col6">
                                    <div class="row">
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                            <i class="fa fa-map-marker txt-theme"></i>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                            <p><small>145 strait street, CT.No City, No Region United States(01234)</small></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                            <i class="fa fa-mobile txt-theme"></i>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                            <p><small>098-879-46548</small></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12" style="margin-top: 8px;"><button class="btn btn-main" style="padding: 3px 4px;"><small>Send Gift Certificates</small></button></div>
                                        <div class="col-lg-12" style="margin-top: 5px;"><button class="btn btn-main" style="padding: 3px 4px;"><small>Send Discount Coupons</small></button></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px;">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="2" placeholder="Add Information"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="modal-body-inner-tabs">
                                <ul class="nav nav-tabs nav-main nav-customer-details">
                                    <li class="active">
                                        <a href="#up-app" data-toggle="tab">Upcoming Appointments</a>
                                    </li>
                                    <li>
                                        <a href="#past-app" data-toggle="tab">Past Appointments</a>
                                    </li>
                                    <li>
                                        <a href="#payments" data-toggle="tab">Payments</a>
                                    </li>
                                    <li>
                                        <a href="#promotion" data-toggle="tab">Promotion</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="up-app">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="past-app">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="payments">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="promotion">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="ban">
            <div class="modal-dialog modal-dialog-customer">
                <div class="modal-content">
                    <div class="modal-header">
                        <ul class="list-inline list-modal-header">
                            <li class="text-uppercase"><i class="fa fa-user" style="color: #FC7600;"></i> customer booking restrictions</li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                                <a href=""><i class="fa fa-reply"></i></a>
                            </li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                                <a href=""><i class="fa fa-plus"></i></a>
                            </li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban"><i class="fa fa-ban"></i></a></li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay"><i class="fa fa-dollar"></i></a></li>
                        </ul>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body-inner-contents">
                            <div class="alert alert-warning">
                                <i class="fa fa-warning"></i> <small>Client Advance setting is a premium feature available with PRO and above packages. <a href="#" class="txt-theme">Upgrade your account </a> or <a href="" class="txt-theme">Compare various Packages</a></small>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>This Customer is</p>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option selected>Post-Paying</option>
                                            <option>Post-Paying</option>
                                            <option>Post-Paying</option>
                                        </select>
                                    </div>
                                    <p class="text-muted">
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                        </small>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>Future Bookings Status</p>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option selected>As per settings</option>
                                            <option>Post-Paying</option>
                                            <option>Post-Paying</option>
                                        </select>
                                    </div>
                                    <p class="text-muted">
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                        </small>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>With Restriction</p>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option selected>As per settings</option>
                                            <option>Post-Paying</option>
                                            <option>Post-Paying</option>
                                        </select>
                                    </div>
                                    <p class="text-muted">
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                        </small>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="list-inline pull-right">
                                        <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;">Cancel</button></li>
                                        <li><button class="btn btn-main" style="padding-right: 19px; padding-left: 19px;">Save</button></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="pay">
            <div class="modal-dialog modal-dialog-customer">
                <div class="modal-content">
                    <div class="modal-header">
                        <ul class="list-inline list-modal-header">
                            <li class="text-uppercase"><i class="fa fa-dollar" style="color: #FC7600;"></i> add membership payment</li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                                <a href=""><i class="fa fa-reply"></i></a>
                            </li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                                <a href=""><i class="fa fa-plus"></i></a>
                            </li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban"><i class="fa fa-ban"></i></a></li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay"><i class="fa fa-dollar"></i></a></li>
                        </ul>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6 col-md-6">
                                <p>Payment Date</p>
                                <div class="form-group">
                                    <input type="text" placeholder="4-17-2016" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6">
                                <p>Payment For</p>
                                <div class="form-group">
                                    <input type="text" placeholder="Add New Payment" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="text-muted pull-right" style="margin-top: -9px;"><small>Customer restrictions of the selected membership will be applied automatically.</small></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <textarea class="form-control" rows="2" placeholder="Note against payment"></textarea>
                                    <p class="text-danger pull-right"><small>Please do not enter html tags</small></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="modal-body-inner-tabs">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <p class="txt-theme">Amount</p>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Additional Charges">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <span style="position: absolute; left: -2px;">$</span> <input type="text" class="form-control" placeholder="$$$">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Discount">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <span style="position: absolute; left: -6px;">by</span>
                                            <select class="form-control" placeholder="Cash">
                                                <option selected="">Cash</option>
                                                <option>d</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Total">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <ul class="list-inline pull-right">
                                            <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;">Cancel</button></li>
                                            <li><button class="btn btn-main" style="padding-right: 19px; padding-left: 19px;">Save</button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

     </div>
 <?php $script="   
    $('body').on('beforeSubmit', 'form.customize_email', function () {
    var form = $(this);

    if (form.find('.has-error').length) {
    return false;
    }
    $.ajax({
    url: form.attr('action'),
    type: 'post',
    data: form.serialize(),
    success: function (response) {
    if(response==1){
    alert('changed');
    $.pjax.reload({container:'#blockdatescont'});

    }else {
    alert('not updated');

    }
    }
    });
    return false
    });
$('.bkr_textinput').change(function(){

var savedmess = $(this).siblings('.savedMess');
   var optionval =  $(this).val();
   var selectbox_name =  $(this).attr('name');
   var csrf_name = $('booking_csrf').attr('name');
   var csrf_val = $('booking_csrf').val();
$.ajax({
url: '". Yii::getAlias('@web')."/index.php/booking-rules/updaterulessetting',
type: 'post',
data:{csrf_name:csrf_val,
'SettingBookingRules[bkrules_name]':selectbox_name,
'SettingBookingRules[bkrules_value]':optionval},
success: function (response) {
    if(response==1){
    
 savedmess.css('display','inline');
     setTimeout(function(){
 
    savedmess.hide('slow');
}, 2000);
    }else {
        alert('not updated');

    }
}
});
});
});

 var text_max = 2000;
    $('.textarea_characters').html(text_max + ' characters remaining');

    $('.textareacharacter_limit').keyup(function() {
        var text_length = $(this).val().length;
        var text_remaining = text_max - text_length;

        $(this).siblings('.textarea_characters').html(text_remaining + ' characters remaining');

    "

    ;

    $this->registerJs($script); ?>
    <script>
        function insertAtCaret(areaId,xor) {
         //   alert(areaId);
            var optionval =  $('option:selected',xor).val();
            if(optionval!='smarty') {
                console.log(optionval);


                CKEDITOR.instances[areaId].insertText(optionval);
            }

        }
    </script>
