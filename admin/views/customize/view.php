<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CustomizeEmail */

$this->title = $model->custom_email_id;
$this->params['breadcrumbs'][] = ['label' => 'Customize Emails', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customize-email-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->custom_email_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->custom_email_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'custom_email_id:email',
            'email_subject:email',
            'email_content:ntext',
            'emai_type',
            'business_id',
        ],
    ]) ?>

</div>
