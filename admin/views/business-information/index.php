<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap\Modal;
use app\models\TimezonesList;

/* @var $model app\models\BusinessInformation */
/* @var $form yii\widgets\ActiveForm */

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


// with UI

//use dosamigos\fileupload\FileUpload;;
?>


<?
$this->title = 'Business Informations';


//->yii\db\BaseActiveRecord:private
//$timezoneCurrency[0]
//foreach($timezoneCurrency[0] as $timezone){

   // echo $timezone->currencyformat;

//}
//
// ?>
<?php  $timezone_list =  TimezonesList::find()->all(); ?>

<div class="col-lg-3 col-md-3 col-sm-3 no-padding-l media-xs-full">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ul class="list-inline list-staff">
                <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/settings.png"> <span class="staff-head">Additional Setting</span></h5></li>
            </ul>
        </div>
        <div class="panel-body panel-body-nav-tabs-sidebar">
            <div class="row">
                <ul class="nav nav-stacked nav-tabs nav-tabs-sidebar">
                    <li class="active"><a href="#preference" data-toggle="tab" >2 Preferences</a></li>
                    <li><a href="#bhours" class="last" data-toggle="tab">3 Business Hours</a></li>
                </ul>
            </div>
        </div>
    </div>
</div>
<div class="col-lg-9 col-md-9 col-sm-9 media-xs-full">
    <div class="panel panel-default">
        <div class="panel-heading">
            <ul class="list-inline list-staff">
                <li><h5 class="panel-title text-uppercase"><i class="fa fa-briefcase" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Business Details</span></h5></li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="preference">
                        <div class="panel-collapse collapse in active">
                            <div class="col-lg-6">
                                <div class="panel-body" style="border: none;">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <ul class="list-inline list-staff">
                                                <li><h5 class="panel-title text-uppercase"><i class="fa fa-phone-square" style="color: #FC7600; position: relative; top: 3px;"></i>
                                                        <span class="staff-head">Business Images</span></h5></li>
                                            </ul>
                                        </div>
                                        <div class="panel-body">



                                                <?php /*
                                                // File and new size
                                                $filename = 'http://localhost/boocked/customer/web/images/banner.jpg';
                                                $percent = 0.5;

                                                // Content type
                                                header('Content-Type: image/jpeg');

                                                // Get new sizes
                                                list($width, $height) = getimagesize($filename);
                                                $newwidth = $width+300;
                                                $newheight = $height+300;

                                                // Load
                                                $thumb = imagecreatetruecolor($newwidth, $newheight);
                                                $source = imagecreatefromjpeg($filename);

                                                // Resize
                                                imagecopyresized($thumb, $source, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);

                                                // Output
                                                print_r($source);
                                                print_r(imagejpeg($thumb));   */
                                                ?>


                                                <div class="row">
                                                    <div class="col-sm-12">
                                                        <p style="margin-bottom:0">Business Logo</p>
                                                        <p><small class="text-muted">Logo will be shown on customer booking page.</small></p>
                                                        <div class="form-group">
                                                            <form action="<?= Yii::getAlias('@web')?>/index.php/business-information/updatelogo/" enctype="multipart/form-data" id="updatelogo">
                                                            <?  ?>
                                                            <div style="display:none;">
                                                            <input type="file" id="business_logo_upload" name="business_logo">
                                                            </div>
                                                                <?php if($business_information->logo!=""){ ?>
                                                                <img src="<?= Yii::getAlias('@web').'/'.$business_information->logo ?>" id="business_logo_img" style="width:150px">
                                                                <a href="#" id="business_logo_click">Change</a> <a href="logo" class="business_logo_delete">Delete</a>
                                                                <?php }else { ?>
                                                                <img src="<?= Yii::getAlias('@web')?>/img/no-image.png" id="business_logo_img" style="width:150px">
                                                                <a href="#" id="business_logo_click">Change</a>
                                                                <?php   } ?>
                                                            </form>

                                                        </div>

                                                        <p style="margin-bottom:0">Cover For About</p>
                                                        <p><small class="text-muted">These images will be visible on the "About us" section of your customer booking page. </small></p>
                                                        <div class="form-group">
                                                            <?  ?>
                                                            <form action="<?= Yii::getAlias('@web')?>/index.php/business-information/updateaboutimage/" enctype="multipart/form-data" id="updateaboutimage">
                                                            <div style="display:none;">
                                                                <input type="file" id="business_about_upload" name="business_about_image">
                                                            </div>
                                                            <?php if($business_information->about_image!=""){ ?>
                                                                <img src="<?= Yii::getAlias('@web').'/'.$business_information->about_image ?>" id="business_about_img" style="width:150px">
                                                                <a href="#" id="business_about_click">Change</a> <a href="cover" class="business_logo_delete">Delete</a>
                                                            <?php }else { ?>
                                                                <img src="<?= Yii::getAlias('@web')?>/img/no-image.png" id="business_about_img" style="width:150px">
                                                                <a href="#" id="business_about_click">Change</a>
                                                            <?php   } ?>
                                                                </form>

                                                        </div>


                                                    </div>

                                                </div>


                                            <?php // } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="panel-body" style="border: none;">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <ul class="list-inline list-staff">
                                               <li><h5 class="panel-title text-uppercase"><i class="fa fa-dollar" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Business Information</span></h5></li>
                                            </ul>
                                        </div>
                                        <?php Pjax::begin(); ?>
                                        <div class="panel-body" style="padding-bottom: 25px;" id="business_informationform">
                                         

                                            <?php $form = ActiveForm::begin(['action'=>Yii::getAlias('@web').'/index.php/business-information/update'] ); ?>
                                            <div class="row">
                                                <div class="col-lg-12 col-md-12 col-sm-12">
                                                    <?= $form->field($model, 'business_name')->textInput(['maxlength' => true]) ?>
                                                </div>

                                            </div>

                                            <?= $form->field($model, 'business_description')->textarea(['rows' => 4]) ?>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group field-businessinformation-country required has-success">
                                                        <label for="businessinformation-country" class="control-label">Country</label>
                                                        <select name="BusinessInformation[country]" class="form-control" id="businessinformation-country">
                                                        <?php foreach ($countries as $country){ ?>
                                                            <option <?php if($model->country==$country->id){ echo 'selected'; }?> value="<?= $country->id ?>">
                                                                <?= $country->name ?>
                                                            </option>
                                                            <?php } ?>
                                                        </select>

                                                        <div class="help-block"></div>
                                                    </div>
                                                    <? //= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <div class="form-group field-businessinformation-state ">
                                                        <label for="businessinformation-state" class="control-label">State</label>
                                                        <select name="BusinessInformation[state]" class="form-control" id="businessinformation-state">
                                                            <?php foreach ($states as $state){ ?>
                                                                <option <?php if($model->state==$state->id){ echo 'selected'; }?> value="<?= $state->id ?>">
                                                                    <?= $state->name ?>
                                                                </option>
                                                            <?php } ?>
                                                        </select>

                                                        <div class="help-block"></div>
                                                    </div>

                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <? //= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
                                                    <div class="form-group field-businessinformation-city">
                                                        <label for="businessinformation-state" class="control-label">City</label>
                                                        <select name="BusinessInformation[city]" class="form-control" id="businessinformation-city" >
                                                            <?php foreach ($cities as $city){ ?>
                                                                <option <?php if($model->city==$city->id){ echo 'selected'; }?> value="<?= $city->id ?>">
                                                                    <?= $city->name ?>
                                                                </option>
                                                            <?php } ?>
                                                        </select>

                                                        <div class="help-block"></div>
                                                    </div>


                                                </div>
                                            </div>

                                            <?= $form->field($model, 'zip')->textInput(['maxlength' => true]) ?>

                                            <? //= $form->field($model, 'logo')->textInput(['maxlength' => true]) ?>

                                            <?= $form->field($model, 'business_phone')->textInput(['maxlength' => true]) ?>

                                            <div class="form-group">
                                                <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-main pull-right' : 'btn btn-main pull-right']) ?>
                                            </div>
                                            <div class="alert alert-success" style="display:none">
                                                <strong>Successfully Updated!</strong>.
                                            </div>

                                            <?php ActiveForm::end(); ?>
                                          
                                        </div>
                                        <?php Pjax::end(); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="panel-body" style="border: none;">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <ul class="list-inline list-staff">
                                                <li><h5 class="panel-title text-uppercase"><i class="fa fa-dollar" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Timezone and Currency</span></h5></li>
                                            </ul>
                                        </div>
                                        <div class="panel-body" style="padding-bottom: 25px;">
                                            <?php foreach($timezoneCurrency as $timezonee){ ?>
                                                <form id="timezoneform" action="<?= Yii::getAlias('@web')?>/index.php/business-information/updatetimezone/?id=<?= $timezonee['tnc_id']; ?>">
                                            <div class="row">
                                                <div class="col-lg-12">

                                                    <p>Timezone <small><a href="#timezoneselet" class="txt-theme editedfield">Edit</a></small></p>
                                                    <p style="margin-top: -11px;"><small class="text-muted"><? //= $timezonee['timezone'] ?><!--Karachi, Islamabad (GMT +05:00) --></small></p>
                                                    <select class="form-control timezoneselect" name="timezone" onchange="setTimezoneSetting(this);" id="timezoneselet" style="display: none;">
                                                      <?php foreach($timezone_list as $timezonelist){ ?>
                                                            <option value="<?= $timezonelist->id ?>"><?= $timezonelist->gmt ?> <?= $timezonelist->timezone_location ?></option>
                                                      <?php   } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 20px;">
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <p>Time Format</p>
                                                    <div class="form-group">
                                                        <select class="form-control" name="timeformat" onchange="setTimezoneSetting(this);">
                                                            <option value="12:00" <?php if($timezonee['timeformat']=='12:00'){echo 'selected';} ?>>12:00</option>
                                                            <option value="24.00" <?php if($timezonee['timeformat']=='12:00'){echo 'selected';} ?> >24:00</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <p>Date Format</p>
                                                    <div class="form-group">
                                                        <select class="form-control" name="dateformat" onchange="setTimezoneSetting(this);">
                                                            <option value="MM/DD/YYYY" <?php if($timezonee['dateformat']=='MM/DD/YYYY'){echo 'selected';} ?>>MM/DD/YYYY</option>
                                                            <option value="DD/MM/YYYY" <?php if($timezonee['dateformat']=='DD/MM/YYYY'){echo 'selected';} ?>>DD/MM/YYYY</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 20px;">
                                                <div class="col-lg-6 col-md-6 col-sm-6">


                                                    <p>Currency</p>
                                                    <small> <?php   echo  \app\models\CurrencyList::showcurrency_name($timezonee['currency']) ?> <a href="#currency_list" class="txt-theme editedfield" name="currency" onchange="setTimezoneSetting(this);">Edit</a></small>
                                                    <select class="form-control" id="currency_list" style="display:none">
                                                       <?php foreach($currency_list as $currencylist){ ?>
                                                           <option value="<?= $currencylist->currency_id ?>" <?php if($timezonee['currency']==$currencylist->currency_id){ echo 'selected'; } ?> ><?= $currencylist->country  ?> <?= $currencylist->code.'('.$currencylist->symbol.')'  ?></option>

                                                     <?php  }?>

                                                    </select>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <p>Currency Format</p>
                                                    <div class="form-group">
                                                        <select class="form-control" name="" onchange="setTimezoneSetting(this);">
                                                            <option selected>XX . XX . XX</option>
                                                            <option></option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                                    </form>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="panel-body" style="border: none;">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <ul class="list-inline list-staff">
                                                <li><h5 class="panel-title text-uppercase"><i class="fa fa-briefcase" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Services</span></h5></li>
                                            </ul>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p>Language Preference</p>
                                                    <p style="margin-top: -11px;"><small class="text-muted">Example service, Message, Tour etc</small></p>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 20px;">
                                                <div class="col-lg-6">
                                                    <p>Default Languages</p>
                                                    <div class="form-group">
                                                        <select class="form-control" name="default_language">
                                                            <option selected value="english">English</option>
                                                            <option value="arabic"></option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p>Select Languages</p>
                                                    <p class="text-muted">
                                                        <small>
                                                            Selsct any languages that meet your customer requirements. Your customer will b able to select any of the languages you have chosen.
                                                        </small>
                                                    </p>
                                                    <p><small><a href="#" class="txt-theme">Select languages here</a></small></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="panel-body" style="border: none;">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <ul class="list-inline list-staff">
                                                <li><h5 class="panel-title text-uppercase"><i class="fa fa-user" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Personal Information</span></h5></li>
                                            </ul>
                                        </div>
                                        <div class="panel-body" style="padding-bottom: 46px;">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                   <?php  //print_r(Yii::$app->user->identity); ?>
                                                    <p>First Name</p>
                                                    <div class="form-group">
                                                        <input type="text" placeholder="First Name" class="form-control" value="<?= Yii::$app->user->identity->username; ?>">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <p>Last Name</p>
                                                    <div class="form-group">
                                                        <input type="text" placeholder="Last Name" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 20px;">
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <p>Email</p>
                                                    <small class="text-muted"><?= Yii::$app->user->identity->email; ?> <a href="#emaileditbox" id="emailedittedlink" class="txt-theme">Edit</a></small>
                                                    <span id="emaileditbox" style="display:none"><input type="email" class="form-control"> <span><a href="#" id="ChangeEmail">Save</a> / <a href="#" onclick="cancelemailedit(event)" >Cancel</a></span>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <p>Password</p>
                                                    <small class="text-muted" id="changepaswdcont">********* <a href="" class="txt-theme" id="changepasswordbut">Change Password</a></small>
                                                    <?php // Pjax::begin(); ?>
                                                    <div class="businesschangepassword"></div>
                                                    <?php // Pjax::end(); ?>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="panel-body" style="border: none;">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <ul class="list-inline list-staff">
                                                <li><h5 class="panel-title text-uppercase"><i class="fa fa-phone-square" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Contact Information</span></h5></li>
                                            </ul>
                                        </div>
                                        <div class="panel-body">
                                           <?php if(!empty($contact_phone)) {
                                               foreach ($contact_phone as $contactphones) {
                                                   $homephone = $contactphones['home_phone'];
                                                   $workphone = $contactphones['work_phone'];
                                                   $mobilephone = $contactphones['mobile_phone'];
                                                   $mobileactionid = $contactphones['contact_phone_id'];


                                               }

                                           }else {

                                               $homephone = '';
                                               $workphone = '';
                                               $mobilephone = '';
                                               $mobileactionid = 0;


                                           }
                                           ?>
                                            <form action="<?= Yii::getAlias('@web')?>/index.php/business-information/updatephone/?id=<?= $mobileactionid; ?>" id="contactphneform">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <p>Home Phone</p>
                                                    <div class="form-group">
                                                        <input placeholder="Enter Number" class="form-control" value="<?= $homephone ?>" name="home_phone" type="text">
                                                        <span style="display:none;"><a href="#" class="phonesaved">save</a>/<a href="#" class="phonesavecancel">Cancel</a></span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <p>Work Phone</p>
                                                    <div class="form-group">
                                                        <input placeholder="Enter Number" class="form-control" type="text" name="work_phone" value="<?= $workphone ?>">
                                                        <span style="display:none;"><a href="#" class="phonesaved">save</a>/<a href="#" class="phonesavecancel">Cancel</a></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 20px;">
                                                <div class="col-lg-6">
                                                    <p>Mobile Phone</p>
                                                    <div class="form-group">
                                                        <input placeholder="Enter Number" class="form-control" type="text" name="mobile_phone" value="<?= $mobilephone ?>">
                                                        <span style="display:none;"><a href="#" class="phonesaved">save</a>/<a href="#" class="phonesavecancel">Cancel</a></span>
                                                    </div>
                                                </div>
                                            </div>
                                                </form>
                                            <?php // } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="bhours">
                        <div class="panel-collapse collapse in active">
                            <div class="panel-body" style="border: none;">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <ul class="list-inline list-staff">
                                            <li><h5 class="panel-title text-uppercase"><i class="fa fa-hourglass" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Business Hours</span></h5></li>
                                            <li class="pull-right li-right">Link service and staff to generate business hours</li>
                                        </ul>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row" style="margin-top: 20px;">
                                            <div class="col-lg-12">
                                                <?php  foreach($staff_list as $staffs){?>
                                                    <a href="#" onclick="staff_business_hours(event,'<?= $staffs->id ?>','<?= $staffs->business_id ?>')" class="btn btn-success btn btn-warning ad1" ><?= $staffs->username; ?></a>

                                                <?php } ?>
                                                <div id="staffworkinghourscont">

                                                </div>
                                              
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body" style="border: none;">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <ul class="list-inline list-staff">
                                            <li><h5 class="panel-title text-uppercase"><i class="fa fa-calendar" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Date Specific / Irregular Times</span></h5></li>
                                            <li class="pull-right li-right"><small><a href="" class="txt-theme">Add additional times</a></small></li>
                                        </ul>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p style="margin-top: 5px;"><small class="text-muted">Add unavailability  outside of regular working hours (e.g. 10:am to 1:00 pm on Sunday, January 1st) <a href="" class="txt-theme">(Mouse over any time to see option to delete that time)</a></small></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="panel panel-default" style="padding: 15px;">
                                                <div class="panel-heading" style="padding-bottom: 35px;">
                                                    <ul class="list-inline list-staff">
                                                        <li class="col-lg-3 text-center">Date</li>
                                                        <li class="col-lg-9">Design</li>
                                                    </ul>
                                                </div>
                                                <div class="panel-body" style="padding: 0;">
                                                    <table class="table table-responsive table-striped">
                                                        <tbody>
                                                        <tr>
                                                            <td class="col-lg-3 text-center">
                                                                <i class="fa fa-calendar fa-2x"></i>
                                                            </td>
                                                            <td class="col-lg-9">
                                                                <small class="text-muted">
                                                                    Additional work schedule operates independently to the regular work schedule. This option allows you to open specific or reoccuring dates and time forexample: Open times on this Sunday. Open every 3rd Saturday.
                                                                </small>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <ul style="margin-left: 60px;">
                                                <li><small>As per lead team setting, booking will be allowed 120 minutes from now for the next 90 days.</small></li>
                                                <li><small>Time will open in an interval of 30 minutes. Change interval</small></li>
                                                <li><small>You can create multiple schedules forexample, 'Winter schedule' or 'Summer schedule from here'.</small></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

     

    

      </div>
    </div>










































<?php
Modal::begin([
'header' => '<h4>Appointments</h4>',
'id' => 'modal-slotsform2',
//'size' => 'modal-lg',
//'htmlOptions' => ['style' => 'width:800px;']
]);
echo "<div id='modalslotsContent'></div>";
Modal::end();
?>
<?php
$csrf_ttoken = '"_csrf-token"';
$script = "
$('#ChangeEmail').click(function(event){
event.preventDefault();
var change_email = $('#emaileditbox input').val();
var csrfToken = $('meta[name=".$csrf_ttoken."]').attr('content');
 $.ajax({
    type     :'POST',
    cache    : false,
    data:{'ChangeEmail[email]':change_email,'_csrf':csrfToken},
     url  : '".Yii::getAlias('@web')."/index.php/site/changeemail',
    success  : function(response) {
    var ab = JSON.stringify(response);
  //  alert(ab);
    if(response==''){
    $('#emailedittedlink').show();
     //$('#emailedittedlink').parent('.text-muted').text(change_email);
    
        $('#emaileditbox').append('save').hide(1000);
        
    }
    else { $('#emaileditbox').append(response.success);}
    }
    });
	



});
$('#emailedittedlink').click(function(event){
event.preventDefault();

 var idbutemail= $(this).attr('href');
 $(this).hide();
 $(idbutemail).show();
});
$('#changepasswordbut').click(function(event){
event.preventDefault();
 $('#changepaswdcont').hide();
  $('.businesschangepassword').show();
   
 $.ajax({
    type     :'POST',
    cache    : false,
    url  : '".Yii::getAlias('@web')."/index.php/site/changepassword',
    success  : function(response) {
        $('.businesschangepassword').html(response);
    }
    });
	


});


$('body').on('beforeSubmit', '#business_informationform form', function () {

var form = $(this);
var sc = form.attr('action');
if (form.find('.has-error').length) {
return false;
}
$.ajax({
url: form.attr('action'),
type: 'post',
data: form.serialize(),
success: function (response) {
    if(response==1){
       $('#business_informationform .alert').show(0).delay(2000).hide(0);

    }else {
        alert('not updated');

    }
}
});
return false;
});


$('body').on('beforeSubmit', '#changepassword-form', function () {
var form = $(this);
var sc = form.attr('action');
//alert(sc);
// return false if form still have some validation errors
if (form.find('.has-error').length) {
return false;
}
// submit form
$.ajax({
url: form.attr('action'),
type: 'post',
data: form.serialize(),
success: function (response) {
alert(response);
var json = $.parseJSON(response).error.oldpass;
if(json){
alert('yes');
$('#passwrd_response').html(json);
}
else{
alert('successfully changes');
}
setTimeout(function(){
 
    $('#passwrd_response').text('')
}, 5000);
},
   
 
});
return false;
});
 // $('.timezoneselect').timezones();
  $('.editedfield').click(function(event){
    event.preventDefault();
    $(this).parent().hide();
    var selectattr= $(this).attr('href');

    $(selectattr).show();

});
function setTimezoneSetting(ad){
alert(ad);
}
$('#contactphneform input').change(function(){
$(this).siblings('span').show();
  var phonename = $(this).attr('name');
  var phone = $(this).value;
  alert(phonename);
  
});
$('.phonesaved').click(function(event){
 event.preventDefault();
 $(this).parent().hide();
var phoneaction = $('#contactphneform').attr('action');
var phonefield = $(this).parent().siblings('input');
  var phonename = phonefield.attr('name');
  var phonevalue = phonefield.val();
  
  $.ajax({
url: phoneaction,
type: 'post',
data: {'phone_name':phonename, 'phone_value':phonevalue},
success: function (response) {
if(response=='updated'){



}else{
$('#contactphneform').attr('action','".Yii::getAlias('@web')."/index.php/business-information/updatephone/?id='+response);
}
},
   
 
});
  
});
$('.phonesavecancel').click(function(event){
 event.preventDefault();
    $(this).parent().hide();
});
 $.ajax({
            url: 'userbusiness_hours',
            type: 'post',
            data:{'st_id':1, 'bs_id':1},
            success: function (response) {
                if(response){
                    $('#staffworkinghourscont').html(response);
                   

                }
                    // else {
                //    alert('not updated');

              //  }
            }
        });
$('body').on('beforeSubmit', 'form.date_timeblock', function () {
var form = $(this);

if (form.find('.has-error').length) {
return false;
}
$.ajax({
url: form.attr('action'),
type: 'post',
data: form.serialize(),
success: function (response) {
    if(response==1){
        alert('changed');
         $.pjax.reload({container:'#blockdatescont'});

    }else {
        alert('not updated');

    }
}
});
return false
});  

 $('#businessinformation-country').change(function(){
  var country_id =  $('#businessinformation-country option:selected').val();
 $.ajax({
url:'".Yii::getAlias('@web')."/index.php/business-information/address_states',
type: 'post',
data: {'country_id':country_id,'type':'country'},
success: function (response) {
    if(response){
    //    alert('changed');
        $('#businessinformation-state').html(response);
        $('#businessinformation-city').html('');

    }else {
        alert('not updated');

    }
}
});
 });
 
  $('#businessinformation-state').change(function(){
  var state_id =  $('#businessinformation-state option:selected').val();
 $.ajax({
url:'".Yii::getAlias('@web')."/index.php/business-information/address_states',
type: 'post',
data: {'state_id':state_id,'type':'state'},
success: function (response) {
    if(response){
    //    alert('changed');
        $('#businessinformation-city').html(response);

    }else {
        alert('not updated');

    }
}
});
 });
 
 $('body').on('beforeSubmit', 'form.updatelogo', function () {
var form = $(this);

if (form.find('.has-error').length) {
return false;
}
$.ajax({
url: form.attr('action'),
type: 'post',
data: form.serialize(),
success: function (response) {
    if(response==1){
        alert('changed');
         $.pjax.reload({container:'#blockdatescont'});

    }else {
        alert('not updated');

    }
}
});
return false
}); 
  
  
  
   $('#updatelogo').on('submit',(function(e) {
        e.preventDefault();
        
        var formData = new FormData(this);
        var formid = $(this).attr('id');

        $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
               if(data!= '0'){
               alert('dfasf');
               $('#business_logo_img').attr('src','".Yii::getAlias('@web')."/'+data);
               }
            },
            error: function(data){
                console.log('error');
                console.log(data);
            }
        });
    }));

    $('#business_logo_upload').on('change', function() {
        $('#updatelogo').submit();
    });
  
  $('#business_logo_click').click(function(event){
  
        event.preventDefault();
        $('#business_logo_upload').click();
        
        });
  
  $('.business_logo_delete').on('click', function(event){
  event.preventDefault();
   current = $(this);
   var type_src  =  $(this).attr('href');
   $.ajax({
            type:'POST',
            url: '".Yii::getAlias('@web')."/index.php/business-information/deltelogo',
           data:{'type':type_src},
       
            success:function(data){
               if(data== '1'){
               alert($(current).siblings('img').attr('src'));
               $(current).siblings('img').attr('src','".Yii::getAlias('@web')."/img/no-image.png');
               }
            }
            
        });
    });
  
  //about scripting
  
  $('#business_about_upload').on('change', function() {
        $('#updateaboutimage').submit();
    });
  
  $('#business_about_click').click(function(event){
  
        event.preventDefault();
        $('#business_about_upload').click();
        
        });
   $('#updateaboutimage').on('submit',(function(e) {
        e.preventDefault();
        
        var formData = new FormData(this);
        var formid = $(this).attr('id');

        $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
               if(data!= '0'){
               alert('dfasf');
               $('#business_about_img').attr('src','".Yii::getAlias('@web')."/'+data);
               }
            },
            error: function(data){
                console.log('error');
                console.log(data);
            }
        });
    }));
  
  $('#currency_list').change(function(){
    var currencylist_id =  $('#currency_list option:selected').val();
     var curformaction = $('#timezoneform').attr('action');
     alert(currencylist_id);
     $.ajax({
            type:'POST',
            url: curformaction,
            data:{'item_name':'currency','item_value':currencylist_id},
            success:function(data){
               if(data!= '0'){
                alert(data);
               }
            },
           
        });
  });
  
    "

;

$this->registerJs($script);
?>
<script>
function setTimezoneSetting(currentEle){
    var itemvalue = currentEle.value;
    var itemname = currentEle.name;
    var curformaction = $('#timezoneform').attr('action');
    $.ajax({
        url: curformaction,
        type: 'post',
        data:{'item_name':itemname, 'item_value':itemvalue},
        success: function (response) {
            if(response==1){
                alert('Updated');

            }else {
                alert('not updated');

            }
        }
    });



 //'var abc = ad.attr('id');
  //  alert(abc);
}
function cancelemailedit(event){
    event.preventDefault();
    $('#emaileditbox').hide();
    $('#emailedittedlink').show();

}
function hidepaswdform(event){
    event.preventDefault();
    $('.businesschangepassword').hide();
    $('#changepaswdcont').show();
}

function buisness_hour_load(event,xor){
    event.preventDefault();
    $('#modal-slotsform2').modal('show').find('#modalslotsContent').load($(xor).attr('href'));

}
    function staff_business_hours(event,st_id,bs_id){
        event.preventDefault();
        $.ajax({
            url: 'userbusiness_hours',
            type: 'post',
            data:{'st_id':st_id, 'bs_id':bs_id},
            success: function (response) {
                if(response){
                    $('#staffworkinghourscont').html(response);
                  

                }
                    // else {
                //    alert('not updated');

              //  }
            }
        });

    }
</script>
