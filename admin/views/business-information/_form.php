<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $model app\models\BusinessInformation */
/* @var $form yii\widgets\ActiveForm */
?>

<?php Pjax::begin(); ?>

    <?php $form = ActiveForm::begin(['enableAjaxValidation' => true, 'validationUrl' => 'validate'] ); ?>
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12">
    <?= $form->field($model, 'business_name')->textInput(['maxlength' => true]) ?>
    </div>

</div>

    <?= $form->field($model, 'business_description')->textarea(['rows' => 4]) ?>
<div class="row">
<div class="col-lg-6 col-md-6 col-sm-6">
    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>
</div>
<div class="col-lg-6 col-md-6 col-sm-6">
    <?= $form->field($model, 'country')->textInput(['maxlength' => true]) ?>
</div>
    </div>
    <div class="row">
        <div class="col-lg-6 col-md-6 col-sm-6">
    <?= $form->field($model, 'state')->textInput(['maxlength' => true]) ?>
        </div>
        <div class="col-lg-6 col-md-6 col-sm-6">
    <?= $form->field($model, 'city')->textInput(['maxlength' => true]) ?>
         </div>
        </div>

    <?= $form->field($model, 'zip')->textInput(['maxlength' => true]) ?>

    <? //= $form->field($model, 'logo')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'business_phone')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
<div class="alert alert-success" style="display:none">
    <strong>Successfully Updated!</strong>.
</div>

    <?php ActiveForm::end(); ?>
<?php Pjax::end(); ?>


