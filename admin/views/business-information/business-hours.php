<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\helpers\Url;
use app\models\BlockedDateTimes;

/* @var $model app\models\BusinessInformation */
/* @var $form yii\widgets\ActiveForm */

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */


// with UI

//use dosamigos\fileupload\FileUpload;;
?>

<?php

//->yii\db\BaseActiveRecord:private
//$timezoneCurrency[0]
//foreach($timezoneCurrency[0] as $timezone){

   // echo $timezone->currencyformat;

//} ?>

<ul class="nav nav-tabs">
    <li class="active"><a aria-expanded="true" href="#schedule" data-toggle="tab">Work Schedule</a></li>
    <li><a aria-expanded="true" href="#unavailability" data-toggle="tab" style="border-top-right-radius: 4px;">Future Unavailability</a></li>
    <li><a aria-expanded="true" href="#staff_detailsch" data-toggle="tab" style="border-top-right-radius: 4px;">Staff Details</a></li>
</ul>
<small><a href="addscedule" style="color: #FC7600;position: absolute; right: 15px; top: 10px;" onclick="buisness_hour_load(event,this)">Add or update schedule</a></small>
<div class="tab-content">
    <div class="tab-pane fade in active" id="schedule">
        <div class="panel-collapse collapse in active">
            <div class="panel-body panel-table">

                <ul class="list-inline" style="padding: 15px 15px 0;">
                    <li><p>Current Schedule <small><a href="" style="color: #FC7600;">Edit</a></small></p></li>
                    <li style="margin-left: 15px;"><small class="text-muted">Current . Forever</small></li>
                    <li class="pull-right"><small class="text-muted">Add time to schedule <span class="txt-theme">(Mouse over any time to see option to delete that time)</span></small></li>
                </ul>
                <table class="table table-responsive table-striped">
                    <thead>
                    <tr style="background: #EAEAEA;">
                        <th>Service</th>
                        <th>Sunday</th>
                        <th>Monday</th>
                        <th>Tuesday</th>
                        <th>Wednesday</th>
                        <th>Thursday</th>
                        <th>Friday</th>
                        <th>Saturday</th>
                    </tr>
                    </thead>
<?php Pjax::begin(['id'=>'weeklytable']); ?>
                    <tbody>
                    <?php foreach($weekly_times as $weeklytimes){?>
                        <?php $sunday = json_decode($weeklytimes->sunday, true);
                        $monday = json_decode($weeklytimes->monday, true);
                        $tuesday = json_decode($weeklytimes->tuesday, true);
                        $wednesday = json_decode($weeklytimes->wednesday, true);
                        $thursday = json_decode($weeklytimes->thursday, true);
                        $friday = json_decode($weeklytimes->friday, true);
                        $saturday = json_decode($weeklytimes->saturday, true);
                        $sunday_size = sizeof($sunday['from']);
                        $monday_size = sizeof($monday['from']);
                        $tuesday_size = sizeof($tuesday['from']);
                        $wednesday_size = sizeof($wednesday['from']);
                        $thursday_size = sizeof($thursday['from']);
                        $friday_size = sizeof($friday['from']);
                        $saturday_size = sizeof($saturday['from']);
                        ?>
                    <tr>
                        <td>
                            <ul class="list-unstyled">
                                <li>
                                    <small><b><?= $weeklytimes['services']['service_name']  ?></b></small>
                                </li>
                                <li><small class="text-muted">Scale: <span class="txt-theme">30 min</span></small></li>
                            </ul>
                        </td>

                        <?php // print_r($weeks['sunday']); ?>
                        <td>
                            <small><a href="<?= Url::to(['services/updatestaff_formslots', 'service_id' => $weeklytimes['services']['service_id'],'staff_id'=>$weeklytimes['staff_id']]);?>" onclick="buisness_hour_load(event,this)" >Edit</a></small><br/>
                        <?php for ($x = 0; $x != $sunday_size; $x++) {
                            if (!empty($sunday['from'][$x]) && !empty($sunday['to'][$x])) {
                                ?>

                                <small class="text-muted">From <?= $sunday['from'][$x] ?><br/>
                                    to <?= $sunday['to'][$x] ?></small><br/>

                            <?php }
                        }?>
                        </td>
                        <td>
                            <small><a href="<?= Url::to(['services/updatestaff_formslots', 'service_id' => $weeklytimes['services']['service_id'],'staff_id'=>$weeklytimes['staff_id']]);?>" onclick="buisness_hour_load(event,this)" >Edit</a></small><br/>
                            <?php for ($x = 0; $x != $monday_size; $x++) {
                            if(!empty($monday['from'][$x]) && !empty($monday['to'][$x])) {
                                ?>

                                <small class="text-muted">From <?= $monday['from'][$x] ?><br/> to <?= $monday['to'][$x] ?></small><br/>

                            <?php }
                            } ?>
                        </td>
                        <td>
                            <small><a href="<?= Url::to(['services/updatestaff_formslots', 'service_id' => $weeklytimes['services']['service_id'],'staff_id'=>$weeklytimes['staff_id']]);?>" onclick="buisness_hour_load(event,this)" >Edit</a></small><br/>
                            <?php for ($x = 0; $x != $tuesday_size; $x++) {
                            if(!empty($tuesday['from'][$x]) && !empty($tuesday['to'][$x])) { ?>


                                    <small class="text-muted">From <?= $tuesday['from'][$x] ?><br/>
                                        to <?= $tuesday['to'][$x] ?></small><br/>

                                    <?php
                                }
                            } ?>
                        </td>
                        <td>
                            <small><a href="<?= Url::to(['services/updatestaff_formslots', 'service_id' => $weeklytimes['services']['service_id'],'staff_id'=>$weeklytimes['staff_id']]);?>" onclick="buisness_hour_load(event,this)" >Edit</a></small><br/>
                            <?php for ($x = 0; $x != $wednesday_size; $x++) {?>

                              <?php if(!empty($wednesday['from'][$x]) && !empty($wednesday['to'][$x])) { ?>
                                    <small class="text-muted">From <?= $wednesday['from'][$x] ?><br/>
                                        to <?= $wednesday['to'][$x] ?></small><br/>

                                    <?php
                                }
                              } ?>
                        </td>
                        <td>
                            <small><a href="<?= Url::to(['services/updatestaff_formslots', 'service_id' => $weeklytimes['services']['service_id'],'staff_id'=>$weeklytimes['staff_id']]);?>" onclick="buisness_hour_load(event,this)" >Edit</a></small><br/>
                            <?php for ($x = 0; $x != $thursday_size; $x++) {
                           if(!empty($thursday['from'][$x]) && !empty($thursday['to'][$x])) { ?>


                                    <small class="text-muted">From <?= $thursday['from'][$x] ?><br/>
                                        to <?= $thursday['to'][$x] ?></small><br/>

                                    <?php
                                }
                            } ?>
                        </td>
                        <td>
                            <small><a href="<?= Url::to(['services/updatestaff_formslots', 'service_id' => $weeklytimes['services']['service_id'],'staff_id'=>$weeklytimes['staff_id']]);?>" onclick="buisness_hour_load(event,this)" >Edit</a></small><br/>
                            <?php for ($x = 0; $x != $friday_size; $x++) {
                            if(!empty($friday['from'][$x]) && !empty($friday['to'][$x])) { ?>


                                <small class="text-muted">From <?= $friday['from'][$x] ?><br/>
                                    to <?= $friday['to'][$x] ?></small><br/>

                                <?php
                            }
                            } ?>
                        </td>
                        <td>
                            <small><a href="<?= Url::to(['services/updatestaff_formslots', 'service_id' => $weeklytimes['services']['service_id'],'staff_id'=>$weeklytimes['staff_id']]);?>" onclick="buisness_hour_load(event,this)" >Edit</a></small><br/>
                            <?php for ($x = 0; $x != $saturday_size; $x++) {
                            if(!empty($saturday['from'][$x]) && !empty($saturday['to'][$x])) {
                                ?>

                                <small class="text-muted">From <?= $saturday['from'][$x] ?><br/> to <?= $saturday['to'][$x] ?></small><br/>

                            <?php }
                            } ?>
                        </td>


                    </tr>
                   <?php } ?>

                    </tbody>
                    <?php Pjax::end(); ?>
                   
                </table>
            </div>
        </div>
    </div>
    <div class="tab-pane fade" id="unavailability">
        <div class="panel-collapse collapse in active">
            <div class="panel-body panel-table">
                <table class="table table-responsive table-striped">
                    <tbody>
                    <tr>
                        <td>
                            <div class="col-sm-12">
                            <h4 style="display:inline">Unavailable Dates </h4><span><small><a href="#" onclick="event.preventDefault();"  data-target="#block_dates" data-toggle="collapse">Block More Dates</a></small></span>
                            </div>
                            <div class="collapse" id="block_dates">
                            <?php
                        Pjax::begin();
                        $form = ActiveForm::begin(['action' => 'savedateblock',
                            'enableAjaxValidation' => true, 'validationUrl' => 'validateblock','options' => ['class' => 'date_timeblock']]); ?>
                        <div class="col-sm-6" style="margin-top: 30px;">
                            <div class="form-group">
                                

                                <?= $form->field($blockedtimes, 'block_date')->textInput(['maxlength' => true,'class' => 'form-control multidate'])->label('Block Dates') ?>

                                <?= $form->field($blockedtimes, 'block_reason')->textarea(['maxlength' => true])->label('Block Reason') ?>
                                <?php foreach($staff_list as $staff_name){ ?>
                                <input type="checkbox" name="staff_id[]" value="<?= $staff_name->id?>" <?php if($staff_name->id==$staff_id){ echo 'checked'; }?>/> <?= $staff_name->username ?> <br/>
                                <?php } ?>
                                <!-- <input type="text" placeholder="Enter text here" class="form-control">-->
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <?= Html::submitButton($blockedtimes->isNewRecord ? 'Add' : 'Update', ['class' => $blockedtimes->isNewRecord ? 'btn btn-main' : 'btn btn-main']) ?>
                            <!-- <button class="btn btn-main" style="padding: 8px 36px;">Add</button> -->

                        </div>
                        <?php ActiveForm::end();
                        Pjax::end();
                        ?>
                            </div>
                         <?php   Pjax::begin(['id'=>'blockdatescont']);  ?>
                            <div class="col-sm-12">
                            <?php foreach($blocked_results as $bluk) {
                                $strdate = strtotime($bluk->block_date);
                                $year = date("Y", $strdate);

                                $business_id = $bluk->business_id;
                                $blocked_months = BlockedDateTimes::find()->where(['business_id' => $business_id, 'YEAR(block_date)' => $year,'staff_id'=>$staff_id])->groupBy(['MONTH(block_date)'])->all();
                                // print_r($blocked_months);
                                foreach ($blocked_months as $blck_month){
                                $strmonth  = strtotime($blck_month->block_date);
                                $monthNum = date("F", $strmonth);

                               ?><br/><a href="#" class="btn"><span class="glyphicon glyphicon-calendar"></span> <?php echo $monthNum.' '.$year; ?><span class="glyphicon glyphicon-trash"></span> </a> <?php

                                    $blocked_days = BlockedDateTimes::find()->where(['business_id' => $business_id,'staff_id'=>$staff_id, 'YEAR(block_date)' => $year,'MONTH(block_date)'=>date("m",$strmonth)])->all();
                                    foreach($blocked_days as $blocked_day){
                                        $strday = strtotime($blocked_day->block_date);
                                        echo '<a href="#" class="btn">'.date('d', $strday).'</a>';
                                    }
                                }
                            } ?>
                            </div>
                            <?php   Pjax::end();  ?>
                        </td>
                      <!--  blockedtimes
                        <input type="date" class="form-control multidate" />
                        <text area -->
                        </tr>

                    <tr>
                        <td>
                            <div class="col-sm-12">
                                <h4 style="display:inline">Unavailable Times </h4><span><small><a href="#" onclick="event.preventDefault();"  data-target="#block_time" data-toggle="collapse">Block More Times</a></small></span>
                            </div>
                            <div class="collapse" id="block_time">
                                <?php
                                Pjax::begin();
                                $form = ActiveForm::begin(['action' => 'savedateblock',
                                    'enableAjaxValidation' => true, 'validationUrl' => 'validateblock', 'options' => ['class' => 'date_timeblock']]); ?>
                                <div class="col-sm-6" style="margin-top: 30px;">
                                    <div class="form-group">
                                        <strong>Select Time</strong>
                                        <?= $form->field($blockedtimes, 'blocktimeFrom')->textInput(['maxlength' => true,'class' => 'form-control timepicker2'])->label(false) ?>
                                        <?= $form->field($blockedtimes, 'blocktimeTo')->textInput(['maxlength' => true,'class' => 'form-control timepicker2'])->label('To') ?>
                                        <?= $form->field($blockedtimes, 'block_date')->textInput(['maxlength' => true,'class' => 'form-control datepicker1'])->label('Block Dates') ?>

                                        <?= $form->field($blockedtimes, 'block_reason')->textarea(['maxlength' => true])->label('Block Reason') ?>
                                        <?php foreach($staff_list as $staff_name){ ?>
                                            <input type="checkbox" name="staff_id[]" value="<?= $staff_name->id?>" <?php if($staff_name->id==$staff_id){ echo 'checked'; }?>/> <?= $staff_name->username ?> <br/>
                                        <?php } ?>
                                        <!-- <input type="text" placeholder="Enter text here" class="form-control">-->
                                    </div>
                                </div>
                                <div class="col-sm-12">
                                    <?= Html::submitButton($blockedtimes->isNewRecord ? 'Add' : 'Update', ['class' => $blockedtimes->isNewRecord ? 'btn btn-main' : 'btn btn-main']) ?>
                                    <!-- <button class="btn btn-main" style="padding: 8px 36px;">Add</button> -->

                                </div>
                                <?php ActiveForm::end();
                                Pjax::end();
                                ?>
                            </div>
                            <div class="col-sm-12">
                                <br/>
                            <?php foreach($blocked_time_results as $blocked_time ){ ?>
                            <a href="#" class="btn"><span class="glyphicon glyphicon-calendar"></span>  <?php echo date("d F Y",strtotime($blocked_time->block_date)); ?></a>
                               <?php  $blocked_days = BlockedDateTimes::find()->where(['business_id' => $blocked_time->business_id,'staff_id'=>$staff_id, 'block_date' => $blocked_time->block_date])->all(); ?>


                                <?php

                                foreach($blocked_days as $blockedday ) {

                                   echo '<a href="#" class="btn" >'.date("g:i a", strtotime($blockedday->blocktimeFrom)).' to ' . date("g:i a", strtotime($blockedday->blocktimeTo)).'</a>'; ?>

                                    <?php
                                }
                                } ?>

                            <?php   Pjax::begin(['id'=>'blockdatescont']);  ?>

                             <?php
                                    // print_r($blocked_months);
                                ?>
                            </div>
                            <?php   Pjax::end();  ?>
                        </td>
                        <!--  blockedtimes
                          <input type="date" class="form-control multidate" />
                          <text area -->
                    </tr>

               
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="tab-pane fade" id="staff_detailsch">
        <div class="panel-collapse collapse in active">
            <div class="panel-body panel-table">

                <div class="row" style="  margin: 0;padding: 12px;">
                  <?php  foreach($current_user as $currentuser){ ?>
                    <div class="col-lg-2 col-md-2 col-sm-2">
                        <?php   if($currentuser->user_profile_image){ ?>
                            <img src="<?= Yii::getAlias('@web').'/'.$currentuser->user_profile_image ?>" style="width:109px;height:109px;">
                        <?php } else { ?>
                            <img src="<?= Yii::getAlias('@web') ?>/img/img.png" class="img-responsive">
                        <?php } ?>
                    </div>
                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 globe">
                        <ul class="list-unstyled line1">
                            <li style="margin-top:5px"><i class="fa fa-envelope-o fa-2x" style="font-size: 20px;color: #FC7600; position: relative; top: 3px;"></i><span class="font"><?= $currentuser->email ?></span></li>
                            <li style="margin-top:5px"><i class="fa fa-globe fa-2x" style="font-size: 20px;color: #FC7600; position: relative; top: 3px;"></i><span class="font"> <?= $currentuser->mobile_phone ?></span></li>
                            <li style="margin-top:5px"><i class="fa fa-phone-square fa-2x" style="font-size: 20px;color: #FC7600; position: relative; top: 3px;"></i><span class="font"> Karachi,Islamabad(GMT+50:00)</span></li>
                        </ul>
                    </div>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>

</div>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />

    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>
<?php
$scripthOURS="
jQuery('.multidate').datepicker({
                multidate: true,
                
         });
  $('.timepicker2').timepicker({
        showPeriod: true
      
    });   
 jQuery('.datepicker1').datepicker({
                
                
         });       
  
            
            ";
$this->registerJs($scripthOURS);
?>


