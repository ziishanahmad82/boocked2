<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Appointments Reports';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
     <!--   <div class="col-lg-3 col-md-3 col-sm-3 no-padding-l media-xs-full">
            <?php /*
            echo Highcharts::widget([
                'options' => [
                    'title' => ['text' => 'Sales Chart'],
                    'options3d'=>['enabled'=>true,
                        'alpha'=>100,  //adjust for tilt
                        'beta'=>100,  // adjust for turn
                        'depth'=>100,
                    ],
                    'plotOptions' => [
                        'pie' => [
                            'cursor' => 'pointer',
                        ],
                    ],
                    'series' => [
                        [ // new opening bracket
                            'type' => 'pie',
                            'name' => 'Elements',
                            'data' => [
                                ['Firefox', 45.0],
                                ['IE', 26.8],
                                ['Safari', 8.5],
                                ['Opera', 6.2],
                                ['Others', 0.7]
                            ],
                        ] // new closing bracket
                    ],
                ],
            ]);
          */  ?>




        </div>-->


        <div style="padding: 0" class="col-sm-12 media-xs-full">
            <div class="panel panel-default">
                <div class="panel-heading full">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/coupons.png"><span class="staff-head">Appointment Report</span></h5></li>
                    </ul>
                </div>
                <div class="panel-body panel-main reports_filter_panel" style="border-top: 1px solid #c4c4c4;">
                    <div class="row row-custom">
                                <div class="col-lg-12">
                                    <?php $form = ActiveForm::begin(['id'=>'search_appointments','action'=>Yii::getAlias('@web').'/index.php/reports/appointmentsearch']); ?>

                                       <div class="row">
                                                   <div class="col-xs-12">
                                                       <div class="row">
                                                       <div class="form-group col-sm-6 col-lg-6">
                                                           <strong>Select Type</strong>
                                                           <select class="form-control" name="list_type">
                                                                <option value="0">List appointments by service date</option>
                                                                 <option value="1">List appointments by booking date</option>
                                                           </select>

                                                       </div>
                                                       <div class="form-group col-sm-6 col-lg-6">
                                                           <strong style="color:transparent">Select Type</strong>
                                                           <select class="form-control" name="report_type">
                                                                <option value="0">Detail Reports</option>
                                                                <option value="1">Group By Date</option>
                                                                <option value="2">Group By Month</option>
                                                            </select>
                                                           <? //= $form->field($new_appointments_payments, 'discount')->input('discount', ['placeholder' => "Discount"])->label(false) ?>
                                                       </div>
                                                       <div class="clearfix"></div>
                                                       </div>
                                                   </div>
                                                   <div class="col-xs-12">
                                                       <div class="row">
                                                       <div class="form-group col-sm-6 col-lg-6">
                                                            <strong>Select Date</strong>
                                                           <?= DatePicker::widget([
                                                               'id'=>'report_start_date',
                                                               'name' => 'start_date',
                                                               'value'=>date('Y-m-d'),
                                                               'options' => ['class' => 'form-control'],
                                                               'dateFormat' => 'yyyy-MM-dd',


                                                               'clientOptions' => [
                                                                   'format' => 'L',

                                                               ],

                                                           ]);?>
                                                       </div>

                                                       <?//= $form->field($model, 'appointment_date')->widget(\yii\jui\DatePicker::classname(), [
                                                           //'language' => 'ru',
                                                           //'dateFormat' => 'yyyy-MM-dd',
                                                       //]) ?>
                                                       <div class="form-group col-sm-6 col-lg-6">
                                                         <strong>To</strong> <?= DatePicker::widget([
                                                               'id'=>'report_end_date',
                                                               'name' => 'end_date',
                                                               'value'=>date('Y-m-d'),
                                                               'options' => ['class' => 'form-control'],
                                                               'dateFormat' => 'yyyy-MM-dd',
                                                               'clientOptions' => [
                                                                   'format' => 'L',

                                                               ],

                                                           ]);?>
                                                       </div>
                                                           <div class="clearfix"></div>
                                                           </div>
                                                   </div>

                                           <div class="col-xs-12" id="reports_today_yesterday">
                                               <div class="row">
                                               <div class="col-xs-12">
                                                   <small>
                                                       <a href="#" id="report_today">Today</a>
                                                       <a href="#" id="report_yesterday">Yesterday</a>
                                                       <a href="#" id="report_next_week">Next Week</a>
                                                       <a href="#" id="report_last_week">Last Week</a>
                                                   </small>
                                               </div>

                                           </div>

                                           <div class="clearfix"></div>
                                          </div>
                                           <div class="collapse" id="advance_search_fields" >
                                               <div class="col-xs-12">
                                                   <div class="row">
                                                       <div class="form-group col-sm-6 col-lg-4">
                                                            <h3>Advance Options</h3>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-xs-12">
                                                   <div class="row">
                                                   <div class="form-group col-sm-6 col-lg-6">

                                                       <select class="form-control" name="service_type">
                                                           <option value="0">All Services</option>
                                                           <?php foreach($services as $service){ ?>
                                                              <option value="<?= $service->service_id ?>"><?= $service->service_name ?></option>
                                                          <?php } ?>
                                                       </select>

                                                   </div>
                                                   <div class="form-group col-sm-6 col-lg-6">

                                                       <select class="form-control" name="staff_type">
                                                           <option value="0">All Staff</option>
                                                         <?php foreach($staffs as $staff){ ?>
                                                           <option value="<?= $staff->id ?>"><?= $staff->username ?></option>
                                                           <?php } ?>

                                                       </select>
                                                       <? //= $form->field($new_appointments_payments, 'discount')->input('discount', ['placeholder' => "Discount"])->label(false) ?>
                                                   </div>
                                                   </div>
                                                   <div class="clearfix"></div>

                                               </div>
                                               <div class="col-xs-12">
                                                   <div class="row">
                                                       <div class="form-group col-sm-6 col-lg-6">

                                                           <select class="form-control" name="appointment_type">
                                                               <option value="0">All Appointments</option>
                                                               <option value="1">Paid</option>
                                                               <option value="2">Not Paid</option>
                                                               <option value="3">Approved</option>
                                                               <option value="4">Unapproved</option>
                                                           </select>

                                                       </div>
                                                       <div class="form-group col-sm-6 col-lg-6">

                                                           <select class="form-control" name="appointment_status">
                                                               <option value="0">All</option>
                                                               <option value="1">As Scheduled</option>
                                                               <option value="2">Arrived Late</option>
                                                               <option value="3">Gift Certificates</option>
                                                               <option value="4">Cancel</option>
                                                               <option value="5">No Show</option>
                                                           </select>
                                                           <? //= $form->field($new_appointments_payments, 'discount')->input('discount', ['placeholder' => "Discount"])->label(false) ?>
                                                       </div>
                                                   <div class="clearfix"></div>
                                                       </div>
                                               </div>

                                            </div>

                                    <div class="col-sm-12"></div>
                                       </div>
                                           <div class="col-xs-12">
                                               <div class="row">
                                                   <input type="submit" value="Search" class="pull-left btn btn-main" />
                                                   <a id="advancesearchbutton" class="pull-right" style="font-size: 12px; display: block; margin-bottom: 12px; color: rgb(254, 117, 0);cursor:pointer">Advance Search</a>
                                               </div>
                                           </div>
                                           <div class="clearfix"></div>
                                           </div>

                                      <?php ActiveForm::end(); ?>
                        <div class="clearfix"></div>

                        </div></div>
                <div class="panel-body panel-main reports_results_cont" style="border-top: 1px solid #c4c4c4;">
                        <div class="col-sm-12">
                                    <div id="reports_results"></div>
                                    <div class="clearfix"></div>
                            </div>


                                </div>
                            </div>
                           <!-- <div class="row">
                                <div class="col-lg-12 text-center">
                                    <ol class="pagination">
                                        <li><a href="#" class="go-prev"><i class="fa fa-chevron-left"></i></a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">5</a></li>
                                        <li><a href="#" class="go-next"><i class="fa fa-chevron-right"></i></a></li>
                                    </ol>
                                </div>
                            </div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>



<?php 
$script = "
$('body').on('beforeSubmit', '#search_appointments', function () {
 var form = $(this);
$.ajax({
    url:form.attr('action'),
    type: 'post',
    data: form.serialize(),
    success: function (response) {
    if(response){
    $('#reports_results').html(response);
    }
    }
    }); 
 return false;
 
 });

 $('#advancesearchbutton').click(function(event){
        button_text = $(this).text();
        if(button_text=='Advance Search'){
        $(this).text('Basic Search');
        $('#advance_search_fields').show();
        }else {
        $(this).text('Advance Search');
        $('#advance_search_fields').hide();
        }
   });
";

$this->registerJs($script);
?>
