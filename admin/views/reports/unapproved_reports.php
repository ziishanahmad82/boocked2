<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Unapproved Appointments';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 no-padding-l media-xs-full">
        <div style="text-align:center">
            <div class="GaugeMeter" id="PreviewGaugeMeter_2" data-percent="88" data-append="%" data-size="200" data-theme="red" data-back="#F7F7F7" data-animate_gauge_colors="1" data-animate_text_colors="1" data-width="15" data-label="Speed" data-style="Full" data-label_color="#FFF"></div>
        </div>
          <div class="panel panel-default">
                <div class="panel-heading">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/staff-icon.png"> <span class="staff-head" style="font-size:13px">Unapproved Appointments</span></h5></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="" id="gaugeContainer">

                        </div>

                    </div>
                </div>
            </div>



        </div>
        <div class="col-lg-9 col-md-9 col-sm-9 media-xs-full">
            <div class="panel panel-default">

                <div class="panel-heading full">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/coupons.png"><span class="staff-head">Unapproved Appointments</span></h5></li>
                    </ul>
                </div>
                <div class="panel-body panel-main reports_filter_panel" style="border-top: 1px solid #c4c4c4;">
                        <div class="row row-custom">
                                <div class="col-lg-12">
                                    <?php $form = ActiveForm::begin(['id'=>'search_appointments','action'=>Yii::getAlias('@web').'/index.php/reports/unapprovedsearch']); ?>

                                       <div class="row">
                                           <div class="col-xs-12">
                                                   <div class="row">
                                                       <div class="form-group col-sm-6 col-lg-6">
                                                           <strong>Select Type</strong>
                                                           <select class="form-control" name="list_type">
                                                               <option value="0">List appointments by service date</option>
                                                               <option value="1">List appointments by booking date</option>
                                                           </select>

                                                       </div>
                                                       <div class="form-group col-sm-6 col-lg-6">
                                                           <strong style="color:transparent">Select Type</strong>
                                                           <select class="form-control" name="report_type">
                                                               <option value="0">Detail Reports</option>
                                                               <option value="1">Group By Date</option>
                                                               <option value="2">Group By Month</option>
                                                           </select>
                                                           <? //= $form->field($new_appointments_payments, 'discount')->input('discount', ['placeholder' => "Discount"])->label(false) ?>
                                                       </div>
                                                       <div class="clearfix"></div>
                                                   </div>
                                           </div>
                                                   <div class="col-xs-12">
                                                       <div class="row">
                                                       <div class="form-group col-sm-6 col-lg-6">
                                                            <strong>Select Date</strong>
                                                           <?= DatePicker::widget([
                                                               'name' => 'start_date',
                                                               'value'=>date('Y-m-d'),
                                                               'options' => ['class' => 'form-control'],
                                                               'dateFormat' => 'yyyy-MM-dd',


                                                               'clientOptions' => [
                                                                   'format' => 'L',

                                                               ],

                                                           ]);?>
                                                       </div>

                                                       <?//= $form->field($model, 'appointment_date')->widget(\yii\jui\DatePicker::classname(), [
                                                           //'language' => 'ru',
                                                           //'dateFormat' => 'yyyy-MM-dd',
                                                       //]) ?>
                                                       <div class="form-group col-sm-6 col-lg-6">
                                                         <strong>To </strong><?= DatePicker::widget([
                                                               'name' => 'end_date',
                                                               'value'=>date('Y-m-d'),
                                                               'options' => ['class' => 'form-control'],
                                                               'dateFormat' => 'yyyy-MM-dd',
                                                               'clientOptions' => [
                                                                   'format' => 'L',

                                                               ],

                                                           ]);?>
                                                       </div>
                                                   </div>

                                                    <div class="col-xs-4">

                                           </div>

                                           <div class="clearfix"></div>
                                                       </div>

                                           <div class="collapse" id="advance_search_fields">
                                               <div class="col-xs-12">
                                                   <div class="row">
                                                   <div class="form-group col-sm-6 col-lg-4">
                                                        <h3>Advance Options</h3>
                                                   </div>
                                                       </div>
                                               </div>
                                               <div class="col-xs-12">
                                                   <div class="row">
                                                       <div class="form-group col-sm-6 col-lg-6">

                                                           <select class="form-control" name="service_type">
                                                               <option value="0">All Services</option>
                                                               <?php foreach($services as $service){ ?>
                                                                  <option value="<?= $service->service_id ?>"><?= $service->service_name ?></option>
                                                              <?php } ?>
                                                           </select>

                                                       </div>
                                                       <div class="form-group col-sm-6 col-lg-6">

                                                           <select class="form-control" name="staff_type">
                                                               <option value="0">All Staff</option>
                                                             <?php foreach($staffs as $staff){ ?>
                                                               <option value="<?= $staff->id ?>"><?= $staff->username ?></option>
                                                               <?php } ?>

                                                           </select>
                                                           <? //= $form->field($new_appointments_payments, 'discount')->input('discount', ['placeholder' => "Discount"])->label(false) ?>
                                                       </div>
                                                       <div class="clearfix"></div>
                                                   </div>
                                               </div>

                                               <div class="col-xs-12">


                                                   <div class="clearfix"></div>
                                               </div>

                                           </div>
                                           <div class="col-sm-12"></div>
                                           <div class="col-xs-12"><input type="submit" value="Search" class="pull-left btn btn-main" /> <a id="advancesearchbutton" class="pull-right" style="font-size: 12px; display: block; margin-bottom: 12px; color: rgb(254, 117, 0);cursor:pointer">Advance Search</a> </div>
                                           <div class="clearfix"></div>
                                           </div>

                                      <?php ActiveForm::end(); ?>

                                </div>

                        <div class="clearfix"></div>
                        </div>
                    </div>
                <div class="panel-body panel-main reports_results_cont" style="border-top: 1px solid #c4c4c4;">
                        <div class="col-sm-12">

                            <div id="reports_results"></div>
                            <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php
$script = "
$('body').on('beforeSubmit', '#search_appointments', function () {
 var form = $(this);
$.ajax({
    url:form.attr('action'),
    type: 'post',
    data: form.serialize(),
    success: function (response) {
    if(response){
    $('#reports_results').html(response);
    }
    }
    }); 
 return false;
 
 });
 $('#advancesearchbutton').click(function(event){
        button_text = $(this).text();
        if(button_text=='Advance Search'){
        $(this).text('Basic Search');
        $('#advance_search_fields').show();
        }else {
        $(this).text('Advance Search');
        $('#advance_search_fields').hide();
        }
   });

";

$this->registerJs($script);
$dependjs =" $('.GaugeMeter').gaugeMeter();
        $('#gaugeContainer').jqxGauge({
            ranges: [{ startValue: 0, endValue: 55, style: { fill: '#4bb648', stroke: '#4bb648' }, endWidth: 35, startWidth: 35 },
                { startValue: 55, endValue: 110, style: { fill: '#fbd109', stroke: '#fbd109' }, endWidth: 35, startWidth: 35 },
                { startValue: 110, endValue: 165, style: { fill: '#ff8000', stroke: '#ff8000' }, endWidth: 35, startWidth: 35 },
                { startValue: 165, endValue: 220, style: { fill: '#e02629', stroke: '#e02629' }, endWidth: 35, startWidth: 35 }],
            ticksMinor: { visible: false},
            ticksMajor: { visible: false },
            border:{ visible: false},
            labels:{visible: false, position: 'outside'},
            value: 0,
            colorScheme: 'scheme04',
            animationDuration: 1200,
            cap:{ size: '10%', style: { fill: '#FF7A0D', stroke: '#FF7A0D' } , visible: true },
            pointer: { pointerType: 'default', style: { fill: '#252525', stroke: '#252525' }, length: '50%', width: '6%', visible: true }
        });

        $('#gaugeContainer').on('valueChanging', function (e) {
            $('#gaugeValue').text(Math.round(e.args.value) + 'kph');
        });

        $('#gaugeContainer').jqxGauge('value', 140);";
$this->registerJs($dependjs);
?>