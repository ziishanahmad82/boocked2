<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customer Reports';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    #search_appointments input , #search_appointments select {
        border-radius:0;

    }
</style>
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 no-padding-l media-xs-full">
            <?php
            echo Highcharts::widget([


                'options' => [
                    //   'credits' => ['enabled' => false],
                    'chart' => ['type' => 'pie','height'=>350,'plotBackgroundColor'=> null,'plotShadow'=>false,

                    ],
                    'plotOptions' => [ // it is important here is code for change depth and use pie as donut
                        'pie' => [
                            'allowPointSelect' => true,
                            'cursor' => 'pointer',
                            'innerSize' => 0,
                            'depth' => 30
                        ]
                    ],
                    'title' => ['text' => 'Customer Report'],
                    'series' => [[// mind the [[ instead of [
                        'type' => 'pie',
                        'name' => 'customer',
                        'colors'=>['yellow','#00F4AE'],
                        'data' => [
                            ['Active', 25],
                            ['Inactive', 75],
                            //   ['Active', $activecustomers],
                            //   ['Inactive', $inactivecustomers],
                        ],
                    ]], //mind the ]] instead of ]
                ]
            ]); ?>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/staff-icon.png"> <span class="staff-head">New Users</span></h5></li>
                        <li class="pull-right li-right"></li>
                    </ul>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <?php       echo Highcharts::widget([

                            //  'scripts' => [
                            //       'highcharts-3d',
                            //   ],
                            'options' => [
                                'chart' => ['type' => 'spline', 'height'=>250
                                    /* 'options3d'=>['enabled'=>true,
                                                   'alpha'=>12,  //adjust for tilt
                                                   'beta'=>14,  // adjust for turn
                                                   'depth'=>70,
                                                   ]  */
                                ],
                                'xAxis' => [
                                    'categories' => [date("M", strtotime("-5 months")),date("M", strtotime("-4 months")),date("M", strtotime("-3 months")),date("M", strtotime("-2 months")),date("M", strtotime("-1 months")),date("M")],
                                ],
                                /* 'plotOptions'=>[
                                    'series'=> [
                                        'borderWidth'=>0,
                                        // 'color'=>'rgb(0,220,158)',
                                        'color'=>'#fc7600',
                                        'pointWidth'=>27,


                                    ]
                                ], */
                                'plotLines'=>[[
                                    'value'=>0,
                                    'width'=>3,
                                    'color'=> 'orange'
                                ]],

                                'title' => ['text' => ''],

                                'series' => [[
                                    //   'type'=>'column',
                                    'name'=>'Users',
                                    //  'data'=>[$six_month_users,$five_month_users, $four_month_users,$third_month_users,$second_month_users,$first_month_users],
                                    'data'=>[2,5,3,4,5,10],
                                    'color'=> 'orange',
                                    //   'lineWidth'=>'2'
                                ],
                                    [
                                        //   'type'=>'column',
                                        'name'=>'Users',
                                        //  'data'=>[$six_month_users,$five_month_users, $four_month_users,$third_month_users,$second_month_users,$first_month_users],
                                        'data'=>[9,8,12,8,5,9],
                                        'color'=> 'red',
                                        //   'lineWidth'=>'2'
                                    ],

                                    /*   [
                                           'type'=>'column',
                                           'name'=>'Sales',
                                           'data'=>[intval($six_month),intval($five_month),intval($four_month),intval($third_month),intval($second_month),intval($first_month)],
                                       ] */
                                ],

                            ]
                        ]);
                        ?>


                    </div>
                </div>
            </div>



        </div>


        <div class="col-lg-9 col-md-9 col-sm-9 media-xs-full">
            <div class="panel panel-default">

                <div class="panel-heading full">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/coupons.png"><span class="staff-head">Customer Report</span></h5></li>
                    </ul>
                </div>

                        <div class="panel-body panel-main reports_filter_panel" style="border-top: 1px solid #c4c4c4;">
                            <div class="row row-custom">
                                <div class="col-lg-12">
                                    <?php $form = ActiveForm::begin(['id'=>'search_appointments','action'=>Yii::getAlias('@web').'/index.php/reports/customersearch']); ?>

                                       <div class="row">

                                                   <div class="col-xs-12">
                                                       <div class="form-group col-sm-6 col-lg-6">
                                                            <strong>Registration Date Range </strong>
                                                           <?= DatePicker::widget([
                                                               'id'=>'report_start_date',
                                                               'name' => 'start_date',
                                                               'value'=>date('Y-m-d'),
                                                               'options' => ['class' => 'form-control'],
                                                               'dateFormat' => 'yyyy-MM-dd',


                                                               'clientOptions' => [
                                                                   'format' => 'L',

                                                               ],

                                                           ]);?>
                                                       </div>

                                                       <?//= $form->field($model, 'appointment_date')->widget(\yii\jui\DatePicker::classname(), [
                                                           //'language' => 'ru',
                                                           //'dateFormat' => 'yyyy-MM-dd',
                                                       //]) ?>
                                                       <div class="form-group col-sm-6 col-lg-6">
                                                         <strong>To</strong>   <?= DatePicker::widget([
                                                               'id'=>'report_end_date',
                                                               'name' => 'end_date',
                                                               'value'=>date('Y-m-d'),
                                                               'options' => ['class' => 'form-control'],
                                                               'dateFormat' => 'yyyy-MM-dd',
                                                               'clientOptions' => [
                                                                   'format' => 'L',

                                                               ],

                                                           ]);?>
                                                       </div>
                                                   </div>
                                           <div class="col-xs-12" id="reports_today_yesterday">
                                               <div class="col-xs-12">
                                                   <small>
                                                       <a href="#" id="report_today">Today</a>
                                                       <a href="#" id="report_yesterday">Yesterday</a>
                                                       <a href="#" id="report_last_week">Last Week</a>
                                                   </small>
                                               </div>
                                           </div>

                                           <div class="col-xs-12">
                                               <div class="form-group col-sm-6 col-lg-6">

                                                   <input class="form-control" name="customer_status" />

                                               </div>
                                               <div class="form-group col-sm-6 col-lg-6">

                                                   <select class="form-control" name="verify_customer">
                                                       <option value="2">All</option>
                                                       <option value="1">Verfied</option>
                                                       <option value="0">Unverified</option>


                                                   </select>
                                                   <? //= $form->field($new_appointments_payments, 'discount')->input('discount', ['placeholder' => "Discount"])->label(false) ?>
                                               </div>
                                               <div class="clearfix"></div>
                                           </div>

                                           <div class="clearfix"></div>

                                           <div class="collapse" id="advance_search_fields">
                                               <div class="col-xs-12">
                                                   <div class="form-group col-sm-6 col-lg-4">
                                                        <h3 style="margin-top: 7px; font-size: 20px; margin-bottom: 0px;">Advance Options</h3>
                                                   </div>
                                               </div>
                                               <div class="col-xs-12">
                                                   <div class="form-group col-sm-12 col-lg-12">
                                                    <input type="radio" name="advance_customer_radio" checked value="0"> Get all users:


                                                   </div>
                                                   <div class="form-group col-sm-12 col-lg-12">
                                                       <input type="radio" name="advance_customer_radio" value="1"> Users with no appointment:


                                                   </div>
                                                   <div class="form-group col-sm-12 col-lg-12">
                                                       <input type="radio" name="advance_customer_radio" value="2" >  Not booked between:


                                                   </div>

                                                   <div class="clearfix"></div>
                                               </div>
                                               <div class="col-xs-12">
                                                   <div class="form-group col-sm-6 col-lg-6">

                                                       <?= DatePicker::widget([
                                                           'name' => 'booked_start_date',
                                                           'value'=>date('Y-m-d'),
                                                           'options' => ['class' => 'form-control'],
                                                           'dateFormat' => 'yyyy-MM-dd',


                                                           'clientOptions' => [
                                                               'format' => 'L',

                                                           ],

                                                       ]);?>
                                                   </div>

                                                   <?//= $form->field($model, 'appointment_date')->widget(\yii\jui\DatePicker::classname(), [
                                                   //'language' => 'ru',
                                                   //'dateFormat' => 'yyyy-MM-dd',
                                                   //]) ?>
                                                   <div class="form-group col-sm-6 col-lg-6">
                                                        <?= DatePicker::widget([
                                                           'name' => 'booked_end_date',
                                                           'value'=>date('Y-m-d'),
                                                           'options' => ['class' => 'form-control'],
                                                           'dateFormat' => 'yyyy-MM-dd',
                                                           'clientOptions' => [
                                                               'format' => 'L',

                                                           ],

                                                       ]);?>
                                                   </div>
                                               </div>

                                               <div class="col-xs-12">


                                                   <div class="clearfix"></div>
                                               </div>

                                           </div>

                                           <div class="col-xs-12">
                                               <div class="col-sm-12">
                                                   <input type="submit" value="Search" class="pull-left btn btn-main" />
                                                   <a id="advancesearchbutton" class="pull-right" >Advance Search</a>
                                               </div></div>
                                           <div class="clearfix"></div>
                                           </div>

                                      <?php ActiveForm::end(); ?>

                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body panel-main reports_results_cont" style="border-top: 1px solid #c4c4c4;">
                            <div class="col-sm-12">
                            <div id="reports_results"></div>
                                <div class="clearfix"></div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php
$script = "
$('body').on('beforeSubmit', '#search_appointments', function () {
 var form = $(this);
$.ajax({
    url:form.attr('action'),
    type: 'post',
    data: form.serialize(),
    success: function (response) {
    if(response){
    $('#reports_results').html(response);
    }
    }
    }); 
 return false;
 
 });

   $('#advancesearchbutton').click(function(event){
        button_text = $(this).text();
        if(button_text=='Advance Search'){
        $(this).text('Basic Search');
        $('#advance_search_fields').show();
        }else {
        $(this).text('Advance Search');
        $('#advance_search_fields').hide();
        }
   });


";

$this->registerJs($script);
?>
<script>
    function export_excelreports(xor,event){
        event.prevantDefault();
         var exportdata  = $(xor).sibling('table').html();
        $.ajax({
            url:'<?= Yii::getAlias('@web')?>/index.php/reports/export',
            type: 'post',
            data:{'data':exportdata},
            success: function (response) {
              //  if(response){
                //    $('#reports_results').html(response);
               // }
            }
        });
        return false;



    }

</script>