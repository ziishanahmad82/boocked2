<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reports';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if(!empty($appointmentReports)){ ?>
<div class="panel-default">
    <div class="panel-body panel-no-padding" style="border:none;padding-top: 30px">
      <!--  scroller-tab-->
        <div class="">
<?php if($grouped=='month'){
    ?>
                                                <table class="table table-responsive table-striped tab-reports" id="example">
                                                    <thead>
                                                    <tr class="table-panel-head row-panel-head">
                                                        <th>Sr No</th>
                                                        <th>Month</th>
                                                        <th># Appointments </th>
                                                        <th># Users </th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach($appointmentReports as $appointmentReports){ ?>
                                                    <tr>
                                                        <td></td>
                                                        <td><?php if($appointmentReports->m_date){ echo $appointmentReports->m_date; } else {echo $appointmentReports->appointment_date; }  ?></td>
                                                        <td><?= $appointmentReports->cnt ?></td>
                                                        <td><?= $appointmentReports->cntuser ?></td>
                                                    </tr>
                                                    <?php } ?>
                                                    </tbody>

                                                   
                                                </table>

    <div class="col-sm-12">
        <div class="row">

            <?php
            echo Highcharts::widget([


                'options' => [
                    //   'credits' => ['enabled' => false],
                    'chart' => ['type' => 'pie','height'=>350,'plotBackgroundColor'=> null,'plotShadow'=>false,

                    ],
                    'plotOptions' => [ // it is important here is code for change depth and use pie as donut
                        'pie' => [
                            'allowPointSelect' => true,
                            'cursor' => 'pointer',
                            'innerSize' => 0,
                            'depth' => 30
                        ]
                    ],
                    'title' => ['text' => 'Customer Report'],
                    'series' => [[// mind the [[ instead of [
                        'type' => 'pie',
                        'name' => 'customer',
                        'colors'=>['yellow','#00F4AE'],
                        'data' => [
                            ['Active', 25],
                            ['Inactive', 75],
                            //   ['Active', $activecustomers],
                            //   ['Inactive', $inactivecustomers],
                        ],
                    ]], //mind the ]] instead of ]
                ]
            ]); 

            echo Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => 'Sales',
                    ],
                    'xAxis' => [
                        // 'categories' => $chart_payment_date,
                        'categories' => ['Apr','May','Jun','Jul','Aug','Sep'],
                    ],
                    'labels' => [
                        'items' => [
                            [
                                'html' => 'Sales',
                                'style' => [
                                    'left' => '50px',
                                    'top' => '18px',
                                    'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                                ],
                            ],
                        ],
                    ],
                    'series' => [
                        [
                            'type' => 'column',
                            'name' => 'Appointments',
                            'data' => [ 2, 1, 3, 4,10,30],
                            //    'data'=>[1,80,60,20,0,0,0,0,0]
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Sales',
                            'data' => [2, 1, 3, 4,10,40],
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Payment Users',
                            'data' => [ 2, 1, 3, 4,10,20],
                        ],



                    ],
                ]
            ]);
            ?>
        </div>
    </div>
    
 <?php }elseif ($grouped=='detail') {

                ?>
                <table class="table table-responsive table-striped tab-reports" id="example">
                    <thead>
                    <tr class="table-panel-head row-panel-head">
                        <th>Sr No</th>
                        <th>Who Booked</th>
                        <th>Contact Info</th>
                        <th>Service </th>
                        <th>Staff </th>
                        <th>Appointment Date </th>
                        <th>Info</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $a=1;
                    foreach($appointmentReports as $appointmentReport){ ?>
                        <tr>
                            <td><?= $a ?></td>
                            <td><? if($appointmentReport['customers']['first_name']){ echo $appointmentReport['customers']['first_name'];  }else { echo 'Unknown'; } ?><br/>
                               <small><?php if($appointmentReport['customers']['email']){ echo $appointmentReport['customers']['email'];} else{ echo 'Unknown'; } ?></small><br/>
                             <strong><small>Booked on</small></strong><small>: <?php echo date('D M j Y', strtotime($appointmentReport->date_created)); ?></small>
                            </td>
                            <td><small><? if($appointmentReport['customers']['customer_address']){echo $appointmentReport['customers']['customer_address'].'<br/>';}
                                if($appointmentReport['customers']['customer_city']){echo $appointmentReport['customers']['customer_city'].', ';}
                                if($appointmentReport['customers']['customer_region']){echo $appointmentReport['customers']['customer_region'].', ';}
                                if($appointmentReport['customers']['customer_country']){echo $appointmentReport['customers']['customer_country'].', ';}
                                if($appointmentReport['customers']['customer_zip']){echo $appointmentReport['customers']['customer_zip'].'';}
                              echo '<span style="display: block">';  if($appointmentReport['customers']['cell_phone']){echo $appointmentReport['customers']['cell_phone'].'(M) ';}
                                if($appointmentReport['customers']['home_phone']){echo $appointmentReport['customers']['home_phone'].'(H) ';}
                                if($appointmentReport['customers']['work_phone']){echo $appointmentReport['customers']['work_phone'].'(W) ';}
                              echo '</span>';
                                ?>
                                </small>
                            </td>
                            <td><?= $appointmentReport['services']['service_name'] ?></td>
                            <td><?= $appointmentReport['users']['username'] ?></td>
                            <td><small><?php echo date('D M j Y', strtotime($appointmentReport->appointment_date)).'  '.date('g:i A', strtotime($appointmentReport->appointment_start_time)); ?></small></td>
                            <td><small><?php if($appointmentReport->approval==0){echo 'Unapproved';}else { echo 'Approved';  } ?> , <?php if($appointmentReport->payment_status){echo '<span style="color: #840000;font-weight: bold;">'.$appointmentReport->payment_status;} ?></span></small> </td>

                        </tr>
                    <?php $a++; } ?>
                    </tbody>

                </table>


    <div class="col-sm-12">
        <div class="row">


            <?php
            echo Highcharts::widget([


                'options' => [
                    //   'credits' => ['enabled' => false],
                    'chart' => ['type' => 'pie','height'=>350,'plotBackgroundColor'=> null,'plotShadow'=>false,

                    ],
                    'plotOptions' => [ // it is important here is code for change depth and use pie as donut
                        'pie' => [
                            'allowPointSelect' => true,
                            'cursor' => 'pointer',
                            'innerSize' => 0,
                            'depth' => 30
                        ]
                    ],
                    'title' => ['text' => 'Customer Report'],
                    'series' => [[// mind the [[ instead of [
                        'type' => 'pie',
                        'name' => 'customer',
                        'colors'=>['yellow','#00F4AE'],
                        'data' => [
                            ['Active', 25],
                            ['Inactive', 75],
                            //   ['Active', $activecustomers],
                            //   ['Inactive', $inactivecustomers],
                        ],
                    ]], //mind the ]] instead of ]
                ]
            ]);

            echo Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => 'Sales',
                    ],
                    'xAxis' => [
                        // 'categories' => $chart_payment_date,
                        'categories' => ['Apr','May','Jun','Jul','Aug','Sep'],
                    ],
                    'labels' => [
                        'items' => [
                            [
                                'html' => 'Sales',
                                'style' => [
                                    'left' => '50px',
                                    'top' => '18px',
                                    'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                                ],
                            ],
                        ],
                    ],
                    'series' => [
                        [
                            'type' => 'column',
                            'name' => 'Appointments',
                            'data' => [ 2, 1, 3, 4,10,30],
                            //    'data'=>[1,80,60,20,0,0,0,0,0]
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Sales',
                            'data' => [2, 1, 3, 4,10,40],
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Payment Users',
                            'data' => [ 2, 1, 3, 4,10,20],
                        ],



                    ],
                ]
            ]);
            ?>
        </div>
    </div>
            <?php }else if($grouped=='sales_detail'){ ?>

    <table class="table table-responsive table-striped tab-reports" id="example">
        <thead>
        <tr class="table-panel-head row-panel-head">
            <th><span class="hidden-xs">Sr No</span></th>
            <th><span class="hidden-xs">Who Booked</span></th>
            <th><span class="hidden-xs">Service</span></th>
            <th><span class="hidden-xs">Staff</th>
            <th><span >Amount</span></th>
            <th><span class="hidden-xs">Appointment Date </span></th>
            <th><span class="hidden-xs">Info</span></th>
        </tr>
        </thead>
        <tbody>
        <?php
        $a=1;
        foreach($appointmentReports as $appointmentReport){ ?>
            <tr>
                <td><?= $a ?></td>
                <td><? if($appointmentReport['customers']['first_name']){ echo $appointmentReport['customers']['first_name'];  }else { echo 'Unknown'; } ?><br/>
                    <small><?php if($appointmentReport['customers']['email']){ echo $appointmentReport['customers']['email'];} else{ echo 'Unknown'; } ?></small><br/>
                    <strong><small>Booked on</small></strong><small>: <?php echo date('D M j Y', strtotime($appointmentReport['appointments']['date_created'])); ?></small>
                </td>

                <td><?= $appointmentReport['appointments']['services']['service_name'] ?></td>
                <td><?= $appointmentReport['appointments']['users']['username'] ?></td>
                <td><?= $appointmentReport->total ?></td>
                <td><small><?php echo date('D M j Y', strtotime($appointmentReport['appointments']['appointment_date'])).'  '.date('g:i A', strtotime($appointmentReport['appointments']['appointment_start_time'])); ?></small></td>
                <td><small><?php echo date('D M j Y', strtotime($appointmentReport->payment_date));  ?>
                        <?php if($appointmentReport->additional_charges!=0){echo '<span style="display: block">Additional Charges <span style="color: #840000;font-weight: bold;">'.$appointmentReport->additional_charges;} ?></span></small> </td>

            </tr>
            <?php $a++; }

        ?>
        </tbody>





<?php ?>

    </table>
    <div class="col-sm-12">
        <div class="row">

            <?php
            echo Highcharts::widget([


                'options' => [
                    //   'credits' => ['enabled' => false],
                    'chart' => ['type' => 'pie','height'=>350,'plotBackgroundColor'=> null,'plotShadow'=>false,

                    ],
                    'plotOptions' => [ // it is important here is code for change depth and use pie as donut
                        'pie' => [
                            'allowPointSelect' => true,
                            'cursor' => 'pointer',
                            'innerSize' => 0,
                            'depth' => 30
                        ]
                    ],
                    'title' => ['text' => 'Customer Report'],
                    'series' => [[// mind the [[ instead of [
                        'type' => 'pie',
                        'name' => 'customer',
                        'colors'=>['yellow','#00F4AE'],
                        'data' => [
                            ['Active', 25],
                            ['Inactive', 75],
                            //   ['Active', $activecustomers],
                            //   ['Inactive', $inactivecustomers],
                        ],
                    ]], //mind the ]] instead of ]
                ]
            ]);

            echo Highcharts::widget([
                'scripts' => [
                    'modules/exporting',
                    'themes/grid-light',
                ],
                'options' => [
                    'title' => [
                        'text' => 'Sales',
                    ],
                    'xAxis' => [
                        // 'categories' => $chart_payment_date,
                        'categories' => ['Apr','May','Jun','Jul','Aug','Sep'],
                    ],
                    'labels' => [
                        'items' => [
                            [
                                'html' => 'Sales',
                                'style' => [
                                    'left' => '50px',
                                    'top' => '18px',
                                    'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                                ],
                            ],
                        ],
                    ],
                    'series' => [
                        [
                            'type' => 'column',
                            'name' => 'Appointments',
                            'data' => [ 2, 1, 3, 4,10,30],
                            //    'data'=>[1,80,60,20,0,0,0,0,0]
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Sales',
                            'data' => [2, 1, 3, 4,10,40],
                        ],
                        [
                            'type' => 'column',
                            'name' => 'Payment Users',
                            'data' => [ 2, 1, 3, 4,10,20],
                        ],



                    ],
                ]
            ]);
            ?>
        </div>
    </div>


<?php }  else if($grouped=='salesmonth'){

?>

<table class="table table-responsive table-striped tab-reports" id="example">
    <thead>
                                                    <tr class="table-panel-head row-panel-head">
                                                        <th><span class="hidden-xs">Sr No</</th>
                                                        <th><span class="hidden-xs">Month</span></th>
                                                        <th><span class="hidden-xs"># Appointments </span></th>
                                                        <th><span class="hidden-xs"># Users </span></th>
                                                        <th><span>Amount</span></th>
                                                    </tr>
    </thead>
    <tbody>
                                                    <?php
                                                    $totaluser=0;
                                                    $totalpayment=0;
                                                    $totalcnt=0;
                                                //    $chart_payment[]
                                                //    $chart_paymentuser[]
                                                //    $chart_totalpayment[]
                                                    foreach($appointmentReports as $appointmentReports){ ?>
                                                    <tr>
                                                        <td></td>
                                                        <td> <?= $appointmentReports->payment_date  ?>  <?php if($appointmentReports->payment_date){ echo date('M Y', strtotime($appointmentReports->payment_date)); } else{echo date('D M j Y', strtotime($appointmentReports->created_at));} ?></td>
                                                        <td><?= $appointmentReports->paymentcnt ?></td>
                                                        <td><?= $appointmentReports->paymentuser ?></td>
                                                        <td><?= $appointmentReports->totalpayment ?></td>
                                                    <?php    $totaluser=$totaluser+$appointmentReports->paymentuser;
                                                        $totalpayment=$totalpayment+$appointmentReports->totalpayment;
                                                        $totalcnt=$totalcnt+$appointmentReports->paymentcnt;
                                                    $chart_payment[] = $appointmentReports->paymentcnt;
                                                    $chart_paymentuser[] = $appointmentReports->paymentuser;
                                                    $chart_totalpayment[] = $appointmentReports->totalpayment;
                                                    if($appointmentReports->payment_date){ $chart_payment_date[] = date('d M Y', strtotime($appointmentReports->payment_date));  } else{$chart_payment_date[] = date('d M Y', strtotime($appointmentReports->created_at));}
                                                    ?>
                                                    </tr>
<?php } ?>
    </tbody>
    <tfoot>
    <tr class="table-panel-head row-panel-head">
    <th><span></span></th>
    <th><span ">Total</span></th>
    <th><span ><?= $totalcnt ?></span></th>
    <th><span ><?= $totaluser ?> </span></th>
    <th><span><?= $totalpayment ?></span></th>
    </tr>
    </tfoot>


            </table>
    <?php
//    $chart_payment_date=json_encode($chart_payment_date);
  //  $chart_payment_date = str_replace('"'," ", $chart_payment_date);
   // print_r($chart_payment_date);
   // print_r(array_values($chart_payment));

    //$chart_payment  = array_values($chart_payment);
   //print_r($chart_payment);
   // print_r($chart_payment_date);
  //  $chart_payment=json_encode($chart_payment);
 //   print_r($chart_payment);
  //  $chart_paymentuser = json_encode($chart_paymentuser);
   // print_r($chart_paymentuser);
  //  $chart_payment = json_encode($chart_payment,JSON_NUMERIC_CHECK);
  //  $chart_paymentuser = json_encode($chart_paymentuser,JSON_NUMERIC_CHECK);

  //  $chart_totalpayment = json_encode($chart_totalpayment);


   /* echo Highcharts::widget([
        'options' => [
            'title' => ['text' => 'Fruit Consumption'],
            'xAxis' => [
                'categories' => ['Apples', 'Bananas', 'Oranges']
            ],
            'yAxis' => [
                'title' => ['text' => 'Fruit eaten']
            ],
            'series' => [
                ['name' => 'Jane', 'data' => $chart_payment],
                ['name' => 'John', 'data' => [5, 7, 3]]
            ]
        ]
    ]);   */






    $chart_payment = '['.implode(',', $chart_payment).']';

    //print_r($chart_payment_date);
    //print_r(intval($chart_payment));

?>
    <div class="col-sm-12">
<div class="row">

    <?php
    echo Highcharts::widget([


        'options' => [
            //   'credits' => ['enabled' => false],
            'chart' => ['type' => 'pie','height'=>350,'plotBackgroundColor'=> null,'plotShadow'=>false,

            ],
            'plotOptions' => [ // it is important here is code for change depth and use pie as donut
                'pie' => [
                    'allowPointSelect' => true,
                    'cursor' => 'pointer',
                    'innerSize' => 0,
                    'depth' => 30
                ]
            ],
            'title' => ['text' => 'Customer Report'],
            'series' => [[// mind the [[ instead of [
                'type' => 'pie',
                'name' => 'customer',
                'colors'=>['yellow','#00F4AE'],
                'data' => [
                    ['Active', 25],
                    ['Inactive', 75],
                    //   ['Active', $activecustomers],
                    //   ['Inactive', $inactivecustomers],
                ],
            ]], //mind the ]] instead of ]
        ]
    ]);

echo Highcharts::widget([
    'scripts' => [
        'modules/exporting',
        'themes/grid-light',
    ],
    'options' => [
        'title' => [
            'text' => 'Sales',
        ],
        'xAxis' => [
           // 'categories' => $chart_payment_date,
            'categories' => ['Apr','May','Jun','Jul','Aug','Sep'],
        ],
        'labels' => [
            'items' => [
                [
                    'html' => 'Sales',
                    'style' => [
                        'left' => '50px',
                        'top' => '18px',
                        'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
                    ],
                ],
            ],
        ],
        'series' => [
            [
                'type' => 'column',
                'name' => 'Appointments',
                'data' => [ 2, 1, 3, 4,10,30],
                //    'data'=>[1,80,60,20,0,0,0,0,0]
            ],
            [
                'type' => 'column',
                'name' => 'Sales',
                'data' => [2, 1, 3, 4,10,40],
            ],
            [
                'type' => 'column',
                'name' => 'Payment Users',
                'data' => [ 2, 1, 3, 4,10,20],
            ],



        ],
    ]
]);
    ?>
</div>
    </div>
  <?php  }else if($grouped=='customer_group'){ ?>

            <table class="table table-responsive table-striped tab-reports " id="example">
              <!--  <thead>
                <tr class="table-panel-head row-panel-head">
                    <th><span class="hidden-xs">Sr No</span></th>
                    <th><span class="hidden-xs">Customer Name</span><span class="hidden-lg hidden-md hidden-sm visible-xs">Col 1</span> <span class="caret caret-down-table hidden-xs"></span></th>
                    <th><span class="hidden-xs">Contact Info </span></th>
                    <th><span class="hidden-xs">Other Info</span></th>


                </tr>
                </thead>-->
                <thead>
                <tr class="table-panel-head row-panel-head">
                    <th>Sr No</th>
                    <th>Customer Name</th>
                    <th>Contact Info</th>
                    <th>Other Info</th>


                </tr>
                </thead>
                <tfoot>
                <tr class="table-panel-head row-panel-head">
                    <th>Sr No</th>
                    <th>Customer Name</th>
                    <th>Contact Info</th>
                    <th>Other Info</th>



                </tr>
                </tfoot>
                <tbody>
                <?php
                $a=1;
                foreach($appointmentReports as $appointmentReport){ ?>
                    <tr>
                        <td><?= $a ?></td>
                        <td><? if($appointmentReport->first_name){ echo $appointmentReport->first_name;  }else { echo 'Unknown'; } echo $appointmentReport->customer_id; ?><br/>
                            <small><?php if($appointmentReport->email){ echo $appointmentReport->email;} else{ echo 'Unknown'; } ?></small><br/>
                            <strong><small>Booked on</small></strong><small>: <?php // echo date('D M j Y', strtotime($appointmentReport->date_created)); ?></small>
                        </td>
                        <td><small><? if($appointmentReport->customer_address){echo $appointmentReport->customer_address.'<br/>';}
                                if($appointmentReport->customer_city){echo $appointmentReport->customer_city.', ';}
                                if($appointmentReport->customer_region){echo $appointmentReport->customer_region.', ';}
                                if($appointmentReport->customer_country){echo $appointmentReport->customer_country.', ';}
                                if($appointmentReport->customer_zip){echo $appointmentReport->customer_zip.'';}
                                echo '<span style="display: block">';  if($appointmentReport->cell_phone){echo $appointmentReport->cell_phone.'(M) ';}
                                if($appointmentReport->home_phone){echo $appointmentReport->home_phone.'(H) ';}
                                if($appointmentReport->work_phone){echo $appointmentReport->work_phone.'(W) ';}
                                echo '</span>';
                                ?>
                            </small>
                        </td>
                       <td></td>



                    </tr>
                    <?php $a++; } ?>
                </tbody>


            </table>


        <?php } ?>
         </div>
    </div>
</div>
<?php } else  {


    echo '<br/><div class="alert alert-danger" sty>
  <strong>No Records !</strong> You have no record for this criteria.
</div>';
}

$script2 = "
$('#example').DataTable( {
dom: 'Bfrtip',
buttons: [
'copy', 'csv', 'excel', 'pdf', 'print'
]
} );




";

$this->registerJs($script2);
?>

<script>
    function export_excelreports(xor,event){
        event.preventDefault();
        var exportdata  = $(xor).siblings('table').html();
        var redirect = 'http://localhost/boocked/admin/web/index.php/reports/export';

        $.ajax({
            url:'http://localhost/boocked/admin/web/index.php/reports/export',
            type: 'post',
            data:{'data':exportdata},
            success: function (response) {
                window.location.href = response.url;
            }
        });
        return false;

    }



</script>
<style>
    .dataTables_wrapper .dataTables_paginate .paginate_button.current, .dataTables_wrapper .dataTables_paginate .paginate_button.current:hover {
        background:#fe7500;
        color: #fff !important;
        border:none;
    }

</style>


