<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Reports';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid section-main">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 no-padding-l media-xs-full">
            <div class="panel panel-default">
                <div class="panel-body section-calendar">
                    <div class="page">
                        <div style="width:100%; max-width:600px; display:inline-block;">
                            <div class="monthly" id="mycalendar"></div>
                        </div>
                    </div>
                    <ul class="list-inline list-wmy">
                        <li><a href="">Week</a></li>
                        <li><a href="">Month</a></li>
                        <li><a href="">Year</a></li>
                    </ul>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/staff-icon.png"> <span class="staff-head">Staff</span></h5></li>
                        <li class="pull-right li-right"><i class="fa fa-plus plus-square"></i></li>
                    </ul>
                </div>
                <div class="panel-body staff-div">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff2.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff2.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff2.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="panel panel-default">
                <div class="panel-heading">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/analytics.png"> <span class="staff-head">analytics</span></h5></li>
                        <li class="pull-right li-right"><i class="fa fa-plus plus-square"></i></li>
                    </ul>
                </div>
                <div class="panel-body section-analytics">
                    <div class="row">
                        <div class="div-analytics">
                            <canvas id="canvas" height="450" width="600"></canvas>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="col-lg-4 col-md-4 col-sm-4 div-relative pull-right">
            <ul class="list-inline pull-right">
                <li><a href="#" data-toggle="tooltip" data-placement="left" data-original-title="Add Service"><i class="fa fa-plus fa-add-services"></i></a></li>
                <li><a href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="Help"><i class="fa fa-question fa-question-mark"></i></a></li>
            </ul>
        </div>
        <div class="col-lg-9 col-md-9 col-sm-9 media-xs-full container-reports">
            <div class="tab-content">
                <div class="tab-pane fade in active" id="first">
                    <div class="panel-collapse collapse in active">
                        <div class="panel-body panel-main" style="border-top: 1px solid #c4c4c4;">
                            <div class="row row-custom">
                                <div class="col-lg-12">
                                    <?php $form = ActiveForm::begin(['id'=>'search_appointments','action'=>Yii::getAlias('@web').'/index.php/reports/appointmentsearch']); ?>

                                       <div class="row">
                                                   <div class="col-xs-12">
                                                       <div class="form-group col-sm-6 col-lg-4">
                                                           <strong>Select Type</strong>
                                                           <select class="form-control" name="list_type">
                                                                <option value="0">List appointments by service date</option>
                                                                 <option value="1">List appointments by booking date</option>
                                                           </select>

                                                       </div>
                                                       <div class="form-group col-sm-6 col-lg-4">
                                                           <strong style="color:transparent">Select Type</strong>
                                                           <select class="form-control" name="report_type">
                                                                <option value="0">Detail Reports</option>
                                                                <option value="1">Group By Date</option>
                                                                <option value="2">Group By Month</option>
                                                            </select>
                                                           <? //= $form->field($new_appointments_payments, 'discount')->input('discount', ['placeholder' => "Discount"])->label(false) ?>
                                                       </div>
                                                       <div class="clearfix"></div>
                                                   </div>
                                                   <div class="col-xs-12">
                                                       <div class="form-group col-sm-6 col-lg-4">
                                                            <strong>Select Date</strong>
                                                           <?= DatePicker::widget([
                                                               'name' => 'start_date',
                                                               'value'=>date('Y-m-d'),
                                                               'options' => ['class' => 'form-control'],
                                                               'dateFormat' => 'yyyy-MM-dd',


                                                               'clientOptions' => [
                                                                   'format' => 'L',

                                                               ],

                                                           ]);?>
                                                       </div>

                                                       <?//= $form->field($model, 'appointment_date')->widget(\yii\jui\DatePicker::classname(), [
                                                           //'language' => 'ru',
                                                           //'dateFormat' => 'yyyy-MM-dd',
                                                       //]) ?>
                                                       <div class="form-group col-sm-6 col-lg-4">
                                                         to   <?= DatePicker::widget([
                                                               'name' => 'end_date',
                                                               'value'=>date('Y-m-d'),
                                                               'options' => ['class' => 'form-control'],
                                                               'dateFormat' => 'yyyy-MM-dd',
                                                               'clientOptions' => [
                                                                   'format' => 'L',

                                                               ],

                                                           ]);?>
                                                       </div>
                                                   </div>

                                                    <div class="col-xs-4">

                                           </div>

                                           <div class="clearfix"></div>
                                           <div>
                                               <div class="col-xs-12">
                                                   <div class="form-group col-sm-6 col-lg-4">
                                                        <h3>Advance Options</h3>
                                                   </div>
                                               </div>
                                               <div class="col-xs-12">
                                                   <div class="form-group col-sm-6 col-lg-4">

                                                       <select class="form-control" name="service_type">
                                                           <option value="0">All Services</option>
                                                           <?php foreach($services as $service){ ?>
                                                              <option value="<?= $service->service_id ?>"><?= $service->service_name ?></option>
                                                          <?php } ?>
                                                       </select>

                                                   </div>
                                                   <div class="form-group col-sm-6 col-lg-4">

                                                       <select class="form-control" name="staff_type">
                                                           <option value="0">All Staff</option>
                                                         <?php foreach($staffs as $staff){ ?>
                                                           <option value="<?= $staff->id ?>"><?= $staff->username ?></option>
                                                           <?php } ?>

                                                       </select>
                                                       <? //= $form->field($new_appointments_payments, 'discount')->input('discount', ['placeholder' => "Discount"])->label(false) ?>
                                                   </div>
                                                   <div class="clearfix"></div>
                                               </div>
                                               <div class="col-xs-12">
                                                   <div class="form-group col-sm-6 col-lg-4">

                                                       <select class="form-control" name="appointment_type">
                                                           <option value="0">All Appointments</option>
                                                           <option value="1">Paid</option>
                                                           <option value="2">Not Paid</option>
                                                           <option value="3">Approved</option>
                                                           <option value="4">Unapproved</option>
                                                       </select>

                                                   </div>
                                                   <div class="form-group col-sm-6 col-lg-4">

                                                     <input type="text" class="form-control" placeholder="impliment after marketing" />
                                                       <? //= $form->field($new_appointments_payments, 'discount')->input('discount', ['placeholder' => "Discount"])->label(false) ?>
                                                   </div>
                                                   <div class="clearfix"></div>
                                               </div>
                                               <div class="col-xs-12">
                                                   <div class="form-group col-sm-6 col-lg-4">

                                                       <select class="form-control" name="appointment_status">
                                                           <option value="0">All</option>
                                                           <option value="1">As Scheduled</option>
                                                           <option value="2">Arrived Late</option>
                                                           <option value="3">Gift Certificates</option>
                                                           <option value="4">Cancel</option>
                                                           <option value="5">No Show</option>
                                                       </select>

                                                   </div>

                                                   <div class="clearfix"></div>
                                               </div>

                                           </div>
                                           <div class="col-xs-12"><input type="submit" value="Search" class="btn btn-main" /></div>
                                           <div class="clearfix"></div>
                                           </div>

                                      <?php ActiveForm::end(); ?>
                                    <div class="panel panel-default">
                                        <div class="panel-body panel-no-padding" style="border-bottom-right-radius: 0; border-bottom-left-radius: 0;">

                                            <div class="scroller-tab">
                                                <table class="table table-responsive table-striped tab-reports">
                                                    <tr class="table-panel-head row-panel-head">
                                                        <th><span class="hidden-xs">Column 1</span><span class="hidden-lg hidden-md hidden-sm visible-xs">Col 1</span> <span class="caret caret-down-table hidden-xs"></span></th>
                                                        <th><span class="hidden-xs">Column 2</span><span class="hidden-lg hidden-md hidden-sm visible-xs">Col 2</span> <span class="caret caret-down-table hidden-xs"></span></th>
                                                        <th><span class="hidden-xs">Column 3</span><span class="hidden-lg hidden-md hidden-sm visible-xs">Col 3</span> <span class="caret caret-down-table hidden-xs"></span></th>
                                                        <th><span class="hidden-xs">Column 4</span><span class="hidden-lg hidden-md hidden-sm visible-xs">Col 4</span> <span class="caret caret-down-table hidden-xs"></span></th>
                                                        <th><span class="hidden-xs">Column 5</span> <span class="hidden-lg hidden-md hidden-sm visible-xs">Col 5</span> <span class="caret caret-down-table hidden-xs"></span></th>
                                                        <th><span class="hidden-xs">Column 6</span> <span class="hidden-lg hidden-md hidden-sm visible-xs">Col 6</span> <span class="caret caret-down-table hidden-xs"></span></th>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr><tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr><tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr><tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr><tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr><tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr><tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr><tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr><tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr><tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr><tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr><tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr><tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr><tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr><tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <tr>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12 text-center">
                                    <ol class="pagination">
                                        <li><a href="#" class="go-prev"><i class="fa fa-chevron-left"></i></a></li>
                                        <li><a href="#">1</a></li>
                                        <li><a href="#">2</a></li>
                                        <li><a href="#">3</a></li>
                                        <li><a href="#">4</a></li>
                                        <li><a href="#">5</a></li>
                                        <li><a href="#" class="go-next"><i class="fa fa-chevron-right"></i></a></li>
                                    </ol>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="reports-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>-->
<!--        --><?//= Html::a('Create Reports', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'name',

            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
            ],
        ],
    ]); ?>

</div>
<?php 
$script = "
$('body').on('beforeSubmit', '#search_appointments', function () {
alert('accc');
 var form = $(this);
$.ajax({
    url:form.attr('action'),
    type: 'post',
    data: form.serialize(),
    success: function (response) {
    if(response==1){
    
    $('#new_customer').modal('hide');
  $.pjax.reload({container: '#customer_list_reload'});

    }
    }
    }); 
 return false;
 
 });

";

$this->registerJs($script);
?>
