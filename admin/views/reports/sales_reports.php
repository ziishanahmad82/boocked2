<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\jui\DatePicker;
use miloschuman\highcharts\Highcharts;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ReportsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sales Reports';
$this->params['breadcrumbs'][] = $this->title;
?>

    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 no-padding-l media-xs-full">
<div class="col-sm-12">
    <div class="row">
        <?php
        echo Highcharts::widget([


            'options' => [
                //   'credits' => ['enabled' => false],
                'chart' => ['type' => 'pie','height'=>350,'plotBackgroundColor'=> null,'plotShadow'=>false,

                ],
                'plotOptions' => [ // it is important here is code for change depth and use pie as donut
                    'pie' => [
                        'allowPointSelect' => true,
                        'cursor' => 'pointer',
                        'innerSize' => 0,
                        'depth' => 30
                    ]
                ],
                'title' => ['text' => 'Customer Report'],
                'series' => [[// mind the [[ instead of [
                    'type' => 'pie',
                    'name' => 'customer',
                    'colors'=>['#8AFB17','#F660AB'],
                    'data' => [
                        ['Active', 25],
                        ['Inactive', 75],
                        //   ['Active', $activecustomers],
                        //   ['Inactive', $inactivecustomers],
                    ],
                ]], //mind the ]] instead of ]
            ]
        ]); ?>
</div>
</div>
            <div class="clearfix"></div>
   <?php
 /*   echo Highcharts::widget([
        'scripts' => [
            'modules/exporting',
            'themes/grid-light',
        ],
        'options' => [
            'title' => [
                'text' => 'Combination chart',
            ],
            'type' => 'pie',
            'name' => 'Total consumption',
            'data' => [
            [
            'name' => 'Sales',
            'y' => 13,
            'color' => new JsExpression('Highcharts.getOptions().colors[0]'), // Jane's color
            ],
            [
            'name' => 'Appointments',
            'y' => 23,
            'color' => new JsExpression('Highcharts.getOptions().colors[1]'), // John's color
            ],


            ],
            'center' => [100, 80],
            'size' => 100,
            'showInLegend' => false,
            'dataLabels' => [
            'enabled' => false,
            ],

        ]]); */
        ?>
            <!--   <div class="panel panel-default">
                   <div class="panel-body section-calendar">
                       <div class="page">
                           <div style="width:100%; max-width:600px; display:inline-block;">
                               <div class="monthly" id="mycalendar"></div>
                           </div>
                       </div>
                       <ul class="list-inline list-wmy">
                           <li><a href="">Week</a></li>
                           <li><a href="">Month</a></li>
                           <li><a href="">Year</a></li>
                       </ul>
                   </div>
               </div>-->

             <!--  <div class="panel panel-default">
                   <div class="panel-heading">
                       <ul class="list-inline list-staff">
                           <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/staff-icon.png"> <span class="staff-head">Staff</span></h5></li>
                           <li class="pull-right li-right"></li>
                       </ul>
                   </div>
                   <div class="panel-body">
                       <div class="row">

                       </div>
                   </div>
               </div>-->

           <!--    <div class="panel panel-default">
                   <div class="panel-heading">
                       <ul class="list-inline list-staff">
                           <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/analytics.png"> <span class="staff-head">analytics</span></h5></li>
                           <li class="pull-right li-right"><i class="fa fa-plus plus-square"></i></li>
                       </ul>
                   </div>
                   <div class="panel-body section-analytics">
                       <div class="row">
                           <div class="div-analytics">
                               <canvas id="canvas" height="450" width="600"></canvas>
                           </div>
                       </div>
                   </div>
               </div>-->
            

        </div>

       <!-- <div class="col-lg-4 col-md-4 col-sm-4 div-relative pull-right" style="display:none">
            <ul class="list-inline pull-right">
                <li><a href="#" data-toggle="tooltip" data-placement="left" data-original-title="Add Service"><i class="fa fa-plus fa-add-services"></i></a></li>
                <li><a href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="Help"><i class="fa fa-question fa-question-mark"></i></a></li>
            </ul>
        </div>-->
        <div class="col-lg-9 col-md-9 col-sm-9 media-xs-full">
            <div class="panel panel-default">

                    <!-- <div class="tab-content">
                         <div class="tab-pane fade in active" id="first">
                             <div class="panel-collapse collapse in active">-->
                    <div class="panel-heading full">
                        <ul class="list-inline list-staff">
                            <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/coupons.png"><span class="staff-head">Sales Report</span></h5></li>
                        </ul>
                    </div>
                        <div class="panel-body panel-main reports_filter_panel" style="border-top: 1px solid #c4c4c4;">
                            <div class="row row-custom">
                                <div class="col-lg-12">
                                    <?php $form = ActiveForm::begin(['id'=>'search_appointments','action'=>Yii::getAlias('@web').'/index.php/reports/salessearch']); ?>

                                       <div class="row">
                                                   <div class="col-xs-12">
                                                       <div class="row">
                                                       <div class="form-group col-sm-6">
                                                           <strong >Select Type</strong>
                                                           <select class="form-control" name="report_type">
                                                                <option value="0">Detail Reports</option>
                                                                <option value="1">Group By Date</option>
                                                                <option value="2">Group By Month</option>
                                                            </select>
                                                           <? //= $form->field($new_appointments_payments, 'discount')->input('discount', ['placeholder' => "Discount"])->label(false) ?>
                                                       </div>
                                                       <div class="clearfix"></div>
                                                           </div>
                                                   </div>
                                                   <div class="col-xs-12">
                                                       <div class="row">
                                                       <div class="form-group col-sm-6 ">
                                                            <strong>Select Date</strong>
                                                           <?= DatePicker::widget([
                                                               'id'=>'report_start_date',
                                                               'name' => 'start_date',
                                                               'value'=>date('Y-m-d'),
                                                               'options' => ['class' => 'form-control'],
                                                               'dateFormat' => 'yyyy-MM-dd',


                                                               'clientOptions' => [
                                                                   'format' => 'L',

                                                               ],

                                                           ]);?>
                                                       </div>

                                                       <?//= $form->field($model, 'appointment_date')->widget(\yii\jui\DatePicker::classname(), [
                                                           //'language' => 'ru',
                                                           //'dateFormat' => 'yyyy-MM-dd',
                                                       //]) ?>
                                                       <div class="form-group col-sm-6">
                                                        <strong>To</strong>
                                                           <?= DatePicker::widget([
                                                               'id'=>'report_end_date',
                                                               'name' => 'end_date',
                                                               'value'=>date('Y-m-d'),
                                                               'options' => ['class' => 'form-control'],
                                                               'dateFormat' => 'yyyy-MM-dd',
                                                               'clientOptions' => [
                                                                   'format' => 'L',

                                                               ],

                                                           ]);?>
                                                       </div>

                                                       </div>


                                                   </div>
                                           <div class="col-xs-12" id="reports_today_yesterday" >
                                               <div class="row">
                                               <div class="col-xs-12">
                                               <small>
                                               <a href="#" id="report_today">Today</a>
                                               <a href="#" id="report_yesterday">Yesterday</a>
                                               <a href="#" id="report_next_week">Next Week</a>
                                               <a href="#" id="report_last_week">Last Week</a>
                                               </small>
                                               </div>
                                                   </div>
                                           </div>



                                           <div class="clearfix"></div>
                                           <div class="collapse" id="advance_search_fields">
                                               <div class="col-xs-12">
                                                   <div class="row">
                                                       <div class="form-group col-sm-6">
                                                            <h3>Advance Options</h3>
                                                       </div>
                                                   </div>
                                               </div>
                                               <div class="col-xs-12">
                                                   <div class="row">
                                                   <div class="form-group col-sm-6">

                                                       <select class="form-control" name="service_type">
                                                           <option value="0">All Services</option>
                                                           <?php foreach($services as $service){ ?>
                                                              <option value="<?= $service->service_id ?>"><?= $service->service_name ?></option>
                                                          <?php } ?>
                                                       </select>

                                                   </div>
                                                   <div class="form-group col-sm-6 ">

                                                       <select class="form-control" name="staff_type">
                                                           <option value="0">All Staff</option>
                                                         <?php foreach($staffs as $staff){ ?>
                                                           <option value="<?= $staff->id ?>"><?= $staff->username ?></option>
                                                           <?php } ?>

                                                       </select>
                                                       <? //= $form->field($new_appointments_payments, 'discount')->input('discount', ['placeholder' => "Discount"])->label(false) ?>
                                                   </div>
                                                       <div class="clearfix"></div>
                                                       </div>
                                               </div>
                                               <div class="col-xs-12">
                                                   <div class="row">
                                                       <div class="form-group col-sm-6">

                                                           <select class="form-control" name="appointment_type">
                                                               <option value="0">All Appointments</option>
                                                               <option value="3">Approved</option>
                                                               <option value="4">Unapproved</option>
                                                           </select>

                                                       </div>


                                                   <div class="clearfix"></div>
                                                   </div>
                                               </div>
                                               <div class="col-xs-12">


                                                   <div class="clearfix"></div>
                                               </div>

                                           </div>
                                           <div class="col-sm-12"> </div>
                                           <div class="col-xs-12"><input type="submit" value="Search" class="btn btn-main pull-left" /><a id="advancesearchbutton" class="pull-right">Advance Search</a></div>
                                           <div class="clearfix"></div>
                                           </div>

                                      <?php ActiveForm::end(); ?>

                                </div>
                            </div>
                            </div>
                <div class="panel-body panel-main reports_results_cont" style="border-top: 1px solid #c4c4c4;">
                            <div id="reports_results">



                            </div>

                            <div class="col-sm-6" style="margin-top:20px">

                                <div class="col-sm-12" style="border: 1px solid rgb(196, 196, 196);">
                                    <h4>Appointments</h4>
                    <?php       echo Highcharts::widget([

                        //  'scripts' => [
                        //       'highcharts-3d',
                        //   ],
                        'options' => [
                            'chart' => ['type' => 'column', 'height'=>250

                            ],
                            'xAxis' => [
                                'categories' => [date("M", strtotime("-5 months")),date("M", strtotime("-4 months")),date("M", strtotime("-3 months")),date("M", strtotime("-2 months")),date("M", strtotime("-1 months")),date("M")],
                            ],
                            'plotOptions'=>[
                                'series'=> [
                                    'borderWidth'=>0,
                                    // 'color'=>'rgb(0,220,158)',

                                    'pointWidth'=>27,

                                    //  'dataLabels'=> [
                                    //          'enabled'=>true,
                                    //       'format'=>'{point.y:.1f}%'
                                    //    ]
                                ]
                            ],

                            'title' => ['text' => ''],

                            'series' => [
                                [
                                    'type'=>'column',
                                    'name'=>'Appointments',
                                    //  'data'=>[$six_month_users,$five_month_users, $four_month_users,$third_month_users,$second_month_users,$first_month_users],
                                    'data'=>[5,7,5,9,7,5],
                                    'lineWidth'=>'25',
                                    'color'=>'#08F7B2',
                                ],
                                /*   [
                                       'type'=>'column',
                                       'name'=>'Sales',
                                       'data'=>[intval($six_month),intval($five_month),intval($four_month),intval($third_month),intval($second_month),intval($first_month)],
                                   ] */
                            ],
                        ]
                    ]);
                                ?>
                                </div>
                            </div>
                            <div class="col-sm-6" style="margin-top:20px">
                                <div class="col-sm-12" style="border: 1px solid rgb(196, 196, 196);">
                                    <h4>Users/Sales</h4>

                                <?php       echo Highcharts::widget([

                                    //  'scripts' => [
                                    //       'highcharts-3d',
                                    //   ],
                                    'options' => [
                                        'chart' => ['type' => 'spline', 'height'=>250
                                            /* 'options3d'=>['enabled'=>true,
                                                           'alpha'=>12,  //adjust for tilt
                                                           'beta'=>14,  // adjust for turn
                                                           'depth'=>70,
                                                           ]  */
                                        ],
                                        'xAxis' => [
                                            'categories' => [date("M", strtotime("-5 months")),date("M", strtotime("-4 months")),date("M", strtotime("-3 months")),date("M", strtotime("-2 months")),date("M", strtotime("-1 months")),date("M")],
                                        ],
                                        'plotOptions'=>[
                                            'series'=> [
                                                'borderWidth'=>0,
                                                // 'color'=>'rgb(0,220,158)',

                                                'pointWidth'=>27,

                                                //  'dataLabels'=> [
                                                //          'enabled'=>true,
                                                //       'format'=>'{point.y:.1f}%'
                                                //    ]
                                            ]
                                        ],

                                        'title' => ['text' => ''],

                                        'series' => [[
                                            'type'=>'spline',
                                            'name'=>'Users',
                                            //  'data'=>[$six_month_users,$five_month_users, $four_month_users,$third_month_users,$second_month_users,$first_month_users],
                                            'data'=>[2,5,3,4,5,10],
                                            'lineWidth'=>'2',
                                            'color'=>'#4BB648',
                                        ],
                                            [
                                                'type'=>'spline',
                                                'name'=>'Sales',
                                                //  'data'=>[$six_month_users,$five_month_users, $four_month_users,$third_month_users,$second_month_users,$first_month_users],
                                                'data'=>[8,10,8,12,3,2],
                                                'lineWidth'=>'2',
                                                'color'=>'#E02629',
                                            ],


                                        ],
                                    ]
                                ]);
                                ?>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>


    <?php
  /*
    echo Highcharts::widget([
    'scripts' => [
    'modules/exporting',
    'themes/grid-light',
    ],
    'options' => [
    'title' => [
    'text' => 'Combination chart',
    ],
    'xAxis' => [
    'categories' => ['Jan', 'Feb', 'Mar', 'Apr', 'May','Jun', 'Jul', 'Aug', 'Sep', 'Oct','Nov','Dec'],
    ],
    'labels' => [
    'items' => [
    [
    'html' => 'Total fruit consumption',
    'style' => [
    'left' => '50px',
    'top' => '18px',
    'color' => new JsExpression('(Highcharts.theme && Highcharts.theme.textColor) || "black"'),
    ],
    ],
    ],
    ],
    'series' => [
    [
    'type' => 'column',
    'name' => 'Appointments',
    'data' => [3, 2, 1, 3, 4,3, 2, 1, 3, 4,10,30],
    ],
    [
    'type' => 'column',
    'name' => 'Sales',
    'data' => [2, 3, 5, 7, 6,3, 2, 1, 3, 4,10,30],
    ],


    ],
    ]
    ]); */
    ?>

<?php
$script = "
$('body').on('beforeSubmit', '#search_appointments', function () {
 var form = $(this);
$.ajax({
    url:form.attr('action'),
    type: 'post',
    data: form.serialize(),
    success: function (response) {
    if(response){
    $('#reports_results').html(response);
    }
    }
    }); 
 return false;
 
 });
 
 $('#advancesearchbutton').click(function(event){
        button_text = $(this).text();
        if(button_text=='Advance Search'){
        $(this).text('Basic Search');
        $('#advance_search_fields').show();
        }else {
        $(this).text('Advance Search');
        $('#advance_search_fields').hide();
        }
   });
";

$this->registerJs($script);
?>

