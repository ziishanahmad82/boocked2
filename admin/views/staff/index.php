<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\ActiveField;
use app\controllers\services;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ServicesSearch */
/* @var $servicesCommonNameData app\models\ServicesCommonName */
/* @var $dataProvider yii\data\ActiveDataProvider */

use yii\widgets\Pjax;

$this->title = 'Staff';
$this->params['breadcrumbs'][] = $this->title;
?>



<?php
if(Yii::$app->session->get('bk_singular_staff')!=""){   $singular_staff_name = Yii::$app->session->get('bk_singular_staff'); }else { $singular_staff_name = 'staff'; }
if(Yii::$app->session->get('bk_plural_staffs')!=""){   $plural_staff_name = Yii::$app->session->get('bk_plural_staffs'); }else { $plural_staff_name = 'staffs'; }

?>

<div class="col-lg-3 col-md-3 col-sm-3 no-padding-l media-xs-full">
    <div class="panel panel-default">
        <div class="panel-heading full">
            <ul class="list-inline list-staff">
                <li><h5 class="panel-title"><img src="<?= Yii::getAlias('@web') ?>/img/settings.png"> <span class="staff-head">Manage <?= $plural_staff_name ?></span></h5></li>
            </ul>
        </div>
        <div class="panel-body panel-body-nav-tabs-sidebar">
            <div class="row">

            </div>
            <div class="panel panel-default" style="margin-top: 16px;">
                <div class="panel-heading full">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><i class="fa fa-plus" style="color: #FC7600; position: relative; top: 3px;"></i><span class="staff-head">Add <?= $singular_staff_name ?></span></h5></li>
                    </ul>
                </div>
                <div class="panel-body panel-body-nav-tabs-sidebar">


                    <?php Pjax::begin(); ?>
                    <?php  $form = ActiveForm::begin(['action' => 'staff_user_create', 'id'=>'createstaff',
                        'enableAjaxValidation' => true, 'validationUrl' => 'validatecreatestaff',]); ?>

                    <?= $form->field($userCreateModel, 'username')->textInput(['maxlength' => true]) ?>

                    <?= $form->field($userCreateModel, 'description')->textarea(['rows' => 6]) ?>

                    <?= $form->field($userCreateModel, 'email')->textInput() ?>

                    <?= $form->field($userCreateModel, 'mobile_phone')->textInput() ?>

                    <!--    --><?//= $form->field($model, 'business_id')->textInput() ?>

                    <div class="row">
                        <ul class="list-inline pull-right">
                            <li>
                                <?= Html::submitButton($userCreateModel->isNewRecord ? 'Add' : 'Update', ['class' => $userCreateModel->isNewRecord ? 'btn btn-warning ad1' : 'btn btn-primary']) ?>
                            </li>
                            <li>
                                <button class="btn btn-warning ad1" type="button">Cancel</button>
                            </li>
                        </ul>

                    </div>
                    <div class="alert alert-success" style="display:none">
                        <strong>Success!</strong> Indicates a successful or positive action.
                    </div>
                    <?php ActiveForm::end(); ?>

                    <?php Pjax::end(); ?>
                    
                    <!-- <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 top">
                            <p>Name</p>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Title">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <p>Description</p>
                            <div class="form-group">
                                <textarea class="form-control group" cols="30"  placeholder="Description"></textarea>

                            </div>
                            <small>HTML tags not allowed.1500 char.remaining</small>
                        </div>

                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 top">
                            <p>Email</p>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Text here...">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12 top">
                            <p>Mobile</p>
                            <div class="form-group">
                                <input type="text" class="form-control" placeholder="Text here..">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <ul class="list-inline pull-right">
                            <li><button type="button" class="btn btn-warning ad1">Add</button></li>
                            <li><button type="button" class="btn btn-warning ad1">Cancel</button></li>
                        </ul>
                    </div> -->
                </div>
            </div>

        </div>
    </div>
</div>

<div class="col-lg-9 col-md-9 col-sm-9 media-xs-full">
    <div class="panel panel-default">
        <div class="panel-heading full">
            <ul class="list-inline list-staff">
                <li><h5 class="panel-title text-uppercase"><i class="fa fa-user" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head"><?= $plural_staff_name ?></span></h5></li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="service">
                        <div class="panel-collapse collapse in active">
                            <div class="panel-body" style="border: none;">
                                <div class="panel panel-default">
                                    <div class="panel-heading full">
                                        <ul class="list-inline list-staff">
                                            <li><h5 class="panel-title"><i class="fa fa-file" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">General name for staff</span></h5></li>
                                            <li class="pull-right li-right hidden-xs">General name for staff</li>
                                        </ul>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p>Common name for staff (or first resource)</p>
                                                <p style="margin-top: -11px;"><small class="text-muted">Example teacher, lecturer, beautician etc</small></p>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 20px;">
                                            <div class="col-lg-10 col-lg-offset-1">
                                                <div class="row">
                                                    <?php Pjax::begin(); ?>
                                                    <?php  $form = ActiveForm::begin(['id'=>'sevicecommonform', 'action' => 'savecommon',
                                                        'enableAjaxValidation' => true, 'validationUrl' => 'validate',
                                                        'fieldConfig' => ['options' => ['class' => 'col-lg-6 col-md-6 col-sm-6']],]); ?>
                                                    <?= $form->field($staffCommonNameData[0], 'staff_singular',[ 'labelOptions' => [ 'class' => 'small text-muted']])->textInput(['maxlength' => true]) ?>

                                                    <?= $form->field($staffCommonNameData[0], 'staff_plural',[ 'labelOptions' => [ 'class' => 'small text-muted']])->textInput(['maxlength' => true]) ?>
                                                    <?php ActiveForm::end();
                                                    Pjax::end(); ?>
                                                  <!--  <div class="col-lg-6 col-md-6 col-sm-6">
                                                        <small class="text-muted">Enter singular form</small>
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" placeholder="Design">
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6 col-md-6 col-sm-6">
                                                        <div class="form-group">
                                                            <input type="text" class="form-control" style="margin-top: 20px;" placeholder="Design">
                                                        </div>
                                                    </div>-->
                                                </div>
                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <p><small class="text-muted">This will be shown t your clients at various places while booking. Sample sentences: "Please select (service)", Number of (services)."</small></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel-body" style="border: none;">
                                <div class="panel panel-default">
                                    <div class="panel-heading full">
                                        <ul class="list-inline list-staff">
                                            <li><h5 class="panel-title text-uppercase"><i class="fa fa-user" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head"><?= $plural_staff_name ?></span></h5></li>
                                            <li class="pull-right li-right" style="visibility:hidden"><i class="fa fa-plus plus-square"></i></li>
                                            <li class="pull-right li-right hidden-xs">Total <?= $plural_staff_name ?>:<?= count($inactivestaffList)+count($staffList)?></li>
                                            <li class="pull-right li-right hidden-xs">Inactive: <?= count($inactivestaffList) ?></li>

                                        </ul>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                              <!--  <p>Default Category <small><a href="" style="color: #FC7600 !important;">Edit</a></small></p>
                                                <p style="margin-top: -11px;"><small class="text-muted">Example service, Message, Tour etc</small></p>-->
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 20px;">
                                            <div class="col-lg-12">

                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a href="#active" data-toggle="tab">Active</a></li>
                                                    <li><a href="#inactive" data-toggle="tab" style="border-top-right-radius: 4px;">Inactive</a></li>
                                                </ul>
                                                <?php Pjax::begin(['id'=>'staffcont']); ?>
                                                <div class="tab-content">
                                                    <div class="tab-pane fade in active" id="active">
                                                        <div class="panel-collapse collapse in active">
                                                            <div class="panel-body panel-table">
                                                                <table class="table table-responsive table-striped">
                                                                    <tbody>
                                                                    <?php foreach($staffList as $staff){ ?>
                                                                    <tr>
                                                                        <td>
                                                                            <ul class="list-inline list-action">

                                                                                <li style="position:relative">
                                                                                 
                                                                                    <?php if($staff->user_profile_image){ ?>
                                                                                        <img src="<?= Yii::getAlias('@web').'/'.$staff->user_profile_image ?>" style="width:58px;height: 57px">
                                                                                    <?php }else { ?>
                                                                                    <img src="<?= Yii::getAlias('@web') ?>/img/user.png" style="width:58px;height: 57px">
                                                                                    <?php } ?>
                                                                               <form enctype="multipart/form-data"   method="post" class="staffimageform" action="uploaduserprofile?id=<?= $staff->id ?>" id="staffimageform<?= $staff->id ?>" >
                                                                                     <input type="hidden" id="resource_csrf" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />

                                                                                     <input type="file" name="profile_image" style="display: none;" class="staffimagelog">
                                                                                    <a href="#" class="changeProfile">Change</a>
                                                                                 </form>

                                                                                </li>
                                                                                <li>
                                                                                    <ul class="list-unstyled list-name">
                                                                                        <li><?= $staff->username ?></li>
                                                                                        <li><small><span class="txt-theme"><?= $staff->email ?></span></small></li>
                                                                                    </ul>
                                                                                </li>
                                                                                <div class="pull-right div-desc">
                                                                                    <ul class="list-inline">
                                                                                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-lock"></i></a>
                                                                                            <ul class="dropdown-menu drop">
                                                                                                <li><a href="#" onclick="changeactivation(event,<?= $staff->id ?>,'inactive')">Disable</a></li>
                                                                                            </ul>
                                                                                        </li>
                                                                                        <li class="dropdown"><a href="#" onclick="event.preventDefault();" class="dropdown-toggle" data-target="#staffdemo<?= $staff->id ?>" data-toggle="collapse" ><i class="fa fa-chevron-circle-down"></i></a>

                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </ul>
                                                                        </td>

                                                                    </tr>
                                                                    <tr id="staffdemo<?= $staff->id ?>" class="collapse">
                                                                        <td>
                                                                            <ul class="nav nav-tabs">
                                                                                <li class="active"><a data-toggle="tab" href="#home<?= $staff->id ?>">Who & When</a></li>

                                                                                <?php /*    <li><a data-toggle="tab" href="#menu2<?= $Activeservice['service_id'] ?>">Menu 2</a></li> */ ?>
                                                                            </ul>

                                                                            <div class="tab-content">
                                                                                <div id="home<?= $staff->id ?>" class="tab-pane fade in active">
                                                                                    <div id="staffserviceform<?= $staff->id ?>">

                                                                                    </div>
                                                                                    <br/>
                                                                                 <?php /*   service_id=<?= $service_list->service_id ?>&staff_id=<?php echo $staff->id; ?> */?>
                                                                                    
                                                                                    <p><?php foreach($services_list as $service_list){

                                                                                            ?> <a href="<?= Url::to(['services/updatestaff_formslots', 'service_id' => $service_list->service_id,'staff_id'=>$staff->id]);?>" class="btn btn-info staff_serviceslots" onclick="staff_serviceslots(event,this)"> <?php echo $service_list->service_name; ?></a> <?php

                                                                                        }?></p>
                                                                                </div>

                                                                            <?php /*    <div id="menu2<?= $Activeservice['service_id'] ?>"" class="tab-pane fade">
                                                                                      <h3>Menu 2</h3>
                                                                                      <p>Some content in menu 2.</p>
                                                                                  </div> */ ?>
                                                            </div>
                                                            </td>
                                                                        <?php // echo \app\controllers\StaffController::get_service_name($staff->id); ?>
                                                                    </tr>
                                                                    <?php } ?>

                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="inactive">
                                                        <div class="panel-collapse collapse in active">
                                                            <div class="panel-body panel-table">
                                                                <table class="table table-responsive table-striped">
                                                                    <tbody>
                                                                    <?php foreach($inactivestaffList as $staff){ ?>
                                                                    <tr>
                                                                        <td>
                                                                            <ul class="list-inline list-action">
                                                                                <li style="position:relative">

                                                                                    <?php if($staff->user_profile_image){ ?>
                                                                                        <img src="<?= Yii::getAlias('@web').'/'.$staff->user_profile_image ?>" style="width:58px;height: 57px">
                                                                                    <?php }else { ?>
                                                                                        <img src="<?= Yii::getAlias('@web') ?>/img/user.png" style="width:58px;height: 57px">
                                                                                    <?php } ?>
                                                                                    <form enctype="multipart/form-data"   method="post" class="staffimageform" action="uploaduserprofile?id=<?= $staff->id ?>" id="staffimageform<?= $staff->id ?>" >
                                                                                        <input type="hidden" id="resource_csrf" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />

                                                                                        <input type="file" name="profile_image" style="display: none;" class="staffimagelog">
                                                                                        <a href="#" class="changeProfile">Change</a>
                                                                                    </form>

                                                                                </li>
                                                                                <li>
                                                                                    <ul class="list-unstyled list-name">
                                                                                        <li><?= $staff->username ?></li>
                                                                                        <li><small><span class="txt-theme"><?= $staff->email ?></span></small></li>
                                                                                    </ul>
                                                                                </li>
                                                                                <div class="pull-right div-desc">
                                                                                    <ul class="list-inline">
                                                                                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-unlock"></i></a>
                                                                                                <ul class="dropdown-menu drop">
                                                                                                    <li><a href="#" onclick="changeactivation(event,<?= $staff->id ?>,'active')">Enable</a></li>
                                                                                                </ul>
                                                                                        </li>
                                                                                        <li class="dropdown"><a href="#" onclick="event.preventDefault();" class="dropdown-toggle" data-target="#staffdemo<?= $staff->id ?>" data-toggle="collapse" ><i class="fa fa-chevron-circle-down"></i></a></li>



                                                                                    </ul>
                                                                                </div>
                                                                            </ul>
                                                                        </td>
                                                                    </tr>
                                                                        <tr id="staffdemo<?= $staff->id ?>" class="collapse">
                                                                            <td>
                                                                                <ul class="nav nav-tabs">
                                                                                    <li class="active"><a data-toggle="tab" href="#home<?= $staff->id ?>">Who & When</a></li>

                                                                                    <?php /*    <li><a data-toggle="tab" href="#menu2<?= $Activeservice['service_id'] ?>">Menu 2</a></li> */ ?>
                                                                                </ul>

                                                                                <div class="tab-content">
                                                                                    <div id="home<?= $staff->id ?>" class="tab-pane fade in active">
                                                                                        <div id="staffserviceform<?= $staff->id ?>">

                                                                                        </div>
                                                                                        <br/>
                                                                                        <?php /*   service_id=<?= $service_list->service_id ?>&staff_id=<?php echo $staff->id; ?> */?>

                                                                                        <p><?php foreach($services_list as $service_list){

                                                                                                ?> <a href="<?= Url::to(['services/updatestaff_formslots', 'service_id' => $service_list->service_id,'staff_id'=>$staff->id]);?>" class="btn btn-info staff_serviceslots" onclick="staff_serviceslots(event,this)"> <?php echo $service_list->service_name; ?></a> <?php

                                                                                            }?></p>
                                                                                    </div>

                                                                                    <?php /*    <div id="menu2<?= $Activeservice['service_id'] ?>"" class="tab-pane fade">
                                                                                      <h3>Menu 2</h3>
                                                                                      <p>Some content in menu 2.</p>
                                                                                  </div> */ ?>
                                                                                </div>
                                                                            </td>
                                                                            <?php // echo \app\controllers\StaffController::get_service_name($staff->id); ?>
                                                                        </tr>

                                                                        <?php } ?>



                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php Pjax::end(); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body" style="border: none;">
                                <div class="panel panel-default">
                                    <div class="panel-heading full">
                                        <ul class="list-inline list-staff">
                                            <li><h5 class="panel-title text-uppercase"><i class="fa fa-user" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Staff Details</span></h5></li>


                                            <li class="pull-right li-right hidden-xs">Total staff: 2</li>
                                            <li class="pull-right li-right hidden-xs">Inactive: 0</li>


                                        </ul>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row list">
                                            <ul class="list-inline">
                                                <?php foreach($staffList as $staff){ ?>
                                                <li><a href="#staffdisplayblock<?=$staff->id ?>" onclick="staff_block_display(event,this)" class="staff_block_display">
                                                     <?php   if($staff->user_profile_image){ ?>
                                                        <img src="<?= Yii::getAlias('@web').'/'.$staff->user_profile_image ?>" style="width:58px;height:57px;">
                                                <?php    }else { ?>
                                                    <img src="<?= Yii::getAlias('@web') ?>/img/user.png">
                                                    <?php } ?>
                                                    </a>
                                                </li>

                                                <?php } ?>
                                            </ul>
                                        </div>
                                        <div class="row" style="margin-top: 20px;">
                                            <?php foreach($staffList as $staff){ ?>
                                            <div class="col-lg-12 displaystaff_bigblock" id="staffdisplayblock<?=$staff->id ?>">
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a href="#active" data-toggle="tab"><?= $staff->username ?></a></li>
                                                </ul>
                                                <div class="tab-content">
                                                    <div class="tab-pane fade in active" id="active">
                                                        <div class="panel-collapse collapse in active">
                                                            <div class="panel-body panel-table" style="padding:15px">
                                                                <div class="row">
                                                                    <div class="col-lg-2 col-md-2 col-sm-2">
                                                                        <?php   if($staff->user_profile_image){ ?>
                                                                        <img src="<?= Yii::getAlias('@web').'/'.$staff->user_profile_image ?>" style="width:109px;height:109px;">
                                                                       <?php } else { ?>
                                                                        <img src="<?= Yii::getAlias('@web') ?>/img/img.png" class="img-responsive">
                                                                        <?php } ?>
                                                                    </div>
                                                                    <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12 globe">
                                                                        <ul class="list-unstyled line1">
                                                                            <li style="margin-top:5px"><i class="fa fa-envelope-o fa-2x" style="font-size: 20px;color: #FC7600; position: relative; top: 3px;"></i><span class="font"><?= $staff->email ?></span></li>
                                                                            <li style="margin-top:5px"><i class="fa fa-globe fa-2x" style="font-size: 20px;color: #FC7600; position: relative; top: 3px;"></i><span class="font"> <?= $staff->mobile_phone ?></span></li>
                                                                            <li style="margin-top:5px"><i class="fa fa-phone-square fa-2x" style="font-size: 20px;color: #FC7600; position: relative; top: 3px;"></i><span class="font"> Karachi,Islamabad(GMT+50:00)</span></li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>
                                            </div>
                                            <?php }  ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="preference">
                        <div class="panel-collapse collapse in active">
                            <div class="col-lg-6">
                                <div class="panel-body" style="border: none;">
                                    <div class="panel panel-default">
                                        <div class="panel-heading full">
                                            <ul class="list-inline list-staff">
                                                <li><h5 class="panel-title text-uppercase"><i class="fa fa-dollar" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Timezone and Currency</span></h5></li>
                                            </ul>
                                        </div>
                                        <div class="panel-body" style="padding-bottom: 46px;">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p>Timezone <small><a href="" class="txt-theme">Edit</a></small></p>
                                                    <p style="margin-top: -11px;"><small class="text-muted">Karachi, Islamabad (GMT +05:00)</small></p>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 20px;">
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <p>Time Format</p>
                                                    <div class="form-group">
                                                        <select class="form-control">
                                                            <option selected>12:00</option>
                                                            <option>12:05</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <p>Date Format</p>
                                                    <div class="form-group">
                                                        <select class="form-control">
                                                            <option selected>MM / DD / YY</option>
                                                            <option></option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 20px;">
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <p>Currency</p>
                                                    <small>PKR(Rs) <a href="" class="txt-theme">Edit</a></small>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <p>Currency Format</p>
                                                    <div class="form-group">
                                                        <select class="form-control">
                                                            <option selected>XX . XX . XX</option>
                                                            <option></option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="panel-body" style="border: none;">
                                    <div class="panel panel-default">
                                        <div class="panel-heading full">
                                            <ul class="list-inline list-staff">
                                                <li><h5 class="panel-title text-uppercase"><i class="fa fa-user" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Staff</span></h5></li>
                                            </ul>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p>Language Preference</p>
                                                    <p style="margin-top: -11px;"><small class="text-muted">Example service, Message, Tour etc</small></p>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 20px;">
                                                <div class="col-lg-6">
                                                    <p>Default Languages</p>
                                                    <div class="form-group">
                                                        <select class="form-control">
                                                            <option selected>English</option>
                                                            <option>English</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p>Select Languages</p>
                                                    <p class="text-muted">
                                                        <small>
                                                            Selsct any languages that meet your customer requirements. Your customer will b able to select any of the languages you have chosen.
                                                        </small>
                                                    </p>
                                                    <p><small><a href="#" class="txt-theme">Select languages here</a></small></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="panel-body" style="border: none;">
                                    <div class="panel panel-default">
                                        <div class="panel-heading full">
                                            <ul class="list-inline list-staff">
                                                <li><h5 class="panel-title text-uppercase"><i class="fa fa-user" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Personal Information</span></h5></li>
                                            </ul>
                                        </div>
                                        <div class="panel-body" style="padding-bottom: 46px;">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <p>First Name</p>
                                                    <div class="form-group">
                                                        <input type="text" placeholder="First Name" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <p>Last Name</p>
                                                    <div class="form-group">
                                                        <input type="text" placeholder="Last Name" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 20px;">
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <p>Email</p>
                                                    <small class="text-muted">namename@gmail.com <a href="" class="txt-theme">Edit</a></small>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <p>Password</p>
                                                    <small class="text-muted">********* <a href="" class="txt-theme">Change Password</a></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="panel-body" style="border: none;">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <ul class="list-inline list-staff">
                                                <li><h5 class="panel-title text-uppercase"><i class="fa fa-phone-square" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Contact Information</span></h5></li>
                                            </ul>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <p>Home Phone</p>
                                                    <div class="form-group">
                                                        <input placeholder="Enter Number" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <p>Work Phone</p>
                                                    <div class="form-group">
                                                        <input placeholder="Enter Number" class="form-control" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 20px;">
                                                <div class="col-lg-6">
                                                    <p>Mobile Phone</p>
                                                    <div class="form-group">
                                                        <input placeholder="Enter Number" class="form-control" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="bhours">
                        <div class="panel-collapse collapse in active">
                            <div class="panel-body" style="border: none;">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <ul class="list-inline list-staff">
                                            <li><h5 class="panel-title text-uppercase"><i class="fa fa-hourglass" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Business Hours</span></h5></li>
                                            <li class="pull-right li-right">Link service and staff to generate business hours</li>
                                        </ul>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row" style="margin-top: 20px;">
                                            <div class="col-lg-12">
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a aria-expanded="true" href="#schedule" data-toggle="tab">Work Schedule</a></li>
                                                    <li><a aria-expanded="true" href="#unavailability" data-toggle="tab" style="border-top-right-radius: 4px;">Future Unavailability</a></li>
                                                </ul>
                                                <small><a href="" class="font" style="color: #FC7600;position: absolute; right: 15px; top: 10px;">Add or update schedule</a></small>
                                                <div class="tab-content">
                                                    <div class="tab-pane fade in active" id="schedule">
                                                        <div class="panel-collapse collapse in active">
                                                            <div class="panel-body panel-table">
                                                                <ul class="list-inline" style="padding: 15px 15px 0;">
                                                                    <li><p>Current Schedule <small><a href="" style="color: #FC7600;">Edit</a></small></p></li>
                                                                    <li style="margin-left: 15px;"><small class="text-muted">Current . Forever</small></li>
                                                                    <li class="pull-right"><small class="text-muted">Add time to schedule <span class="txt-theme">(Mouse over any time to see option to delete that time)</span></small></li>
                                                                </ul>
                                                                <table class="table table-responsive table-striped">
                                                                    <thead>
                                                                    <tr style="background: #EAEAEA;">
                                                                        <th>Service</th>
                                                                        <th>Sunday</th>
                                                                        <th>Monday</th>
                                                                        <th>Tuesday</th>
                                                                        <th>Wednesday</th>
                                                                        <th>Thursday</th>
                                                                        <th>Friday</th>
                                                                        <th>Saturday</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <ul class="list-unstyled">
                                                                                <li>
                                                                                    <small><b>Graphic Design (12h)</b></small>
                                                                                </li>
                                                                                <li><small class="text-muted">Scale: <span class="txt-theme">30 min</span></small></li>
                                                                            </ul>
                                                                        </td>
                                                                        <td></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <ul class="list-unstyled">
                                                                                <li>
                                                                                    <small><b>Graphic Design (12h)</b></small>
                                                                                </li>
                                                                                <li><small class="text-muted">Scale: <span class="txt-theme">30 min</span></small></li>
                                                                            </ul>
                                                                        </td>
                                                                        <td></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="unavailability">
                                                        <div class="panel panel-default" style="padding: 15px;">
                                                            <div class="panel-heading" style="padding-bottom: 35px;">

                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><i class="fa fa-calendar" style="margin-right:6px;color:#FC7600"></i>Unavailable dates</div>
                                                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 first"><a href="" style="color: #FC7600;position: absolute; right: 15px; top: 10px;">Add additional times</a></div>

                                                            </div>
                                                            <div class="panel-body" style="padding: 0;padding-bottom:40px;padding-top:40px">

                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center">
                                                                    <i class="fa fa-calendar fa-2x"></i>
                                                                </div>
                                                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                                                    <small class="text-muted" style="float:right">
                                                                        Block a particular date.<a href="" class="txt-theme">(Mouse over any time to see option to delete that time)</a></small>
                                                                    </small>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default" style="padding: 15px;">
                                                            <div class="panel-heading" style="padding-bottom: 35px;">

                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><i class="fa fa-calendar" style="margin-right:6px;color:#FC7600"></i>Unavailable times</div>
                                                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 first"><a href="" style="color: #FC7600;position: absolute; right: 15px; top: 10px;">Add additional times</a></div>

                                                            </div>
                                                            <div class="panel-body" style="padding: 0;padding-bottom:40px;padding-top:40px;">

                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center">
                                                                    <i class="fa fa-calendar fa-2x"></i>
                                                                </div>
                                                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                                                    <small class="text-muted" style="float:right">
                                                                        Block a particular date.<a href="" class="txt-theme">(Mouse over any time to see option to delete that time)</a></small>
                                                                    </small>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default" style="padding: 15px;">
                                                            <div class="panel-heading fir" style="padding-bottom: 35px;">

                                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><i class="fa fa-calendar" style="margin-right:6px;color:#FC7600"></i>Times blocked from google calendar</div>
                                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 first"><a href="" style="color: #FC7600;position: absolute; right: 15px; top: 10px;">Add additional times</a></div>

                                                            </div>
                                                            <div class="panel-body" style="padding: 0;padding-bottom:20px;padding-top:20px">

                                                                <div class="col-lg-3 text-center">
                                                                </div>
                                                                <div class="col-lg-9">
                                                                    <small class="text-muted" style="float:right">
                                                                        Block a particular date.<a href="" class="txt-theme">(Mouse over any time to see option to delete that time)</a></small>
                                                                    </small>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body" style="border: none;">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <ul class="list-inline list-staff">
                                            <li><h5 class="panel-title text-uppercase"><i class="fa fa-calendar" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Date Specific / Irregular Times</span></h5></li>
                                            <li class="pull-right li-right"><small><a href="" class="txt-theme">Add additional times</a></small></li>
                                        </ul>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p style="margin-top: 5px;"><small class="text-muted">Add unavailability  outside of regular working hours (e.g. 10:am to 1:00 pm on Sunday, January 1st) <a href="" class="txt-theme">(Mouse over any time to see option to delete that time)</a></small></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="panel panel-default" style="padding: 15px;">
                                                <div class="panel-heading" style="padding-bottom: 35px;">
                                                    <ul class="list-inline list-staff">
                                                        <li class="col-lg-3 text-center">Date</li>
                                                        <li class="col-lg-9">Design</li>
                                                    </ul>
                                                </div>
                                                <div class="panel-body" style="padding: 0;">
                                                    <table class="table table-responsive table-striped">
                                                        <tbody>
                                                        <tr>
                                                            <td class="col-lg-3 text-center">
                                                                <i class="fa fa-calendar fa-2x"></i>
                                                            </td>
                                                            <td class="col-lg-9">
                                                                <small class="text-muted">
                                                                    Additional work schedule operates independently to the regular work schedule. This option allows you to open specific or reoccuring dates and time forexample: Open times on this Sunday. Open every 3rd Saturday.
                                                                </small>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <ul style="margin-left: 60px;">
                                                <li><small>As per lead team setting, booking will be allowed 120 minutes from now for the next 90 days.</small></li>
                                                <li><small>Time will open in an interval of 30 minutes. Change interval</small></li>
                                                <li><small>You can create multiple schedules forexample, 'Winter schedule' or 'Summer schedule from here'.</small></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-customer-details">
            <div class="modal-dialog modal-dialog-customer">
                <div class="modal-content">
                    <div class="modal-header">
                        <ul class="list-inline list-modal-header">
                            <li class="text-uppercase"><i class="fa fa-user" style="color: #FC7600;"></i> customer</li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                                <a href=""><i class="fa fa-reply"></i></a>
                            </li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                                <a href=""><i class="fa fa-plus"></i></a>
                            </li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban" id="ban-btn-toggle"><i class="fa fa-ban"></i></a></li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay" id="pay-btn-toggle"><i class="fa fa-dollar"></i></a></li>
                            <li class="pull-right" style="font-size: 12px;">Gift Certificates / Discount Coupons</li>
                        </ul>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body-inner-contents">
                            <div class="row">
                                <div class="col-lg-7 col-md-7 col-sm-7">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <i class="fa fa-plus upload-image" data-toggle="tooltip" data-placement="top" title="Upload Image"></i>
                                            <img src="images/staff2.png">
                                        </div>
                                        <div class="col-lg-8">
                                            <h4 class="txt-theme" style="position: relative; top: 9px;">John Doe</h4>
                                            <p><small>Johndoe@gmail.com</small></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <ul class="list-inline" style="margin-top: 15px;">
                                                <li><small><a href="" class="txt-theme">Invite to schedule online</a></small></li><li class="txt-theme"><small>|</small></li>
                                                <li><small><a href="" class="txt-theme">Verify</a></small></li><li class="txt-theme"><small>|</small></li>
                                                <li><small><a href="" class="txt-theme">Edit</a></small></li><li class="txt-theme"><small>|</small></li>
                                                <li><small><a href="" class="txt-theme">Delete</a></small></li>
                                            </ul>
                                            <button class="btn btn-main" style="padding: 4px 9px;"><i class="fa fa-plus"></i> Add Tags</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 media-col6">
                                    <div class="row">
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                            <i class="fa fa-map-marker txt-theme"></i>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                            <p><small>145 strait street, CT.No City, No Region United States(01234)</small></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                            <i class="fa fa-mobile txt-theme"></i>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                            <p><small>098-879-46548</small></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12" style="margin-top: 8px;"><button class="btn btn-main" style="padding: 3px 4px;"><small>Send Gift Certificates</small></button></div>
                                        <div class="col-lg-12" style="margin-top: 5px;"><button class="btn btn-main" style="padding: 3px 4px;"><small>Send Discount Coupons</small></button></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px;">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="2" placeholder="Add Information"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="modal-body-inner-tabs">
                                <ul class="nav nav-tabs nav-main nav-customer-details">
                                    <li class="active">
                                        <a href="#up-app" data-toggle="tab">Upcoming Appointments</a>
                                    </li>
                                    <li>
                                        <a href="#past-app" data-toggle="tab">Past Appointments</a>
                                    </li>
                                    <li>
                                        <a href="#payments" data-toggle="tab">Payments</a>
                                    </li>
                                    <li>
                                        <a href="#promotion" data-toggle="tab">Promotion</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="up-app">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="past-app">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="payments">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="promotion">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="ban">
            <div class="modal-dialog modal-dialog-customer">
                <div class="modal-content">
                    <div class="modal-header">
                        <ul class="list-inline list-modal-header">
                            <li class="text-uppercase"><i class="fa fa-user" style="color: #FC7600;"></i> customer booking restrictions</li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                                <a href=""><i class="fa fa-reply"></i></a>
                            </li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                                <a href=""><i class="fa fa-plus"></i></a>
                            </li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban"><i class="fa fa-ban"></i></a></li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay"><i class="fa fa-dollar"></i></a></li>
                        </ul>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body-inner-contents">
                            <div class="alert alert-warning">
                                <i class="fa fa-warning"></i> <small>Client Advance setting is a premium feature available with PRO and above packages. <a href="#" class="txt-theme">Upgrade your account </a> or <a href="" class="txt-theme">Compare various Packages</a></small>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>This Customer is</p>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option selected>Post-Paying</option>
                                            <option>Post-Paying</option>
                                            <option>Post-Paying</option>
                                        </select>
                                    </div>
                                    <p class="text-muted">
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                        </small>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>Future Bookings Status</p>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option selected>As per settings</option>
                                            <option>Post-Paying</option>
                                            <option>Post-Paying</option>
                                        </select>
                                    </div>
                                    <p class="text-muted">
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                        </small>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>With Restriction</p>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option selected>As per settings</option>
                                            <option>Post-Paying</option>
                                            <option>Post-Paying</option>
                                        </select>
                                    </div>
                                    <p class="text-muted">
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                        </small>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="list-inline pull-right">
                                        <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;">Cancel</button></li>
                                        <li><button class="btn btn-main" style="padding-right: 19px; padding-left: 19px;">Save</button></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="pay">
            <div class="modal-dialog modal-dialog-customer">
                <div class="modal-content">
                    <div class="modal-header">
                        <ul class="list-inline list-modal-header">
                            <li class="text-uppercase"><i class="fa fa-dollar" style="color: #FC7600;"></i> add membership payment</li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                                <a href=""><i class="fa fa-reply"></i></a>
                            </li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                                <a href=""><i class="fa fa-plus"></i></a>
                            </li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban"><i class="fa fa-ban"></i></a></li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay"><i class="fa fa-dollar"></i></a></li>
                        </ul>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <p>Payment Date</p>
                                <div class="form-group">
                                    <input type="text" placeholder="4-17-2016" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <p>Payment For</p>
                                <div class="form-group">
                                    <input type="text" placeholder="Add New Payment" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="text-muted pull-right" style="margin-top: -9px;"><small>Customer restrictions of the selected membership will be applied automatically.</small></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <textarea class="form-control" rows="2" placeholder="Note against payment"></textarea>
                                    <p class="text-danger pull-right"><small>Please do not enter html tags</small></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="modal-body-inner-tabs">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <p class="txt-theme">Amount</p>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Additional Charges">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <span style="position: absolute; left: -2px;">$</span> <input type="text" class="form-control" placeholder="$$$">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Discount">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <span style="position: absolute; left: -6px;">by</span>
                                            <select class="form-control" placeholder="Cash">
                                                <option selected="">Cash</option>
                                                <option>d</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Total">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <ul class="list-inline pull-right">
                                            <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;">Cancel</button></li>
                                            <li><button class="btn btn-main" style="padding-right: 19px; padding-left: 19px;">Save</button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

       </div>
</div>

























<?php
$script = "
$('form#sevicecommonform :text').change(function(){
$('form#sevicecommonform').submit();

});
$('body').on('beforeSubmit', 'form#sevicecommonform', function () {
var form = $(this);
// return false if form still have some validation errors
if (form.find('.has-error').length) {
return false;
}
// submit form
$.ajax({
url: form.attr('action'),
type: 'post',
data: form.serialize(),
success: function (response) {
if(response==1){
alert('changed');

}else {
alert('not updated');

}
}
});
return false;
});

$('body').on('beforeSubmit', 'form#createstaff', function () {
var form = $(this);
// return false if form still have some validation errors
if (form.find('.has-error').length) {
return false;
}
// submit form
$.ajax({
url: form.attr('action'),
type: 'post',
data: form.serialize(),
success: function (response) {
if(response==1){
  $.pjax.reload({container:'#staffcont',timeout:60000});
document.getElementById('createstaff').reset();
$('#createstaff .alert.alert-success').show();
setTimeout(function () {
$('#createstaff .alert.alert-success').hide();
}, 3000);





}else {
alert('not updated');

}
}
});
return false;
});
$('.changeProfile').click(function(event){
event.preventDefault();
$(this).siblings().click();
});

    $('.staffimageform').on('submit',(function(e) {
        e.preventDefault();
        
        var formData = new FormData(this);
        var formid = $(this).attr('id');

        $.ajax({
            type:'POST',
            url: $(this).attr('action'),
            data:formData,
            cache:false,
            contentType: false,
            processData: false,
            success:function(data){
               if(data!= '0'){
               alert('dfasf');
               $('#'+formid).siblings('img').attr('src','".Yii::getAlias('@web')."/'+data)
               }
            },
            error: function(data){
                console.log('error');
                console.log(data);
            }
        });
    }));

    $('.staffimagelog').on('change', function() {
   
        $(this).parent('form').submit();
    });

 
    ";
$this->registerJs($script);

?>

<script>
    function staff_serviceslots(event,xor){
        event.preventDefault();
        var linkattr =  $(xor).attr('href');
        var divid =  $(xor).parent('p').siblings('div').attr('id');
        $('#'+divid).load(linkattr);


        //$('#scdemodal').modal('show').find('#modalscedule').load($(this).attr('href'));
    }
    function changeactivation(event,staff,active){
        event.preventDefault();

        $.ajax({
            url: '<?= Yii::getAlias('@web') ?>/index.php/staff/activate_status',
            type: 'post',
            data:{'staff':staff,'activate':active},
            success: function (response) {
              
                if(response==1){
                    $.pjax.reload({container:'#staffcont',timeout:60000});

                }else {
                //    alert('not updated');

                }
            }
        });

    }
    function staff_block_display(event,xor){
        event.preventDefault();
        var cuurentactive = $(xor).attr('href');
        $('.displaystaff_bigblock').hide();
        $(cuurentactive).show();
    }
</script>

<style>

</style>