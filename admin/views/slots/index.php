<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel app\models\SlotsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Calendar Maintenance';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="slots-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

<!--    <p>-->
<!--        --><?//= Html::a('Create Slots', ['create'], ['class' => 'btn btn-success']) ?>
<!--    </p>-->


    <?=Html::beginForm(['slots/changestatus'],'post');?>
    <div id="sitem11">
    <?=Html::dropDownList('action','',['mark'=>'Mark selected as: ','Active'=>'Active','Inactive'=>'Inactive', 'Auto Reschedule'=>'Auto Reschedule', 'Reschedule'=>'Reschedule'],['class'=>'dropdown','id'=>'status_dropdown'])?>
    <?=Html::submitButton('Save', ['class' => 'btn btn-success',]);?>
        <br/>
        <span id="span-check-all-records">
            <input type="checkbox" name="checkbox-check-all-records" id="checkbox-check-all-records" value="all_pages"> Select all records on all pages?
        </span>
    </div>



    <?
    if(!stristr(Yii::$app->user->identity->user_group, 'thirdparty-')){
        ?>
        <!--    --><?php //Pjax::begin(['id' => 'p1']); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'id' => 'p1',
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                //'calendar_type',
                [
                    'attribute'=>'calendar_type',
                    'filter'=>\app\models\Slots::calendarfilter(),
                    'value' => function($data){
                        $get=Yii::$app->request->get('SlotsSearch');


                        if($get['calendar_type']=='NCDL') {
                            return 'Standard';
                        }
                        if($get['calendar_type']== 'All Third Party' OR stristr($get['calendar_type'], 'thirdparty')){

                            return $data->user->displayname;

                        }

                            return $data->calendar_type;



                    }
                ],

                //'slot_date',
                [
                    'attribute' => 'slot_date',
                    //'value' => 'slot_date',
                    'value' => function($data){
                        return date('m-d-Y', strtotime($data->slot_date));
                    },
                    'filter' => \yii\jui\DatePicker::widget([
                            'language' => 'en',
                            //'dateFormat' => 'yyyy-MM-dd',
                            'dateFormat' => 'MM/dd/yyyy',
                            'model' => $searchModel,
                            'attribute' => 'slot_date',
                            //'value' => 'slot_date',
                        ]
                    ),
                    'format' => 'html',
                ],
                //'start_time:time',
                [
                    'attribute' => 'start_time',
                    'value' => function($data){
                        return date("h:i A", strtotime($data->start_time));
                    },
                ],
                'duration',
//            [
//                'attribute'=>'is_reserved',
//                'filter'=>array("yes"=>"Yes","no"=>"No"),
//                'format'=>'raw',
//                'value'=> function($data){
//                    return ucfirst($data->is_reserved);
//                }
//            ],
                //'reserved_by_customer_id',


                [
                    'label'=>'Permit Number',
                    'attribute' => 'permit_number',
                    'format'=>'raw',
                    //'value'=>'permitnumber.learners_permit_number'
                    'value'=>function($data) {
                        if(@$data->permitnumber->learners_permit_number)
                            return Html::a(
                                $data->permitnumber->learners_permit_number,
                                Yii::getAlias('@web').'/index.php/customers/view/?id='.$data->reserved_by_customer_id,

                                [

                                    'target'=>'_blank',
//                                'data-toggle'=>'tooltip',
//                                'title'=>'Popover Header',
//                                'data-content'=>'Some content <br>inside the popover'
                                ]
                            );
                        else
                            return '';
                    },
                ],
                [
                    'attribute'=>'status',
                    'filter'=>array("Active"=>"Active","Inactive"=>"Inactive", "Reserved"=>"Reserved"),
                    'format'=>'raw',
                    'value'=> function($data){
                        return ucfirst($data->status);
                    }
                ],
                [
                    'label'=>'',
                    'format' => 'raw',
                    'value' => function($data){
                        if($data->status=='Reserved'){
                            return \app\models\Slots::getrescheduledate($data);
                        } else {
                            return '';
                        }

                    }
                ],
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    // you may configure additional properties here
                ],

                //['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>

        <!--    --><?php //Pjax::end(); ?>
        <?
    } else {
        ?>
        <!--    --><?php //Pjax::begin(['id' => 'p1']); ?>
        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'id' => 'p1',
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                //'calendar_type',
                [
                    'attribute'=>'calendar_type',
                    'filter'=>'',
                    'value' => function($data){
                        if($data->calendar_type=='NCDL') {
                            return 'Standard';
                        }
                        else if($data->calendar_type== 'Third party'){
                            return $data->user->displayname;
                        }
                        else {
                            return $data->calendar_type;
                        }
                    }
                ],

                //'slot_date',
                [
                    'attribute' => 'slot_date',
                    //'value' => 'slot_date',
                    'value' => function($data){
                        return date('m-d-Y', strtotime($data->slot_date));
                    },
                    'filter' => \yii\jui\DatePicker::widget([
                            'language' => 'en',
                            //'dateFormat' => 'yyyy-MM-dd',
                            'dateFormat' => 'MM/dd/yyyy',
                            'model' => $searchModel,
                            'attribute' => 'slot_date',
                            //'value' => 'slot_date',
                        ]
                    ),
                    'format' => 'html',
                ],
                //'start_time:time',
                [
                    'attribute' => 'start_time',
                    'value' => function($data){
                        return date("h:i A", strtotime($data->start_time));
                    },
                ],
                'duration',
//            [
//                'attribute'=>'is_reserved',
//                'filter'=>array("yes"=>"Yes","no"=>"No"),
//                'format'=>'raw',
//                'value'=> function($data){
//                    return ucfirst($data->is_reserved);
//                }
//            ],
                //'reserved_by_customer_id',


                [
                    'label'=>'Permit Number',
                    'attribute' => 'permit_number',
                    'format'=>'raw',
                    //'value'=>'permitnumber.learners_permit_number'
                    'value'=>function($data) {
                        if(@$data->permitnumber->learners_permit_number)
                            return Html::a(
                                $data->permitnumber->learners_permit_number,
                                Yii::getAlias('@web').'/index.php/customers/view/?id='.$data->reserved_by_customer_id,

                                [

                                    'target'=>'_blank',
//                                'data-toggle'=>'tooltip',
//                                'title'=>'Popover Header',
//                                'data-content'=>'Some content <br>inside the popover'
                                ]
                            );
                        else
                            return '';
                    },
                ],
                [
                    'attribute'=>'status',
                    'filter'=>array("Active"=>"Active","Inactive"=>"Inactive", "Reserved"=>"Reserved"),
                    'format'=>'raw',
                    'value'=> function($data){
                        return ucfirst($data->status);
                    }
                ],
                [
                    'label'=>'',
                    'format' => 'raw',
                    'value' => function($data){
                        if($data->status=='Reserved'){
                            return \app\models\Slots::getrescheduledate($data);
                        } else {
                            return '';
                        }

                    }
                ],
                [
                    'class' => 'yii\grid\CheckboxColumn',
                    // you may configure additional properties here
                ],

                //['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>

        <!--    --><?php //Pjax::end(); ?>
        <?

    }
    ?>




    <?= Html::endForm();?>

</div>



<?


$this->registerJs("
    $(document).ready(function(){
        $('#span-check-all-records').hide();
    })


    $('#status_dropdown').change(function(){
        if($(this).val()=='Reschedule'){
            //alert('ssss');
        }
    });


    $('.select-on-check-all').click(function(){
        if($('.select-on-check-all').prop('checked') == true){
            $('#span-check-all-records').show();
        } else {
            $('#checkbox-check-all-records').prop('checked',false);
            $('#span-check-all-records').hide();
        }
    })
");
?>