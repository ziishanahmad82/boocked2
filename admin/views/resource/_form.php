<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Resources */
/* @var $form yii\widgets\ActiveForm */
/* @var $servicesdata app\models\Services */
?>

<div class="resources-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'resource_name')->textInput(['maxlength' => true]) ?>
    <?=  $form->field($model, 'resource_status')
        ->dropDownList(
            //$items,           // Flat array ('id'=>'label')
            ['Active'=>'Active', 'Inactive'=>'Inactive']    // options
        );  ?>
    <? //= $form->field($model, 'resource_status')->textInput(['maxlength' => true]) ?>

    <?= Html::activeDropDownList($servicesdata, 'service_id',$items) ?>
    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
