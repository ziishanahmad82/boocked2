<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ActiveForm;
use app\models\Resources;

/* @var $resourcesCommonData app\models\ResourcesCommonName */
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Resources';
$this->params['breadcrumbs'][] = $this->title;
//print_r($dataProvider);

//print_r($ServicesList);
?>
<div class="col-lg-3 col-md-3 col-sm-3 no-padding-l media-xs-full">
    <div class="panel panel-default">
        <div class="panel-heading full2">
            <ul class="list-inline list-staff">
                <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web') ?>/img/settings.png"> <span class="staff-head">Manage Resources</span></h5></li>
                <li class="pull-right li-right"><i class="fa fa-plus plus-square"></i></li>
            </ul>
        </div>

        <form id="createform" method="post" action="<?= Yii::getAlias('@web') ?>/index.php/resource/create">
            <input type="hidden" id="resource_csrf" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
            <?php // $form = ActiveForm::begin(); ?>

            <? //= $form->field($model, 'resource_name')->textInput(['maxlength' => true]) ?>
            <?php // ActiveForm::end(); ?>
        <div class="panel-body panel-body-nav-tabs-sidebar" style="padding-bottom:20px">
            <div class="row">
                <div class="col-sm-12"
                <p style="padding-top:15px">Resources</p>
                <div class="form-group">
                    <input type="text" name="resource_name" class="form-control" id="resource_name" required placeholder="">
                </div>

            </div>
            <div class="col-sm-12">
            <p>Linked Designs</p>
            <div class="form-group">
                <div class="multiselect" style="width: 100%;">
                    <div class="selectBox" onclick="showCheckboxes(this)">
                        <select class="form-control">
                            <option>Select an option</option>
                        </select>
                        <div class="overSelect"></div>
                    </div>
                    <div class="resource_selectcheckboxes">
                        <label for="one123"><input type="checkbox" id="one123" value="linked_not" class="singlecheck resource_linkedcreate" name="linked_status" checked readonly/>Not Linked</label>
                        <label for="one"><input type="checkbox" id="one" value="linked_all" class="singlecheck resource_linkedcreate" name="linked_status"/>Linked to all</label>

                       <?php foreach($ServicesList as $serviceelist){ ?>
                           <label ><input type="checkbox" value="<?= $serviceelist->service_id ?>" name="service_data" class="other_servicecheck other_ResourceCreate" ><?= $serviceelist->service_name ?></label>

                     <?php   } ?>
                    </div>
                </div>
               <!-- <input type="text" class="form-control" placeholder="">-->
            </div>

        </div>
        <div class="col-lg-12">
            <button type="button" id="resource_createbut" class="btn btn-warning ad">Add</button>
        </div>

        </form>

    </div>
    </div>
    </div>
</div>
<div class="col-lg-9 col-md-9 col-sm-9 media-xs-full sed">
    <div class="panel panel-default">
        <div class="panel-heading full2">
            <ul class="list-inline list-staff">
                <li><h5 class="panel-title text-uppercase"><i class="fa fa-share-alt" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Resources</span></h5></li>
            </ul>
        </div>
        <div class="panel-body">
            <div class="row">
                <div class="tab-content">
                    <div class="tab-pane fade active in" id="service">
                        <div class="panel-collapse collapse in active">
                            <div class="panel-body" style="border: none;">
                                <div class="panel panel-default">
                                    <div class="panel-heading full2">
                                        <ul class="list-inline list-staff">
                                            <li><h5 class="panel-title"><i class="fa fa-file" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">General name for Resources</span></h5></li>
                                            <li class="pull-right li-right hidden-xs">General name for service</li>
                                        </ul>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p>Common name for Resource</p>
                                                <p style="margin-top: -11px;"><small class="text-muted">Example Room,Equipment etc</small></p>
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 20px;">
                                            <div class="col-lg-10 col-lg-offset-1">
                                                <div class="row">
                                                    <?php Pjax::begin(); ?>
                                                    <?php  $form = ActiveForm::begin(['id'=>'resourcecommonform', 'action' => 'savecommon',
                                                        'enableAjaxValidation' => true, 'validationUrl' => 'validate',
                                                        'fieldConfig' => ['options' => ['class' => 'col-lg-6 col-md-6 col-sm-6']],]); ?>
                                                    <?= $form->field($resourcesCommonData[0], 'singular_name',[ 'labelOptions' => [ 'class' => 'small text-muted']])->textInput(['maxlength' => true]) ?>

                                                    <?= $form->field($resourcesCommonData[0], 'plural_name', [ 'labelOptions' => [ 'class' => 'small text-muted']])->textInput(['maxlength' => true]) ?>

                                                    <?php ActiveForm::end();
                                                    Pjax::end(); ?>
                                                    <div class="col-lg-6 col-md-6 col-sm-6 field-servicescommonname-singular_name required" ><span class="savedMess" id="singularsaved" style="display:none;">saved</span></div>


                                                <div class="row">
                                                    <div class="col-lg-12">
                                                        <p><small class="text-muted">This will be shown t your clients at various places while booking. Sample sentences: "Please select (service)", Number of (services)."</small></p>
                                                    </div>
                                                </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php Pjax::begin(['id' => 'resource_reload']); ?>
                            <div class="panel-body" style="border: none;  margin-top: -2px;">
                                <div class="panel panel-default">
                                    <div class="panel-heading full2">
                                        <ul class="list-inline list-staff">
                                            <?php    $total_resource=  Resources::find()->where(['business_id'=>Yii::$app->user->identity->business_id])->all();
                                                     $inactive_resource =  Resources::find()->where(['business_id'=>Yii::$app->user->identity->business_id,'resource_status'=>'Inactive'])->all();
                                            ?>
                                            <li><h5 class="panel-title"><i class="fa fa-share-alt" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Resources</span></h5></li>
                                            <li class="pull-right li-right hidden-xs">Inactive: <?= count($inactive_resource) ?></li>
                                            <li class="pull-right li-right hidden-xs">Total Resources:<?=  count($total_resource) ?></li>

                                        </ul>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <div class="panel panel-default">
                                                    <div class="panel-heading">
                                                        <div class="row">
                                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                <p>Resources</p>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                <p>Linked Designs</p>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                                                                <p>Active</p>
                                                            </div>
                                                        </div>

                                                    </div>
                                                    <!--<div class="panel-body" style="background-color: rgba(221, 221, 221, 0.29)">

                                                        <div class="row col">
                                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 min">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" placeholder="">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 min">
                                                                <div class="form-group">
                                                                    <input type="text" class="form-control" placeholder="Not Linked">
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 min">
                                                                <div class="form-group">
                                                                    <label class="checkbox check" style="left:40px;top:-2px;height:24px">
                                                                        <input type="checkbox" class="">
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>-->
                                                    <div class="clearfix"></div>
                                                    <?php  if(!empty($dataProvider)){ ?>
                                                      <?php  foreach ($dataProvider as $model) {
                                                          ?>

                                                          <div class="row col resource_table_colum" style="margin-left:0;margin-right: 0;">
                                                              <form id="updateformresource<?= $model->resource_id ?>">
                                                                  <input type="hidden" id="resource_csrf" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" />
                                                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 min">
                                                                  <div class="form-group resource_tableview_<?= $model->resource_id ?>">
                                                                      <?= $model->resource_name ?>

                                                                  </div>
                                                                  <div class="form-group resource_tableedit_<?= $model->resource_id ?>" style="display:none">
                                                                      <input type="text" name="Resources[resource_name]" value="<?= $model->resource_name ?>" class="form-control resouce_nameeditf" />
                                                                  </div>

                                                              </div>
                                                              <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 min">
                                                                  <div class="multiselect">
                                                                      <!--<div class="selectBox" onclick="showCheckboxes()">
                                                                          <select>
                                                                              <option>Select an option</option>
                                                                          </select>
                                                                          <div class="overSelect"></div>
                                                                      </div>
                                                                      <div id="checkboxes">
                                                                          <label for="one"><input type="checkbox" id="one"/>First checkbox</label>
                                                                          <label for="two"><input type="checkbox" id="two"/>Second checkbox </label>
                                                                          <label for="three"><input type="checkbox" id="three"/>Third checkbox</label>
                                                                      </div>
                                                                  </div>-->
                                                                  <div class="form-group resource_tableview_<?= $model->resource_id ?>">
                                                                      <?php
                                                                      $linked_bar = 'No Linked';
                                                                      if($model->linked_status=="linked_all" ){
                                                                          echo 'Linked to All';
                                                                          $linked_bar = 'Linked to All';
                                                                      }else if($model->linked_status=="linked_all"){

                                                                          echo 'No Linked';
                                                                          $linked_bar = 'No Linked';
                                                                      }
                                                                      else if($model->services_data){
                                                                         $service_attach = json_decode($model->services_data);
                                                                         if(!empty($service_attach))
                                                                          $linked_bar = sizeof($service_attach).' Linked';
                                                                          echo $linked_bar;

                                                                      }


                                                                      ?>
                                                                   <?php
                                                                  //  $resservice = \app\models\Resources::get_resource_service($model->resource_id);
                                                                  // print_r($resservice); ?>
                                                                   
                                                                  </div>
                                                                      <div class="form-group resource_tableedit_<?= $model->resource_id ?>" style="display:none">



                                                                          <div class="multiselect">
                                                                              <div class="selectBox" onclick="showCheckboxes(this)">
                                                                                  <select class="form-control">
                                                                                      <option><?= $linked_bar ?></option>
                                                                                  </select>
                                                                                  <div class="overSelect"></div>
                                                                              </div>
                                                                              <?php // $serdata = implode(',' , $model->services_data);

                                                                             // print_r(array($serdata));
                                                                             $decodedsevice_data = json_decode($model->services_data);
                                                                              ?>

                                                                              <div class="resource_selectcheckboxes">
                                                                                  <label for="one123"><input type="checkbox" id="one123" value="linked_not" class="singlecheck resource_linkedcreate" name="Resources[linked_status]" checked readonly/>Not Linked</label>
                                                                                  <label for="one"><input type="checkbox" id="one" value="linked_all" class="singlecheck resource_linkedcreate" name="Resources[linked_status]"/>Linked to all</label>

                                                                                  <?php foreach($ServicesList as $serviceelist){ ?>

                                                                                      <label ><input type="checkbox" value="<?= $serviceelist->service_id ?>" <?php  if(!empty($decodedsevice_data)){ if(in_array($serviceelist->service_id,$decodedsevice_data )){ echo 'checked'; }} ?> name="Resources[service_data]" class="other_servicecheck other_ResourceCreate" ><?= $serviceelist->service_name ?></label>

                                                                                  <?php   } ?>
                                                                              </div>
                                                                          </div>







                                                                      </div>
                                                              </div>
                                                              </div>
                                                              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 min">
                                                                  <div class="form-group resource_tableview_<?= $model->resource_id ?>">
                                                                      <label class="checkbox check" style="top:-2px">
                                                                          <?= $model->resource_status ?>

                                                                      </label>
                                                                  </div>
                                                                  <div class="form-group resource_tableedit_<?= $model->resource_id ?>" style="display:none">
                                                                      <input type="checkbox" name="Resources[resource_status]" value="Active" <?php if($model->resource_status=='Active'){ echo 'checked'; } ?> class="resourceedit_status" />
                                                                  </div>
                                                              </div>
                                                              <div class="col-lg-2 col-md-2 col-sm-2 col-xs-6 min">
                                                                      <div class="form-group resource_tableview_<?= $model->resource_id ?>" >
                                                                          <label class="checkbox check" style="left:40px;top:-2px">
                                                                             <a href=".resource_tableedit_<?= $model->resource_id ?>" onclick="showResourceEditbox(event,'resource_tableedit_','resource_tableview_',<?= $model->resource_id ?>)" > Edit </a>
                                                                             <a href="<?= Yii::getAlias('@web') ?>/index.php/resource/delete/?id=<?= $model->resource_id ?>" class="deleteTableResource"> Delete </a>

                                                                          </label>
                                                                      </div>
                                                                  <div class="form-group resource_tableedit_<?= $model->resource_id ?>" style="display:none">
                                                                      <label class="checkbox check" style="top:2px">
                                                                          <a href=".resource_tableedit_<?= $model->resource_id ?>" onclick="UpdateTableresourceForm(event,this,<?= $model->resource_id ?>)"  > Update </a> <a href="#" onclick="showResourceEditbox(event,'resource_tableview_','resource_tableedit_',<?= $model->resource_id ?>)"> Cancel </a>

                                                                      </label>
                                                                  </div>
                                                              </div>
                                                              </form>
                                                              <div class="clearfix"></div>
                                                              </div>



                                                        <?php
                                                      //  echo "addMarker({$model->lat_field}, {$model->lon_field});";
                                                        }
                                                      ?>
                                                    <?php }else { ?>
                                                        <div class="row" style="background:antiquewhite;padding:10px;margin:0;"><p>No Resources Created</p></div>
                                                        
                                                 <?php   } ?>



                                                    <!--</div> -->

                                                </div>
                                            </div>
                                        </div>

                                    </div>

                                </div>

                            </div>
                            <?php Pjax::end(); ?>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="preference">
                        <div class="panel-collapse collapse in active">
                            <div class="col-lg-6">
                                <div class="panel-body" style="border: none;">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <ul class="list-inline list-staff">
                                                <li><h5 class="panel-title text-uppercase"><i class="fa fa-dollar" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Timezone and Currency</span></h5></li>
                                            </ul>
                                        </div>
                                        <div class="panel-body" style="padding-bottom: 46px;">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p>Timezone <small><a href="" class="txt-theme">Edit</a></small></p>
                                                    <p style="margin-top: -11px;"><small class="text-muted">Karachi, Islamabad (GMT +05:00)</small></p>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 20px;">
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <p>Time Format</p>
                                                    <div class="form-group">
                                                        <select class="form-control">
                                                            <option selected>12:00</option>
                                                            <option>12:05</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <p>Date Format</p>
                                                    <div class="form-group">
                                                        <select class="form-control">
                                                            <option selected>MM / DD / YY</option>
                                                            <option></option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 20px;">
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <p>Currency</p>
                                                    <small>PKR(Rs) <a href="" class="txt-theme">Edit</a></small>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <p>Currency Format</p>
                                                    <div class="form-group">
                                                        <select class="form-control">
                                                            <option selected>XX . XX . XX</option>
                                                            <option></option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="panel-body" style="border: none;">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <ul class="list-inline list-staff">
                                                <li><h5 class="panel-title text-uppercase"><i class="fa fa-briefcase" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Services</span></h5></li>
                                            </ul>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p>Language Preference</p>
                                                    <p style="margin-top: -11px;"><small class="text-muted">Example service, Message, Tour etc</small></p>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 20px;">
                                                <div class="col-lg-6">
                                                    <p>Default Languages</p>
                                                    <div class="form-group">
                                                        <select class="form-control">
                                                            <option selected>English</option>
                                                            <option>English</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <p>Select Languages</p>
                                                    <p class="text-muted">
                                                        <small>
                                                            Selsct any languages that meet your customer requirements. Your customer will b able to select any of the languages you have chosen.
                                                        </small>
                                                    </p>
                                                    <p><small><a href="#" class="txt-theme">Select languages here</a></small></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="panel-body" style="border: none;">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <ul class="list-inline list-staff">
                                                <li><h5 class="panel-title text-uppercase"><i class="fa fa-user" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Personal Information</span></h5></li>
                                            </ul>
                                        </div>
                                        <div class="panel-body" style="padding-bottom: 46px;">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <p>First Name</p>
                                                    <div class="form-group">
                                                        <input type="text" placeholder="First Name" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <p>Last Name</p>
                                                    <div class="form-group">
                                                        <input type="text" placeholder="Last Name" class="form-control">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 20px;">
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <p>Email</p>
                                                    <small class="text-muted">namename@gmail.com <a href="" class="txt-theme">Edit</a></small>
                                                </div>
                                                <div class="col-lg-6 col-md-6 col-sm-6">
                                                    <p>Password</p>
                                                    <small class="text-muted">********* <a href="" class="txt-theme">Change Password</a></small>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="panel-body" style="border: none;">
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <ul class="list-inline list-staff">
                                                <li><h5 class="panel-title text-uppercase"><i class="fa fa-phone-square" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Contact Information</span></h5></li>
                                            </ul>
                                        </div>
                                        <div class="panel-body">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <p>Home Phone</p>
                                                    <div class="form-group">
                                                        <input placeholder="Enter Number" class="form-control" type="text">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <p>Work Phone</p>
                                                    <div class="form-group">
                                                        <input placeholder="Enter Number" class="form-control" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row" style="margin-top: 20px;">
                                                <div class="col-lg-6">
                                                    <p>Mobile Phone</p>
                                                    <div class="form-group">
                                                        <input placeholder="Enter Number" class="form-control" type="text">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="bhours">
                        <div class="panel-collapse collapse in active">
                            <div class="panel-body" style="border: none;">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <ul class="list-inline list-staff">
                                            <li><h5 class="panel-title text-uppercase"><i class="fa fa-hourglass" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Business Hours</span></h5></li>
                                            <li class="pull-right li-right">Link service and staff to generate business hours</li>
                                        </ul>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row" style="margin-top: 20px;">
                                            <div class="col-lg-12">
                                                <ul class="nav nav-tabs">
                                                    <li class="active"><a aria-expanded="true" href="#schedule" data-toggle="tab">Work Schedule</a></li>
                                                    <li><a aria-expanded="true" href="#unavailability" data-toggle="tab" style="border-top-right-radius: 4px;">Future Unavailability</a></li>
                                                </ul>
                                                <small><a href="" class="font" style="color: #FC7600;position: absolute; right: 15px; top: 10px;">Add or update schedule</a></small>
                                                <div class="tab-content">
                                                    <div class="tab-pane fade in active" id="schedule">
                                                        <div class="panel-collapse collapse in active">
                                                            <div class="panel-body panel-table">
                                                                <ul class="list-inline" style="padding: 15px 15px 0;">
                                                                    <li><p>Current Schedule <small><a href="" style="color: #FC7600;">Edit</a></small></p></li>
                                                                    <li style="margin-left: 15px;"><small class="text-muted">Current . Forever</small></li>
                                                                    <li class="pull-right"><small class="text-muted">Add time to schedule <span class="txt-theme">(Mouse over any time to see option to delete that time)</span></small></li>
                                                                </ul>
                                                                <table class="table table-responsive table-striped">
                                                                    <thead>
                                                                    <tr style="background: #EAEAEA;">
                                                                        <th>Service</th>
                                                                        <th>Sunday</th>
                                                                        <th>Monday</th>
                                                                        <th>Tuesday</th>
                                                                        <th>Wednesday</th>
                                                                        <th>Thursday</th>
                                                                        <th>Friday</th>
                                                                        <th>Saturday</th>
                                                                    </tr>
                                                                    </thead>
                                                                    <tbody>
                                                                    <tr>
                                                                        <td>
                                                                            <ul class="list-unstyled">
                                                                                <li>
                                                                                    <small><b>Graphic Design (12h)</b></small>
                                                                                </li>
                                                                                <li><small class="text-muted">Scale: <span class="txt-theme">30 min</span></small></li>
                                                                            </ul>
                                                                        </td>
                                                                        <td></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td>
                                                                            <ul class="list-unstyled">
                                                                                <li>
                                                                                    <small><b>Graphic Design (12h)</b></small>
                                                                                </li>
                                                                                <li><small class="text-muted">Scale: <span class="txt-theme">30 min</span></small></li>
                                                                            </ul>
                                                                        </td>
                                                                        <td></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                        <td><small class="text-muted">From 10:00 AM to 5:00 PM</small></td>
                                                                    </tr>
                                                                    </tbody>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tab-pane fade" id="unavailability">
                                                        <div class="panel panel-default" style="padding: 15px;">
                                                            <div class="panel-heading" style="padding-bottom: 35px;">

                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><i class="fa fa-calendar" style="margin-right:6px;color:#FC7600"></i>Unavailable dates</div>
                                                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 first"><a href="" style="color: #FC7600;position: absolute; right: 15px; top: 10px;">Add additional times</a></div>

                                                            </div>
                                                            <div class="panel-body" style="padding: 0;padding-bottom:40px;padding-top:40px">

                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center">
                                                                    <i class="fa fa-calendar fa-2x"></i>
                                                                </div>
                                                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                                                    <small class="text-muted" style="float:right">
                                                                        Block a particular date.<a href="" class="txt-theme">(Mouse over any time to see option to delete that time)</a></small>
                                                                    </small>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default" style="padding: 15px;">
                                                            <div class="panel-heading" style="padding-bottom: 35px;">

                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12"><i class="fa fa-calendar" style="margin-right:6px;color:#FC7600"></i>Unavailable times</div>
                                                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 first"><a href="" style="color: #FC7600;position: absolute; right: 15px; top: 10px;">Add additional times</a></div>

                                                            </div>
                                                            <div class="panel-body" style="padding: 0;padding-bottom:40px;padding-top:40px;">

                                                                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 text-center">
                                                                    <i class="fa fa-calendar fa-2x"></i>
                                                                </div>
                                                                <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9">
                                                                    <small class="text-muted" style="float:right">
                                                                        Block a particular date.<a href="" class="txt-theme">(Mouse over any time to see option to delete that time)</a></small>
                                                                    </small>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="panel panel-default" style="padding: 15px;">
                                                            <div class="panel-heading fir" style="padding-bottom: 35px;">

                                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12"><i class="fa fa-calendar" style="margin-right:6px;color:#FC7600"></i>Times blocked from google calendar</div>
                                                                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 first"><a href="" style="color: #FC7600;position: absolute; right: 15px; top: 10px;">Add additional times</a></div>

                                                            </div>
                                                            <div class="panel-body" style="padding: 0;padding-bottom:20px;padding-top:20px">

                                                                <div class="col-lg-3 text-center">
                                                                </div>
                                                                <div class="col-lg-9">
                                                                    <small class="text-muted" style="float:right">
                                                                        Block a particular date.<a href="" class="txt-theme">(Mouse over any time to see option to delete that time)</a></small>
                                                                    </small>
                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body" style="border: none;">
                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <ul class="list-inline list-staff">
                                            <li><h5 class="panel-title text-uppercase"><i class="fa fa-calendar" style="color: #FC7600; position: relative; top: 3px;"></i> <span class="staff-head">Date Specific / Irregular Times</span></h5></li>
                                            <li class="pull-right li-right"><small><a href="" class="txt-theme">Add additional times</a></small></li>
                                        </ul>
                                    </div>
                                    <div class="panel-body">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <p style="margin-top: 5px;"><small class="text-muted">Add unavailability  outside of regular working hours (e.g. 10:am to 1:00 pm on Sunday, January 1st) <a href="" class="txt-theme">(Mouse over any time to see option to delete that time)</a></small></p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="panel panel-default" style="padding: 15px;">
                                                <div class="panel-heading" style="padding-bottom: 35px;">
                                                    <ul class="list-inline list-staff">
                                                        <li class="col-lg-3 text-center">Date</li>
                                                        <li class="col-lg-9">Design</li>
                                                    </ul>
                                                </div>
                                                <div class="panel-body" style="padding: 0;">
                                                    <table class="table table-responsive table-striped">
                                                        <tbody>
                                                        <tr>
                                                            <td class="col-lg-3 text-center">
                                                                <i class="fa fa-calendar fa-2x"></i>
                                                            </td>
                                                            <td class="col-lg-9">
                                                                <small class="text-muted">
                                                                    Additional work schedule operates independently to the regular work schedule. This option allows you to open specific or reoccuring dates and time forexample: Open times on this Sunday. Open every 3rd Saturday.
                                                                </small>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <ul style="margin-left: 60px;">
                                                <li><small>As per lead team setting, booking will be allowed 120 minutes from now for the next 90 days.</small></li>
                                                <li><small>Time will open in an interval of 30 minutes. Change interval</small></li>
                                                <li><small>You can create multiple schedules forexample, 'Winter schedule' or 'Summer schedule from here'.</small></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-customer-details">
            <div class="modal-dialog modal-dialog-customer">
                <div class="modal-content">
                    <div class="modal-header">
                        <ul class="list-inline list-modal-header">
                            <li class="text-uppercase"><i class="fa fa-user" style="color: #FC7600;"></i> customer</li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                                <a href=""><i class="fa fa-reply"></i></a>
                            </li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                                <a href=""><i class="fa fa-plus"></i></a>
                            </li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban" id="ban-btn-toggle"><i class="fa fa-ban"></i></a></li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay" id="pay-btn-toggle"><i class="fa fa-dollar"></i></a></li>
                            <li class="pull-right" style="font-size: 12px;">Gift Certificates / Discount Coupons</li>
                        </ul>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body-inner-contents">
                            <div class="row">
                                <div class="col-lg-7 col-md-7 col-sm-7">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <i class="fa fa-plus upload-image" data-toggle="tooltip" data-placement="top" title="Upload Image"></i>
                                            <img src="images/staff2.png">
                                        </div>
                                        <div class="col-lg-8">
                                            <h4 class="txt-theme" style="position: relative; top: 9px;">John Doe</h4>
                                            <p><small>Johndoe@gmail.com</small></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <ul class="list-inline" style="margin-top: 15px;">
                                                <li><small><a href="" class="txt-theme">Invite to schedule online</a></small></li><li class="txt-theme"><small>|</small></li>
                                                <li><small><a href="" class="txt-theme">Verify</a></small></li><li class="txt-theme"><small>|</small></li>
                                                <li><small><a href="" class="txt-theme">Edit</a></small></li><li class="txt-theme"><small>|</small></li>
                                                <li><small><a href="" class="txt-theme">Delete</a></small></li>
                                            </ul>
                                            <button class="btn btn-main" style="padding: 4px 9px;"><i class="fa fa-plus"></i> Add Tags</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-lg-5 col-md-5 col-sm-5 media-col6">
                                    <div class="row">
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                            <i class="fa fa-map-marker txt-theme"></i>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                            <p><small>145 strait street, CT.No City, No Region United States(01234)</small></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1">
                                            <i class="fa fa-mobile txt-theme"></i>
                                        </div>
                                        <div class="col-lg-10 col-md-10 col-sm-10 col-xs-10">
                                            <p><small>098-879-46548</small></p>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12" style="margin-top: 8px;"><button class="btn btn-main" style="padding: 3px 4px;"><small>Send Gift Certificates</small></button></div>
                                        <div class="col-lg-12" style="margin-top: 5px;"><button class="btn btn-main" style="padding: 3px 4px;"><small>Send Discount Coupons</small></button></div>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 20px;">
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <textarea class="form-control" rows="2" placeholder="Add Information"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="modal-body-inner-tabs">
                                <ul class="nav nav-tabs nav-main nav-customer-details">
                                    <li class="active">
                                        <a href="#up-app" data-toggle="tab">Upcoming Appointments</a>
                                    </li>
                                    <li>
                                        <a href="#past-app" data-toggle="tab">Past Appointments</a>
                                    </li>
                                    <li>
                                        <a href="#payments" data-toggle="tab">Payments</a>
                                    </li>
                                    <li>
                                        <a href="#promotion" data-toggle="tab">Promotion</a>
                                    </li>
                                </ul>
                                <div class="tab-content">
                                    <div class="tab-pane fade in active" id="up-app">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="past-app">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="payments">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="tab-pane fade" id="promotion">
                                        <div class="panel-collapse collapse in active">
                                            <div class="panel-body">
                                                <div class="row row-custom">
                                                    <div class="col-lg-12">
                                                        <div class="panel panel-default">
                                                            <p>No Upcoming Appointments</p>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="ban">
            <div class="modal-dialog modal-dialog-customer">
                <div class="modal-content">
                    <div class="modal-header">
                        <ul class="list-inline list-modal-header">
                            <li class="text-uppercase"><i class="fa fa-user" style="color: #FC7600;"></i> customer booking restrictions</li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                                <a href=""><i class="fa fa-reply"></i></a>
                            </li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                                <a href=""><i class="fa fa-plus"></i></a>
                            </li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban"><i class="fa fa-ban"></i></a></li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay"><i class="fa fa-dollar"></i></a></li>
                        </ul>
                    </div>
                    <div class="modal-body">
                        <div class="modal-body-inner-contents">
                            <div class="alert alert-warning">
                                <i class="fa fa-warning"></i> <small>Client Advance setting is a premium feature available with PRO and above packages. <a href="#" class="txt-theme">Upgrade your account </a> or <a href="" class="txt-theme">Compare various Packages</a></small>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>This Customer is</p>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option selected>Post-Paying</option>
                                            <option>Post-Paying</option>
                                            <option>Post-Paying</option>
                                        </select>
                                    </div>
                                    <p class="text-muted">
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                        </small>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>Future Bookings Status</p>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option selected>As per settings</option>
                                            <option>Post-Paying</option>
                                            <option>Post-Paying</option>
                                        </select>
                                    </div>
                                    <p class="text-muted">
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                        </small>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p>With Restriction</p>
                                    <div class="form-group">
                                        <select class="form-control">
                                            <option selected>As per settings</option>
                                            <option>Post-Paying</option>
                                            <option>Post-Paying</option>
                                        </select>
                                    </div>
                                    <p class="text-muted">
                                        <small>
                                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras vitae mi eu purus faucibus porttitor at sit amet ex. Donec condimentum, magna sit amet porta feugiat, eros leo rhoncus
                                        </small>
                                    </p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <ul class="list-inline pull-right">
                                        <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;">Cancel</button></li>
                                        <li><button class="btn btn-main" style="padding-right: 19px; padding-left: 19px;">Save</button></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="pay">
            <div class="modal-dialog modal-dialog-customer">
                <div class="modal-content">
                    <div class="modal-header">
                        <ul class="list-inline list-modal-header">
                            <li class="text-uppercase"><i class="fa fa-dollar" style="color: #FC7600;"></i> add membership payment</li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Export Customer">
                                <a href=""><i class="fa fa-reply"></i></a>
                            </li>
                            <li class="pull-right" data-toggle="tooltip" data-placement="bottom" title="Add New Customer">
                                <a href=""><i class="fa fa-plus"></i></a>
                            </li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#ban"><i class="fa fa-ban"></i></a></li>
                            <li class="pull-right"><a href="" data-toggle="modal" data-target="#pay"><i class="fa fa-dollar"></i></a></li>
                        </ul>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <p>Payment Date</p>
                                <div class="form-group">
                                    <input type="text" placeholder="4-17-2016" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <p>Payment For</p>
                                <div class="form-group">
                                    <input type="text" placeholder="Add New Payment" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <p class="text-muted pull-right" style="margin-top: -9px;"><small>Customer restrictions of the selected membership will be applied automatically.</small></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <textarea class="form-control" rows="2" placeholder="Note against payment"></textarea>
                                    <p class="text-danger pull-right"><small>Please do not enter html tags</small></p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="modal-body-inner-tabs">
                                <div class="row">
                                    <div class="col-lg-4">
                                        <p class="txt-theme">Amount</p>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Additional Charges">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <span style="position: absolute; left: -2px;">$</span> <input type="text" class="form-control" placeholder="$$$">
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Discount">
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group">
                                            <span style="position: absolute; left: -6px;">by</span>
                                            <select class="form-control" placeholder="Cash">
                                                <option selected="">Cash</option>
                                                <option>d</option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <input type="text" class="form-control" placeholder="Total">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-12">
                                        <ul class="list-inline pull-right">
                                            <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;">Cancel</button></li>
                                            <li><button class="btn btn-main" style="padding-right: 19px; padding-left: 19px;">Save</button></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
























<div class="clearfix"></div>

<?php
$script = "
$('form#resourcecommonform :text').focusout(function(){
    $('form#resourcecommonform').submit();
   
});
$('body').on('beforeSubmit', 'form#resourcecommonform', function () {
var form = $(this);
// return false if form still have some validation errors
if (form.find('.has-error').length) {
return false;
}
// submit form
$.ajax({
url: form.attr('action'),
type: 'post',
data: form.serialize(),
success: function (response) {
    if(response==1){
        $('#singularsaved').css('display','inline-block');
       setTimeout(function(){
         $('#singularsaved').hide('slow');
            },2000);

    }else {
        alert('not updated');

    }
}
});
return false;
});

$('input.singlecheck').on('change', function() {
if($(this).is(':checked')){
//var cuurentite = $(this);
//$(this).closest('div').child('input').prop('checked', false);
 $(this).parents().siblings('label').children('input').not(this).prop('checked', false);
//alert($(this).closest('div').attr('id'));
}
   //$(this).closest('div').child('input').not(this).prop('checked', false);
});
////////////// Resource form submit
$('#resource_createbut').click(function(event){
event.preventDefault();
var form = $(this).parents('form');
  var formdate =  $(this).parents('form').attr('id');
  console.log(formdate);
   var resource_name = $('#resource_name').val();
   console.log('name:'+resource_name);
   var csrf = $('#resource_csrf').val();
    console.log('csrf:'+csrf);
   var linked_status = $('.resource_linkedcreate:checked').val();
   if(!linked_status){
   linked_status='NULL';
   }
  var service_data =[]
 var service_data = $('#createform .other_ResourceCreate:checked').map(function() { return $(this).val(); }).get();
 service_data = JSON.stringify(service_data);
 console.log(service_data);
 $.ajax({
url: form.attr('action'),
type: 'post',
data: {'_csrf':csrf,'Resources[resource_name]':resource_name,'Resources[linked_status]':linked_status, 'Resources[services_data]':service_data},
success: function (response) {
    if(response==1){
     //   alert('changed');

    }else {
      //  alert('not updated');

    }
}
});     
});
$('input.other_servicecheck').on('change', function() {

if($(this).is(':checked')){
$(this).parents().siblings('label').children('input.singlecheck').prop('checked', false);
  
}
});
$('.deleteTableResource').click(function(e){
e.preventDefault();
var cur_delete = $(this)
 var curretload = $(this).attr('href');
  $.ajax({
            url: curretload,
            type: 'post',
             success: function (response) {
             if(response==1){
             cur_delete.closest('.row.col').hide();
             
             }
             
             }
            });
});


"

;
$this->registerJs($script);
?>
<script>
   // onclick="showResourceEditbox(event,resource_tableview_,resource_tableedit_,<? //= $model->resource_id ?>,)"

    function showResourceEditbox(event,showclass,hideclass,id){
        event.preventDefault();
        $('.'+hideclass+id).hide();
        $('.'+showclass+id).show();


    }
    var expanded = false;
    function showCheckboxes(currentselect) {
        var checkboxes = $(currentselect).siblings('.resource_selectcheckboxes');
      //  var checkboxes = document.getElementById("checkboxes");
        if (!expanded) {
            checkboxes.show();
            expanded = true;
        } else {
            checkboxes.hide();
            expanded = false;
        }
    }
    function UpdateTableresourceForm(event,crrentupdate,id){
        event.preventDefault();
        var formcr = $(crrentupdate).closest('form').attr('id');

       /// var dataall = $(crrentupdate).closest('form').serializeArray();
       // var service_data = new array();
       // $('#'+formcr+' .other_ResourceCreate:checked').each(function(){
      //      service_data.push($(this).val());
      //  });
     //   console.log(service_data);
       var service_data = $('#'+formcr+' .other_ResourceCreate:checked').map(function() { return $(this).val(); }).get();
      //  console.log(service_data);
        service_data = JSON.stringify(service_data);
        var resource_name = $('#'+formcr+' .resouce_nameeditf').val();
        var csrf = $('#'+formcr+' #resource_csrf').val();
        console.log('csrf:'+csrf);
        var linked_status = $('#'+formcr+' .resource_linkedcreate:checked').val();

        if(!linked_status){
            linked_status='NULL';
        }
        var resource_status = $('#'+formcr+' .resourceedit_status:checked').val();
        if(!resource_status){
            resource_status="Inactive";

        }
        $.ajax({
            url: '<?= Yii::getAlias('@web') ?>/index.php/resource/update?id='+id,
            type: 'post',
            data: {'_csrf':csrf,'Resources[resource_name]':resource_name,'Resources[linked_status]':linked_status, 'Resources[services_data]':service_data,'Resources[resource_status]':resource_status},
            //{'_csrf':csrf,'Resources[resource_name]':resource_name,'Resources[linked_status]':linked_status, 'Resources[services_data]':service_data},
            success: function (response) {
                if(response=='saved'){
                    $.pjax.reload({container:'#resource_reload'});

                }else {
                    //  alert('not updated');

                }
            }
        });



    }
</script>
<style>
    .multiselect {
        width: 200px;
        position:relative;
    }
    .selectBox {
        position: relative;
    }
    .selectBox select {
        width: 100%;
        font-weight: bold;
    }
    .overSelect {
        position: absolute;
        left: 0; right: 0; top: 0; bottom: 0;
    }
    .resource_selectcheckboxes {
        display: none;
        border: 1px #dadada solid;
        background: #fff none repeat scroll 0 0;
        padding: 0 10px;
        position: absolute;
        width: 100%;
        z-index: 10001;
    }
    .resource_selectcheckboxes label {
        display: block;
    }
    .resource_selectcheckboxes label:hover {
        background-color: #1e90ff;
    }
    .row.col{
        min-height:80px;

    }
</style>
