<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\jui\DatePicker;
use app\models\SoldGiftCertificates;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gift Certificates';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
   .inlinedatepicker {
       display:inline-block;
       max-width:100px;

   }

</style>

    <div class="row">

      <!--  col-sm-12 media-xs-full-->

        <div class="">
            <ul class="gift_certifcates_tabs">
                <li >
                    <a href="<?= Yii::getAlias('@web')?>/index.php/marketing/giftcertificates" >Design</a>
                </li>
                <li class="active">
                    <a href="<?= Yii::getAlias('@web')?>/index.php/marketing/manage_giftcertificates" >Manage</a>
                </li>
            </ul>
            <div class="panel panel-default ">
                <div class="panel-heading full">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/coupons.png"> <span class="staff-head">Manage Certificates</span></h5></li>
                    </ul>
                </div>
                <div class="tab-pane fade in active" id="first">
                    <div class="panel-collapse collapse in active">
                        <div class="panel-body panel-marketing" style="height: auto">

                            <div class="row collapse" id="new_certificate">
                                <div class="col-sm-6" >
                                    <div class='inner' style="background:#EAEAEA">
                                        <div class="clearfix"></div>
                                        </div>
                                </div>

                            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="scroller-tab">
                    <table class="table table-responsive table-striped tab-reports" id="manage_cerificates">
                        <thead>
                        <tr class="table-panel-head row-panel-head">
                            <th><span class="hidden-xs"> Code</</th>
                            <th><span class="hidden-xs">Purchased By</span></th>
                            <th><span class="hidden-xs">Purchased</span></th>
                            <th><span class="hidden-xs">Purchased On</span></th>
                            <th><span class="hidden-xs">Amount</span></th>
                            <th><span></span></th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php

                        foreach($model as $gift){ ?>
                          <?php  $gift_certificate = SoldGiftCertificates::giftrecord($gift->gc_id); ?>
                            <tr>
                                <td><?= $gift->certificate_code ?></td>
                                <td><?= $gift->from_email ?></td>
                                <td>at <?= $gift->purchased_at ?></td>
                                <td><?= $gift_certificate->gift_selling_price ?></td>
                                <td><?= date('d M, Y',strtotime($gift->created_at)) ?></td>
                                <td></td>

                            </tr>
                        <?php } ?>
                        </tbody>



                    </table>
                    </div>
                </div>
            </div>



                        </div>
                    </div>
                </div>

                
            </div>
        </div>
    </div>


<!--<script type="text/javascript">
    var appointy = 'abdulrehman';
    var scheduleoHeight = '700';
    var scheduleoWidth = '900';
    var ShowSchedulemeImg = true;
    //if showSchedulemeImg is set to false then it will override the properties below. This can be used if you want to call overlay from your own custom link.
    var ScheduleMeBgImg = 'http://static.appointy.com/Widget/Images/scheduleme.png';
    var ScheduleMeBg = 'transparent';
    var ScheduleMeWidth = '47';
    var ScheduleMeHeight = '150';
    var ScheduleMePosition = 'right';  // right, left
    // You can also call function ShowAppointyInOverlay() onclick of any tag.
    // e.g. <a href="javascript:void(0)" onclick="ShowAppointyInOverlay();">Schedule with us</a>
</script>
<script type="text/javascript" src="http://localhost/boocked/admin/web/index.php/js/staticpopup.js"></script>-->














<?php
$script = "
$('#manage_cerificates').DataTable( {
dom: 'Bfrtip',
buttons: [
'copy', 'csv', 'excel', 'pdf', 'print'
]
} );
       
        ";

$this->registerJs($script);
?>
<script>
    function showmodel(){
     $('#gift_certificate_images').modal('show');

    }


</script>


<?php
Modal::begin([
    'header' => '<h5 style="margin:0">Edit Certificate</h5>',
    'id' => 'gift_certificate_edit',
    'size' => 'modal-md',
//'htmlOptions' => ['style' => 'width:800px;']
]);
   echo '<div id="modalContent"></div>';

Modal::end();

?>

<?php
Modal::begin([
    'header' => '<h5 style="margin:0">Sell Certificate</h5>',
    'id' => 'sell_certificate',
    'size' => 'modal-sm',
//'htmlOptions' => ['style' => 'width:800px;']
]);
echo '<div id="modalContent"></div>';

Modal::end();

?>



<?php
Modal::begin([
    'header' => '<h5 style="margin:0">Change Image</h5>',
    'id' => 'gift_certificate_images',
    'size' => 'modal-md',
//'htmlOptions' => ['style' => 'width:800px;']
]); ?>
<div id='modalContent'>
    <div class="col-xs-2" style="padding:0">
        <label>Custom URL:</label>
    </div>
    <div class="col-xs-8">
    <input type="text" class="form-control">
    </div>
    <div class="col-xs-2">
        <button class="btn">Apply</button>
    </div>
    <div class="col-xs-12" style="padding-left:0">
        <p><small>Please enter complete URL (http://www.boocked.com/header.jpg) of your image. The size of the image should be 540 x 180 (px)</small></p>
    </div>
    <div class="col-xs-12">
        <h5>Pre-Design</h5>
    </div>
    <div class="col-sm-12">
        <div class="col-xs-4">
            <img src="<?= Yii::getAlias('@web') ?>/img/giftCertificate_01.png" class="pre_design_img" style="width:75px" />
        </div>
        <div class="col-xs-4">
            <img src="<?= Yii::getAlias('@web') ?>/img/giftCertificate_03.png" class="pre_design_img" style="width:75px" />
        </div>
        <div class="col-xs-4">
            <img src="<?= Yii::getAlias('@web') ?>/img/giftCertificate_05.png" class="pre_design_img" style="width:75px" />
        </div>
    </div>
</div>
<div class="clearfix" ></div>
<?php
Modal::end();

?>