<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use boundstate\mailgun;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Marketing';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$form = 'asdas';
//, ['contactForm' => $form]
//Yii::$app->mailer->compose()
//->setFrom('from@domain.com')
//->setTo('sikandar.maqbool@tabsusa.com')
//->setSubject('this is subject')->setTextBody('It is so simple to send a message.')
//->send();
//exit();
?>
<div class="container-fluid section-main">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 no-padding-l media-xs-full">


            <div class="panel panel-default" style="visibility:hidden">
                <div class="panel-heading">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/staff-icon.png"> <span class="staff-head">Staff</span></h5></li>
                        <li class="pull-right li-right"><i class="fa fa-plus plus-square"></i></li>
                    </ul>
                </div>
                <div class="panel-body staff-div">
                    <div class="row">
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff2.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff2.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>

                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff2.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                            <img src="<?= Yii::getAlias('@web')?>/img/staff1.png">
                            <p><small class="title-sm">John Doe</small></p>
                        </div>
                    </div>
                </div>
            </div >

           <!-- <div class="panel panel-default">
                <div class="panel-heading">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/analytics.png"> <span class="staff-head">analytics</span></h5></li>
                        <li class="pull-right li-right"><i class="fa fa-plus plus-square"></i></li>
                    </ul>
                </div>
                <div class="panel-body section-analytics">

                </div>
            </div>-->

        </div>

        <div style="display:none;" class="col-lg-4 col-md-4 col-sm-4 div-relative pull-right div-relative-marketing">
            <ul class="list-inline pull-right">
                <li><a href="#" data-toggle="tooltip" data-placement="left" data-original-title="Add Service"><i class="fa fa-plus fa-add-services"></i></a></li>
                <li><a href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="Help"><i class="fa fa-question fa-question-mark"></i></a></li>
            </ul>
        </div >
        <div class="col-lg-9 col-md-9 col-sm-9 media-xs-full panel-market">
            <ul class="nav nav-tabs nav-main" style="border-bottom:none">
                <li class="active">
                    <a href="#first" data-toggle="tab">Widgets</a>
                </li>
                <li>
                    <a href="#second" data-toggle="tab">Social Media Channels</a>
                </li>
                <li>
                    <a href="#third" data-toggle="tab">Invite Contacts</a>
                </li>
                <!--<li>
                    <a href="#forth" data-toggle="tab">Get Viral</a>
                </li>-->
                <li>
                    <a href="#fourcal" data-toggle="tab">Add 3rd Party Calendar</a>
                </li>
                <li class="caret-dropdown">
                    <a href="" data-toggle="tab"><i class="fa fa-caret-down"></i></a>
                </li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="first">
                    <div class="panel-collapse collapse in active">
                        <div class="panel panel-body panel-marketing">
                            <div class="row row-custom">
                                <div class="col-lg-12">
                                    <h4 class="text-uppercase text-center">welcome to boocked marketing wizard</h4>
                                    <p style="margin-top: 3%;">
                                        <small class="text-muted text-size">
                                            We will help you through your journey to get maximum impact from our platform.
                                            Just choose the appropriate option in each step and click Continue to move to the next. You can always skip a step by clicking on the links above.
                                        </small>
                                    </p>
                                    <p style="margin-top: 2%;">Would you like to add our scheduling widget to your existing website?</p>
                                    <ul class="list-inline list-radio">
                                        <li class="radio">
                                            <label>
                                                <input type="radio" name="select" value="yes" checked>
                                                Yes
                                            </label>
                                        </li>
                                        <li class="radio">
                                            <label>
                                                <input type="radio" name="select" value="no">
                                                No
                                            </label>
                                        </li>
                                        <li class="radio">
                                            <label>
                                                <input type="radio" name="select" value="not_sure">
                                                I am not sure
                                            </label>
                                        </li>
                                        <li class="pull-right btn-continuation">
                                            <button class="btn btn-main">Continue</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-lg-12">
                                    <p style="margin-top: 2%;">
                                        <small class="text-muted text-size">
                                            You can add boocking.com's widget to your existing website or use our system to create a free website with our scheduling tools.
                                            You can also manage your social media channels and other features from above menu to get maximum impact from Boocking.com. Copy and paste one line code into your webpage and start accepting appointments directly on your website.
                                        </small>
                                    </p>
                                </div>
                            </div>
                            <div class="radio-toggle">
                                <div class="row" style="margin-top: 2%;">
                                    <div class="col-lg-12">
                                        <ul class="nav nav-tabs nav-toggle">
                                            <li class="active">
                                                <a href="#subtab-1" data-toggle="tab">Full Page Widget</a>
                                            </li>
                                            <li>
                                                <a href="#subtab-2" data-toggle="tab">Overlay Widget</a>
                                            </li>
                                            <li>
                                                <a href="#subtab-3" data-toggle="tab">Schedule Button</a>
                                            </li>
                                            <li>
                                                <a href="#subtab-4" data-toggle="tab">Facebook Page Widget</a>
                                            </li>
                                            <li>
                                                <a href="#subtab-5" data-toggle="tab" style="border-top-right-radius: 4px;">Your own Boocked website</a>
                                            </li>
                                        </ul>
                                        <div class="tab-content">
                                            <div class="tab-pane fade in active" id="subtab-1">
                                                <div class="panel-collapse collapse in active">
                                                    <div class="panel-body subpanel-1">
                                                        <div class="row">
                                                            <div class="col-lg-6 col-md-6 col-sm-6" style="padding-top:36px">
                                                                <? //= Yii::getAlias('@webroot')?>
                                                                <img src="<?= Yii::getAlias('@web')?>/img/overlay.PNG" class="img-xs" width="100%" style="border-radius: 8px; margin: 10px;">
                                                            </div>
                                                            <div class="col-lg-6 col-md-6 col-sm-6">
                                                                <div class="content-inner">
                                                                    <div class="description-inner text-muted " id="full_page_code" style="height:auto;min-height:200px;">

                                                                                &lt;iframe src="//abdulrehman.appointy.com/?isGadget=1" width="760px" height="555px" scrolling="auto" frameborder="0" allowtransparency="true"&gt;&lt;/iframe&gt;

                                                                                <!--<iframe src="//abdulrehman.appointy.com/?isGadget=1" width="760px" height="555px" scrolling="auto" frameborder="0" allowtransparency="true"></iframe>--></code></pre>


                                                                    </div>
                                                                    <button class="btn btn-main pull-right btn-copy" onclick="clipboard('full_page_code')">Copy Code</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="subtab-2">
                                                <div class="panel-collapse">
                                                    <div class="panel-body subpanel-1">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <img src="<?= Yii::getAlias('@web')?>/img/overlay.PNG" class="img-xs" width="100%" style="border-radius: 8px; margin: 10px;padding-top:20%">
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="content-inner">
                                                                    <div class="description-inner text-muted " id="sidebar_button_code">

                                                                        for now its working change its variable name when we creates subdomain for users<br/> please copy this code and paste in your site


                                                                        &lt;script type="text/javascript"&gt;<br />
                                                                        var appointy = 'abdulrehman';<br />
                                                                        var scheduleoHeight = '700';<br />
                                                                        var scheduleoWidth = '900';<br />
                                                                        var ShowSchedulemeImg = true;<br />
                                                                        var ScheduleMeBgImg = 'http://static.appointy.com/Widget/Images/scheduleme.png';<br />
                                                                        var ScheduleMeBg = 'transparent';<br />
                                                                        var ScheduleMeWidth = '47';<br />
                                                                        var ScheduleMeHeight = '150';<br />
                                                                        var ScheduleMePosition = 'right';  // right, left<br />
                                                                        <br />
                                                                        &lt;/script&gt;<br />
                                                                        &lt;script type="text/javascript" src="&lt;?= Yii::getAlias('@web')?&gt;/js/staticpopup.js"&gt;&lt;/script&gt;
                                                                    </div>
                                                                    <button class="btn btn-main pull-right btn-copy" onclick="clipboard('sidebar_button_code')">Copy Code</button>
                                                                   <!-- <button class="btn btn-main pull-right btn-copy">Copy Code</button> -->
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="subtab-3">
                                                <div class="panel-collapse collapse in active">
                                                    <div class="panel-body subpanel-1">
                                                        <div class="row">
                                                            <div class="col-lg-6 text-center">
                                                                <div class="content-inner container-btns">
                                                                    <button class="btn btn-main" style="padding-right: 31px; padding-left: 31px;">Schedule Now</button></li>
                                                                </div>
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="content-inner">
                                                                    <div class="description-inner text-muted " id="schedule_button_code">
                                                                        &lt;a href="<?= str_replace('/admin/web', "/customer/web/index.php",Yii::getAlias('@web') );?>" target="_blank"&gt;&lt;img src="http://appointy.com/Images/scheduleme.png" alt="" border="0" /&gt;&lt;/a&gt;

                                                                    </div>
                                                                    <button class="btn btn-main pull-right btn-copy" onclick="clipboard('schedule_button_code')">Copy Code</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="subtab-4">
                                                <div class="panel-collapse">
                                                    <div class="panel-body subpanel-1">
                                                        <div class="row">
                                                            <div class="col-lg-6">
                                                                <img src="<?= Yii::getAlias('@web')?>/img/fb-plugin.png" class="img-rounded img-xs" width="100%" style="margin: 10px;">
                                                            </div>
                                                            <div class="col-lg-6">
                                                                <div class="content-inner">
                                                                    <div class="description-inner text-muted ">

                                                                        <div class="fb-login-button" data-scope="manage_pages" data-max-rows="1" data-size="medium"></div>


                                                                        <div id="pageBtn" class="btn btn-success clearfix">Click me to show the list of Pages you admin.</div>

                                                                        <ul id="pagesList" class="btn-group btn-group-vertical clearfix"></ul>
                                                                        <div id="fb-root"></div>






                                                                        <script>
                                                                      /*  FB.getLoginStatus(function(response) {
                                                                        if (response.status === 'connected') {

                                                                        var uid = response.authResponse.userID;
                                                                        var accessToken = response.authResponse.accessToken;
                                                                        } else if (response.status === 'not_authorized') {
                                                                        // the user is logged in to Facebook,
                                                                        // but has not authenticated your app
                                                                        } else {
                                                                            alert('not login');
                                                                        // the user isn't logged in to Facebook.
                                                                        }
                                                                        }); */
                                                                        </script>




















                                                                        <script>(function(d, s, id) {
                                                                                var js, fjs = d.getElementsByTagName(s)[0];
                                                                                if (d.getElementById(id)) return;
                                                                                js = d.createElement(s); js.id = id;
                                                                                js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7&appId=273749176329186";
                                                                                fjs.parentNode.insertBefore(js, fjs);
                                                                            }(document, 'script', 'facebook-jssdk'));
                                                                        </script>
                                                                        <script>
                                                                          //  EAAD4ZBTJpoZBIBAE4kdj1y6...eZCT7ClO0LxyZCSqiWgZDZD
                                                                            document.getElementById('pageBtn').onclick = function() {
                                                                               // console.log(response.data);
                                                                               // exit();
                                                                                FB.init({appId: "273749176329186", status: true, cookie: true,version:'v2.7'});
                                                                            //   console.log(FB);
                                                                            //    $.ajax({
                                                                            //        url: 'http://localhost/boocked/admin/web/index.php/marketing/insertfb',
                                                                            //        type: 'POST',
                                                                              //      data: {'fb':FB},
                                                                            //        success: function () {
                                                                             //           alert('aaa');
                                                                             //       }
                                                                            //    });
                                                                                <? //= $_SESSION['fb'] ?> FB.api('/me/accounts?fields=name,access_token,link', function(response) {
                                                                                    console.log(response);
                                                                                  //  console.log(response.error.message);
                                                                                  //  exit();
                                                                                    // Log.info('API response', response);
                                                                                    var list = document.getElementById('pagesList');
                                                                                    for (var i=0; i < response.data.length; i++) {
                                                                                        var li = document.createElement('li');
                                                                                        li.innerHTML = response.data[i].name;
                                                                                      //  li.innerHTML = response.data[i];
                                                                                        console.log(response.data[i]);
                                                                                        li.dataset.token = response.data[i].access_token;
                                                                                        li.dataset.link = response.data[i].link;
                                                                                        li.className = 'btn btn-mini';
                                                                                        li.onclick = function() {
                                                                                            document.getElementById('pageName').innerHTML = this.innerHTML;
                                                                                            //  document.getElementById('pageToken').innerHTML = this.dataset.token;
                                                                                            document.getElementById('pageLink').setAttribute('href', this.dataset.link);
                                                                                        }
                                                                                        list.appendChild(li);
                                                                                    }
                                                                                });
                                                                               // exit();

                                                                           //     FB.getLoginStatus(function(response) {
                                                                             //       console.log(response);
                                                                                 //   console.log(FB);
                                                                                //    if (response.status === 'connected') {
                                                                                  //      console.log('connected');
                                                                                  //      console.log(response.authResponse.accessToken);
                                                                                 //   }else {
                                                                                 //       console.log('not conected');

                                                                               //     }
                                                                             //   });

                                                                                    FB.api('/me/accounts?fields=name,access_token,link', function(response) {
                                                                                   // Log.info('API response', response);
                                                                                    var list = document.getElementById('pagesList');
                                                                                    for (var i=0; i < response.data.length; i++) {
                                                                                        var li = document.createElement('li');
                                                                                        li.innerHTML = response.data[i].name;
                                                                                      //  li.innerHTML = response.data[i];
                                                                                       // console.log(response.data[i]);
                                                                                        li.dataset.token = response.data[i].access_token;
                                                                                         var tkn = response.data[i].access_token;
                                                                                        console.log('aaaaa');
                                                                                        FB.api('/oauth/access_token?grant_type=fb_exchange_token&client_id=273749176329186&client_secret=a48679861238dd1a0b846c364120fc4a&fb_exchange_token='+tkn,function(response1){
                                                                                            console.log(response1.error);
                                                                                        });
                                                                                    //    $.ajax({
                                                                                      //      url:'/oauth/access_token?grant_type=fb_exchange_token&client_id=273749176329186&client_secret=a48679861238dd1a0b846c364120fc4a&fb_exchange_token='+tkn',
                                                                                      //      success: function (response1) {
                                                                                      //          console.log(response1);
                                                                                       //     }
                                                                                      //  });
                                                                                        li.dataset.link = response.data[i].link;
                                                                                        li.className = 'btn btn-mini';
                                                                                        li.onclick = function() {
                                                                                            document.getElementById('pageName').innerHTML = this.innerHTML;
                                                                                          //  document.getElementById('pageToken').innerHTML = this.dataset.token;
                                                                                            document.getElementById('pageLink').setAttribute('href', this.dataset.link);
                                                                                        }
                                                                                        list.appendChild(li);
                                                                                    }
                                                                                });
                                                                                return false;
                                                                            }
                                                                        </script>

                                                                        <hr />



                                                                        <span id="pageName" class="label label-success">No page selected</span>
                                                                        <!--<span id="pageToken" class="label label-success">EAAD4ZBTJpoZBIBANxMJmNjZBvXwPova0JbnYuybWROI7QGqX82q3RySm2Xyn7WYBZCHdWeSJZCDsNqxPesD0aFzLgzZBfLRrRTBLCGKMulZBqC2NhatZC2sHISH2tVqCLepysWT2wGb11DCyjmT21VuBZC2XPqTsZCFqGUZA0ZBxRasGiAZDZD</span>-->
                                                                        <div id="publishBtn" class="btn btn-success">Publish me!</div>

                                                                        <script>
                                                                            document.getElementById('publishBtn').onclick = function() {
                                                                                var pageToken = document.getElementById('pageToken').innerHTML;
                                                                             //   , access_token: pageToken
                                                                              //  console.log(FB);
                                                                              //  exit();
                                                                                FB.api('/me/feed', 'post', {message: 'Hello, world!'}, function(response) {
                                                                               //     Log.info('API response', response);
                                                                                    console.log(response);
                                                                                    document.getElementById('publishBtn').innerHTML = 'API response is ' + response.error.message;
                                                                                });
                                                                                return false;
                                                                            }
                                                                        </script>

                                                                        <p>Now go look at <a id="pageLink" href="#">your page</a> and if the API request was successful, you'll see the Hello, World post in the feed.</p>

                                                                        <hr />





                                                                    </div>
                                                                    <button class="btn btn-main pull-right btn-copy">Connect with facebook</button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="tab-pane fade" id="subtab-5">
                                                <div class="panel-collapse collapse in active">
                                                    <div class="panel-body subpanel-1">
                                                        <div class="row">
                                                            <div class="col-lg-6" style="padding-top:28px">
                                                                <img src="<?= Yii::getAlias('@web')?>/img/oursite.PNG" class="img-xs" width="100%" style="border-radius: 8px; margin: 10px;">
                                                            </div>
                                                            <div class="col-lg-6 text-center">
                                                                <div class="content-inner container-btns" style="height:auto;padding-top:90px;min-height:200">
                                                                    <ul class="list-unstyled">
                                                                        <li><a href="http://boocked.com/theapp/customer/web/index.php/" target="_blank" class="btn btn-main" style="padding-right: 31px; padding-left: 31px;">Visit Your Website</a></li>
                                                                        <li style="margin-top: 10px;"><a class="btn btn-main">Customize Your Website</a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="second">
                    <div class="panel-collapse collapse in active">
                        <div class="panel-body panel-marketing" style="height:auto;">
                            <p>Update your social networks to include information on scheduling with Boocked</p>
                            <button id="fbsharebutton1" class="fb-btn clearfix">

                                <i class="fa fa-facebook" aria-hidden="true"></i>Share on Facebook</button>
                            <button id="twiiter_button" class="tw-btn" onclick="window.open('https://twitter.com/intent/tweet?text=Now%20accepting%20appointments%20online%20at%20&original_referer=http%3A%2F%2Fboocked.com&tw_p=tweetbutton&url=http%3A%2F%2Fboocked.com','','width=300,height=300,scrollbars=yes')">
                                <i class="fa fa-twitter" aria-hidden="true"></i>Share on Twitter</button>




                            <script type="text/javascript" async src="https://platform.twitter.com/widgets.js"></script>
                            <p style="margin-top: 50px"><hr /></p>

                            <script>

                                document.getElementById('fbsharebutton1').onclick = function() {
                                    FB.ui({
                                        method: 'share',
                                        display: 'popup',
                                        href: 'http://boocked.com/',
                                    }, function(response){});
                                }
                            </script>
                         </div>
                    </div>
                 </div>
                <div class="tab-pane fade" id="third">
                    <div class="panel-collapse collapse in active">
                        <div class="panel-body panel-marketing" style="height:auto;">
                            <p>Let your current customers and friends know about Boocked page.</p>

                            <button id="fbsharebutton1" class="fb-btn clearfix">

                                <i class="fa fa-facebook" aria-hidden="true"></i>Share on Facebook</button>
                            <button id="twiiter_button" class="tw-btn" onclick="window.open('https://twitter.com/intent/tweet?text=Now%20accepting%20appointments%20online%20at%20&original_referer=http%3A%2F%2Fboocked.com&tw_p=tweetbutton&url=http%3A%2F%2Fboocked.com','','width=300,height=300,scrollbars=yes')">
                                <i class="fa fa-twitter" aria-hidden="true"></i>Share on Twitter</button>




                            <script type="text/javascript" async src="https://platform.twitter.com/widgets.js"></script>
                            <p style="margin-top: 50px"><hr /></p>

                            <script>
                                document.getElementById('fbsharebutton1').onclick = function() {
                                    FB.ui({
                                        method: 'share',
                                        display: 'popup',
                                        href: 'http://boocked.com/',
                                    }, function(response){});
                                }
                            </script>

                            <div class="col-sm-12 attach_offerbox">
                                <div class="offerbox_1" >
                                <h4>Attach an invite offer</h4>
                                <p>You can invite customers with or without a discount, but customers are more likely to schedule a booking if they have an incentive.</p>
                                <p>Select the best discount that you can offer to your clients, or click on the link to invite customers to schedule without a discount offer.</p>

                                <input type="text" class="form-control" id="discount_number" value="10" />
                                <select class="form-control" id="discount_unit">
                                    <option value="%">%</option>
                                    <option value="<?= Yii::$app->session->get('bk_currency'); ?>"><?= Yii::$app->session->get('bk_currency'); ?></option>
                                </select>
                                <br/>
                                <div class="col-sm-6 pull-right text-right" >
                                <a href="#" id="invite_vnot_discount">Invite without a discount</a>  or  <button class="btn btn-main" id="invite_v_discount" >Invite with a discount</button>
                                </div>
                                </div>

                                <div class="offerbox_2" style="display:none;" >
                                    <h4>Invite your fans from your Facebook</h4>
                                    <p></p>
                                    <p>Select the best discount that you can offer to your clients, or click on the link to invite customers to schedule without a discount offer.</p>

                                   <!-- <input type="text" class="form-control"  />-->
                                   <textarea id="facebook_post_content" class="form-control" > </textarea>
                                    <br/>
                                    <div class="col-sm-6 pull-right text-right" >
                                        <a href="#" onclick="skipoffers('.offerbox_2','.offerbox_3',event)">Skip</a>  or  <button id="post_on_fb" class="fb-btn clearfix"><i class="fa fa-facebook" aria-hidden="true"></i>Post on Facebook</button>
                                    </div>
                                    <script>
                                    document.getElementById('post_on_fb').onclick = function() {
                                        document.getElementById('post_on_fb');
                                    FB.ui({
                                    method: 'share',
                                    title:'thos tis title',
                                    quote:'this is asi quote',
                                    // href:'http://boocked.com/theapp/admin/web/index.php/marketing/coupanshare',
                                    name:'this is name',
                                    picture:'http://boocked.com/theapp/admin/web/img/comun.png',
                                    href:'http://boocked.com/theapp/admin/web/index.php/marketing/coupanshare?title=thos tis title',
                                    caption: 'this is caption',
                                    description:'this is description',
                                                }, function(response){});
                                            }
                                        </script>

                                </div>
                                <div class="offerbox_3" style="display:none;" >
                                    <h4>Invite your followers on Twitter</h4>
                                    <p></p>
                                    <p>Permission is needed to post on your behalf. Here is what will be posted. Feel free to edit it.</p>


                                    <textarea class="form-control" id="post_on_twitter_content"></textarea>
                                    <br/>
                                    <div class="col-sm-6 text-right pull-right" >
                                       <!-- <a href="">Skip</a>  or  --><button id="post_to_button" class="tw-btn" >
                                            <i class="fa fa-twitter" aria-hidden="true"></i>Post on Twitter</button>
                                    </div>
                                    <script>
                                      /*  document.getElementById('fbsharebutton123456').onclick = function() {
                                            FB.ui({
                                                method: 'share',
                                                // href:'http://boocked.com/theapp/admin/web/index.php/marketing/coupanshare',
                                                name:'this is name',
                                                picture:'http://boocked.com/theapp/admin/web/img/comun.png',
                                                href:'http://boocked.com/theapp/admin/web/index.php/marketing/coupanshare',
                                                caption: 'this is caption',
                                                description:'this is description',
                                            }, function(response){});
                                        }  */
                                    </script>

                                </div>


                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="fourcal">
                    <div class="panel-collapse collapse in active">
                        <div class="panel-body panel-marketing" style="height:auto;">
                            <p>Sync with google calendar.</p>
                         <?php
                         //   GET https://outlook.office.com/api/v2.0/me/calendarview?startDateTime={start_datetime}&endDateTime={end_datetime}

                          //  $curl = curl_init();
                            // Set some options - we are passing in a useragent too here
                          //  curl_setopt_array($curl, array(
                          //  CURLOPT_RETURNTRANSFER => 1,
                           // CURLOPT_URL => 'https://outlook.office.com/api/v2.0/me/events/AAMkAGE0MGM1Y2M5LWEAAA=/instances?startDateTime=2014-10-01T01:00:00Z&endDateTime=2016-12-31T23:00:00Z&$select=Subject,Start,End',
                            //CURLOPT_USERAGENT => 'Codular Sample cURL Request'
                          //   'CURLOPT_URL' => 'https://outlook.office.com/api/v2.0/me/calendars/{calendar_id}/events'
                           // ));
                            //abdulrehmanphp%40gmail.com
                            //adeeelbaigg%40gmail.com
                            // Send the request & save response to $resp
                         //   $resp = curl_exec($curl);
                            // Close request to clear up some resources
                         //   curl_close($curl);
                          //  echo '<pre>';
                          //  print_r(json_decode($resp));
                        // echo '</pre>';
                         ?>




                            <button id="google_sync" class="fb-btn clearfix" style="background:#EA4335"><i class="fa fa-google" aria-hidden="true"></i>Sync with Google Calendar</button>
                            <button id="outlook_sync" class="fb-btn clearfix" style="background:" onclick="window.open('https://outlook.office.com/common/oauth2/authorize?client_id=6bdea8aa-8d0f-4ae7-8070-6ec39114d3d2&redirect_uri=http%3A%2F%2Fboocked.com%2Fphp-calendar%2Fo365%2Fauthorize.php&response_type=code','targetWindow',width=300, height=300);" ><i class="fa fa-outlook" aria-hidden="true"></i>Sync with Outlook Calendar</button>
                         <!--   <button id="twiiter_button" class="tw-btn" onclick="window.open('https://twitter.com/intent/tweet?text=Now%20accepting%20appointments%20online%20at%20&original_referer=http%3A%2F%2Fboocked.com&tw_p=tweetbutton&url=http%3A%2F%2Fboocked.com','','width=300,height=300,scrollbars=yes')">
                                <i class="fa fa-twitter" aria-hidden="true"></i>Share on Twitter</button>-->




                            <script type="text/javascript" async src="https://platform.twitter.com/widgets.js"></script>
                            <p style="margin-top: 50px"><hr /></p>

                            <script>
                                document.getElementById('fbsharebutton1').onclick = function() {
                                    FB.ui({
                                        method: 'share',
                                        display: 'popup',
                                        href: 'http://boocked.com/',
                                    }, function(response){});
                                }
                            </script>

                            <div class="col-sm-12 attach_offerbox">
                                <div class="offerbox_1" >
                                    <h4>Attach an invite offer</h4>
                                    <p>You can invite customers with or without a discount, but customers are more likely to schedule a booking if they have an incentive.</p>
                                    <p>Select the best discount that you can offer to your clients, or click on the link to invite customers to schedule without a discount offer.</p>

                                    <input type="text" class="form-control" id="discount_number" value="10" />
                                    <select class="form-control" id="discount_unit">
                                        <option value="%">%</option>
                                        <option value="<?= Yii::$app->session->get('bk_currency'); ?>"><?= Yii::$app->session->get('bk_currency'); ?></option>
                                    </select>
                                    <br/>
                                    <div class="col-sm-6 pull-right text-right" >
                                        <a href="#" id="invite_vnot_discount">Invite without a discount</a>  or  <button class="btn btn-main" id="invite_v_discount" >Invite with a discount</button>
                                    </div>
                                </div>

                                <div class="offerbox_2" style="display:none;" >
                                    <h4>Invite your fans from your Facebook</h4>
                                    <p></p>
                                    <p>Select the best discount that you can offer to your clients, or click on the link to invite customers to schedule without a discount offer.</p>

                                    <!-- <input type="text" class="form-control"  />-->
                                    <textarea id="facebook_post_content" class="form-control" > </textarea>
                                    <br/>
                                    <div class="col-sm-6 pull-right text-right" >
                                        <a href="#" onclick="skipoffers('.offerbox_2','.offerbox_3',event)">Skip</a>  or  <button id="post_on_fb" class="fb-btn clearfix"><i class="fa fa-facebook" aria-hidden="true"></i>Post on Facebook</button>
                                    </div>
                                    <script>
                                        document.getElementById('post_on_fb').onclick = function() {
                                            document.getElementById('post_on_fb');
                                            FB.ui({
                                                method: 'share',
                                                title:'thos tis title',
                                                quote:'this is asi quote',
                                                // href:'http://boocked.com/theapp/admin/web/index.php/marketing/coupanshare',
                                                name:'this is name',
                                                picture:'http://boocked.com/theapp/admin/web/img/comun.png',
                                                href:'http://boocked.com/theapp/admin/web/index.php/marketing/coupanshare?title=thos tis title',
                                                caption: 'this is caption',
                                                description:'this is description',
                                            }, function(response){});
                                        }
                                    </script>

                                </div>
                                <div class="offerbox_3" style="display:none;" >
                                    <h4>Invite your followers on Twitter</h4>
                                    <p></p>
                                    <p>Permission is needed to post on your behalf. Here is what will be posted. Feel free to edit it.</p>


                                    <textarea class="form-control" id="post_on_twitter_content"></textarea>
                                    <br/>
                                    <div class="col-sm-6 text-right pull-right" >
                                        <!-- <a href="">Skip</a>  or  --><button id="post_to_button" class="tw-btn" >
                                            <i class="fa fa-twitter" aria-hidden="true"></i>Post on Twitter</button>
                                    </div>
                                    <script>
                                        /*  document.getElementById('fbsharebutton123456').onclick = function() {
                                         FB.ui({
                                         method: 'share',
                                         // href:'http://boocked.com/theapp/admin/web/index.php/marketing/coupanshare',
                                         name:'this is name',
                                         picture:'http://boocked.com/theapp/admin/web/img/comun.png',
                                         href:'http://boocked.com/theapp/admin/web/index.php/marketing/coupanshare',
                                         caption: 'this is caption',
                                         description:'this is description',
                                         }, function(response){});
                                         }  */
                                    </script>

                                </div>


                            </div>
                        </div>
                    </div>
                </div>


                
            </div>
        </div>
    </div>
</div>





<!--<script type="text/javascript">
    var appointy = 'abdulrehman';
    var scheduleoHeight = '700';
    var scheduleoWidth = '900';
    var ShowSchedulemeImg = true;
    //if showSchedulemeImg is set to false then it will override the properties below. This can be used if you want to call overlay from your own custom link.
    var ScheduleMeBgImg = 'http://static.appointy.com/Widget/Images/scheduleme.png';
    var ScheduleMeBg = 'transparent';
    var ScheduleMeWidth = '47';
    var ScheduleMeHeight = '150';
    var ScheduleMePosition = 'right';  // right, left
    // You can also call function ShowAppointyInOverlay() onclick of any tag.
    // e.g. <a href="javascript:void(0)" onclick="ShowAppointyInOverlay();">Schedule with us</a>
</script>
<script type="text/javascript" src="http://localhost/boocked/admin/web/index.php/js/staticpopup.js"></script>-->




























<a href="http://abdulrehman.appointy.com/" target="_blank"><img src="http://appointy.com/Images/scheduleme.png" alt="" border="0" /></a>
<?php
$script = " 
        $('input[name=select]:radio').click(function () {
            if ($('input[name = select]:checked').val() == 'yes') {
                $('.radio-toggle').show();
            }else if ($('input[name = select]:checked').val() == 'no') {
                $('.radio-toggle').hide();
                $('.tab-toggle').hide();
                $('.row-toggle').show();
            }else   if ($('input[name = select]:checked').val() == 'no') {
                $('.radio-toggle').hide();
                $('.tab-toggle').hide();
                $('.row-toggle').show();
            }
        });
        
      
      $(document).on('click', '#invite_vnot_discount', function(event) { 
         event.preventDefault();
         
         $('#facebook_post_content').val('Now offering online scheduling that allows scheduling from desktop, mobile or tablet even @ 2am in the morning.');
          $('#post_on_twitter_content').val('".$businessInformation->business_name." (#".\app\models\ProfessionsList::profession_name($businessInformation->profession).") is now accepting appointments online 24x7. boocked.com/theapp/customer/web/index.php');
           $('.offerbox_1').hide();
           $('.offerbox_2').show();
         
        });
        
        $(document).on('click', '#invite_v_discount', function(event) { 
         event.preventDefault();
         var discount_number = $('#discount_number').val();
         var discount_unit   = $('#discount_unit').val();
         if($.isNumeric(discount_number)){
         
            $('#facebook_post_content').val(discount_number+discount_unit+' off  on  Any Service  by  Any Staff  for  Any Client.* Works on Any Weekday.');
            
            $('#post_on_twitter_content').val(discount_number+discount_unit+' off at ".$businessInformation->business_name." (#".\app\models\ProfessionsList::profession_name($businessInformation->profession).")');
           $('.offerbox_1').hide();
           $('.offerbox_2').show();
            } else {
            alert('Please enter numeric value');
            }
        });
       $(document).on('click', '#post_to_button', function(event) {
         var twitter_content =  $('#post_on_twitter_content').val();
         twitter_content = twitter_content.split(' ').join(';');
        window.open('https://twitter.com/intent/tweet?text='+twitter_content+'&original_referer=http%3A%2F%2Fboocked.com&tw_p=tweetbutton&url=http%3A%2F%2Fboocked.com','','width=300,height=300,scrollbars=yes');
        });
        
        
        // Google Sync
        $('#google_sync').click(function(event){
            event.preventDefault();
        $.ajax({
        url: '".Yii::getAlias('@web')."/index.php/marketing/googleauth',
        type: 'post',
        success: function (response) {
           window.open(response);

   

        }
        });
        
         });
        
        
        ";

$this->registerJs($script);
?>
<script>
    function skipoffers(hidedob,dobs,event){
        event.preventDefault();
        $(hidedob).hide();
        $(dobs).show();


    }

      //  $("#login img").on("click", function () {
     ////       FB.login(function(response) {
     //           if (response.authResponse) {
    //                _wdfb_notifyAndRedirect();
    //            }
   //         });
  //      });



</script>
<script type="text/javascript">
    var appointy = 'abdulrehman';
    var scheduleoHeight = '700';
    var scheduleoWidth = '900';
    var ShowSchedulemeImg = true;
    //if showSchedulemeImg is set to false then it will override the properties below. This can be used if you want to call overlay from your own custom link.
    var ScheduleMeBgImg = 'http://static.appointy.com/Widget/Images/scheduleme.png';
    var ScheduleMeBg = 'transparent';
    var ScheduleMeWidth = '47';
    var ScheduleMeHeight = '150';
    var ScheduleMePosition = 'right';  // right, left
    // You can also call function ShowAppointyInOverlay() onclick of any tag.
    // e.g. <a href="javascript:void(0)" onclick="ShowAppointyInOverlay();">Schedule with us</a>


    function getSelectionText(){
        var selectedText = "";
        if (window.getSelection){ // all modern browsers and IE9+
            selectedText = window.getSelection().toString();
        }
        return selectedText
    }
    function selectElementText(el){
        var range = document.createRange(); // create new range object
        range.selectNodeContents(el); // set range to encompass desired element text
        var selection = window.getSelection(); // get Selection object from currently user selected text
        selection.removeAllRanges(); // unselect any user selected text (if any)
        selection.addRange(range); // add range to Selection object to select it
    }
    function copySelectionText(){
        var copysuccess // var to check whether execCommand successfully executed
        try{
            copysuccess = document.execCommand("copy") // run command to copy selected text to clipboard
        } catch(e){
            copysuccess = false
        }
        return copysuccess
    }





    function clipboard(ID_value)
    {
   //     alert(ID_value);

   //     var copyclip =  document.getElementById(ID_value).createTextRange();
     //  console.log(copyclip);
     //   copyclip.select();
     //   copyclip.execCommand("Copy");

        var para = document.getElementById(ID_value);
    //    selectElementText(para);
   //     var paratext = getSelectionText();
        selectElementText(para); // select the element's text we wish to read
        var copysuccess = copySelectionText();
      //  alert(paratext);

    }

</script>
<script type="text/javascript" src="<?= Yii::getAlias('@web')?>/js/staticpopup.js"></script>


