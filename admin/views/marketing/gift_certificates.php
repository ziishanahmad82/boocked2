<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gift Certificates';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
   .inlinedatepicker {
       display:inline-block;
       max-width:100px;

   }

</style>


    <div class="row">


        <div class="col-lg-4 col-md-4 col-sm-4 div-relative pull-right div-relative-marketing">
            <!--<ul>
                <li><a href="#" data-toggle="tooltip" data-placement="left" data-original-title="Add Service"><i class="fa fa-plus fa-add-services"></i></a></li>
                <li><a href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="Help"><i class="fa fa-question fa-question-mark"></i></a></li>
            </ul>-->
            <ul class="list-inline pull-right">
                <li><button class="certifcate_btn btn blue-btn dropdown-toggle" data-target="#new_certificate" data-toggle="collapse">Create new Gift Certificate</button></li>
                <li><button class="certifcate_btn btn  green-btn dropdown-toggle" data-target="#my_certificate" data-toggle="collapse">My Gift certificates</button></li>
            </ul>
        </div>
        <div class="">
            <ul class="gift_certifcates_tabs">
                <li class="active">
                    <a href="<?= Yii::getAlias('@web')?>/index.php/marketing/giftcertificates" >Design</a>
                </li>
                <li>
                    <a href="<?= Yii::getAlias('@web')?>/index.php/marketing/manage_giftcertificates" >Manage</a>
                </li>


            </ul>


                    <div class="panel-collapse collapse in active">


                            <div class="collapse " id="new_certificate">
                              <!--  <div class="col-sm-6" >-->
                                <div class="col-lg-4 col-md-4 col-sm-4 no-padding-l media-xs-full">
                                    <div class="panel panel-default coupon-panel">
                                        <div class="panel-heading full">
                                            <ul class="list-inline list-staff">
                                                <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/coupons.png"> <span class="staff-head">Create an Offer</span></h5></li>
                                            </ul>
                                        </div>
                                        <div class="panel-body panel-body-nav-tabs-sidebar">
                                    <div class='col-sm-12 gift_certificate_form_cont inner' style="padding-left:0;padding-right:0;" >
                                    <p style="margin-top: 2%;">   </p>
                                        <?php
                                        // 'enableAjaxValidation' => true, 'validationUrl' => 'customize_emailvalidate',
                                            $form = ActiveForm::begin(['id'=>'new_giftcertificate_form','action' => Yii::getAlias('@web').'/index.php/marketing/create_gift_certificate',
                                            'options' => ['class' => 'generate_email_template']]);  ?>
                                        <p>This gift certificate can be purchased for</p>
                                        <div class="row">
                                        <div class="col-xs-12">
                                            <?= $form->field($new_gift_certificate, 'gift_value')->textInput(['maxlength' => true,'class' => 'form-control'])->label('Value') ?>
                                        </div>
                                        <div class="col-xs-12">
                                            <?= $form->field($new_gift_certificate, 'gift_selling_price')->textInput(['maxlength' => true,'class' => 'form-control'])->label('Selling Price') ?>
                                        </div>


                                    <div class="col-xs-12 form_cont">
                                        <p>Gift certificate redemption validity</p>
                                        <p>
                                        <input type="radio" name="GiftCertificates[gift_validity]" value="0" /> Forever <br/>
                                        <input type="radio" name="GiftCertificates[gift_validity]" value="1" id="untill_radio" /> Until fixed date <?= DatePicker::widget([
                                                'id'=>'until',
                                                'name' => 'GiftCertificates[gift_validity_date]',
                                                'options' => ['class' => 'form-control inlinedatepicker'],
                                                'dateFormat' => 'yyyy-MM-dd',
                                                'clientOptions' => [
                                                    'format' => 'L',

                                                ],

                                            ]);?> <br/>
                                        <input type="radio" name="GiftCertificates[gift_validity]" id="validity_radio" value="2" /><input type="number" name="GiftCertificates[gift_validity_day]" id="validity_text" min="1" class="form-control inlinedatepicker" >   days after purchase <br/>
                                        </p>
                                            <? //= $form->field($new_gift_certificate, 'gift_value')->textInput(['maxlength' => true,'class' => 'form-control'])->label('Template Name') ?>
                                    </div>
                                    <div class="col-sm-12 form_cont">
                                        <br/>
                                        <p>Number of gift certificates for sale</p>
                                        <p><input type="radio" name="GiftCertificates[sale_limit]" value="0"  /> No limit <br/>
                                        <input type="radio" id="limit_radio" name="GiftCertificates[sale_limit]" value="1"  />Limit to <input type="number" name="GiftCertificates[gift_sale_limit]" id="limit_text" min="1" class="form-control inlinedatepicker" > </p>
                                    </div>
                                    <div class="col-sm-12 form_cont">
                                        <br/>
                                        <p>Gift certificate purchase window</p>
                                        <p><input type="radio" name="GiftCertificates[purchase_window]" value="1" /> Forever unless deactivated<br/>
                                            <input type="radio" name="GiftCertificates[purchase_window]" id="till_radio" value="0" /> Till date  <?= DatePicker::widget([
                                                'id'=>'till_text',
                                                'name' => 'GiftCertificates[purchase_window_date]',
                                                'options' => ['class' => 'form-control inlinedatepicker'],
                                                'dateFormat' => 'yyyy-MM-dd',
                                                'clientOptions' => [
                                                    'format' => 'L',

                                                ],

                                            ]);?>
                                        </p>

                                    </div>
                                    <div class="col-sm-12">
                                       <h5>Gift certificate banner image</h5>
                                        <input type="hidden" name="GiftCertificates[gift_image]" value=""  id="selected_img_val" />
                                        <img src="<?= Yii::getAlias('@web') ?>/img/giftCertificate_01.png" id="selected_img" style="width:75px" />
                                        <span onclick="showmodel()">Change Image</span>

                                    </div>
                                    <div class="col-xs-6">
                                        <? //= $form->field($new_gift_certificate, 'gift_value')->textInput(['maxlength' => true,'class' => 'form-control'])->label('Template Name') ?>
                                    </div>
                                        <?//= $form->field($new_gift_certificate, 'template_subject')->textInput(['maxlength' => true,'class' => 'form-control'])->label('Subject Line') ?>
                                      
                                        <br/>
                                       <div class="col-sm-12">
                                           <br/>
                                           <br />
                                        <?= Html::submitButton($new_gift_certificate->isNewRecord ? 'Save' : 'Update', ['class' => $new_gift_certificate->isNewRecord ? 'btn btn-main' : 'btn btn-main']) ?>
                                       </div>
                                       </div>
                                        <?php ActiveForm::end(); ?>

                                        <div class="clearfix"></div>
                                        </div>
                                            </div>
                                </div>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 media-xs-full">

                                    <div class="panel panel-default">
                                        <div class="panel-heading full">
                                            <ul class="list-inline list-staff">
                                                <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/coupons.png"><span class="staff-head">Coupons</span></h5></li>
                                            </ul>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6" style="float: none; margin: 30px auto;">
                                    <div class='inner' style="background:#EAEAEA;border-radius:18px;">
                                        <h4 style="text-align:center;padding: 12px 0;">Gift Certificate</h4>
                                        <h6 style="padding-left:10px;"><?= $business_information->business_name ?><br/><?= $business_information->business_phone ?></h6>

                                        <h6 class="pull-right" style="padding-right:10px;">XX.XX <br/>XXXXXXXX</h6>
                                        <div class="clearfix"></div>
                                        <img src="<?= Yii::getAlias('@web') ?>/img/giftCertificate_01.png" id="certificate_img" style="width:100%" " />
                                        <div class="col-sm-12" style="text-align:center;margin:20px 0;">
                                            <h6 style="font-size:12px;color:#888">To:  ...........................</h6>
                                            <h6 style="font-size:12px;color:#888">From:  ...........................</h6>
                                            <h6 style="font-size:12px;color:#888">Message:  ...........................</h6>

                                        </div>
                                        <div class="col-sm-12" style="text-align:center;padding:20px 0;border-top:dashed 1px #bbb">
                                            <h6 style="font-size:12px;color:#888">Use this code on <strong>Boocked.com</strong></h6>
                                            <h6 style="font-size:12px;color:#888">Lifetime validity on voucher</h6>

                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                </div>
                            </div>
                             </div>

                            <div class="row collapse in" id="my_certificate">
                                <div class="col-sm-12">
                                    <div class="panel panel-default">
                                    <div class="panel-heading full">
                                        <ul class="list-inline list-staff">
                                            <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/coupons.png"><span class="staff-head">Gift Certificates</span></h5></li>
                                        </ul>
                                    </div>
                                        <div class="col-sm-8 pull-right">
                                          <!--  <ul class="list-inline pull-right three-btns">
                                             
                                                <li class="dropdown">
                                                    <button class="btn default-btn dropdown-toggle" type="button" data-toggle="dropdown">Active / Inactive / All / Expired
                                                        <span class="caret"></span></button>
                                                    <ul class="dropdown-menu dropdown-menu-2">
                                                        <li><a href="#">All</a></li>
                                                        <li><a href="#">Active</a></li>
                                                        <li><a href="#">In active</a></li>
                                                    </ul>

                                                </li>
                                            </ul>-->
                                        </div>
                                    <?php foreach($all_gift_certificate as $gift_certificate){ ?>
                                      <div class="col-sm-6 created_gift_certificates">

                                        <div class='gift_certificates_body'>
                                            <div class="gifts_certificate_selling_buttons">

                                                <a href="#" datagc-id="<?= $gift_certificate->gc_id ?>" class="sell_certificate btn" >Sell</a>
                                                <a href="#"  datagc-id="<?= $gift_certificate->gc_id ?>" class="edit_certificate btn ">Edit</a>
                                                 <div class="certifiate_scroll_button" >
                                                    <a href="" class="pull-left">Delete</a>
                                                    <span class="pull-right"><input type="checkbox" name="my-checkbox"  datagc-id="<?= $gift_certificate->gc_id ?>" class="coupan_switch" <?php if($gift_certificate->gift_status=='1'){ ?> checked <?php } ?>></span>
                                                </div>
                                            </div>
                                            <h4 style="text-align:center;padding: 12px 0;">Gift Certificate</h4>
                                            <h6 style="padding-left:10px;"><?= $business_information->business_name ?><br/><?= $business_information->business_phone ?></h6>

                                            <h6 class="pull-right" style="padding-right:10px;"><?= $gift_certificate->gift_selling_price ?><br/>XXXXXXXX</h6>
                                            <div class="clearfix"></div>
                                            <img src="<?= $gift_certificate->gift_image?>" style="width:100%" />
                                            <div class="col-sm-12" style="text-align:center;margin:20px 0;">
                                                <h6 style="font-size:12px;color:#888">To:  ...........................</h6>
                                                <h6 style="font-size:12px;color:#888">From:  ...........................</h6>
                                                <h6 style="font-size:12px;color:#888">Message:  ...........................</h6>

                                            </div>
                                            <div class="col-sm-12" style="text-align:center;padding:20px 0;border-top:dashed 1px #bbb">
                                                <h6 style="font-size:12px;color:#888">Use this code on <strong>Boocked.com</strong></h6>
                                                <h6 style="font-size:12px;color:#888"><? if($gift_certificate->gift_validity==0){ echo 'Lifetime validity on voucher'; }
                                                    else if($gift_certificate->gift_validity==1){ echo 'Gift certificate valid  till '.date('M,d,Y', strtotime($gift_certificate->gift_validity_date));  }
                                                    else if($gift_certificate->gift_validity==2){ echo 'Gift certificate valid  for '.$gift_certificate->gift_validity_day.' Days after purchase';  }
                                                    ?>
                                                </h6>

                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                    </div>

                                    <?php } ?>
                                   <div class="clearfix"></div>
                                    </div>

                                </div>

                            </div>



                    </div>


                

        </div>
    </div>

    <div id="preloader">
        <div id="loadingstatus" style="display:block">&nbsp;</div>
    </div>
<!--<script type="text/javascript">
    var appointy = 'abdulrehman';
    var scheduleoHeight = '700';
    var scheduleoWidth = '900';
    var ShowSchedulemeImg = true;
    //if showSchedulemeImg is set to false then it will override the properties below. This can be used if you want to call overlay from your own custom link.
    var ScheduleMeBgImg = 'http://static.appointy.com/Widget/Images/scheduleme.png';
    var ScheduleMeBg = 'transparent';
    var ScheduleMeWidth = '47';
    var ScheduleMeHeight = '150';
    var ScheduleMePosition = 'right';  // right, left
    // You can also call function ShowAppointyInOverlay() onclick of any tag.
    // e.g. <a href="javascript:void(0)" onclick="ShowAppointyInOverlay();">Schedule with us</a>
</script>
<script type="text/javascript" src="http://localhost/boocked/admin/web/index.php/js/staticpopup.js"></script>-->














<?php
$script = "
    $('#new_giftcertificate_form').submit(function(){
    var errorr = 0;
        if($('#untill_radio').is(':checked') && !$('#until').val()){    
           $('#until').parents('.form_cont').addClass('has-error');
            errorr=1;   
        }
         if($('#limit_radio').is(':checked') && !$('#limit_text').val()){ 
            $('#limit_text').parents('.form_cont').addClass('has-error');
            errorr=1;
         }
          if($('#till_radio').is(':checked') && !$('#till_text').val()){ 
            $('#till_text').parents('.form_cont').addClass('has-error');
            errorr=1;
         }
          if($('#validity_radio').is(':checked') && !$('#validity_text').val()){ 
            $('#validity_radio').parents('.form_cont').addClass('has-error');
            errorr=1;
         }
         
         if(errorr > 0){
         return false;
         }
        
    });
    
  $('body').on('beforeSubmit', 'form#new_giftcertificate_form', function () {
   var errorr = 0;
        if($('#untill_radio').is(':checked') && !$('#until').val()){    
           $('#until').parents('.form_cont').addClass('has-error');
            errorr=1;   
        }
         if($('#limit_radio').is(':checked') && !$('#limit_text').val()){ 
            $('#limit_text').parents('.form_cont').addClass('has-error');
            errorr=1;
         }
          if($('#till_radio').is(':checked') && !$('#till_text').val()){ 
            $('#till_text').parents('.form_cont').addClass('has-error');
            errorr=1;
         }
          if($('#validity_radio').is(':checked') && !$('#validity_text').val()){ 
            $('#validity_radio').parents('.form_cont').addClass('has-error');
            errorr=1;
         }
         
         if(errorr > 0){
         return false;
         }else {
       //  alert('yeeees');
         }

 
    var form = $(this);

    if (form.find('.has-error').length) {
    return false;
    }
    $.ajax({
    url: form.attr('action'),
    type: 'post',
    data: form.serialize(),
    success: function (response) {
    if(response==1){
   // alert('changed');
   // $.pjax.reload({container:'#blockdatescont'});

    }else {
//    alert('not updated');

    }
    }
    });
    return false
    });
    $('.pre_design_img').click(function(){
    var img_src =  $(this).attr('src');
     $('#gift_certificate_images').modal('hide');
        $('#selected_img_val').val(img_src);
        $('#certificate_img').attr('src',img_src);
        $('#selected_img').attr('src',img_src);
    });
    
    $('.coupan_switch').on('switchChange.bootstrapSwitch', function(event, state) {
 
      var gc_id = $(this).attr('datagc-id');
      if(state==true){
        gcstate=1;
      }else {
      gcstate=0;
      }
      
     $.ajax({
    type     :'POST',
    cache    : false,
    data:{'gcstate':gcstate,'gc_id':gc_id},
    url  : '".Yii::getAlias('@web')."/index.php/marketing/changecertificate_status',
    success  : function(response) {
   //    alert(response);
    }
    });
      
      
//  console.log(this); // DOM element
//  console.log(event); // jQuery event
  state // true | false
});
    
    $('.coupan_switch').bootstrapSwitch();
    
    $('.edit_certificate').click(function(event){
        event.preventDefault();
      jQuery('#loadingstatus').show();
        jQuery('#preloader').show();
         var gc_id = $(this).attr('datagc-id');
        $('#gift_certificate_edit').find('#modalContent').load('".Yii::getAlias('@web')."/index.php/marketing/edit_certificate/?id='+gc_id,function(){
           jQuery('#loadingstatus').hide();
        jQuery('#preloader').hide();
            $('#gift_certificate_edit').modal('show');
        });
        
        
    });
     $('.sell_certificate').click(function(event){
        event.preventDefault();
         var gc_id = $(this).attr('datagc-id');
        $('#sell_certificate').find('#modalContent').load('".Yii::getAlias('@web')."/index.php/marketing/sell_certificate/?id='+gc_id,function(){
        $('#sell_certificate').modal('show');
        
        });
       
        
    });
       
       
$('body').on('beforeSubmit', 'form#sells_giftcertificate_form', function () {
    var form = $(this);

    if (form.find('.has-error').length) {
    return false;
    }
    $.ajax({
    url: form.attr('action'),
    type: 'post',
    data: form.serialize(),
    success: function (response) {
    if(response==1){
//    alert('changed');
    $('#sell_certificate').modal('hide');
   // $.pjax.reload({container:'#blockdatescont'});

    }else {
 //   alert('not updated');

    }
    }
    });
    return false

}); 
      $('.certifcate_btn').click(function(){
          var  target = $(this).attr('data-target');
          $(target).siblings('div').removeClass('in');
        });
        ";

$this->registerJs($script);
?>
<script>
    function showmodel(){
     $('#gift_certificate_images').modal('show');

    }


</script>


<?php
Modal::begin([
    'header' => '<h5 style="margin:0"><i class="fa fa-tag" ></i> Edit Certificate</h5>',
    'id' => 'gift_certificate_edit',
    'size' => 'modal-md',
//'htmlOptions' => ['style' => 'width:800px;']
]);
   echo '<div id="modalContent"></div>';

Modal::end();

?>

<?php
Modal::begin([
    'header' => '<h5 style="margin:0"><i class="fa fa-shopping-cart" aria-hidden="true"></i> Sell Certificate</h5>',
    'id' => 'sell_certificate',
    'size' => 'modal-sm',
//'htmlOptions' => ['style' => 'width:800px;']
]);
echo '<div id="modalContent"></div>';

Modal::end();

?>



<?php
Modal::begin([
    'header' => '<h5 style="margin:0">Change Image</h5>',
    'id' => 'gift_certificate_images',
    'size' => 'modal-md',
//'htmlOptions' => ['style' => 'width:800px;']
]); ?>
<div id='modalContent'>
    <div class="col-xs-2" style="padding:0">
        <label>Custom URL:</label>
    </div>
    <div class="col-xs-8">
    <input type="text" class="form-control">
    </div>
    <div class="col-xs-2">
        <button class="btn">Apply</button>
    </div>
    <div class="col-xs-12" style="padding-left:0">
        <p><small>Please enter complete URL (http://www.boocked.com/header.jpg) of your image. The size of the image should be 540 x 180 (px)</small></p>
    </div>
    <div class="col-xs-12">
        <h5>Pre-Design</h5>
    </div>
    <div class="col-sm-12">
        <div class="col-xs-4">
            <img src="<?= Yii::getAlias('@web') ?>/img/giftCertificate_01.png" class="pre_design_img" style="width:75px" />
        </div>
        <div class="col-xs-4">
            <img src="<?= Yii::getAlias('@web') ?>/img/giftCertificate_03.png" class="pre_design_img" style="width:75px" />
        </div>
        <div class="col-xs-4">
            <img src="<?= Yii::getAlias('@web') ?>/img/giftCertificate_05.png" class="pre_design_img" style="width:75px" />
        </div>
    </div>
</div>
<div class="clearfix" ></div>
<?php
Modal::end();

?>
<?php
$script2 = "  $('body').on('beforeSubmit', 'form#edit_giftcertificate_form', function () {

var errorr = 0;
if($('#untill_radio_edit').is(':checked') && !$('#until_edit').val()){
$('#until_edit').parents('.form_cont').addClass('has-error');
errorr=1;
}
if($('#limit_radio_edit').is(':checked') && !$('#limit_text_edit').val()){
$('#limit_text_edit').parents('.form_cont').addClass('has-error');
errorr=1;
}
if($('#till_radio_edit').is(':checked') && !$('#till_text_edit').val()){
$('#till_text_edit').parents('.form_cont').addClass('has-error');
errorr=1;
}
if($('#validity_radio_edit').is(':checked') && !$('#validity_text_edit').val()){
$('#validity_radio_edit').parents('.form_cont').addClass('has-error');
errorr=1;
}

if(errorr > 0){
return false;
}else {
//alert('yeeees');
}


var form = $(this);

if (form.find('.has-error').length) {
return false;
}
$.ajax({
url: form.attr('action'),
type: 'post',
data: form.serialize(),
success: function (response) {
if(response==1){
//alert('changed');
$('#gift_certificate_edit').modal('hide');
// $.pjax.reload({container:'#blockdatescont'});

}else {
//alert('not updated');

}
}
});
return false
});


$('.pre_design_img_edit').click(function(){
var img_src =  $(this).attr('src');
$('#gift_certificate__edit_images').modal('hide');
$('#selected_img_val_edit').val(img_src);
$('#selected_img_edit').attr('src',img_src);
});

";
 $this->registerJs($script2);
?>