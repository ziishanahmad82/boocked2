<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Gift Certificates';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php if($type=='edit') { ?>
    <style>
        .inlinedatepicker {
            display: inline-block;
            max-width: 100px;

        }

    </style>

    <p style="margin-top: 2%;">
        <?php
        // 'enableAjaxValidation' => true, 'validationUrl' => 'customize_emailvalidate',
        $form = ActiveForm::begin(['id' => 'edit_giftcertificate_form', 'action' => Yii::getAlias('@web') . '/index.php/marketing/update_gift_certificate?id=' . $new_gift_certificate->gc_id,
            'options' => ['class' => 'generate_email_template']]); ?>
    <p>This gift certificate can be purchased for</p>
    <div class="col-xs-6">
        <?= $form->field($new_gift_certificate, 'gift_value')->textInput(['maxlength' => true, 'class' => 'form-control'])->label('Value') ?>
    </div>
    <div class="col-xs-6">
        <?= $form->field($new_gift_certificate, 'gift_selling_price')->textInput(['maxlength' => true, 'class' => 'form-control'])->label('Selling Price') ?>
    </div>


    <div class="col-xs-12 form_cont">
        <p>Gift certificate redemption validity</p>
        <p>
            <input type="radio" name="GiftCertificates[gift_validity]"
                   value="0" <?php if ($new_gift_certificate->gift_validity == 0) {
                echo 'checked';
            } ?> /> Forever <br/>
            <input type="radio" name="GiftCertificates[gift_validity]" value="1"
                   id="untill_radio_edit" <?php if ($new_gift_certificate->gift_validity == 1) {
                echo 'checked';
            } ?> /> Until fixed date <?= DatePicker::widget([
                'id' => 'until_edit',
                'value' => $new_gift_certificate->purchase_window_date,
                'name' => 'GiftCertificates[gift_validity_date]',
                'options' => ['class' => 'form-control inlinedatepicker'],
                'dateFormat' => 'yyyy-MM-dd',
                'clientOptions' => [
                    'format' => 'L',

                ],

            ]); ?> <br/>
            <input type="radio" name="GiftCertificates[gift_validity]" id="validity_radio_edit" value="2"/><input
                type="number" name="GiftCertificates[gift_validity_day]"
                value="<?= $new_gift_certificate->gift_validity_day ?>" id="validity_text_edit" min="1"
                class="form-control inlinedatepicker"> days after purchase <br/>
        </p>
        <? //= $form->field($new_gift_certificate, 'gift_value')->textInput(['maxlength' => true,'class' => 'form-control'])->label('Template Name') ?>
    </div>
    <div class="col-sm-12 form_cont">
        <br/>
        <p>Number of gift certificates for sale</p>
        <p><input type="radio" name="GiftCertificates[sale_limit]" <?php if ($new_gift_certificate->sale_limit == 0) {
                echo 'checked';
            } ?> value="0"/> No limit <br/>
            <input type="radio" id="limit_radio_edit"
                   name="GiftCertificates[sale_limit]" <?php if ($new_gift_certificate->sale_limit == 1) {
                echo 'checked';
            } ?> value="1"/>Limit to <input type="number" name="GiftCertificates[gift_sale_limit]"
                                            value="<?= $new_gift_certificate->gift_sale_limit ?>" id="limit_text_edit"
                                            min="1" class="form-control inlinedatepicker"></p>
    </div>
    <div class="col-sm-12 form_cont">
        <br/>
        <p>Gift certificate purchase window</p>
        <p><input type="radio"
                  name="GiftCertificates[purchase_window]" <?php if ($new_gift_certificate->purchase_window == 1) {
                echo 'checked';
            } ?> value="1"/> Forever unless deactivated<br/>
            <input type="radio"
                   name="GiftCertificates[purchase_window]" <?php if ($new_gift_certificate->purchase_window == 0) {
                echo 'checked';
            } ?> id="till_radio_edit" value="0"/> Till date <?= DatePicker::widget([
                'id' => 'till_text_edit',
                'name' => 'GiftCertificates[purchase_window_date]',
                'value' => $new_gift_certificate->purchase_window_date,
                'options' => ['class' => 'form-control inlinedatepicker'],
                'dateFormat' => 'yyyy-MM-dd',
                'clientOptions' => [
                    'format' => 'L',

                ],

            ]); ?>
        </p>

    </div>
    <div class="col-sm-12">
        <h5>Gift certificate banner image</h5>
        <input type="hidden" name="GiftCertificates[gift_image]" value="<?= $new_gift_certificate->gift_image ?>"
               id="selected_img_val_edit"/>
        <img src="<?= $new_gift_certificate->gift_image ?>" id="selected_img_edit" style="width:75px"/>
        <span onclick="showmodel_edit()">Change Image</span>

    </div>
    <div class="col-xs-6">
        <? //= $form->field($new_gift_certificate, 'gift_value')->textInput(['maxlength' => true,'class' => 'form-control'])->label('Template Name') ?>
    </div>
    <?//= $form->field($new_gift_certificate, 'template_subject')->textInput(['maxlength' => true,'class' => 'form-control'])->label('Subject Line') ?>

    <br/>
    <div class="col-sm-12">
        <br/>
        <br/>
        <?= Html::submitButton($new_gift_certificate->isNewRecord ? 'Save' : 'Update', ['class' => $new_gift_certificate->isNewRecord ? 'pull-right btn btn-main' : 'pull-right btn btn-main']) ?>
    </div>
    <div class="clearfix"></div>
    <?php ActiveForm::end(); ?>
    </p>
    <div class="clearfix"></div>
    </div>
    </div>

    </div>


    <?php


$script =  "  
    $('.pre_design_img_edit').click(function(){
    var img_src =  $(this).attr('src');
     $('#gift_certificate__edit_images').modal('hide');
        $('#selected_img_val_edit').val(img_src);
        $('#selected_img_edit').attr('src',img_src);
    });  
    
    ";
    $this->registerJs($script);

    ?>
    <script>
        function showmodel_edit() {
            $('#gift_certificate__edit_images').modal('show');

        }


    </script>
    <?php
    Modal::begin([
        'header' => '<h5 style="margin:0">Change Image</h5>',
        'id' => 'gift_certificate__edit_images',
        'size' => 'modal-md',
//'htmlOptions' => ['style' => 'width:800px;']
    ]); ?>
    <div id='modalContent'>
        <div class="col-xs-2" style="padding:0">
            <label>Custom URL:</label>
        </div>
        <div class="col-xs-8">
            <input type="text" class="form-control">
        </div>
        <div class="col-xs-2">
            <button class="btn">Apply</button>
        </div>
        <div class="col-xs-12" style="padding-left:0">
            <p>
                <small>Please enter complete URL (http://www.boocked.com/header.jpg) of your image. The size of the
                    image should be 540 x 180 (px)
                </small>
            </p>
        </div>
        <div class="col-xs-12">
            <h5>Pre-Design</h5>
        </div>
        <div class="col-sm-12">
            <div class="col-xs-4">
                <img src="<?= Yii::getAlias('@web') ?>/img/giftCertificate_01.png" class="pre_design_img_edit"
                     style="width:75px"/>
            </div>
            <div class="col-xs-4">
                <img src="<?= Yii::getAlias('@web') ?>/img/giftCertificate_03.png" class="pre_design_img_edit"
                     style="width:75px"/>
            </div>
            <div class="col-xs-4">
                <img src="<?= Yii::getAlias('@web') ?>/img/giftCertificate_05.png" class="pre_design_img_edit"
                     style="width:75px"/>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <?php
    Modal::end();
}else if($type=='sell'){
    $form = ActiveForm::begin(['id' => 'sells_giftcertificate_form', 'action' => Yii::getAlias('@web') . '/index.php/marketing/create_sell_gift_certificate?id=' . $new_gift_certificate->gc_id,
    'options' => ['class' => 'generate_email_template']]); ?>
    <div class="col-sm-12">
        <?= $form->field($model, 'amount')->textInput(['maxlength' => true, 'class' => 'form-control','value'=>$new_gift_certificate->gift_selling_price])->label('Amount') ?>
    </div>
    <div class="col-sm-12">
        <label>To</label>
        <?= $form->field($model, 'to_name')->textInput(['maxlength' => true, 'class' => 'form-control','placeholder' => "Name"])->label(false) ?>
        <?= $form->field($model, 'to_email')->textInput(['maxlength' => true, 'class' => 'form-control','placeholder' => "Email"])->label(false) ?>
    </div>
    <div class="col-sm-12">

        <?= $form->field($model, 'message')->textInput(['maxlength' => true, 'class' => 'form-control'])->label('Message') ?>
    </div>
    <div class="col-sm-12">
        <label>From</label>
        <?= $form->field($model, 'from_name')->textInput(['maxlength' => true, 'class' => 'form-control','placeholder' => "Name"])->label(false) ?>
        <?= $form->field($model, 'from_email')->textInput(['maxlength' => true, 'class' => 'form-control','placeholder' => "Email"])->label(false) ?>
    </div>
<div class="col-sm-12"><?= Html::submitButton($model->isNewRecord ? 'Save' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-main' : 'btn btn-main']) ?></div>
    <div class="clearfix"></div>
<?php  ActiveForm::end(); ?>
<?php

$script = "  $('body').on('beforeSubmit', 'form#edit_giftcertificate_form', function () {

 alert('aaaaaaaaaaa');
return false;
 
    var form = $(this);

    if (form.find('.has-error').length) {
    return false;
    }
    $.ajax({
    url: form.attr('action'),
    type: 'post',
    data: form.serialize(),
    success: function (response) {
    if(response==1){
    alert('changed');
    $('#sell_certificate').modal('hide');
   // $.pjax.reload({container:'#blockdatescont'});

    }else {
    alert('not updated');

    }
    }
    });
    return false
    });  
    
    ";
//$this->registerJs($script);
    
    
    
    
}
?>
