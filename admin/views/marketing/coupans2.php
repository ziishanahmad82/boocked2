<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\jui\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Customers';
$this->params['breadcrumbs'][] = $this->title;
?>
<!--<style>
    #coupan_form_cont.hidecoupan_form{
        display: none;

    }
    #coupan_form_cont {
        margin-bottom:30px;

    }
    #coupan_form_cont.active{
        display:block;

    }
    .discount_coupan_form .col-sm-12 > .coupanformcontt.col-sm-4{
        background:#cfd6f9;

    }
    .offer_coupan_form .col-sm-12 > .coupanformcontt.col-sm-4{
        background:#fdedb4;

    }
    .discount_services_list label{
         display:block;
         width: 100%;

     }
    .discount_services_list > label {
        padding:0px 12px;
    }
    .discount_services_list > label > input {
        margin-right:8px;
    }
    .coupanview_all {
        padding-top:10px;
        padding-bottom: 10px;
        border:1px solid #fc7600;

    }
    .coupengrid_content {
        min-height:200px;
        overflow-y: scroll;

    }
    .coupengrid_content {
        padding-top:20px;

    }
    .disable_coupan_back {
        background:#e6e6e6;

    }

</style>-->

































<div class="container-fluid section-main">
    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 media-xs-full panel-market">
            <ul class="nav nav-tabs nav-main coupantabss">
                <li>
                    <a href="#" id="discount_coupan_button" >Create a Discount Coupon</a>
                </li>
                <li>
                    <a href="#second" id="offer_coupan_button" >Create An Offer</a>
                </li>

            </ul>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="first">
                    <div class="panel-collapse collapse in active">
                        <div class="panel-body">
                            <div class="row row-custom hidecoupan_form discount_coupan_form" id="coupan_form_cont">
                                <div class="col-sm-12">
                                    <div class="col-sm-4 coupanformcontt">

                                        <?php $form = ActiveForm::begin(['id'=>'create_discount','action'=>Yii::getAlias('@web').'/index.php/marketing/coupan_create']); ?>

                                        <div id="discountfields">


                                        </div>

                                        <div class="col-sm-12">
                                            <?= $form->field($coupan, 'coupan_code')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-12">
                                            <?= $form->field($coupan, 'coupan_title')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-12">
                                            <?= $form->field($coupan, 'coupan_description')->textarea() ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= $form->field($coupan, 'image_url')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= $form->field($coupan, 'work_over')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-6">
                                        <div class="form-group field-coupans-service_id ">
                                            <div class="selectBox">
                                            <label for="coupans-service_id" class="control-label">For</label>
                                            <select class="form-control" id="coupans-service_id">
                                                <option>Any Service</option>
                                            </select>
                                            <div class="overSelect"></div>
                                            </div>
                                            <div class="discount_services_list" style="background:#fff;z-index: 10001;position: absolute;display:none">
                                                <label><input type="checkbox" class="services_checkbox" name="Coupans[service_id]"  id="anyservice_check" value="all">Any Service</label>
                                            <?php
                                            foreach($services as $service){?>
                                                <label style="width: 100%;display:block"><input type="checkbox" name="Coupans[service_id][]" class="services_checkbox" value="<?= $service->service_id ?>" > <?= $service->service_name ?></label>
                                                <?php } ?>
                                            </div>
                                    </div>

                                        </div>
                                        <div class="col-sm-12">
                                            <div class="form-group field-coupans-staff_id required">

                                                <label for="coupans-staff_id" class="control-label">By</label>
                                                <div class="selectBox">
                                                <select  class="form-control" id="coupans-staff_id">
                                                </select>
                                                <div class="overSelect"></div>
                                                    </div>
                                                <div class="discount_services_list" style="background:#fff;z-index: 10001;position: absolute;display:none">
                                                    <label><input type="checkbox"  id="anystaff_check" name="Coupans[staff_id]" value="all">Any Staff</label>
                                                <?php
                                                foreach($staff as $staff){
                                                    ?>
                                                    <label style="width: 100%;display:block"><input type="checkbox" class="staff_checkbox" name="Coupans[staff_id][]" value="<?= $staff->id ?>"> <?= $staff->username ?></label>
                                                    <?php
                                                }
                                                ?>
                                                    </div>

                                            </div>
                                        </div>
                                        <div class="col-sm-6">
                                            <label>From</label>
                                            <?= DatePicker::widget([
                                                'id'=>'qweqw',
                                                'name' => 'Coupans[start_date]',
                                                'value'=>date('Y-m-d'),
                                                'options' => ['class' => 'form-control'],
                                                'dateFormat' => 'yyyy-MM-dd',
                                                'clientOptions' => [
                                                    'format' => 'L',

                                                ],

                                            ]);?>

                                        </div>
                                        <div class="col-sm-6">
                                            <label>To</label>
                                            <?= DatePicker::widget([
                                                'id'=>'qweqed',
                                                'name' => 'Coupans[end_date]',
                                                'options' => ['class' => 'form-control'],
                                                'dateFormat' => 'yyyy-MM-dd',
                                                'clientOptions' => [
                                                    'format' => 'L',

                                                ],

                                            ]);?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= $form->field($coupan, 'start_time')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= $form->field($coupan, 'end_time')->textInput(['maxlength' => true]) ?>
                                        </div>
                                        <div class="col-sm-6">

                                            <div class="form-group field-coupans-weekdays">
                                                <label for="coupans-weekdays" class="control-label">On</label>
                                                <div class="selectBox">
                                                    <select name="Coupans[weekdays]" class="form-control" >

                                                    </select>
                                                    <div class="overSelect"></div>
                                                </div>
                                                <div class="discount_services_list" style="background:#fff;z-index: 10001;position: absolute;display:none">
                                                    <label><input type="checkbox" id="anyweek_check" name="Coupans[weekdays]" value="all" checked >Any Weekday</label>
                                                    <label><input type="checkbox" class="week_checkbox" value="sunday" name="Coupans[weekdays][]" >Sunday</label>
                                                    <label><input type="checkbox" class="week_checkbox"  value="monday" name="Coupans[weekdays][]">Monday</label>
                                                    <label><input type="checkbox" class="week_checkbox"  value="tuesday" name="Coupans[weekdays][]">Tuesday</label>
                                                    <label><input type="checkbox" class="week_checkbox"  value="wednesday" name="Coupans[weekdays][]">Wednesday</label>
                                                    <label><input type="checkbox" class="week_checkbox"  value="thursday" name="Coupans[weekdays][]">Thursday</label>
                                                    <label><input type="checkbox" class="week_checkbox"  value="friday" name="Coupans[weekdays][]">Friday</label>
                                                    <label><input type="checkbox" class="week_checkbox"  value="saturday" name="Coupans[weekdays][]">Saturday</label>
                                                </div>


                                            </div>
                                        </div>


                                        <div class="col-sm-12">
                                          <input type="checkbox"name="Coupans[valid_for_new_customer]" value="1" id="first_appointment_form" >  Valid for new customers who haven't booked before.
                                        </div>
                                        <br/>
                                        <div class="col-sm-12">
                                            <input type="checkbox" name="Coupans[valid_for_one_time]" id="once_used_form" value="1" >Coupon valid for one time use only
                                        </div>
                                        <br/>
                                        <div class="col-sm-12">
                                            First  <input type="text" name="Coupans[first_booking_nums]" class="form-control" id="coupans-first_booking_nums" style="display:inline;width:200px;"> Bookings
                                        </div>

                                        <div class="clearfix"></div>
                                        <br/>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <ul class="list-inline pull-right">
                                                    <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;" onclick="hidemodel('#new_customer')">Cancel</button></li>
                                                    <li> <?= Html::submitButton($coupan->isNewRecord ? 'Save' : 'Save', ['class' => $coupan->isNewRecord ? 'btn btn-main' : 'btn btn-main']) ?></li>
                                                </ul>
                                            </div>

                                        </div>


                                        <?php ActiveForm::end(); ?>
                                    </div>
                                    <div class="col-sm-8">
                                        <div class="col-sm-12" style="background:#cfd6f9">
                                            <h5 class="coupan_title" >Coupan Title</h5>
                                            <div class="col-sm-4">
                                                <img id="coupan_image" src="http://www.celebritydogwatcher.com/coupons/images/ftd_nice_day.jpg" />
                                            </div>
                                            <div class="col-sm-4 coupan_content">
                                                <p><span id="coupan_discount_title">coupan_image</span> on  <span id="service_names">Any Service </span> with <span id="staff_id"> Any Staff  </span></p>
                                                <p>Works on <span id="coupan_weekday"> any Weekday</span> from <span id="coupan_start_date">08/31/2016 </span><span id="coupan_end_date"> to 08/31/2016</span></p>

                                            </div>



                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>
                            <div class="clearfix"></div>
                            <div class="row" id="coupan_list" >
                                <div class="col-sm-12">

                                    <?php
                                    $i=1;
                                    if(!empty($coupan_list)) {
                                        foreach ($coupan_list as $coupans) { ?>

                                            <div class="col-sm-6">

                                                <div
                                                    class="col-sm-12 <?php if ($coupans->coupan_status == '0') { ?>disable_coupan_back <?php } ?> coupanview_all"
                                                    id="coupanboxid<?= $i ?>">
                                                    <div class="col-sm-12 bg-primary">
                                                        <h5><?= $coupans->coupan_code ?></h5>
                                                    </div>
                                                    <div class="col-sm-12 coupengrid_content">
                                                        <ul>


                                                            <li><p><strong><?= $coupans->coupan_title ?></strong> on
                                                                    <strong><?php if (($coupans->service_id == 'all') || ($coupans->service_id == '')) { ?> Any Service <?php } else {
                                                                            foreach (json_decode($coupans->service_id) as $service_id) {
                                                                                echo \app\models\Services::getservice_name($service_id) . ', ';
                                                                            }
                                                                        } ?> </strong> with
                                                                    <strong><?php if (($coupans->staff_id == 'all') || ($coupans->staff_id == '')) { ?> Any Staff <?php } else {
                                                                            foreach (json_decode($coupans->staff_id) as $staff_id) {
                                                                                echo \app\models\Users::getusername($staff_id) . ', ';
                                                                            }
                                                                        } ?> </strong></p></li>
                                                            <li><p>Works on
                                                                    <strong><?php if (($coupans->weekdays == 'all') || ($coupans->weekdays == '')) { ?> Any Weekday <?php } else {
                                                                            foreach (json_decode($coupans->weekdays) as $weekdays) {
                                                                                echo $weekdays . ', ';
                                                                            }
                                                                        } ?> </strong> from
                                                                    <strong><?= date('d M Y', strtotime($coupans->start_date)) ?></strong> <?php if (!empty($coupans->end_date)) {
                                                                        echo 'to  <strong>' . date('d M Y', strtotime($coupans->end_date)) . '</strong>';
                                                                    } ?></p></li>
                                                            <?php if ($coupans->valid_for_new_customer == '1') { ?>
                                                                <li><p>Works on <strong>first</strong> appointment only.
                                                                    </p></li> <?php } ?>
                                                            <?php if ($coupans->valid_for_one_time == '1') { ?>
                                                                <li><p>Coupon valid for <strong>one</strong> time use
                                                                        only.</p></li> <?php } ?>
                                                            <?php if ($coupans->first_booking_nums == '1') { ?>
                                                                <li><p>Applicable for first
                                                                    <strong><?= $coupans->first_booking_nums ?></strong>
                                                                    bookings.</li><?php } ?>
                                                        </ul>
                                                    </div>
                                                    <a class="btn btn-primary">Share on Facebook </a>
                                                </div>
                                                <div class="col-sm-12" style="padding-top:20px">
                                                    <span class="pull-right"><input type="checkbox" name="my-checkbox"
                                                                                    data-boxid="#coupanboxid<?= $i ?>"
                                                                                    datacp_bid="<?= Yii::$app->user->identity->business_id ?>"
                                                                                    datacp-id="<?= $coupans->coupan_id ?>"
                                                                                    class="coupan_switch" <?php if ($coupans->coupan_status == '1') { ?> checked <?php } ?>></span>
                                                </div>
                                            </div>
                                            <?php $i++;
                                        }
                                    }else {
?>                                      <br/>
                                        <div class="alert alert-warning">
                                            <strong>No Coupan !</strong>You Created no coupan.Please select any tab from top to create Coupan.
                                        </div>
  <?php
                                    }
                                    ?>
                                </div>
                                <div class="clearfix"></div>

                            </div>
                            <div class="clearfix"></div>


                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                
            </div>
        </div>
    </div>
</div>

























<div class="clearfix"></div>






<?php
$script = "  

$('.coupan_switch').on('switchChange.bootstrapSwitch', function(event, state) {
    var boxid = $(this).attr('data-boxid');
    
    if(state==true){
   $(boxid).removeClass('disable_coupan_back');
    cpstate='1';
    } else{
    $(boxid).addClass('disable_coupan_back');
    cpstate='0';
    }
      var cp_id = $(this).attr('datacp-id');
      var b_id = $(this).attr('datacp_bid');
      
     $.ajax({
    type     :'POST',
    cache    : false,
    data:{'cpstate':cpstate,'cp_id':cp_id,'b_id':b_id},
    url  : '".Yii::getAlias('@web')."/index.php/marketing/changecoupan_status',
    success  : function(response) {
       alert(response);
    }
    });
      
      
//  console.log(this); // DOM element
//  console.log(event); // jQuery event
  state // true | false
});



  $('#create_discount #coupans-coupan_title').change(function(){
    var coupan_title_val = $(this).val();
    $('#coupan_discount_title').text(coupan_title_val);
  });
     var expanded = true;
         $('.selectBox').click(function() {
        var checkboxes = $(this).siblings('.discount_services_list');
        if (!expanded) {
            checkboxes.show();
            expanded = true;
        } else {
            checkboxes.hide();
            expanded = false;
        }
    })
      $('#anyservice_check').click(function(){
if($(this).is(':checked')){
        $('.services_checkbox').not(this).attr('checked', false);
        }else {
        alert('uncheck');
        }
      
      });  
       $('.week_checkbox').click(function(){
if($('.week_checkbox').is(':checked')){
        $('#anyweek_check').attr('checked', false);
        }
      
      });  
       $('#anyweek_check').click(function(){
if($('#anyweek_check').is(':checked')){
        $('.week_checkbox').attr('checked', false);
        }
      });  
        $('.staff_checkbox').click(function(){
   
        if($('.staff_checkbox').is(':checked')){
            $('#anystaff_check').attr('checked', false);
        }
      
      });  
       $('#anystaff_check').click(function(){
            if($('#anystaff_check').is(':checked')){
                $('.staff_checkbox').attr('checked', false);
                 }
      });  
      
       $('body').on('beforeSubmit', '#create_discount', function () {
            var form = $(this);
            $.ajax({
                url:form.attr('action'),
                type: 'post',
                data: form.serialize(),
                success: function (response) {
                if(response){
                $('#reports_results').html(response);
                }
                }
                }); 
 return false;
 
 });
 
 
   $('.services_checkbox').click(function(){
      var allVals = [];
 $('.services_checkbox:checked').each(function() {
       allVals.push($(this).parent('label').text());
     });
 

 $('#service_names').text(allVals);
 });
 
 $('#coupans-image_url').change(function(){
 var imgsrc = $('#coupans-image_url').val();
 if(imgsrc!=''){
    $('#coupan_image').attr('src',imgsrc);
 }else{
  $('#coupan_image').attr('src','http://www.celebritydogwatcher.com/coupons/images/ftd_nice_day.jpg');
 } 
 });  
  $('.coupan_switch').bootstrapSwitch();   
        ";
$this->registerJs($script);
$script2=" 
 $('#first_appointment_form').click(function(){
 if($(this).is(':checked')){
 $('.coupan_content').append('<p id=\"first_appointment_coupan\">Works on first appointment only.</p>');
 }else {
 $('#first_appointment_coupan').remove();
 }
 });
  $('#once_used_form').click(function(){
 if($(this).is(':checked')){
 $('.coupan_content').append('<p id=\"once_used_coupan\">Coupon can be used only once.</p>');
 }else {
 $('#once_used_coupan').remove();
 }
 });
 
 
    $('#discount_coupan_button').click(function(event){

         $('.coupantabss li').removeClass('active');
         $(this).parent('li').addClass('active');
         event.preventDefault();
         if($('#coupan_form_cont').hasClass('active') && $('#coupan_form_cont').hasClass('discount_coupan_form')){
             alert('hereee');
             $('#coupan_form_cont').removeClass('active');
         }else if($('#coupan_form_cont').hasClass('discount_coupan_form') && !$('#coupan_form_cont').hasClass('active')  ){
             $('#coupan_form_cont').addClass('active');
             alert('add content');
         }else {
             $('#coupan_form_cont').removeClass('offer_coupan_form');
             $('#coupan_form_cont').addClass('discount_coupan_form');
             $('#coupan_form_cont').addClass('active');
             $('#discountfields').html('<div class=\"col-sm-7\"><div class=\"form-group field-coupans-discount_value\"><label for=\"coupans-discount_value\" class=\"control-label\">Discount Value</label><input type=\"number\" name=\"Coupans[discount_value]\" class=\"form-control\" id=\"coupans-discount_value\"><div class=\"help-block\"></div></div></div><div class=\"col-sm-5\"> <div class=\"form-group field-coupans-discount_unit\"> <label for=\"coupans-discount_unit\" class=\"control-label\"></label> <select name=\"Coupans[discount_unit]\" class=\"form-control\" id=\"coupans-discount_unit\"> <option value=\"1\">%</option> <option value=\"0\">%</option> </select><div class=\"help-block\"></div> </div></div><div class=\"clearfix\"></div>');
         }

     });

     $('#offer_coupan_button').click(function(event){
         $('.coupantabss li').removeClass('active');
         $(this).parent('li').addClass('active');
         event.preventDefault();
         if($('#coupan_form_cont').hasClass('active') && $('#coupan_form_cont').hasClass('offer_coupan_form')){
            
             $('#coupan_form_cont').removeClass('active');
         }else if($('#coupan_form_cont').hasClass('offer_coupan_form') && !$('#coupan_form_cont').hasClass('active')  ){
             $('#coupan_form_cont').addClass('active');
          
         }else if(!$('#coupan_form_cont').hasClass('offer_coupan_form')) {
             $('#coupan_form_cont').removeClass('discount_coupan_form');
             $('#coupan_form_cont').addClass('offer_coupan_form');
             $('#coupan_form_cont').addClass('active');
             $('#discountfields').html(' ');
         }

     });
 
 
 ";
$this->registerJs($script2);
?>

<script>

     //   $("#login img").on("click", function () {
      //      FB.login(function(response) {
       //         if (response.authResponse) {
              //      _wdfb_notifyAndRedirect();
      //          }
      //      });
     //   });







</script>
<script type="text/javascript">
    var appointy = 'abdulrehman';
    var scheduleoHeight = '700';
    var scheduleoWidth = '900';
    var ShowSchedulemeImg = true;
    //if showSchedulemeImg is set to false then it will override the properties below. This can be used if you want to call overlay from your own custom link.
    var ScheduleMeBgImg = 'http://static.appointy.com/Widget/Images/scheduleme.png';
    var ScheduleMeBg = 'transparent';
    var ScheduleMeWidth = '47';
    var ScheduleMeHeight = '150';
    var ScheduleMePosition = 'right';  // right, left
    // You can also call function ShowAppointyInOverlay() onclick of any tag.
    // e.g. <a href="javascript:void(0)" onclick="ShowAppointyInOverlay();">Schedule with us</a>
</script>
<script type="text/javascript" src="http://localhost/boocked/admin/web/js/staticpopup.js"></script>
<style>
    .overSelect {
        position: absolute;
        left: 0; right: 0; top: 0; bottom: 0;
    }
    .selectBox {
        position: relative;
    }

</style>