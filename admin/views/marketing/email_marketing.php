<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Email Marketing';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="container-fluid section-main">
    <div class="row">


        <div class="col-lg-4 col-md-4 col-sm-4 div-relative pull-right div-relative-marketing" style="display: none;">
            <ul class="list-inline pull-right">
                <li><a href="#" data-toggle="tooltip" data-placement="left" data-original-title="Add Service"><i class="fa fa-plus fa-add-services"></i></a></li>
                <li><a href="#" data-toggle="tooltip" data-placement="bottom" data-original-title="Help"><i class="fa fa-question fa-question-mark"></i></a></li>
            </ul>
        </div>
       <!-- <ul class="nav nav-tabs nav-main">
            <li class="active">
                <a href="#first" data-toggle="tab">Ready-to-send Templates</a>
            </li>
            <li>
                <a href="#second" data-toggle="tab">My Templates</a>
            </li>


        </ul>-->
        <div class="col-sm-12 media-xs-full">

            <div class="row">

                    <div class="panel panel-default coupon-panel">
                        <div class="panel-heading full">
                            <ul class="list-inline list-staff">
                                <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/coupons.png"> <span class="staff-head">Create an Offer</span></h5></li>
                            </ul>
                        </div>
                        <div class="panel-body">

                            <div class="row">
                                <div class="col-lg-12">
                                   <h5><a href="#" id="create_new_button" class="btn green-btn" >Create new</a> <a href="#" id="mytemplates" class="certifcate_btn btn blue-btn" >My templates</a></h5>

                                    <div class="col-sm-8" id="create_new_form" style="display:none">
                                        <h3>Create Template</h3>
                                   <?php
                                  // 'enableAjaxValidation' => true, 'validationUrl' => 'customize_emailvalidate',
                                   $form = ActiveForm::begin(['action' => Yii::getAlias('@web').'/index.php/marketing/create_email_template',
                                           'options' => ['class' => 'generate_email_template']]);  ?>
                                        <?= $form->field($new_template, 'template_name')->textInput(['maxlength' => true,'class' => 'form-control'])->label('Template Name') ?>
                                        <?= $form->field($new_template, 'template_subject')->textInput(['maxlength' => true,'class' => 'form-control'])->label('Subject Line') ?>
                                        <select onchange="insertAtCaret('booking_confirmationqw',this)" class="form-control" >
                                            <option value="smarty">Smarty Tags</option>
                                            <option value="{{business_name}}" >Business Name</option>
                                            <option value="{{customer_name}}" >Customer Name</option>
                                        </select>
                                        <br/>
                                        <?= $form->field($new_template, 'template_content') //->textarea();
                                          ->widget(CKEditor::className(), [
                                            'options' => ['rows' => 6,'id'=>'booking_confirmationqw'],

                                            'preset' => 'standard',
                                            'clientOptions'=>[
                                                'enterMode' => 2,
                                                'forceEnterMode'=>false,
                                                'shiftEnterMode'=>1,
                                                'autoGrow_bottomSpace'=>0,
                                                'allowedContent'=>true,

                                            ]
                                        ])->label(false);
                                        ?>
                                        <?= Html::submitButton($new_template->isNewRecord ? 'Save' : 'Update', ['class' => $new_template->isNewRecord ? 'btn btn-main' : 'btn btn-main']) ?>
                                        <?php ActiveForm::end(); ?>
                                    </div>
                                    <div class="clearfix"></div>
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    
                                    <p style="margin-top: 2%;">
                                        <small class="text-muted text-size">
                                            You can add boocking.com's widget to your existing website or use our system to create a free website with our scheduling tools.
                                            You can also manage your social media channels and other features from above menu to get maximum impact from Boocking.com. Copy and paste one line code into your webpage and start accepting appointments directly on your website.
                                        </small>
                                    </p>
                                </div>
                            </div>
                            <div id="my_templates_content">
                                <div class="row">
                                <?php foreach ($all_templates as $template){ ?>
                                <div class="col-sm-12">
                                    <h4 style="margin-bottom:0px;"><?= $template->template_name ?> <small><a data-target="#editemail_template<?= $template->et_id ?>" data-toggle="collapse" style="color:#fc7600;cursor:pointer;font-size:14px;"  onclick="event.preventDefault();" >Edit</a></small></h4>
                                    <p><small><?= $template->template_subject ?></small></p>
                                    <p><small style="font-size:12px;color: #fc7600;">Created at <?= date('d,M,Y', strtotime($template->created_at)) ?></small></p>
                                    <button class="btn btn-primary"  data-target="#sendcampmgnemail<?= $template->et_id ?>" data-toggle="collapse" >Send Campaigns</button>
                                    <div class="collapse" id="sendcampmgnemail<?= $template->et_id ?>" >
                                        
                                        <br/>
                                        <label>Select Group</label>
                                        <select class="form-control" style="width:100px" onchange="selectcampaign_users(this,<?= $template->et_id ?>)">
                                            <option value="active">Active</option>
                                            <option value="inactive">InActive</option>
                                            <option value="new">New</option>
                                        </select>
                                        <br/>
                                        <p></p>

                                    </div>

                                    <div class="editemail_template collapse" id="editemail_template<?= $template->et_id ?>">
                                    <?php
                                    // 'enableAjaxValidation' => true, 'validationUrl' => 'customize_emailvalidate',
                                    $form = ActiveForm::begin(['action' => Yii::getAlias('@web').'/index.php/marketing/create_email_template',
                                        'options' => ['class' => 'generate_email_template']]);  ?>
                                    <?= $form->field($template, 'template_name')->textInput(['maxlength' => true,'class' => 'form-control'])->label('Template Name') ?>
                                    <?= $form->field($template, 'template_subject')->textInput(['maxlength' => true,'class' => 'form-control'])->label('Subject Line') ?>
                                    <select onchange="insertAtCaret('booking_confirmation<?= $template->et_id ?>',this)" class="form-control" >
                                        <option value="smarty">Smarty Tags</option>
                                        <option value="{{business_name}}" >Business Name</option>
                                        <option value="{{customer_name}}" >Customer Name</option>
                                    </select>
                                    <br/>
                                    <?= $form->field($template, 'template_content') //->textarea();
                                    ->widget(CKEditor::className(), [
                                        'options' => ['rows' => 6,'id'=>'booking_confirmation'.$template->et_id],

                                        'preset' => 'standard',
                                        'clientOptions'=>[
                                            'enterMode' => 2,
                                            'forceEnterMode'=>false,
                                            'shiftEnterMode'=>1,
                                            'autoGrow_bottomSpace'=>0,
                                            'allowedContent'=>true,

                                        ]
                                    ])->label(false);
                                    ?>
                                    <?= Html::submitButton($template->isNewRecord ? 'Save' : 'Update', ['class' => $template->isNewRecord ? 'btn btn-main' : 'btn btn-main']) ?>
                                    <?php ActiveForm::end(); ?>

                                    </div>

                                </div>
                                <?php } ?>
                            </div>
                            </div>
                        </div>
                    </div>
                </div>

                
            </div>
        </div>
    </div>
</div>

























<script>
    function selectcampaign_users(xor,temp_id){
        selectusers  = $(xor).children('option:selected').val();
        alert(selectusers);
        $.ajax({
            url: "<?= Yii::getAlias('@web')?>/index.php/customers/"+selectusers+"count",
            type: 'post',
            success: function (response) {
                $(xor).siblings('p').html('in this group '+response+' users contain' )
            }
        });


    }

    function insertAtCaret(areaId,xor) {
        //   alert(areaId);
        var optionval =  $('option:selected',xor).val();
        if(optionval!='smarty') {
            console.log(optionval);


            CKEDITOR.instances[areaId].insertText(optionval);
        }

    }
</script>







<?php
$script = "
 $('body').on('beforeSubmit', 'form.generate_email_template', function () {
    
  
    var form = $(this);

    if (form.find('.has-error').length) {
    return false;
    }
    if(!$('#booking_confirmation').val()){
    alert('Please add Email Content');
    return false;
    }
    $.ajax({
    url: form.attr('action'),
    type: 'post',
    data: form.serialize(),
    success: function (response) {
    if(response==1){
    alert('changed');
    $.pjax.reload({container:'#blockdatescont'});

    }else {
    alert('not updated');

    }
    }
    });
    return false
    });
    
    $('#create_new_button').click(function(event) {
    event.preventDefault();
    $('#my_templates_content').hide();
  $('#create_new_form').show();
  //toggle('slow', function() {
    // Animation complete.
  //});
});
 $('#mytemplates').click(function(event) {
  event.preventDefault();
    $('#create_new_form').hide();
        $('#my_templates_content').show();
        
        });
        ";

$this->registerJs($script);
?>
