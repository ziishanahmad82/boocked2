<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;
use yii\bootstrap\Modal;
use yii\widgets\Pjax;
use yii\jui\DatePicker;
use app\models\States;
use app\models\Cities;
use app\models\Countries;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CustomersSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Coupans';
$this->params['breadcrumbs'][] = $this->title;
?>
<style> /*
    #coupan_form_cont.hidecoupan_form{
        display: none;

    }
    #coupan_form_cont {
        margin-bottom:30px;

    }
    #coupan_form_cont.active{
        display:block;

    }
    .discount_coupan_form .col-sm-12 > .coupanformcontt.col-sm-4{
        background:#cfd6f9;

    }
    .offer_coupan_form .col-sm-12 > .coupanformcontt.col-sm-4{
        background:#fdedb4;

    }
    */
    .discount_services_list label{
         display:block;
         width: 100%;

     }
    .discount_services_list > label {
        padding:0px 12px;
    }
    .discount_services_list > label > input {
        margin-right:8px;
    }
    /*
    .coupanview_all {
        padding-top:10px;
        padding-bottom: 10px;
        border:1px solid #fc7600;

    }
    .coupengrid_content {
        min-height:200px;
        overflow-y: scroll;

    }
    .coupengrid_content {
        padding-top:20px;

    }
    .disable_coupan_back {
        background:#e6e6e6;

    }
*/
</style>




    <div class="row">


        <div class="col-lg-12 col-md-12 col-sm-12 media-xs-full">
            <div class="panel panel-default">
                <div class="panel-heading full">
                    <ul class="list-inline list-staff">
                        <li><h5 class="panel-title text-uppercase"><img src="<?= Yii::getAlias('@web')?>/img/coupons.png"><span class="staff-head">Coupons</span></h5></li>
                    </ul>
                </div>
                <div class="row">
                    <div class="col-sm-8 pull-right">
                        <ul class="list-inline pull-right three-btns">
                            <li><a href="<?= Yii::getAlias('@web')?>/index.php/marketing/coupans" class="btn btn-primary blue-btn">Create a Discount Coupon</a></li>
                            <li><a href="<?= Yii::getAlias('@web')?>/index.php/marketing/offers" class="btn btn-warning orange-btn">Create An Offer</a></li>
                            <li class="dropdown">
                                <button class="btn default-btn dropdown-toggle" type="button" data-toggle="dropdown">Active / Inactive / All / Expired
                                    <span class="caret"></span></button>
                                <ul class="dropdown-menu dropdown-menu-2">
                                    <li><a href="#">All</a></li>
                                    <li><a href="#">Active</a></li>
                                    <li><a href="#">In active</a></li>
                                </ul>

                            </li>
                        </ul>
                    </div>

                    <div class="col-sm-12">
                        <p class="text-box">
                            Instead of giving a discount on the price, you can offer extra services as an add-on. For example, you can offer "100% Free Delivery" or "Free Welcome Drink". It works like discount coupons and is only applicable to customers using the offer code while booking an appointment. Like "Discount Coupons", these can also be promoted on Facebook, Twitter or via Email.
                        </p>
                    </div>
                    <script>  (function(d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (d.getElementById(id)) return;
                            js = d.createElement(s); js.id = id;
                            js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7&appId=273749176329186";
                            fjs.parentNode.insertBefore(js, fjs);
                        }(document, 'script', 'facebook-jssdk'));
                    </script>
                    <div class="col-sm-12">
                        <div class="row coupon-table">
                            <?php
                            $i = 0;
                            foreach($coupan_list as $coupans){
                           if($i==0){ ?>
                            <div class="col-sm-12">
                                <div class="row">
                                    <?php } ?>
                                     <div class="col-sm-6" style="margin-bottom:30px">
                                <div class="col-sm-12 ">
                                <div class="table-outer">
                                    <table>
                                        <tbody>
                                        <tr>
                                            <th colspan="2" scope="col" class="coupan_discount_title"><?= $coupans->coupan_title ?></th>
                                        </tr>
                                        <tr>
                                            <td class="address-cell" colspan="2"><?= Cities::getCity_id($business_information->city) ?>, <?= States::getState_id($business_information->state) ?>, <?= Countries::getCountry_id($business_information->country) ?> - <?= $business_information->zip ?></td>
                                        </tr>
                                        <tr>
                                           <!-- <td width="0%" class="img-cell"><div class="gift-img"><img src="<?= $coupans->image_url ?>" id="coupan_image" style="width:100%" /></div></td>-->
                                            <td width="100%" class="info-cell"><table width="100%" border="0">
                                                    <tbody class="inner-table">
                                                    <tr>
                                                        <td class="list-cell" scope="col">
                                                            <ul class="table-list coupan_content">
                                                                <li><span class="coupan_discount_title"><?= $coupans->coupan_title ?></span> on  <span id="service_names"><?php if (($coupans->service_id == 'all') || ($coupans->service_id == '')) { ?> Any Service <?php } else {
                                                                            foreach (json_decode($coupans->service_id) as $service_id) {
                                                                                echo \app\models\Services::getservice_name($service_id) . ', ';
                                                                            }
                                                                        } ?></span> with <span id="staff_id"><?php if (($coupans->staff_id == 'all') || ($coupans->staff_id == '')) { ?> Any Staff <?php } else {
                                                                            foreach (json_decode($coupans->staff_id) as $staff_id) {
                                                                                echo \app\models\Users::getusername($staff_id) . ', ';
                                                                            }
                                                                        } ?></span> </li>
                                                                <li>Works on <span id="coupanweekdays_change"> <?php if (($coupans->weekdays == 'all') || ($coupans->weekdays == '')) { ?> Any Weekday <?php } else {
                                                                            foreach (json_decode($coupans->weekdays) as $weekdays) {
                                                                                echo $weekdays . ', ';
                                                                            }
                                                                        } ?></span> from <?= date('d M Y', strtotime($coupans->start_date)) ?> <?php if (!empty($coupans->end_date)) {
                                                                        echo 'to ' . date('d M Y', strtotime($coupans->end_date)) . '';
                                                                    } ?></li>
                                                                <?php if ($coupans->valid_for_new_customer == '1') { ?>
                                                                    <li>Works on <strong>first</strong> appointment only.
                                                                        </li> <?php } ?>
                                                                <?php if ($coupans->valid_for_one_time == '1') { ?>
                                                                    <li>Coupon valid for <strong>one</strong> time use
                                                                            only.</li> <?php } ?>
                                                                <?php if ($coupans->first_booking_nums == '1') { ?>
                                                                    <li>Applicable for first
                                                                        <strong><?= $coupans->first_booking_nums ?></strong>
                                                                        bookings.</li><?php } ?>
                                                            </ul>
                                                        </td>
                                                 <!--       <td class="icon-cell" rowspan="2" scope="col"><span class="glyphicon glyphicon-calendar
                                "></span></td>-->
                                                    </tr>
                                                    <tr style="height:25px">
                                                        <td><!--<button class="schedule-btn">Schedule Now</button>--></td>
                                                    </tr>
                                                    </tbody>
                                                </table></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                                    <div class="col-sm-12" style="padding-top:20px">

                                        <button id="fbsharebutton<?= $coupans->coupan_id ?>" class="fb-btn clearfix">

                                            <i class="fa fa-facebook" aria-hidden="true"></i>Share on Facebook</button>
                                        <script>



                                          //  FB.init({appId: "273749176329186", status: true, cookie: true,version:'v2.7'});
                                            document.getElementById('fbsharebutton<?= $coupans->coupan_id ?>').onclick = function() {
                                                FB.ui({
                                                    method: 'feed',
                                                   // href:'http://boocked.com/theapp/admin/web/index.php/marketing/coupanshare',
                                                    name:'<?= $coupans->coupan_title ?>',
                                                    picture:'http://boocked.com/theapp/admin/web/img/comun.png',
                                                    link:'http://boocked.com/theapp/admin/web/index.php/marketing/coupanshare',
                                                    caption: '<?= $coupans->coupan_title ?>',
                                                    description:'<?= $coupans->coupan_title ?> on <?php if (($coupans->service_id == 'all') || ($coupans->service_id == '')) { ?> Any Service <?php } else {foreach (json_decode($coupans->service_id) as $service_id) {
                                                            echo \app\models\Services::getservice_name($service_id) . ', ';
                                                        }
                                                    } ?>with <?php if (($coupans->staff_id == 'all') || ($coupans->staff_id == '')) { ?> Any Staff <?php } else {
                                                        foreach (json_decode($coupans->staff_id) as $staff_id) {
                                                            echo \app\models\Users::getusername($staff_id) . ', ';
                                                        }
                                                    } ?>.Works on <span id="coupanweekdays_change"> <?php if (($coupans->weekdays == 'all') || ($coupans->weekdays == '')) { ?> Any Weekday <?php } else {
                                                    foreach (json_decode($coupans->weekdays) as $weekdays) {
                                                        echo $weekdays . ', ';
                                                    }
                                                } ?> from <?= date('d M Y', strtotime($coupans->start_date)) ?> <?php if (!empty($coupans->end_date)) {
                                                    echo 'to ' . date('d M Y', strtotime($coupans->end_date)) . '';
                                                } ?><?php if ($coupans->valid_for_new_customer == '1') { ?> Works on first appointment only.<?php } ?><?php if ($coupans->valid_for_one_time == '1') { ?>Coupon valid for one time use only.<?php } ?><?php if ($coupans->first_booking_nums == '1') { ?>Applicable for first<?= $coupans->first_booking_nums ?> bookings.<?php } ?>'
                                                }, function(response){});
                                            }
                                        </script>
                                                    <span class="pull-right"><input type="checkbox" name="my-checkbox"
                                                                                    data-boxid="#coupanboxid<?= $i ?>"
                                                                                    datacp_bid="<?= Yii::$app->user->identity->business_id ?>"
                                                                                    datacp-id="<?= $coupans->coupan_id ?>"
                                                                                    class="coupan_switch" <?php if ($coupans->coupan_status == '1') { ?> checked <?php } ?>></span>
                                    </div>
                            </div>

                            </div>
                         <?php if($i==1){ ?>
                                </div>
                                </div>
                            <?  $i=-1;  } ?>
                            <?php $i++; } ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>


































                            <div class="clearfix"></div>
<!--
                            <div class="row" id="coupan_list" >
                                <div class="col-sm-12">

                                    <?php
                                    $i=1;
                                    if(!empty($coupan_list)) {
                                        foreach ($coupan_list as $coupans) { ?>

                                            <div class="col-sm-6">

                                                <div
                                                    class="col-sm-12 <?php if ($coupans->coupan_status == '0') { ?>disable_coupan_back <?php } ?> coupanview_all"
                                                    id="coupanboxid<?= $i ?>">
                                                    <div class="col-sm-12 bg-primary">
                                                        <h5><?= $coupans->coupan_code ?></h5>
                                                    </div>
                                                    <div class="col-sm-12 coupengrid_content">
                                                        <ul>


                                                            <li><p><strong><?= $coupans->coupan_title ?></strong> on
                                                                    <strong><?php if (($coupans->service_id == 'all') || ($coupans->service_id == '')) { ?> Any Service <?php } else {
                                                                            foreach (json_decode($coupans->service_id) as $service_id) {
                                                                                echo \app\models\Services::getservice_name($service_id) . ', ';
                                                                            }
                                                                        } ?> </strong> with
                                                                    <strong><?php if (($coupans->staff_id == 'all') || ($coupans->staff_id == '')) { ?> Any Staff <?php } else {
                                                                            foreach (json_decode($coupans->staff_id) as $staff_id) {
                                                                                echo \app\models\Users::getusername($staff_id) . ', ';
                                                                            }
                                                                        } ?> </strong></p></li>
                                                            <li><p>Works on
                                                                    <strong><?php if (($coupans->weekdays == 'all') || ($coupans->weekdays == '')) { ?> Any Weekday <?php } else {
                                                                            foreach (json_decode($coupans->weekdays) as $weekdays) {
                                                                                echo $weekdays . ', ';
                                                                            }
                                                                        } ?> </strong> from
                                                                    <strong><?= date('d M Y', strtotime($coupans->start_date)) ?></strong> <?php if (!empty($coupans->end_date)) {
                                                                        echo 'to  <strong>' . date('d M Y', strtotime($coupans->end_date)) . '</strong>';
                                                                    } ?></p></li>
                                                            <?php if ($coupans->valid_for_new_customer == '1') { ?>
                                                                <li><p>Works on <strong>first</strong> appointment only.
                                                                    </p></li> <?php } ?>
                                                            <?php if ($coupans->valid_for_one_time == '1') { ?>
                                                                <li><p>Coupon valid for <strong>one</strong> time use
                                                                        only.</p></li> <?php } ?>
                                                            <?php if ($coupans->first_booking_nums == '1') { ?>
                                                                <li><p>Applicable for first
                                                                    <strong><?= $coupans->first_booking_nums ?></strong>
                                                                    bookings.</li><?php } ?>
                                                        </ul>
                                                    </div>
                                                    <a class="btn btn-primary">Share on Facebook </a>
                                                </div>
                                                <div class="col-sm-12" style="padding-top:20px">
                                                    <span class="pull-right"><input type="checkbox" name="my-checkbox"
                                                                                    data-boxid="#coupanboxid<?= $i ?>"
                                                                                    datacp_bid="<?= Yii::$app->user->identity->business_id ?>"
                                                                                    datacp-id="<?= $coupans->coupan_id ?>"
                                                                                    class="coupan_switch" <?php if ($coupans->coupan_status == '1') { ?> checked <?php } ?>></span>
                                                </div>
                                            </div>
                                            <?php $i++;
                                        }
                                    }else {
?>                                      <br/>
                                        <div class="alert alert-warning">
                                            <strong>No Coupan !</strong>You Created no coupan.Please select any tab from top to create Coupan.
                                        </div>
  <?php
                                    }
                                    ?>
                                </div>
                                <div class="clearfix"></div>

                            </div>
                            <div class="clearfix"></div>


                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>

                
            </div>
        </div>
    </div>
</div>
-->
























<div class="clearfix"></div>






<?php
$script = "  

$('.coupan_switch').on('switchChange.bootstrapSwitch', function(event, state) {
    var boxid = $(this).attr('data-boxid');
    
    if(state==true){
   $(boxid).removeClass('disable_coupan_back');
    cpstate='1';
    } else{
    $(boxid).addClass('disable_coupan_back');
    cpstate='0';
    }
      var cp_id = $(this).attr('datacp-id');
      var b_id = $(this).attr('datacp_bid');
      
     $.ajax({
    type     :'POST',
    cache    : false,
    data:{'cpstate':cpstate,'cp_id':cp_id,'b_id':b_id},
    url  : '".Yii::getAlias('@web')."/index.php/marketing/changecoupan_status',
    success  : function(response) {
       alert(response);
    }
    });
      
      
//  console.log(this); // DOM element
//  console.log(event); // jQuery event
  state // true | false
});



  $('#create_discount #coupans-coupan_title').change(function(){
    var coupan_title_val = $(this).val();
    $('.coupan_discount_title').text(coupan_title_val);
  });
     var expanded = true;
         $('.selectBox').click(function() {
        var checkboxes = $(this).siblings('.discount_services_list');
        if (!expanded) {
            checkboxes.show();
            expanded = true;
        } else {
            checkboxes.hide();
            expanded = false;
        }
    })
      $('#anyservice_check').click(function(){
if($(this).is(':checked')){
        $('.services_checkbox').not(this).attr('checked', false);
        }else {
        alert('uncheck');
        }
      
      });  
       $('.week_checkbox').click(function(){
            if($('.week_checkbox').is(':checked')){
                 $('#anyweek_check').attr('checked', false);
                 var allVals = [];
                 $('.week_checkbox:checked').each(function() {
                 allVals.push($(this).parent('label').text());
     });
                $('#coupanweekdays_change').text(allVals);
        
        }
      
      });  
       $('#anyweek_check').click(function(){
        if($('#anyweek_check').is(':checked')){
             $('.week_checkbox').attr('checked', false);
             $('#coupanweekdays_change').text($(this).parent('label').text());
        }
      });  
        $('.staff_checkbox').click(function(){
   
        if($('.staff_checkbox').is(':checked')){
            $('#anystaff_check').attr('checked', false);
             var allVals = [];
            $('.staff_checkbox:checked').each(function() {
             allVals.push($(this).parent('label').text());
     });
            $('#staff_id').text(allVals);
        }
      
      });  
       $('#anystaff_check').click(function(){
            if($('#anystaff_check').is(':checked')){
                $('.staff_checkbox').attr('checked', false);
                $('#staff_id').text($(this).parent('label').text());
                
                 }
      });  
      
       $('body').on('beforeSubmit', '#create_discount', function () {
            var form = $(this);
            $.ajax({
                url:form.attr('action'),
                type: 'post',
                data: form.serialize(),
                success: function (response) {
                if(response){
                $('#reports_results').html(response);
                }
                }
                }); 
 return false;
 
 });
 
 
   $('.services_checkbox').click(function(){
      var allVals = [];
        $('.services_checkbox:checked').each(function() {
       allVals.push($(this).parent('label').text());
     });
    $('#service_names').text(allVals);
 });
 
// $('.services_checkbox').click(function(){
     
// });
 
 $('#coupans-image_url').change(function(){
 var imgsrc = $('#coupans-image_url').val();
 if(imgsrc!=''){
    $('#coupan_image').attr('src',imgsrc);
 }else{
  $('#coupan_image').attr('src','http://www.celebritydogwatcher.com/coupons/images/ftd_nice_day.jpg');
 } 
 });  
  $('.coupan_switch').bootstrapSwitch();   
    $('#workover_field').change(function(){
        if($('#workover_field').val()){
         workoverval = $('#workover_field').val();
        $('.coupan_content').append('<li id=\"workover_coupan_cont\">Reedemable over '+workoverval+'.</li>');
        }else {
         $('#workover_coupan_cont').remove();
        }
    });
    
        ";
$this->registerJs($script);
$script2=" 
 $('#first_appointment_form').click(function(){
 if($(this).is(':checked')){
 $('.coupan_content').append('<li id=\"first_appointment_coupan\">Works on first appointment only.</li>');
 }else {
 $('#first_appointment_coupan').remove();
 }
 });
  $('#once_used_form').click(function(){
 if($(this).is(':checked')){
 $('.coupan_content').append('<li id=\"once_used_coupan\">Coupon can be used only once.</li>');
 }else {
 $('#once_used_coupan').remove();
 }
 });
 
 
    $('#discount_coupan_button').click(function(event){

         $('.coupantabss li').removeClass('active');
         $(this).parent('li').addClass('active');
         event.preventDefault();
         if($('#coupan_form_cont').hasClass('active') && $('#coupan_form_cont').hasClass('discount_coupan_form')){
             alert('hereee');
             $('#coupan_form_cont').removeClass('active');
         }else if($('#coupan_form_cont').hasClass('discount_coupan_form') && !$('#coupan_form_cont').hasClass('active')  ){
             $('#coupan_form_cont').addClass('active');
             alert('add content');
         }else {
             $('#coupan_form_cont').removeClass('offer_coupan_form');
             $('#coupan_form_cont').addClass('discount_coupan_form');
             $('#coupan_form_cont').addClass('active');
             $('#discountfields').html('<div class=\"col-sm-7\"><div class=\"form-group field-coupans-discount_value\"><label for=\"coupans-discount_value\" class=\"control-label\">Discount Value</label><input type=\"number\" name=\"Coupans[discount_value]\" class=\"form-control\" id=\"coupans-discount_value\"><div class=\"help-block\"></div></div></div><div class=\"col-sm-5\"> <div class=\"form-group field-coupans-discount_unit\"> <label for=\"coupans-discount_unit\" class=\"control-label\"></label> <select name=\"Coupans[discount_unit]\" class=\"form-control\" id=\"coupans-discount_unit\"> <option value=\"1\">%</option> <option value=\"0\">%</option> </select><div class=\"help-block\"></div> </div></div><div class=\"clearfix\"></div>');
         }

     });

     $('#offer_coupan_button').click(function(event){
         $('.coupantabss li').removeClass('active');
         $(this).parent('li').addClass('active');
         event.preventDefault();
         if($('#coupan_form_cont').hasClass('active') && $('#coupan_form_cont').hasClass('offer_coupan_form')){
            
             $('#coupan_form_cont').removeClass('active');
         }else if($('#coupan_form_cont').hasClass('offer_coupan_form') && !$('#coupan_form_cont').hasClass('active')  ){
             $('#coupan_form_cont').addClass('active');
          
         }else if(!$('#coupan_form_cont').hasClass('offer_coupan_form')) {
             $('#coupan_form_cont').removeClass('discount_coupan_form');
             $('#coupan_form_cont').addClass('offer_coupan_form');
             $('#coupan_form_cont').addClass('active');
             $('#discountfields').html(' ');
         }

     });
 
 
 ";
$this->registerJs($script2);
?>

<script>

     //   $("#login img").on("click", function () {
      //      FB.login(function(response) {
       //         if (response.authResponse) {
              //      _wdfb_notifyAndRedirect();
      //          }
      //      });
     //   });







</script>
<script type="text/javascript">
    var appointy = 'abdulrehman';
    var scheduleoHeight = '700';
    var scheduleoWidth = '900';
    var ShowSchedulemeImg = true;
    //if showSchedulemeImg is set to false then it will override the properties below. This can be used if you want to call overlay from your own custom link.
    var ScheduleMeBgImg = 'http://static.appointy.com/Widget/Images/scheduleme.png';
    var ScheduleMeBg = 'transparent';
    var ScheduleMeWidth = '47';
    var ScheduleMeHeight = '150';
    var ScheduleMePosition = 'right';  // right, left
    // You can also call function ShowAppointyInOverlay() onclick of any tag.
    // e.g. <a href="javascript:void(0)" onclick="ShowAppointyInOverlay();">Schedule with us</a>
</script>
<script type="text/javascript" src="http://localhost/boocked/admin/web/js/staticpopup.js"></script>
<style>
    .overSelect {
        position: absolute;
        left: 0; right: 0; top: 0; bottom: 0;
    }
    .selectBox {
        position: relative;
    }

</style>