<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Customers */
/* @var $form yii\widgets\ActiveForm */
?>

                <div class="modal-body-inner-contents">

                    <?php $form = ActiveForm::begin(['id'=>'update_customerfrom']); ?>


                    <div class="col-sm-6">
                        <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-12">
                        <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'cell_phone')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'home_phone')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'work_phone')->textInput(['maxlength' => true]) ?>
                    </div>              <div class="col-sm-12">
                        <?= $form->field($model, 'customer_address')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'customer_region')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'customer_city')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="col-sm-6">
                        <?= $form->field($model, 'customer_zip')->textInput(['maxlength' => true]) ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="list-inline pull-right">
                                <li><button class="btn btn-default" style="font-size: 13px; color: #FC7600 !important;" onclick="hidemodel('#new_customer')">Cancel</button></li>
                                <li> <?= Html::submitButton($model->isNewRecord ? 'Save' : 'Save', ['class' => $model->isNewRecord ? 'btn btn-main' : 'btn btn-main']) ?></li>
                            </ul>
                        </div>
                    </div>


                    <?php ActiveForm::end(); ?>
                </div>

