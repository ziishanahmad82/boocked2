//var ruiParameters = [];
//ruiParameters['agency'] = 'The District of Columbia';
//ruiParameters['siteRef'] = 'http://dc.gov/'; 

//function getTokens(){
//    var tokens = [];            // new array to hold result
//    var query = location.search; // everything from the '?' onward 
//    query = query.slice(1);     // remove the first character, which will be the '?' 
//    query = query.split('&');   // split via each '&', leaving us an array of something=something strings
//    if (query != ''){
      // iterate through each something=something string
//      $.each(query, function(i,value){    
          // split the something=something string via '=', creating an array containing the token name and data
//          var token = value.split('=');   
          // assign the first array element (the token name) to the 'key' variable
//          var key = decodeURIComponent(token[0]);     
          // assign the second array element (the token data) to the 'data' variable
//          var data = decodeURIComponent(token[1]);
//          tokens[key] = data;     // add an associative key/data pair to our result array, with key names being the URI token names
//      });
//	if(tokens['agency']){
//		 ruiParameters['agency'] = tokens['agency'];
//      	}
//      	if(tokens['siteRef']){
//      		ruiParameters['siteRef'] = tokens['siteRef'];
//      	}   
//      return tokens;  // return the array

//    }
//}

//getTokens();

$(document).ready(function() {

      $('.dcFooterLabel').click(function () {
          $(this).siblings('ul').slideToggle();
          $(this).toggleClass('expanded');
          return false;
      });




function checkresponsive(){
if ($('.dcrui-main-nav').length > 0) {
if ($('#mobile-head').length < 1) {
    $(".region-branding-inner").append("<div id='mobile-head'><div id='mobile-main-menu-container'><ul id='mobile-main-menu'></ul></div>");
    $(".dcrui-main-nav ul:first-child").children().each(function() {
      $(this).clone().appendTo('#mobile-main-menu');
      
    });
    
    $('#mobile-main-menu li.expanded > a').each(function () {
      var list = $(this).siblings('ul.menu-sub');
      $(this).clone().prependTo(list).wrap('<li></li>');
    });

    $('#mobile-main-menu-container').append('<div class="rightButton"><a href="#">Menu</a></div');
    
    $('#mobile-main-menu li a').append('<span class="mobile-main-link"></span>');
    
    $('#mobile-main-menu li.expanded > a').click(function () {
          $(this).toggleClass('expand').siblings('ul').slideToggle();
          return false;
       });
    
    $('#mobile-head .rightButton').click(function() {
    $('#mobile-main-menu').toggleClass('showme');
    $('#mobile-head .rightButton').toggleClass('pressed');
    $('.pane-content.mobile-search').slideUp();
    $('#mobile-head #mobile-search-icon').removeClass('pressed');
    //jQuery('#mobile-main-menu').css("min-height",jQuery(window).height()+"px");
    });
    
    $('#mobile-head #mobile-search-icon a').click(function() {
        $('#mobile-main-menu').removeClass('showme');
      $('#mobile-head .rightButton').removeClass('pressed');
      $('#search-container').slideToggle();
      $('#mobile-head #mobile-search-icon').toggleClass('pressed');
      $(this).blur();
    });   
      }
  }
  else
  {
    $('.header-link').toggleClass('header-link-no-menu');
  }
}

checkresponsive();

//Make entire view rows clickable, usefull for mobile
 $('.header-link').click(function() {
        var href = jQuery(this).find("a").attr("href");
        if(href) {
            window.location = href;
        }
    });
}
);
