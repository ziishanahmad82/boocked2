<?php

namespace app\controllers;

use Yii;
use app\models\ThirdPartyUsers;
use app\models\ThirdPartyUsersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\CustomerReservedTimeslots;
use app\models\SiteIndexCustomerReservedTimeslotsSearch;
use app\models\SiteIndexTab2CustomerReservedTimeslotsSearch;


/**
 * ThirdPartyUsersController implements the CRUD actions for ThirdPartyUsers model.
 */
class ThirdPartyUsersController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all ThirdPartyUsers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ThirdPartyUsersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        $request = Yii::$app->request;
        $get = $request->get();
        $post = $request->post();
        $session = Yii::$app->session;

        if(@$post["user_name"])
        {
            $thirdpartylogin=ThirdPartyUsers::findOne([
                'user_name'=> $post["user_name"],
                'password' =>md5($post["password"]),
                'status' =>'active',
            ]);
            if($thirdpartylogin){
                $session["3rd_user_id"]=$thirdpartylogin->id;
                $session["3rd_user_name"]=$thirdpartylogin->user_name;
                Yii::$app->getSession()->setFlash('success', 'You are logged in as Third Party User.');
                return $this->redirect(['third-party-users/thirdpartydashboard']);


            } else {
                Yii::$app->getSession()->setFlash('warning', 'Invalid user name or password.');

            }

        }

        if(@$get["logout"]=='yes'){
            unset($session["3rd_user_id"]);
            unset($session["3rd_user_name"]);
            Yii::$app->getSession()->setFlash('success', 'You are logged out successfully.');
            return $this->redirect(['site/index']);

        }



        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ThirdPartyUsers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ThirdPartyUsers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ThirdPartyUsers();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing ThirdPartyUsers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing ThirdPartyUsers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the ThirdPartyUsers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ThirdPartyUsers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ThirdPartyUsers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    public function actionThirdpartydashboard(){
        $request = Yii::$app->request;
        $get = $request->get();
        $session = Yii::$app->session;
        if(@$session["3rd_user_id"]) {
            $searchModel = new SiteIndexCustomerReservedTimeslotsSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
            $count1=$dataProvider->getTotalCount();

            $searchModel2 = new SiteIndexTab2CustomerReservedTimeslotsSearch();
            $dataProvider2 = $searchModel2->search(Yii::$app->request->queryParams);
            $count2=$dataProvider2->getTotalCount();



            return $this->render('thirdpartydashboard', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'count1' => $count1,
                'searchModel2' => $searchModel2,
                'dataProvider2' => $dataProvider2,
                'count2' => $count2,
            ]);

        } else {
            return false;
        }
    }
}
