<?php

namespace app\controllers;

use app\models\Customers;
use Yii;
use app\models\CustomerReservedTimeslots;
use app\models\CustomerReservedTimeslotsSearch;
use app\models\Holidays;
use app\models\Examiners;
use app\models\TimeSlotsConfiguration;
use app\models\Slots;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\Json;

use \DateTime;

/**
 * CustomerReservedTimeslotsController implements the CRUD actions for CustomerReservedTimeslots model.
 */
class CustomerReservedTimeslotsController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all CustomerReservedTimeslots models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomerReservedTimeslotsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }



    public function actionIndexedit()
    {
        $request = Yii::$app->request;
        $get = $request->get();
        if(@$get["delete"]=='yes') {

            $session = Yii::$app->session;
            $customer_id = $session["customer_id"];
            $get["customer_id"] = $session["customer_id"];
            $slot_id = $session["slot_id"];
            $get["id"] = $session["slot_id"];

            $customerReservedTimeslotsModel = CustomerReservedTimeslots::findOne($slot_id);
            $customerReservedTimeslotsModel->confirmed=0;
            $customerReservedTimeslotsModel->confirmation_number='C'.($customerReservedTimeslotsModel->timeslot_id+10000);
            $customerReservedTimeslotsModel->save();

            $customersModel = Customers::findOne($customer_id);
            $customersModel->confirmed=0;
            $customersModel->email_repeat=$customersModel->email;
            $customersModel->save();


            Yii::$app->getSession()->setFlash('success', 'Your appointment has been cancelled successfully.');
            return $this->redirect(['site/index']);


        }


        $searchModel = new CustomerReservedTimeslotsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('indexedit', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }



    public function actionSavestatus(){
        $request = Yii::$app->request;
        $get = $request->get();
        $post = $request->post();
        $session = Yii::$app->session;
        $model = $this->findModel($post['timeslot_id']);
        if($post['assign_toughbook']=='yes'){


            $model->tough_book_id=$post['toughbook-select1'];
            $model->status='Assigned to toughbook';
            if($model->save()){
                $session->setFlash('success', 'Changes Saved.');
                $this->redirect(Yii::getAlias("@web").'/index.php/third-party-users/thirdpartydashboard');
            }

        } elseif($post['assign_toughbook']=='no'){
            $model->status=$post['status'];
            if($model->save()){
                $session->setFlash('success', 'Changes Saved.');
                $this->redirect(Yii::getAlias("@web").'/index.php/third-party-users/thirdpartydashboard');
            }
        }



    }

    /**
     * Displays a single CustomerReservedTimeslots model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CustomerReservedTimeslots model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CustomerReservedTimeslots();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->timeslot_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }


    public function actionForm2()
    {
        $model = new CustomerReservedTimeslots();




        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            $post=Yii::$app->request->post();


            $session = Yii::$app->session;
            $session["slot_id"] = $model->timeslot_id;
            $session["new_slot_id"] = $post["timeslot_id"];
            return $this->redirect(['index', 'id' => $model->timeslot_id, 'customer_id'=>$session["customer_id"], 'new_slot_id'=>$session["new_slot_id"]]);
        } else {
            $this->layout = 'noheader';
            return $this->render('_form2', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing CustomerReservedTimeslots model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->timeslot_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    public function actionCellcolor(){


        $session = Yii::$app->session;
        if(@$session["3rd_user_id"]){
            $res_slots_count= Slots::find()
                ->select(['COUNT(id) AS cnt'])
                ->where("
                         slot_date = '{$_REQUEST['date']}'
                         and status = 'Active'
                         and third_party_user_id = {$session['3rd_user_id']}
                         ")
                ->groupBy('start_time')
                ->one();
        } else {
            $res_slots_count= Slots::find()
                ->select(['COUNT(id) AS cnt'])
                ->where("
                         calendar_type = '{$_REQUEST['test_type']}'
                         and slot_date = '{$_REQUEST['date']}'
                         and status = 'Active'

                         ")
                ->groupBy('start_time')
                ->one();
        }




        $total_slots_per_day=$_REQUEST["total_slots_per_day"];
        if(!$res_slots_count)
            $returnthis=['bgcolor'=>'#cc0000', 'date'=>$_REQUEST["date"], 'total_slots_per_day'=>$_REQUEST["total_slots_per_day"], 'day_status'=>'Unavailable<span style=\'display:none;\'>holiday</span>'];
        else
            $returnthis=['bgcolor'=>'#228B22', 'date'=>$_REQUEST["date"], 'total_slots_per_day'=>$_REQUEST["total_slots_per_day"], 'day_status'=>'Available'];

        $holidays=Holidays::find()
            ->where('holiday_date>=CURDATE()')
            ->all();

        //var_dump($holidays);



        foreach($holidays as $holiday){
            $holiday->holiday_name;
            $holiday->holiday_span;
            $holiday->holiday_date;
            $i=0;
            while($i < $holiday->holiday_span){


                $holiday_date = new DateTime($holiday->holiday_date);

                $holiday_date->modify("+{$i} day");
                $holiday_date = $holiday_date->format('Y-m-d') . "\n";

                $all_holidays[]=['holiday_name'=>$holiday->holiday_name, 'holiday_date'=>$holiday_date];
                $i++;
            }

        }

        $returnthis['all_holidays']=$all_holidays;
//        echo "<pre>";
//        var_dump($returnthis);
//        echo "</pre>";

        $returnthis=json_encode($returnthis);
        return $returnthis;
    }


    public function actionLocationtimeslots(){




        $selected_date=$_REQUEST["selected_date"];
        $location_id=$_REQUEST["location_id"];
        $test_type=$_REQUEST["test_type"];




        $strReturn="";


        $i = 1;
        $shown_slots_counter=0;

        $session = Yii::$app->session;
        if(@$session["3rd_user_id"]){
            $slots = Slots::find()->
            where("
                slot_date = '{$selected_date}'
                and status = 'Active'
                and third_party_user_id = {$session['3rd_user_id']}
                "
                )
                ->groupBy('start_time')
                ->all();
        } else {
            $slots = Slots::find()->
            where([
                'calendar_type' => $test_type,
                'slot_date'=>$selected_date,
                'status'=>'Active',
                'is_reserved'=>'no',
            ])
                ->groupBy('start_time')
                ->all();
        }

        foreach($slots as $slot) {
            $startTime = $slot->start_time;
            $startTimeDisplay=date("h:i A", strtotime($startTime));
            $endTimeDisplay = date("h:i A",strtotime('+'.$slot->duration.' minutes',strtotime($slot->start_time)));


            $slotsCount = Slots::find()
                ->select(['COUNT(id) AS cnt'])
                ->where("
                         calendar_type = '{$test_type}'
                         and slot_date = '$selected_date'
                         and status = 'Active'
                         and is_reserved = 'no'
                         ")
                ->groupBy('start_time')
                ->one();
             $slots_count = $slotsCount->cnt;



            if($slots_count > 0) {



                    $strReturn .=
                        "<div class='sitem11'><input type=\"radio\" name=\"timeslot_id\" value='{$slot->id}'
                       onclick=\"$('#customerreservedtimeslots-test_start_time').val('" . $startTimeDisplay . "');
                           $('#customerreservedtimeslots-test_end_time').val('" . $endTimeDisplay . "');\"
                    > " . date("g:i A", strtotime(substr($startTime, 0, -3))) . " to " . date("g:i A", strtotime($endTimeDisplay)) . "</div>
                    ";


                $shown_slots_counter++;

            }
            $i++;
        }

        if(!$shown_slots_counter){

            $strReturn="<div class='alert alert-warning'>Sorry! No slots available for selected location. Please try other locations.</div>";
        }




        return $strReturn;
    }

    /**
     * Deletes an existing CustomerReservedTimeslots model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the CustomerReservedTimeslots model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CustomerReservedTimeslots the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CustomerReservedTimeslots::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }


    public function actionJsoncalendar($start=NULL,$end=NULL,$_=NULL){
        $events = array();

        //Demo
        $Event = new \yii2fullcalendar\models\Event();
        $Event->id = 1;
        $Event->title = 'Testing';
        $Event->start = date('Y-m-d\TH:m:s\Z');
        $events[] = $Event;

        $Event = new \yii2fullcalendar\models\Event();
        $Event->id = 2;
        $Event->title = 'Testing';
        $Event->start = date('Y-m-d\TH:m:s\Z',strtotime('tomorrow 8am'));
        $events[] = $Event;


        $event3 = new DateTime('+2days 10am');
        $Event = new \yii2fullcalendar\models\Event();
        $Event->id = 2;
        $Event->title = 'Testing111';
        $Event->start = $event3->format('Y-m-d\Th:m:s\Z');
        $Event->end = $event3->modify('+3 hours')->format('Y-m-d\TH:m:s\Z');
        $events[] = $Event;


        header('Content-type: application/json');
        echo Json::encode($events);

        Yii::$app->end();
    }
}
