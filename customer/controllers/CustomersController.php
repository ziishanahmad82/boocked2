<?php

namespace app\controllers;

use app\models\CustomerReservedTimeslots;
use Yii;
use app\models\Customers;
use app\models\CustomersSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

use yii\rest\ActiveController;

/**
 * CustomersController implements the CRUD actions for Customers model.
 */
class CustomersController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Customers models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new CustomersSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionEditcustomer(){
        $request = Yii::$app->request;
        $get = $request->get();
        $post = $request->post();
        $session = Yii::$app->session;
        $dob=date('Y-m-d', strtotime(str_ireplace('-', '/', $post["dob"])));

        $customers=Customers::findOne([
            "learners_permit_number"=>"{$post["learners_permit_number"]}",
        "last_name"=>"{$post["last_name"]}",
        "date_of_birth"=>"{$dob}",
            "confirmed"=>'1'

        ]);


        if($customers){
            $session = Yii::$app->session;
            $session["customer_id"]=$customers->customer_id;
            return $this->redirect(['customer-reserved-timeslots/indexedit', 'id' => $customers->customer_id]);

        } else {
            Yii::$app->getSession()->setFlash('warning', 'The provided information doesnt match our records.');
            return $this->redirect(['/', 'customer_action' => 'edit']);

        }



        exit();
    }



    public function actionDeletecustomer(){
        $request = Yii::$app->request;
        $get = $request->get();
        $post = $request->post();
        $session = Yii::$app->session;
        $customers=Customers::findOne([
            "learners_permit_number"=>"{$post["learners_permit_number"]}",
            "last_name"=>"{$post["last_name"]}",
            "email"=>"{$post["email"]}",
            "confirmed"=>'1'

        ]);

        $reserved_time_slot=CustomerReservedTimeslots::findOne([
            'customer_id'=>$customers->customer_id,
            'confirmed'=>1
        ]);
        $slot_id=$reserved_time_slot->timeslot_id;


        if($customers){
            return $this->redirect(['customer-reserved-timeslots/indexedit', 'customer_id' => $customers->customer_id, 'id' => $slot_id]);

        } else {
            Yii::$app->getSession()->setFlash('warning', 'The provided information doesnt match our records.');
            return $this->redirect(['/', 'customer_action' => 'delete']);

        }



        exit();
    }




    /**
     * Displays a single Customers model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Customers model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Customers();
        $session = Yii::$app->session;

        if ($model->load(Yii::$app->request->post())) {



            $url = "http://deistest.in.dc.gov:8081/services/sts/dln/DC890011/verify";

            $xmlRequest="

<EligibilityVerificationRequest xmlns:ns0=\"http://dmv.dc.gov/destiny/eis/services/sts/schema\" xmlns:xs=\"http://www.w3.org/2001/XMLSchema\">
    <EligibilityVerificationData>
        <firstName>Joe</firstName>
        <lastName>Public</lastName>
        <dob>1994-02-14</dob>
        <licenseType>CDL</licenseType>
        <licenseClass>A</licenseClass>
                   <testType>General</testType>
    </EligibilityVerificationData>
</EligibilityVerificationRequest>
            ";


            //setting the curl parameters.
            $headers = array(
                "Content-type: text/xml;charset=\"utf-8\"",
                "Accept: text/xml",
                "Cache-Control: no-cache",
                "Pragma: no-cache",
                "SOAPAction: \"run\""
            );

//            try{
//                $ch = curl_init();
//                curl_setopt($ch, CURLOPT_URL, $url);
//                curl_setopt($ch, CURLOPT_POST, 1);
//
//                // send xml request to a server
//
//                curl_setopt ($ch, CURLOPT_SSL_VERIFYHOST, 0);
//                curl_setopt ($ch, CURLOPT_SSL_VERIFYPEER, 0);
//
//                curl_setopt($ch, CURLOPT_POSTFIELDS,  $xmlRequest);
//                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
//
//                curl_setopt($ch, CURLOPT_VERBOSE, 0);
//                curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
//                $data = curl_exec($ch);
//
//                //convert the XML result into array
//                if($data === false){
//                    $error = curl_error($ch);
//                    echo $error;
//                    die('error occured');
//                }else{
//
//                    $data = json_decode(json_encode(simplexml_load_string($data)), true);
//                }
//                curl_close($ch);
//
//            }catch(Exception  $e){
//                echo 'Message: ' .$e->getMessage();die("Error");
//            }
//
//        exit();





            $date = date_create_from_format('m-d-Y', $model->date_of_birth);
            $model->date_of_birth = date_format($date, 'Y-m-d');
            if(@$session['3rd_user_id']){
                $model->third_user_id=$session['3rd_user_id'];
            }
            if($model->test_type=='CDL') {
                $model->sub_test_type = implode($model->sub_test_type, ',');
            }
            //echo $model->sub_test_type;
            if(!$model->save()){
                return $this->render('create', [
                    'model' => $model,
                ]);
                print_r($model->getErrors());
                exit();
            }
            $session = Yii::$app->session;
            $session["customer_id"]=$model->customer_id;




            return $this->redirect(['customer-reserved-timeslots/create', 'id' => $model->customer_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Customers model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        $session = Yii::$app->session;
        $id=$session["customer_id"];
        $model = $this->findModel($id);
        $request = Yii::$app->request;
        $get = $request->get();


        if ($model->load(Yii::$app->request->post())) {

            $date = date_create_from_format('m-d-Y', $model->date_of_birth);
            $model->date_of_birth = date_format($date, 'Y-m-d');
            if(@$session['3rd_user_id']){
                $model->third_user_id=$session['3rd_user_id'];
            }
            $model->save();

            return $this->redirect(['customer-reserved-timeslots/index', 'customer_id' => $model->customer_id, 'id'=>$get["slot_id"]]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }





    public function actionUpdate2()
    {
        $session = Yii::$app->session;
        $id=$session["customer_id"];
        $model = $this->findModel($id);
        $request = Yii::$app->request;
        $get = $request->get();

        if ($model->load(Yii::$app->request->post())) {



            $reserved_time_slot=CustomerReservedTimeslots::findOne([
               'customer_id'=>$id,
                'confirmed'=>1
            ]);
            $slot_id=$reserved_time_slot->timeslot_id;

            $date = date_create_from_format('m-d-Y', $model->date_of_birth);
            $model->date_of_birth = date_format($date, 'Y-m-d');
            if(@$session['3rd_user_id']){
                $model->third_user_id=$session['3rd_user_id'];
            }
            $model->save();

            return $this->redirect(['customer-reserved-timeslots/indexedit', 'customer_id' => $model->customer_id, 'id'=>$slot_id]);
        } else {
            return $this->render('update2', [
                'model' => $model,
            ]);
        }
    }





    /**
     * Deletes an existing Customers model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Customers model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Customers the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Customers::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    protected function afterFind ()
    {
        // convert to display format
        $this->date_of_birth = strtotime ($this->date_of_birth);
        $this->date_of_birth = date ('mm-dd-Y', $this->date_of_birth);

        parent::afterFind ();
    }

    protected function beforeValidate ()
    {
        // convert to storage format
        $this->date_of_birth = strtotime ($this->date_of_birth);
        $this->date_of_birth = date ('Y-m-d', $this->date_of_birth);

        return parent::beforeValidate ();
    }
}
