<?php

namespace app\controllers;

use Yii;
use app\models\UserGroups;
use app\models\UserGroupsSearch;
use yii\rbac\DbManager;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\base\Event;
//use app\controllers\CampaignController;

/**
 * UserGroupsController implements the CRUD actions for UserGroups model.
 */
class UserGroupsController extends Controller
{
    const EVENT_GET_AUTH_CONTROLLERS = 'getAuthControllers';
    private $controllers;
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all UserGroups models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserGroupsSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single UserGroups model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new UserGroups model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new UserGroups();
        $this->getAllControllers();
        //echo '<pre>';
        //var_dump($this->controllers);
        //echo '</pre>';
        //die();

        if ($model->load(Yii::$app->request->post())) {
            $model->group_rights=json_encode(Yii::$app->request->post());


//            $auth=new DbManager;
//            $auth->init();
//            $admin = $auth->createRole('admin');
//            $auth->add($admin);
//            $auth->assign('admin', 14);



//            $rbac = \Yii::$app->authManager;
//            $rbac->removeAll();
//            $manager = $rbac->createRole('manager');
//            $manager->description = 'Can manage entities in database, but not users';
//            $rbac->add($manager);
//            $rbac->assign(
//                $manager,
//                14
//            );
//
//            exit();
            $model->save();
            return $this->redirect(['view', 'id' => $model->group_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'controllers' => $this->controllers,
            ]);
        }
    }

    /**
     * Updates an existing UserGroups model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $this->getAllControllers();
//        echo '<pre>';
//        var_dump($this->controllers);
//        echo '</pre>';
        //die();

        if ($model->load(Yii::$app->request->post())) {
            $model->group_rights=json_encode(Yii::$app->request->post());
            $model->save();
            return $this->redirect(['view', 'id' => $model->group_id]);
        } else {
            $group_rights=$model->group_rights;
            return $this->render('update', [
                'model' => $model,
                'group_rights'=> $group_rights,
                'controllers' => $this->controllers,
            ]);
        }
    }

    /**
     * Deletes an existing UserGroups model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }


    public function actionCreateGroup()
    {

        return $this->redirect(['index']);
    }

    /**
     * Finds the UserGroups model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return UserGroups the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = UserGroups::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }



    public function getAllControllers(){
        $this->controllers = false;
        $d = dir(Yii::$app->controllerPath);
        while (false !== ($entry = $d->read())) {
            if($entry !== '.' && $entry !== '..' && $entry !== 'UserGroupsController.php'){
                $this->controllers[] = str_replace('.php','',$entry);
            }
        }
        $d->close();
    }

    public function getGroupRights()
    {
        echo "hello world! here i am";
        exit();
    }
}

