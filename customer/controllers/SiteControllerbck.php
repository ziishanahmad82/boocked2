<?php

namespace app\controllers;

use app\models\Notification;
use app\models\Services;
use app\models\User;
use app\models\Users;
use app\models\WeeklyRecurringTime;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SignupForm;
use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;
use app\models\Customers;
use app\models\BusinessInformation;
use app\models\Staff;
use app\models\Appointments;
use app\models\Countries;
use app\models\States;
use app\models\Cities;
use yii\web\Session;
use app\models\SettingBookingRules;
use app\models\Timezoneandcurrency;
use app\models\CurrencyList;
use app\models\Reviews;
use yii\widgets\ActiveForm;
use yii\web\UploadedFile;



class SiteController extends Controller
{
    public $successUrl;
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    // [
                    //     'actions' => ['signup'],
                    //     'allow' => true,
                    //     'roles' => ['@'],
                    // ],
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                    'language' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
            'auth' => [
                'class' => 'yii\authclient\AuthAction',
                'successCallback' => [$this, 'oAuthSuccess'],
            ],

        ];
    }

    public function actionIndex()
    {
        // $subdomain = str_replace('.boocked.com','',$_SERVER['HTTP_HOST']);
        //  $business_information  = BusinessInformation::find()->where(['business_subdomain'=>$subdomain])->one();
        //   if(empty($business_information)){
        //       return 'Business not available';
        //    }
        //    $business_id = $business_information->business_id;
        $business_id=1;

        $request = Yii::$app->request;


        /* $timezone_currency  =  Timezoneandcurrency::findOne(['business_id'=>$business_id]);
         $settingrules =   SettingBookingRules::find()->where(['business_id'=>$business_id])->all();
         Yii::$app->session->set('bk_currency', CurrencyList::showcurrency_name($timezone_currency->currency));
         foreach($settingrules as $setting_rule){
             Yii::$app->session->set('bk_'.$setting_rule->bkrules_name, $setting_rule->bkrules_value);


         } */


        //  service will not show untill its not assigned to staff
        $services = Services::find()->where(['business_id'=>$business_id,'service_status'=>'active'])->all();
        //   getWeeklyrecurringtime
        $query = Services::find()->joinWith(['weeklyrecurringtime']);
        $services = $query->select(['*'])->where(['services.business_id'=>$business_id,'services.service_status'=>'active'])
            ->andWhere(['not', ['wrt_id' => null]])->all();
        /*   foreach($services as $servic){
               echo '<pre>';
               print_r($servic->weeklyrecurringtime);
               echo '</pre>';
           } */


        $staffs = Staff::find()->where(['business_id'=>$business_id])->all();
        $get = $request->get();
        /* if(@$get['agreed']=='no') {

             $session = Yii::$app->session;
             $customer_id = $session["customer_id"];
             $customermodel = Customers::findOne($customer_id);
             $customermodel->agreement_status = 'Disagreed';
             $customermodel->email = $customermodel->email;
             $customermodel->email_repeat = $customermodel->email;
             $customermodel->save();

         }*/

        return $this->render('index',[
            'services' => $services,
            'staffs'=>$staffs,

        ]);
    }
    public function actionDashboard()
    {
        $subdomain = str_replace('.boocked.com','',$_SERVER['HTTP_HOST']);
        $business_information  = BusinessInformation::find()->where(['business_subdomain'=>$subdomain])->one();
        if(empty($business_information)){
            return 'Business not available';
        }
        $business_id = $business_information->business_id;

        $request = Yii::$app->request;

        //   ->andFilterWhere(['and', ['=', 'appointments.user_id', $staff_type]])
        //    ->andFilterWhere(['and', ['=', 'appointments.service_id', $service_type]])
        //    ->andFilterWhere(['or', ['=', $list_type, $start_date], ['>', $list_type, $start_date]])
        $current_date  = date('Y-m-d');
        $customer_id  = Yii::$app->user->identity->customer_id;
        // ,'customer_id'=>$customer_id
        $upcoming_appointments  = Appointments::find()->where(['business_id'=>1])
            //  ->andFilterWhere(['and', ['=', 'appointments.user_id', $staff_type]])
            ->andFilterWhere(['or', ['=', 'appointment_date', $current_date], ['>','appointment_date', $current_date]])->all();
        //   echo '<pre>';
        //   print_r($upcoming_appointments);
        //   echo '</pre>';
        $past_appointments  = Appointments::find()->where(['business_id'=>1])
            //  ->andFilterWhere(['and', ['=', 'appointments.user_id', $staff_type]])
            ->andFilterWhere(['or', ['=', 'appointment_date', $current_date], ['<','appointment_date', $current_date]])->all();
        $services = Services::find()->where(['business_id'=>$business_id,'service_status'=>'active'])->all();
        //   getWeeklyrecurringtime
        $query = Services::find()->joinWith(['weeklyrecurringtime']);

        $services = $query->select(['*'])->where(['services.business_id'=>$business_id,'services.service_status'=>'active'])
            ->andWhere(['not', ['wrt_id' => null]])->all();
        /*   foreach($services as $servic){
               echo '<pre>';
               print_r($servic->weeklyrecurringtime);
               echo '</pre>';
           } */

        $model = User::findOne($customer_id);

        $staffs = Staff::find()->where(['business_id'=>$business_id])->all();
        $get = $request->get();


        return $this->render('dashboard',[
            'services' => $services,
            'staffs'=>$staffs,
            'upcoming_appointments'=>$upcoming_appointments,
            'past_appointments'=>$past_appointments,
            'model'=>$model

        ]);
    }
    public function actionLoadcalendar()
    {
        $service = $_POST['service'];
        if (isset($_POST['staff'])) {

            return $this->renderAjax('calendar', [
                'service' => $service,
                'staff' => $_POST['staff'],
            ]);

        } else {
            return $this->renderAjax('calendar', [
                'service' => $service,
            ]);

        }
    }
    public function actionBooking()
    {
        $subdomain = str_replace('.boocked.com','',$_SERVER['HTTP_HOST']);
        $business_information  = BusinessInformation::find()->where(['business_subdomain'=>$subdomain])->one();
        if(empty($business_information)){
            return 'Business not available';
        }
        $business_id = $business_information->business_id;
        if(isset($_POST['service'])) {
            $staff_info = Staff::find()->where(['business_id' => $business_id, 'staff_status' => 1])->all();
            //   $allstaffs = Staff::find()->where(['business_id'=>$business_id])->all();
            $service = $_POST['service'];
            $query = Staff::find()->joinWith(['weeklyrecurringtime']);
            $allstaffs= $query->select(['*'])->where(['user.business_id'=>$business_id,'weekly_recurring_time.service_id'=>$service])
                ->andWhere(['not', ['wrt_id' => null]])->all();
            //  $service = 1;
            $service_info = Services::findOne($service);
            $model = new SignupForm();
            //  print_r($service_info);
            if (isset($_POST['staff'])) {

                return $this->render('booking', [
                    'service' => $service,
                    'staff' => $_POST['staff'],
                    'service_info' => $service_info,
                    'model'=>$model,
                    'business_information'=>$business_information,
                    'allstaffs'=>$allstaffs,
                ]);

            } else {
                return $this->render('calendar', [
                    'service' => $service,
                    'service_info' => $service_info,
                    'model'=>$model,
                    'business_information'=>$business_information,
                    'allstaffs'=>$allstaffs,
                ]);

            }
        }else {
            return $this->redirect(Yii::getAlias('@web') . '/index.php/');

        }
    }

    // $services = Services::findOne(['business_id'=>1,'service_status'=>'active','service_id'=>$service]);
    //   print_r($services->service_name);
    //   exit();





    public function actionCustomerdata(){
        return $this->render('customerdata');
    }

    public function actionRoutes()
    {
        return $this->render('routes');
    }

    /**
     * Ajax handler for language change dropdown list. Sets cookie ready for next request
     */
    public function actionLanguage()
    {
        if ( Yii::$app->request->post('_lang') !== NULL && array_key_exists(Yii::$app->request->post('_lang'), Yii::$app->params['languages']))
        {
            Yii::$app->language = Yii::$app->request->post('_lang');
            $cookie = new yii\web\Cookie([
                'name' => '_lang',
                'value' => Yii::$app->request->post('_lang'),
            ]);
            Yii::$app->getResponse()->getCookies()->add($cookie);
        }
        Yii::$app->end();
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }
    public function actionRelatedstaff(){

        $service_id  = $_POST['serviceval'];
        $subdomain = str_replace('.boocked.com','',$_SERVER['HTTP_HOST']);
        $business_information  = BusinessInformation::find()->where(['business_subdomain'=>$subdomain])->one();
        if(empty($business_information)){
            return 'Business not available';
        }
        $business_id = $business_information->business_id;
        $query = Staff::find()->joinWith(['weeklyrecurringtime']);
        $staffs= $query->select(['*'])->where(['user.business_id'=>$business_id,'weekly_recurring_time.service_id'=>$service_id])
            ->andWhere(['not', ['wrt_id' => null]])->all();
        /*   foreach($services as $servic){
               echo '<pre>';
               print_r($servic->weeklyrecurringtime);
               echo '</pre>';
           } */

        //   foreach($staffs as $staafid){
        //        echo $staafid->username;

        //    }
        $path   = Yii::getAlias('@web');
        $admin_sidepath = substr($path, 0, strrpos($path, "customer"));


        foreach ($staffs as $staff){  ?>
            <div class="search_staff_box ">
                <input type="checkbox" value="<?=  $staff->id ?>" name="staff_name" class="staff_checkbox" id="staff_checkbox<?=  $staff->id ?>" style="display:none" >
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-5 col-xs-3 container-staff-img">
                        <?php
                        if($staff->user_profile_image!=""){?>
                            <img src="<?= $admin_sidepath . 'admin/web/' . $staff->user_profile_image ?>" class="<?= $staff->username ?>" style="width:98px;">
                        <?php }else{ ?>
                            <img src="<?= Yii::getAlias('@web') ?>/images/staff12.png" class="<?= $staff->username ?>" style="width:98px;">
                        <?php } ?>
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-7 col-xs-9 container-staff-desc">
                        <div class="staff-desc">
                            <p class="staff-name"><?= $staff->username ?></p>
                            <small>Designation</small>
                            <ul class="list-inline list-staff-ratings">
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star"></i></li>
                                <li><i class="fa fa-star-half-o"></i></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <hr>
            </div>
        <?php }


    }
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $subdomain = str_replace('.boocked.com','',$_SERVER['HTTP_HOST']);
        $business_information  = BusinessInformation::find()->where(['business_subdomain'=>$subdomain])->one();
        if(empty($business_information)){
            return 'Business not available';
        }
        $business_id = $business_information->business_id;
        $business_information = BusinessInformation::findOne($business_id);
        $model = new ContactForm();
        //  && $model->contact(Yii::$app->params['adminEmail'])
        if ($model->load(Yii::$app->request->post()) ) {
            print_r($model->contact('sikandarawan91@gmail.com'));

            //  exit();
            Yii::$app->session->setFlash('contactFormSubmitted');
            //    print_r($model->contact('sikandarawan91@gmail.com'));
            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
                'business_information'=>$business_information
            ]);
        }
    }

    public function actionAbout()
    {
        $subdomain = str_replace('.boocked.com','',$_SERVER['HTTP_HOST']);
        $business_information  = BusinessInformation::find()->where(['business_subdomain'=>$subdomain])->one();
        if(empty($business_information)){
            return 'Business not available';
        }
        $business_id = $business_information->business_id;
        $query = Services::find()->joinWith(['weeklyrecurringtime']);
        $services = $query->select(['*'])->where(['services.business_id'=>$business_id,'services.service_status'=>'active'])
            ->andWhere(['not', ['wrt_id' => null]])->andWhere(['not', ['services.service_car_image1' => '']])->all();
        $query1 = Services::find()->joinWith(['weeklyrecurringtime']);
        $videosservices = $query1->select(['*'])->where(['services.business_id'=>$business_id,'services.service_status'=>'active'])
            ->andWhere(['not', ['weekly_recurring_time.wrt_id' => '']])
            //  ->andWhere(['not', ['services.service_video' => null]])
            ->andWhere(['not',['service_video' => '']])->all();

        $business_information = BusinessInformation::findOne(['business_id'=>$business_id]);
        $staff = Staff::find()->where(['business_id'=>$business_id])->all();



        return $this->render('about',[
            'business_information'=>$business_information,
            'services'=>$services,
            'videosservices'=>$videosservices,
            'staff'=>$staff,
        ]);
    }
    public function actionCheckavailableslots()
    {
        if(isset($_SESSION['book_service_id'])){
            unset($_SESSION['book_slot']);
            unset($_SESSION['book_service_id']);
            unset($_SESSION['book_staff_id']);
        }
        $service = $_POST['service'];
        $date = $_POST['date'];
        $timestamp = strtotime($date);
        $day = strtolower(date('l', $timestamp));
        if($_POST['staff'] != 0){
            $staff_id =  $_POST['staff'];
        }else {
            $staff_id = "";
        }
        $query = WeeklyRecurringTime::find()->joinWith(['services','staff']);
        $weekly_times = $query->select(['*'])->where(['weekly_recurring_time.service_id' => $service])
            ->andFilterWhere(['and',['weekly_recurring_time.staff_id' => $staff_id ]])->andWhere(['not', ['user.id' => null]])
            ->all();
//print_r($weekly_times);
        //      exit();
        $appointments = Appointments::find()->where(['appointment_date'=>$date])->all();


        if(!empty($weekly_times)) {
            foreach ($weekly_times as $weeklytimes) {
                $sunday = json_decode($weeklytimes->$day, true);
                $sunday_size = sizeof($sunday['from']);
                //    print_r($sunday['to']);
                //    print_r($sunday['from']);
                $service_duration = $weeklytimes->services['service_duration'];

                $timeslots = '';

                for ($x = 0; $x != $sunday_size; $x++) {
                    $allSlots = [];
                    $BlockSlots=[];
                    if (!empty($sunday['from'][$x]) && !empty($sunday['to'][$x])) {

                        $sunday['from'][$x] = strtotime($date.' '. $sunday['from'][$x]);
                        $sunday['to'][$x] = strtotime($date.' '. $sunday['to'][$x]);

                        $ad=0;
                        while ($sunday['from'][$x] < $sunday['to'][$x]) {
                            if($ad!=0){
                                $sunday['from'][$x] = strtotime('+1 hour', $sunday['from'][$x]);
                            }


                            /*   $sunday['from'][$x] = strtotime($sunday['from'][$x]);
                               $sunday['to'][$x] = strtotime($sunday['to'][$x]);

                               while ($sunday['from'][$x] < $sunday['to'][$x]) {
                                   $sunday['from'][$x] = strtotime('+1 hour', $sunday['from'][$x]); */
                            $timeslots[] = $sunday['from'][$x];
                            $ad++;
                        }
                        ?><?php
                        $i = 1;
                        if (!empty($timeslots)) {

                            foreach ($timeslots as $slot) {
                                $startSlot = $slot;
                                $endSlot = strtotime('+ 1 hour', $startSlot);
                                if (!empty($appointments)) {
                                    foreach ($appointments as $appointment) {

                                        // '+'.$service_duration.' minutes', 06:30:00 2016-07-29
                                        $apnt_start_time = strtotime($date.' '.$appointment->appointment_start_time);
                                        //   $apnt_start_time = strtotime($appointment->appointment_start_time.' ' );
                                        $apnt_end_time = strtotime('+' . $service_duration . ' minutes', $apnt_start_time);
                                        if (($startSlot > $apnt_start_time && $endSlot > $apnt_start_time && $startSlot > $apnt_end_time && $endSlot >= $apnt_end_time && $startSlot != $apnt_start_time ) || ($startSlot < $apnt_start_time && $endSlot <= $apnt_start_time && $startSlot <= $apnt_end_time && $endSlot <= $apnt_end_time && $startSlot != $apnt_start_time )) {

                                            $allSlots[] = $startSlot;


                                        }else {
                                            $BlockSlots[]=$startSlot;

                                        }
                                        $i++;




                                        $i = 1;
                                    }
                                } else {
                                    return 1;
                                }

                                ?>


                            <?php }
                        }
                    }


                }
                //  $allSlots=[];
                //  print_r('aaa');


            }
        }else {

            return 0;
        }
        $allSlots = array_unique($allSlots);
        // foreach($allSlots as $alot){
        //  echo '   All  '.date('Y-m-d H:i:s',$alot);

        //  }
        //   echo 'block';
        //  foreach($BlockSlots as $alot){
        //      echo  ' block   '.date('Y-m-d H:i:s',$alot);

        //    }
        //      echo 'Block dates';
        //    print_r($allSlots);
        //   print_r($BlockSlots);
        $slotsfinal = array_diff($allSlots, $BlockSlots);
        //   foreach($slotsfinal as $alot){
        //   echo  ' final   '.date('Y-m-d H:i:s',$alot);

//        }
        //    echo '          size         :   '.sizeof($slotsfinal);
        return sizeof($slotsfinal);


    }
    public function actionSlots()
    {
        //  service will not show untill its not assigned to staff
        $date = $_POST['date'];
        $service_id  = $_POST['service'];
        $BlockSlots['slot']=[];

        $timestamp = strtotime($date);
        $day = strtolower(date('l', $timestamp));
        if($_POST['staff']!=0){
            $staff_id =  $_POST['staff'];
        }else {
            $staff_id = "";
        }
        if(Yii::$app->session->get('bk_apointment_time_slot')=='fixed') {
            if (Yii::$app->session->get('bk_fixed_interval_customer') != '') {
                $interval_gap  =  '+ '.Yii::$app->session->get('bk_fixed_interval_customer').' minutes';
            } else {
                $interval_gap  = '+ 30 minutes';
            }


        }

        $query = WeeklyRecurringTime::find()->joinWith(['services','staff']);
        $weekly_times = $query->select(['*'])->where(['weekly_recurring_time.service_id' => $service_id])
            ->andFilterWhere(['and',['weekly_recurring_time.staff_id' => $staff_id ]])->andWhere(['not', ['user.id' => null]])
            ->all();
        //   print_r($weekly_times);
        //   exit();
        $appointments = Appointments::find()->where(['appointment_date'=>$date])->all();



        foreach($weekly_times as $weeklytimes) {

            $staff=$weeklytimes->staff_id;
            //  exit();
            //  echo  $weekly_times['services'];

            $sunday = json_decode($weeklytimes->$day, true);


            $sunday_size = sizeof($sunday['from']);


            $service_duration = $weeklytimes->services['service_duration'];

            for ($x = 0; $x != $sunday_size; $x++) {
                //  echo '<pre>';

                // echo '</pre>';
                if (!empty($sunday['from'][$x]) && !empty($sunday['to'][$x])) {

                    $sunday['from'][$x] = strtotime($date.' '. $sunday['from'][$x]);
                    $sunday['to'][$x] = strtotime($date.' '. $sunday['to'][$x]);
                    $abiterate = 0;
                    $ad=0;
                    while ($sunday['from'][$x] < $sunday['to'][$x]) {
                        if($ad!=0){
                            //  $sunday['from'][$x] = strtotime('+1 hour', $sunday['from'][$x]);
                            $sunday['from'][$x] = strtotime($interval_gap, $sunday['from'][$x]);
                        }
                        //   $timeslots['start'] =   $sunday['from'][$x];

                        $timeslot[] = date('Y-m-d H:i:s',$sunday['from'][$x]);
                        $timeslots[] = $sunday['from'][$x];
                        $abiterate++;
                        $ad++;
                    }


                    ?>

                    <?php // foreach ( $timeslots as $slot ) {
                    //                       echo date('Y-m-d H:i:s', $slot);


                    //          }


                    ?>


                    <?php
                    $i = 1;
                    if ($abiterate > 0) {
                        foreach ($timeslots as $slot) {
                            $startSlot = $slot;
                            $endSlot = strtotime('+ 1 hour',$startSlot);
                            if (!empty($appointments)) {
                                foreach ($appointments as $appointment) {
                                    //   if ($i == 1) {
                                    //     echo 'Apnt '.$appointment->appointment_start_time.'               slot '.date('Y-m-d H:i:s', $startSlot).'               ';
                                    // '+'.$service_duration.' minutes', 06:30:00 2016-07-29
                                    $apnt_start_time = strtotime($date.' '.$appointment->appointment_start_time);
                                    //     echo date('Y-m-d H:i:s', $apnt_start_time).' <br/> ';
                                    //  $apnt_start_time = strtotime('02:50:00 2016-07-29');
                                    $apnt_end_time = strtotime('+' . $service_duration . ' minutes', $apnt_start_time);
                                    if (($startSlot > $apnt_start_time && $endSlot > $apnt_start_time && $startSlot > $apnt_end_time && $endSlot >= $apnt_end_time && $startSlot != $apnt_start_time ) || ($startSlot < $apnt_start_time && $endSlot <= $apnt_start_time && $startSlot <= $apnt_end_time && $endSlot <= $apnt_end_time && $startSlot != $apnt_start_time )) {
                                        //   echo 'appointment not exist in slot';
                                        //   echo 'hereeee   ';
                                        //   echo date('Y-m-d H:i:s', $startSlot) . '      ' . date('Y-m-d H:i:s', $endSlot) . '  appointmnet between   ' . date('Y-m-d H:i:s', $apnt_start_time) . ' to  ' . date('Y-m-d H:i:s', $apnt_end_time) . '<br/>';


                                        $allSlots['slot'][] = $startSlot;
                                        $allSlots['staff'][] = $weeklytimes->staff_id;
                                        $allSlots['service'][] = $weeklytimes->service_id;





                                    }else {
                                        $BlockSlots['slot'][]=$startSlot;
                                        $BlockSlots['staff'][] = $weeklytimes->staff_id;
                                        $BlockSlots['service'][] = $weeklytimes->service_id;

                                    }

                                    $i++;

                                }

                            } else {
                                $allSlots['slot'][] = $startSlot;
                                $allSlots['staff'][] = $weeklytimes->staff_id;
                                $allSlots['service'][] = $weeklytimes->service_id;


                            }
                            $i = 1;
                        }
                    }else {
                        $allSlots['slot'] =[];
                        $timeslots=[];


                    }
//exit();

                    ?>


                <?php }else {

                    // echo 'empty';

                }
            }
        }
        //   print_r($allSlots);
//print_r($BlockSlots['slot']);
        //      print_r($allSlots['staff']);

        $allSlots['slot'] = array_unique($allSlots['slot']);
        foreach ($allSlots['slot'] as $slot){
            //        echo '  all slot '. date('Y-m-d H:i:s', $slot).'<br/>';;

        }
        //  echo '<pre>';
        //  print_r($BlockSlots['slot']);
        //  print_r(array_diff($allSlots['slot'], $BlockSlots['slot']));
        //  echo '</pre>';
        $allSlots['slot']  =  array_diff($allSlots['slot'], $BlockSlots['slot']);
        // print_r($allSlots['slot']);
        // print_r($BlockSlots['slot']);
        //  exit();

        // print_r($allSlots);


        return $this->renderAjax('slots', [
            'allSlots' => $allSlots,
            'totalslots'=>$timeslots,
            'staff'=>$staff,
        ]);

    }
    public function actionSlotbooking()
    {
        if(isset($_POST['newlogin'])){
            $session = new Session();
            $session->open();
            $service_id = $_SESSION['book_service_id'];
            $slot = $_SESSION['book_slot'];
            $staff = $_SESSION['book_staff_id'];
            unset($_SESSION['book_service_id']);
            unset($_SESSION['book_slot']);
            unset($_SESSION['book_staff_id']);

            $model = Services::find()->where(['business_id' => 1, 'service_id' => $service_id])->all();
            return $this->renderAjax('order_summary', [
                'model' => $model,
                'slot' => $slot,
                'login' => 0,
                'staff' => $staff,
            ]);

        }else {

            $service = $_POST['service'];
            $slot = $_POST['slot'];
            $staff = $_POST['staff'];
            if (Yii::$app->user->isGuest) {


                $model = Services::find()->where(['business_id' => 1, 'service_id' => $service])->all();
                $model1 = new LoginForm();
                $_SESSION['book_service_id'] = $service;
                $_SESSION['book_slot'] = $slot;
                $_SESSION['book_staff_id'] = $staff;
                return $this->renderAjax('order_summary', [
                    'model' => $model,
                    //   'slot'=>$slot,
                    'login' => 1,
                    'model1' => $model1,
                ]);

            } else {
                unset($_SESSION['book_service_id']);
                unset($_SESSION['book_slot']);
                unset($_SESSION['book_staff_id']);
                $service_id = $service;
                $slot = $slot;

                $model = Services::find()->where(['business_id' => 1, 'service_id' => $service_id])->all();
                return $this->renderAjax('order_summary', [
                    'model' => $model,
                    'slot' => $slot,
                    'login' => 0,
                    'staff' => $staff,
                ]);
            }
        }

    }





    public function actionSignup()
    {
        $business_id = 1;
        $business_information = BusinessInformation::findOne($business_id);
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                $notification_model = new Notification();
                $notification_model->notification_type='2';
                $notification_model->business_id= $model->business_id;
                if($notification_model->save(false)) {
                    //if (Yii::$app->getUser()->login($user)) {
                    return $this->redirect(Yii::getAlias('@web') . '/index.php/users/index', 302);
                    //}
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
            'business_information'=>$business_information,
        ]);
    }

    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->getSession()->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->getSession()->setFlash('error', 'Sorry, we are unable to reset password for email provided.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->getSession()->setFlash('success', 'New password was saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    public function checkPersmission($permission){
        $this_user_permissions = Yii::$app->authManager->getPermissions();
        if($this_user_permissions["/*"])
        {
            return true;
        } elseif($this_user_permissions[$permission])
        {
            return true;
        } else {
            return false;
        }
    }

    public function actionBookingloginvalidate()
    {
        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            return 1;
        } else {
            // validation failed: $errors is an array containing error messages
            return 'Incorrect Username/Password';
        }
    }
    public function actionBookinglogin()
    {
        // exit('hereeeeeeeee');
        //   if (!\Yii::$app->user->isGuest) {
        //       return $this->goHome();
        //   }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            //  if ($model->load(Yii::$app->request->post())) {
            return 1;
        } else {
            return 3;
            // return $this->renderAjax('login', [
            //       'model' => $model,
            //   ]);
        }
    }
    public function actionBookingsignup()
    {
        // if(Yii::$app->request->post()!=null) {
        $business_id=1;
        $model = new SignupForm();
        $model->business_id = $business_id;

        //   if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
        //      Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        //  echo 'sdasdasdasdas';
        //       return ActiveForm::validate($model);

        //     }
        //   if ($model->load(Yii::$app->request->post())) {

        if ($model->load(Yii::$app->request->post())) {
            //  print_r($model);

            $path   = Yii::getAlias('@webroot');
            $admin_sidepath = substr($path, 0, strrpos($path, "customer"));
            //$admin_sidepath . '/admin/web/';
            $model->profile_image = UploadedFile::getInstance($model,'profile_image');



            $random_digit = rand(0000, 9999999);

            if ($model->profile_image->saveAs($admin_sidepath . '/admin/web/uploads/' . str_replace(' ', '', $model->profile_image->baseName) . $random_digit . '.' . $model->profile_image->extension)) {
                $model->profile_image = 'uploads/' . str_replace(' ', '', $model->profile_image->baseName) . $random_digit . '.' . $model->profile_image->extension;

            }


            //  $model->imageFiles = UploadedFile::getInstances($model, 'imageFiles');
            //  $model->file = UploadedFile::getInstance($model, 'imageFile');
            //  $model->file->saveAs('uploads/signature/' . $model->user_id . '.' . $model->file->extension);
            //  $model->url = 'uploads/signature/' . $model->user_id . '.' . $model->file->extension;

            //           exit();
            if ($user = $model->signup()) {

                $notification_model = new Notification();
                $notification_model->notification_type = '2';
                $notification_model->business_id = $model->business_id;
                if ($notification_model->save(false)) {
                    if (Yii::$app->getUser()->login($user)) {
                        return $this->goBack();
                        //   return $this->redirect(Yii::getAlias('@web').'/index.php/site/index',302);
                    }
                }
            } else {

                //  return $this->actionSignup();
                exit('not load');

            }

        }

        //  return $this->renderAjax('signup', [
        //      'model' => $model,
        //  ]);
        // }else {
        //      return $this->redirect(Yii::getAlias('@web').'/index.php/site/index',302);
        //  }
    }
    public function actionBookingsignup_validate()
    {
        // if(Yii::$app->request->post()!=null) {
        $business_id=1;
        $model = new SignupForm();
        $model->business_id = $business_id;

        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            //  echo 'sdasdasdasdas';
            return ActiveForm::validate($model);

        }



    }
    public function actionBookingappointment()
    {

        $model = new Appointments();
        $request = \Yii::$app->getRequest();
        $model->business_id = Yii::$app->user->identity->business_id;
        //  print_r($request->post());
        if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
            $notification_model = new Notification();
            $notification_model->notification_type='1';
            $notification_model->business_id = $model->business_id;
            if($notification_model->save(false)) {
                return 1;
            }
        }

        return 0;
    }

// Social Icons
    public function oAuthSuccess($client) {
        $attributes = $client->getUserAttributes();
        $business_id = 1;
        if($_REQUEST['authclient']=='facebook') {
            //   print_r($attributes);
            $name = explode(" ", $attributes['name']);
            $first_name = $name[0];
            if(isset($name[1])){
                $last_name = $name[1];

            }

            $email = $attributes['email'];
        }else {
            $email = $attributes['emails'][0]['value'];
            $first_name = $attributes['name']['givenName'];
            $last_name = $attributes['name']['familyName'];
            //    echo '<pre>';
            //    print_r($attributes);
            //    echo '</pre>';
        }
        $user =   User::find()->where(['email'=>$email,'business_id'=>$business_id])->one();
        if(!empty($user)){
            Yii::$app->user->login($user);

        }else{
            $model =  new User();
            $model->first_name = $first_name;
            $model->last_name = $last_name;
            $model->email = $email;
            $model->username = $email;
            $model->business_id = $business_id;

            $alphabets = range('A','Z');
            $numbers = range('0','9');
            $additional_characters = array('_','.');
            $final_array = array_merge($alphabets,$numbers,$additional_characters);

            $password = '';
            $length=20;
            while($length--) {
                $key = array_rand($final_array);
                $password .= $final_array[$key];
            }
            $model->setPassword(md5($password));
            $model->generateAuthKey();

            if($model->save(false)){
                $users =   User::find()->where(['customer_id'=>$model->customer_id,'business_id'=>$business_id])->one();
                Yii::$app->user->login($users);



            }

            // Save session attribute user from FB
            //   $session = Yii::$app->session;
            //   $session['attributes']=$attributes;
            // redirect to form signup, variabel global set to successUrl
            //    $this->successUrl = \yii\helpers\Url::to(['signup']);
        }
        //     exit();


    }
    function coupansharing_id(){
        $this->layout = 'sharing';
        return $this->renderAjax('sharing');


    }

    public function actionAddress_states(){
        if($_POST['type']=='country'){
            $country_id =  $_POST['country_id'];
            $states = States::find()->where(['country_id'=>$country_id])->all();
            $all_states='';
            foreach ($states as $state){


                $all_states .= '<option value="'.$state->id.'" >'.$state->name.'</option>';

            }
            return $all_states;

        }
        if($_POST['type']=='state'){
            $state_id =  $_POST['state_id'];
            $cities = Cities::find()->where(['state_id'=>$state_id])->all();
            $all_cities='';
            foreach ($cities as $city){


                $all_cities .= '<option value="'.$city->id.'" >'.$city->name.'</option>';

            }
            return $all_cities;

        }

    }
    public function actionReviews($apnt_id,$bid){
        // $apnt_id=4;
        //  $bid=1;
        $appointment = Appointments::findOne(['appointment_id'=>$apnt_id,'business_id'=>$bid]);
        $business_id = $bid;
        $business_information  = BusinessInformation::findOne($business_id);
        $this->layout = 'noheader';

        $model = new Reviews();
        //   ,'business_id'=>$bid
        $reviews = Reviews::findOne(['appointment_id'=>$apnt_id]);
        if(isset($reviews)){
            $review_status = $reviews->review_status;
            $review_text   = $reviews->review_text;
        }else {
            $review_status = '';
            $review_text   = '';
        }

        $query = Appointments::find()->joinWith(['services','customers']);

        $items = $query->select(['*'])->where(['appointments.appointment_id'=>$apnt_id])->one();
        //    echo '<pre>';
        //    print_r($items);
        //    echo '<br/><br/><br/><br/><br/><br/><br/>';
        //    print_r($items['services']['service_name']);
        //    exit();

        return $this->render('reviews', [
            'model' => $model,
            'appointment'=>$appointment,
            'business_information'=>$business_information,
            'items'=>$items,
            'apnt_id'=>$apnt_id,
            'review_status'=>$review_status,
            'review_text'=>$review_text
        ]);
    }
    public function actionReviewsupdate(){
        if(isset($_POST)){
            $apnt_id =  $_POST['apnt_id'];
            if($_POST['type']==1) {
                $review = Reviews::findOne(['appointment_id' => $_POST['apnt_id']]);
                $appointment = Appointments::findOne(['appointment_id'=>$apnt_id]);
                if (!empty($review)) {
                    $model = $review;
                    $model->review_status = $_POST['feedback'];
                    if($model->save(false)){
                        return 1;
                    }
                } else {

                    $model = new Reviews();
                    $model->service_id = $appointment->service_id;
                    $model->customer_id = $appointment->customer_id;
                    $model->staff_id =  $appointment->user_id;
                    $model->appointment_id = $apnt_id;
                    $model->review_status = $_POST['feedback'];
                    if($model->save(false)){
                        return 1;
                    }

                }
            }else if($_POST['type']==2) {

                $review = Reviews::findOne(['appointment_id' => $_POST['apnt_id']]);
                $appointment = Appointments::findOne(['appointment_id'=>$apnt_id]);

                if (!empty($review)) {
                    $model = $review;
                    $model->review_text = $_POST['feedback'];
                    if($model->save(false)){
                        return 1;
                    }
                } else {

                    $model = new Reviews();
                    $model->service_id = $appointment->service_id;
                    $model->customer_id = $appointment->customer_id;
                    $model->staff_id =  $appointment->user_id;
                    $model->appointment_id = $apnt_id;
                    $model->review_text = $_POST['feedback'];
                    if($model->save(false)){
                        return 1;
                    }

                }



            }


        }
    }

    public function actionServicereview(){
        $step = $_POST['step'];
        $apnt_id = $_POST['apnt_id'];
        $business_id = 1;
        $appointment = Appointments::findOne(['appointment_id'=>$apnt_id,'business_id'=>$business_id]);
        if($step==1){

            $business_information  = BusinessInformation::findOne($business_id);


            $model = new Reviews();
            //   ,'business_id'=>$bid
            $reviews = Reviews::findOne(['appointment_id'=>$apnt_id]);
            if(isset($reviews)){
                $review_status = $reviews->review_status;
                $review_text   = $reviews->review_text;
            }else {
                $review_status = '';
                $review_text   = '';
            }

            $query = Appointments::find()->joinWith(['services','customers']);

            $items = $query->select(['*'])->where(['appointments.appointment_id'=>$apnt_id])->one();
            //    echo '<pre>';
            //    print_r($items);
            //    echo '<br/><br/><br/><br/><br/><br/><br/>';
            //    print_r($items['services']['service_name']);
            //    exit();

            return $this->renderAjax('service_reviews', [
                'model' => $model,
                'appointment'=>$appointment,
                'business_information'=>$business_information,
                'items'=>$items,
                'apnt_id'=>$apnt_id,
                'review_status'=>$review_status,
                'review_text'=>$review_text
            ]);
        }else if($step==2){

        }

    }


    public function actionCancel_appointment(){
        $apnt_id = $_POST['apnt_id'];
        $business_id =Yii::$app->user->identity->business_id;
        $customer_id =Yii::$app->user->identity->customer_id;
        $appointment = Appointments::find()->where(['business_id'=>$business_id,'appointment_id'=>$apnt_id])->one();
        $appointment->appointment_status='cancel';
        if($appointment->save(false)){
            return 1;
        }
    }
    public function actionReschedule(){
        $apnt_id = $_POST['apnt_id'];
        $business_id =Yii::$app->user->identity->business_id;
        $appointment = Appointments::find()->where(['business_id'=>$business_id,'appointment_id'=>$apnt_id])->one();
        return $this->renderAjax('reschedule', [
            'appointment'=>$appointment
        ]);

    }
    public function actionRescheduleslots()
    {
        //  service will not show untill its not assigned to staff
        $date = $_POST['date'];
        $service_id  = $_POST['service'];
        $BlockSlots['slot']=[];

        $timestamp = strtotime($date);
        $day = strtolower(date('l', $timestamp));
        if($_POST['staff']!=0){
            $staff_id =  $_POST['staff'];
        }else {
            $staff_id = "";
        }
        if(Yii::$app->session->get('bk_apointment_time_slot')=='fixed') {
            if (Yii::$app->session->get('bk_fixed_interval_customer') != '') {
                $interval_gap  =  '+ '.Yii::$app->session->get('bk_fixed_interval_customer').' minutes';
            } else {
                $interval_gap  = '+ 30 minutes';
            }


        }

        $query = WeeklyRecurringTime::find()->joinWith(['services','staff']);
        $weekly_times = $query->select(['*'])->where(['weekly_recurring_time.service_id' => $service_id])
            ->andFilterWhere(['and',['weekly_recurring_time.staff_id' => $staff_id ]])->andWhere(['not', ['user.id' => null]])
            ->all();
        //   print_r($weekly_times);
        //   exit();
        $appointments = Appointments::find()->where(['appointment_date'=>$date])->all();



        foreach($weekly_times as $weeklytimes) {

            $staff=$weeklytimes->staff_id;
            //  exit();
            //  echo  $weekly_times['services'];

            $sunday = json_decode($weeklytimes->$day, true);


            $sunday_size = sizeof($sunday['from']);


            $service_duration = $weeklytimes->services['service_duration'];

            for ($x = 0; $x != $sunday_size; $x++) {
                //  echo '<pre>';

                // echo '</pre>';
                if (!empty($sunday['from'][$x]) && !empty($sunday['to'][$x])) {

                    $sunday['from'][$x] = strtotime($date.' '. $sunday['from'][$x]);
                    $sunday['to'][$x] = strtotime($date.' '. $sunday['to'][$x]);
                    $abiterate = 0;
                    $ad=0;
                    while ($sunday['from'][$x] < $sunday['to'][$x]) {
                        if($ad!=0){
                            //  $sunday['from'][$x] = strtotime('+1 hour', $sunday['from'][$x]);
                            $sunday['from'][$x] = strtotime($interval_gap, $sunday['from'][$x]);
                        }
                        //   $timeslots['start'] =   $sunday['from'][$x];

                        $timeslot[] = date('Y-m-d H:i:s',$sunday['from'][$x]);
                        $timeslots[] = $sunday['from'][$x];
                        $abiterate++;
                        $ad++;
                    }


                    ?>

                    <?php // foreach ( $timeslots as $slot ) {
                    //                       echo date('Y-m-d H:i:s', $slot);


                    //          }


                    ?>


                    <?php
                    $i = 1;
                    if ($abiterate > 0) {
                        foreach ($timeslots as $slot) {
                            $startSlot = $slot;
                            $endSlot = strtotime('+ 1 hour',$startSlot);
                            if (!empty($appointments)) {
                                foreach ($appointments as $appointment) {
                                    //   if ($i == 1) {
                                    //     echo 'Apnt '.$appointment->appointment_start_time.'               slot '.date('Y-m-d H:i:s', $startSlot).'               ';
                                    // '+'.$service_duration.' minutes', 06:30:00 2016-07-29
                                    $apnt_start_time = strtotime($date.' '.$appointment->appointment_start_time);
                                    //     echo date('Y-m-d H:i:s', $apnt_start_time).' <br/> ';
                                    //  $apnt_start_time = strtotime('02:50:00 2016-07-29');
                                    $apnt_end_time = strtotime('+' . $service_duration . ' minutes', $apnt_start_time);
                                    if (($startSlot > $apnt_start_time && $endSlot > $apnt_start_time && $startSlot > $apnt_end_time && $endSlot >= $apnt_end_time && $startSlot != $apnt_start_time ) || ($startSlot < $apnt_start_time && $endSlot <= $apnt_start_time && $startSlot <= $apnt_end_time && $endSlot <= $apnt_end_time && $startSlot != $apnt_start_time )) {
                                        //   echo 'appointment not exist in slot';
                                        //   echo 'hereeee   ';
                                        //   echo date('Y-m-d H:i:s', $startSlot) . '      ' . date('Y-m-d H:i:s', $endSlot) . '  appointmnet between   ' . date('Y-m-d H:i:s', $apnt_start_time) . ' to  ' . date('Y-m-d H:i:s', $apnt_end_time) . '<br/>';


                                        $allSlots['slot'][] = $startSlot;
                                        $allSlots['staff'][] = $weeklytimes->staff_id;
                                        $allSlots['service'][] = $weeklytimes->service_id;





                                    }else {
                                        $BlockSlots['slot'][]=$startSlot;
                                        $BlockSlots['staff'][] = $weeklytimes->staff_id;
                                        $BlockSlots['service'][] = $weeklytimes->service_id;

                                    }

                                    $i++;

                                }

                            } else {
                                $allSlots['slot'][] = $startSlot;
                                $allSlots['staff'][] = $weeklytimes->staff_id;
                                $allSlots['service'][] = $weeklytimes->service_id;


                            }
                            $i = 1;
                        }
                    }else {
                        $allSlots['slot'] =[];
                        $timeslots=[];


                    }
//exit();

                    ?>


                <?php }else {

                    // echo 'empty';

                }
            }
        }
        //   print_r($allSlots);
//print_r($BlockSlots['slot']);
        //      print_r($allSlots['staff']);

        $allSlots['slot'] = array_unique($allSlots['slot']);
        foreach ($allSlots['slot'] as $slot){
            //        echo '  all slot '. date('Y-m-d H:i:s', $slot).'<br/>';;

        }
        //  echo '<pre>';
        //  print_r($BlockSlots['slot']);
        //  print_r(array_diff($allSlots['slot'], $BlockSlots['slot']));
        //  echo '</pre>';
        $allSlots['slot']  =  array_diff($allSlots['slot'], $BlockSlots['slot']);
        // print_r($allSlots['slot']);
        // print_r($BlockSlots['slot']);
        //  exit();

        // print_r($allSlots);







        echo '<select>';
        foreach($allSlots['slot'] as $slot) {
            echo  '<option class="option_nothours" value="'.date('h:i A', $slot).'"><span style="color:red">'.date('h:i A', $slot).'</span></option>';
        }
        //  $hours = array_diff($hours, $not_hours);
        //    print_r($hours);

        echo '</select>';







    }

    public function actionRescheduleappointment(){
        $old_appointment_id = $_POST['apntID'];
        $service_id =  $_POST['service'];
        $staff_id =  $_POST['staff'];
        $reschedule_date =  date('Y-m-d',strtotime($_POST['reschedule_date']));;
        $reschedule_time =  date('H:i',strtotime($_POST['reschedule_time']));
        //  echo($reschedule_time);
        //  echo($reschedule_date);
        //  exit();
        $business_id = Yii::$app->user->identity->business_id;
        $old_appointment = Appointments::find()->where(['business_id'=>$business_id,'appointment_id'=>$old_appointment_id])->one();
        if($old_appointment){
            $new_appointment = new Appointments();
            $new_appointment->appointment_start_time = $reschedule_time;
            $new_appointment->appointment_date = $reschedule_date;
            $new_appointment->business_id = $business_id;
            $new_appointment->user_id = $staff_id;
            $new_appointment->service_id = $service_id;
            $new_appointment->customer_id = $old_appointment->customer_id;
            if($new_appointment->save(false)){
                $old_appointment->apnt_reschedule_id =$new_appointment->appointment_id;
                if($old_appointment->save(false)){
                    return 1;
                }


            }
        }

    }

}
