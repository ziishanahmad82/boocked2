<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "appointments".
 *
 * @property integer $appointment_id
 * @property integer $service_id
 * @property integer $customer_id
 * @property integer $user_id
 * @property string $appointment_start_time
 * @property string $appointment_end_time
 * @property string $appointment_date
 * @property integer $business_id
 * @property string $confirmation_number
 * @property string $appointment_status
 * @property string $date_created
 */
class Appointments extends \yii\db\ActiveRecord
{
    public $cnt;
    public $cntuser;
    public $m_date;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'appointments';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service_id', 'customer_id', 'user_id', 'appointment_start_time', 'appointment_end_time', 'appointment_date', 'business_id', 'confirmation_number', 'date_created'], 'required'],
            [['service_id', 'customer_id', 'user_id', 'business_id'], 'integer'],
            [['appointment_start_time', 'appointment_end_time', 'appointment_date', 'date_created'], 'safe'],
            [['confirmation_number', 'appointment_status', 'payment_status'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'appointment_id' => 'Appointment ID',
            'service_id' => 'Service ID',
            'customer_id' => 'Customer ID',
            'user_id' => 'User ID',
            'appointment_start_time' => 'Appointment Start Time',
            'appointment_end_time' => 'Appointment End Time',
            'appointment_date' => 'Appointment Date',
            'business_id' => 'Business ID',
            'confirmation_number' => 'Confirmation Number',
            'appointment_status' => 'Appointment Status',
            'date_created' => 'Date Created',
        ];
    }

    /**
     * @inheritdoc
     * @return AppointmentsQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AppointmentsQuery(get_called_class());
    }
    public function getServices() {
        return  $this->hasOne(Services::className(), ['service_id' => 'service_id']);
    }
    public function getUsers() {
        return  $this->hasOne(Users::className(), ['id' => 'user_id']);
    }
    public function getCustomers() {
        return  $this->hasOne(Customers::className(), ['customer_id' => 'customer_id']);
    }
    public function getAppointments_payments() {
        return  $this->hasOne(AppointmentsPayments::className(), ['appointment_id' => 'appointment_id']);
    }
}
