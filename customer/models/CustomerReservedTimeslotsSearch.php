<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\CustomerReservedTimeslots;

/**
 * CustomerReservedTimeslotsSearch represents the model behind the search form about `app\models\CustomerReservedTimeslots`.
 */
class CustomerReservedTimeslotsSearch extends CustomerReservedTimeslots
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['timeslot_id', 'customer_id', 'location_id'], 'integer'],
            [['test_date', 'test_start_time', 'test_end_time'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CustomerReservedTimeslots::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'timeslot_id' => $this->timeslot_id,
            'customer_id' => $this->customer_id,
            'location_id' => $this->location_id,
            'test_date' => $this->test_date,
            'test_start_time' => $this->test_start_time,
            'test_end_time' => $this->test_end_time,
        ]);

        return $dataProvider;
    }
}
