<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "holidays".
 *
 * @property integer $holiday_id
 * @property string $holiday_date
 * @property string $holiday_name
 * @property integer $holiday_span
 */
class Holidays extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'holidays';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['holiday_date', 'holiday_name', 'holiday_span'], 'required'],
            [['holiday_date'], 'safe'],
            [['holiday_span'], 'integer'],
            [['holiday_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'holiday_id' => 'Holiday ID',
            'holiday_date' => 'Holiday Date',
            'holiday_name' => 'Holiday Name',
            'holiday_span' => 'Holiday Span',
        ];
    }
}
