<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "weekly_recurring_time".
 *
 * @property integer $wrt_id
 * @property integer $staff_id
 * @property integer $service_id
 * @property string $monday
 * @property string $tuesday
 * @property string $wednesday
 * @property string $thursday
 * @property string $friday
 * @property string $saturday
 * @property string $sunday
 */
class WeeklyRecurringTime extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'weekly_recurring_time';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['staff_id', 'service_id', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'], 'required'],
            [['staff_id', 'service_id'], 'integer'],
            [['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'wrt_id' => 'Wrt ID',
            'staff_id' => 'Staff ID',
            'service_id' => 'Service ID',
            'monday' => 'Monday',
            'tuesday' => 'Tuesday',
            'wednesday' => 'Wednesday',
            'thursday' => 'Thursday',
            'friday' => 'Friday',
            'saturday' => 'Saturday',
            'sunday' => 'Sunday',
        ];
    }
    public function getServices() {
        return  $this->hasOne(Services::className(), ['service_id' => 'service_id']);
    }
    public function getStaff() {
        return  $this->hasOne(Staff::className(), ['id' => 'staff_id']);
    }
}
