<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "business_contact_form".
 *
 * @property integer $contact_id
 * @property string $contact_name
 * @property string $contact_email
 * @property string $contact_message
 * @property string $business_id
 * @property string $created_at
 * @property string $updated_at
 */
class BusinessContactForm extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'business_contact_form';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contact_id', 'contact_name', 'contact_email', 'contact_message', 'business_id', 'created_at', 'updated_at'], 'required'],
            [['contact_id'], 'integer'],
            [['contact_message'], 'string'],
            [['contact_name', 'contact_email', 'business_id', 'created_at', 'updated_at'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'contact_id' => 'Contact ID',
            'contact_name' => 'Contact Name',
            'contact_email' => 'Contact Email',
            'contact_message' => 'Contact Message',
            'business_id' => 'Business ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
