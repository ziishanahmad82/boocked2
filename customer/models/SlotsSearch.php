<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Slots;

/**
 * SlotsSearch represents the model behind the search form about `app\models\Slots`.
 */
class SlotsSearch extends Slots
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'duration', 'reserved_by_customer_id'], 'integer'],
            [['calendar_type', 'slot_date', 'start_time', 'status', 'is_reserved'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Slots::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'duration' => $this->duration,
            'slot_date' => $this->slot_date,
            'start_time' => $this->start_time,
            'reserved_by_customer_id' => $this->reserved_by_customer_id,
        ]);

        $query->andFilterWhere(['like', 'calendar_type', $this->calendar_type])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'is_reserved', $this->is_reserved]);

        return $dataProvider;
    }
}
