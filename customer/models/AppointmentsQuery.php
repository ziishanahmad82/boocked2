<?php

namespace app\models;

/**
 * This is the ActiveQuery class for [[Appointments]].
 *
 * @see Appointments
 */
class AppointmentsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        $this->andWhere('[[status]]=1');
        return $this;
    }*/

    /**
     * @inheritdoc
     * @return Appointments[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * @inheritdoc
     * @return Appointments|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}