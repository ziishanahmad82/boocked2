<?php

namespace app\models;
use app\models\Resources;
use Yii;

/**
 * This is the model class for table "services".
 *
 * @property integer $service_id
 * @property string $service_name
 * @property string $service_description
 * @property integer $service_duration
 * @property string $service_price
 * @property integer $service_capacity
 * @property integer $business_id
 */
class Services extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'services';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['service_name', 'service_description', 'service_duration', 'service_price', 'service_capacity'], 'required'],
            [['service_description'], 'string'],
            [['service_duration', 'service_capacity', 'business_id','service_cat_id'], 'integer'],
            [['service_name'], 'string', 'max' => 255],
            [['service_price'], 'string', 'max' => 20],
            [['service_video'], 'file','extensions' => 'mp4, mp3','maxFiles' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'service_id' => 'Service ID',
            'service_name' => 'Service Name',
            'service_description' => 'Service Description',
            'service_duration' => 'Duration (Min)',
            'service_price' => 'Price',
            'service_capacity' => 'Capacity',
            'business_id' => 'Business ID',
            'service_cat_id'=> 'Service Category'
        ];
    }
    public function showServicename($id){
        $service_name = Services::findOne($id);
        return $service_name->service_name;

    }
    public function getResource() {
        return $this->hasMany(Resources::className(), ['resource_id' => 'resource_id'])
            ->viaTable('resouces_service', ['service_id' => 'service_id']);
    }
    public function getWeeklyrecurringtime() {
        return $this->hasOne(WeeklyRecurringTime::className(),  ['service_id' => 'service_id']);
    }
    public function getListServices(){
       // return (new \yii\db\Query())->select(['service_id', 'service_name']);
        $site_id = Yii::$app->user->identity->business_id;
        $modele = Services::find()
            ->where(['business_id' => $site_id])
            ->all();
        return $modele;

    }
    public function getServicesList($business_id){
        // return (new \yii\db\Query())->select(['service_id', 'service_name']);
      //  $site_id = 1;
        $modele = Services::find()
            ->where(['business_id' => $business_id])
            ->all();
        return $modele;

    }
    public function getServicebyid($id,$business_id){
        $service =  Services::find()->where(['business_id' => $business_id,'service_id'=>$id])->one();
        return $service;
    }
    public function getLinkedActiveServices(){
        $site_id = Yii::$app->user->identity->business_id;
        $model = Services::find()
            ->where(['business_id' => $site_id]) ->andWhere(['service_status'=>'active']) ->andWhere((['<>','linked_status', 'no']))
            ->all();

return $model;
    }
}
