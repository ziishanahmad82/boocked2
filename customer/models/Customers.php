<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "customers".
 *
 * @property integer $customer_id
 * @property string $learners_permit_number
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $date_of_birth
 * @property string $email
 * @property string $cell_phone
 * @property string $test_type
 * @property string $sub_test_type
 * @property string $license_class
 * @property integer $confirmed
 * @property integer $third_user_id
 *
 * @property CustomerReservedTimeslots[] $customerReservedTimeslots
 */
class Customers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $email_repeat;
    public $imageFile;
    public static function tableName()
    {
        return 'customers';
    }

    /**
     * @inheritdoc
     */

    public function rules()
    {
        return [
            [['learners_permit_number', 'first_name',  'last_name', 'date_of_birth', 'email', 'cell_phone', 'test_type', 'email_repeat', 'license_class'], 'required'],
            [['date_of_birth'], 'safe'],
            [['test_type'], 'string', 'max' => 6],
            [['sub_test_type'], 'string', 'max' => 255],
            [['license_class'], 'string', 'max' => 3],
            [['learners_permit_number', 'email'], 'string', 'max' => 255],
            [['first_name', 'middle_name', 'last_name'], 'string', 'max' => 100],
            [['cell_phone'], 'string', 'max' => 50],
            [['test_type2'], 'string', 'max' => 50],
            [['agreement_status'], 'string', 'max' => 15],

            [['email', 'email_repeat'], 'filter', 'filter' => 'trim'],
            [['email', 'email_repeat'], 'email'],
            ['email_repeat', 'compare', 'compareAttribute'=>'email', 'message' => Yii::t("app", "The email must match with repeat email")],
            //['email', 'unique', 'targetClass' => '\app\models\User', 'message' => Yii::t('app','This email address already exists in our record. Please use another one.')],
            [['confirmed'], 'integer'],
            [['third_user_id'], 'integer'],
            [['sub_test_type'], 'validateSubtesttype', 'skipOnError' => false],
//            [['sub_test_type'], 'required',
//                'when'=>function($model){
//
//                if(stristr($model->sub_test_type, 'Tank')){
//                    return false;
//                }
//            },
//                'whenClient'=>'function(attribute, value){
//                    //console.log(attribute.name);
//                    console.log(value);
//                    if(value==\'Tank\'){
//                        alert("sorry");
//                        false;
//                    }
//
//                }',
//                'message'=>'Endorsements must be selected.'],
        ];
    }

    /**
     * @inheritdoc
     */

    public function validateSubtesttype($attribute, $params){
            //echo $attribute;
            if(stristr($this->$attribute, 'Tank')){
                if(stristr($this->$attribute, 'School Bus') OR stristr($this->$attribute, 'Passenger')) {
                    $this->addError($attribute, 'Tank cannot be selected if School bus or passenger is selected.');
                }
            }


    }
    public function attributeLabels()
    {
        return [
            'customer_id' => 'Customer ID',
            'learners_permit_number' => 'Learner Permit Number',
            'first_name' => 'First Name',
            'full_name' => 'Full Name',
            'middle_name' => 'Middle Name',
            'last_name' => 'Last Name',
            'date_of_birth' => 'Date of Birth',
            'email' => 'Email',
            'email_repeat' => 'Re-type Email',
            'cell_phone' => 'Cell Phone',
            'test_type' => 'License Type',
            'sub_test_type' => 'Test Type',
            'confirmed' => 'Is confirmed?',
            'license_class' => 'License Class',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerReservedTimeslots()
    {
        return $this->hasMany(CustomerReservedTimeslots::className(), ['customer_id' => 'customer_id']);
    }
   
//app\models\Notification


}
