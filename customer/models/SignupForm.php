<?php
namespace app\models;

use app\models\User;
use yii\base\Model;
use Yii;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $password_repeat;
    public $user_group;
    public $first_name;
    public $last_name;
    public $customer_zip;
    public $customer_country;
    public $customer_address;
    public $customer_city;
    public $customer_region;
    public $business_id;
    public $profile_image;

    /**
     * @inheritdoc
     */
   // const ACTION_ADD = 1;
   // public function init()
  //  {
        //$this->on(Event::ACTION_ADD, ['app\models\Event', 'sendInLog']);

  //  }
    public function attributeLabels()
    {
        return [
            'username' => Yii::t('app', 'User name'),
            'email' => Yii::t('app', 'Email address'),
            'first_name' => Yii::t('app', 'First Name'),
            'password' => Yii::t('app', 'Password'),
            'password_repeat' => Yii::t('app', 'Repeat Password'),
            'user_group' => Yii::t('app', 'User Group'),

        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['username', 'filter', 'filter' => 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\app\models\User', 'message' => Yii::t('app','This username has already been taken.')],
            [['username'], 'string', 'min' => 4, 'max' => 20],

            ['email', 'filter', 'filter' => 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'unique', 'targetClass' => '\app\models\User', 'message' => Yii::t('app','This email address has already been taken.')],

            [['password','password_repeat'], 'required'],
            [['password','password_repeat'], 'string', 'min' => 4],
            [['password'], 'in', 'range'=>['password','Password','Password123','123456','12345678','letmein','monkey'] ,'not'=>true, 'message'=>Yii::t('app', 'You cannot use any really obvious passwords')],
            ['password_repeat', 'compare', 'compareAttribute'=>'password', 'message' => Yii::t("app", "The passwords must match")],
            [['first_name', 'last_name', 'email', 'customer_address','customer_city','customer_region', 'customer_country','customer_zip'], 'required'],
            [['profile_image'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],

        //    [['user_group'], 'string', 'max' => 20]
        ];
    }



    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function signup()
    {
        if ($this->validate()) {
          //  echo 'heeeeeeeeeeer';
            $user = new User();
            $user->username = $this->username;
            $user->email = $this->email;
            $user->first_name = $this->first_name;
            $user->last_name = $this->last_name;
            $user->customer_address = $this->customer_address;
            $user->customer_city = $this->customer_city;
            $user->customer_region = $this->customer_region;
            $user->customer_country = $this->customer_country;
            $user->customer_zip = $this->customer_zip;
            $user->profile_image = $this->profile_image;
            $user->business_id = $this->business_id;
         //   $user->user_group = $this->user_group;
            $user->setPassword($this->password);
            $user->generateAuthKey();
            //echo '<pre>';
          //  echo '<pre>';
          //  exit();
            $user->save(false);
            return $user;
        }else {
            echo 'not validate';

        }

      //  return null;
    }

    public static function sendInLog($event)
    {
        print_r($event->sender->attributes);

        /** @var \yii\base\Event $event */
        /** @var ActiveRecord $event->sender */
        $model = new Notification([
            //   'type' => $event->sender->getType(),
            'notification_type' =>1,
            'business_id' =>1,

        ]);
        $model->save(false);
    }

}
