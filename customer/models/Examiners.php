<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "examiners".
 *
 * @property integer $examiner_id
 * @property integer $location_id
 * @property string $examiner_name
 * @property string $examiner_email
 * @property string $examiner_phone
 * @property string $test_type
 */
class Examiners extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */

    public $cnt;
    public static function tableName()
    {
        return 'examiners';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['location_id', 'examiner_name', 'examiner_email', 'examiner_phone', 'test_type'], 'required'],
            [['location_id'], 'integer'],
            [['examiner_name', 'examiner_email', 'examiner_phone'], 'string', 'max' => 255],
            [['test_type'], 'string', 'max' => 6]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'examiner_id' => 'Examiner ID',
            'location_id' => 'Location ID',
            'examiner_name' => 'Examiner Name',
            'examiner_email' => 'Examiner Email',
            'examiner_phone' => 'Examiner Phone',
            'test_type' => 'Test Type',
        ];
    }
}
