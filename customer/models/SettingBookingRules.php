<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "setting_booking_rules".
 *
 * @property integer $bkrules_id
 * @property string $bkrules_name
 * @property string $bkrules_value
 * @property integer $business_id
 */
class SettingBookingRules extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'setting_booking_rules';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bkrules_name', 'bkrules_value', 'business_id'], 'required'],
            [['business_id'], 'integer'],
            [['bkrules_value'], 'string'],
            [['bkrules_name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bkrules_id' => 'Bkrules ID',
            'bkrules_name' => 'Bkrules Name',
            'bkrules_value' => 'Bkrules Value',
            'business_id' => 'Business ID',
        ];
    }
}
