<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "license_classes".
 *
 * @property integer $id
 * @property integer $parent_id
 * @property string $license_class_name
 */
class LicenseClasses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'license_classes';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_id', 'license_class_name'], 'required'],
            [['parent_id'], 'integer'],
            [['license_class_name'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent_id' => 'Parent ID',
            'license_class_name' => 'License Class Name',
        ];
    }
}
