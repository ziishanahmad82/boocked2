<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $displayname
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $role
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'customers';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
      //      [['username', 'auth_key', 'displayname', 'password_hash', 'email', 'created_at', 'updated_at'], 'required'],

            [['username', 'auth_key', 'password_hash', 'email', 'createdat', 'updated_at','first_name', 'last_name','customer_address','customer_city','customer_region', 'customer_country','customer_zip'], 'required'],
           // 'role', 'status',    [['first_name', 'middle_name', 'last_name','createdat'], 'string', 'max' => 100],
            [[ 'createdat', 'updated_at'], 'string', 'max' => 100],
            [['username', 'password_hash', 'password_reset_token', 'email'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
           // [['displayname'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'customer_id' => 'Customer ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'first_name'=>'First Name',
            'last_name'=>'Last Name',
       //     'displayname' => 'Displayname',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
         //   'role' => 'Role',
          //  'status' => 'Status',
         //   'createdat' => 'Created At',
         //   'updated_at' => 'Updated At',
        ];
    }
    public function getcustomername($id){
        $customer = User::findOne($id);
        if($customer->first_name!=""){
            return $customer->first_name.' '.$customer->last_name;
        }else {
           return $customer->username;
        }

    }
}
