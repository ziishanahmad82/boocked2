<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "sub_test_types".
 *
 * @property integer $id
 * @property integer $test_parent_id
 * @property string $test_name
 */
class SubTestTypes extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sub_test_types';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['test_parent_id', 'test_name'], 'required'],
            [['test_parent_id'], 'integer'],
            [['test_name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'test_parent_id' => 'Test Parent ID',
            'test_name' => 'Test Name',
        ];
    }
}
