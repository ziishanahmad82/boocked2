<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "toughbooks".
 *
 * @property integer $toughbook_id
 * @property string $toughbook_code
 * @property string $toughbook_status
 */
class Toughbooks extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'toughbooks';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['toughbook_code'], 'required'],
            [['toughbook_code'], 'string', 'max' => 255],
            [['toughbook_status'], 'string', 'max' => 15],
            [['toughbook_code'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'toughbook_id' => 'Toughbook ID',
            'toughbook_code' => 'Toughbook Code',
            'toughbook_status' => 'Toughbook Status',
        ];
    }
}
