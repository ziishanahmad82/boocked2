<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "slots".
 *
 * @property integer $id
 * @property string $calendar_type
 * @property integer $duration
 * @property string $slot_date
 * @property string $start_time
 * @property string $status
 * @property string $is_reserved
 * @property integer $reserved_by_customer_id
 */
class Slots extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $cnt=0;
    public static function tableName()
    {
        return 'slots';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['calendar_type', 'duration', 'slot_date', 'start_time'], 'required'],
            [['duration', 'reserved_by_customer_id'], 'integer'],
            [['slot_date', 'start_time'], 'safe'],
            [['calendar_type'], 'string', 'max' => 20],
            [['status'], 'string', 'max' => 15],
            [['is_reserved'], 'string', 'max' => 3]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'calendar_type' => 'Calendar Type',
            'duration' => 'Duration',
            'slot_date' => 'Slot Date',
            'start_time' => 'Start Time',
            'status' => 'Status',
            'is_reserved' => 'Is Reserved',
            'reserved_by_customer_id' => 'Reserved By Customer ID',
        ];
    }
}
