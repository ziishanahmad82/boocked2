<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "notification".
 *
 * @property integer $notification_id
 * @property integer $notification_type
 * @property integer $business_id
 * @property integer $notification_status
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notification_id', 'notification_type', 'business_id', 'notification_status'], 'required'],
            [['notification_id', 'notification_type', 'business_id', 'notification_status'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'notification_id' => 'Notification ID',
            'notification_type' => 'Notification Type',
            'business_id' => 'Business ID',
            'notification_status' => 'Notification Status',
        ];
    }
}
