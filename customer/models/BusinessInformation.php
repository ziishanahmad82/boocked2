<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "business_information".
 *
 * @property integer $business_id
 * @property string $business_name
 * @property string $business_description
 * @property string $address
 * @property string $country
 * @property string $state
 * @property string $city
 * @property string $zip
 * @property string $logo
 * @property string $business_phone
 * @property string $profession
 * @property integer $business_status
 */
class BusinessInformation extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'business_information';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['business_name', 'business_description', 'address', 'country', 'state', 'city', 'zip', 'business_phone', 'profession'], 'required'],
            [['business_description'], 'string'],
            [['business_status'], 'integer'],
            [['business_name', 'address', 'country', 'state', 'city', 'zip', 'logo', 'business_phone', 'profession'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'business_id' => 'Business ID',
            'business_name' => 'Business Name',
            'business_description' => 'Business Description',
            'address' => 'Address',
            'country' => 'Country',
            'state' => 'State',
            'city' => 'City',
            'zip' => 'Zip',
            'logo' => 'Logo',
            'business_phone' => 'Business Phone',
            'profession' => 'Profession',
            'business_status' => 'Business Status',
        ];
    }
}
