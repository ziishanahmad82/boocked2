<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "third_party_users".
 *
 * @property integer $id
 * @property string $user_name
 * @property string $password
 * @property string $email
 * @property string $phone
 * @property string $status
 */
class ThirdPartyUsers extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'third_party_users';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_name', 'password', 'email', 'phone', 'status'], 'required'],
            [['user_name', 'password', 'email', 'phone'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 8]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_name' => 'User Name',
            'password' => 'Password',
            'email' => 'Email',
            'phone' => 'Phone',
            'status' => 'Status',
        ];
    }
}
