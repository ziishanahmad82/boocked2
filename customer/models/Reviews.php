<?php

namespace app\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;

/**
 * This is the model class for table "reviews".
 *
 * @property integer $review_id
 * @property string $customer_id
 * @property string $service_id
 * @property string $review_text
 * @property string $appointment_id
 * @property string $staff_id
 * @property string $review_status
 * @property string $updated_at
 * @property string $created_at
 */
class Reviews extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'value' => new Expression('NOW()'),
            ],
        ];
    }
    public static function tableName()
    {
        return 'reviews';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id', 'service_id', 'review_text', 'appointment_id', 'staff_id', 'review_status', 'updated_at', 'created_at'], 'required'],
            [['review_text'], 'string'],
            [['customer_id', 'service_id', 'appointment_id', 'staff_id', 'review_status', 'updated_at', 'created_at'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'review_id' => 'Review ID',
            'customer_id' => 'Customer ID',
            'service_id' => 'Service ID',
            'review_text' => 'Review Text',
            'appointment_id' => 'Appointment ID',
            'staff_id' => 'Staff ID',
            'review_status' => 'Review Status',
            'updated_at' => 'Updated At',
            'created_at' => 'Created At',
        ];
    }
}
