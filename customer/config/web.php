<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'authClientCollection' => [
            'class' => 'yii\authclient\Collection',
            'clients' => [
                'facebook' => [
                    'class' => 'yii\authclient\clients\Facebook',
                    'authUrl' => 'https://www.facebook.com/dialog/oauth?display=popup',
                    'clientId' => '273750632995707',
                    'clientSecret' => '67516effcf0875ea35d7b78aee78b29e',
                ],
                'google' => [
                    'class' => 'yii\authclient\clients\GoogleOAuth',
                  //  'authUrl' => 'https://www.gmail.com/dialog/oauth?display=popup',
                   // 'clientId' => '610851043120-fommr6vka1kovt3urj5gh3465qiaknao.apps.googleusercontent.com',
                    'clientId'=>'610851043120-fommr6vka1kovt3urj5gh3465qiaknao.apps.googleusercontent.com',
                   // 'project_id'=>'boocked',

                    'authUrl'=>'https://accounts.google.com/o/oauth2/auth?display=popup',
                    'tokenUrl'=>'https://accounts.google.com/o/oauth2/token',
                 //   'auth_provider_x509_cert_url'=>'https://www.googleapis.com/oauth2/v1/certs',
                    'clientSecret'=>'ia76BQv0egoz0boxA8dElpA-'

                ],
            ],
        ],
        'session' => [
      //'cookieParams' => ['path'=>'/boocked/customer','httponly'=>1],
],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'jhgjhg&^$%#$%gfgh^%kjh',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => '_customer',
                'path'=>'/boocked/customer'
            ]

        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
//        'mailer' => [
//            'class' => 'yii\swiftmailer\Mailer',
//            'transport' => [
//                'class' => 'Swift_SmtpTransport',
//                'host' => 'aspmx.l.google.com',
//                'username' => 'azlaan.ahmad@gmail.com',
//                'password' => 'mmnnbb098',
//                'port' => '25',
//                //'encryption' => 'tls',
//            ],
//        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => ['enablePrettyUrl'=>true],
    ],

    'params' => $params,

];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
