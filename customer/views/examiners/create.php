<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Examiners */

$this->title = 'Create Examiners';
$this->params['breadcrumbs'][] = ['label' => 'Examiners', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="examiners-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
