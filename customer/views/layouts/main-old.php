<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\bootstrap\Modal;
use yii\helpers\Url;

AppAsset::register($this);
$session = Yii::$app->session;
$get = Yii::$app->request->get();
$route=Yii::$app->controller->route;



//if(@Yii::$app->user->identity->id AND Yii::$app->user->identity->user_group=='thirdparty')
//{
 //   $session["3rd_user_id"]=Yii::$app->user->identity->id;
 //   $session["3rd_user_name"]=Yii::$app->user->identity->username;
//}


?>
<?php $this->beginPage() ?>


<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>


</head>
<body>





<?
if($route=='site/index'){
    $session['customer_session']='true';
}

if(!$session["customer_session"]){
    ?>
    <script>
        var url="<?= Yii::getAlias("@web") ?>/index.php";
        
        //window.location=url;
    </script>
    <?
}
?>

<?php $this->beginBody() ?>



<header class="t_header">

    <div class="row">
        <div class="col-sm-3 pad">
            <div class="logo"><img src="<?= Yii::getAlias('@web') ?>/img/logo.png" class="img-responsive"></div>
        </div>
        <div class="col-sm-7"><h1 id="fittext1" class="heading2">Boocked</h1></div>
        <p><?= Html::a('Booking', ['site/index'], ['class' => 'btn btn-success', 'linkOptions' => ['data-method' => 'post']]) ?></p>
        <p>
            <?= Html::a('About', ['site/about'], ['class' => 'btn btn-success', 'linkOptions' => ['data-method' => 'post']]) ?>
            <? /*= Html::a('Logout ('.Yii::$app->user->identity->username.')', [Yii::getAlias('@web') . '/customer/web/index.php/site/logout'], ['class' => 'btn btn-success', 'data' => [
                        'method' => 'post',
                    ],]) */ ?>
        </p>
        <?
       //if(@Yii::$app->user->identity->customer_id) {
        if (!Yii::$app->user->isGuest) {
            //rint_r(Yii::$app->user);
           // exit();
        //    if(@Yii::$app->user->identity->user_group=='thirdparty') {
                ?>
                <p>

                    <? //= Html::a('Logout ('.Yii::$app->user->identity->username.')', [Yii::getAlias('@web')], ['class' => 'btn btn-success', 'data' => [

                      //  'method' => 'post',
                   // ],])  ?>
                    <a href="<?= Url::to(['site/logout']) ?>" data-method="post"><?= Yii::$app->user->identity->username ?></a>

                </p>

                <?
          //  Yii::getAlias('@web') . '/customer/web/index.php'
         //   } else {
                ?>

                <?
          //  }
        }else { ?>
        <?= Html::a('Login', ['site/login'], ['class' => 'btn btn-success', 'linkOptions' => ['data-method' => 'post']]) ?>
            <?php
        }
        ?>
    </div>

</header>





<div class="container">





    <?php
    foreach (Yii::$app->session->getAllFlashes() as $key => $message) {
        Modal::begin([
            'id' => 'modal_flash',
            'header' => '<h3 class="alert alert-' . $key . '">'.ucfirst($key).' : '.$message.'</h3>',
            'size' => 'modal-md',
        ]);

        Modal::end();
        $flasjJs= <<<EOF
        $('#modal_flash .modal-body').remove();
        $('#modal_flash').modal('show');
EOF;




        $this->registerJs($flasjJs);


    }
    ?>


    <?
    $customer_portal_status = \app\models\SiteSettings::find()->where(['id'=>'1'])->one()->value;
    if($customer_portal_status=='active'){
        echo $content;
    } else {
        ?>
        <div id="div_site_disabled">
            The site is currently undergoing maintenance.<br/>Please try later.
        </div>
    <?
    }

    ?>




</div>

<footer class="footer">


    <p>&copy; DMV <?= date('Y') ?></p>

</footer>








<?php $this->endBody() ?>



</body>
</html>
<?
if($route=="customer-reserved-timeslots/index" AND @$get['confirm']=='yes'){
    unset($session['customer_session']);
}
?>
<?php $this->endPage() ?>

