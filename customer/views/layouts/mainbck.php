<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\bootstrap\Modal;
use yii\helpers\Url;
use app\models\BusinessInformation;
use app\models\Timezoneandcurrency;
use app\models\SettingBookingRules;
use app\models\CurrencyList;

AppAsset::register($this);
$session = Yii::$app->session;
$get = Yii::$app->request->get();
$route=Yii::$app->controller->route;



//if(@Yii::$app->user->identity->id AND Yii::$app->user->identity->user_group=='thirdparty')
//{
 //   $session["3rd_user_id"]=Yii::$app->user->identity->id;
 //   $session["3rd_user_name"]=Yii::$app->user->identity->username;
//}


?>
<?php $this->beginPage() ?>


<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>


</head>
<body>
<?php $this->beginBody() ?>
<nav class="navbar">
    <div class="container-fluid">
        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle='collapse' data-target='#menu'>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <?php
            $path   = Yii::getAlias('@web');
            $admin_sidepath = substr($path, 0, strrpos($path, "customer"));
            $subdomain = str_replace('.boocked.com','',$_SERVER['HTTP_HOST']);
            $business_information  = BusinessInformation::find()->where(['business_subdomain'=>$subdomain])->one();
            if(empty($business_information)){
                return 'Business not available';
            }
            $business_id = $business_information->business_id;
            $business_information_logo = BusinessInformation::findOne($business_id);
            ?>
            <a href='#' class="navbar-brand">
               <?php if($business_information_logo->logo!=""){ ?>
                <img src='<?= $admin_sidepath ?>admin/web/<?= $business_information_logo->logo ?>' alt="<?= $business_information_logo->business_name ?>">
              <?php }else {
                   ?>
                    <h4><?= $business_information_logo->business_name ?></h4>
                   <?php
               } ?>
            </a>

        </div>
       <?php
        $timezone_currency  =  Timezoneandcurrency::findOne(['business_id'=>$business_id]);
        $settingrules =   SettingBookingRules::find()->where(['business_id'=>$business_id])->all();
        Yii::$app->session->set('bk_currency', CurrencyList::showcurrency_name($timezone_currency->currency));
        foreach($settingrules as $setting_rule){
        Yii::$app->session->set('bk_'.$setting_rule->bkrules_name, $setting_rule->bkrules_value);
        //  echo 'Name  :   '.$setting_rule->bkrules_name; echo 'Value  :   '.$setting_rule->bkrules_value.'<br/>';

        }
       ?>

        <div class="collapse navbar-collapse" id="menu">

            <ul class="nav navbar-nav navbar-right">
                <li <?php if($this->context->route == 'site/dashboard'){ echo 'class="active"'; } ?> ><a href="<?= Yii::getAlias('@web') ?>/index.php/site/dashboard"><i class="fa fa-question-circle icon-menu"></i> <span class='menu'>Dashboard</span></a></li>
                <li <?php if($this->context->route == 'site/about'){ echo 'class="active"'; } ?> ><a href="<?= Yii::getAlias('@web') ?>/index.php/site/about"><i class="fa fa-question-circle icon-menu"></i> <span class='menu'>About</span></a></li>
                <li <?php if($this->context->route == 'site/index'){ echo 'class="active"'; } ?> ><a href="<?= Yii::getAlias('@web') ?>/index.php/site/index"><i class="fa fa-gear icon-menu"></i> <span class='menu'>Services</span></a></li>
                <li <?php if($this->context->route == 'site/booking'){ echo 'class="active"'; } ?> ><a href="#"><i class="fa fa-tag icon-menu"></i> <span class='menu'>Book</span></a></li>
                <li <?php if($this->context->route == 'site/contact'){ echo 'class="active"'; } ?> ><a href="<?= Yii::getAlias('@web') ?>/index.php/site/contact"  ><i class="fa fa-mobile icon-menu"></i> <span class='menu'>Contact Us</span></a></li>
                <?php  if (!Yii::$app->user->isGuest) { ?>
                    <li>
                        <a href="<?= Url::to(['site/logout']) ?>" data-method="post" style="display: block; text-align: right; font-size: 14px; border-left: 1px solid #cecece;">
                            <i class="glyphicon glyphicon-log-out icon-menu" ></i>  <span class='menu'>  Logout </span>  <? //= Yii::$app->user->identity->username ?></a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </div>
</nav>





<?= $content ?>






<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <p class="pull-right">Powered by Boocked.com</p>
        </div>
    </div>
</div>
<?php $this->endBody() ?>

</body>
</html>





<!--<footer class="footer">


    <p>&copy; DMV <? //= date('Y') ?></p>

</footer>-->












</body>
</html>
<?
if($route=="customer-reserved-timeslots/index" AND @$get['confirm']=='yes'){
    unset($session['customer_session']);
}
?>
<?php $this->endPage() ?>

