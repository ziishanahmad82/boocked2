<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\bootstrap\Modal;
use app\models\Locations;
use app\models\Examiners;
use app\models\Customers;
use app\models\TimeSlotsConfiguration;
use app\models\CustomerReservedTimeslots;

if (@!$_REQUEST["suggested_date"]) {
    $date = substr($_REQUEST["date"], 0, -3);
    $selected_date = date('Y-m-d', $date);
    // $selected_date_heading=date('m-d-Y', $date);
} else {
    $date = $_REQUEST["date"];
    $selected_date = date('Y-m-d', strtotime(str_ireplace('-', '/', $date)));
    //$selected_date_heading=date('m-d-Y', $date);
}
$session = Yii::$app->session;
$customer_id = $session["customer_id"];
$test_type = Customers::find()->where(['customer_id' => $customer_id])->one()->test_type;

//$slot_config = TimeSlotsConfiguration::find()->where(['test_type' => $test_type])->one();
//
//$day_starts_time = $slot_config->day_starts_time;
//$day_ends_time = $slot_config->day_ends_time;
//$slot_duration = $slot_config->slot_period;
//
//// Create two new DateTime-objects...
//$date1 = new DateTime(date("Y-m-d") . 'T' . $slot_config->day_starts_time);
//$date2 = new DateTime(date("Y-m-d") . 'T' . $slot_config->day_ends_time);
//
//
//// The diff-methods returns a new DateInterval-object...
//$diff = $date2->diff($date1);
//
//// Call the format method on the DateInterval-object
//$time_per_day = $diff->format('%h');
////echo $time_per_day;

//$slots_per_day = ($time_per_day * 60) / $slot_config->slot_period;
//
//$slots_per_day = floor($slots_per_day);


if($test_type=='cdl'){
    $slots_per_day=4;
} else {
    $slots_per_day=11;
}

//echo $_SERVER['QUERY_STRING'];
//echo $date = date('d-m-Y', $date);

$ajax_path=Yii::getAlias('@web');
?>


<script>
    $(function() {
        $('#customerreservedtimeslots-location_id').on('change',function(){
            locationtimeslots($(this).val());
        })

        locationtimeslots($('#customerreservedtimeslots-location_id').val());
    });

    function locationtimeslots(location_id){
        $.ajax({
            url: "<?=$ajax_path?>/index.php/customer-reserved-timeslots/locationtimeslots/?selected_date=<?=$selected_date?>&slots_per_day=<?=$slots_per_day?>&test_type=<?=$test_type?>&location_id="+location_id,
            dataType: 'html',
            success: function (result) {
                $('#ajax_slots').html(result);
            }
        })
    }

</script>

<div class="customer-reserved-timeslots-form " >


    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <h2 id="modal-selected-date-h2">Selected Date : <?= date('m-d-Y', strtotime($selected_date)); ?></h2>
    </div>

    <?= $form->field($model, 'customer_id')->hiddenInput(['value' => $customer_id])->label(false) ?>


    <?= $form->field($model, 'location_id')->dropDownList(ArrayHelper::map(Locations::find()->where(['test_type' => $test_type])->all(), 'location_id', 'location_name')); ?>
    <div id="ajax_slots">
<!--        <div class="row">-->
<!--            Please select a time for test</label>-->
<!--        </div>-->
<!--        --><?php
//
//
//
//        $slots = TimeSlotsConfiguration::find()->where(['test_type' => $test_type])->all();
//        foreach($slots as $slot){
//            $startTime=$slot->slot_start_time;
//            $endTime=$slot->slot_end_time;
//            if (!@CustomerReservedTimeslots::find()->where(['test_date' => $selected_date, 'test_start_time' => $startTime])->one()->timeslot_id) {
//
//                ?>
<!--                <input type="radio" name="timeslot"-->
<!--                       onclick="$('#customerreservedtimeslots-test_start_time').val('--><?//= substr($startTime, 0, -3) ?>//');
//                           $('#customerreservedtimeslots-test_end_time').val('<?//= substr($endTime, 0, -3) ?>//');"
//                    > From <?//= substr($startTime, 0, -3) ?><!-- To --><?//= substr($endTime, 0, -3) ?><!--<br/>-->
<!--                --><?//
//            }
//        }
//
//
//        ?>
    </div>


    <?= $form->field($model, 'test_date')->hiddenInput(['value' => $selected_date])->label(false) ?>
    <?= $form->field($model, 'test_start_time')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'test_end_time')->hiddenInput()->label(false) ?>
    <?= $form->field($model, 'test_type')->hiddenInput(['value' => $test_type])->label(false) ?>
    <?= $form->field($model, 'examiner_id')->hiddenInput(['value' => 1])->label(false) ?>

    <div class="form-line-breaker"></div>
    <div>
        <?= Html::submitButton($model->isNewRecord ? 'Next' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success sitem12' : 'btn btn-primary sitem12']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
