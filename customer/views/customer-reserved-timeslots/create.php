<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Customers */

$this->title = 'Step 3 - Please select a date for test.';

?>
<div class="form">
    <header class="padoff pad">
        <div class="row">
            <div class="col-sm-6"><h2 class=""><?=$this->title;?></h2></div>
            <div class="col-sm-6">  <img src="<?=Yii::getAlias("@web")?>/img/step_3.png" class="img-responsive  margin" ></div>
        </div>
    </header>
    <div class="customers-create">


        <!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>

    </div>
</div>
