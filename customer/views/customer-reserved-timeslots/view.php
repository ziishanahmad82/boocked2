<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerReservedTimeslots */

$this->title = $model->timeslot_id;
$this->params['breadcrumbs'][] = ['label' => 'Customer Reserved Timeslots', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="customer-reserved-timeslots-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->timeslot_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->timeslot_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'timeslot_id:datetime',
            'customer_id',
            'location_id',
            'test_date',
            'test_start_time',
            'test_end_time',
        ],
    ]) ?>

</div>
