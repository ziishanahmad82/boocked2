<?php
use app\models\Customers;
use app\models\CustomerReservedTimeslots;
use app\models\Locations;
use app\models\Slots;
use yii\bootstrap\Modal;

ob_start();

$request = Yii::$app->request;
$get = $request->get();

$session = Yii::$app->session;
$customer_id = $session["customer_id"];

@$third_user_id=$session["3rd_user_id"];
@$session["3rd_user_group"]=Yii::$app->user->identity->user_group;
$get["customer_id"]=$session["customer_id"];

$slot_id = $session["slot_id"];
$new_slot_id = $session["new_slot_id"];
$get["id"]=$session["slot_id"];



if(@!$get["confirm"]) {
    $this->title = 'Step 4. Please confirm appointment details';
}
else {
    $this->title = 'Step 5. Appointment Confirmed';
    $delete_any_previous_confirmed_slot=CustomerReservedTimeslots::deleteAll("
    customer_id={$get["customer_id"]} AND
     timeslot_id != {$get["id"]}
    ");

    //change sattus of customer to 1
    $customersModel = Customers::findOne($get["customer_id"]);
    $customersModel->confirmed=1;
    $customersModel->email_repeat=$customersModel->email;

    $customersModel->save();


    //change slot confirmation to 1
    $customerReservedTimeslotsModel = CustomerReservedTimeslots::findOne($get["id"]);
    $customerReservedTimeslotsModel->confirmed=1;
    $customerReservedTimeslotsModel->confirmation_number='B'.($customerReservedTimeslotsModel->timeslot_id+10000);
    if(@$third_user_id){
        $customerReservedTimeslotsModel->third_user_id=$third_user_id;
    }
    $customerReservedTimeslotsModel->save();

    $slotsModel = Slots::findOne($new_slot_id);
    $slotsModel->is_reserved='yes';
    $slotsModel->status='Reserved';
    $slotsModel->reserved_by_customer_id=$get["customer_id"];
    $slotsModel->save();




}


$customersModel = Customers::findOne($get["customer_id"]);
$customersNewModel = new Customers();
$customerAttributes = $customersNewModel->attributeLabels();


$customerReservedTimeslotsModel = CustomerReservedTimeslots::findOne($get["id"]);
$customerReservedTimeslotsNewModel = new CustomerReservedTimeslots();
$customerReservedTimeslotsAttributes = $customerReservedTimeslotsNewModel->attributeLabels();




?>
<div class="container2" xmlns="http://www.w3.org/1999/html">

    <div class="form">
        <header class="padoff pad">
            <?if(@!$get["confirm"]) { ?>
                <div class="row">

                    <div class="col-sm-6"><h2 class="">Please confirm appointment details</h2></div>
                    <div class="col-sm-6"><img
                            src="<?= Yii::getAlias("@web") ?>/img/<? if (@!$get["confirm"]) { ?>step_4.png<? } else { ?>step_5.png<?
                            } ?>"
                            class="img-responsive  margin"></div>
                </div>
                <?
            }
            ?>
        </header>
        <form action="page4.html" method="post">
            <fieldset>


                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <div class="box1">
<!--                                <h2>Personal Details</h2>-->

                                <div class="col-xs-12">


                                    <ul>

                                        <?
                                        if($customerReservedTimeslotsModel->confirmed==1){
                                            ?>
                                            <li class="sitem22"><b><?= $customerReservedTimeslotsAttributes["confirmation_number"] ?></b>
                                                : <?= $customerReservedTimeslotsModel->confirmation_number; ?></li>
                                            <?
                                        }
                                        ?>
                                        <li><b><?= $customerAttributes["learners_permit_number"] ?></b>
                                            : <?= $customersModel->learners_permit_number; ?></li>
                                        <li><b><?= $customerAttributes["full_name"] ?></b>
                                            : <?= $customersModel->first_name; ?>&nbsp;<?= $customersModel->last_name; ?></li>


                                        <li><b><?= $customerAttributes["date_of_birth"] ?></b>
                                            : <?= date('m-d-Y', strtotime($customersModel->date_of_birth)); ?></li>
                                        <li><b><?= $customerAttributes["email"] ?></b> : <?= $customersModel->email; ?>
                                        </li>
                                        <li><b>Cell Phone</b>
                                            : <?= $customersModel->cell_phone; ?></li>
                                        <li><b><?= $customerAttributes["test_type"] ?></b>
                                            : <?
                                            if($customersModel->test_type=='NCDL'){
                                                echo "Standard";
                                            } else {
                                                echo $customersModel->test_type;
                                            }

                                             ?>
                                            &nbsp;&nbsp;
                                            <b><?
                                                if($customersModel->test_type=='CDL')
                                                    echo "CMV Class";
                                                else
                                                    echo "License Class";
                                                ?></b>
                                            : <?= $customersModel->license_class; ?>
                                            <?if($customersModel->test_type=='CDL'){
                                                echo "&nbsp";
                                                echo "<b>Test Type</b> : ";
                                                echo $customersModel->test_type2;
                                            }?>
                                            </li>

                                        <?
                                        if($customersModel->test_type!='NCDL') {
                                            ?>
                                            <li><b>Endorsement(s)</b>
                                                : <?= $customersModel->sub_test_type; ?></li>
                                            <?
                                        }
                                        ?>




                                    </ul>
                                </div>
                                <?
                                if (@!$get["confirm"]) {
                                    ?>
<!--                                    <div class="col-sm-18">-->
<!--                                        <a href="--><?//= Yii::getAlias("@web") ?><!--/index.php/customers/update?id=--><?//=$get["customer_id"]?><!--&test_type=--><?//=strtolower($customersModel->test_type)?><!--&slot_id=--><?//=$get["id"]?><!--"-->
<!--                                           class="button2">Change</a>-->
<!--                                    </div>-->
                                    <?
                                }
                                ?>
                            </div>
                        </div>


                        <div class="col-sm-6">

                            <div class="box1">
<!--                                <h2>Appointment Details</h2>-->

                                <div class="col-xs-12">

                                    <ul>
                                        <li><b><?= $customerReservedTimeslotsAttributes["test_date"] ?></b>
                                            : <?= date('m-d-Y', strtotime($customerReservedTimeslotsModel->test_date)); ?>
                                            &nbsp;&nbsp;
                                            <b>Time</b>
                                            : <?= date("g:i A", strtotime($customerReservedTimeslotsModel->test_start_time)); ?>
                                        </li>

<!--                                        <li><b>--><?//= $customerReservedTimeslotsAttributes["test_end_time"] ?><!--</b>-->
<!--                                            : --><?//= $customerReservedTimeslotsModel->test_end_time; ?><!--</li>-->
                                        <li><b><?= $customerReservedTimeslotsAttributes["location"] ?></b>
                                            : <?= Locations::findOne($customerReservedTimeslotsModel->location_id)->location_name ?>
<!--                                            change/remove later-->
<!--                                            <a href="#">-->
<!--                                                <div class="g-btn">Get Location</div>-->
<!--                                            </a>-->
                                        </li>

                                    </ul>
                                </div>






                            </div>
                        </div>




                        <div class="col-sm-6">


                        </div>


                        <div class="col-sm-6">






                                <?
                                if (@!$get["confirm"]) {

                                    if(!$third_user_id) {
                                        ?>
                            <div class="box1">

                                        <div class="row">

                                            <div class="col-sm-12">


                                                <label>How do you want to receive
                                                    reminder?</label>


                                            </div>
                                        </div>

                                        <div class="option">

                                            <div class="row">

                                                <input type="checkbox" name="notify_by_email" id="notify_by_email"
                                                       checked onclick="return false"/>
                                                Get notification via email
                                            </div>

                                            <div class="row">

                                                <input type="checkbox" name="notify_by_sms" id="notify_by_sms"/>
                                                Get notification via Text Message
                                            </div>


                                        </div>
                            </div>
                                        <?
                                    }
                                } else{
                                    ?>

                            <div class="box1">
                                    <div class="row">

                                        <div class="col-sm-12">
                                            <?
                                            $web=Yii::getAlias("@web");
                                            if(@$get['notify_email']=='yes'){
                                                echo "<img src=\"$web/img/icon_tick.png\">&nbsp;&nbsp;&nbsp;E-Mail sent<br/>";

                                            }
                                            if(@$get['notify_sms']=='yes'){
                                                echo "<img src=\"$web/img/icon_tick.png\">&nbsp;&nbsp;&nbsp;Text Message sent <br/>";
                                            }
                                            ?>
                                            <b>Your appointment is confirmed. You can close the browser now or <a href="http://dmv.dc.gov/">click here</a> to return to our main website.</b><br/>
                                            <img style="height: 36px;" src="<?= Yii::getAlias("@web") ?>/img/icon_print.png">&nbsp;&nbsp;&nbsp;<a class="sitem23" onclick="window.print();" style="cursor: pointer;">Click here to print</a><br/>
                                            <img style="height: 36px;" src="<?= Yii::getAlias("@web") ?>/img/icon_save.png">&nbsp;&nbsp;&nbsp;<a href="../../../confirmation.pdf" class="sitem23"  style="cursor: pointer;">Click here to save</a><br/>
                                        </div>
                                    </div>
                            </div>

                                    <?
                                }
                                ?>









                        </div>

                        <div class="form-line-breaker"></div>


                        <?
                        if (@!$get["confirm"]) {
                            ?>
                            <div class="sitem14">

                                <a href="<?= Yii::getAlias("@web") ?>/index.php/customer-reserved-timeslots/create?id=<?=$get["customer_id"]?>"
                                   class="btn btn-primary sitem16-b">Modify</a>

                                <a href="#"
                                   class="btn btn-primary sitem15" onclick="return is_notify_checked();">Book It!</a>

                            </div>
                            <?
                        } else {
                            ?>
                            <div class="sitem24">
                                Appointment Confirmed

                            </div>
                            <?
                        }
                        ?>






                    </div>
            </fieldset>
        </form>


    </div>
</div>


<?
Modal::begin([
    'header' => '<h1 id="modal-h1">Road Test Appointment Instructions</h1>',
    'id' => 'modal-preconfirm',
    'size' => 'modal-lg',
]);

if(@$session["3rd_user_group"]=='thirdparty-cdl') {
    echo "<div id='modalContent'>

<div id='modalContentinner'>


<h2>Road Test Instructions</h2>
Please read the following instructions.  You must accept the terms before continuing with scheduling an appointment.<br/>
To take the road test, you must: <br/>
<ul style='list-style-type:decimal;'>
    <li>Arrive at least 15 minutes before your scheduled appointment time</li>
    <li>Bring your valid learner permit</li>
    <li>Be accompanied by licensed driver 21 years or older</li>
    <li>Arrive wearing your seatbelt</li>
    <li>Bring your completed 40 hours daytime driving certification form, if applicable (under 21 years of age) </li>
    <li>Bring $10 road test fee (check, money order or credit/debit card - MasterCard/Visa)
        <span style='color:red;font-weight:bold;' >NO CASH ACCEPTED</span> at Road Test Location</li>

    <li>Bring $47 Driver License fee or $20 Provisional Driver License (under 21 years of age) fee
    (check, money order or credit/debit card - MasterCard/Visa)  <span style='color:red;font-weight:bold;'>NO CASH ACCEPTED</span> at Road Test Location</li>
</ul>

To take the road test, your vehicle must:<br/>
<ul  style='list-style-type:decimal;'>
    <li>Bring current registration and have window registration sticker (if applicable) affixed to windshield or current sticker displaying valid expiration date affixed to plates (if applicable)</li>
    <li>Bring valid motor vehicle liability insurance card or policy, identifying the vehicle and policy expiration date (insurance policies/coverage on Cellphones/Laptops are NOT allowed for road test). Printed copy required</li>
    <li>Display valid inspection sticker, if applicable (failed inspection stickers not acceptable)</li>
    <li>Display front, if applicable; and rear plate properly affixed to bumper</li>
    <li>Have properly functioning brake lights, turn signal lights, horn, windows that roll up and down, operable doors with door handles (inside and outside), inside rear view mirror and outside (left/right) side view mirrors properly placed, and <span style='color: red'>hand emergency brake located between driver and front passenger seats</span></li>
    <li>Have windshield with no cracks or debris, and nothing hanging from the rearview mirror</li>
    <li>Have tires in good condition and properly inflated (spare/donut tires not acceptable) </li>
    <li>Have NO service or warning lights illuminated on dashboard, including low gas</li>
</ul>

Rental vehicles are allowed for road tests only if person taking the test is listed on rental contract as an approved driver of the rental vehicle. If you are unable to keep your scheduled appointment, you must cancel your appointment at least two (2) business days prior to date of your scheduled appointment. A $30 no cancellation fee will be charged to customers not complying with cancellation policy. Your road test may be canceled if you arrive late for your appointment or bring a vehicle that does not meet the vehicle testing requirements listed above. <br/>

DC DMV does NOT have vehicles to rent or use for road test examinations.  <br/><br/>

If you are unable to keep your scheduled appointment, you must cancel or reschedule your appointment at least two (2) business days prior to date of your scheduled appointment.  A $30 no cancellation fee will be charged to customers not complying with cancellation policy AND will result in a <span style='color:red;font-weight:bold;'>FAILED TEST</span>. Additionally, if you arrive for your appointment late or you arrive timely without a vehicle for testing, this will be considered not complying with the cancellation policy and will result in the $30 no cancellation fee and count as a <span style='color:red;font-weight:bold;'>FAILED TEST</span>.
</div>
<div class='sitem19'>
    <a href=\"#\" class=\"btn btn-primary sitem20\" onclick=\"return disagreed();\">Disagree</a>
    <a href=\"#\" class=\"btn btn-primary sitem21\" onclick=\"return agreed();\">Agree</a>

</div>

</div>";

} else if(@$session["3rd_user_group"]=='thirdparty-ncdl') {
    echo "<div id='modalContent'>

<div id='modalContentinner'>

Please read the following instructions.  You must accept the terms at the bottom of this message before continuing with scheduling an appointment.<br/>
To take the CDL road test, you must:<br/>
<ul  style='list-style-type:decimal;'>
    <li>Arrive at least 15 minutes before your scheduled appointment time</li>
    <li>Bring your valid DC driver license or Commercial driver license AND Commercial learner permit</li>
    <li>Bring your valid, certified DOT Medical Card</li>
    <li>Be accompanied by licensed CDL driver of the same license class or higher; the driver must present their CDL at time of check-in at the CDL Road Test Lot</li>
    <li>Have the proper vehicle for testing. Appointment will be canceled, if vehicle is not on the testing lot at the scheduled appointment time. </li>
    <li>Bring $10 road test fee (check, money order or credit/debit card - MasterCard/Visa) <span style='color:red;font-weight:bold;'>NO CASH ACCEPTED</span> at Road Test Location</li>
</ul>

To take the CDL road test, your commercial vehicle must:

<ul  style='list-style-type:decimal;'>
    <li>Have NO cargo or load in commercial vehicle/trailer used for road test</li>
    <li>Have a valid registration card, stickers and tags properly affixed to vehicle</li>
    <li>Have valid proof of insurance </li>
    <li>Have properly functioning brake lights, turn signal lights, horn, windows that roll-up and down, operable doors with door handles (inside and outside), mirrors (inside and outside) properly placed</li>
    <li>Have tires in good condition and properly inflated</li>
    <li>Bring wheel chalks to secure vehicle during the road test examination</li>
    <li>Have no service or warning lights illuminated on dashboard, including low gas</li>
</ul>

If you are unable to keep your scheduled appointment, you must cancel or reschedule your appointment at least two (2) business days prior to date of your scheduled appointment.  A $30 no cancellation fee will be charged to customers not complying with cancellation policy AND will result in a <span style='color:red;font-weight:bold;'>FAILED TEST</span>. Additionally, if you arrive for your appointment late or you arrive timely without a vehicle for testing, this will be considered not complying with the cancellation policy and will result in the $30 no cancellation fee and count as a <span style='color:red;font-weight:bold;'>FAILED TEST</span>.
</div>
<div class='sitem19'>
    <a href=\"#\" class=\"btn btn-primary sitem20\" onclick=\"return disagreed();\">Disagree</a>
    <a href=\"#\" class=\"btn btn-primary sitem21\" onclick=\"return agreed();\">Agree</a>

</div>

</div>";
}



if($customersModel->test_type!="CDL" AND !$third_user_id) {
    echo "<div id='modalContent'>

<div id='modalContentinner'>

Please read the following instructions.  You must accept the terms at the bottom of this message before continuing with scheduling an appointment.<br/>
To take the road test, you must: <br/>
<ul style='list-style-type:decimal;'>
    <li>Arrive at least 15 minutes before your scheduled appointment time</li>
    <li>Bring your valid learner permit</li>
    <li>Be accompanied by licensed driver 21 years or older</li>
    <li>Arrive wearing your seatbelt</li>
    <li>Bring your completed 40 hours daytime driving certification form, if applicable (under 21 years of age) </li>
    <li>Bring $10 road test fee (check, money order or credit/debit card - MasterCard/Visa)
        <span style='color:red;font-weight:bold;' >NO CASH ACCEPTED</span> at Road Test Location</li>

    <li>Bring $47 Driver License fee or $20 Provisional Driver License (under 21 years of age) fee
    (check, money order or credit/debit card - MasterCard/Visa)  <span style='color:red;font-weight:bold;'>NO CASH ACCEPTED</span> at Road Test Location</li>
</ul>

To take the road test, your vehicle must:<br/>
<ul  style='list-style-type:decimal;'>
    <li>Bring current registration and have window registration sticker (if applicable) affixed to windshield or current sticker displaying valid expiration date affixed to plates (if applicable)</li>
    <li>Bring valid motor vehicle liability insurance card or policy, identifying the vehicle and policy expiration date (insurance policies/coverage on Cellphones/Laptops are NOT allowed for road test). Printed copy required</li>
    <li>Display valid inspection sticker, if applicable (failed inspection stickers not acceptable)</li>
    <li>Display front, if applicable; and rear plate properly affixed to bumper</li>
    <li>Have properly functioning brake lights, turn signal lights, horn, windows that roll up and down, operable doors with door handles (inside and outside), inside rear view mirror and outside (left/right) side view mirrors properly placed, and <span style='color: red'>hand emergency brake located between driver and front passenger seats</span></li>
    <li>Have windshield with no cracks or debris, and nothing hanging from the rearview mirror</li>
    <li>Have tires in good condition and properly inflated (spare/donut tires not acceptable) </li>
    <li><span style='color: red'>Have NO service or warning lights illuminated on dashboard, including low gas</span></li>
</ul>

Rental vehicles are allowed for road tests only if person taking the test is listed on rental contract as an approved driver of the rental vehicle. If you are unable to keep your scheduled appointment, you must cancel your appointment at least two (2) business days prior to date of your scheduled appointment. A $30 no cancellation fee will be charged to customers not complying with cancellation policy. Your road test may be canceled if you arrive late for your appointment or bring a vehicle that does not meet the vehicle testing requirements listed above. <br/>

DC DMV does NOT have vehicles to rent or use for road test examinations.  <br/><br/>


</div>
<div class='sitem19'>
    <a href=\"#\" class=\"btn btn-primary sitem20\" onclick=\"return disagreed();\">Disagree</a>
    <a href=\"#\" class=\"btn btn-primary sitem21\" onclick=\"return agreed();\">Agree</a>

</div>

</div>";

} else if($customersModel->test_type=="CDL" AND !$third_user_id) {
    echo "<div id='modalContent'>

<div id='modalContentinner'>




<h2>Road Test Instructions</h2>
Please read the following instructions.  You must accept the terms before continuing with scheduling an appointment.<br/>
To take the CDL road test, you must:<br/>
<ul  style='list-style-type:decimal;'>
    <li>Arrive at least 15 minutes before your scheduled appointment time</li>
    <li>Bring your valid DC driver license or Commercial driver license AND Commercial learner permit</li>
    <li>Bring your valid, certified DOT Medical Card</li>
    <li>Be accompanied by licensed CDL driver of the same license class or higher; the driver must present their CDL at time of check-in at the CDL Road Test Lot</li>
    <li>Have the proper vehicle for testing. Appointment will be canceled, if vehicle is not on the testing lot at the scheduled appointment time. </li>
    <li>Bring $10 road test fee (check, money order or credit/debit card - MasterCard/Visa) <span style='color:red;font-weight:bold;'>NO CASH ACCEPTED</span> at Road Test Location</li>
</ul>

To take the CDL road test, your commercial vehicle must:

<ul  style='list-style-type:decimal;'>
    <li>Have NO cargo or load in commercial vehicle/trailer used for road test</li>
    <li>Have a valid registration card, stickers and tags properly affixed to vehicle</li>
    <li>Have valid proof of insurance </li>
    <li>Have properly functioning brake lights, turn signal lights, horn, windows that roll-up and down, operable doors with door handles (inside and outside), mirrors (inside and outside) properly placed</li>
    <li>Have tires in good condition and properly inflated</li>
    <li>Bring wheel chalks to secure vehicle during the road test examination</li>
    <li>Have no service or warning lights illuminated on dashboard, including low gas</li>
</ul>

If you are unable to keep your scheduled appointment, you must cancel or reschedule your appointment at least two (2) business days prior to date of your scheduled appointment.  A $30 no cancellation fee will be charged to customers not complying with cancellation policy AND will result in a <span style='color:red;font-weight:bold;'>FAILED TEST</span>. Additionally, if you arrive for your appointment late or you arrive timely without a vehicle for testing, this will be considered not complying with the cancellation policy and will result in the $30 no cancellation fee and count as a <span style='color:red;font-weight:bold;'>FAILED TEST</span>.
</div>
<div class='sitem19'>
    <a href=\"#\" class=\"btn btn-primary sitem20\" onclick=\"return disagreed();\">Disagree</a>
    <a href=\"#\" class=\"btn btn-primary sitem21\" onclick=\"return agreed();\">Agree</a>

</div>

</div>";
}



Modal::end();

echo $email=ob_get_clean();


if(@$get['notify_email']=='yes'){
    if(Yii::$app->mailer->compose()
        ->setFrom('notify@dmv.com')
        ->setTo('ziishanahmad@gmail.com')
        ->setSubject('Your appointment details')
        ->setHtmlBody($email)
        ->send()){
       // echo "<img src=\"$web/img/icon_tick.png\">&nbsp;&nbsp;&nbsp;E-Mail sent<br/>";
    } else {
        echo "E-mail server error";
    }


}
?>

<script>
    function is_notify_checked(){
        $('#modal-preconfirm').modal('show');
        return false;
    }
    function agreed(){
        var url="<?= Yii::getAlias("@web") ?>/index.php/customer-reserved-timeslots/index?id=<?= $get["id"] ?>&customer_id=<?= $get["customer_id"] ?>&confirm=yes";
        if($("#notify_by_email").is(":checked")){
            url+="&notify_email=yes";
        }
        if($("#notify_by_sms").is(":checked")){
            url+="&notify_sms=yes";
        }
        window.location=url;
        return false;
    }

    function disagreed(){
        var url="<?= Yii::getAlias("@web") ?>/index.php?id=<?= $get["id"] ?>&customer_id=<?= $get["customer_id"] ?>&agreed=no";
        if($("#notify_by_email").is(":checked")){
            url+="&notify_email=yes";
        }
        if($("#notify_by_sms").is(":checked")){
            url+="&notify_sms=yes";
        }
        window.location=url;
        return false;
    }
</script>