<style>
    .fc-week {
        height: 50px !important;
    }

    .fc-other-month .fc-day-number { display:none;}

</style>
<?php

use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
use yii\web\JsExpression;
use yii\bootstrap\Modal;

use app\models\Locations;
use app\models\Customers;
use app\models\TimeSlotsConfiguration;
use app\models\CustomerReservedTimeslots;
use app\models\Examiners;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerReservedTimeslots */
/* @var $form yii\widgets\ActiveForm */


$session = Yii::$app->session;
$customer_id = $session["customer_id"];


$test_type = Customers::find()->where(['customer_id' => $customer_id])->one()->test_type;

$slot_config = TimeSlotsConfiguration::find()->where(['test_type' => $test_type])->all();

//$day_starts_time = $slot_config->day_starts_time;
//$day_ends_time = $slot_config->day_ends_time;
//$slot_duration = $slot_config->slot_period;
//
//// Create two new DateTime-objects...
//$date1 = new DateTime(date("Y-m-d") . 'T' . $slot_config->day_starts_time);
//$date2 = new DateTime(date("Y-m-d") . 'T' . $slot_config->day_ends_time);
//
//
//// The diff-methods returns a new DateInterval-object...
//$diff = $date2->diff($date1);
//
//// Call the format method on the DateInterval-object
//$time_per_day = $diff->format('%h');
////echo $time_per_day;
//
//$slots_per_day = ($time_per_day * 60) / $slot_config->slot_period;
//
//$slots_per_day = floor($slots_per_day);

if($test_type=='cdl'){
    $slots_per_day=4;
} else {
    $slots_per_day=11;
}

$examinerCount = Examiners::find()
    ->select(['COUNT(examiner_id) AS cnt'])
    ->where("test_type = '{$test_type}'")
    ->one();


$total_slots_per_day = $slots_per_day * $examinerCount->cnt;


$DragJS = <<<EOF
/* initialize the external events
-----------------------------------------------------------------*/

$('#external-events .fc-event').each(function() {
    // store data so the calendar knows to render an event upon drop
    $(this).data('event', {
        title: $.trim($(this).text()), // use the element's text as the event title
        stick: true // maintain when user navigates (see docs on the renderEvent method)
    });
    // make the event draggable using jQuery UI
    $(this).draggable({
        zIndex: 999,
        revert: true,      // will cause the event to go back to its
        revertDuration: 0  //  original position after the drag
    });
});

EOF;

$this->registerJs($DragJS);


$JSCode = <<<EOF
function(start, end) {

var check = start._d.toJSON().slice(0,10);
var today = new Date().toJSON().slice(0,10);

alert($(this).html());

if(check >= today)
    {
        $('#modal-form2').modal('show').find('#modalContent').load($('#modalButton').attr('value')+'?date='+start+'&customer_id='+$customer_id);
    }
    else
    {
       // alert("You cannot select from past dates.");
    }



}
EOF;

$JSDropEvent = <<<EOF
function(date) {
    alert("Dropped on " + date.format());
    if ($('#drop-remove').is(':checked')) {
        // if so, remove the element from the "Draggable Events" list
        $(this).remove();
    }
}
EOF;

$ajax_path=Yii::getAlias('@web');

$JSDayRender = <<<EOF
function (date, cell) {

    var check = date._d.toJSON().slice(0,10);
    var today = new Date().toJSON().slice(0,10);



    $.ajax({
    url: "$ajax_path/index.php/customer-reserved-timeslots/cellcolor/?date="+check+"&total_slots_per_day=$total_slots_per_day&test_type=$test_type",
    dataType: 'json',
    success: function(result){

        if(check >= today){

            //cell.css("background-color", "#228B22");
            cell.css("background-color", result['bgcolor']);
            cell.css("color", "black");
            cell.css("font-weight", "bold");
            cell.css("font-size", "20px");
            cell.html(result['day_status']);


        } else if(check < today){
            cell.css("background-color", "lightgray");
            cell.html("<span style='display:none;'>past</span>");
        }
//        if(check == today){
//            cell.css("background-color", "#228B22");
//
//        }





        $.each(result.all_holidays, function(i, item) {



             if(check == result.all_holidays[i].holiday_date.trim()){
                cell.css("background-color", "#cc0000");

                //cell.html(result.all_holidays[i].holiday_name+"<span style='display:none;'>holiday</span>");
                cell.html("Unavailable<span style='display:none;'>holiday</span>");


             }

        })



    }});




}
EOF;

$JSEventClick = <<<EOF
function(date, allDay, jsEvent, view) {


    var check = date._d.toJSON().slice(0,10);
    var today = new Date().toJSON().slice(0,10);





if($(this).html()!="Full" && $(this).html().search("holiday")<1 && $(this).css('visibility')!="hidden"){


        if(check >= today)
        {
            $('#modal-form2').modal('show').find('#modalContent').load($('#modalButton').attr('value')+'?date='+date+'&customer_id='+$customer_id);
        }
        else
        {
            //alert("You cannot select from past dates.");
        }

    } else if($(this).html().search("holiday")>1){
//        alert('This date is not available. Please select another date.');
    } else if($(this).html()=="Full"){
//        alert("This date is fully reserved. Please select another date.");
    }
}

EOF;


$JSEventRender = <<<EOF
function(event, element, view)
{
    if(event.start.getMonth() !== view.start.getMonth()) { return false; }
}

EOF;





$JSviewRender = <<<EOF
function(view) {



      var now = new Date();
      var end = new Date();
      end.setMonth(now.getMonth() + 2); //Adjust as needed

      var cal_date_string = view.start._d.getMonth()+1+'/'+view.start._d.getFullYear();

      var cal_end_date_string = view.start._d.getMonth()+'/'+view.start._d.getFullYear();

      var cur_date_string = now.getMonth()+'/'+now.getFullYear();
      var end_date_string = end.getMonth()+'/'+end.getFullYear();




      if(cal_date_string == cur_date_string) {


        $('.fc-prev-button').addClass("fc-state-disabled");

      }
      else {
        $('.fc-prev-button').removeClass("fc-state-disabled");
      }


      if(end_date_string == cal_end_date_string) {

        $('.fc-next-button').addClass("fc-state-disabled");
      }
      else {
        $('.fc-next-button').removeClass("fc-state-disabled");
      }
}


EOF;








?>

<script>
    function suggested_onlick(){

        $('#modal-form2').modal('show').find('#modalContent').load($('#modalButton').attr('value')+'?date=09-15-2015&suggested_date=yes&customer_id='+<?=$customer_id?>);

    }
</script>


<div class="customer-reserved-timeslots-form inner-form" >


    <?php $form = ActiveForm::begin(); ?>


<!--    <div class="form-group field-customerreservedtimeslots-8:00 Am to 9:00 AM At Location 1 ">-->
<!--        <label class="control-label" for="customerreservedtimeslots-suggested_time_slot">Suggested date : </label>-->
<!--        09-15-2015 &nbsp;&nbsp;&nbsp;<button type="button" onclick="suggested_onlick();" class="btn btn-success">Reserve</button>-->
<!--    </div>-->


    <!--    --><? //= $form->field($model, 'customer_id')->textInput() ?>
    <!---->
    <!--    --><? //= $form->field($model, 'location_id')->textInput() ?>
    <!---->
    <!--    --><? //= $form->field($model, 'test_date')->textInput() ?>
    <!---->
    <!--    --><? //= $form->field($model, 'test_start_time')->textInput() ?>
    <!---->
    <!--    --><? //= $form->field($model, 'test_end_time')->textInput() ?>




    <?php ActiveForm::end(); ?>

</div>


<input type="hidden" id="modalButton" value="<?= url::to('form2') ?>">


<?php

$events = array();
//Testing
$Event = new \yii2fullcalendar\models\Event();
$Event->id = 1;
$Event->title = 'Available';
$Event->start = date('Y-m-d\Th:m:s\Z');
$events[] = $Event;

$Event = new \yii2fullcalendar\models\Event();
$Event->id = 2;
$Event->title = 'Available';
$Event->start = date('Y-m-d\Th:m:s\Z', strtotime('tomorrow 6am'));
$events[] = $Event;

?>

<?= yii2fullcalendar\yii2fullcalendar::widget(array(
    'header' => [
        'left' => 'prev,next today',
        'center' => 'title',
        'right' => ''
    ],

    'clientOptions' => [
        'selectable' => true,
        'selectHelper' => true,
        'droppable' => true,
        'editable' => true,
        'drop' => new JsExpression($JSDropEvent),
        //'select' => new JsExpression($JSCode),
        'dayRender' => new JsExpression($JSDayRender),
        'dayClick' => new JsExpression($JSEventClick),
        'viewRender' => new JsExpression($JSviewRender),
        'defaultDate' => date('Y-m-d'),
        'weekends' => false,
        'slotDuration' => '00:30:00',
        'eventRender' => new JsExpression($JSEventRender),
    ],
    //'ajaxEvents' => Url::toRoute(['/customer-reserved-timeslots/jsoncalendar'])
));


Modal::begin([
    'header' => '<h1 id="modal-h1">Please select appointment time</h1>',
    'id' => 'modal-form2',
    'size' => 'modal-md',
]);
echo "<div id='modalContent'></div>";
Modal::end();
?>

<div id="color-legend">

    <div id="sitem8"></div> <span style="float: left">&nbsp; Unavailable&nbsp;</span>
    <div id="sitem9"></div> <span style="float: left">&nbsp; Available&nbsp;</span>
    <div id="sitem10"></div> <span style="float: left">&nbsp; Past Dates&nbsp;</span>

</div>


