<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerReservedTimeslots */

$this->title = 'Update Customer Reserved Timeslots: ' . ' ' . $model->timeslot_id;
$this->params['breadcrumbs'][] = ['label' => 'Customer Reserved Timeslots', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->timeslot_id, 'url' => ['view', 'id' => $model->timeslot_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="customer-reserved-timeslots-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
