<?php
use app\models\Customers;
use app\models\CustomerReservedTimeslots;
use app\models\Locations;


$request = Yii::$app->request;
$get = $request->get();

$session = Yii::$app->session;
$customer_id = $session["customer_id"];
$get["customer_id"]=$session["customer_id"];
$slot_id = $session["slot_id"];
$get["id"]=$session["slot_id"];
@$third_user_id=$session["3rd_user_id"];



if(@!$get["confirm"]) {
    $this->title = 'Edit your details';
}
else {
    $this->title = 'Reserveration Confirmed';
    $delete_any_previous_confirmed_slot=CustomerReservedTimeslots::deleteAll("
    customer_id={$get["customer_id"]} AND
     timeslot_id != {$get["id"]}
    ");

    //change sattus of customer to 1
    $customersModel = Customers::findOne($get["customer_id"]);
    $customersModel->confirmed=1;
    $customersModel->email_repeat=$customersModel->email;
    $customersModel->save();


    //change slot confirmation to 1
    $customerReservedTimeslotsModel = CustomerReservedTimeslots::findOne($get["id"]);
    $customerReservedTimeslotsModel->confirmed=1;
    $customerReservedTimeslotsModel->save();
}


$customersModel = Customers::findOne($get["customer_id"]);
$customersNewModel = new Customers();
$customerAttributes = $customersNewModel->attributeLabels();


$customerReservedTimeslotsModel = CustomerReservedTimeslots::findOne($get["id"]);
$customerReservedTimeslotsNewModel = new CustomerReservedTimeslots();
$customerReservedTimeslotsAttributes = $customerReservedTimeslotsNewModel->attributeLabels();




?>
<div class="container2" xmlns="http://www.w3.org/1999/html">

    <div class="form">
        <header class="padoff pad">
            <div class="row">
                <div class="col-sm-6"><h2 class=""><?if(@!$get["confirm"]){?>Confirm your details<?} else {?>Reserveration Confirmed<?}?></h2></div>
                <div class="col-sm-6"><img src="<?= Yii::getAlias("@web") ?>/img/<?if(@!$get["confirm"]){?>step_4.png<?} else {?>step_5.png<?}?>"
                                           class="img-responsive  margin"></div>
            </div>
        </header>
        <form action="page4.html" method="post">
            <fieldset>


                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-6">
                            <div class="box1">
                                <h2>Personal Details</h2>

                                <div class="col-xs-12">


                                    <ul>
                                        <li><b><?= $customerAttributes["learners_permit_number"] ?></b>
                                            : <?= $customersModel->learners_permit_number; ?></li>
                                        <li><b><?= $customerAttributes["full_name"] ?></b>
                                            : <?= $customersModel->first_name; ?>&nbsp;<?= $customersModel->last_name; ?></li>


                                        <li><b><?= $customerAttributes["date_of_birth"] ?></b>
                                            : <?= date('m-d-Y', strtotime($customersModel->date_of_birth)); ?></li>
                                        <li><b><?= $customerAttributes["email"] ?></b> : <?= $customersModel->email; ?>
                                        </li>
                                        <li><b>Cell Phone</b>
                                            : <?= $customersModel->cell_phone; ?></li>
                                        <li><b><?= $customerAttributes["test_type"] ?></b>
                                            : <?
                                            if($customersModel->test_type=='NCDL'){
                                                echo "Standard";
                                            } else {
                                                echo $customersModel->test_type;
                                            }

                                            ?>
                                            &nbsp;&nbsp;
                                            <b><?
                                                if($customersModel->test_type=='CDL')
                                                    echo "CMV Class";
                                                else
                                                    echo "License Class";
                                                ?></b>
                                            : <?= $customersModel->license_class; ?>
                                            <?if($customersModel->test_type=='CDL'){
                                                echo "&nbsp";
                                                echo "<b>Test Type</b> : ";
                                                echo $customersModel->test_type2;
                                            }?>
                                        </li>

                                        <?
                                        if($customersModel->test_type!='NCDL') {
                                            ?>
                                            <li><b>Endorsement(s)</b>
                                                : <?= $customersModel->sub_test_type; ?></li>
                                            <?
                                        }
                                        ?>




                                    </ul>
                                </div>
                                <?
//                                if (@!$get["confirm"]) {
//                                    ?>
<!--                                    <div class="col-sm-18">-->
<!--                                        <a href="--><?//= Yii::getAlias("@web") ?><!--/index.php/customers/update2?id=--><?//=$get["customer_id"]?><!--&test_type=--><?//=strtolower($customersModel->test_type)?><!--&slot_id=--><?//=$get["id"]?><!--"-->
<!--                                           class="button2">Change</a>-->
<!--                                    </div>-->
<!--                                    --><?//
//                                }
                                ?>
                            </div>
                        </div>


                        <div class="col-sm-6">

                            <div class="box1">
                                <h2>Appointment Details</h2>

                                <div class="col-xs-12">

                                    <ul>
                                        <li><b><?= $customerReservedTimeslotsAttributes["test_date"] ?></b>
                                            : <?= date('m-d-Y', strtotime($customerReservedTimeslotsModel->test_date)); ?>
                                            &nbsp;&nbsp;
                                            <b>Time</b>
                                            : <?= date("g:i A", strtotime($customerReservedTimeslotsModel->test_start_time)); ?>
                                        </li>

                                        <!--                                        <li><b>--><?//= $customerReservedTimeslotsAttributes["test_end_time"] ?><!--</b>-->
                                        <!--                                            : --><?//= $customerReservedTimeslotsModel->test_end_time; ?><!--</li>-->
                                        <li><b><?= $customerReservedTimeslotsAttributes["location"] ?></b>
                                            : <?= Locations::findOne($customerReservedTimeslotsModel->location_id)->location_name ?>
                                            <!--                                            change/remove later-->
                                            <!--                                            <a href="#">-->
                                            <!--                                                <div class="g-btn">Get Location</div>-->
                                            <!--                                            </a>-->
                                        </li>

                                    </ul>
                                </div>




                            </div>
                        </div>

                        <div class="col-sm-6">

                            <?
                            if (@!$get["confirm"]) {

                                if(!$third_user_id) {
                                    ?>
                                    <div class="box1">

                                        <div class="row">

                                            <div class="col-sm-12">


                                                <label>How do you want to receive
                                                    reminder?</label>


                                            </div>
                                        </div>

                                        <div class="option">

                                            <div class="row">

                                                <input type="checkbox" name="notify_by_email" id="notify_by_email"
                                                       checked onclick="return false"/>
                                                Get notification via email.
                                            </div>

                                            <div class="row">

                                                <input type="checkbox" name="notify_by_sms" id="notify_by_sms"/>
                                                Get notification via Text Message.
                                            </div>


                                        </div>
                                    </div>
                                    <?
                                }
                            } else{
                                ?>

                                <div class="box1">
                                    <div class="row">

                                        <div class="col-sm-12">
                                            <?
                                            $web=Yii::getAlias("@web");
                                            if(@$get['notify_email']=='yes'){
                                                echo "<img src=\"$web/img/icon_tick.png\">&nbsp;&nbsp;&nbsp;E-Mail sent <br/>";
                                            }
                                            if(@$get['notify_sms']=='yes'){
                                                echo "<img src=\"$web/img/icon_tick.png\">&nbsp;&nbsp;&nbsp;Text Message sent <br/>";
                                            }
                                            ?>
                                            <b>Your appointment is confirmed. You can close the browser now or <a href="http://dmv.dc.gov/">click here</a> to return to our main website.</b><br/>
                                            <img style="height: 36px;" src="<?= Yii::getAlias("@web") ?>/img/icon_print.png">&nbsp;&nbsp;&nbsp;<a onclick="window.print();" style="cursor: pointer;">Click here to print.</a><br/>
                                            <img style="height: 36px;" src="<?= Yii::getAlias("@web") ?>/img/icon_save.png">&nbsp;&nbsp;&nbsp;<a onclick="window.print();" style="cursor: pointer;">Click here to save.</a><br/>
                                        </div>
                                    </div>
                                </div>

                                <?
                            }
                            ?>
                        </div>


                        <div class="form-line-breaker"></div>


                        <?
                        if (@!$get["confirm"]) {
                            ?>
                            <div class="sitem14">

                                <a href="<?= Yii::getAlias("@web") ?>/index.php/customer-reserved-timeslots/create?id=<?=$get["customer_id"]?>"
                                   class="btn btn-primary sitem16-b">Modify</a>

                                <a href="<?= Yii::getAlias("@web") ?>/index.php/customer-reserved-timeslots/indexedit?id=<?= $get["id"] ?>&customer_id=<?= $get["customer_id"] ?>&delete=yes"
                                   class="btn btn-primary sitem16" >Cancel Appointment</a>

                                <a href="#"
                                   class="btn btn-primary sitem15" onclick="return is_notify_checked();">Keep It!</a>

                            </div>
                            <?
                        }
                        ?>






                    </div>
            </fieldset>
        </form>


    </div>
</div>


<script>
    function is_notify_checked(){

            var url="<?= Yii::getAlias("@web") ?>/index.php/customer-reserved-timeslots/index?id=<?= $get["id"] ?>&customer_id=<?= $get["customer_id"] ?>&confirm=yes";
            if($("#notify_by_email").is(":checked")){
                url+="&notify_email=yes";
            }
            if($("#notify_by_sms").is(":checked")){
                url+="&notify_sms=yes";
            }

            window.location=url;
            return false;

    }

</script>