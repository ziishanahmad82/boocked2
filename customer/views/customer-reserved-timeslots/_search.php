<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\CustomerReservedTimeslotsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="customer-reserved-timeslots-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'timeslot_id') ?>

    <?= $form->field($model, 'customer_id') ?>

    <?= $form->field($model, 'location_id') ?>

    <?= $form->field($model, 'test_date') ?>

    <?= $form->field($model, 'test_start_time') ?>

    <?php // echo $form->field($model, 'test_end_time') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
