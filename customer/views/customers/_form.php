<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\SubTestTypes;
use app\models\LicenseClasses;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Customers */
/* @var $form yii\widgets\ActiveForm */
$request = Yii::$app->request;
$get = $request->get();
?>

<div class="customers-form inner-form sitem5">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'learners_permit_number')->textInput(['maxlength' => 20, 'size'=>20]) ?>


    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <!--    --><?//= $form->field($model, 'middle_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>


    <div class="form-line-breaker"></div>

    <?=
    $form->field($model, 'date_of_birth')->widget(\yii\jui\DatePicker::classname(), [
        'dateFormat' => 'MM-dd-yyyy',
        'clientOptions' => [
            //'dateFormat' => 'yyyy-MM-dd',
            'showAnim' => 'fold',
            'yearRange' => 'c-25:c+10',
            'changeMonth' => true,
            'changeYear' => true,
            'autoSize' => true,
            'defaultDate' => '01-01-1990',
            //'showOn'=> "button",
            //'buttonText' => 'clique aqui',
            //'buttonImage'=> "images/calendar.gif",
        ]
    ])
    ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email_repeat')->textInput(['maxlength' => true, 'value' => $model->email]) ?>

    <input type="hidden" name="slot_id" value="<?= @$get["slot_id"] ?>">
    <div class="form-line-breaker"></div>


    <?= $form->field($model, 'cell_phone')->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '999-999-9999',
    ])->label('Cell Phone'); ?>



    <?php

    if ($model->isNewRecord) {
        $test_type = Yii::$app->request->get('test_type');
        if ($test_type == 'cdl') {
            $test_type = 'CDL';
        } elseif ($test_type == 'ncdl') {
            $test_type = 'NCDL';
        } else {
            exit();
        }
    } else {
        $test_type=$model->test_type;
    }


    ?>






    <?php

    if ($test_type == "CDL") {
        echo $form->field($model, 'license_class')->radioList(ArrayHelper::map(LicenseClasses::find()->where(['parent_id' => '1'])->all(), 'license_class_name', 'license_class_name'))->label('CMV Class');
    } else {
        echo $form->field($model, 'license_class')->radioList(ArrayHelper::map(LicenseClasses::find()->where(['parent_id' => '2'])->all(), 'license_class_name', 'license_class_name'));
    }



    if ($test_type == "CDL") {
        echo $form->field($model, 'sub_test_type[]')->checkboxList(ArrayHelper::map(SubTestTypes::find()->where(['test_parent_id' => '1'])->all(), 'test_name', 'test_name'))->label('Endorsements');

    } else {
        echo $form->field($model, 'sub_test_type')->radioList(ArrayHelper::map(SubTestTypes::find()->where(['test_parent_id' => '2'])->all(), 'test_name', 'test_name'));
    }




    if ($test_type == "CDL") {
        echo $form->field($model, 'test_type2')->radioList(['Skills Road Test'=>'Skills Road Test'])->label('Test Type');
    }

    ?>




    <div class="form-line-breaker"></div>

    <?= $form->field($model, 'test_type')->hiddenInput(['value' => $test_type])->label(false); ?>
    <div class="form-line-breaker"></div>

    <div class="sitem4">

            <?= Html::submitButton($model->isNewRecord ? 'Next' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary sitem3' : 'btn btn-primary sitem3']) ?>

    </div>

    <?php ActiveForm::end(); ?>

</div>

<?
$this->registerJs(
    '
    $("#customers-license_class input[type=radio][value=D]").attr("checked", "checked");
    $("#customers-sub_test_type input[type=radio][value=\'Skills Road Test\']").attr("checked", "checked");
    $("#customers-test_type2 input[type=radio][value=\'Skills Road Test\']").attr("checked", "checked");
    ');
?>


