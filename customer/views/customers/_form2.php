<?php


use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\SubTestTypes;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model app\models\Customers */
/* @var $form yii\widgets\ActiveForm */
$request = Yii::$app->request;
$get = $request->get();
?>

<div class="customers-form inner-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'learners_permit_number')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'middle_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>



    <?=
    $form->field($model, 'date_of_birth')->widget(\yii\jui\DatePicker::classname(), [
        'dateFormat' => 'MM-dd-yyyy',
        'clientOptions' => [
            //'dateFormat' => 'yyyy-MM-dd',
            'showAnim' => 'fold',
            'yearRange' => 'c-25:c+10',
            'changeMonth' => true,
            'changeYear' => true,
            'autoSize' => true,
            'defaultDate' => '01-01-1990',
            //'showOn'=> "button",
            //'buttonText' => 'clique aqui',
            //'buttonImage'=> "images/calendar.gif",
        ]
    ])
    ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email_repeat')->textInput(['maxlength' => true, 'value' => $model->email]) ?>

    <input type="hidden" name="slot_id" value="<?= @$get["slot_id"] ?>">

    <?= $form->field($model, 'cell_phone')->textInput(['maxlength' => true]) ?>


    <?php

    if ($model->isNewRecord) {
        $test_type = Yii::$app->request->get('test_type');
        if ($test_type == 'cdl') {
            $test_type = 'CDL';
        } elseif ($test_type == 'ncdl') {
            $test_type = 'NCDL';
        } else {
            exit();
        }
    } else {
        $test_type=$model->test_type;
    }


    ?>

    <?= $form->field($model, 'test_type')->hiddenInput(['value' => $test_type])->label(false); ?>


    <?php
    if ($test_type == "CDL") {
        echo $form->field($model, 'sub_test_type')->dropDownList(ArrayHelper::map(SubTestTypes::find()->where(['test_parent_id' => '1'])->all(), 'test_name', 'test_name'));
    }


    ?>

    <div class="form-group btns btns-right">
        <?= Html::submitButton($model->isNewRecord ? 'Next' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-primary' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
