<?php
use yii\helpers\Html;
use app\models\States;
use app\models\Countries;
use app\models\Cities;
use app\models\Staff;

/* @var $this yii\web\View */
$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$path   = Yii::getAlias('@web');
$admin_sidepath = substr($path, 0, strrpos($path, "customer")) ?>
<div class="container-fluid banner-container">
    <div class="row">
        <div class="col-lg-12">
           <?php if($business_information->about_image!=""){ ?>
            <img src='<?= $admin_sidepath ?>admin/web/<?= $business_information->about_image ?>' width="100%" style="border-radius: 4px;">
           <?php }else {
               ?>
               <img src='<?= Yii::getAlias('@web') ?>/images/banner.jpg' width="100%" style="border-radius: 4px;">
           <?php } ?>
        </div>
    </div>
</div>

<div class="container-fluid" style="margin-top: 6%;">
    <div class="row">
        <div class="col-lg-7 col-md-7 col-sm-7">

                <div class=" about-customer">
                    <h3><?= $business_information->business_name ?></h3>
                    <p><?= $business_information->business_description ?></p>
                    <hr>
                    <address>
                        <strong><?= $business_information->business_name ?></strong>,<br>
                        <?= $business_information->address.', '.Cities::getCity_id($business_information->city).', '.States::getState_id($business_information->state).', '.Countries::getCountry_id($business_information->country).' - '.$business_information->zip ?><br><br>
                        <strong>Call:</strong> <?= $business_information->business_phone ?><br>
                        <?php $staffs = Staff::find()->where(['business_id'=>$business_information->business_id,'staff_status'=>1,'user_group'=>'admin'])->all(); ?>

                        <strong>Email:</strong> <? foreach($staffs as $staffs){ echo $staffs->email.',  ';  } ?><br>
                    </address>
                    <a href="<?= Yii::getAlias('@web') ?>/index.php/site/index" class="btn btn-main btn-save btn-schedule"> Schedule Now</a>
                </div>


           
        </div>

        <div class="col-lg-5 col-md-5 col-sm-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title text-uppercase"><i class="fa fa-arrow-circle-o-up icon-title"></i> <span class="title">gallery</span></h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <p style="margin-top: 15px; margin-bottom: 15px;">Images</p>
                            <div class="gallery-scroller">
                                <ul class="list-inline list-gallery">
                                   <?php foreach($services as $service){ ?>

                                    <li><a href="#"><img src="<?= $admin_sidepath ?>admin/web/<?= $service->service_car_image1 ?>" alt="<?= $service->service_name ?>" style="max-width:71px"></a></li>
                                  <?php  } ?>
                                  
                                    <!--  <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>-->
                                </ul>
                            </div>
                        </div>
                    </div><hr style="margin-top: 30px;">
                    <div class="row">
                        <div class="col-lg-12">
                            <p style="margin-top: 15px; margin-bottom: 15px;">Videos</p>
                            <div class="gallery-scroller">
                                <ul class="list-inline list-gallery">
                                    <?php foreach($videosservices as $video_service){
                                       // echo $video_service->service_id.$video_service->service_name;
                                        ?>
                                    <li><a href="#">
                                                <video style="max-width:200px" controls>
                                                    <source src="<?= $admin_sidepath.'admin/web/'.$video_service->service_video ?>"  type="video/mp4">
                                                </video>
                                            </li>
                                   <?php  } ?>
                                  <!--  <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li> -->
                                </ul>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!--
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <h2><?= $business_information->business_name ?></h2>
    <?= $business_information->address.' '.$business_information->city.' '.$business_information->state.' '.$business_information->country.' '.$business_information->zip ?>
    <br/><strong>Call</strong> : <?= $business_information->business_phone ?>

    <br/>
    <?= $business_information->business_description ?>


    <h3>Staff</h3>
    <?php foreach ($staff as $staffs){ ?>
       <p><?= $staffs->username ?></p>
    <?php } ?>

</div> -->
