<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Staff;
use app\models\Services;

$this->title = 'Reviews';
?>
<?php
$path   = Yii::getAlias('@web');
$admin_sidepath = substr($path, 0, strrpos($path, "customer"));

?>

<div class="modal fade" id="reschedule_box" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header review_model">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Reschedule Appointments</h4>
            </div>
            <?php
            $service =  \app\models\Services::getServicebyid($appointment->service_id,$appointment->business_id);
            ?>
            <div class="modal-body">
                <form id="reschedule_button_form">
                <div class="review_content_box">

                    <input type="hidden" value="<?= $appointment->appointment_id ?>" name="apntID" >
                    <input type="hidden" value="<?=  $service->service_id ?>" name="service" >
                    <input type="hidden" value="<?=  $appointment->user_id ?>" name="staff" >
                    <p>Service Name: <span id="re_srid" style="color:#f07a30" data-srid="<?=  $service->service_id ?>" ><?=  $service->service_name ?></span><span style="color:#636363"> @ <?= date('g:i a',strtotime($appointment->appointment_start_time))  ?> </span></p>
                    <p>Employ Name:  <span id="re_staffs" data-stid="<?=  $appointment->user_id ?>" style="color:#f07a30"><?= Staff::getusername($appointment->user_id) ?></span><span style="color:#636363"> <?= date('j F , Y',strtotime($appointment->appointment_date))  ?> </span> </p>
                    Date <input  name="reschedule_date" type="text" class="form-control" id="reschedule_datepicker" style="max-width:100px;display:inline-block" readonly='true'>
                                    <span style=" display:inline-block;margin-left:10px;" id="reschedule_mod_time_cont">
                                        <select name="reschedule_time"  type="text" class="form-control" id="reschedule_time" style="max-width:100px;display:inline-block;padding-left:5px">
                                            <option>-NA-</option>
                                        </select>
                                        </span>
                </div>

                </form>
                <div class="review_button_box">
                    <a href="#"   data-apnt="<?= $appointment->appointment_id ?>" id="rechedule_submit" >Reschedule<span class="glyphicon glyphicon-arrow-right"></span></a>
                </div>
            </div>


        </div>

    </div>
</div>
<!-- Modal -->
<?php

$script1 = "$('#reschedule_datepicker').datepicker({
    minDate: 0,
    setDate: new Date(),
});";
$this->registerJs($script1);
?>


