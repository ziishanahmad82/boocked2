<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use app\models\Reviews;
use app\models\Countries;

/* @var $this yii\web\View */

$this->title = 'Dashboard';

$countries = Countries::find()->all();


$request = Yii::$app->request;
$get = $request->get();
$session = Yii::$app->session;
if(@Yii::$app->user->identity->id)
{ // AND stristr(Yii::$app->user->identity->user_group, 'thirdparty-')
   // $session["3rd_user_id"]=Yii::$app->user->identity->id;
   // $session["3rd_user_name"]=Yii::$app->user->identity->username;
    //$session["3rd_user_group"]=Yii::$app->user->identity->user_group;
}
$ajax_path=Yii::getAlias('@web');
?>
<style>
  /*  .servicecheckbox {
        top:40%;
        left:40%;
        position:absolute;
        display:none;
        z-index:10001;
    }
    .services_customercontbox:hover .service-select{
        display:block !important;
        z-index:10001;


    } */

</style>
<?php
$review_names = Yii::$app->session->get('bk_show_customer_name_reviews');


?>
<?  $path   = Yii::getAlias('@web');
$admin_sidepath = substr($path, 0, strrpos($path, "customer"));
?>
<style>
    .dashboard_tabs li {
        margin-left:0 !important;
        background:#ebebeb !important;
        border-radius:5px 5px 0 0px;

    }

    .dashboard_tabs li a {
        border:none;
        background:none;
        margin-left:0;
        color:#000;

    }
    .dashboard_tabs li:hover , .dashboard_tabs li.active{
        background:#f27a21 !important;
        border-right:0;
    }
    .dashboard_tabs li.active a , .dashboard_tabs li:hover a {
        border:none !important;
        background:none !important;
        color:#fff !important;
    }
</style>
<div class="container-fluid" style="margin-top: 2%;">
    <div class="row">
        <div class="col-lg-3 col-md-3 col-sm-3 main-staff-container">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title text-uppercase" ><i class="fa fa-user icon-title"></i> <span class="title">INFO</span></h4> </a>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">

                        </div>
                    </div>
                   <?php
                   $business_information = Yii::$app->user->identity->business_id;
                   $customer_id  = Yii::$app->user->identity->customer_id ?>
                    <div class="staffresults">
                            <div class="search_staff_box ">

                                <div class="row" style="background:#fff">
                                    <div class="col-sm-12" style="text-align:center">
                                         <?php
                                       if(Yii::$app->user->identity->profile_image!=""){?>
                                            <img src="<?= $admin_sidepath . 'admin/web/' . Yii::$app->user->identity->profile_image ?>" class="<?= Yii::$app->user->identity->first_name ?>" style="width:150px;border-radius:50%">
                                        <?php  }else{ ?>
                                            <img src="<?= Yii::getAlias('@web') ?>/images/dashboard_thumbnail.jpg" class="<?= Yii::$app->user->identity->first_name ?>" style="width:150px;border-radius:50%">
                                        <?php } ?>
                                     </div>

                                    <div class="col-sm-12" style="text-align:center">
                                        <p class="staff-name"><?= Yii::$app->user->identity->first_name ?> <?= Yii::$app->user->identity->last_name ?></p>
                                        <a href="#" data-toggle="modal" data-target="#modal-update-c"><!--<i class="fa fa-pencil-square-o" aria-hidden="true"></i>--> Edit </a>

                                    </div>
                                </div>

                            </div>
                        <div class="row" style="background: #f27a21;color: #fff;font-size: 16px;margin-bottom: -15px;margin-top: 17px;padding: 12px;text-align: center;
}">
                        <?= Yii::$app->user->identity->email ?>
                        </div>

                    </div>


                </div>
            </div>
        </div>
        <div class="<?php if(Yii::$app->session->get('bk_staff_member_customer_visibility')!='0'){ ?> col-sm-9 <?php }else { echo 'col-sm-12'; } ?> main-services-container">
            <ul class="nav nav-tabs dashboard_tabs">
                <li class="active"><a href="#upcoming_apnt" data-toggle="tab">Upcoming</a></li>
                <li><a href="#past_apnt" data-toggle="tab" style="border-top-right-radius: 4px;">Past</a></li>
            </ul>
            <div class="panel panel-default">
                <div class="panel-heading" style="display:none">
                    <h4 class="panel-title text-uppercase"><i class="fa fa-gear icon-title"></i> <span class="title">services</span></h4>
                </div>
                <div class="tab-content">
                    <div class="panel-body panel-scroller tab-pane fade  " id="past_apnt">
                        <div class="panel-inner-services-container">
                            <div class="row">
                                <div class="col-lg-4 col-lg-offset-8 col-md-6 col-md-offset-6 col-sm-6 col-sm-offset-6" style="display:none">
                                    <div class="form-group">
                                        <input type="text" placeholder="Search services here"  id="service_search" class="form-control field-search">
                                    </div>
                                </div>
                            </div>
                            <?php

                                foreach ($past_appointments as $past_appointment){
                                $service =  \app\models\Services::getServicebyid($past_appointment->service_id,$past_appointment->business_id);
                               //     print_r($past_appointment->service_id);
                               //     exit();
                               ?>
                                <div class="services_listbox">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                            <ul class="list-inline">

                                                <li class="stars">
                                                    <ul class="list-unstyled list-service-title">
                                                        <li class="text-uppercase"><b class="service_namecont"><?=  $service->service_name ?></b></li>

                                                    </ul>
                                                </li>
                                            </ul>
                                        </div>

                                    </div>
                                    <div class="row" style="margin-top: 3%;">
                                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 media-col-slider services_customercontbox">
                                            <div class="carousel slide" data-ride="carousel" id="car">

                                                <!-- <div class="service-select"> <input type="checkbox"></div>-->


                                                <!-- <div class="item active service-container">
                                            <img src="<? //= Yii::getAlias('@web') ?>/images/service-1.png" width="100%;">
                                            <div class="service-select">
                                                <input type="checkbox">
                                            </div>
                                        </div>-->

                                                <?php if($service->service_image!="") { ?>
                                                    <div class="carousel-inner">
                                                        <div class="item active service-container">
                                                            <img src="<?= $admin_sidepath . 'admin/web/' . $service->service_image ?>" width="100%;">

                                                        </div>
                                                    </div>
                                                <?php  }else { ?>
                                                    <div class="carousel-inner">
                                                        <div class="item active service-container">
                                                            <img src="<?= Yii::getAlias('@web') ?>/images/no-image.png" width="100%;">

                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                                ?>

                                            </div>



                                        </div>
                                        <!--   <p>Description</p>
                                            <p style="margin-top: -5px;"><small><?=  $service->service_description ?></small></p> -->
                                        <?php
                                        $reviews_content = Reviews::find()->where(['appointment_id'=>$past_appointment->appointment_id])->all();
                                        ?>
                                        <?php

                                        if(!empty($reviews_content)) {
                                        foreach ($reviews_content as $review_content) {
                                        ?>
                                        <div class="col-lg-8 col-md-8 col-sm-8 media-col-desc">
                                            <div class="">




                                                        <p style="color:#8a8a8a;font-size:18px;line-height:25px"><?= $review_content->review_text ?></p>
                                                        <!--     <div class="row">
                                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 container-media-1">

                                                            <img src="<?= Yii::getAlias('@web') ?>/images/good.png" width="30">
                                                        </div>
                                                        <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 container-media-11">
                                                            <small><?= $review_content->review_text ?> <?php if($review_names!=0){ echo' - '.\app\models\Users::getcustomername($review_content->customer_id); } ?></small>
                                                        </div>
                                                    </div> -->



                                            </div>
                                        </div>
                                        <div class="col-sm-1">
                                            <?php if($review_content->review_status=='4'){ ?>
                                                <img src="<?=Yii::getAlias('@web'); ?>/images/excelant.png" style="max-width: 100%; border-radius: 44%;" />

                                            <?php   }else if($review_content->review_status=='3'){ ?>
                                                <img src="<?=Yii::getAlias('@web'); ?>/images/good.png" style="max-width: 100%; border-radius: 44%;" />
                                            <?php   }else if($review_content->review_status=='2'){ ?>
                                                <img src="<?=Yii::getAlias('@web'); ?>/images/fair.png" style="max-width: 100%; border-radius: 44%;" />
                                            <?php   }else if($review_content->review_status=='1'){ ?>
                                                <img src="<?=Yii::getAlias('@web'); ?>/images/bad.png" style="max-width: 100%; border-radius: 44%;" />
                                            <?php   }else if($review_content->review_status=='0'){ ?>
                                                <img src="<?=Yii::getAlias('@web'); ?>/images/poor.png"  style="max-width: 100%; border-radius: 44%;" />
                                            <?php } ?>
                                        </div>

                                            <?php
                                        }
                                            $review_button = 'Edit';
                                        }else {
                                            $review_button = 'Write a Review';
                                        }
                                        ?>
                                        <div class="clearfix"></div>
                                        <div class="col-sm-6 pull-left">
                                            <?php  $staff  = \app\models\Staff::getstaff($past_appointment->user_id,$past_appointment->business_id);

                                            ?>
                                            <p style="margin-top:20px" >By <?  if($staff!="Unknown"){ echo $staff->username; }else { echo 'Unknown';
                                                } ?> @ <?= date('M d, Y',strtotime($past_appointment->appointment_date)) ?> <?php  if(Yii::$app->session->get('bk_show_service_time_customer')!='0'){ ?>(<?=  $service->service_duration ?> Mins) <?php }  if(Yii::$app->session->get('bk_show_service_cost_customer')!='0'){ ?><span class=""><small style="color:#f27a21"><?= Yii::$app->session->get('bk_currency'); ?> <?=  $service->service_price ?></small> </span></p>
                                        <?php } ?>
                                        </div>
                                        <div class="col-sm-6 pull-right" style="margin-top:20px">
                                            <a href="#" class="btn btn-main pull-right edit_review" data-apnt="<?= $past_appointment->appointment_id ?>" style="margin-right:10px;"><?= $review_button ?> </a>
                                        </div>

                                    </div>
                                    <hr style="margin-top: 30px;">
                                </div>
                            <? } ?>

                            <a href="book.html" class="btn btn-main pull-right" onclick="nextcalendar(event)">Continue</a>
                        </div>
                    </div>
                    <div class="panel-body panel-scroller tab-pane fade in active" id="upcoming_apnt">
                        <div class="panel-inner-services-container">
                        <div class="row">
                            <div class="col-lg-4 col-lg-offset-8 col-md-6 col-md-offset-6 col-sm-6 col-sm-offset-6" style="display:none">
                                <div class="form-group">
                                    <input type="text" placeholder="Search services here"  id="service_search" class="form-control field-search">
                                </div>
                            </div>
                        </div>
                        <?php  foreach ($upcoming_appointments as $upcoming_appointments){
                              $service =  \app\models\Services::getServicebyid($upcoming_appointments->service_id,$upcoming_appointments->business_id);


                            ?>
                            <div class="services_listbox">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <ul class="list-inline">

                                            <li class="stars">
                                                <ul class="list-unstyled list-service-title">
                                                    <li class="text-uppercase"><b class="service_namecont"><?=  $service->service_name ?></b></li>

                                                </ul>
                                            </li>
                                        </ul>
                                    </div>

                                </div>
                                <div class="row" style="margin-top: 3%;">
                                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 media-col-slider services_customercontbox">
                                        <div class="carousel slide" data-ride="carousel" id="car">

                                            <!-- <div class="service-select"> <input type="checkbox"></div>-->


                                            <!-- <div class="item active service-container">
                                            <img src="<? //= Yii::getAlias('@web') ?>/images/service-1.png" width="100%;">
                                            <div class="service-select">
                                                <input type="checkbox">
                                            </div>
                                        </div>-->

                                            <?php if($service->service_image!="") { ?>
                                            <div class="carousel-inner">
                                                <div class="item active service-container">
                                                    <img src="<?= $admin_sidepath . 'admin/web/' . $service->service_image ?>" width="100%;">

                                                </div>
                                            </div>
                                            <?php  }else { ?>
                                                <div class="carousel-inner">
                                                    <div class="item active service-container">
                                                        <img src="<?= Yii::getAlias('@web') ?>/images/no-image.png" width="100%;">

                                                    </div>
                                                </div>
                                                <?php
                                            }
                                            ?>

                                        </div>



                                    </div>
                                    <div class="col-lg-9 col-md-9 col-sm-9 media-col-desc">
                                        <div class="">
                                         <!--   <p>Description</p>
                                            <p style="margin-top: -5px;"><small><?=  $service->service_description ?></small></p>
                                            <?php
                                            $reviews_content = Reviews::find()->where(['service_id'=>$service->service_id ])->all();
                                            ?>
                                            <p>Customer Reviews: <?= count($reviews_content) ?> reviews <small></small></p>-->
                                            <?php

                                            if(isset($reviews_content)) {
                                                foreach ($reviews_content as $review_content) {

                                                    ?>
                                                    <p style="color:#8a8a8a;font-size:18px;line-height:25px">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
                                               <!--     <div class="row">
                                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 container-media-1">

                                                            <img src="<?= Yii::getAlias('@web') ?>/images/good.png" width="30">
                                                        </div>
                                                        <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 container-media-11">
                                                            <small><?= $review_content->review_text ?> <?php if($review_names!=0){ echo' - '.\app\models\Users::getcustomername($review_content->customer_id); } ?></small>
                                                        </div>
                                                    </div> -->

                                                    <?php
                                                }
                                            }
                                            ?>


                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="col-sm-6 pull-left">
                                        <?php  $staff  = \app\models\Staff::getstaff($upcoming_appointments->user_id,$upcoming_appointments->business_id); ?>
                                    <p style="margin-top:20px" >By <?= $staff->username ?> @ <?= date('m-d-Y',strtotime($upcoming_appointments->appointment_date)) ?> <?php  if(Yii::$app->session->get('bk_show_service_time_customer')!='0'){ ?><?=  $service->service_duration ?> Minutes <?php }  if(Yii::$app->session->get('bk_show_service_cost_customer')!='0'){ ?><span class=""><small style="color:#f27a21"><?= Yii::$app->session->get('bk_currency'); ?> <?=  $service->service_price ?></small> </span></p>
                                    <?php } ?>
                                    </div>
                                    <div class="col-sm-6 pull-right" style="margin-top:20px">
                                        <a href="#" class="btn btn-main pull-right cancel_appointment_butt" data-apnt="<?= $upcoming_appointments->appointment_id ?>"> Cancel Appointment </a>
                                        <a href="#" class="btn btn-main pull-right reschedule_appointment_but"  data-apnt="<?= $upcoming_appointments->appointment_id ?>" style="margin-right:10px;"> Rescedule </a>
                                    </div>

                                </div>
                                <hr style="margin-top: 30px;">
                            </div>
                        <? } ?>

                        <a href="book.html" class="btn btn-main pull-right" onclick="nextcalendar(event)">Continue</a>
                    </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="modal fade" id="modal-update-c">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body modal-ex">
                <div class="panel panel-default" style="margin-bottom: 0;">
                    <div class="panel-heading">
                        <h4 class="panel-title text-uppercase"><i class="fa fa-user icon-title"></i> <span class="title">My Information</span></h4>
                    </div>
                    <?php
                    // ,'layout' => 'horizontal'
                    /*  $form = ActiveForm::begin(['id' => 'login-form']); ?>
                        <?= $form->field($model, 'username') ?>
                        <?= $form->field($model, 'first_name') ?>
                        <?= $form->field($model, 'last_name')?>
                        <?= $form->field($model, 'email') ?>
                        <?= $form->field($model, 'password')->passwordInput() ?>
                        <?= $form->field($model, 'password_repeat')->passwordInput()  */ ?>
                    <div class="panel-body panel-ex" id="signupformcont" style="padding-top:40px;padding-bottom:40px">
                        <?php $form = ActiveForm::begin(['id' => 'form-signupbooking',  'validationUrl' => Yii::getAlias('@web').'/index.php/site/bookingsignup_validate', 'enableAjaxValidation' => true,'action'=>Yii::getAlias('@web').'/index.php/site/bookingsignup', 'options' => ['enctype' => 'multipart/form-data'] ]); ?>
                        <?= $form->field($model, 'username')->textInput(['placeholder' => "Username"])->label(false) ?>

                        <!-- <div class="form-group">
                                <input type="text" class="form-control" placeholder="First Name">
                            </div>-->
                        <div class="row">
                            <div class="col-lg-6 mid">
                                <?= $form->field($model, 'first_name')->textInput(['placeholder' => "First Name"])->label(false) ?>
                            </div>
                            <!--<div class="form-group col-lg-6 last field-signupform-first_name required">
                                <input type="text" name="SignupForm[first_name]" class="form-control" id="signupform-first_name">
                                <p class="help-block"></p>
                            </div>-->
                            <div class="col-lg-6 last">
                                <?= $form->field($model, 'last_name')->textInput(['placeholder' => "Last Name"])->label(false) ?>
                            </div>
                            <!-- <div class="last field-signupform-last_name required">
                                <label for="signupform-last_name" class="control-label"></label>
                                <input type="text" name="SignupForm[last_name]" class="form-control" id="signupform-last_name">

                                <div class="help-block"></div>
                            </div>-->
                            <? //= $form->field($model, 'last_name')?>
                            <!-- <div class="form-group col-lg-6 last">
                                 <input type="text" class="form-control" placeholder="Last Name">
                             </div>-->
                        </div>
                        <? //= $form->field($model, 'first_name') ?>
                        <?= $form->field($model, 'email')->input('email',['placeholder' => "Email"])->label(false) ?>
                        <?php if(Yii::$app->session->get('bk_zip_field')!='0'){ ?>
                            <?= $form->field($model, 'customer_address')->textInput(['placeholder'=>'Postal Address'])->label(false) ?>
                        <?php } ?>

                        <!--  <div class="form-group">
                                  <input type="text" class="form-control" placeholder="Postal Address">
                              </div>-->
                        <?php if(Yii::$app->session->get('bk_city_state_field')!='0'){ ?>
                            <div class="row">
                                <div class="form-group col-lg-6 mid">
                                    <select class="form-control" id="businessinformation-country" name="SignupForm[customer_country]" onchange="buisness_country(this)">
                                        <?php  foreach($countries as $country){ ?>
                                            <option <?php if($model->customer_country==$country->id){ echo 'selected'; }?> value="<?= $country->id ?>">
                                                <?= $country->name ?>
                                            </option>
                                        <?php }  ?>
                                    </select>
                                    <!--   <input type="text" class="form-control" placeholder="State">-->
                                </div>
                                <?php
                                if(!empty($model->customer_country)){
                                     $count_id = $model->customer_country;

                                }else {
                                    $count_id = 1;
                                }

                                if(!empty($model->customer_region != '')){
                                    $state_id = $model->customer_region;

                                }else {
                                    $state_id = 1;
                                }


                                $states = \app\models\States::find()->where(['country_id'=>$count_id])->all();

                                $cities = \app\models\Cities::find()->where(['state_id'=>$state_id])->all();
                               ?>
                               <div class="form-group col-lg-6 last">
                                    <select class="form-control" id="businessinformation-state" name="SignupForm[customer_region]">
                                        <?php
                                        foreach($states as $state){
                                            ?>
                                            <option <?php if($model->customer_region==$state->id ){  echo 'selected';  } ?> value="<?= $state->id ?>"><?= $state->name ?></option>
                                        <?php }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        <?php } ?>
                        <div class="row">
                            <?php if(Yii::$app->session->get('bk_city_state_field')!='0'){ ?>
                                <div class="form-group col-lg-6 mid">
                                    <select class="form-control" id="signupform-customer_city" name="SignupForm[customer_city]" >
                                     <?php
                                        foreach($cities as $city){
                                            ?>
                                            <option <?php if($model->customer_city==$city->id ){  echo 'selected';  } ?> value="<?= $city->id ?>"><?= $city->name ?></option>
                                       <?php }
                                        ?>
                                    </select>
                                </div>
                            <?php   }    ?>
                            <?php if(Yii::$app->session->get('bk_zip_field')!='0'){ ?>
                                <div class="col-lg-6 last">
                                    <?= $form->field($model, 'customer_zip')->textInput(['placeholder'=>'Zip Code'])->label(false) ?>
                                    <!--  <input type="text" class="form-control" placeholder="Zip Code">-->
                                </div>
                            <?php } ?>
                            <div class="clearfix"></div>
                        </div>
                        <div class="row">


                         <!--   <div class="form-group col-lg-6 mid">
                                <? //= $form->field($model, 'password')->passwordInput(['placeholder'=>'Password'])->label(false) ?>
                            </div>
                            <div class="form-group col-lg-6 mid">
                                <? //= $form->field($model, 'password_repeat')->passwordInput(['placeholder'=>'Confirm Password'])->label(false) ?>
                            </div>-->
                        </div>
                        <?= Html::submitButton('Submit', ['class' => 'btn btn-main pull-right', 'name' => 'signup-button']) ?>
                        <!--  <button class="btn btn-main pull-right" type="button" data-toggle="modal" data-target="#thanku" id="submit-ex-btn">Submit</button>-->
                        <?php ActiveForm::end(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="hiddenformset"></div>





<?php /* ?>
<div class="modal fade" id="review_screen_1" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header review_model">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Rate Your Experience</h4>
            </div>
            <div class="modal-body">
                <div class="review_content_box">
                    <p>Service Name: <span style="color:#f07a30"><?=  $items['services']['service_name']; ?></span><span style="color:#636363"> @ <?= date('g:i a',strtotime($items->appointment_start_time))  ?> </span></p>
                    <p>Employ Name:  <span style="color:#f07a30"><?= Staff::getusername($items->user_id) ?></span><span style="color:#636363"> <?= date('j F , Y',strtotime($items->appointment_date))  ?> </span> </p>
                </div>
                <div class="review_gray_box emjisicons">
                    <a href="#" <?php if($review_status=='' || $review_status=='4' ){ ?> class="active" <?php } ?> data-toggle="tooltip" data-review="4" title="Excellent"><img src="<?=Yii::getAlias('@web'); ?>/images/excelant.png" /> </a>
                    <a href="#" <?php if($review_status=='3' ){ ?> class="active" <?php } ?> data-toggle="tooltip" data-review="3" title="Good"><img src="<?=Yii::getAlias('@web'); ?>/images/good.png" /> </a>
                    <a href="#" <?php if($review_status=='2' ){ ?> class="active" <?php } ?> data-toggle="tooltip" data-review="2" title="Fair"><img src="<?=Yii::getAlias('@web'); ?>/images/fair.png" /> </a>
                    <a href="#" <?php if($review_status=='1' ){ ?> class="active" <?php } ?> data-toggle="tooltip" data-review="1" title="Bad"><img src="<?=Yii::getAlias('@web'); ?>/images/bad.png" /> </a>
                    <a href="#" <?php if($review_status=='0' ){ ?> class="active" <?php } ?> data-toggle="tooltip" data-review="0" title="Poor"><img src="<?=Yii::getAlias('@web'); ?>/images/poor.png" /> </a>
                </div>
                <div class="review_button_box">
                    <a href="#" data-toggle="modal" data-target="#review_screen_2" id="review_continue" >Continue <span class="glyphicon glyphicon-arrow-right"></span></a>


                </div>
            </div>

        </div>

    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="review_screen_2" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header review_model">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Care To Write A Few Words</h4>
            </div>
            <div class="modal-body">
                <div class="review_content_box">
                    <textarea class="form-control" id="review_text"><?= $review_text ?></textarea>
                </div>

                <div class="review_button_box" style="text-align:right">
                    <a href="#"  id="review_done">Done</a>

                </div>
            </div>

        </div>

    </div>
</div> <?php */ ?>

<div class="popups_content">

</div>
<div class="reschedule_pop"></div>

























































<?php
$script1 = "


$('.service_checkbox').click(function(){
    $('.service_checkbox').each(function(){
    $(this).prop('checked', false);
});
    $(this).prop('checked', true);
     serviceval = $(this).val();
     $.ajax({
            url: '".$ajax_path."/index.php/site/relatedstaff',
            type:'POST',
            data:{'serviceval':serviceval},
             success: function(response){
                $('.staffresults').html(response);
               
            }
            
            });
});


var rows = $('.services_listbox');
$('#service_search').keyup(function() {
//console.log($(this).children('.service_namecont').text());
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
    //console.log(val);
    rows.show().filter(function() {
  var text = $(this).find('.service_namecont').text().replace(/\s+/g, ' ').toLowerCase();
        console.log(text);
       return !~text.indexOf(val);
    }).hide();
});
var staffrows = $('.search_staff_box');
$('#search_staff').keyup(function() {
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
    staffrows.show().filter(function() {
  var text = $(this).find('.staff-name').text().replace(/\s+/g, ' ').toLowerCase();
       return !~text.indexOf(val);
    }).hide();
});

$('div.staffresults').on('click', '.search_staff_box', function() {
  
//$('.search_staff_box').click(function(event){
        $('.search_staff_box').removeClass('active-staff');
        $(this).addClass('active-staff');
        $('.search_staff_box .staff_checkbox').prop('checked', false);
        $(this).children('.staff_checkbox').prop('checked', true);
        
});


$('#reschedule_appointment').click(function(event){
    event.preventDefault();
     apntid = $(this).attr(data-apnt);
     
     $.ajax({
            url: '".$ajax_path."/index.php/site/rescedule_slot',
            type:'POST',
            data:{'apntid':apntid},
             success: function(response){
                $('.staffresults').html(response);
               
            }
            
            });
});

$('.edit_review').click(function(event){
    event.preventDefault();
    apnt_id  = $(this).attr('data-apnt');
      $.ajax({
            url: '".$ajax_path."/index.php/site/servicereview',
            type:'POST',
            data:{'apnt_id':apnt_id,'step':1},
             success: function(response){
                $('.popups_content').html(response);
                $('#review_screen_1').modal('show');
               
            }
            
            });
    

});
$(document).on('click','.emjisicons > a', function (event) {
    event.preventDefault();
    $('.emjisicons > a').removeClass('active');
    $(this).addClass('active');

});
 $('[data-toggle=\"tooltip\"]').tooltip();   
 
 $(document).on('click','#review_continue', function (event) {
 //$('#review_continue').click(function(){
  imgcontent   =  $('.emjisicons > a.active').html();
  imtitle   =  $('.emjisicons > a.active').attr('data-review');  
 $('#emojicontreview').html(imgcontent);
 $('#review_screen_1').modal('hide');
 apnt_id = $(this).attr('data-apnt');
 $.ajax({
    url: '".Yii::getAlias('@web')."/index.php/site/reviewsupdate',
    type: 'post',
  data:{
   'apnt_id':apnt_id,
    'feedback':imtitle,'type':1},
success: function (response) {
    if(response==1){
     
    }else {
        alert('not updated');

    }
}
});
 
 });
 $(document).on('click','#review_done', function (event) {
    event.preventDefault();
     review_text = $('#review_text').val();
     apnt_id = $(this).attr('data-apnt');
      $.ajax({
        url: '".Yii::getAlias('@web')."/index.php/site/reviewsupdate',
        type: 'post',
        data:{'apnt_id':apnt_id,'feedback':review_text,'type':2},
        success: function (response) {
            if(response==1){
             $('#review_screen_2').modal('hide');
            }
            }
            });
     
 });
 
 $('.reschedule_appointment_but').click(function(event){
        event.preventDefault();
        apnt_id = $(this).attr('data-apnt');
        $.ajax({
        url: '".Yii::getAlias('@web')."/index.php/site/reschedule',
        type: 'post',
        data:{'apnt_id':apnt_id},
        success: function (response) {
           $('.reschedule_pop').html(response);
           $('#reschedule_box').modal('show');
            }
            });
 });
 
 $('.cancel_appointment_butt').click(function(event){
        event.preventDefault();
         var r = confirm('You want to cancel this appointment?');
    if (r == true) {
        apnt_id = $(this).attr('data-apnt');  
        $.ajax({
        url: '".Yii::getAlias('@web')."/index.php/site/cancel_appointment',
        type: 'post',
        data:{'apnt_id':apnt_id},
        success: function (response) {
            if(response==1){
             location.reload();
            }
            }
            });
    } 
 });
 
 


    $(document).on('click', '#appointment_reschedule_fin', function(event) { 
           var reschedule_date1 = $('#reschedule_datepicker').val();
            var reschedule_date = new Date(reschedule_date1);
           var date1 = new Date();
           var rescedhuletime   =  $('#reschedule_time option:selected').val()
           if((date1 > reschedule_date) || ( reschedule_date1=='' )){
                alert('Please Select date');
           }else if(!rescedhuletime){
               alert('Please select Time');
           }else {
           fromdata = $('#reschedule_form').serialize();
           $.ajax({
        url: '". Yii::getAlias('@web')."/index.php/site/rdsa',
        type: 'post',
        data:fromdata,
        success: function (response) {
            if(response==1){
            $('#appointment_detail2').modal('hide');
             $.pjax.reload({container:'#calendar_reload'});
            }
        }
    });
           }
            //if((reschedule_date=='') || )
    });
     $(document).on('change', '#reschedule_datepicker', function(event) { 
  //  $('#reschedule_datepicker').change(function(){
            var reschedule_date = $('#reschedule_datepicker').val();
            var reschedule_date1 = new Date(reschedule_date);
            var date1 = new Date();
           
           if(date1 > reschedule_date1 ){
                $('#reschedule_mod_time_cont').html('<select id=\"reschedule_time\" class=\"form-control\" ><option>N/A</option></select>');
           }else{
                // stID = $('#re_staffs_list option:selected').val();
               
                 stID = $('#re_staffs').attr('data-stid');
                 srID = $('#re_srid').attr('data-srid');
                 
                  $.ajax({
                        url: '". Yii::getAlias('@web')."/index.php/site/rescheduleslots',
                        type: 'post',
                        data:{'service':srID,'staff':stID,'date':reschedule_date},
                        success: function (response) {
                           $('#reschedule_time').html(response);
                        }
                });
                 
           }
    });
    
     $(document).on('click', '#rechedule_submit', function(event) {
     event.preventDefault();
      var reschedule_date = $('#reschedule_datepicker').val();
      var reschedule_time = $('#reschedule_time option:selected').val();
      if((reschedule_time!='') && (reschedule_date !='')){ 
         form_data  = $('#reschedule_button_form').serialize();
         $.ajax({
                        url: '". Yii::getAlias('@web')."/index.php/site/rescheduleappointment',
                        type: 'post',
                        data:form_data,
                        success: function (response) {
                          if(response==1){
                          $('#reschedule_box').modal('hide');
                          }
                        }
                });
      }else {
      alert('Please enter date and time');
      }
      })
    
 

";
$this->registerJs($script1);
?>
<script>
    function slotselection(xor,event,service,staff){
        event.preventDefault();
        var slot = $(xor).children('span').text();
         var service = $('input[name=service_name]:checked').val();


        alert(slot);

        $.ajax({
            url: "<?= $ajax_path?>/index.php/site/slotbooking",
            type:"POST",
            data:{'service':service,'slot':slot,'staff':staff},
            success: function(response){
                $('#loginbox').html(response);
                $('#slotbox').addClass('hidden');
            }

        });

    }

    function customerSignupLogin(event,xor){
        event.preventDefault();
         var target_url = $(xor).attr('href');
       $('#loginSignupcont').load(target_url);

    }
    function nextcalendar(event){
        event.preventDefault();
        var service = $('input[name=service_name]:checked').val();
        if(!service){
            alert('Please select atleast one service');
            return false;
        }
        var staff = $('input[name=staff_name]:checked').val();

        form = '<input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" /><input type="hidden" name="service" value="'+service+'"><input type="hidden" name="staff" value="'+staff+'">';
    $('<form action="<?= $ajax_path ?>/index.php/site/booking" method="POST">'+form+'</form>').appendTo('#hiddenformset').submit();



     /*   if(service) {
            $('#select_service_cont').addClass('hidden');
            $('#calendar_cont').removeClass('hidden');
            $.ajax({
              //  url: "<?= $ajax_path?>/index.php/site/loadcalendar",
                url: "<?= $ajax_path?>/index.php/site/booking",
                type:"POST",
                data:{'service':service,'staff':staff},
                success: function(response){
                    $('#calendar_cont').html(response);
                }

            });
          //  $('#calendar_cont').load('<?= $ajax_path?>/index.php/site/loadcalendar');
            $('.fc-today-button').click();
        }else {
            alert('Please select atleast One Service');

        }  */
    }
    function changeCustomerService(){
        $.pjax.reload({container:"#change_service"});


    }
    function bookorder(){
         var formdata = $('#book_customer_apointment').serialize();
        $.ajax({
            url: "<?= $ajax_path?>/index.php/site/bookingappointment",
            type:"POST",
            data:formdata,
            //,
            success: function(response){
                if(response==1) {
                    $('#order_summary').addClass('hidden');
                    $('#thankyou').removeClass('hidden');

                }
            },

        });
        
    }
    function backtocalendar(){
        $('#slotbox').addClass('hidden');
        $('#calendar_cont').removeClass('hidden');

    }

</script>
<?php

$this->registerJsFile('https://code.jquery.com/ui/1.12.1/jquery-ui.js', ['depends' => [\yii\web\JqueryAsset::className()]]);
$this->registerCssFile("//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css");
?>


