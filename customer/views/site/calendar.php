<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;

?>



<?php
if(!isset($staff)){
    $staff=0;


}
?>
<div class="container-fluid" style="margin-top: 2%;">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title text-uppercase"><i class="fa fa-gear icon-title"></i> <span class="title">heading</span></h4>
                        </div>
                        <div class="panel-body">
                            <div class="checkbox-service">
                                <ul class="list-inline">
                                    <li><img src="<?= Yii::getAlias('@web') ?>/images/service.png"></li>
                                    <li>
                                        <ul class="list-unstyled list-service-title">
                                            <li class="text-uppercase"><b>service 1</b></li>
                                            <li>
                                                <ul class="list-inline list-service-title-ratings">
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                                <p style="margin-top: 15px;"><b>Description</b></p>
                                <p style="margin-top: -5px;"><small>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</small></p>
                            </div>
                            <div class="service-selected">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <ul class="list-unstyled">
                                            <li><i class="fa fa-check txt-white" style="font-size: 45px;"></i></li>
                                            <li class="text-uppercase" style="margin-top: 15px;">service duration</li>
                                            <li><small>5 days</small></li>
                                            <li class="text-uppercase" style="margin-top: 15px;">service cost</li>
                                            <li><small>$150</small></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center">
                                        <img src="images/staff2.png" style="margin-top: 25px;">
                                        <p class="text-uppercase" style="margin-top: 15px;">service provider</p>
                                    </div>
                                </div>
                            </div>
                            <hr class="media-hr">
                            <div class="panel panel-default panel-staff">
                                <div class="panel-heading">
                                    <h4 class="panel-title text-uppercase"><i class="fa fa-user icon-title"></i> <span class="title">staff</span></h4>
                                </div>
                                <div class="panel-body staff-div">
                                    <div class="row">
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff2.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff2.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff2.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff2.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff2.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff2.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8 col-md-8 col-sm-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <ul class="list-inline">
                                <li><h4 class="panel-title text-uppercase"><i class="fa fa-user icon-title"></i> <span class="title">calendar</span></h4></li>
                                <li class="pull-right">
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-calendar">Week</button>
                                        <button type="button" style="border-left: 1px solid #fff;" class="btn btn-calendar dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu menu-cal" role="menu">
                                            <li><a href="#">Month</a></li>
                                            <li><a href="#">Year</a></li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <p class="pull-right"><small>Available Slots</small> <i class="fa fa-square square-green"></i></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div style="width:100%; max-width:100%; display:inline-block;">
                                        <div class="monthly" id="mycalendar"></div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title text-uppercase"><i class="fa fa-clock-o icon-title"></i> <span class="title">timings</span></h4>
                                </div>
                                <div class="panel-body timings-div">
                                    <table class="table table-striped table-responsive table-condensed">
                                        <tr class="text-center">
                                            <td><h5 class="text-uppercase text-muted">available timings</h5></td>
                                            <td><h5 class="text-uppercase text-muted">unavailable timings</h5></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="conatiner-fluid" style="margin-right: 15px; margin-bottom: 20px;">
            <div class="row">
                <div class="col-lg-12">
                    <button class="btn btn-main pull-right" data-toggle="modal" data-target="#modal-ex-customer">Continue</button>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-ex-customer">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body modal-ex">
                        <div class="panel panel-default" style="margin-bottom: 0;">
                            <div class="panel-heading">
                                <h4 class="panel-title text-uppercase"><i class="fa fa-user icon-title"></i> <span class="title">existing customer</span></h4>
                            </div>
                            <div class="panel-body panel-ex">
                                <form method="post" action="">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="User Name">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Password">
                                    </div>
                                    <ul class="list-inline list-submit">
                                        <li class="checkbox forgot-pwd">
                                            <input type="checkbox"> <small> Forgot Password</small>
                                        </li>
                                        <li class="pull-right">
                                            <button type="submit" class="btn btn-main">Submit</button>
                                        </li>
                                    </ul>
                                </form>
                                <button id="modal-ex" class="btn btn-main btn-block" data-toggle="modal" data-target="#modal-new-c" style="margin-top: 70px; padding: 11px 12px;">New Customer</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-new-c">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body modal-ex">
                        <div class="panel panel-default" style="margin-bottom: 0;">
                            <div class="panel-heading">
                                <h4 class="panel-title text-uppercase"><i class="fa fa-user icon-title"></i> <span class="title">new customer</span></h4>
                            </div>
                            <div class="panel-body panel-ex">
                                <form method="post" action="">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="First Name">
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-6 mid">
                                            <input type="text" class="form-control" placeholder="Middle Name">
                                        </div>
                                        <div class="form-group col-lg-6 last">
                                            <input type="text" class="form-control" placeholder="Last Name">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="email" class="form-control" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Postal Address">
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-6 mid">
                                            <input type="text" class="form-control" placeholder="State">
                                        </div>
                                        <div class="form-group col-lg-6 last">
                                            <input type="text" class="form-control" placeholder="City">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-6 mid">
                                            <input type="text" class="form-control" placeholder="Zip Code">
                                        </div>
                                        <div class="form-group col-lg-6 last">
                                            <select class="form-control">
                                                <option selected>Country</option>
                                                <option>Country</option>
                                                <option>Country</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-lg-6 mid">
                                            <input type="text" class="form-control" placeholder="Tel">
                                        </div>
                                        <div class="form-group col-lg-6 mid">
                                            <input type="text" class="form-control" placeholder="Cell Phone #">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Card Number">
                                    </div>
                                    <ul class="list-inline list-payments">
                                        <li>Payment Types</li>
                                        <div class="list-inner">
                                            <li class="radio pull-right cc"><input type="radio" data-toggle="modal" data-target="#card-options" class="radio-payment"> <small>Credit Card</small></li>
                                            <li class="radio pull-right"><input type="radio" data-toggle="modal" data-target="#paypal-options"> <small>Paypal</small></li>
                                        </div>
                                    </ul>
                                    <div class="row">
                                        <div class="col-lg-4 exp"><p>Expiration Date</p></div>
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="mm/dd/yy">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="CSV Code">
                                    </div>
                                    <button class="btn btn-main pull-right" type="button" data-toggle="modal" data-target="#thanku" id="submit-ex-btn">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="thanku">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body text-center">
                        <h3 class="text-thanku">Thank you for the registration</h3>
                        <h4 class="text-thanku">You will soon receive a confirmation email</h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="card-options">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="panel panel-default" style="margin-bottom: 0;">
                            <div class="panel-heading">
                                <button class="close" data-dismiss="modal">&times;</button>
                                <h4 class="panel-title text-uppercase"><i class="fa fa-dollar icon-title"></i> <span class="title">payment screen</span></h4>
                            </div>
                            <div class="panel-body panel-ex">
                                <form method="post" action="">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Full Name">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Card Number">
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Card Number">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="CVV">
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-main" type="button">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="paypal-options">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="panel panel-default" style="margin-bottom: 0;">
                            <div class="panel-heading">
                                <button class="close" data-dismiss="modal">&times;</button>
                                <h4 class="panel-title text-uppercase"><i class="fa fa-dollar icon-title"></i> <span class="title">paypal payment screen</span></h4>
                            </div>
                            <div class="panel-body panel-ex">
                                <form method="post" action="">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="User Name">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Password">
                                    </div>
                                    <button class="btn btn-main" type="button">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>