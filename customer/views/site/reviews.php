<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Staff;
use app\models\Services;

$this->title = 'Reviews';
?>
<div class="review_header">
    <h4>Rate Appointment</h4>
</div>
<div class="clearfix"></div>
<?php
$path   = Yii::getAlias('@web');
$admin_sidepath = substr($path, 0, strrpos($path, "customer"));

?>
<div class="review_outer_box">
    <div class="review_logo_box">

        <img src="<?= $admin_sidepath ?>admin/web/<?= $business_information->logo ?>" alt="<?=$business_information->business_name ?>" />
    </div>
    <div class="review_content_box">
        <p>Thank you for taking the time to rate our services. Your opinion really does count, so if you have a moment please let us know what you think.</p>
    </div>
    <div class="review_gray_box">
<div style="float:left;width:80% ">
        <p>Service Name: <span style="color:#f07a30"><?=  $items['services']['service_name']; ?></span><span style="color:#636363"> @ <?= date('g:i a',strtotime($items->appointment_start_time))  ?> </span></p>
        <p>Employ Name:  <span style="color:#f07a30"><?= Staff::getusername($items->user_id) ?></span><span style="color:#636363"> <?= date('j F , Y',strtotime($items->appointment_date))  ?> </span> </p>
</div>
<div style="float: left; width: 20%; text-align: right;" id="emojicontreview">
    <?php if($review_status=='4'){ ?>
        <img src="<?=Yii::getAlias('@web'); ?>/images/excelant.png" />

 <?php   }else if($review_status=='3'){ ?>
        <img src="<?=Yii::getAlias('@web'); ?>/images/good.png" />
    <?php   }else if($review_status=='2'){ ?>
        <img src="<?=Yii::getAlias('@web'); ?>/images/fair.png" />
    <?php   }else if($review_status=='1'){ ?>
        <img src="<?=Yii::getAlias('@web'); ?>/images/bad.png" />
    <?php   }else if($review_status=='0'){ ?>
        <img src="<?=Yii::getAlias('@web'); ?>/images/poor.png" />
    <?php } ?>


</div>
        <div style="clear:both"></div>
    </div>
    <div class="review_button_box">
        <a data-toggle="modal" href="#" data-target="#review_screen_1" data-dismiss="modal">Write a Review</a>

    </div>


</div>

<div class="modal fade" id="review_screen_1" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header review_model">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Rate Your Experience</h4>
            </div>
            <div class="modal-body">
                <div class="review_content_box">
                    <p>Service Name: <span style="color:#f07a30"><?=  $items['services']['service_name']; ?></span><span style="color:#636363"> @ <?= date('g:i a',strtotime($items->appointment_start_time))  ?> </span></p>
                    <p>Employ Name:  <span style="color:#f07a30"><?= Staff::getusername($items->user_id) ?></span><span style="color:#636363"> <?= date('j F , Y',strtotime($items->appointment_date))  ?> </span> </p>
                </div>
                <div class="review_gray_box emjisicons">
                    <a href="#" <?php if($review_status=='' || $review_status=='4' ){ ?> class="active" <?php } ?> data-toggle="tooltip" data-review="4" title="Excellent"><img src="<?=Yii::getAlias('@web'); ?>/images/excelant.png" /> </a>
                    <a href="#" <?php if($review_status=='3' ){ ?> class="active" <?php } ?> data-toggle="tooltip" data-review="3" title="Good"><img src="<?=Yii::getAlias('@web'); ?>/images/good.png" /> </a>
                    <a href="#" <?php if($review_status=='2' ){ ?> class="active" <?php } ?> data-toggle="tooltip" data-review="2" title="Fair"><img src="<?=Yii::getAlias('@web'); ?>/images/fair.png" /> </a>
                    <a href="#" <?php if($review_status=='1' ){ ?> class="active" <?php } ?> data-toggle="tooltip" data-review="1" title="Bad"><img src="<?=Yii::getAlias('@web'); ?>/images/bad.png" /> </a>
                    <a href="#" <?php if($review_status=='0' ){ ?> class="active" <?php } ?> data-toggle="tooltip" data-review="0" title="Poor"><img src="<?=Yii::getAlias('@web'); ?>/images/poor.png" /> </a>
                </div>
                <div class="review_button_box">
                    <a href="#" data-toggle="modal" data-target="#review_screen_2" id="review_continue" >Continue <span class="glyphicon glyphicon-arrow-right"></span></a>


                </div>
            </div>

        </div>

    </div>
</div>
<!-- Modal -->
<div class="modal fade" id="review_screen_2" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header review_model">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Care To Write A Few Words</h4>
            </div>
            <div class="modal-body">
                <div class="review_content_box">
                   <textarea class="form-control" id="review_text"><?= $review_text ?></textarea>
                </div>

                <div class="review_button_box" style="text-align:right">
                    <a href="#"  id="review_done">Done</a>

                </div>
            </div>

        </div>

    </div>
</div>

<?php

$script1 ='
$(".emjisicons > a").click(function(event){
    event.preventDefault();
    $(".emjisicons > a").removeClass("active");
    $(this).addClass("active");

});
 $(\'[data-toggle="tooltip"]\').tooltip();   
 
 $("#review_continue").click(function(event){
  imgcontent   =  $(".emjisicons > a.active").html();
  imtitle   =  $(".emjisicons > a.active").attr("data-review");  
 $("#emojicontreview").html(imgcontent);
 $("#review_screen_1").modal("hide");
 $.ajax({
    url: "'.Yii::getAlias('@web').'/index.php/site/reviewsupdate",
    type: "post",
  data:{
"apnt_id":'.$apnt_id.',
"feedback":imtitle,"type":1},
success: function (response) {
    if(response==1){
     
    }else {
        alert("not updated");

    }
}
});
 
 });
 $("#review_done").click(function(event){
    event.preventDefault();
     review_text = $("#review_text").val();
     
      $.ajax({
        url: "'.Yii::getAlias('@web').'/index.php/site/reviewsupdate",
        type: "post",
        data:{"apnt_id":'.$apnt_id.',"feedback":review_text,"type":2},
        success: function (response) {
            if(response==1){
             $("#review_screen_2").modal("hide");
            }
            }
            });
     
 });
';
$this->registerJs($script1);
?>

