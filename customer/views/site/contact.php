<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<style>
    .marginclasscontact{
        margin-top: 6px;
    margin-bottom:20px;

    }
</style>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7&appId=273749176329186";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<div id="fb-root"></div>

<div class="container-fluid" style="margin-top: 2%;">
    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title text-uppercase"><i class="fa fa-gear icon-title"></i> <span class="title"><?= $business_information->business_name ?></span></h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <script src="https://apis.google.com/js/platform.js"></script>

                            <iframe src="https://www.youtube.com/embed/WRU83c06luM" width="100%" height="400"></iframe>
                            <!--<img src="<?= Yii::getAlias('@web') ?>/images/utube.png" >-->

                           <span class="pull-right"><div class="g-ytsubscribe " data-channel="News" data-layout="default" data-theme="dark" data-count="hidden"></div></span>
                            <!-- <a href="#" style="margin-top: 3%;" class="btn btn-default btn-subscribe "><i class="fa fa-play-circle"></i> Subscribe</a>-->
                        </div>
                    </div><hr style="margin-top: 30px;">
                    <div class="row">
                        <div class="col-lg-12">


                            <div class="fb-page" data-href="https://www.facebook.com/tabsinnovations"
                                 data-tabs="timeline" data-width="400" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/harbanspura.official/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/harbanspura.official/"></a></blockquote></div>
                           <!--  <img src="<?= Yii::getAlias('@web') ?>/images/utube.png" width="100%">-->
                        </div>
                    </div><hr style="margin-top: 30px;">
                    <!--<div class="row">
                        <div class="col-lg-12">
                            <p>Vendor's Twitter Page</p>
                            <img src="<?= Yii::getAlias('@web') ?>/images/tw.png" width="100%">
                        </div>
                    </div>
                    <hr style="margin-top: 30px;">-->
                </div>
            </div>
        </div>
        <div class="col-lg-8 col-md-8 col-sm-8">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title text-uppercase"><i class="fa fa-user icon-title"></i> <span class="title">Contact Us</span></h4>
                </div>
                <div class="panel-body">
                    <div class="panel-inner-form-container">
                        <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

                        <div class="alert alert-success">
                            Thank you for contacting us. We will respond to you as soon as possible.
                        </div>

                        <p>
                            Note that if you turn on the Yii debugger, you should be able
                            to view the mail message on the mail panel of the debugger.
                            <?php if (Yii::$app->mailer->useFileTransport): ?>
                                Because the application is in development mode, the email is not sent but saved as
                                a file under <code><?= Yii::getAlias(Yii::$app->mailer->fileTransportPath) ?></code>.
                                                                                                                    Please configure the <code>useFileTransport</code> property of the <code>mail</code>
                                application component to be false to enable email sending.
                            <?php endif; ?>
                        </p>
                        <?php endif; ?>

                    <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                            <div class="row">
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <p>Enter your name</p>
                                    <?= $form->field($model, 'name')->label(false) ?>
                                </div>
                                <div class="col-lg-6 col-md-6 col-sm-6">
                                    <p>Enter your email</p>
                                    <?= $form->field($model, 'email')->label(false) ?>
                                </div>
                            </div>
                            <div class="row" style="margin-top: 2%;">
                                <div class="col-lg-12">
                                    <p>Enter your message here</p>
                                    <?= $form->field($model, 'body')->textArea(['rows' => 6])->label(false) ?>
                                   <!-- <div class="form-group">

                                        <textarea class="form-control" placeholder="Your Message here" rows="8"></textarea>
                                    </div>-->
                                </div>
                            </div>
                            <?= Html::submitButton('Send', ['class' => 'btn btn-main btn-save marginclasscontact', 'name' => 'contact-button']) ?>
                            <!--<button type="submit" class="btn btn-main btn-save" style="margin-top: 6px; margin-bottom: 20px;">Send</button>-->
                        <?php ActiveForm::end(); ?><!--AIzaSyBUXvdf6dZLcZODFMXxAbqbxBGPkkDQ4N0
                        AIzaSyDTXCjl8X9cT1vyy2IBV8RtzQdE4XfE2sM -->
                            <div class="row" style="margin-top: 2%;">
                                <div class="col-lg-12">


                                <!--  <script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?v=3&sensor=false"></script>
                                    <div id="googleMap" style="height: 400px;"></div>
                                    <form action="maps.php" method="post">
                                        Select your location
                                        <input type='hidden' name='lat' id='lat'>
                                        <input type='hidden' name='lng' id='lng'>
                                        <input type="submit" value="Submit">
                                    </form> -->
                              <!--      <script>
                                    function initialize() {
                                    var myLatlng = new google.maps.LatLng(51.508742,-0.120850);
                                    var mapProp = {
                                    center:myLatlng,
                                    zoom:5,
                                    mapTypeId:google.maps.MapTypeId.ROADMAP

                                    };
                                    var map=new google.maps.Map(document.getElementById("googleMap"), mapProp);
                                    var marker = new google.maps.Marker({
                                    position: myLatlng,
                                    map: map,
                                    title: 'Hello World!',
                                    draggable:true
                                    });
                                    document.getElementById('lat').value= 51.508742
                                    document.getElementById('lng').value= -0.120850
                                    // marker drag event
                                    google.maps.event.addListener(marker,'drag',function(event) {
                                    document.getElementById('lat').value = event.latLng.lat();
                                    document.getElementById('lng').value = event.latLng.lng();
                                    });

                                    //marker drag event end
                                    google.maps.event.addListener(marker,'dragend',function(event) {
                                    document.getElementById('lat').value = event.latLng.lat();
                                    document.getElementById('lng').value = event.latLng.lng();
                                    alert("lat=>"+event.latLng.lat());
                                    alert("long=>"+event.latLng.lng());
                                    });
                                    }

                                    google.maps.event.addDomListener(window, 'load', initialize);
                                    </script>-->

                                    <!-- <iframe style="background: #ccc;" src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d23636799.78135718!2d-87.854802375!3d43.682454609499196!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2s!4v1461050926046" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>-->
                                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d13615.131920462101!2d74.3214916!3d31.447641!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x21572b0c6148a6f3!2sTABS+Groups+International!5e0!3m2!1sen!2s!4v1474953106839" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
                                </div>
                            </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>














<!--<div class="site-contact">
    <h1><?= Html::encode($this->title) ?></h1>

    <?php if (Yii::$app->session->hasFlash('contactFormSubmitted')): ?>

    <div class="alert alert-success">
        Thank you for contacting us. We will respond to you as soon as possible.
    </div>

    <p>
        Note that if you turn on the Yii debugger, you should be able
        to view the mail message on the mail panel of the debugger.
        <?php if (Yii::$app->mailer->useFileTransport): ?>
        Because the application is in development mode, the email is not sent but saved as
        a file under <code><?= Yii::getAlias(Yii::$app->mailer->fileTransportPath) ?></code>.
        Please configure the <code>useFileTransport</code> property of the <code>mail</code>
        application component to be false to enable email sending.
        <?php endif; ?>
    </p>

    <?php else: ?>

    <p>
        If you have business inquiries or other questions, please fill out the following form to contact us. Thank you.
    </p>

    <div class="row">
        <div class="col-lg-5">
            <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'email') ?>
                <?= $form->field($model, 'subject') ?>
                <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>
                <?= $form->field($model, 'verifyCode')->widget(Captcha::className(), [
                    'template' => '<div class="row"><div class="col-lg-3">{image}</div><div class="col-lg-6">{input}</div></div>',
                ]) ?>
                <div class="form-group">
                    <?= Html::submitButton('Submit', ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>

    <?php endif; ?>
</div>-->
