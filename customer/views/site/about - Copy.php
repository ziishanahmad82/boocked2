<?php
use yii\helpers\Html;

/* @var $this yii\web\View */
$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
<?php // foreach($business_information as $business){ ?>
    <h2><?= $business_information->business_name ?></h2>
    <?= $business_information->address.' '.$business_information->city.' '.$business_information->state.' '.$business_information->country.' '.$business_information->zip ?>
    <br/><strong>Call</strong> : <?= $business_information->business_phone ?>
   <?php // } ?>
    <br/>
    <?= $business_information->business_description ?>


    <h3>Staff</h3>
    <?php foreach ($staff as $staffs){ ?>
       <p><?= $staffs->username ?></p>
    <?php } ?>
    <code><? //= __FILE__ ?></code>
</div>
