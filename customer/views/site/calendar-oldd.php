<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;

?>

<div class="col-sm-12" style="background:#fc7600;padding:15px" xmlns="http://www.w3.org/1999/html">
    <span style="color: #ffffff;display: block; cursor: pointer" onclick="changeCustomerService()"><?php echo \app\models\Services::showServicename($service); ?></span>

</div>
<br/>
<br/>
<br/>

<?php
if(!isset($staff)){
    $staff=0;


}
$ajax_path=Yii::getAlias('@web');
$JSDayRender = <<<EOF
    function(date, cell){
    curdate = new Date();
     var today = new Date().toJSON().slice(0,10);
     var check = date._d.toJSON().slice(0,10);
     if(check > today){
      $.ajax({
    url: "$ajax_path/index.php/site/checkavailableslots",
    type: 'POST',
    data:{'date':check,'service':$service,'staff':$staff  },
  
    dataType: 'json',
    success: function(response){
        if(response>0){
          cell.css("background-color", "green");
           cell.css("color", "#fff");
            cell.text("Available");
        }else {
          cell.css("background-color", "red");
          cell.css("color", "#fff");
            cell.text("Unavailable");
        }
        }
        });
        }
   
       
      if(check < today){
            cell.css("background-color", "lightgray");
            cell.html("<span style='display:none;'>past</span>");
        }
   
 //   if (date < curdate){
   
 // cell.addClass("disabled");
   //     cell.css("background-color","red");
    //    cell.text("Unavailable");
    //    cell.addClass("disabled");
  
   // }
    }
EOF;
$JSEventClick = <<<EOF
    function(date, cell){
    var box_text = $(this).text();
    if(box_text=='Unavailable' || box_text=='past' ){
      
        }else {
        alert('asdddd');
        
        }
    }
EOF;

$JSDayClick = <<<EOF
    function(date, cell){
    var box_text = $(this).text();
      var today = new Date().toJSON().slice(0,10);
     var check = date._d.toJSON().slice(0,10);
     $.ajax({
    url: "$ajax_path/index.php/site/slots",
    type: 'POST',
    data:{'date':check,'service':$service ,'staff':$staff  },
    success: function(response){
            $('#slotbox').html(response);
            $('#calendar_cont').addClass('hidden');
             $('#slotbox').removeClass('hidden');
    },
      
        });
    }
EOF;
?>




<div class="col-sm-12">
    <div id="calendarbox">
        <?php // print_r(Yii::$app->user->identity);
        ?>

        <?= yii2fullcalendar\yii2fullcalendar::widget(array(
            'header' => [
                'left' => 'prev,next today',
                'center' => 'title',
                //   'right' => 'agendaDay,agendaWeek,month',
                'slotLabelInterval' => '00:45:00',
            ],

            'clientOptions' => [
                'selectable' => true,
                'selectHelper' => true,
                'droppable' => true,
                'editable' => true,
                'slotLabelFormat' => 'h(:mm)a',
                //'drop' => new JsExpression($JSDropEvent),
                //'select' => new JsExpression($JSCode),
                //   'dayRender' => new JsExpression($JSDayRender),
                'dayClick' => new JsExpression($JSDayClick),
                //   'eventClick'=> new JsExpression($JSslotClick),
                // 'viewRender' => new JsExpression($JSviewRender),
                'defaultDate' => date('Y-m-d'),
                'weekends' => false,
                'slotDuration' => '1:30:00',
                'dayRender' => new \yii\web\JsExpression($JSDayRender),
                // 'Duration'=>'00:45:00',


                //'eventRender' => new JsExpression($JSEventRender),
            ],
            // 'events'=> $events,
            //'ajaxEvents' => Url::toRoute(['/customer-reserved-timeslots/jsoncalendar'])
        ));



        ?>
    </div>
</div>
<div class="clearfix"></div>