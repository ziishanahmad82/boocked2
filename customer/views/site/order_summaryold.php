<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
  
?>
<?php if($login==0){

   foreach ($model as $service) {

       $slotstr = strtotime($slot);
       ?>
       <?php $form = ActiveForm::begin(['id'=>'book_customer_apointment']); ?>
       <input type="hidden" value="<?= date("Y-m-d", $slotstr) ?>" name="Appointments[appointment_date]" >
       <input type="hidden" value="<?php echo $staff ?>" name="Appointments[user_id]" >
       <input type="hidden" value="<?=$service->service_id ?>" name="Appointments[service_id]" >
       <input type="hidden" value="<? echo Yii::$app->user->identity->customer_id; ?>" name="Appointments[customer_id]" >
       <input type="hidden" value="<?= date("H:i:s",$slotstr) ?>" name="Appointments[appointment_start_time]" >
       <?php $form = ActiveForm::end(); ?>
<div id="order_summary" >
       <div class="col-sm-12" style="background:#fc7600;padding:15px" xmlns="http://www.w3.org/1999/html" >
           <span style="color: #ffffff;display: block; cursor: pointer" onclick="changeCustomerService()"><?=$service->service_name ?></span>

       </div>
       <div class="col-sm-12" style="background:#ffebdd">
           <h3> Order Summary </h3>
           <?php // echo $slot;

           echo date("M d Y l", $slotstr).'<br/>';
?>
       <div class="col-xs-6" style="width:48%"><?php
           echo date("h:i:s A",$slotstr).'              '.$service->service_name;
                ?>
       </div>
           <div class="col-xs-6" style="width:48%"><?php
               echo $service->service_price.'<br/><br/> <strong>Grand Total :</strong>'.$service->service_price;
               ?>

               <br/>
               <br/>
               <br/>
               <br/>
               <br/>
               <br/>
               <button class="btn btn-success" onclick="bookorder()">Book Now</button>

           </div>

       </div>
</div>


<div class="hidden" id="thankyou">
     <p>Your Booking is confirmed<br/>
       The booking details will be sent to your email.<br/>

       <?php echo date("M d Y l", $slotstr) ?> @ <?php echo date("h:i:s A",$slotstr); ?><br/>
       service common name:<br/>
       <?php  echo date("h:i:s A",$slotstr).'              '.$service->service_name; ?><br/>
       Confirmed appointments can be cancelled 24 hours prior to the appointment time.<a href="#" onclick="changeCustomerService()">Book More Appointment</a>
     </p>
</div>






       <?php
   }
   } else if($login==1){
    foreach ($model as $service) {

        ?>

        <div class="col-sm-12" style="background:#fc7600;padding:15px" xmlns="http://www.w3.org/1999/html">
            <span style="color: #ffffff;display: block; cursor: pointer" onclick="changeCustomerService()"><?=$service->service_name ?></span>

        </div>
        <div class="col-sm-12" style="background:#ffebdd">

           <span><a href="<?= Yii::getAlias('@web') ?>/index.php/site/bookinglogin" style="color:#fff" onclick="customerSignupLogin(event,this)" class="btn btn-success"  >Login</a>  <a href="<?= Yii::getAlias('@web') ?>/index.php/site/bookingsignup"  onclick="customerSignupLogin(event,this)" style="color:#fff" class="btn btn-success">Signup</a></span>
            
            
        </div>
        <div class="clearfix"></div><br/>

        <?php
    }
} 
?>