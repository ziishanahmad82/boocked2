<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\widgets\Pjax;
use app\models\Reviews;
/* @var $this yii\web\View */

$this->title = 'Customer Schedule';
$request = Yii::$app->request;
$get = $request->get();
$session = Yii::$app->session;
if(@Yii::$app->user->identity->id)
{ // AND stristr(Yii::$app->user->identity->user_group, 'thirdparty-')
   // $session["3rd_user_id"]=Yii::$app->user->identity->id;
   // $session["3rd_user_name"]=Yii::$app->user->identity->username;
    //$session["3rd_user_group"]=Yii::$app->user->identity->user_group;
}
$ajax_path=Yii::getAlias('@web');
?>
<style>
  /*  .servicecheckbox {
        top:40%;
        left:40%;
        position:absolute;
        display:none;
        z-index:10001;
    }
    .services_customercontbox:hover .service-select{
        display:block !important;
        z-index:10001;


    } */

</style>
<?php
$review_names = Yii::$app->session->get('bk_show_customer_name_reviews');


?>
<!--<div class="container-fluid" style="margin-top: 2%;">
    <center>
        <form action="" method="get" class="form-inline">
        <label>Choose Business</label>
        <select class="form-control">
            <option>a</option>
            <option>a</option>
            <option>a</option>
            <option>a</option>

        </select>
        </form>
    </center>
</div>-->
<div class="container-fluid" style="margin-top: 2%;">
    <div class="row">
        <div class="<?php if(Yii::$app->session->get('bk_staff_member_customer_visibility')!='0'){ ?> col-sm-9 <?php }else { echo 'sol-sm-12'; } ?> main-services-container">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title text-uppercase"><i class="fa fa-gear icon-title"></i> <span class="title">services</span></h4>
                </div>
                <div class="panel-body panel-scroller">
                    <div class="panel-inner-services-container">
                        <div class="row">
                            <div class="col-lg-4 col-lg-offset-8 col-md-6 col-md-offset-6 col-sm-6 col-sm-offset-6">
                                <div class="form-group">
                                    <input type="text" placeholder="Search services here"  id="service_search" class="form-control field-search">
                                </div>
                            </div>
                        </div>

                          <!--  <div class="form-group">
                                <label>
                                    <input type="checkbox" value="<? //=  $service->service_id ?>" name="service_name" class="service_checkbox" > <? //=  $service->service_name ?><br>
                                </label>
                                <br>
                            </div>-->
                        <?  $path   = Yii::getAlias('@web');
                            $admin_sidepath = substr($path, 0, strrpos($path, "customer"));
                     ?>

                          <?php foreach ($services as $service){  ?>
                       <div class="services_listbox">
                           <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <ul class="list-inline">
                                    <li><div class="squaredTwo">
                                            <input type="checkbox"  value="<?=  $service->service_id ?>" class="service_checkbox servicecheckbox" name="service_name" id="squaredTwo<?=  $service->service_id ?>"  />
                                            <label for="squaredTwo<?=  $service->service_id ?>"></label>
                                        </div></li>
                                    <li class="stars">
                                        <ul class="list-unstyled list-service-title">
                                            <li class="text-uppercase"><b class="service_namecont"><?=  $service->service_name ?></b></li>
                                            <li>
                                                <ul class="list-inline list-service-title-ratings">
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <div class="pull-right">

                                    <?php if(Yii::$app->session->get('bk_show_service_time_customer')!='0'){ ?> <p><b><?=  $service->service_duration ?> Minutes</b></p> <?php } ?>
                                    <?php if(Yii::$app->session->get('bk_show_service_cost_customer')!='0'){ ?><p class="txt-price"><small><?= Yii::$app->session->get('bk_currency'); ?> <?=  $service->service_price ?></small></p> <?php } ?>
                                </div>
                            </div>
                        </div>
                                <div class="row" style="margin-top: 3%;">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 media-col-slider services_customercontbox">
                                <div class="carousel slide" data-ride="carousel" id="car">

                                   <!-- <div class="service-select"> <input type="checkbox"></div>-->


                                       <!-- <div class="item active service-container">
                                            <img src="<? //= Yii::getAlias('@web') ?>/images/service-1.png" width="100%;">
                                            <div class="service-select">
                                                <input type="checkbox">
                                            </div>
                                        </div>-->
                                        <?php if($service->service_car_image1!="" || $service->service_car_image2!="" || $service->service_car_image3!="" ) { ?>
                                            <ol class="carousel-indicators">
                                                <li class="active" data-slide-to="0" data-target="#car"></li>
                                                <li data-slide-to="1" data-target="#car"></li>
                                                <li data-slide-to="2" data-target="#car"></li>
                                            </ol>
                                    <div class="carousel-inner">
                                            <?php if ($service->service_car_image1 != "") { ?>
                                                <div class="item active service-container">
                                                    <img src="<?= $admin_sidepath . 'admin/web/' . $service->service_car_image1 ?>" width="100%;">

                                                </div>
                                            <?php } ?>
                                            <?php if ($service->service_car_image2 != "") { ?>
                                                <div class="item service-container">
                                                    <img src="<?= $admin_sidepath . 'admin/web/' . $service->service_car_image2 ?>" width="100%;">

                                                </div>
                                            <?php }
                                            if ($service->service_car_image3 != "") { ?>
                                                <div class="item service-container">
                                                    <img
                                                        src="<?= $admin_sidepath . 'admin/web/' . $service->service_car_image3 ?>"
                                                        width="100%;">

                                                </div>

                                            <?php } ?>
                                    </div>
                                      <?php  }else { ?>
                                    <div class="carousel-inner">
                                            <div class="item active service-container">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/no-image.png" width="100%;">

                                        </div>
                                  </div>
                                         <?php
                                        }
                                        ?>

                                </div>
                            </div>
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 media-col-video">
                                <div class="carousel slide" data-ride="carousel" id="car-video">

                                    <div class="carousel-inner">
                                        <div class="item active">

                                           <?php if(!empty($service->service_video)){ ?>
                                            <video width="100%" controls>
                                                <source src="<?= $admin_sidepath.'admin/web/'.$service->service_video ?>" type="video/mp4">
                                            </video>
                                            <?php }else {
                                               ?>
                                               <img src="<?= Yii::getAlias('@web') ?>/images/no-video.png"  style="width:100%;" />
                                               <?php
                                           } ?>
                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6 col-md-6 col-sm-6 media-col-desc">
                                <div class="container-description">
                                    <p>Description</p>
                                    <p style="margin-top: -5px;"><small><?=  $service->service_description ?></small></p>
                                    <?php
                                    $reviews_content = Reviews::find()->where(['service_id'=>$service->service_id ])->all();
                                    ?>
                                    <p>Customer Reviews: <?= count($reviews_content) ?> reviews <small></small></p>
                                    <?php

                                    if(isset($reviews_content)) {
                                        foreach ($reviews_content as $review_content) {

                                            ?>
                                            <div class="row">
                                                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 container-media-1">
                                                   <!-- <img src="<?= Yii::getAlias('@web') ?>/images/service.png" width="30">-->
                                                    <img src="<?= Yii::getAlias('@web') ?>/images/good.png" width="30">
                                                </div>
                                                <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 container-media-11">
                                                    <small><?= $review_content->review_text ?> <?php if($review_names!=0){ echo' - '.\app\models\Users::getcustomername($review_content->customer_id); } ?></small>
                                                </div>
                                            </div>
                                            <hr>
                                            <?php
                                        }
                                    }
                                        ?>

                                   <!-- <p>Description</p>
                                    <p style="margin-top: -5px;"><small>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</small></p>
                                    <p>Customer Reviews: 11 reviews <small> (showing 4)</small></p>
                                    <div class="row">
                                        <div class="col-lg-1 col-md-1 col-sm-1 col-xs-1 container-media-1"><img src="<?= Yii::getAlias('@web') ?>/images/service.png" width="30"></div>
                                        <div class="col-lg-11 col-md-11 col-sm-11 col-xs-11 container-media-11"><small>Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet Lorem ipsum dolor sit amet</small></div>
                                    </div>-->
                                </div>
                            </div>
                        </div>
                        <hr style="margin-top: 30px;">
                       </div>
                          <? } ?>

                        <a href="book.html" class="btn btn-main pull-right" onclick="nextcalendar(event)">Continue</a>
                    </div>
                </div>
            </div>
        </div>
        <?php if(Yii::$app->session->get('bk_staff_member_customer_visibility')!='0'){ ?>
        <div class="col-lg-3 col-md-3 col-sm-3 main-staff-container">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title text-uppercase"><i class="fa fa-user icon-title"></i> <span class="title">staff</span></h4>
                </div>
                <div class="panel-body panel-scroller-staff">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <input type="text" placeholder="Search staff here.." id="search_staff" class="form-control field-search">
                            </div>
                        </div>
                    </div>
                    <div class="staffresults">
                    <?php foreach ($staffs as $staff){  ?>
                        <div class="search_staff_box ">
                            <input type="checkbox" value="<?=  $staff->id ?>" name="staff_name" class="staff_checkbox" id="staff_checkbox<?=  $staff->id ?>" style="display:none" >
                            <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-3 container-staff-img">
                            <?php
                            if($staff->user_profile_image!=""){?>
                                <img src="<?= $admin_sidepath . 'admin/web/' . $staff->user_profile_image ?>" class="<?= $staff->username ?>" style="width:98px;">
                            <?php }else{ ?>
                            <img src="<?= Yii::getAlias('@web') ?>/images/staff12.png" class="<?= $staff->username ?>" style="width:98px;">
                            <?php } ?>
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-9 container-staff-desc">
                            <div class="staff-desc">
                                <p class="staff-name"><?= $staff->username ?></p>
                                <small>Designation</small>
                                <ul class="list-inline list-staff-ratings">
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star-half-o"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <hr>
                        </div>
                    <?php } ?>
                    </div>
                  <!--  <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-3 container-staff-img">
                            <img src="<?= Yii::getAlias('@web') ?>/images/staff-us.png" class="img-staff-member">
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-9 container-staff-desc">
                            <div class="staff-desc">
                                <p class="staff-name">Adam Smith</p>
                                <small>Designation</small>
                                <ul class="list-inline list-staff-ratings">
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star-half-o"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-3 container-staff-img">
                            <img src="<?= Yii::getAlias('@web') ?>/images/staff-us.png" class="img-staff-member">
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-9 container-staff-desc">
                            <div class="staff-desc">
                                <p class="staff-name">Adam Smith</p>
                                <small>Designation</small>
                                <ul class="list-inline list-staff-ratings">
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star-half-o"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-3 container-staff-img">
                            <img src="<?= Yii::getAlias('@web') ?>/images/staff-us.png" class="img-staff-member">
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-9 container-staff-desc">
                            <div class="staff-desc">
                                <p class="staff-name">Adam Smith</p>
                                <small>Designation</small>
                                <ul class="list-inline list-staff-ratings">
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star-half-o"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-3 container-staff-img">
                            <img src="images/staff-us.png" class="img-staff-member">
                        </div>
                        <div class="col-lg-7 col-md-7 col-sm-7 col-xs-9 container-staff-desc">
                            <div class="staff-desc">
                                <p class="staff-name">Adam Smith</p>
                                <small>Designation</small>
                                <ul class="list-inline list-staff-ratings">
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star"></i></li>
                                    <li><i class="fa fa-star-half-o"></i></li>
                                </ul>
                            </div>
                        </div>
                    </div>-->

                </div>
            </div>
        </div>
        <?php } ?>
    </div>
</div>
<div id="hiddenformset"></div>



































































<?php
$script1 = "$('.service_checkbox').click(function(){
    $('.service_checkbox').each(function(){
    $(this).prop('checked', false);
});
    $(this).prop('checked', true);
     serviceval = $(this).val();
     $.ajax({
            url: '".$ajax_path."/index.php/site/relatedstaff',
            type:'POST',
            data:{'serviceval':serviceval},
             success: function(response){
                $('.staffresults').html(response);
               
            }
            
            });
});


var rows = $('.services_listbox');
$('#service_search').keyup(function() {
//console.log($(this).children('.service_namecont').text());
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
    //console.log(val);
    rows.show().filter(function() {
  var text = $(this).find('.service_namecont').text().replace(/\s+/g, ' ').toLowerCase();
        console.log(text);
       return !~text.indexOf(val);
    }).hide();
});
var staffrows = $('.search_staff_box');
$('#search_staff').keyup(function() {
    var val = $.trim($(this).val()).replace(/ +/g, ' ').toLowerCase();
    staffrows.show().filter(function() {
  var text = $(this).find('.staff-name').text().replace(/\s+/g, ' ').toLowerCase();
       return !~text.indexOf(val);
    }).hide();
});

$('div.staffresults').on('click', '.search_staff_box', function() {
  
//$('.search_staff_box').click(function(event){
        $('.search_staff_box').removeClass('active-staff');
        $(this).addClass('active-staff');
        $('.search_staff_box .staff_checkbox').prop('checked', false);
        $(this).children('.staff_checkbox').prop('checked', true);
        
});


";
$this->registerJs($script1);
?>
<script>
    function slotselection(xor,event,service,staff){
        event.preventDefault();
        var slot = $(xor).children('span').text();
         var service = $('input[name=service_name]:checked').val();


        alert(slot);

        $.ajax({
            url: "<?= $ajax_path?>/index.php/site/slotbooking",
            type:"POST",
            data:{'service':service,'slot':slot,'staff':staff},
            success: function(response){
                $('#loginbox').html(response);
                $('#slotbox').addClass('hidden');
            }

        });

    }

    function customerSignupLogin(event,xor){
        event.preventDefault();
         var target_url = $(xor).attr('href');
       $('#loginSignupcont').load(target_url);

    }
    function nextcalendar(event){
        event.preventDefault();
        var service = $('input[name=service_name]:checked').val();
        if(!service){
            alert('Please select atleast one service');
            return false;
        }
        var staff = $('input[name=staff_name]:checked').val();

        form = '<input type="hidden" name="<?= Yii::$app->request->csrfParam; ?>" value="<?= Yii::$app->request->csrfToken; ?>" /><input type="hidden" name="service" value="'+service+'"><input type="hidden" name="staff" value="'+staff+'">';
    $('<form action="<?= $ajax_path ?>/index.php/site/booking" method="POST">'+form+'</form>').appendTo('#hiddenformset').submit();



     /*   if(service) {
            $('#select_service_cont').addClass('hidden');
            $('#calendar_cont').removeClass('hidden');
            $.ajax({
              //  url: "<= $ajax_path?>/index.php/site/loadcalendar",
                url: "<= $ajax_path?>/index.php/site/booking",
                type:"POST",
                data:{'service':service,'staff':staff},
                success: function(response){
                    $('#calendar_cont').html(response);
                }

            });
          //  $('#calendar_cont').load('<= $ajax_path?>/index.php/site/loadcalendar');
            $('.fc-today-button').click();
        }else {
            alert('Please select atleast One Service');

        }  */
    }
    function changeCustomerService(){
        $.pjax.reload({container:"#change_service"});


    }
    function bookorder(){
         var formdata = $('#book_customer_apointment').serialize();
        $.ajax({
            url: "<?= $ajax_path?>/index.php/site/bookingappointment",
            type:"POST",
            data:formdata,
            //,
            success: function(response){
                if(response==1) {
                    $('#order_summary').addClass('hidden');
                    $('#thankyou').removeClass('hidden');

                }
            },

        });

    }
    function backtocalendar(){
        $('#slotbox').addClass('hidden');
        $('#calendar_cont').removeClass('hidden');

    }

</script>
