<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\BusinessInformation;
use app\models\Countries;
use app\models\States;
use app\models\Cities;
use app\models\Staff;
  
?>
<?php if($login==0){
?>
    <?php if(Yii::$app->session->get('bk_rules_allow_customer_booking')=='0'){?>
         <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body text-center">
                    <div class="online_booking_close" style="margin:-15px">
                    <div class="col-sm-12" style="background:#eaeaea;padding:15px;" xmlns="http://www.w3.org/1999/html" >
                        <span style="display: block; cursor: pointer;text-align:center;" onclick="changeCustomerService()"><!--<i class="fa fa-user" style="margin-right:6px;color: #FC7600;"></i>--> To book an appointment you can contact us at the address below.</span>

                    </div>
                    <div class="col-sm-12">
                        <?
                        $subdomain = str_replace('.boocked.com','',$_SERVER['HTTP_HOST']);
                        $business_information  = BusinessInformation::find()->where(['business_subdomain'=>$subdomain])->one();
                        if(empty($business_information)){
                            return 'Business not available';
                        }
                        $business_id = $business_information->business_id;
                        $business_information = BusinessInformation::findOne($business_id); ?>
                        <h4 style="color:#fe7500 "><?= $business_information->business_name ?></h4>
                        <p><?= $business_information->address.', '.Cities::getCity_id($business_information->city).', '.States::getState_id($business_information->state).', '.Countries::getCountry_id($business_information->country).' - '.$business_information->zip ?></p>
                        <p>Contact Number:<span style="color:#fe7500 " ><?= $business_information->business_phone ?></span></p>
                        <?php $staffs = Staff::find()->where(['business_id'=>$business_id,'staff_status'=>1,'user_group'=>'admin'])->all(); ?>
                        <p style="margin-bottom:30px">Email: <span style="color:#fe7500 " ><? foreach($staffs as $staffs){ echo $staffs->email.',  ';  } ?></span> </p>

                    </div>
                    <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

        <?php }else { ?>
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body text-center">


  <?php
    foreach ($model as $service) {

        $slotstr = strtotime($slot);
        ?>
        <?php $form = ActiveForm::begin(['id' => 'book_customer_apointment']); ?>
        <input type="hidden" value="<?= date("Y-m-d", $slotstr) ?>" name="Appointments[appointment_date]">
        <input type="hidden" value="<?php echo $staff ?>" name="Appointments[user_id]">
        <input type="hidden" value="<?= $service->service_id ?>" name="Appointments[service_id]">
        <input type="hidden" value="<? echo Yii::$app->user->identity->customer_id; ?>"
               name="Appointments[customer_id]">
        <input type="hidden" value="<?= date("H:i:s", $slotstr) ?>" name="Appointments[appointment_start_time]">
        <?php $form = ActiveForm::end(); ?>
        <div id="order_summary" style="margin:-15px">
            <div class="col-sm-12" style="background:#eaeaea;padding:15px" xmlns="http://www.w3.org/1999/html">
                <span style="display: block; cursor: pointer;text-align:left" onclick="changeCustomerService()"><i
                        class="fa fa-user" style="margin-right:6px;color: #FC7600;"></i> Booking Summary</span>

            </div><!--#ffebdd-->
            <div class="col-sm-12" style="padding-top: 8px;">
                <!--<h3> Order Summary </h3>
           <? // =$service->service_name  ?> --> <?php // echo $slot;

                //  echo date("M d Y l", $slotstr).'<br/>';
                ?>
                <div class="col-xs-6" style="width:60%;text-align:left;">
                    <h5 style="text-align:left;color:#fe7500 ">Services Details</h5>
                    <h5 style="text-align:left"><strong><?= $service->service_name ?></strong></h5>
                    <p>
                        <small><?= date("h:i:s A", $slotstr) ?></small>
                        <br/>
                        <small><?= date("D, M d, Y", $slotstr) ?></small>
                    </p>
                    <?php

                    //  echo date("h:i:s A",$slotstr).'              '.$service->service_name;
                    ?>
                </div>
                <div class="col-xs-6" style="width:38%;text-align:right;padding-top:40px;padding-right: 5px;"><?php
                    echo $service->service_price . '  ' . Yii::$app->session->get('bk_currency') . ' <br/><br/>'; ?>
                </div>
                <div class="col-xs-12">
                    <div class="col-xs-6 pull-right" style="width:38%;text-align:right;padding-right: 0px;"><strong>Grand
                            Total
                            :</strong><?= $service->service_price . ' ' . Yii::$app->session->get('bk_currency') ?>


                        <br/>
                        <button class="btn btn-main" onclick="bookorder()" style=" margin: 27px 0;">Book Now</button>
                    </div>
                </div>

            </div>
            <div class="clearfix"></div>
        </div>


        <div class="hidden" id="thankyou" style="margin:-15px">
            <div class="col-sm-12" style="background:#eaeaea;padding:15px" xmlns="http://www.w3.org/1999/html">
                <span style="display: block; cursor: pointer;text-align:left" onclick="changeCustomerService()"><i
                        class="fa fa-user" style="margin-right:6px;color: #FC7600;"></i> Booking Confirmed</span>

            </div>
            <div class="col-sm-12">
                <p></p>

                <p style="line-height: 30px;margin: 32px 0;">Your Booking is confirmed<br/>
                    The booking details will be sent to your email.<br/>

                    <span style="color:#fe7500 "><?php echo date("D, M d, Y", $slotstr) ?></span> @ <span
                        style="color:#fe7500 "><?php echo date("h:i:s A", $slotstr); ?></span><br/>
                    service common name: <span style="color:#fe7500 "><?= $service->service_name; ?></span>
                    <?php //  echo date("h:i:s A",$slotstr).'              '.$service->service_name; ?><br/>
                    Confirmed appointments can be cancelled 24 hours prior to the appointment time.<br/></p>
                <p style="margin-bottom:16px">
                    <a href="#" class="btn btn-main" onclick="changeCustomerService()">Book More Appointment</a>
                </p>
            </div>
            <div class="clearfix"></div>
        </div>


        </div>
        </div>
        </div>


        <?php
    }
   }
   } else if($login==1){

    foreach ($model as $service) {
        ?>

        <div class="form">
            <header id="log">Admin Login</header>
            <fieldset>

                <? //= $form->field(model1, 'username') ?>

                <? //= $form->field(model1, 'password')->passwordInput() ?>

                <? // = yii\authclient\widgets\AuthChoice::widget(['baseAuthUrl' => ['site/auth']]) ?>


                <div class="col12"><? //= Html::submitButton('Login', ['class' => 'bt btn btn-primary', 'name' => 'login-button']) ?></div>

            </fieldset>


        </div>


        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body modal-ex">
                    <div class="panel panel-default" style="margin-bottom: 0;">
                        <div class="panel-heading">
                            <h4 class="panel-title text-uppercase"><i class="fa fa-user icon-title"></i> <span class="title">existing customer</span></h4>
                        </div>
                        <div class="panel-body panel-ex">
                            <?php
                            $form = ActiveForm::begin([
                                'id' => 'login-form',
                                'options' => ['class' => 'form-horizontal'],
                                //    'fieldConfig' => [
                                //         'template' => "<div class=\"row\"><section><label class=\"col-sm-4 txt\">{label}:</label><div class=\"col-sm-8\"><label class=\"input\">{input}</label></div></section></div>\n<div class=\"col-lg-8\">{error}</div>"

                                //        ,
                                //       'labelOptions' => ['class' => 'col-lg-1 control-label'],
                                //   ],
                            ]);
                            ?>
                                <?= $form->field($model1, 'username')->input('username', ['placeholder' => "Enter Your Email"])->label(false) ?>
                                <?= $form->field($model1, 'password')->passwordInput(['placeholder' => "Password"])->label(false) ?>
                                <!--<div class="form-group">
                                    <input type="text" class="form-control" placeholder="User Name">
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Password">
                                </div>-->
                                <ul class="list-inline list-submit">
                                    <li class="checkbox forgot-pwd">
                                        <input type="checkbox"> <small> Forgot Password</small>
                                    </li>
                                    <li class="pull-right">
                                        <button type="submit" class="btn btn-main">Submit</button>
                                    </li>
                                </ul>

                            <?= yii\authclient\widgets\AuthChoice::widget([
                                'baseAuthUrl' => ['site/auth']
                            ]) ?>
                                <?php ActiveForm::end(); ?>
                            <button id="modal-ex" class="btn btn-main btn-block" data-toggle="modal" data-target="#modal-new-c" style="margin-top: 70px; padding: 11px 12px;">New Customer</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

     <!--   <div class="col-sm-12" style="background:#fc7600;padding:15px" xmlns="http://www.w3.org/1999/html">
            <span style="color: #ffffff;display: block; cursor: pointer" onclick="changeCustomerService()"><? //=$service->service_name ?></span>

        </div>
        <div class="col-sm-12" style="background:#ffebdd">

           <span><a href="<? //= Yii::getAlias('@web') ?>/index.php/site/bookinglogin" style="color:#fff" onclick="customerSignupLogin(event,this)" class="btn btn-success"  >Login</a>  <a href="<?= Yii::getAlias('@web') ?>/index.php/site/bookingsignup"  onclick="customerSignupLogin(event,this)" style="color:#fff" class="btn btn-success">Signup</a></span>
            
            
        </div>-->
        <div class="clearfix"></div><br/>

        <?php
    }
} 
?>