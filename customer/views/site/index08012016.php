<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
/* @var $this yii\web\View */

$this->title = 'Customer Schedule';
$request = Yii::$app->request;
$get = $request->get();
$session = Yii::$app->session;
if(@Yii::$app->user->identity->id)
{ // AND stristr(Yii::$app->user->identity->user_group, 'thirdparty-')
   // $session["3rd_user_id"]=Yii::$app->user->identity->id;
   // $session["3rd_user_name"]=Yii::$app->user->identity->username;
    //$session["3rd_user_group"]=Yii::$app->user->identity->user_group;
}
$ajax_path=Yii::getAlias('@web');
?>
<?php
if(isset( $_SESSION['book_service_id']) && isset( $_SESSION['book_slot']) ){
    $scriptredirect ="
 $.ajax({
            url: '".$ajax_path."/index.php/site/slotbooking',
            //    type:post,
            data:{'service':".$_SESSION['book_service_id'].",'slot':'".$_SESSION['book_slot']."'},
            success: function(response){
                $('#loginbox').html(response);
            }

        });     
  
            
            ";
    $this->registerJs($scriptredirect);


}

?>
<div id="select_service_cont">
<div class="col-sm-12" style="background:#fc7600;padding:15px" xmlns="http://www.w3.org/1999/html">
    <span style="color: #ffffff;">Service Name <? //=$service->service_name ?></span>

</div>
<div class="col-sm-12" style="background:#ffebdd">
<form action="http://localhost/boocked/customer/web/index.php/site/" method="post">
<div class="col-sm-6">
<h3>Services</h3>

<?php foreach ($services as $service){  ?>
     <input type="radio" value="<?=  $service->service_id ?>" name="service_name" > <?=  $service->service_name ?><br>

<? } ?>
</div>
<div class="col-sm-6" id="customer_services_list" >
    <h3>Staff</h3>
    <?php foreach ($staffs as $staff){  ?>
        <input type="radio" value="<?=  $staff->id ?>" name="staffs" > <?=  $staff->username ?><br>

    <? } ?>
</div>
    <div class="clearfix"></div>
    <input type="submit" value="Submit" name="submit" />
    </form>
<div class="clearfix"></div>
    <button class="btn btn-success" onclick="nextcalendar()" > Next </button>
    </div>
</div>
<div class="clearfix"></div>
<?php
$JSDayRender = <<<EOF
    function(date, cell){
    curdate = new Date();
     var today = new Date().toJSON().slice(0,10);
     var check = date._d.toJSON().slice(0,10);
     if(check > today){
      $.ajax({
    url: "$ajax_path/index.php/site/checkavailableslots?date="+check,
    dataType: 'json',
    success: function(response){
        if(response>0){
          cell.css("background-color", "green");
            cell.text("Available");
        }else {
          cell.css("background-color", "red");
            cell.text("Unavailable");
        }
        }
        });
        }
   
       
      if(check < today){
            cell.css("background-color", "lightgray");
            cell.html("<span style='display:none;'>past</span>");
        }
   
 //   if (date < curdate){
   
 // cell.addClass("disabled");
   //     cell.css("background-color","red");
    //    cell.text("Unavailable");
    //    cell.addClass("disabled");
  
   // }
    }
EOF;
$JSEventClick = <<<EOF
    function(date, cell){
    var box_text = $(this).text();
    if(box_text=='Unavailable' || box_text=='past' ){
      
        }else {
        alert('asdddd');
        
        }
    }
EOF;

$JSDayClick = <<<EOF
    function(date, cell){
    var box_text = $(this).text();
      var today = new Date().toJSON().slice(0,10);
     var check = date._d.toJSON().slice(0,10);
     $.ajax({
    url: "$ajax_path/index.php/site/slots?date="+check,
   // dataType: 'json',
    success: function(response){
            $('#slotbox').html(response);
    },
      
        });
    }
EOF;
?>

<div class="container2">
    <br/>
    <br/>
    <br/>
    <br/>
    <div id="calendar_cont" class="hidden">
    <div class="col-sm-12" style="background:#fc7600;padding:15px" xmlns="http://www.w3.org/1999/html">
        <span style="color: #ffffff;">Service Name</span>

    </div>
        <br/>
        <br/>
        <br/>

        <div class="col-sm-12">
<div id="calendarbox">
    <?php // print_r(Yii::$app->user->identity);
    ?>

    <?= yii2fullcalendar\yii2fullcalendar::widget(array(
        'header' => [
            'left' => 'prev,next today',
            'center' => 'title',
            //   'right' => 'agendaDay,agendaWeek,month',
            'slotLabelInterval' => '00:45:00',
        ],

        'clientOptions' => [
            'selectable' => true,
            'selectHelper' => true,
            'droppable' => true,
            'editable' => true,
            'slotLabelFormat' => 'h(:mm)a',
            //'drop' => new JsExpression($JSDropEvent),
            //'select' => new JsExpression($JSCode),
            //   'dayRender' => new JsExpression($JSDayRender),
            'dayClick' => new JsExpression($JSDayClick),
            //   'eventClick'=> new JsExpression($JSslotClick),
            // 'viewRender' => new JsExpression($JSviewRender),
            'defaultDate' => date('Y-m-d'),
            'weekends' => false,
            'slotDuration' => '1:30:00',
            'dayRender' => new \yii\web\JsExpression($JSDayRender),
            // 'Duration'=>'00:45:00',


            //'eventRender' => new JsExpression($JSEventRender),
        ],
        // 'events'=> $events,
        //'ajaxEvents' => Url::toRoute(['/customer-reserved-timeslots/jsoncalendar'])
    ));


  
?>
    </div>
</div>
    <div class="clearfix"></div>
    </div>
<div id="slotbox"></div>
<div id="loginbox"></div>
    <div id="loginSignupcont"></div>

    </div>


<script>
    function slotselection(xor,event){
        event.preventDefault();
        var slot = $(xor).children('span').text();
         var service = $('input[name=service_name]:checked').val();
        alert(slot);

        $.ajax({
            url: "<?= $ajax_path?>/index.php/site/slotbooking",
        //    type:post,
            data:{'service':service,'slot':slot},
            success: function(response){
                $('#loginbox').html(response);
            }

        });

    }

    function customerSignupLogin(event,xor){
        event.preventDefault();
         var target_url = $(xor).attr('href');
       $('#loginSignupcont').load(target_url);

    }
    function nextcalendar(){
        $('#select_service_cont').addClass('hidden');
         $('#calendar_cont').removeClass('hidden');
        $('.fc-today-button').click();

    }

</script>


