<?php
use yii\helpers\Html;
use app\models\States;
use app\models\Countries;
use app\models\Cities;

/* @var $this yii\web\View */
$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid banner-container">
    <div class="row">
        <div class="col-lg-12">
            <img src='<?= Yii::getAlias('@web') ?>/images/banner.jpg' width="100%" style="border-radius: 4px;">
        </div>
    </div>
</div>

<div class="container-fluid" style="margin-top: 6%;">
    <div class="row">
        <div class="col-lg-7 col-md-7 col-sm-7">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title text-uppercase"><i class="fa fa-question-circle icon-title"></i> <span class="title">about</span></h4>
                </div>
                <div class="panel-body">
                    <div class="panel-inner-container about-customer">
                        <h3><?= $business_information->business_name ?></h3>
                        <p><?= $business_information->business_description ?></p>
                        <hr>
                        <address>
                            <strong>Company XYZ</strong>,<br>
                            <?= $business_information->address.', '.Cities::getCity_id($business_information->city).', '.States::getState_id($business_information->state).', '.Countries::getCountry_id($business_information->country).' - '.$business_information->zip ?><br><br>
                            <strong>Call:</strong> 12346546556464646<br>
                            <strong>Email:</strong> sikandarawan91@gmail.com<br>
                        </address>
                        <a href="<?= Yii::getAlias('@web') ?>/index.php/site/index" class="btn btn-main btn-save btn-schedule"> Schedule Now</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-lg-5 col-md-5 col-sm-5">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h4 class="panel-title text-uppercase"><i class="fa fa-arrow-circle-o-up icon-title"></i> <span class="title">gallery</span></h4>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-lg-12">
                            <p style="margin-top: 15px; margin-bottom: 15px;">Images</p>
                            <div class="gallery-scroller">
                                <ul class="list-inline list-gallery">
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div><hr style="margin-top: 30px;">
                    <div class="row">
                        <div class="col-lg-12">
                            <p style="margin-top: 15px; margin-bottom: 15px;">Videos</p>
                            <div class="gallery-scroller">
                                <ul class="list-inline list-gallery">
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                    <li><a href="#"><img src="<?= Yii::getAlias('@web') ?>/images/thumb.png"></a></li>
                                </ul>
                            </div>
                        </div>
                    </div><hr style="margin-top: 30px;">

                </div>
            </div>
        </div>
    </div>
</div>

<!--
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <h2><?= $business_information->business_name ?></h2>
    <?= $business_information->address.' '.$business_information->city.' '.$business_information->state.' '.$business_information->country.' '.$business_information->zip ?>
    <br/><strong>Call</strong> : <?= $business_information->business_phone ?>

    <br/>
    <?= $business_information->business_description ?>


    <h3>Staff</h3>
    <?php foreach ($staff as $staffs){ ?>
       <p><?= $staffs->username ?></p>
    <?php } ?>

</div> -->
