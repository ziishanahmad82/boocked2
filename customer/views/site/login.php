<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Login';

?>
<div class="login">
    <?php
    $form = ActiveForm::begin([
        'id' => 'login-form',
        'options' => ['class' => 'form-horizontal'],
    //    'fieldConfig' => [
   //         'template' => "<div class=\"row\"><section><label class=\"col-sm-4 txt\">{label}:</label><div class=\"col-sm-8\"><label class=\"input\">{input}</label></div></section></div>\n<div class=\"col-lg-8\">{error}</div>"

    //        ,
     //       'labelOptions' => ['class' => 'col-lg-1 control-label'],
     //   ],
    ]);
    ?>
    <div class="form">
        <header id="log">Admin Login</header>
        <fieldset>

            <?= $form->field($model, 'username') ?>

            <?= $form->field($model, 'password')->passwordInput() ?>

            <?= yii\authclient\widgets\AuthChoice::widget([
                'baseAuthUrl' => ['site/auth']
            ]) ?>


            <div class="col12"><?= Html::submitButton('Login', ['class' => 'bt btn btn-primary', 'name' => 'login-button']) ?></div>

        </fieldset>


    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php



 $scriplogin =  "
//   $('#login-form' ).submit(function( event ) {
//    event.preventDefault();
//    alert('aaaaa');
 //   });
 
 
 $('#login-form').submit(function () {
var form = $(this);
var sc = form.attr('action');
if (form.find('.has-error').length) {
return false;
}

$.ajax({
url: 'http://localhost/boocked/customer/web/index.php/site/bookingloginvalidate',
type: 'post',
data: form.serialize(),
success: function (response) {
if(response==2){
//$('#business_currency_next').click();
//form.submit();
alert('Incorrect Username/Password');
  

}else {
window.location.href = 'http://localhost/boocked/customer/web/index.php/site/';
//return false;

}
}
});
return false;
});
";

//$this->registerJs($scriplogin);
?>