<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Countries;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

$this->title = '    Customer Sign in';
?>
<?php
$countries =  Countries::find()->all();
$staff=0;
if(!isset($staff)){
$staff=0;


}
?>










<?  $path   = Yii::getAlias('@web');
$admin_sidepath = substr($path, 0, strrpos($path, "customer"));

?>

<?php $form = ActiveForm::begin(['id' => 'form-signupbooking','action'=>Yii::getAlias('@web').'/index.php/site/bookingsignup', 'options' => ['enctype' => 'multipart/form-data'] ]); ?>
<?= $form->field($model, 'username')->textInput(['placeholder' => "Username"])->label(false) ?>

<!-- <div class="form-group">
        <input type="text" class="form-control" placeholder="First Name">
    </div>-->
<div class="row">
    <div class="col-lg-6 mid">
        <?= $form->field($model, 'first_name')->textInput(['placeholder' => "First Name"])->label(false) ?>
    </div>
    <!--<div class="form-group col-lg-6 last field-signupform-first_name required">
        <input type="text" name="SignupForm[first_name]" class="form-control" id="signupform-first_name">
        <p class="help-block"></p>
    </div>-->
    <div class="col-lg-6 last">
        <?= $form->field($model, 'last_name')->textInput(['placeholder' => "Last Name"])->label(false) ?>
    </div>
    <!-- <div class="last field-signupform-last_name required">
        <label for="signupform-last_name" class="control-label"></label>
        <input type="text" name="SignupForm[last_name]" class="form-control" id="signupform-last_name">

        <div class="help-block"></div>
    </div>-->
    <? //= $form->field($model, 'last_name')?>
    <!-- <div class="form-group col-lg-6 last">
         <input type="text" class="form-control" placeholder="Last Name">
     </div>-->
</div>
<? //= $form->field($model, 'first_name') ?>
<?= $form->field($model, 'email')->input('email',['placeholder' => "Email"])->label(false) ?>
<?php if(Yii::$app->session->get('bk_zip_field')!='0'){ ?>
    <?= $form->field($model, 'customer_address')->textInput(['placeholder'=>'Postal Address'])->label(false) ?>
<?php } ?>
<?= $form->field($model, 'imageFile')->fileInput() ?>

<!--  <div class="form-group">
          <input type="text" class="form-control" placeholder="Postal Address">
      </div>-->
<?php if(Yii::$app->session->get('bk_city_state_field')!='0'){ ?>
    <div class="row">
        <div class="form-group col-lg-6 mid">
            <select class="form-control" id="businessinformation-country" name="SignupForm[customer_country]" onchange="buisness_country(this)">
                <?php  foreach($countries as $country){ ?>
                    <option <?php if($business_information->country==$country->id){ echo 'selected'; }?> value="<?= $country->id ?>">
                        <?= $country->name ?>
                    </option>
                <?php }  ?>
            </select>
            <!--   <input type="text" class="form-control" placeholder="State">-->
        </div>

        <div class="form-group col-lg-6 last">
            <select class="form-control" id="businessinformation-state" name="SignupForm[customer_region]">

            </select>
        </div>
    </div>
<?php } ?>
<div class="row">
    <?php if(Yii::$app->session->get('bk_city_state_field')!='0'){ ?>
        <div class="form-group col-lg-6 mid">
            <select class="form-control" id="signupform-customer_city" name="SignupForm[customer_city]" >

            </select>
        </div>
    <?php } ?>
    <?php if(Yii::$app->session->get('bk_zip_field')!='0'){ ?>
        <div class="col-lg-6 last">
            <?= $form->field($model, 'customer_zip')->textInput(['placeholder'=>'Zip Code'])->label(false) ?>
            <!--  <input type="text" class="form-control" placeholder="Zip Code">-->
        </div>
    <?php } ?>
    <div class="clearfix"></div>
</div>
<div class="row">


    <div class="form-group col-lg-6 mid">
        <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Password'])->label(false) ?>
    </div>
    <div class="form-group col-lg-6 mid">
        <?= $form->field($model, 'password_repeat')->passwordInput(['placeholder'=>'Confirm Password'])->label(false) ?>
    </div>
</div>

<?= Html::submitButton('Submit', ['class' => 'btn btn-main pull-right', 'name' => 'signup-button']) ?>
<!--  <button class="btn btn-main pull-right" type="button" data-toggle="modal" data-target="#thanku" id="submit-ex-btn">Submit</button>-->
<?php ActiveForm::end(); ?>
<?php /*
<div class="site-signup">
    <br/>
    <h4><?= Html::encode($this->title) ?></h4>
    <p><?= Yii::t("app", "Please fill out the following fields to create user"); ?></p>

    <div class="row">
        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-8">
        <fieldset><legend><?= Yii::t('app', 'User')?></legend> </fieldset>
            </div>
        <div class="col-sm-2">
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-2">
        </div>
        <div class="col-lg-4">
            'model'=>$model,
            <?= $form->field($model, 'username') ?>
            <?= $form->field($model, 'first_name') ?>
            <?= $form->field($model, 'last_name')?>
            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'password_repeat')->passwordInput() ?>
        </div>
        <div class="col-sm-4">





                <?= $form->field($model, 'customer_address') ?>
                <?= $form->field($model, 'customer_city') ?>
                <?= $form->field($model, 'customer_region') ?>
                <?= $form->field($model, 'customer_country') ?>
                <?= $form->field($model, 'customer_zip') ?>




        </div>
        <div class="col-sm-2">
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-4">
        <div class="form-group">
            <?= Html::submitButton('Create User', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
        </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
 */ ?>
