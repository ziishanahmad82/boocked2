<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \app\models\SignupForm */

$this->title = '    Customer Sign in';
?>

<div class="site-signup">
    <br/>
    <h4><?= Html::encode($this->title) ?></h4>
    <p><?= Yii::t("app", "Please fill out the following fields to create user"); ?></p>

    <div class="row">
        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-8">
        <fieldset><legend><?= Yii::t('app', 'User')?></legend> </fieldset>
            </div>
        <div class="col-sm-2">
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-2">
        </div>
        <div class="col-lg-4">
            <?= $form->field($model, 'username') ?>
            <?= $form->field($model, 'first_name') ?>
            <?= $form->field($model, 'last_name')?>
            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'password_repeat')->passwordInput() ?>
        </div>
        <div class="col-sm-4">





                <?= $form->field($model, 'customer_address') ?>
                <?= $form->field($model, 'customer_city') ?>
                <?= $form->field($model, 'customer_region') ?>
                <?= $form->field($model, 'customer_country') ?>
                <?= $form->field($model, 'customer_zip') ?>
                <?//= $form->field($model, 'user_group')->dropDownList(Array('admin' => 'admin', 'csr' => 'csr' )) ?>



        </div>
        <div class="col-sm-2">
        </div>
        <div class="clearfix"></div>
        <div class="col-sm-2">
        </div>
        <div class="col-sm-4">
        <div class="form-group">
            <?= Html::submitButton('Create User', ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
        </div>
            </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
