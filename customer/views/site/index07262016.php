<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
$request = Yii::$app->request;
$get = $request->get();
$session = Yii::$app->session;
if(@Yii::$app->user->identity->id)
{ // AND stristr(Yii::$app->user->identity->user_group, 'thirdparty-')
   // $session["3rd_user_id"]=Yii::$app->user->identity->id;
   // $session["3rd_user_name"]=Yii::$app->user->identity->username;
    //$session["3rd_user_group"]=Yii::$app->user->identity->user_group;
}



?>
<div class="container2">

    <?
    if(@$get["customer_action"]=='new') {
        $this->title = 'Step 1';
        ?>
        <div class="form sitem17">
            <header class="padoff pad">
                <div class="row">
                    <div class="col-sm-6"><h2 class="">Please Select Test Type</h2></div>
                    <div class="col-sm-6"><img src="<?= Yii::getAlias("@web") ?>/img/step_1.png"
                                               class="img-responsive  margin"></div>
                </div>
            </header>
            <form action="page3.html" method="post" class="sitem18">
                <fieldset>


                    <div class="row btns">
                        <center>
                            <div class="cdl">


                                    <span class="cdl1 cdl_btn3"><a
                                            href="<?= Yii::getAlias("@web") ?>/index.php/customers/create/?test_type=ncdl">STANDARD</a></span>


                                    <span class="cdl1 cdl_btn3" id="sitem1"><a
                                            href="<?= Yii::getAlias("@web") ?>/index.php/customers/create/?test_type=cdl"
                                            id="sitem6">CDL</a></span>


                            </div>

                        </center>
                    </div>


                </fieldset>
            </form>


        </div>

        <?
    } else if(@$get["customer_action"]=='edit') {
        $this->title = 'Edit Appointment';

        ?>
        <div class="form">
        <header class="padoff pad">
            <div class="row">
                <div class="col-sm-6"><h2 class="">Please Provide Required Information</h2></div>
                <div class="col-sm-6"></div>
            </div>
        </header>
        <?php $form = ActiveForm::begin(
            [
                'action' => Yii::getAlias("@web") . '/index.php/customers/editcustomer',

            ]
        ); ?>
        <fieldset>


            <div class="row">
                <center>
                    <div class="cdl">
                        <label for="learner_permit_number">Enter your Learner Permit Number
                            <input type="text" id="learners_permit_number" name="learners_permit_number">
                        </label>
                        <label for="last_name">Enter your Last Name
                            <input type="text" id="last_name" name="last_name">
                        </label>
                        <label for="dob">Enter your Date of Birth
                            <input type="text" id="dob" name="dob">
                        </label>
                        <button class="btn-primary">Submit</button>


                    </div>

                </center>
            </div>


        </fieldset>
        <?php ActiveForm::end(); ?>


        </div><?
    } else if(@$get["customer_action"]=='delete'){
        $this->title = 'Delete Appointment';

        ?>
        <div class="form">
            <header class="padoff pad">
                <div class="row">
                    <div class="col-sm-6"><h2 class="">Please Provide Required Information</h2></div>
                    <div class="col-sm-6">  </div>
                </div>
            </header>
            <?php $form = ActiveForm::begin(
                [
                    'action' => Yii::getAlias("@web").'/index.php/customers/deletecustomer',

                ]
            ); ?>
            <fieldset>



                <div class="row">
                    <center>
                        <div class="cdl">
                            <label for="learner_permit_number">Enter your Learner Permit Number
                                <input type="text" id="learners_permit_number" name="learners_permit_number">
                            </label>
                            <label for="last_name">Enter your Last Name
                                <input type="text" id="last_name" name="last_name">
                            </label>
                            <label for="email">Enter your Email
                                <input type="text" id="email" name="email">
                            </label>
                            <button class="btn-primary">Submit</button>


                        </div>

                    </center>
                </div>


            </fieldset>
            <?php ActiveForm::end(); ?>


        </div>


            <?

    } else {
        $this->title = 'Customer Appointment Portal';
        ?>
        <div class="form" id="sitem2">





                    <div class="row btns">

                            <div class="cdl">

                                <span class="cdl1 cdl_btn3"><a href="<?=Yii::getAlias("@web")?>/index.php?customer_action=new" id="sitem7">New Appointment</a></span>
                                <span class="cdl1 cdl_btn3" id="sitem1"><a href="<?=Yii::getAlias("@web")?>/index.php?customer_action=edit">Change/Cancel Appointment</a></span>


                            </div>


                    </div>





        </div>
        <?
    }
    ?>




    </div>


