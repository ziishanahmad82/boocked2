<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use yii\widgets\Pjax;
/* @var $this yii\web\View */

$this->title = 'Customer Schedule';
$request = Yii::$app->request;
$get = $request->get();
$session = Yii::$app->session;
if(@Yii::$app->user->identity->id)
{ // AND stristr(Yii::$app->user->identity->user_group, 'thirdparty-')
   // $session["3rd_user_id"]=Yii::$app->user->identity->id;
   // $session["3rd_user_name"]=Yii::$app->user->identity->username;
    //$session["3rd_user_group"]=Yii::$app->user->identity->user_group;
}
$ajax_path=Yii::getAlias('@web');
?>
<?php
if(isset( $_SESSION['book_service_id']) && isset( $_SESSION['book_slot']) ){
    $scriptredirect ="
 $.ajax({
            url: '".$ajax_path."/index.php/site/slotbooking',
            type:'POST',
            data:{'service':".$_SESSION['book_service_id'].",'slot':'".$_SESSION['book_slot']."','staff':'".$_SESSION['book_staff_id']."'},
            success: function(response){
                $('#loginbox').html(response);
                $('#select_service_cont').addClass('hidden');
            }

        });     
  
            
            ";
    $this->registerJs($scriptredirect);


}

?>
<?php Pjax::begin(['id'=>'change_service']); ?>
<div id="select_service_cont">
<div class="col-sm-12" style="background:#fc7600;padding:15px" xmlns="http://www.w3.org/1999/html">
    <span style="color: #ffffff;">Service Name <? //=$service->service_name ?></span>

</div>
<div class="col-sm-12" style="background:#ffebdd">
<form action="http://localhost/boocked/customer/web/index.php/site/" method="post">
<div class="col-sm-6">
<h3>Services</h3>

<?php foreach ($services as $service){  ?>
    <div class="form-group">
    <label>
     <input type="checkbox" value="<?=  $service->service_id ?>" name="service_name" class="service_checkbox" > <?=  $service->service_name ?><br>
    </label>
    <br>
        </div>
<? } ?>
</div>
<div class="col-sm-6" id="customer_services_list" >
    <h3>Staff</h3>
    <?php foreach ($staffs as $staff){  ?>
        <div class="form-group">
    <label>
        <input type="checkbox" value="<?=  $staff->id ?>" name="staff_name" class="staff_checkbox" id="staff_checkbox<?=  $staff->id ?>" > <?=  $staff->username ?>
        </label>
            <br>
        </div>

    <? } ?>
</div>
    <div class="clearfix"></div>

    </form>
<div class="clearfix"></div>
    <button class="btn btn-success" onclick="nextcalendar()" > Next </button>
    </div>
</div>
<div class="clearfix"></div>


<div class="container2">
    <br/>
    <br/>
    <br/>
    <br/>
    <div id="calendar_cont" class="hidden">

    </div>
<div id="slotbox"></div>
<div id="loginbox"></div>
    <div id="loginSignupcont"></div>

    </div>
<?php  Pjax::end(); ?>
<?php
$script1 = "$('.service_checkbox').click(function(){
    $('.service_checkbox').each(function(){
    $(this).prop('checked', false);
});
    $(this).prop('checked', true);
});



";
$this->registerJs($script1);
?>
<script>
    function slotselection(xor,event,service,staff){
        event.preventDefault();
        var slot = $(xor).children('span').text();
         var service = $('input[name=service_name]:checked').val();


        alert(slot);

        $.ajax({
            url: "<?= $ajax_path?>/index.php/site/slotbooking",
            type:"POST",
            data:{'service':service,'slot':slot,'staff':staff},
            success: function(response){
                $('#loginbox').html(response);
                $('#slotbox').addClass('hidden');
            }

        });

    }

    function customerSignupLogin(event,xor){
        event.preventDefault();
         var target_url = $(xor).attr('href');
       $('#loginSignupcont').load(target_url);

    }
    function nextcalendar(){
        var service = $('input[name=service_name]:checked').val();
        var staff = $('input[name=staff_name]:checked').val();
        if(service) {
            $('#select_service_cont').addClass('hidden');
            $('#calendar_cont').removeClass('hidden');
            $.ajax({
                url: "<?= $ajax_path?>/index.php/site/loadcalendar",
                type:"POST",
                data:{'service':service,'staff':staff},
                success: function(response){
                    $('#calendar_cont').html(response);
                }

            });
          //  $('#calendar_cont').load('<?= $ajax_path?>/index.php/site/loadcalendar');
            $('.fc-today-button').click();
        }else {
            alert('Please select atleast One Service');

        }
    }
    function changeCustomerService(){
        $.pjax.reload({container:"#change_service"});


    }
    function bookorder(){
         var formdata = $('#book_customer_apointment').serialize();
        $.ajax({
            url: "<?= $ajax_path?>/index.php/site/bookingappointment",
            type:"POST",
            data:formdata,
            //,
            success: function(response){
                if(response==1) {
                    $('#order_summary').addClass('hidden');
                    $('#thankyou').removeClass('hidden');

                }
            },

        });
        
    }
    function backtocalendar(){
        $('#slotbox').addClass('hidden');
        $('#calendar_cont').removeClass('hidden');

    }

</script>


