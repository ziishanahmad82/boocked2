<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\web\JsExpression;
use app\models\Countries;
use app\models\States;
use app\models\Cities;






$this->title = 'Booking';
$this->params['breadcrumbs'][] = $this->title;

$countries =  Countries::find()->all();
$staff=0;
if(!isset($staff)){
    $staff=0;


}
?>
<style>
    .fc-row.fc-week.fc-widget-content{
        max-height:50px !important;
    }
    .fc-center h2{
        font-size:17px;

    }
    .fc-content-skeleton {
     /*   padding-top:20px; */

    }
    .fc-content-skeleton{
        padding-top:16px;

    }
    .fc-content-skeleton tr td, .fc-future{
        text-align:center !important;
        color:#fff;
        font-size:12px;
        opacity:1 !important;

    }
    .fc-day.fc-widget-content{
        text-align:center;
        vertical-align: middle;



       /* position: absolute; */

    }
    .redcalendarbox {
        width:20px;
        height:20px;
        border-radius:11px;
        background:red;
        display:inline-block;

    }
    .fc-day-number {
      /*  color:#fff; */
    }
    .greencalendarbox {
        width:20px;
        height:20px;
        background-color:#00A715;
        display:inline-block;
        border-radius:11px;


    }
</style>
<?  $path   = Yii::getAlias('@web');
$admin_sidepath = substr($path, 0, strrpos($path, "customer"));

?>
<div class="container-fluid" style="margin-top: 2%;">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h4 class="panel-title text-uppercase"><i class="fa fa-gear icon-title"></i> <span class="title">heading</span></h4>
                        </div>
                        <div class="panel-body">
                            <div class="checkbox-service" style="min-height:155px;">
                                <ul class="list-inline">
                                   <?php  $admin_sidepath ?>
                                    <input type="hidden" id="boocked_service_id" value="<?= $service_info->service_id ?>" >
                                   <?  $service_info->service_image ?>

                                    <li><img src="<? echo Yii::getAlias("@web").'/images/service.png'; ?>"></li>
                                    <li>
                                        <ul class="list-unstyled list-service-title">
                                            <li class="text-uppercase"><b><?= $service_info->service_name ?></b></li>
                                            <li>
                                                <ul class="list-inline list-service-title-ratings">
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                    <li><i class="fa fa-star"></i></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                                <p style="margin-top: 15px;"><b>Description</b></p>
                                <p style="margin-top: -5px;"><small><?= $service_info->service_description ?></small></p>
                            </div>
                            <div class="service-selected">
                                <div class="row">
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                        <ul class="list-unstyled">
                                            <li><i class="fa fa-check txt-white" style="font-size: 45px;"></i></li>
                                            <li class="text-uppercase" style="margin-top: 15px;">service duration</li>
                                            <li><small><?= $service_info->service_duration ?></small></li>
                                            <li class="text-uppercase" style="margin-top: 15px;">service cost</li>
                                            <li><small><?= Yii::$app->session->get('bk_currency').' '.$service_info->service_price ?></small></li>
                                        </ul>
                                    </div>
                                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-6 text-center">
                                        <img src="<? if($service_info->service_image!=""){ echo $admin_sidepath.'admin/web/'.$service_info->service_image; }else { echo Yii::getAlias("@web").'/web/images/staff2.png'; }?>" style="margin-top: 25px;">
                                        <p class="text-uppercase" style="margin-top: 15px;"><?= $service_info->service_name ?></p>
                                    </div>
                                </div>
                            </div>
                            <hr class="media-hr">
                            <div class="panel panel-default panel-staff">
                                <div class="panel-heading">
                                    <h4 class="panel-title text-uppercase"><i class="fa fa-user icon-title"></i> <span class="title">staff</span></h4>
                                </div>
                                <div class="panel-body staff-div">
                                    <div class="row">
                                       <?php foreach($allstaffs as $staffs){ ?>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <?php
                                            if($staffs->user_profile_image!=""){?>
                                                <img src="<?= $admin_sidepath . 'admin/web/' . $staffs->user_profile_image ?>" class="<?= $staffs->username ?>">
                                            <?php }else{ ?>
                                                <img src="<?= Yii::getAlias('@web') ?>/images/staff12.png" class="<?= $staffs->username ?>" >
                                            <?php } ?>
                                          <!--  <img src="<? //= Yii::getAlias('@web') ?>/images/staff1.png">-->
                                            <p><small class="title-sm"><?= $staffs->username ?></small></p>
                                        </div>
                                        <?php } ?>
                                   <!--     <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff2.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff2.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff2.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff2.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff2.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>

                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff2.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div>
                                        <div class="col-lg-4 col-md-6 col-sm-6 col-xs-6 text-center media-col5">
                                            <img src="<?= Yii::getAlias('@web') ?>/images/staff1.png">
                                            <p><small class="title-sm">John Doe</small></p>
                                        </div> -->
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-8 col-md-8 col-sm-8">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <ul class="list-inline">
                                <li><h4 class="panel-title text-uppercase"><i class="fa fa-user icon-title"></i> <span class="title">calendar</span></h4></li>
                                <li class="pull-right">
                                    <div class="btn-group" style="display:none">
                                        <button type="button" class="btn btn-calendar">Week</button>
                                        <button type="button" style="border-left: 1px solid #fff;" class="btn btn-calendar dropdown-toggle" data-toggle="dropdown">
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu menu-cal" role="menu">
                                            <li><a href="#">Month</a></li>
                                            <li><a href="#">Year</a></li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <p class="pull-right"><small>Available Slots</small> <i class="fa fa-square square-green"></i></p>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                    <div style="width:100%; max-width:100%;display:inline-block;">
                                        <?php
                                        if(Yii::$app->session->get('bk_advance_days_booking')){
                                            $advance_days_booking = Yii::$app->session->get('bk_advance_days_booking');
                                        }else {
                                            $advance_days_booking = 60;
                                        } ?>
<?php
    $service=$service_info->service_id;
    $ajax_path=Yii::getAlias('@web');


    $JSDayRender = <<<EOF
    function(date, cell){
    curdate = new Date();
  
     var today = new Date().toJSON().slice(0,10);
     
 
 
     var check = date._d.toJSON().slice(0,10);
     if(check >= today){
      
      $.ajax({
    url: "$ajax_path/index.php/site/checkavailableslots",
    type: 'POST',
    data:{'date':check,'service':$service,'staff':$staff  },
  
    dataType: 'json',
    success: function(response){
        if(response>0){
        //  cell.css("background-color", "green");
         //  cell.css("color", "#fff");
          //  cell.html('<i class="glyphicon glyphicon-stop" style="color:#00A715"></i>');
          cell.html('<i class="greencalendarbox" style="color:green">1</i>');
        }else {
         // cell.css("background-color", "red");
        //  cell.css("color", "#fff");
         //   cell.html('<i class="glyphicon glyphicon-ban-circle" style="color:red"></i>');
         cell.html('<i class="redcalendarbox" style="color:red">1</i>');
        }
        }
        });
        }
   
       
      if(check < today){
            cell.css("background-color", "lightgray");
            cell.html("<span style='display:none;'>past</span>");
        }
   
 //   if (date < curdate){
   
 // cell.addClass("disabled");
   //     cell.css("background-color","red");
    //    cell.text("Unavailable");
    //    cell.addClass("disabled");
  
   // }
    }
EOF;
    $JSEventClick = <<<EOF
    function(date, cell){
    var box_text = $(this).text();
    if(box_text=='Unavailable' || box_text=='past' ){
      
        }else {
        alert('asdddd');
        
        }
    }
EOF;

  $JSDayClick = <<<EOF
    function(date, cell){
    var box_text = $(this).text();
      var today = new Date().toJSON().slice(0,10);
     var check = date._d.toJSON().slice(0,10);
     $.ajax({
    url: "$ajax_path/index.php/site/slots",
    type: 'POST',
    data:{'date':check,'service':$service ,'staff':$staff  },
    success: function(response){
          //  $('#slotbox').html(response);
           $('.timings-div').html(response);
           // $('#calendar_cont').addClass('hidden');
           //  $('#slotbox').removeClass('hidden');
    },
      
        });
    }
EOF;
                                        ?>
                 
                 <?php
                 if(Yii::$app->session->get('bk_schedule_view_shown_by_default')==''){
                 $first_day=1;
                 }else if(Yii::$app->session->get('bk_calendar_start_day')=='today'){
                 $first_day = date('N');
                 }else {
                 $first_day = date('N',strtotime(Yii::$app->session->get('bk_calendar_start_day')));
                 } ?>


<?= yii2fullcalendar\yii2fullcalendar::widget(array(
            'header' => [
                'left' => 'prev,next today',
                'center' => 'title',
                //   'right' => 'agendaDay,agendaWeek,month',
                'slotLabelInterval' => '00:45:00',
            ],

            'clientOptions' => [
                'selectable' => true,
                'selectHelper' => true,
                'droppable' => true,
                'editable' => true,
                'slotLabelFormat' => 'h(:mm)a',
                'firstDay'=>$first_day,

                'dayClick' => new JsExpression($JSDayClick),

                'defaultDate' => date('Y-m-d'),

                'slotDuration' => '1:30:00',
                'dayRender' => new \yii\web\JsExpression($JSDayRender),


                //'eventRender' => new JsExpression($JSEventRender),
            ],
            // 'events'=> $events,
            //'ajaxEvents' => Url::toRoute(['/customer-reserved-timeslots/jsoncalendar'])
        ));


                                          
                                            ?>
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        
                                        <div class="monthly" id="mycalendar"></div>
                                    </div>
                                </div>
                            </div>
                            <hr>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title text-uppercase"><i class="fa fa-clock-o icon-title"></i> <span class="title">timings</span></h4>
                                </div>
                                <div class="panel-body timings-div">
                                    <table class="table table-striped table-responsive table-condensed">
                                        <tr class="text-center">
                                            <td><h5 class="text-uppercase text-muted">available timings</h5></td>
                                            <td><h5 class="text-uppercase text-muted">unavailable timings</h5></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                        <tr class="text-center">
                                            <td>9: 00</td>
                                            <td><a href="javascript:void(0)" class="btn btn-link text-danger" data-container="body" data-toggle="popover" data-placement="right" data-content="Update service">8: 00</a></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="conatiner-fluid" style="margin-right: 15px; margin-bottom: 20px;">
            <div class="row">
                <div class="col-lg-12">
                    <button class="btn btn-main pull-right" data-toggle="modal" data-target="#modal-ex-customer">Continue</button>
                </div>
            </div>
        </div>

        <div class="modal fade" id="modal-ex-customer">
          <!--  <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body modal-ex">
                        <div class="panel panel-default" style="margin-bottom: 0;">
                            <div class="panel-heading">
                                <h4 class="panel-title text-uppercase"><i class="fa fa-user icon-title"></i> <span class="title">existing customer</span></h4>
                            </div>
                            <div class="panel-body panel-ex">
                                <form method="post" action="">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="User Name">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Password">
                                    </div>
                                    <ul class="list-inline list-submit">
                                        <li class="checkbox forgot-pwd">
                                            <input type="checkbox"> <small> Forgot Password</small>
                                        </li>
                                        <li class="pull-right">
                                            <button type="submit" class="btn btn-main">Submit</button>
                                        </li>
                                    </ul>
                                </form>
                                <button id="modal-ex" class="btn btn-main btn-block" data-toggle="modal" data-target="#modal-new-c" style="margin-top: 70px; padding: 11px 12px;">New Customer</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>-->
        </div>
<style>
   #modal-new-c .panel-ex{
     padding:30px 90px;
   }

</style>
        <div class="modal fade" id="modal-new-c">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body modal-ex">
                        <div class="panel panel-default" style="margin-bottom: 0;">
                            <div class="panel-heading">
                                <h4 class="panel-title text-uppercase"><i class="fa fa-user icon-title"></i> <span class="title">new customer</span></h4>
                            </div>
                        <?php
                       // ,'layout' => 'horizontal'
                        /*  $form = ActiveForm::begin(['id' => 'login-form']); ?>
                            <?= $form->field($model, 'username') ?>
                            <?= $form->field($model, 'first_name') ?>
                            <?= $form->field($model, 'last_name')?>
                            <?= $form->field($model, 'email') ?>
                            <?= $form->field($model, 'password')->passwordInput() ?>
                            <?= $form->field($model, 'password_repeat')->passwordInput()  */ ?>
                            <div class="panel-body panel-ex" id="signupformcont">
                                <?php $form = ActiveForm::begin(['id' => 'form-signupbooking',  'validationUrl' => Yii::getAlias('@web').'/index.php/site/bookingsignup_validate', 'enableAjaxValidation' => true,'action'=>Yii::getAlias('@web').'/index.php/site/bookingsignup', 'options' => ['enctype' => 'multipart/form-data'] ]); ?>
                                <?= $form->field($model, 'username')->textInput(['placeholder' => "Username"])->label(false) ?>

                                <!-- <div class="form-group">
                                        <input type="text" class="form-control" placeholder="First Name">
                                    </div>-->
                                    <div class="row">
                                        <div class="col-lg-6 mid">
                                            <?= $form->field($model, 'first_name')->textInput(['placeholder' => "First Name"])->label(false) ?>
                                        </div>
                                        <!--<div class="form-group col-lg-6 last field-signupform-first_name required">
                                            <input type="text" name="SignupForm[first_name]" class="form-control" id="signupform-first_name">
                                            <p class="help-block"></p>
                                        </div>-->
                                        <div class="col-lg-6 last">
                                        <?= $form->field($model, 'last_name')->textInput(['placeholder' => "Last Name"])->label(false) ?>
                                        </div>
                                        <!-- <div class="last field-signupform-last_name required">
                                            <label for="signupform-last_name" class="control-label"></label>
                                            <input type="text" name="SignupForm[last_name]" class="form-control" id="signupform-last_name">

                                            <div class="help-block"></div>
                                        </div>-->
                                        <? //= $form->field($model, 'last_name')?>
                                       <!-- <div class="form-group col-lg-6 last">
                                            <input type="text" class="form-control" placeholder="Last Name">
                                        </div>-->
                                    </div>
                                <? //= $form->field($model, 'first_name') ?>
                                <?= $form->field($model, 'email')->input('email',['placeholder' => "Email"])->label(false) ?>
                                <?php if(Yii::$app->session->get('bk_zip_field')!='0'){ ?>
                                <?= $form->field($model, 'customer_address')->textInput(['placeholder'=>'Postal Address'])->label(false) ?>
                                <?php } ?>
                                <?= $form->field($model, 'profile_image')->fileInput() ?>

                              <!--  <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Postal Address">
                                    </div>-->
                                <?php if(Yii::$app->session->get('bk_city_state_field')!='0'){ ?>
                                    <div class="row">
                                        <div class="form-group col-lg-6 mid">
                                            <select class="form-control" id="businessinformation-country" name="SignupForm[customer_country]" onchange="buisness_country(this)">
                                            <?php  foreach($countries as $country){ ?>
                                                <option <?php if($business_information->country==$country->id){ echo 'selected'; }?> value="<?= $country->id ?>">
                                                    <?= $country->name ?>
                                                </option>
                                            <?php }  ?>
                                            </select>
                                         <!--   <input type="text" class="form-control" placeholder="State">-->
                                        </div>

                                        <div class="form-group col-lg-6 last">
                                            <select class="form-control" id="businessinformation-state" name="SignupForm[customer_region]">
                                           
                                            </select>    
                                        </div>
                                    </div>
                                <?php } ?>
                                    <div class="row">
                                        <?php if(Yii::$app->session->get('bk_city_state_field')!='0'){ ?>
                                        <div class="form-group col-lg-6 mid">
                                            <select class="form-control" id="signupform-customer_city" name="SignupForm[customer_city]" >

                                            </select>
                                        </div>
                                        <?php } ?>
                                        <?php if(Yii::$app->session->get('bk_zip_field')!='0'){ ?>
                                        <div class="col-lg-6 last">
                                            <?= $form->field($model, 'customer_zip')->textInput(['placeholder'=>'Zip Code'])->label(false) ?>
                                          <!--  <input type="text" class="form-control" placeholder="Zip Code">-->
                                        </div>
                                        <?php } ?>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="row">


                                        <div class="form-group col-lg-6 mid">
                                            <?= $form->field($model, 'password')->passwordInput(['placeholder'=>'Password'])->label(false) ?>
                                        </div>
                                        <div class="form-group col-lg-6 mid">
                                            <?= $form->field($model, 'password_repeat')->passwordInput(['placeholder'=>'Confirm Password'])->label(false) ?>
                                        </div>
                                    </div>
                                   <!-- <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Card Number">
                                    </div>
                                    <ul class="list-inline list-payments">
                                        <li>Payment Types</li>
                                        <div class="list-inner">
                                            <li class="radio pull-right cc"><input type="radio" data-toggle="modal" data-target="#card-options" class="radio-payment"> <small>Credit Card</small></li>
                                            <li class="radio pull-right"><input type="radio" data-toggle="modal" data-target="#paypal-options"> <small>Paypal</small></li>
                                        </div>
                                    </ul>
                                    <div class="row">
                                        <div class="col-lg-4 exp"><p>Expiration Date</p></div>
                                        <div class="col-lg-8">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="mm/dd/yy">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="CSV Code">
                                    </div> -->
                                <?= Html::submitButton('Submit', ['class' => 'btn btn-main pull-right', 'name' => 'signup-button']) ?>
                                  <!--  <button class="btn btn-main pull-right" type="button" data-toggle="modal" data-target="#thanku" id="submit-ex-btn">Submit</button>-->
                                <?php ActiveForm::end(); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="thanku">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body text-center">
                        <h3 class="text-thanku">Thank you for the registration</h3>
                        <h4 class="text-thanku">You will soon receive a confirmation email</h4>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="card-options">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="panel panel-default" style="margin-bottom: 0;">
                            <div class="panel-heading">
                                <button class="close" data-dismiss="modal">&times;</button>
                                <h4 class="panel-title text-uppercase"><i class="fa fa-dollar icon-title"></i> <span class="title">payment screen</span></h4>
                            </div>
                            <div class="panel-body panel-ex">
                                <form method="post" action="">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Full Name">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Card Number">
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="Card Number">
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <input type="text" class="form-control" placeholder="CVV">
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-main" type="button">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
<div id="loginbox">


</div>
        <div class="modal fade" id="paypal-options">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-body">
                        <div class="panel panel-default" style="margin-bottom: 0;">
                            <div class="panel-heading">
                                <button class="close" data-dismiss="modal">&times;</button>
                                <h4 class="panel-title text-uppercase"><i class="fa fa-dollar icon-title"></i> <span class="title">paypal payment screen</span></h4>
                            </div>
                            <div class="panel-body panel-ex">
                                <form method="post" action="#">
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="User Name">
                                    </div>
                                    <div class="form-group">
                                        <input type="text" class="form-control" placeholder="Password">
                                    </div>
                                    <button class="btn btn-main" type="button">Submit</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<script>

    function slotselection(xor,event,service,staff,date){
        event.preventDefault();
        var slot = $(xor).text();
       // children('span')
     //   var service = $('input[name=service_name]:checked').val();



        $.ajax({
            url: "<?= $ajax_path?>/index.php/site/slotbooking",
            type:"POST",
           // service staff;
            data:{'service':service,'slot':slot,'staff':staff,'date':date},
            success: function(response){

                $('#modal-ex-customer').html(response);
                $('#modal-ex-customer').modal('show');
            //    $('#loginbox').html(response);
            //    $('#slotbox').addClass('hidden');
            }

        });

    }




    function customerSignupLogin(event,xor){
        event.preventDefault();
        var target_url = $(xor).attr('href');
        $('#loginSignupcont').load(target_url);

    }
    function buisness_country(xor){
       // alert('aaaaaaaa');
        var country_id =  $('#businessinformation-country option:selected').val();
        $.ajax({
            url:"<?= Yii::getAlias('@web') ?>/index.php/site/address_states",
            type: 'post',
            data: {'country_id':country_id,'type':'country'},
        success: function (response) {
            if(response){
                //    alert('changed');
                $('#businessinformation-state').html(response);
                $('#businessinformation-city').html('');

            }else {
                alert('not updated');

            }
        }
    });

    }
    function bookorder(){
        var formdata = $('#book_customer_apointment').serialize();
        $.ajax({
            url: "<?= $ajax_path?>/index.php/site/bookingappointment",
            type:"POST",
            data:formdata,
            //,
            success: function(response){
                if(response==1) {
                    $('#order_summary').addClass('hidden');
                    $('#thankyou').removeClass('hidden');

                }
            },

        });

    }
</script>

<?php
$script = "
 
  $('#businessinformation-state').change(function(){
  var state_id =  $('#businessinformation-state option:selected').val();
 $.ajax({
    url:'".Yii::getAlias('@web')."/index.php/site/address_states',
    type: 'post',
    data: {'state_id':state_id,'type':'state'},
    success: function (response) {
    if(response){
    //    alert('changed');
        $('#signupform-customer_city').html(response);

    }else {
    //    alert('not updated');

    }
}
});
 });
 
 $('body').on('beforeSubmit', '#form-signupbooking', function () {
//var form = $('#update_customerfrom');
var form = $('#form-signupbooking');
$.ajax({
url:'".Yii::getAlias('@web')."/index.php/site/bookingsignup',
type: 'POST',
//data: form.serialize(),
 data: new FormData( this ),
      processData: false,
      contentType: false,
success: function (response) {
if(response==1){

 $('#modal-new-c').modal('hide');
$.ajax({
            url: '".$ajax_path."/index.php/site/slotbooking/',
            type:'POST',
           // service staff;
            data:{'newlogin':'newlogin'},
            success: function(response){

                $('#modal-ex-customer').html(response);
              $('#modal-ex-customer').modal('show');
           
            },

        });


}else {
//$('#signupformcont').html(response)
}
}
}); 
return false;
})
 
$('body').on('beforeSubmit', '#login-form', function () {
var form = $('#login-form');
 formaction = form.attr('action');
$.ajax({
url:'".Yii::getAlias('@web')."/index.php/site/bookinglogin',
type: 'post',
data: form.serialize(),
success: function (response) {
if(response==1){
$.ajax({
            url: '".$ajax_path."/index.php/site/slotbooking/',
            type:'POST',
           // service staff;
            data:{'newlogin':'newlogin'},
            success: function(response){

                $('#modal-ex-customer').html(response);
         //       $('#modal-ex-customer').modal('show');
           
            },

        });


          


}else {
alert('Incorrect Username/password')
}
}
}); 
return false;
})
$(document).on('click', '#modal-ex', function(event) { 
    $('#modal-ex-customer').modal('hide');
});

 ";

$this->registerJs($script); ?>


