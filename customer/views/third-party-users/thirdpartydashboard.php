<?php
/* @var $this yii\web\View */
$this->title = 'Dashboard';
use app\models\CustomerReservedTimeslots;
use app\models\Customers;
use app\models\Locations;
use app\models\Toughbooks;
use yii\bootstrap\Tabs;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\widgets\Pjax;

$request = Yii::$app->request;
$get = $request->get();

$session = Yii::$app->session;

$this->registerJs(
    '
$(document).ready( function(){
        $("#div-select-toughbook1").hide();
    });
    $("#status1").change(function(){
        if($("#status1").val()=="assign-toughbook"){
            $("#div-select-toughbook1").show();
        }
    });


    $(document).ready( function(){
        $("#div-select-toughbook2").hide();
    });
    $("#status2").change(function(){
        if($("#status2").val()=="assign-toughbook"){
            $("#div-select-toughbook2").show();
        }
    });
'
);
?>




<div class="site-index">






    <div class="body-content">


                <div style="float: right;">

                    <span class="cdl1 cdl_btn3"><a href="<?=Yii::getAlias("@web")?>/index.php?customer_action=new">New Appointment</a></span>
                    <span class="cdl1 cdl_btn3"><a href="<?=Yii::getAlias("@web")?>/index.php?customer_action=edit">Change/Cancel Appointment</a></span>



                </div>


        <div class="dashboard_heading_div">
            <h1>Dashboard</h1>
        </div>







        <?php ob_start(); ?>



        <?php Pjax::begin(['id'=>'p1']); ?>

        <?= GridView::widget([
            'id'=>'p1',
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                //['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'dl_no',
                    'label' => 'License No',
                    'value' => 'customer.learners_permit_number',
                ],
                [
                    'attribute' => 'full_name',
                    'label' => 'Name',
                    'value' => 'customer.first_name',
                ],
                //'location_id',
                //'test_date',
                [
                    'attribute' => 'test_start_time',
                    'label' => 'Test Time',
                    'value' => 'test_start_time',
                    'format' => ['time', 'php:h:i A'],
                ],

                //'test_start_time',
                // 'test_end_time',
                // 'examiner_id',
                'test_type',
                // 'confirmed',
                // 'third_user_id',
                //'tough_book_id',
                'status',

                //['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>


        <?php $var1 = ob_get_clean(); ?>



        <?php ob_start(); ?>




        <?php Pjax::begin(['id'=>'p2']); ?>

        <?= GridView::widget([
            'id'=>'p2',
            'dataProvider' => $dataProvider2,
            'filterModel' => $searchModel2,
            'columns' => [
                //['class' => 'yii\grid\SerialColumn'],

                [
                    'attribute' => 'dl_no',
                    'label' => 'License No',
                    'value' => 'customer.learners_permit_number',
                ],
                [
                    'attribute' => 'full_name',
                    'label' => 'Name',
                    'value' => 'customer.first_name',
                ],
                //'location_id',
                //'test_date',
                [
                    'attribute' => 'test_start_time',
                    'label' => 'Test Time',
                    'value' => 'test_start_time',
                    'format' => ['time', 'php:h:i A'],
                ],

                //'test_start_time',
                // 'test_end_time',
                // 'examiner_id',
                'test_type',
                // 'confirmed',
                // 'third_user_id',
                //'tough_book_id',
                'status',

                //['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>
        <?php Pjax::end(); ?>




        <?php $var2 = ob_get_clean(); ?>







        <div class="dashboard-tabs">
            <?

            echo Tabs::widget([
                'items' => [
                    [
                        'label' => "Today's Appointments ($count1)",
                        'content' => $var1,
                    ],
                    [
                        'label' => "Today's Completed  Appointments ($count2)",
                        'content' => $var2,
                    ],


                ],
            ]);
            ?>
        </div>











    </div>




</div>

