<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
$request = Yii::$app->request;
$get = $request->get();
$session = Yii::$app->session;
$this->title = 'Third party Login';


?>
<div class="container2" xmlns="http://www.w3.org/1999/html">


        <div class="form">
            <header class="padoff pad">
                <div class="row">
                    <div class="col-sm-6"><h2 class="">Third Party Login</h2></div>
                    <div class="col-sm-6"></div>
                </div>
            </header>

            <div class="third-party-login-form-div inner-form">
                <?php $form = ActiveForm::begin(
                    [
                        'action' => Yii::getAlias("@web") . '/index.php/third-party-users/index',

                    ]
                ); ?>
                <div class="row">

                        <input type="text" id="user_name" name="user_name" placeholder="User Name">

                </div>
                <div class="row">

                        <input type="password" id="password" name="password" placeholder="Password">

                </div>
                <br/>
                <div class="row">
                    <button class="btn-primary">Login</button>
                </div>

                <?php ActiveForm::end(); ?>
            </div>


        </div>



</div>
