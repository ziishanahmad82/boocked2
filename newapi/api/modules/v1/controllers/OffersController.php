<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use yii\rest\Controller;
use api\modules\v1\models\Category;
use api\modules\v1\models\User;
use api\modules\v1\models\Service;
use api\modules\v1\models\CustomerCategory;
use api\modules\v1\models\Appointments;
use yii;
use yii\db\Query;
use yii\web\JsonParser;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\CompositeAuth;
use yii\web\Response;
use DateTime;
/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class OffersController extends Controller
{
    //public $modelClass = 'app\models\User';
    //public $modelClass = 'api\modules\v1\models\Country';

    /**
     * @return string
     */
    public function behaviors(){
       $behaviors = parent::behaviors();
      /*     $behaviors['authenticator'] = [
              'class' => HttpBearerAuth::className()
          ]; */

          return $behaviors;
    }
    public function actionIndex()
    {
        $alldata = json_decode(Yii::$app->request->getRawBody(), true);
        $id = $alldata['id'];
       // $id = 120;
        $query = new Query;
        $query->select([
                'services.*',
                'services.service_id as sid',
                'business_information.*',
                'service_offer.*']
        )
            ->from('services')
            ->join('INNER JOIN', 'business_information',
                'services.business_id=business_information.business_id')
            ->join('LEFT JOIN', 'service_offer',
                'services.service_id=service_offer.service_id')
            ->where(['services.service_id' => $id]);
        $command = $query->createCommand();
        $data5 = $command->queryAll();
        $data2 = array();
        $data10 = array();
        //$view = Views::find()->where(['service_id' => $id,'date'=>date("Y-m-d")])->one();
        $appointment = Appointments::find()->where(['service_id' => $id])->all();
        $totalap = count ( $appointment );
        foreach ($data5 as $service5) {
            $service50=array();

            /*$service50["service_id"] = $service5["sid"];
            $service50["service_name"] = $service5["service_name"];
            $service50["service_description"] = $service5["service_description"];
            $service50["service_duration"] = $service5["service_duration"];
            $service50["service_price"] = $service5["service_price"];
            $service50["service_capacity"] = $service5["service_capacity"];
            $service50["service_status"] = $service5["service_status"];
            $service50["service_cat_id"] = $service5["service_cat_id"];
            $service50["business_id"] = $service5["business_id"];
            $service50["featured"] = $service5["featured"];
            if (empty($service5["service_image"])) {
                $service50["service_image"] = "http://boocked.com/admin/web/uploads/5215service.png";
            } else {
                $service50["service_image"] = "http://boocked.com/admin/web/" . $service5["service_image"];
            }

            $service50["google_address"] = $service5["google_address"];
            $service50["business_name"] = $service5["business_name"];
            $service50["business_description"] = $service5["business_description"];
            $service50["business_phone"] = $service5["business_phone"];
            $service50["profession"] = $service5["profession"];
            $service50["business_subdomain"] = $service5["business_subdomain"];
            $service50["business_status"] = $service5["business_status"];*/
            // $service50["distance"]=$service5["distance"];


            if ($service5['offer_start'] != "" AND $service5['offer_end'] != "") {
                $now = new DateTime(date("Y-m-d"));
                $start = new DateTime($service5['offer_start']);
                $end = new DateTime($service5['offer_end']);
                if ($end >= $now) {
                    $service50["offer_id"] = $service5["id"];
                    $service50["service_id"] = $service5["sid"];
                    $service50["service_name"] = $service5["service_name"];
                    $RemainingDays = $now->diff($end);
                    $TotalDays = $start->diff($end);
                    $service50["service_offer_name"] = $service5['offer_name'];
                    $service50["service_offer_end_date"] = date("Y-m-d",strtotime($service5['offer_end']));

                    $service50["service_offer_end_time"] = "23:59:59";
                    $service50["service_offer_current_datetime"] = date("Y-m-d h:i:s");
                    $service50["service_offer_remainingdays"] = $RemainingDays->days;
                    $service50["service_offer_totaldays"] = $TotalDays->days;
                    $service50["service_offer_description"] = $service5["offer_desc"];
                    $service50["service_off_price_percentage"] = $service5["discount"];
                    $service50["service_off_price"] =round($service5["service_price"]*((100-$service5["discount"])/100),2);
                    $service50["service_discount_price"] = round($service5["service_price"]*(($service5["discount"])/100),2);
                    $service50["service_price"] = $service5["service_price"];
                    $service50["bookedappointments"]=$totalap;

                } else {
                    /*$service50["service_offer_description"] = "";
                    $service50["service_offer_remainingdays"] = 0;
                    $service50["service_offer_totaldays"] = 0;
                    $service50["service_discount_price"] = 0;
                    $service50["service_off_price_percentage"] = 0;*/
                }
            } else {
                /*$service50["service_offer_description"] = "";
                $service50["service_offer_remainingdays"] = 0;
                $service50["service_offer_totaldays"] = 0;
                $service50["service_discount_price"] = 0;
                $service50["service_off_price_percentage"] = 0;*/
            }
           // $service50["views"]=$countt;

            //$data10=$service50;
            if(!empty($service50)) {
                array_push($data10, $service50);
            }
        }
        $data11=array();
        $data11['offers']=$data10;
        return ['success' => true, 'data' => $data11];

      
    }

}


