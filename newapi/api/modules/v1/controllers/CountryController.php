<?php

namespace api\modules\v1\controllers;

use yii;
use yii\rest\Controller;
use yii\filters\auth\HttpBearerAuth;
/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class CountryController extends Controller
{
    public $modelClass = 'api\modules\v1\models\Country';   
    public function behaviors(){
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className()
        ];

        return $behaviors;
    }
    public function actionIndex(){
    	$all=Yii::$app->user->identity;
        echo "string";
    print_r($all);
    }
    
}


