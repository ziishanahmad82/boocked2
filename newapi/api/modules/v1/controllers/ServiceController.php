<?php

namespace api\modules\v1\controllers;

//use app\models\Appointments;
use yii\rest\ActiveController;
use yii\rest\Controller;
use api\modules\v1\models\Category;
use api\modules\v1\models\User;
use api\modules\v1\models\Service;
use api\modules\v1\models\CustomerCategory;
use api\modules\v1\models\ServiceOffer;
use api\modules\v1\models\Views;
use api\modules\v1\models\Appointments;
use yii;
use yii\db\Query;
use DateTime;
use yii\web\JsonParser;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\CompositeAuth;
use yii\web\Response;
/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class ServiceController extends Controller
{
    //public $modelClass = 'app\models\User';
    //public $modelClass = 'api\modules\v1\models\Country';

    /**
     * @return string
     */

    public function behaviors(){
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className()
        ];

        return $behaviors;
    }
    public function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }
    public function actionIndex()
    {
        $alldata = json_decode(Yii::$app->request->getRawBody(), true);
        $id = $alldata['id'];
        $query = new Query;
        $query->select([
                'services.*',
                'services.service_id as sid',
                'business_information.*',
                //'service_offer.*'
            ]
        )
            ->from('services')
            ->join('INNER JOIN', 'business_information',
                'services.business_id=business_information.business_id')
            /*->join('LEFT JOIN', 'service_offer',
                'services.service_id=service_offer.service_id')*/
            ->where(['services.service_id' => $id]);
        $command = $query->createCommand();
        $data5 = $command->queryAll();
        $data2 = array();
        $data10 = array();
        $data19 = array();
        $view = Views::find()->where(['service_id' => $id,'date'=>date("Y-m-d")])->one();
        $appointment = Appointments::find()->where(['service_id' => $id])->all();
        $totalap = count ( $appointment );
        if (empty($view)) {
            $view = new Views;
            $countt=0;
        }else{
            $countt=$view->v_count;
        }
        foreach ($data5 as $service5) {
            $service59=array();
            $service60=1;
            $service50["service_id"] = $service5["sid"];
            $service50["service_name"] = $service5["service_name"];
            $service50["service_description"] = $service5["service_description"];
            $service50["service_duration"] = $service5["service_duration"];
            $service50["service_price"] = $service5["service_price"];
            $service50["service_capacity"] = $service5["service_capacity"];
            $service50["service_status"] = $service5["service_status"];
            $service50["service_cat_id"] = $service5["service_cat_id"];
            $service50["business_id"] = $service5["business_id"];
            $service50["featured"] = $service5["featured"];
            if (empty($service5["service_image"])) {
                $service50["service_image"] = "http://boocked.com/admin/web/uploads/5215service.png";
            } else {
                $service50["service_image"] = "http://boocked.com/admin/web/" . $service5["service_image"];
            }

            $service50["google_address"] = $service5["google_address"];
            if (empty($service5["lat"])){
                $service50["lat"] = 100;
                $service50["lng"] = 100;
            }else {
                $service50["lat"] = $service5["lat"];
                $service50["lng"] = $service5["lng"];
            }
            $service50["business_name"] = $service5["business_name"];
            $service50["business_description"] = $service5["business_description"];
            $service50["business_phone"] = $service5["business_phone"];
            $service50["profession"] = $service5["profession"];
            $service50["business_subdomain"] = $service5["business_subdomain"];
            $service50["business_status"] = $service5["business_status"];
            // $service50["distance"]=$service5["distance"];

            ///////////////////////

            $query1 = new Query;
            $query1->select([
                "COUNT('review_status') as row",
                "AVG(review_status) as 'avg'"
            ])
                ->from('reviews')
                ->where(['service_id' => $service5["sid"]]);
            $command1 = $query1->createCommand();
            $data51 = $command1->queryAll();
            if (empty($data51[0]["avg"])) {
                $data51[0]["avg"] = 0;
            }
            $service50["service_rating_percentage"] = $data51[0]["avg"];
            $service50["service_rating_percentage_total"] = 4;
            $service50["service_rating_count"] = $data51[0]["row"];
            ///////////////////////

            if (!empty($service5['lat']) AND !empty($service5['lng'])) {
                $service50['distance'] = $this->distance($alldata['lat'], $alldata['lng'], $service5['lat'], $service5['lng'], "M");
            } else {
                $service50['distance'] = 1000;
            }

           // $service59['offer_id']=$service5['id'];
            $service59['service_id']=$service5['sid'];
            $service59['service_name']=$service5['service_name'];


            $a = ServiceOffer::find()->where(['service_id' => $service5['sid']])->all();
            $lowdis=0;
            foreach ($a as $item) {
                if (date("Y-m-d",strtotime($item['offer_end'])) >= date("Y-m-d"))
                {
                    if ($lowdis==0){
                        $now = new DateTime(date("Y-m-d"));
                        $start = new DateTime($item['offer_start']);
                        $end = new DateTime($item['offer_end']);
                        $RemainingDays = $now->diff($end);
                        $TotalDays = $start->diff($end);
                        $service50["service_off_price_percentage"] = $item["discount"];
                        $service50["service_offer_end_date"] = date("Y-m-d",strtotime($item['offer_end']));
                        $service50["service_offer_end_time"] = "23:59:59";
                        $service50["service_off_price"] = round($service5["service_price"]*((100-$item["discount"])/100),2);
                        $service50["service_offer_remainingdays"]=$RemainingDays->days;
                        $service50["service_offer_totaldays"]=$TotalDays->days;
                        $service50["service_discount_price"]=round($service5["service_price"]*(($item["discount"])/100),2);

                        $service59['offer_id']=$item['id'];
                        $service59["service_offer_description"] = $item["offer_desc"];
                        $service59["service_off_price_percentage"] = $item["discount"];
                        $service59["service_offer_end_date"] = date("Y-m-d",strtotime($item['offer_end']));
                        $service59["service_offer_end_time"] = "23:59:59";
                        $service59["service_off_price"] = round($service5["service_price"]*((100-$item["discount"])/100),2);
                        $service59["service_offer_remainingdays"]=$RemainingDays->days;
                        $service59["service_offer_totaldays"]=$TotalDays->days;
                        $service59["service_discount_price"]=round($service5["service_price"]*(($item["discount"])/100),2);
                        $service59["service_price"] = $service5["service_price"];
                        $service59["bookedappointments"]=$totalap;
                        $lowdis=$item["discount"];

                    }elseif($lowdis<$item["discount"]){
                        $now = new DateTime(date("Y-m-d"));
                        $start = new DateTime($item['offer_start']);
                        $end = new DateTime($item['offer_end']);
                        $RemainingDays = $now->diff($end);
                        $TotalDays = $start->diff($end);
                        $service50["service_off_price_percentage"] = $item["discount"];
                        $service50["service_offer_end_date"] = date("Y-m-d",strtotime($item['offer_end']));
                        $service50["service_offer_end_time"] = "23:59:59";
                        $service50["service_off_price"] = round($service5["service_price"]*((100-$item["discount"])/100),2);
                        $service50["service_offer_remainingdays"]=$RemainingDays->days;
                        $service50["service_offer_totaldays"]=$TotalDays->days;
                        $service50["service_discount_price"]=round($service5["service_price"]*(($item["discount"])/100),2);

                        $service59['offer_id']=$item['id'];
                        $service59["service_offer_description"] = $item["offer_desc"];
                        $service59["service_off_price_percentage"] = $item["discount"];
                        $service59["service_offer_end_date"] = date("Y-m-d",strtotime($item['offer_end']));
                        $service59["service_offer_end_time"] = "23:59:59";
                        $service59["service_off_price"] = round($service5["service_price"]*((100-$item["discount"])/100),2);
                        $service59["service_offer_remainingdays"]=$RemainingDays->days;
                        $service59["service_offer_totaldays"]=$TotalDays->days;
                        $service59["service_discount_price"]=round($service5["service_price"]*(($item["discount"])/100),2);
                        $service59["service_price"] = $service5["service_price"];
                        $service59["bookedappointments"]=$totalap;
                        $lowdis=$item["discount"];
                    }else{
                        $now = new DateTime(date("Y-m-d"));
                        $start = new DateTime($item['offer_start']);
                        $end = new DateTime($item['offer_end']);
                        $RemainingDays = $now->diff($end);
                        $TotalDays = $start->diff($end);
                        $service59['offer_id']=$item['id'];
                        $service59["service_offer_description"] = $item["offer_desc"];
                        $service59["service_off_price_percentage"] = $item["discount"];
                        $service59["service_offer_end_date"] = date("Y-m-d",strtotime($item['offer_end']));
                        $service59["service_offer_end_time"] = "23:59:59";
                        $service59["service_off_price"] = round($service5["service_price"]*((100-$item["discount"])/100),2);
                        $service59["service_offer_remainingdays"]=$RemainingDays->days;
                        $service59["service_offer_totaldays"]=$TotalDays->days;
                        $service59["service_discount_price"]=round($service5["service_price"]*(($item["discount"])/100),2);

                        $service59["service_price"] = $service5["service_price"];
                        $service59["bookedappointments"]=$totalap;
                    }
                    if ($lowdis!=0)
                        array_push($data19, $service59);
                }

            }





            if ($lowdis==0){
                $service50["service_offer_remainingdays"]=0;
                $service50["service_offer_totaldays"]=0;
                $service50["service_discount_price"]=0;
                $service50["service_off_price"]=0;
                $service50["service_off_price_percentage"] = 0;
                $service50["service_offer_end_date"] = 0;
                $service50["service_offer_end_time"] = 0;

                $service59["service_offer_remainingdays"]=0;
                $service59["service_offer_totaldays"]=0;
                $service59["service_discount_price"]=0;
                $service59["service_off_price"]=0;
                $service59["service_off_price_percentage"] = 0;
                $service59["service_offer_end_date"] = 0;
                $service59["service_offer_end_time"] = 0;
                $service60="";
            }

                {
                /*$service50["service_offer_remainingdays"] = "No Offer";
                $service50["service_offer_totaldays"] = "No Offer";
                $service50["service_discount_price"] = "No Offer";
                $service50["service_off_price"] = "No Offer";*/

                //$showoffer=$service5["discount"];
                //$service50["service_offer_name"] = $service5['offer_name'];
                    //$service50["service_off_price_percentage"] = 0;
               /* $service50["service_offer_end_date"] = 0;
                $service50["service_offer_end_time"] = 0;
                $service50["service_off_price"] = 0;
                $service59["service_offer_description"] = "";*/

                //$data19=null;
                /*$service59["service_offer_remainingdays"] = "No Offer";
                $service59["service_offer_totaldays"] = "No Offer";
                $service59["service_discount_price"] = "No Offer";
                $service59["service_off_price"] = "No Offer";*/

            }
            $service59["service_price"] = $service5["service_price"];
            $service59["bookedappointments"]=$totalap;
            $service50["views"]=$countt;
            $service50["bookedappointments"]=$totalap;

            $data10=$service50;
            //if ($service60!="")
            //array_push($data19, $service59);
        }
        $datastaff=array();
        $datastaff1=array();
        if (!empty($data5)) {
            $staff = User::find()->where(['business_id' => $service50["business_id"]])->all();
            foreach ($staff as $onestaff){
                $datastaff1['id']=$onestaff['id'];
                $datastaff1['name']=$onestaff['displayname'];
                if (!empty($onestaff['user_profile_image'])) {
                    $datastaff1['user_profile_image'] = "http://boocked.com/admin/web/".$onestaff['user_profile_image'];
                }else{
                    $datastaff1['user_profile_image'] ="http://boocked.com/admin/web/uploads/8413staff2.png";
                }
                //$datastaff1['user_profile_image']=$onestaff['user_profile_image'];
                array_push($datastaff, $datastaff1);
            }
        } else {
            $staff = "";
        }

        $view->service_id = $id;
        $view->date = date("Y-m-d");
        $view->v_count = $countt+1;
        $view->save();


       if (empty($data19)){
           $data19=null;
       }
        $data2['staff'] = $datastaff;
        $data2['offers'] = $data19;
        $data2['services'] = $data10;
        return ['success' => true, 'data' => $data2];


    }

}


