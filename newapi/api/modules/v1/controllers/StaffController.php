<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use yii\rest\Controller;
use api\modules\v1\models\Category;
use api\modules\v1\models\User;
use api\modules\v1\models\Service;
use api\modules\v1\models\CustomerCategory;
use yii;
use yii\db\Query;
use yii\web\JsonParser;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\CompositeAuth;
use yii\web\Response;
/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class StaffController extends Controller
{
    //public $modelClass = 'app\models\User';
    //public $modelClass = 'api\modules\v1\models\Country';

    /**
     * @return string
     */
    public function behaviors(){
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className()
        ];

        return $behaviors;
    }
    public function actionIndex()
    {
        $alldata = json_decode(Yii::$app->request->getRawBody(), true);
        $id = $alldata['id'];
        $service = Service::find()
            ->where(['service_id'=>$id])
            ->one();


        $datastaff=array();
        $datastaff1=array();
        if (!empty($service)) {
            $staff = User::find()->where(['business_id' => $service["business_id"]])->all();
            foreach ($staff as $onestaff){
                $datastaff1['id']=$onestaff['id'];
                $datastaff1['name']=$onestaff['displayname'];
                $datastaff1['rating']=$onestaff['rating']-1;
                $datastaff1['rated_count']=$onestaff['rated_count'];
                if (!empty($onestaff['description'])) {
                    $datastaff1['description'] = $onestaff['description'];
                }else{
                    $datastaff1['description'] ="In Informatics, dummy data is benign information that does not contain any useful data, but serves to reserve space where real data is nominally present. Dummy data can be used as a placeholder for both testing and operational purposes.";
                }
                if (!empty($onestaff['user_profile_image'])) {
                    $datastaff1['user_profile_image'] = "http://boocked.com/admin/web/".$onestaff['user_profile_image'];
                }else{
                    $datastaff1['user_profile_image'] ="http://boocked.com/admin/web/uploads/8413staff2.png";
                }
                array_push($datastaff, $datastaff1);
            }
        } else {
            $staff = "";
        }
        $data2['staff']=$datastaff;
        return ['success' => true, 'data' => $data2];

    }

}


