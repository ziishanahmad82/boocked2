<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use yii\rest\Controller;
use api\modules\v1\models\WeeklyRecurringTime;
use api\modules\v1\models\User;
use api\modules\v1\models\Service;
use api\modules\v1\models\Appointments;
use yii;
use yii\db\Query;
use yii\web\JsonParser;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\CompositeAuth;
use yii\web\Response;
/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class CheckavailbilityController extends Controller
{
    //public $modelClass = 'app\models\User';
    //public $modelClass = 'api\modules\v1\models\Country';

    /**
     * @return string
     */
    /*public function behaviors(){
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className()
        ];

        return $behaviors;
    }*/
    public function actionIndex()
    {

        $mesdata["available"]=array();
        $mesdata["unavailable"]=array();
        $data=json_decode(Yii::$app->request->getRawBody(), true);
        $service = $data['service'];
        $date1 = $data['date'];
        /*$timestamp = strtotime($date);
        $day = strtolower(date('l', $timestamp));*/
        for ($ij=1;$ij<=31;$ij++) {
             $date=$date1."-".$ij;
            //$mesdata[$date] = "no";
            $timestamp = strtotime($date);
            $day = strtolower(date('l', $timestamp));
            $query = new Query;
            if ($data['staff'] != 0) {
                $staff_id = $data['staff'];
                $query
                    ->select([
                            'weekly_recurring_time.*',
                            'services.*']
                    )
                    ->from('weekly_recurring_time')
                    ->join('INNER JOIN', 'services',
                        'services.service_id=weekly_recurring_time.service_id')
                    /*->join('INNER JOIN', 'user',
                        'user.id=weekly_recurring_time.staff_id')*/
                    ->where(['weekly_recurring_time.staff_id' => $staff_id, 'weekly_recurring_time.service_id' => $service])/*->andWhere(['not', ['user.id' => null]])*/
                ;
                $command = $query->createCommand();
                $data5 = $command->queryAll();
            } else {
                $staff_id = "";
                $query
                    ->select([
                            'weekly_recurring_time.*',
                            'services.*']
                    )
                    ->from('weekly_recurring_time')
                    ->join('INNER JOIN', 'services',
                        'services.service_id=weekly_recurring_time.service_id')
                    /*->join('INNER JOIN', 'user',
                        'user.id=weekly_recurring_time.staff_id')*/
                    ->where([/*'weekly_recurring_time.staff_id' => $staff_id,*/
                        'weekly_recurring_time.service_id' => $service])/*->andWhere(['not', ['user.id' => null]])*/
                ;
                $command = $query->createCommand();
                $data5 = $command->queryAll();
            }
            $weekly_times = $data5;

            $appointments = Appointments::find()->where(['appointment_date' => $date])->all();

            if (!empty($weekly_times)) {
                foreach ($weekly_times as $weeklytimes) {

                    $sunday = json_decode($weeklytimes[$day], true);
                    $sunday_size = sizeof($sunday['from']);
                    //    print_r($sunday['to']);
                    //    print_r($sunday['from']);
                    $service_duration = $weeklytimes['service_duration'];

                    $timeslots = array();
                    for ($x = 0; $x != $sunday_size; $x++) {
                        $allSlots = [];
                        $BlockSlots = [];
                        if (!empty($sunday['from'][$x]) && !empty($sunday['to'][$x])) {

                            $sunday['from'][$x] = strtotime($date . ' ' . $sunday['from'][$x]);
                            $sunday['to'][$x] = strtotime($date . ' ' . $sunday['to'][$x]);

                            $ad = 0;
                            while ($sunday['from'][$x] < $sunday['to'][$x]) {
                                if ($ad != 0) {
                                    $sunday['from'][$x] = strtotime('+1 hour', $sunday['from'][$x]);
                                }


                                /*   $sunday['from'][$x] = strtotime($sunday['from'][$x]);
                                   $sunday['to'][$x] = strtotime($sunday['to'][$x]);

                                   while ($sunday['from'][$x] < $sunday['to'][$x]) {
                                       $sunday['from'][$x] = strtotime('+1 hour', $sunday['from'][$x]); */
                                $timeslots[] = $sunday['from'][$x];
                                $ad++;
                            }

                            ?><?php
                            $i = 1;
                            if (!empty($timeslots)) {

                                foreach ($timeslots as $slot) {

                                    $startSlot = $slot;
                                    $endSlot = strtotime('+ 1 hour', $startSlot);
                                    if (!empty($appointments)) {
                                        foreach ($appointments as $appointment) {

                                            // '+'.$service_duration.' minutes', 06:30:00 2016-07-29
                                            $apnt_start_time = strtotime($date . ' ' . $appointment->appointment_start_time);
                                            //   $apnt_start_time = strtotime($appointment->appointment_start_time.' ' );
                                            $apnt_end_time = strtotime('+' . $service_duration . ' minutes', $apnt_start_time);
                                            if (($startSlot > $apnt_start_time && $endSlot > $apnt_start_time && $startSlot > $apnt_end_time && $endSlot >= $apnt_end_time && $startSlot != $apnt_start_time) || ($startSlot < $apnt_start_time && $endSlot <= $apnt_start_time && $startSlot <= $apnt_end_time && $endSlot <= $apnt_end_time && $startSlot != $apnt_start_time)) {

                                                $allSlots[] = $startSlot;


                                            } else {
                                                $BlockSlots[] = $startSlot;

                                            }
                                            $i++;


                                            //$i = 1;
                                        }
                                        if ($i<15){
                                            if(array_search($date,$mesdata["available"])==0){
                                            array_push($mesdata["available"],$date);}
                                            //break;
                                        }
                                    } else {
                                        //$mesdata[$date] = "yes";
                                        if(array_search($date,$mesdata["available"])==0){
                                        array_push($mesdata["available"],$date);
                                            }
                                        //echo 1;
                                        break;
                                        //return ['success' => true, 'data' => $mesdata];
                                    }

                                }
                            }
                        }


                    }
                    //  $allSlots=[];
                    //  print_r('aaa');


                }
            } else {
                //$mesdata['message'] = false;
                //echo 0;
                //break;
               // array_push($mesdata["unavailable"],$date);
               // return ['success' => true, 'data' => $mesdata];
            }
            if (count($mesdata["available"])==1 ){
                if ($mesdata["available"][0]!=$date)
                array_push($mesdata["unavailable"],$date);
            }else
            if(array_search($date,$mesdata["available"])==0){

                array_push($mesdata["unavailable"],$date);
            }
            //$mesdata['message'] = false;
           // echo "<br>";
            //break;
           // return ['success' => true, 'data' => $mesdata];
        }
        return ['success' => true, 'data' => $mesdata];

    }
public function actionIndex1()
    {

        $mesdata=array();
        $data=json_decode(Yii::$app->request->getRawBody(), true);
        $service = $data['service'];
        $date = $data['date'];
        $timestamp = strtotime($date);
        $day = strtolower(date('l', $timestamp));
        $query = new Query;
        if($data['staff'] != 0){
            $staff_id =  $data['staff'];
            $query
                ->select([
                        'weekly_recurring_time.*',
                        'services.*']
                )
                ->from('weekly_recurring_time')
                ->join('INNER JOIN', 'services',
                    'services.service_id=weekly_recurring_time.service_id')
                /*->join('INNER JOIN', 'user',
                    'user.id=weekly_recurring_time.staff_id')*/
                ->where(['weekly_recurring_time.staff_id' => $staff_id, 'weekly_recurring_time.service_id' => $service])
                /*->andWhere(['not', ['user.id' => null]])*/;
            $command = $query->createCommand();
            $data5 = $command->queryAll();
        }else {
            $staff_id = "";
            $query
                ->select([
                        'weekly_recurring_time.*',
                        'services.*']
                )
                ->from('weekly_recurring_time')
                ->join('INNER JOIN', 'services',
                    'services.service_id=weekly_recurring_time.service_id')
                /*->join('INNER JOIN', 'user',
                    'user.id=weekly_recurring_time.staff_id')*/
                ->where([/*'weekly_recurring_time.staff_id' => $staff_id,*/ 'weekly_recurring_time.service_id' => $service])
                /*->andWhere(['not', ['user.id' => null]])*/;
            $command = $query->createCommand();
            $data5 = $command->queryAll();
        }
        $weekly_times=$data5;

        $appointments = Appointments::find()->where(['appointment_date'=>$date])->all();

        if(!empty($weekly_times)) {
            foreach ($weekly_times as $weeklytimes) {

                $sunday = json_decode($weeklytimes[$day] , true);
                $sunday_size = sizeof($sunday['from']);
                //    print_r($sunday['to']);
                //    print_r($sunday['from']);
                $service_duration = $weeklytimes['service_duration'];

                $timeslots = array();
                for ($x = 0; $x != $sunday_size; $x++) {
                    $allSlots = [];
                    $BlockSlots=[];
                    if (!empty($sunday['from'][$x]) && !empty($sunday['to'][$x])) {

                        $sunday['from'][$x] = strtotime($date.' '. $sunday['from'][$x]);
                        $sunday['to'][$x] = strtotime($date.' '. $sunday['to'][$x]);

                        $ad=0;
                        while ($sunday['from'][$x] < $sunday['to'][$x]) {
                            if($ad!=0){
                                $sunday['from'][$x] = strtotime('+1 hour', $sunday['from'][$x]);
                            }


                            /*   $sunday['from'][$x] = strtotime($sunday['from'][$x]);
                               $sunday['to'][$x] = strtotime($sunday['to'][$x]);

                               while ($sunday['from'][$x] < $sunday['to'][$x]) {
                                   $sunday['from'][$x] = strtotime('+1 hour', $sunday['from'][$x]); */
                            $timeslots[] = $sunday['from'][$x];
                            $ad++;
                        }

                        ?><?php
                        $i = 1;
                        if (!empty($timeslots)) {

                            foreach ($timeslots as $slot) {
                                $startSlot = $slot;
                                $endSlot = strtotime('+ 1 hour', $startSlot);
                                if (!empty($appointments)) {
                                    foreach ($appointments as $appointment) {

                                        // '+'.$service_duration.' minutes', 06:30:00 2016-07-29
                                        $apnt_start_time = strtotime($date.' '.$appointment->appointment_start_time);
                                        //   $apnt_start_time = strtotime($appointment->appointment_start_time.' ' );
                                        $apnt_end_time = strtotime('+' . $service_duration . ' minutes', $apnt_start_time);
                                        if (($startSlot > $apnt_start_time && $endSlot > $apnt_start_time && $startSlot > $apnt_end_time && $endSlot >= $apnt_end_time && $startSlot != $apnt_start_time ) || ($startSlot < $apnt_start_time && $endSlot <= $apnt_start_time && $startSlot <= $apnt_end_time && $endSlot <= $apnt_end_time && $startSlot != $apnt_start_time )) {

                                            $allSlots[] = $startSlot;


                                        }else {
                                            $BlockSlots[]=$startSlot;

                                        }
                                        $i++;




                                        $i = 1;
                                    }
                                } else {
                                    $mesdata['message']=true;
                                    return ['success' => true, 'data' =>$mesdata];
                                }

                                ?>


                            <?php }
                        }
                    }


                }
                //  $allSlots=[];
                //  print_r('aaa');


            }
        }else {
            $mesdata['message']=false;
            return ['success' => true, 'data' =>$mesdata];
        }

        $mesdata['message']=false;
        return ['success' => true, 'data' =>$mesdata];


        $query = WeeklyRecurringTime::find()->joinWith(['Service','user']);
        $weekly_times = $query->select(['*'])->where(['weekly_recurring_time.service_id' => $service])
            ->andFilterWhere(['and',['weekly_recurring_time.staff_id' => $staff_id ]])->andWhere(['not', ['user.id' => null]])
            ->all();
//print_r($weekly_times);
        //      exit();
        $appointments = Appointments::find()->where(['appointment_date'=>$date])->all();
        return $weekly_times;









        /* $CategoryList = Category::find()->where(['pro_parent'=>0])->all();
         $data=array();
         $data2=array();
         foreach ($CategoryList as $category){
             $data1['id']=$category['profession_id'];
             $data1['name']=$category['profession_name'];
             array_push($data,$data1);
         }*/
       /* $service = Service::find()
            ->joinWith('business')
            ->orderBy('services.business_id, business_information.business_id')
            ->where(['featured'=>1])
            ->all();*/

        $query = new Query;
        $query
            ->select([
                        'services.*',
                        'business_information.*']
                        )
            ->from('services')
            ->join('INNER JOIN', 'business_information',
                'services.business_id=business_information.business_id')
            ->where(['service_id'=>$id]);
        $command = $query->createCommand();
        $data5 = $command->queryAll();

       if (!empty($data5)) {
           $staff = User::find()->where(['business_id' => $data5[0]['business_id']])->all();
       }else{
           $staff="";
       }

        $data2['staff']=$staff;
        $data2['service']=$data5;
        return ['success' => true, 'data' =>$data2];
    }


    public function actionDateslots()
    {

        $data=json_decode(Yii::$app->request->getRawBody(), true);
        $service = $data['service'];
        $date = $data['date'];
        $timestamp = strtotime($date);
        $day = strtolower(date('l', $timestamp));
        $query = new Query;
        if($data['staff'] != 0){
            $staff_id =  $data['staff'];
            $query
                ->select([
                        'weekly_recurring_time.*',
                        'services.*']
                )
                ->from('weekly_recurring_time')
                ->join('INNER JOIN', 'services',
                    'services.service_id=weekly_recurring_time.service_id')
                /*->join('INNER JOIN', 'user',
                    'user.id=weekly_recurring_time.staff_id')*/
                ->where(['weekly_recurring_time.staff_id' => $staff_id, 'weekly_recurring_time.service_id' => $service])
                /*->andWhere(['not', ['user.id' => null]])*/;
            $command = $query->createCommand();
            $data5 = $command->queryAll();
        }else {
            $staff_id = "";
            $query
                ->select([
                        'weekly_recurring_time.*',
                        'services.*']
                )
                ->from('weekly_recurring_time')
                ->join('INNER JOIN', 'services',
                    'services.service_id=weekly_recurring_time.service_id')
                /*->join('INNER JOIN', 'user',
                    'user.id=weekly_recurring_time.staff_id')*/
                ->where([/*'weekly_recurring_time.staff_id' => $staff_id,*/ 'weekly_recurring_time.service_id' => $service])
                /*->andWhere(['not', ['user.id' => null]])*/;
            $command = $query->createCommand();
            $data5 = $command->queryAll();
        }
        if (isset($data5[0]['service_duration']) and $data5[0]['service_duration']>0){
        $service_duration=$data5[0]['service_duration'];}else{
            $service_duration=60;
        }
        $weekly_times=$data5;

        $appointments = Appointments::find()->where(['appointment_date'=>$date])->all();

        /*print_r($weekly_times);
        die();*/
        if(!empty($weekly_times)) {
            foreach ($weekly_times as $weeklytimes) {
                $staff = $weeklytimes['staff_id'];
                $sunday = json_decode($weeklytimes[$day], true);
                $sunday_size = sizeof($sunday['from']);
                //    print_r($sunday['to']);
                //    print_r($sunday['from']);
                $service_duration = $weeklytimes['service_duration'];

                $timeslots = array();
                for ($x = 0; $x != $sunday_size; $x++) {
                    $allSlots = array();
                    $BlockSlots = array();
                    if (!empty($sunday['from'][$x]) && !empty($sunday['to'][$x])) {

                        $sunday['from'][$x] = strtotime($date . ' ' . $sunday['from'][$x]);
                        $sunday['to'][$x] = strtotime($date . ' ' . $sunday['to'][$x]);
                        $abiterate = 0;
                        $ad = 0;
                        while ($sunday['from'][$x] < $sunday['to'][$x]) {
                            if ($ad != 0) {
                                $sunday['from'][$x] = strtotime('+'.$service_duration.' minutes', $sunday['from'][$x]);
                            }

                            /*   $sunday['from'][$x] = strtotime($sunday['from'][$x]);
                               $sunday['to'][$x] = strtotime($sunday['to'][$x]);

                               while ($sunday['from'][$x] < $sunday['to'][$x]) {
                                   $sunday['from'][$x] = strtotime('+1 hour', $sunday['from'][$x]); */
                            /*$timeslots[] = $sunday['from'][$x];
                            $ad++;*/


                            $timeslot[] = date('Y-m-d H:i:s', $sunday['from'][$x]);
                            $timeslots[] = $sunday['from'][$x];
                            $abiterate++;
                            $ad++;
                        }
                        //print_r($timeslots);

                        $i = 1;
                        //return $timeslots;
                        if ($abiterate > 0) {
                            foreach ($timeslots as $slot) {
                                $startSlot = $slot;
                                $endSlot = strtotime('+ 1 hour', $startSlot);
                                if (!empty($appointments)) {
                                    foreach ($appointments as $appointment) {
                                        //   if ($i == 1) {
                                        //     echo 'Apnt '.$appointment->appointment_start_time.'               slot '.date('Y-m-d H:i:s', $startSlot).'               ';
                                        // '+'.$service_duration.' minutes', 06:30:00 2016-07-29
                                        $apnt_start_time = strtotime($date . ' ' . $appointment->appointment_start_time);
                                        //     echo date('Y-m-d H:i:s', $apnt_start_time).' <br/> ';
                                        //  $apnt_start_time = strtotime('02:50:00 2016-07-29');
                                        $apnt_end_time = strtotime('+' . $service_duration . ' minutes', $apnt_start_time);
                                        if (($startSlot > $apnt_start_time && $endSlot > $apnt_start_time && $startSlot > $apnt_end_time && $endSlot >= $apnt_end_time && $startSlot != $apnt_start_time) || ($startSlot < $apnt_start_time && $endSlot <= $apnt_start_time && $startSlot <= $apnt_end_time && $endSlot <= $apnt_end_time && $startSlot != $apnt_start_time)) {
                                            //   echo 'appointment not exist in slot';
                                            //   echo 'hereeee   ';
                                            //   echo date('Y-m-d H:i:s', $startSlot) . '      ' . date('Y-m-d H:i:s', $endSlot) . '  appointmnet between   ' . date('Y-m-d H:i:s', $apnt_start_time) . ' to  ' . date('Y-m-d H:i:s', $apnt_end_time) . '<br/>';


                                            $allSlots['slot'][] = $startSlot;

                                            $allSlots['staff'][] = $weeklytimes['staff_id'];
                                            $allSlots['service'][] = $weeklytimes['service_id'];


                                        } else {
                                            $BlockSlots['slot'][] = $startSlot;
                                            $allSlots['staff'][] = $weeklytimes['staff_id'];
                                            $allSlots['service'][] = $weeklytimes['service_id'];

                                        }

                                        $i++;

                                    }

                                } else {
                                    $allSlots['slot'][] = $startSlot;
                                    $allSlots['staff'][] = $weeklytimes['staff_id'];
                                    $allSlots['service'][] = $weeklytimes['service_id'];


                                }
                                $i = 1;
                            }
                        } else {
                            $allSlots['slot'] = [];
                            $timeslots = [];


                        }
//exit();

                        ?><?php } else {

                        //echo 'empty';

                    }
                }
                //echo "1";
            }
        }else{
            $allSlots['slot']=array();
            $timeslots = array();
        }
        $new2ar['availabletimes']=array();
              // print_r($allSlots);
            //die();
//print_r($BlockSlots['slot']);
            //      print_r($allSlots['staff']);
            if (!isset($allSlots['slot']) OR empty($allSlots['slot'])){
                $allSlots['slot']=array();

                return ['success' => false,'msg' => 'No time available', 'data' => $new2ar];
            }
            $allSlots['slot'] = array_unique($allSlots['slot']);

        //print_r($allSlots['slot']);

          $BlockSlots['slot']=array();
           // foreach ($allSlots['slot'] as $slot){
                //        echo '  all slot '. date('Y-m-d H:i:s', $slot).'<br/>';;

           // }
            //  echo '<pre>';
            //  print_r($BlockSlots['slot']);
            //  print_r(array_diff($allSlots['slot'], $BlockSlots['slot']));
            //  echo '</pre>';
            $allSlots['slot']  =  array_diff($allSlots['slot'], $BlockSlots['slot']);
            // print_r($allSlots['slot']);
            // print_r($BlockSlots['slot']);
            //  exit();
        $new1ar=array();
        $new2ar=array();
        $new2ar['availabletimes']=array();
        foreach ($allSlots['slot'] as $sslot){
            $new1ar['id']=$sslot;
            $new1ar['time_slot']=date('g:i A', $sslot)." to ".date('g:i A',strtotime('+'.$service_duration.' minutes',$sslot));
            //$new1ar['etime']=date('g:i:s A',strtotime('+'.$service_duration.' minutes',$sslot));
            array_push($new2ar['availabletimes'],$new1ar);
        }
        //print_r($new2ar);
        //die();
            // print_r($allSlots);

       /* foreach($allSlots['slot'] as $slot){
            echo date('h:i A', $slot);
        }*/
       // die();

        return ['success' => true, 'data' => $new2ar];

        }

}


