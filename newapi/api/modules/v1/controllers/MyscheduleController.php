<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use yii\rest\Controller;
use api\modules\v1\models\Category;
use api\modules\v1\models\Appointments;
use api\modules\v1\models\User;
use api\modules\v1\models\Service;
use api\modules\v1\models\CustomerCategory;
use yii;
use yii\db\Query;
use yii\web\JsonParser;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\CompositeAuth;
use yii\web\Response;
/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class MyscheduleController extends Controller
{
    //public $modelClass = 'app\models\User';
    //public $modelClass = 'api\modules\v1\models\Country';

    /**
     * @return string
     */
    public function behaviors(){
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className()
        ];

        return $behaviors;
    }
    public function actionIndex()
    {
        $all=Yii::$app->user->identity;
        $id=$all['id'];

        $query = new Query;
        $query->select([
                'services.*',
                'user.*',
                'services.service_id as sid',
                'business_information.*',
                'appointments.*']
        )
            ->from('services')
            ->join('INNER JOIN', 'business_information',
                'services.business_id=business_information.business_id')
            ->join('INNER JOIN', 'appointments',
                'services.service_id=appointments.service_id')
            ->join('INNER JOIN', 'user',
                'user.id=appointments.user_id')
            ->where(['appointments.customer_id' => $id]);
        $command = $query->createCommand();
        $data5 = $command->queryAll();


        $dataservice1=array();
        $dataservice2=array();
        foreach ($data5 as $oneservice) {
            if (strtotime($oneservice['appointment_date']) >= strtotime(date("Y-m-d"))){
            $dataservice['appointment_id'] = $oneservice['appointment_id'];
            $dataservice['service_id'] = $oneservice['service_id'];
            $dataservice['service_name'] = $oneservice['service_name'];
            $dataservice['service_description'] = $oneservice['service_description'];
            $dataservice['staff_name'] = $oneservice['displayname'];
            //$dataservice['staff_image'] = $oneservice['user_profile_image'];
            if (!empty($oneservice['user_profile_image'])) {
                $dataservice['staff_image'] = "http://boocked.com/admin/web/".$oneservice['user_profile_image'];
            }else{
                $dataservice['staff_image'] ="http://boocked.com/admin/web/uploads/8413staff2.png";
            }
            $dataservice['staff_mobile'] = $oneservice['mobile_phone'];
            $dataservice['appointment_start_time'] = $oneservice['appointment_start_time'];
            $dataservice['appointment_date'] = $oneservice['appointment_date'];
            //$dataservice['appointment_status'] = $oneservice['appointment_status'];
            array_push($dataservice1, $dataservice);

        }else{
                $dataservice['appointment_id'] = $oneservice['appointment_id'];
                $dataservice['service_id'] = $oneservice['service_id'];
                $dataservice['service_name'] = $oneservice['service_name'];
                $dataservice['service_description'] = $oneservice['service_description'];
                $dataservice['staff_name'] = $oneservice['displayname'];
                //$dataservice['staff_image'] = $oneservice['user_profile_image'];
                if (!empty($oneservice['user_profile_image'])) {
                    $dataservice['staff_image'] = "http://boocked.com/admin/web/".$oneservice['user_profile_image'];
                }else{
                    $dataservice['staff_image'] ="http://boocked.com/admin/web/uploads/8413staff2.png";
                }
                if (!empty($oneservice['mobile_phone'])) {
                    $dataservice['staff_mobile'] = $oneservice['mobile_phone'];
                }else{
                    $dataservice['staff_mobile'] = "0321212111";
                }
                $dataservice['appointment_start_time'] = $oneservice['appointment_start_time'];
                $dataservice['appointment_date'] = $oneservice['appointment_date'];
                //$dataservice['appointment_status'] = $oneservice['appointment_status'];
                array_push($dataservice2, $dataservice);
            }
        }
        $data2=array();
        $data2['upcomingschedules']=$dataservice1;
        $data2['pastschedules']=$dataservice2;
        return ['success' => true, 'data' => $data2];




        $alldata = json_decode(Yii::$app->request->getRawBody(), true);
        $id = $alldata['id'];
        $service = Service::find()
            ->where(['service_id'=>$id])
            ->one();


        $datastaff=array();
        $datastaff1=array();
        if (!empty($service)) {
            $staff = User::find()->where(['business_id' => $service["business_id"]])->all();
            foreach ($staff as $onestaff){
                $datastaff1['id']=$onestaff['id'];
                $datastaff1['name']=$onestaff['displayname'];
                $datastaff1['rating']=$onestaff['rating']-1;
                $datastaff1['rated_count']=$onestaff['rated_count'];
                $datastaff1['description']=$onestaff['description'];
                if (!empty($onestaff['user_profile_image'])) {
                    $datastaff1['user_profile_image'] = "http://boocked.com/admin/web/".$onestaff['user_profile_image'];
                }else{
                    $datastaff1['user_profile_image'] ="http://boocked.com/admin/web/uploads/8413staff2.png";
                }
                array_push($datastaff, $datastaff1);
            }
        } else {
            $staff = "";
        }
        $data2['staff']=$datastaff;
        return ['success' => true, 'data' => $data2];

    }

    function actionDelete()
    {
        $all=Yii::$app->user->identity;
        $customer_id=$all['customer_id'];

        $data=json_decode(Yii::$app->request->getRawBody(), true);
        $appointment_id = $data['appointment_id'];
        $appointment_find=Appointments::find()->where(['appointment_id'=>$appointment_id])->one();

        if ($appointment_find['customer_id']==$customer_id){
            $appointment_find->delete();
            $data2['msg']=1;
            return ['success' => true,'message'=>'Successfully Deleted', 'data' => $data2];
        }
        $data2['msg']=0;
        return ['success' => true,'message'=>'Not Deleted', 'data' => $data2];


    }
    function actionUpdate()
    {
        $all=Yii::$app->user->identity;
        $customer_id=$all['customer_id'];

        $data=json_decode(Yii::$app->request->getRawBody(), true);
        $appointment_id = $data['appointment_id'];
        $appointment_time = $data['time'];
        $appointment_date = $data['date'];
        $appointment_find=Appointments::find()->where(['appointment_id'=>$appointment_id])->one();

        if ($appointment_find['customer_id']==$customer_id){
            $appointment_find->appointment_start_time=$appointment_time;
            $appointment_find->appointment_date=$appointment_date;
            $appointment_find->save();
            $data2['msg']=1;
            return ['success' => true,'message'=>'Successfully Updated', 'data' => $data2];
        }
        $data2['msg']=0;
        return ['success' => true,'message'=>'Not Updated', 'data' => $data2];
    }

}


