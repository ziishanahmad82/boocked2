<?php

namespace api\modules\v1\controllers;


use yii\rest\Controller;
use api\modules\v1\models\Customers;
use common\models\User;
use api\modules\v1\models\CustomersSearch;
use yii;
use Twilio;
use Services_Twilio;
use yii\web\JsonParser;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers;
use yii\base\Security;


/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class VerifyemailController extends Controller
{
    //public $modelClass = 'app\models\User';
    //public $modelClass = 'api\modules\v1\models\Sms';

    public function actionIndex()
    {
        return "0";
    }
    public function actionVerifyemail()
    {
        $data=json_decode(Yii::$app->request->getRawBody(), true);
        $email=$data['email'];
        $CustomerList = Customers::find()->where(['email'=> $email])->one();
$aa=array();
        if (!empty($CustomerList)){
            $value=true;
            $value1="Email Found";
        }else{
            $value=false;
            $value1="Email Not Found";
        }
        return ['success' => true,'is_exist'=>$value , 'message' => $value1, 'data'=>$aa];
    }
}