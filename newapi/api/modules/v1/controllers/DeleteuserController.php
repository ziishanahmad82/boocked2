<?php

namespace api\modules\v1\controllers;


use yii\rest\Controller;
use api\modules\v1\models\Customers;
use common\models\User;
use api\modules\v1\models\CustomersSearch;
use yii;
use Twilio;
use Services_Twilio;
use yii\web\JsonParser;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers;
use yii\base\Security;


/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class DeleteuserController extends Controller
{


    public function actionIndex($num="")
    {
        $CustomerList = Customers::find()->where(['cell_phone'=> '+'.$num])->all();
        if ($num!="") {
            \Yii::$app
                ->db
                ->createCommand()
                ->delete('customers', ['cell_phone' => '+' . $num])
                ->execute();
            //echo $num;
            return true;
        }else{
            return fasle;
        }

    }

}

