<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\ServiceOffer;
use api\modules\v1\models\Staff;
use yii\rest\ActiveController;
use yii\rest\Controller;
use api\modules\v1\models\WeeklyRecurringTime;
use api\modules\v1\models\User;
use api\modules\v1\models\Service;
use api\modules\v1\models\Appointments;
use yii;
use yii\db\Query;
use yii\web\JsonParser;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\CompositeAuth;
use yii\web\Response;
/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class BookingController extends Controller
{
    //public $modelClass = 'app\models\User';
    //public $modelClass = 'api\modules\v1\models\Country';

    /**
     * @return string
     */
    public function behaviors(){
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className()
        ];

        return $behaviors;
    }
    public function actionIndex()
    {
        $all=Yii::$app->user->identity;
        $customer_id=$all['customer_id'];

        $data=json_decode(Yii::$app->request->getRawBody(), true);
        $service = $data['service'];


        if (isset($data['offer'])){
            $offer = $data['offer'];
            $soffer = ServiceOffer::find()->where(['service_id' => $service])->one();
            if (!empty($soffer)){
                $sprice=$soffer['discount'];
            }else{
                $sprice=0;
            }

        }else{
            $sprice=0;
            $offer = 0;
        }
        $date = $data['date'];
        $time = date('h:i:s', $data['timing']);
        $user_id=$data['staff'];
        $staffid = User::find()->where(['id' => $user_id])->one();
        $date1=date_create($date);
        $date2=date_create(date("Y-m-d"));
        $diff=date_diff($date2,$date1);
        $dday= $diff->format("%R%a");


        if (!empty($service) AND !empty($date) AND !empty($time) AND $dday>=0){
            $appointment_find=Appointments::find()->where(['service_id'=>$service,'user_id'=>$user_id,'appointment_date'=>$date,'appointment_start_time'=>$time])->one();

            if (empty($appointment_find)) {

                $a = Service::find()->where(['service_id' => $service])->one();
                $business = $a['business_id'];
                $appointment = new Appointments;
                $appointment->service_id = $service;
                $appointment->offer_id = $offer;
                $appointment->customer_id = $customer_id;
                $appointment->user_id = $user_id;
                $appointment->appointment_start_time = $time;
                $appointment->appointment_date = $date;
                $appointment->business_id = $business;
                $appointment->save();
                if ($sprice==0){
                    $sprice= $a['service_price'];
                }
                $msgdata['service_name']=$a['service_name'];
                $msgdata['date']=$date;
                $msgdata['time']=$time;
                $msgdata['price']=$sprice;
                $msgdata['staff_id']=$user_id;
                if ($staffid['mobile_phone']==""){
                    $msgdata['staff_phone']="03211234567";
                }else{
                    $msgdata['staff_phone']=$staffid['mobile_phone'];
                }

                $msgdata['staff_name']=$staffid['displayname'];
                $msgdata['staff_image']=$staffid['user_profile_image'];
                if (empty($staffid['user_profile_image'])) {
                    $msgdata["staff_image"] = "http://boocked.com/admin/web/uploads/5215service.png";
                } else {
                    $msgdata["staff_image"] = "http://boocked.com/admin/web/" . $staffid["user_profile_image"];
                }
                //$msgdata['service_name']=$a['service_name'];
                //$msgdata['service_name']=$a['service_name'];



                $message = "inserted";
                return ['success' => true,'message'=>$message, 'data' =>$msgdata];
            }else{
                $message = "already used this slot";
                return ['success' => true,'message'=>$message,  'data' =>$message];
            }
            return ['success' => true,'message'=>$message, 'data' =>$data12];


        }
        $message="Error";
        $data12=array();
        return ['success' => false,'message'=>$message, 'data' =>$data12];
    }
    public function actionDelete()
    {
        $all=Yii::$app->user->identity;
        $customer_id=$all['customer_id'];

        $data=json_decode(Yii::$app->request->getRawBody(), true);
        $appointment_id = $data['appointment_id'];

        $query = new Query;
        $query->select([
                'services.*',
                'user.*',
                'services.service_id as sid',
                'business_information.*',
                'appointments.*']
        )
            ->from('services')
            ->join('INNER JOIN', 'business_information',
                'services.business_id=business_information.business_id')
            ->join('INNER JOIN', 'appointments',
                'services.service_id=appointments.service_id')
            ->join('INNER JOIN', 'user',
                'user.id=appointments.user_id')
            ->where(['appointments.appointment_id' => $appointment_id]);
        $command = $query->createCommand();
        $data5 = $command->queryOne();
        print_r($data5);
        die();

        //$appointment = new Appointments;
        //Appointments::delete('appointment_id = '.$appointment_id);
    }
    public function actionIndex1()
    {

        $data=json_decode(Yii::$app->request->getRawBody(), true);
        $service = $data['service'];
        $date = $data['date'];
        $timestamp = strtotime($date);
        $day = strtolower(date('l', $timestamp));
        $query = new Query;
        if(isset($data['staff']) AND $data['staff'] != 0){
            $staff_id =  $data['staff'];
            $query
                ->select([
                        'weekly_recurring_time.*',
                        'services.*']
                )
                ->from('weekly_recurring_time')
                ->join('INNER JOIN', 'services',
                    'services.service_id=weekly_recurring_time.service_id')
                /*->join('INNER JOIN', 'user',
                    'user.id=weekly_recurring_time.staff_id')*/
                ->where(['weekly_recurring_time.staff_id' => $staff_id, 'weekly_recurring_time.service_id' => $service])
                /*->andWhere(['not', ['user.id' => null]])*/;
            $command = $query->createCommand();
            $data5 = $command->queryAll();
        }else {
            $staff_id = "";
            $query
                ->select([
                        'weekly_recurring_time.*',
                        'services.*']
                )
                ->from('weekly_recurring_time')
                ->join('INNER JOIN', 'services',
                    'services.service_id=weekly_recurring_time.service_id')
                /*->join('INNER JOIN', 'user',
                    'user.id=weekly_recurring_time.staff_id')*/
                ->where([/*'weekly_recurring_time.staff_id' => $staff_id,*/ 'weekly_recurring_time.service_id' => $service])
                /*->andWhere(['not', ['user.id' => null]])*/;
            $command = $query->createCommand();
            $data5 = $command->queryAll();
        }
        $weekly_times=$data5;

        $appointments = Appointments::find()->where(['appointment_date'=>$date])->all();

        if(!empty($weekly_times)) {
            foreach ($weekly_times as $weeklytimes) {

                $sunday = json_decode($weeklytimes[$day] , true);
                $sunday_size = sizeof($sunday['from']);
                //    print_r($sunday['to']);
                //    print_r($sunday['from']);
                $service_duration = $weeklytimes['service_duration'];

                $timeslots = array();
                for ($x = 0; $x != $sunday_size; $x++) {
                    $allSlots = [];
                    $BlockSlots=[];
                    if (!empty($sunday['from'][$x]) && !empty($sunday['to'][$x])) {

                        $sunday['from'][$x] = strtotime($date.' '. $sunday['from'][$x]);
                        $sunday['to'][$x] = strtotime($date.' '. $sunday['to'][$x]);

                        $ad=0;
                        while ($sunday['from'][$x] < $sunday['to'][$x]) {
                            if($ad!=0){
                                $sunday['from'][$x] = strtotime('+1 hour', $sunday['from'][$x]);
                            }


                            /*   $sunday['from'][$x] = strtotime($sunday['from'][$x]);
                               $sunday['to'][$x] = strtotime($sunday['to'][$x]);

                               while ($sunday['from'][$x] < $sunday['to'][$x]) {
                                   $sunday['from'][$x] = strtotime('+1 hour', $sunday['from'][$x]); */
                            $timeslots[] = $sunday['from'][$x];
                            $ad++;
                        }

                        ?><?php
                        $i = 1;
                        if (!empty($timeslots)) {

                            foreach ($timeslots as $slot) {
                                $startSlot = $slot;
                                $endSlot = strtotime('+ 1 hour', $startSlot);
                                if (!empty($appointments)) {
                                    foreach ($appointments as $appointment) {

                                        // '+'.$service_duration.' minutes', 06:30:00 2016-07-29
                                        $apnt_start_time = strtotime($date.' '.$appointment->appointment_start_time);
                                        //   $apnt_start_time = strtotime($appointment->appointment_start_time.' ' );
                                        $apnt_end_time = strtotime('+' . $service_duration . ' minutes', $apnt_start_time);
                                        if (($startSlot > $apnt_start_time && $endSlot > $apnt_start_time && $startSlot > $apnt_end_time && $endSlot >= $apnt_end_time && $startSlot != $apnt_start_time ) || ($startSlot < $apnt_start_time && $endSlot <= $apnt_start_time && $startSlot <= $apnt_end_time && $endSlot <= $apnt_end_time && $startSlot != $apnt_start_time )) {

                                            $allSlots[] = $startSlot;


                                        }else {
                                            $BlockSlots[]=$startSlot;

                                        }
                                        $i++;




                                        $i = 1;
                                    }
                                } else {
                                    return ['success' => true, 'data' =>1];
                                }

                                ?>


                            <?php }
                        }
                    }


                }
                //  $allSlots=[];
                //  print_r('aaa');


            }
        }else {

            return ['success' => true, 'data' =>0];
        }

        return ['success' => true, 'data' =>0];

        $query = WeeklyRecurringTime::find()->joinWith(['Service','user']);
        $weekly_times = $query->select(['*'])->where(['weekly_recurring_time.service_id' => $service])
            ->andFilterWhere(['and',['weekly_recurring_time.staff_id' => $staff_id ]])->andWhere(['not', ['user.id' => null]])
            ->all();
//print_r($weekly_times);
        //      exit();
        $appointments = Appointments::find()->where(['appointment_date'=>$date])->all();
        return $weekly_times;









        /* $CategoryList = Category::find()->where(['pro_parent'=>0])->all();
         $data=array();
         $data2=array();
         foreach ($CategoryList as $category){
             $data1['id']=$category['profession_id'];
             $data1['name']=$category['profession_name'];
             array_push($data,$data1);
         }*/
       /* $service = Service::find()
            ->joinWith('business')
            ->orderBy('services.business_id, business_information.business_id')
            ->where(['featured'=>1])
            ->all();*/

        $query = new Query;
        $query
            ->select([
                        'services.*',
                        'business_information.*']
                        )
            ->from('services')
            ->join('INNER JOIN', 'business_information',
                'services.business_id=business_information.business_id')
            ->where(['service_id'=>$id]);
        $command = $query->createCommand();
        $data5 = $command->queryAll();

       if (!empty($data5)) {
           $staff = User::find()->where(['business_id' => $data5[0]['business_id']])->all();
       }else{
           $staff="";
       }

        $data2['staff']=$staff;
        $data2['service']=$data5;
        return ['success' => true, 'data' =>$data2];
    }


    public function actionTimeslots()
    {

        $data=json_decode(Yii::$app->request->getRawBody(), true);
        $service = $data['service'];
        $date = $data['date'];
        $timestamp = strtotime($date);
        $day = strtolower(date('l', $timestamp));
        $query = new Query;
        if($data['staff'] != 0){
            $staff_id =  $data['staff'];
            $query
                ->select([
                        'weekly_recurring_time.*',
                        'services.*']
                )
                ->from('weekly_recurring_time')
                ->join('INNER JOIN', 'services',
                    'services.service_id=weekly_recurring_time.service_id')
                /*->join('INNER JOIN', 'user',
                    'user.id=weekly_recurring_time.staff_id')*/
                ->where(['weekly_recurring_time.staff_id' => $staff_id, 'weekly_recurring_time.service_id' => $service])
                /*->andWhere(['not', ['user.id' => null]])*/;
            $command = $query->createCommand();
            $data5 = $command->queryAll();
        }else {
            $staff_id = "";
            $query
                ->select([
                        'weekly_recurring_time.*',
                        'services.*']
                )
                ->from('weekly_recurring_time')
                ->join('INNER JOIN', 'services',
                    'services.service_id=weekly_recurring_time.service_id')
                /*->join('INNER JOIN', 'user',
                    'user.id=weekly_recurring_time.staff_id')*/
                ->where([/*'weekly_recurring_time.staff_id' => $staff_id,*/ 'weekly_recurring_time.service_id' => $service])
                /*->andWhere(['not', ['user.id' => null]])*/;
            $command = $query->createCommand();
            $data5 = $command->queryAll();
        }
        $weekly_times=$data5;

        $appointments = Appointments::find()->where(['appointment_date'=>$date])->all();

       // if(!empty($weekly_times)) {
            foreach ($weekly_times as $weeklytimes) {
                $staff=$weeklytimes['staff_id'];
                $sunday = json_decode($weeklytimes[$day] , true);
                $sunday_size = sizeof($sunday['from']);
                //    print_r($sunday['to']);
                //    print_r($sunday['from']);
                $service_duration = $weeklytimes['service_duration'];

                $timeslots = array();
                for ($x = 0; $x != $sunday_size; $x++) {
                    $allSlots = array();
                    $BlockSlots=array();
                    if (!empty($sunday['from'][$x]) && !empty($sunday['to'][$x])) {

                        $sunday['from'][$x] = strtotime($date.' '. $sunday['from'][$x]);
                        $sunday['to'][$x] = strtotime($date.' '. $sunday['to'][$x]);
                        $abiterate = 0;
                        $ad=0;
                        while ($sunday['from'][$x] < $sunday['to'][$x]) {
                            if($ad!=0){
                                $sunday['from'][$x] = strtotime('+1 hour', $sunday['from'][$x]);
                            }


                            /*   $sunday['from'][$x] = strtotime($sunday['from'][$x]);
                               $sunday['to'][$x] = strtotime($sunday['to'][$x]);

                               while ($sunday['from'][$x] < $sunday['to'][$x]) {
                                   $sunday['from'][$x] = strtotime('+1 hour', $sunday['from'][$x]); */
                            /*$timeslots[] = $sunday['from'][$x];
                            $ad++;*/


                            $timeslot[] = date('Y-m-d H:i:s',$sunday['from'][$x]);
                            $timeslots[] = $sunday['from'][$x];
                            $abiterate++;
                            $ad++;
                        }

                        ?>
                        <?php
                        $i = 1;
                        //return $timeslots;
                        if ($abiterate > 0) {
                            foreach ($timeslots as $slot) {
                                $startSlot = $slot;
                                $endSlot = strtotime('+ 1 hour',$startSlot);
                                if (!empty($appointments)) {
                                    foreach ($appointments as $appointment) {
                                        //   if ($i == 1) {
                                        //     echo 'Apnt '.$appointment->appointment_start_time.'               slot '.date('Y-m-d H:i:s', $startSlot).'               ';
                                        // '+'.$service_duration.' minutes', 06:30:00 2016-07-29
                                        $apnt_start_time = strtotime($date.' '.$appointment->appointment_start_time);
                                        //     echo date('Y-m-d H:i:s', $apnt_start_time).' <br/> ';
                                        //  $apnt_start_time = strtotime('02:50:00 2016-07-29');
                                        $apnt_end_time = strtotime('+' . $service_duration . ' minutes', $apnt_start_time);
                                        if (($startSlot > $apnt_start_time && $endSlot > $apnt_start_time && $startSlot > $apnt_end_time && $endSlot >= $apnt_end_time && $startSlot != $apnt_start_time ) || ($startSlot < $apnt_start_time && $endSlot <= $apnt_start_time && $startSlot <= $apnt_end_time && $endSlot <= $apnt_end_time && $startSlot != $apnt_start_time )) {
                                            //   echo 'appointment not exist in slot';
                                            //   echo 'hereeee   ';
                                            //   echo date('Y-m-d H:i:s', $startSlot) . '      ' . date('Y-m-d H:i:s', $endSlot) . '  appointmnet between   ' . date('Y-m-d H:i:s', $apnt_start_time) . ' to  ' . date('Y-m-d H:i:s', $apnt_end_time) . '<br/>';


                                            $allSlots['slot'][] = $startSlot;
                                            $allSlots['staff'][] = $weeklytimes->staff_id;
                                            $allSlots['service'][] = $weeklytimes->service_id;





                                        }else {
                                            $BlockSlots['slot'][]=$startSlot;
                                            $BlockSlots['staff'][] = $weeklytimes->staff_id;
                                            $BlockSlots['service'][] = $weeklytimes->service_id;

                                        }

                                        $i++;

                                    }

                                } else {
                                    $allSlots['slot'][] = $startSlot;
                                    $allSlots['staff'][] = $weeklytimes['staff_id'];
                                    $allSlots['service'][] = $weeklytimes['service_id'];


                                }
                                $i = 1;
                            }
                        }else {
                            $allSlots['slot'] =[];
                            $timeslots=[];


                        }
//exit();

                        ?>


                    <?php }else {

                         //echo 'empty';

                    }
                }
                //echo "1";
            }
              // print_r($allSlots);
            //die();
//print_r($BlockSlots['slot']);
            //      print_r($allSlots['staff']);

            $allSlots['slot'] = array_unique($allSlots['slot']);
          $BlockSlots['slot']=array();
           // foreach ($allSlots['slot'] as $slot){
                //        echo '  all slot '. date('Y-m-d H:i:s', $slot).'<br/>';;

           // }
            //  echo '<pre>';
            //  print_r($BlockSlots['slot']);
            //  print_r(array_diff($allSlots['slot'], $BlockSlots['slot']));
            //  echo '</pre>';
            $allSlots['slot']  =  array_diff($allSlots['slot'], $BlockSlots['slot']);
            // print_r($allSlots['slot']);
            // print_r($BlockSlots['slot']);
            //  exit();

            // print_r($allSlots);

       /* foreach($allSlots['slot'] as $slot){
            echo date('h:i A', $slot);
        }*/
       // die();
            return ([
                'allSlots' => $allSlots,
                'totalslots'=>$timeslots,
                'staff'=>$staff,
            ]);

        }

}


