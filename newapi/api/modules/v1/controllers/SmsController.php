<?php

namespace api\modules\v1\controllers;


use yii\rest\Controller;
use api\modules\v1\models\Customers;
use common\models\User;
use api\modules\v1\models\CustomersSearch;
use yii;
use Twilio;
use Services_Twilio;
use yii\web\JsonParser;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers;
use yii\base\Security;


/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class SmsController extends Controller
{
    //public $modelClass = 'app\models\User';
    //public $modelClass = 'api\modules\v1\models\Sms';


    /**
     * @return string
     */
    public function actionIndex4()
    {

        return "GET";
    }
   
    public function actionNewaccount()
    {
        $request = Yii::$app->request;
        //$data=json_decode(Yii::$app->request->getRawBody(), true);
        $uploads = \yii\web\UploadedFile::getInstanceByName('upfile');
        if (empty($uploads)){
            return ['success' => false, 'message' => 'please add profile pic'];
        }
        $img='uploads/' .date("Y-m-d_H_i_s."). $uploads->name;
        $uploads->saveAs($img);
        $img_link="/newapi/api/web/".$img;
        $name=$request->post('number');
        if (!empty($name) AND !empty($request->post('email')) AND !empty($request->post('first')) AND !empty($request->post('last')) ) {
            $CustomerList = Customers::find()->where(['like', 'cell_phone', $name])->all();

            if (empty($CustomerList)) {
                $customers = new Customers;
                $customers->cell_phone = $name;
                $customers->email = $request->post('email');
                $customers->first_name = $request->post('first');
                $customers->last_name = $request->post('last');
                $customers->profile_image = $img_link;
                $customers->auth_key = $this->generateRandomString(30);
                $customers->save();
                $CustomerList = Customers::find()->where(['cell_phone'=> $name])->one();

                if (!empty($CustomerList)){

                    $data1['first_name']=$CustomerList['first_name'];
                    $data1['last_name']=$CustomerList['last_name'];
                    $data1['email']=$CustomerList['email'];
                    $data1['cell_phone']=$CustomerList['cell_phone'];
                    $data1['auth_key']=$CustomerList['auth_key'];
                    $data1['profile_image']=$CustomerList['profile_image'];

                }else{
                    $data1=0;
                }
                return ['success' => true, 'message' => 'Successfully Register','data'=>$data1];
            } else {
                return ['success' => false, 'message' => 'Already Register'];
            }
        }else {
            return ['success' => false, 'message' => 'missing parameters'];
        }

    }

    public function actionIndex()
    {
        $request = Yii::$app->request;
        $data=json_decode(Yii::$app->request->getRawBody(), true);
        $name=$data['number'];

        $CustomerList = Customers::find()->where(['cell_phone'=> $name])->one();

        if (!empty($CustomerList)){


            $customer = Customers::findOne($CustomerList['customer_id']);
            $customer->auth_key = $this->generateRandomString(30);
            $customer->save();

            $data1['first_name']=$CustomerList['first_name'];
            $data1['last_name']=$CustomerList['last_name'];
            $data1['email']=$CustomerList['email'];
            $data1['cell_phone']=$CustomerList['cell_phone'];
            $data1['auth_key']=$customer->auth_key;
            $data1['profile_image']=$CustomerList['profile_image'];
            $mes="Already Register";
            $reg=true;
        }else{
            $mes="New User";
            $reg=false;
            $data1['first_name']="";
            $data1['last_name']="";
            $data1['email']="";
            $data1['cell_phone']="";
            $data1['auth_key']="";
            $data1['profile_image']="";
        }

        if (Yii::$app->request->isPost) {
            if (!empty($name)) {
                $code = rand(100000, 999999);
                $sid = "AC4c7a0864ddc18a34f4756dc5302020e3"; // Your Account SID from www.twilio.com/user/account
                $token = "b576410701f8180045c8708dde8a920c";
                $client = new Services_Twilio($sid, $token);
                //require(__DIR__ . '/../../../vendor/Twilio/Services/Twilio.php');
                try {
                    $message = $client->account->messages->sendMessage(
                        '+16175397237', // From a Twilio number in your account
                        $name, // Text any number
                        "Your verification code is " . $code
                    );
                }catch (Exception $e){
                    return ['success' => false, 'message' => "no"];
                }
                return ['success' => true, 'is_registered'=> $reg,'message' => $mes,'authentication_code'=>$code, 'data'=>$data1];
            }else{
                return ['success' => false, 'message' => 'Missing parameters'];
            }
        }else{
            return ['success' => false, 'message' => 'Not a POST request.'];
        }
        die();


        print $message->sid;
    }
    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}


