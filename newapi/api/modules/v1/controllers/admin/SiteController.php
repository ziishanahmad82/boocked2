<?php

namespace api\modules\v1\controllers\admin;

use Yii;
use yii\web\Controller;

use app\models\BusinessInformation;

use api\modules\v1\models\Timezoneandcurrency;






class SiteController extends Controller
{


     public function actionTimezone_currency(){

        $requestedData = Yii::$app->request->post();
        if(isset($requestedData['currency']) && $requestedData['currency']!='' &&  isset($requestedData['timezone']) && $requestedData['timezone']!=''){
            $business_id = $requestedData['business_id']; //Yii::$app->user->identity->business_id;
           // $business_id = Yii::$app->user->identity->business_id;

            $timezone = Timezoneandcurrency::findOne(['business_id'=>$business_id]);
            if(empty($timezone)){
                $timezone = new Timezoneandcurrency();

            }



                $timezone->business_id=$business_id;
                $timezone->currency = $requestedData['currency'];
                $timezone->timezone = $requestedData['timezone'];
                if($timezone->save(false)){
                    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                    return ['success' => true, 'data' =>$timezone];
                }



        }
         \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
         return ['success' => false];


        }




}



