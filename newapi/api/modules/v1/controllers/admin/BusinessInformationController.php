<?php

namespace api\modules\v1\controllers\admin;

use Yii;
use api\modules\v1\models\BusinessInformation;
use app\models\Users;
use app\models\Timezoneandcurrency;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\db\Query;
use app\models\ContactPhone;
use yii\db\ActiveQuery;


/**
 * BusinessInformationController implements the CRUD actions for BusinessInformation model.
 */
class BusinessInformationController extends Controller
{


    /**
     * Lists all BusinessInformation models.
     * @return mixed
     */
    public function actionIndex()
    {

        $business_id = Yii::$app->user->identity->business_id;
        $model = BusinessInformation::findone(['business_id'=>Yii::$app->user->identity->business_id]);

        $timezoneCurrency = Timezoneandcurrency::find()->where(['business_id'=>$business_id])->all();

        $staff_list =  Users::getActiveUsers(Yii::$app->user->identity->business_id);
        $countries  = Countries::find()->all();
        $states = States::find()->where(['country_id'=>$model->country])->all();
        $cities = Cities::find()->where(['state_id'=>$model->state])->all();
        $currency_list = CurrencyList::find()->all();

        $contact_phone = ContactPhone::find()->where(['user_id'=>Yii::$app->user->identity->id])->asArray()->all();
       // $business_information  = BusinessInformation::find()->where(['business_id' => $business_id])->all();
        $business_information  = BusinessInformation::find()->where(['business_id' => $business_id])->one();


        return $this->render('index', [
            'business_information' => $business_information,
            'model'=> $model,
            'timezoneCurrency'=>$timezoneCurrency,
            'contact_phone'=>$contact_phone,
            'staff_list'=>$staff_list,
            'countries'=>$countries,
            'states'=>$states,
            'cities'=>$cities,
            'currency_list'=>$currency_list
        ]);
    }

    /**
     * Displays a single BusinessInformation model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new BusinessInformation model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new BusinessInformation();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->business_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing BusinessInformation model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate()
    {
        $recievedData = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if(isset($recievedData['business_id']) && $recievedData['business_id'] !=''){
           // $model = new BusinessInformation();
            $business_id =  $recievedData['business_id']; //Yii::$app->user->identity->business_id;
            $model = BusinessInformation::findone(['business_id'=>$business_id]);
            $model->business_name = $recievedData['business_name'];
            $model->business_phone = $recievedData['business_phone'];
            $model->business_description = $recievedData['business_description'];
            $model->address = $recievedData['address'];
            $model->country = $recievedData['country'];
            $model->state = $recievedData['state'];
            $model->zip = $recievedData['zip'];
            $model->city = $recievedData['city'];
        }



        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        if(!$model->validate()){

            return ['success' => false, 'data' =>$model->getErrors()];
        }else{
            if($model->save(false)){
                return ['success' => true, 'data' =>$model];
            }
        }
        //Yii::$app->user->identity->business_id;


     //   if ($model->load(Yii::$app->request->post()) && $model->save(false)) {
           // return $this->redirect(['view', 'id' => $model->business_id]);
            echo 1;
     //   } else {


       // }
    }

    /**
     * Deletes an existing BusinessInformation model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }
    public function actionValidate()
    {

        $model = new BusinessInformation();
        $request = \Yii::$app->getRequest();
        if ($request->isPost && $model->load($request->post())) {
            \Yii::$app->response->format = Response::FORMAT_JSON;

        }
        return ActiveForm::validate($model);
    }
    /**
     * Finds the BusinessInformation model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return BusinessInformation the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */




    public function actionAddscedule(){
        $business_id = Yii::$app->user->identity->business_id;
         $services_list = Services::getServicesList($business_id);
       // $staff_list = Users::getActiveUsers($business_id);
        $staff_list = Users::find()->where(['business_id'=>$business_id])->all();
        return $this->renderAjax('add-scedule', [
            'services_list' => $services_list,
            'staff_list'=> $staff_list,
        ]);
        
    }







    protected function findModel($id)
    {
        if (($model = BusinessInformation::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
