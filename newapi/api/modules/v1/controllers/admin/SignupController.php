<?php

namespace api\modules\v1\controllers\admin;
use api\modules\v1\models\BusinessInformation;
use api\modules\v1\models\SignupForm;
use app\models\ChangeEmail;
use app\models\SiteSettings;
use app\models\User;
use app\models\Users;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

use app\models\PasswordResetRequestForm;
use app\models\ResetPasswordForm;


use api\modules\v1\models\ServicesCommonName;
use api\modules\v1\models\StaffCommonName;
use api\modules\v1\models\Countries;
use api\modules\v1\models\ProfessionsList;






class SignupController extends Controller
{




    public function actionIndex()
    {

        $model =  new SignupForm();
        $business_information = new BusinessInformation();
        $recievedData = Yii::$app->request->post();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        $model = new SignupForm();
        $model->email = $recievedData['email'];
        $model->username = $recievedData['username'];
        $model->displayname = $recievedData['displayname'];
        $model->password = $recievedData['password'];
        $model->password_repeat = $recievedData['password_repeat'];
        $model->profession = $recievedData['profession'];



        if(!$model->validate()){
            \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ['success' => false, 'data' =>$model->getErrors()];
        }


      // print_r($model);
       // exit();

        $business_information = new BusinessInformation();

            $model->user_group = 'admin';
        $business_information->profession =  $recievedData['profession'];
        $business_information->country =  $recievedData['country'];

                $business_information->business_subdomain = $model->username;
                if($business_information->save(false)){

                    $business_id  = $business_information->business_id;
                    $model->business_id = $business_information->business_id;

                }
             //   \Yii::$app->user->login($model);
              //  return $this->redirect(['/site/index']);



            if(@$user_id=Yii::$app->request->post('user_id')){

                $user=Users::findOne($user_id);

                return $this->redirect(Yii::getAlias('@web').'/index.php/users/index',302);

            } else {
                if ($user = $model->signup()) {
                    $service_common_model = new ServicesCommonName();
                    $service_common_model->business_id=$business_id;
                    $service_common_model->singular_name='Service';
                    $service_common_model->plural_name='Services';
                    $service_common_model->save();
                    $staff_common_model = new StaffCommonName();
                    $staff_common_model->business_id=$business_id;
                    $staff_common_model->staff_singular='Staff';
                    $staff_common_model->staff_plural='Staffs';
                    $staff_common_model->save();
                    return $user;
                    if (Yii::$app->getUser()->login($user)) {
                    return $this->redirect(Yii::getAlias('@web').'/index.php',302);
                     }
                }
            }


    $countries = Countries::find()->all();
    $profession_list = ProfessionsList::find()->all();
        unset($profession_list[0]);
        unset($profession_list[1]);
    }

    public function actionRoutes()
    {
        return $this->render('routes');
    }

    /**
     * Ajax handler for language change dropdown list. Sets cookie ready for next request
     */
    public function actionLanguage()
    {
        if ( Yii::$app->request->post('_lang') !== NULL && array_key_exists(Yii::$app->request->post('_lang'), Yii::$app->params['languages']))
        {
            Yii::$app->language = Yii::$app->request->post('_lang');
            $cookie = new yii\web\Cookie([
                'name' => '_lang',
                'value' => Yii::$app->request->post('_lang'),
            ]);
            Yii::$app->getResponse()->getCookies()->add($cookie);
        }
        Yii::$app->end();
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $this->layout = 'noheader';
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

   












}
