<?php

namespace api\modules\v1\controllers\admin;
use api\modules\v1\models\BusinessInformation;
use api\modules\v1\models\CurrencyList;
use Yii;

use yii\web\Controller;
use yii\web\JsonParser;

use api\modules\v1\models\Countries;
use api\modules\v1\models\States;
use api\modules\v1\models\Cities;
use api\modules\v1\models\ProfessionsList;







class CommonController extends Controller
{




    public function actionCountries()
    {
    $countries = Countries::find()->all();
    \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
    return ['success' => true, 'data' =>$countries];
    }
    public function actionStates($country_id)
    {
        $states = States::find()->where(['country_id'=>$country_id])->all();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['success' => true, 'data' =>$states];
    }
    public function actionCities($state_id)
    {
        $cities = Cities::find()->where(['state_id'=>$state_id])->all();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['success' => true, 'data' =>$cities];
    }

    public function actionProfessions()
    {
        $profession_list = ProfessionsList::find()->all();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['success' => true, 'data' =>$profession_list];
    }
    public function actionCurrencies()
    {
        $currency_list = CurrencyList::find()->all();
        \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
        return ['success' => true, 'data' =>$currency_list];
    }



   












}
