<?php

namespace api\modules\v1\controllers;


use yii\rest\ActiveController;
use yii\rest\Controller;
use api\modules\v1\models\Category;
use api\modules\v1\models\User;
use api\modules\v1\models\Service;
use api\modules\v1\models\Search;
use api\modules\v1\models\CustomerCategory;
use yii;
use yii\db\Query;
use yii\web\JsonParser;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\CompositeAuth;
use yii\web\Response;
/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class SearchController extends Controller
{
    //public $modelClass = 'app\models\User';
    //public $modelClass = 'api\modules\v1\models\Country';

    /**
     * @return string
     */
    public function behaviors(){
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className()
        ];

        return $behaviors;
    }
    public function actionRecent()
    {
        $data=array();

        $all=Yii::$app->user->identity;
        $customer_id=$all['customer_id'];

        $query = new Query;
        $query
            ->select([
                    'search.*']
            )
            ->from('search')
            ->where(['customer_id'=>$customer_id]);
        $command = $query->createCommand();
        $data['search'] = $command->queryAll();



        $CategoryList = Category::find()->all();
        $data['categories']=array();
        foreach ($CategoryList as $category) {
            $data1['id'] = $category['profession_id'];
            $Service = Service::find()->where(['service_cat_id'=>$data1['id']])->all();

            $data1['name'] = $category['profession_name'];
            $data1['image']="http://boocked.com".$category['image'];
            $data1['pressed_image']="http://boocked.com".$category['image_pressed'];
            $data1['total_services']=count($Service);
            array_push($data['categories'], $data1);
        }
        return ['success' => true, 'data' =>$data];

    }
    public function actionIndex()
    {

        $all=Yii::$app->user->identity;
        $customer_id=$all['customer_id'];
        $alldata = json_decode(Yii::$app->request->getRawBody(), true);
        $search = $alldata['search'];

        if (!empty($search)) {
            $csearch = Search::find()->where(['customer_id' => $customer_id,'search_name'=>$search])->all();
            if (empty($csearch)) {
                $savesearch = new Search();
                $savesearch->customer_id = $customer_id;
                $savesearch->search_name = $search;
                $savesearch->save();
            }
        }



        $service = Service::find()
            ->select('service_id,service_name')
            ->where(['like','concat(service_name,\' \', service_description)',$search])
            ->all();
       /* $CategoryList = Category::find()->where(['pro_parent'=>0])->all();
        $data=array();
        $data2=array();
        foreach ($CategoryList as $category){
            $data1['id']=$category['profession_id'];
            $data1['name']=$category['profession_name'];
            array_push($data,$data1);
        }*/
       /* $service = Service::find()
            ->joinWith('business')
            ->orderBy('services.business_id, business_information.business_id')
            ->where(['featured'=>1])
            ->all();*/
       $sdata=array();
       $sdata['found']=count ( $service );
       $sdata['search']=$service;
        echo $count = count ( $service );
        return ['success' => true, 'data' =>$sdata];

        $query = new Query;
        $query
            ->select([
                        'services.*',
                        'business_information.*']
                        )
            ->from('services')
            ->join('INNER JOIN', 'business_information',
                'services.business_id=business_information.business_id')
            ->where(['service_id'=>$id]);
        $command = $query->createCommand();
        $data5 = $command->queryAll();

       if (!empty($data5)) {
           $staff = User::find()->where(['business_id' => $data5[0]['business_id']])->all();
       }else{
           $staff="";
       }

        $data2['staff']=$staff;
        $data2['service']=$data5;
        return ['success' => true, 'data' =>$data2];
    }
    public function actionInsert()
    {
        $all=Yii::$app->user->identity;
        $data=json_decode(Yii::$app->request->getRawBody(), true);


        //print_r($data);


        if (!empty($data) and !empty($all->customer_id) AND !empty($data['0'])){
            $data1=array();
            foreach ($data as $d) {
                $cust_id = $all->customer_id;
                $cate_id = $d;
                try {
                    $customercategory = new CustomerCategory;
                    $customercategory->customer_id = $cust_id;
                    $customercategory->profession_id = $cate_id;
                    $customercategory->save();
                    $data1[$cate_id]="Added";
                } catch (\Exception $e) {
                    $customercategory = "Already Exit";
                    $data1[$cate_id]="Already Exit";
                }
            }
        }else{
            $customercategory="Missing parameters";
        }

        return ['success' => true, 'data' =>$data1];

    }
}


