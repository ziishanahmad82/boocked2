<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use yii\rest\Controller;
use api\modules\v1\models\Category;
use api\modules\v1\models\Service;
use api\modules\v1\models\ServiceOffer;
use api\modules\v1\models\CustomerCategory;
use yii;
use yii\db\Query;
use yii\web\JsonParser;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\CompositeAuth;
use yii\web\Response;
use DateTime;
/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class HomeController extends Controller
{
    //public $modelClass = 'app\models\User';
    //public $modelClass = 'api\modules\v1\models\Country';

    /**
     * @return string
     */
    public function behaviors(){
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className()
        ];

        return $behaviors;
    }

    public function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    public function actionIndex($id=0)
    {
        $data12 = json_decode(Yii::$app->request->getRawBody(), true);
      //  return $data12;
        $data = array();
        $data2 = array();
        $data10 = array();
        $CategoryList = Category::find()->where(['pro_parent' => $id])->all();
        foreach ($CategoryList as $category) {
            $data1['id'] = $category['profession_id'];
            $data1['name'] = $category['profession_name'];
            $data1['image']="http://boocked.com".$category['image'];
            $data1['pressed_image']="http://boocked.com".$category['image_pressed'];
            array_push($data, $data1);
        }
        if ($id == 0 OR $id==1) {
            $query = new Query;
            $query->select([
                    'services.*',
                    'services.service_id as sid',
                    'business_information.*',
                    //'service_offer.*'
                ]
            )
                ->from('services')
                ->join('INNER JOIN', 'business_information',
                    'services.business_id=business_information.business_id')
               // ->join('LEFT JOIN', 'service_offer',
                 //   'services.service_id=service_offer.service_id')
                ->where(['featured' => 1]);
            if (isset($data12['search_service']) && $data12['search_service']!='' && !empty($data12['search_service'])) {
                $service_search = $data12['search_service'];
                $query->andWhere(['like', 'services.service_name', $service_search]);
            }
            $command = $query->createCommand();
            $data5 = $command->queryAll();
            /*foreach ($data5 as $service5){
                $offfer=1;
                $a = ServiceOffer::find()->where(['service_id' => $service5['sid']])->all();
                $lowdis=0;
                foreach ($a as $item) {
                    if (date("Y-m-d",strtotime($item['offer_end'])) >= date("Y-m-d"))
                    {
                        if ($lowdis==0){
                                $now = new DateTime(date("Y-m-d"));
                                $start = new DateTime($item['offer_start']);
                                $end = new DateTime($item['offer_end']);
                                $RemainingDays = $now->diff($end);
                                $TotalDays = $start->diff($end);
                                $service50["service_off_price_percentage"] = $item["discount"];
                                $service50["service_offer_end_date"] = date("Y-m-d",strtotime($item['offer_end']));
                                $service50["service_offer_end_time"] = "23:59:59";
                                $service50["service_off_price"] = $item["price"];
                                $service50["service_offer_remainingdays"]=$RemainingDays->days;
                                $service50["service_offer_totaldays"]=$TotalDays->days;
                                $service50["service_discount_price"]=$item["discount"];
                                $lowdis=$item["discount"];

                        }elseif($lowdis<$item["discount"]){
                            $now = new DateTime(date("Y-m-d"));
                            $start = new DateTime($item['offer_start']);
                            $end = new DateTime($item['offer_end']);
                            $RemainingDays = $now->diff($end);
                            $TotalDays = $start->diff($end);
                            $service50["service_off_price_percentage"] = $item["discount"];
                            $service50["service_offer_end_date"] = date("Y-m-d",strtotime($item['offer_end']));
                            $service50["service_offer_end_time"] = "23:59:59";
                            $service50["service_off_price"] = $item["price"];
                            $service50["service_offer_remainingdays"]=$RemainingDays->days;
                            $service50["service_offer_totaldays"]=$TotalDays->days;
                            $service50["service_discount_price"]=$item["discount"];
                            $lowdis=$item["discount"];
                        }
                    }
                }

            }*/

            foreach ($data5 as $service5){
                $service50["service_id"]=$service5["sid"];
                $service50["service_name"]=$service5["service_name"];
                $service50["service_description"]=$service5["service_description"];
                $service50["service_duration"]=$service5["service_duration"];
                $service50["service_price"]=$service5["service_price"];
                $service50["service_capacity"]=$service5["service_capacity"];
                $service50["service_status"]=$service5["service_status"];
                $service50["service_cat_id"]=$service5["service_cat_id"];
                $service50["business_id"]=$service5["business_id"];
                $service50["featured"]=$service5["featured"];
                if (empty($service5["service_image"])){
                    $service50["service_image"] = "http://boocked.com/admin/web/uploads/5215service.png";
                }else {
                    $service50["service_image"] = "http://boocked.com/admin/web/".$service5["service_image"];
                }
                $service50["google_address"]=$service5["google_address"];
                $service50["business_name"]=$service5["business_name"];
                $service50["business_description"]=$service5["business_description"];
                $service50["business_phone"]=$service5["business_phone"];
                $service50["profession"]=$service5["profession"];
                $service50["business_subdomain"]=$service5["business_subdomain"];
                $service50["business_status"]=$service5["business_status"];


                ///////////////////////

                $query1 = new Query;
                $query1->select([
                        "COUNT('review_status') as row" ,
                    "AVG(review_status) as 'avg'"
                ])
                    ->from('reviews')
                    ->where(['service_id' => $service5["sid"]]);
                $command1 = $query1->createCommand();
                $data51 = $command1->queryAll();
                if(empty($data51[0]["avg"])){
                    $data51[0]["avg"]=0;
                }
                $service50["service_rating_percentage"]=$data51[0]["avg"];
                $service50["service_rating_percentage_total"]=4;
                $service50["service_rating_count"]=$data51[0]["row"];
                ///////////////////////

                if (!empty($service5['lat']) AND !empty($service5['lng'])) {
                    $service50['distance'] = $this->distance($data12['lat'], $data12['lng'], $service5['lat'], $service5['lng'], "M");
                }else{
                    $service50['distance'] =1000;
                }

                $a = ServiceOffer::find()->where(['service_id' => $service5['sid']])->all();
                $lowdis=0;
                foreach ($a as $item) {
                    if (date("Y-m-d",strtotime($item['offer_end'])) >= date("Y-m-d"))
                    {
                        if ($lowdis==0){
                            $now = new DateTime(date("Y-m-d"));
                            $start = new DateTime($item['offer_start']);
                            $end = new DateTime($item['offer_end']);
                            $RemainingDays = $now->diff($end);
                            $TotalDays = $start->diff($end);
                            $service50["service_off_price_percentage"] = $item["discount"];
                            $service50["service_offer_end_date"] = date("Y-m-d",strtotime($item['offer_end']));
                            $service50["service_offer_end_time"] = "23:59:59";
                            $service50["service_off_price"] = round($service5["service_price"]*((100-$item["discount"])/100),2);
                            $service50["service_offer_remainingdays"]=$RemainingDays->days;
                            $service50["service_offer_totaldays"]=$TotalDays->days;
                            $service50["service_discount_price"]=round($service5["service_price"]*(($item["discount"])/100),2);
                            $lowdis=$item["discount"];

                        }elseif($lowdis<$item["discount"]){
                            $now = new DateTime(date("Y-m-d"));
                            $start = new DateTime($item['offer_start']);
                            $end = new DateTime($item['offer_end']);
                            $RemainingDays = $now->diff($end);
                            $TotalDays = $start->diff($end);
                            $service50["service_off_price_percentage"] = $item["discount"];
                            $service50["service_offer_end_date"] = date("Y-m-d",strtotime($item['offer_end']));
                            $service50["service_offer_end_time"] = "23:59:59";
                            $service50["service_off_price"] = round($service5["service_price"]*((100-$item["discount"])/100),2);
                            $service50["service_offer_remainingdays"]=$RemainingDays->days;
                            $service50["service_offer_totaldays"]=$TotalDays->days;
                            $service50["service_discount_price"]=round($service5["service_price"]*(($item["discount"])/100),2);
                            $lowdis=$item["discount"];
                        }
                    }
                }





                if ($lowdis==0){
                    $service50["service_offer_remainingdays"]=0;
                    $service50["service_offer_totaldays"]=0;
                    $service50["service_discount_price"]=0;
                    $service50["service_off_price"]=0;
                    $service50["service_off_price_percentage"] = 0;
                    $service50["service_offer_end_date"] = 0;
                    $service50["service_offer_end_time"] = 0;
                }
                array_push($data10, $service50);
            }

            //return ['success' => true, 'data' => $data10];

        }else{

            $query = new Query;
            $query->select([
                    'services.*',
                    'services.service_id as sid',
                    'business_information.*',
                    //'service_offer.*'
                ]
            )
                ->from('services')
                ->join('INNER JOIN', 'business_information',
                    'services.business_id=business_information.business_id')
                /*->join('LEFT JOIN', 'service_offer',
                    'services.service_id=service_offer.service_id')*/
                ->where(['profession' => $id]);

            if (isset($data12['search_service']) && $data12['search_service']!='' && !empty($data12['search_service'])) {
                $service_search = $data12['search_service'];
                $query->andWhere(['like', 'services.service_name', $service_search]);
            }
            $command = $query->createCommand();
            $data5 = $command->queryAll();
            foreach ($data5 as $service5){
                $service50["service_id"]=$service5["sid"];
                $service50["service_name"]=$service5["service_name"];
                $service50["service_description"]=$service5["service_description"];
                $service50["service_duration"]=$service5["service_duration"];
                $service50["service_price"]=$service5["service_price"];
                $service50["service_capacity"]=$service5["service_capacity"];
                $service50["service_status"]=$service5["service_status"];
                $service50["service_cat_id"]=$service5["service_cat_id"];
                $service50["business_id"]=$service5["business_id"];
                $service50["featured"]=$service5["featured"];
                if (empty($service5["service_image"])){
                    $service50["service_image"] = "http://boocked.com/admin/web/uploads/5215service.png";
                }else {
                    $service50["service_image"] = "http://boocked.com/admin/web/".$service5["service_image"];
                }

                $service50["google_address"]=$service5["google_address"];
                $service50["business_name"]=$service5["business_name"];
                $service50["business_description"]=$service5["business_description"];
                $service50["business_phone"]=$service5["business_phone"];
                $service50["profession"]=$service5["profession"];
                $service50["business_subdomain"]=$service5["business_subdomain"];
                $service50["business_status"]=$service5["business_status"];
               // $service50["distance"]=$service5["distance"];

                ///////////////////////

                $query1 = new Query;
                $query1->select([
                    "COUNT('review_status') as row" ,
                    "AVG(review_status) as 'avg'"
                ])
                    ->from('reviews')
                    ->where(['service_id' => $service5["sid"]]);
                $command1 = $query1->createCommand();
                $data51 = $command1->queryAll();
                if(empty($data51[0]["avg"])){
                    $data51[0]["avg"]=0;
                }
                $service50["service_rating_percentage"]=$data51[0]["avg"];
                $service50["service_rating_percentage_total"]=4;
                $service50["service_rating_count"]=$data51[0]["row"];
                ///////////////////////

                if (!empty($service5['lat']) AND !empty($service5['lng'])) {
                    $service50['distance'] = $this->distance($data12['lat'], $data12['lng'], $service5['lat'], $service5['lng'], "M");
                }else{
                    $service50['distance'] =1000;
                }
                $a = ServiceOffer::find()->where(['service_id' => $service5['sid']])->all();
                $lowdis=0;
                foreach ($a as $item) {
                    if (date("Y-m-d",strtotime($item['offer_end'])) >= date("Y-m-d"))
                    {
                        if ($lowdis==0){
                            $now = new DateTime(date("Y-m-d"));
                            $start = new DateTime($item['offer_start']);
                            $end = new DateTime($item['offer_end']);
                            $RemainingDays = $now->diff($end);
                            $TotalDays = $start->diff($end);
                            $service50["service_off_price_percentage"] = $item["discount"];
                            $service50["service_offer_end_date"] = date("Y-m-d",strtotime($item['offer_end']));
                            $service50["service_offer_end_time"] = "23:59:59";
                            //$service50["service_off_price"] = $service5["service_price"]-$item["discount"];

                            $service50["service_off_price"] = $service5["service_price"]*((100-$item["discount"])/100);

                            $service50["service_offer_remainingdays"]=$RemainingDays->days;
                            $service50["service_offer_totaldays"]=$TotalDays->days;
                            //$service50["service_discount_price"]=$item["price"];
                            $service50["service_discount_price"]=$service5["service_price"]*(($item["discount"])/100);
                            $lowdis=$item["discount"];

                        }elseif($lowdis<$item["discount"]){
                            $now = new DateTime(date("Y-m-d"));
                            $start = new DateTime($item['offer_start']);
                            $end = new DateTime($item['offer_end']);
                            $RemainingDays = $now->diff($end);
                            $TotalDays = $start->diff($end);
                            $service50["service_off_price_percentage"] = $item["discount"];
                            $service50["service_offer_end_date"] = date("Y-m-d",strtotime($item['offer_end']));
                            $service50["service_offer_end_time"] = "23:59:59";
                            $service50["service_off_price"] = $service5["service_price"]*((100-$item["discount"])/100);
                            $service50["service_offer_remainingdays"]=$RemainingDays->days;
                            $service50["service_offer_totaldays"]=$TotalDays->days;
                            $service50["service_discount_price"]=$service5["service_price"]*(($item["discount"])/100);
                            $lowdis=$item["discount"];
                        }
                    }
                }





                if ($lowdis==0){
                    $service50["service_offer_remainingdays"]=0;
                    $service50["service_offer_totaldays"]=0;
                    $service50["service_discount_price"]=0;
                    $service50["service_off_price"]=0;
                    $service50["service_off_price_percentage"] = 0;
                    $service50["service_offer_end_date"] = 0;
                    $service50["service_offer_end_time"] = 0;
                }
                array_push($data10, $service50);
            }

        }

        usort($data10, function ($a, $b) { return strnatcmp($a['distance'], $b['distance']); });
            $data2['categories'] = $data;
            $data2['services'] = $data10;
        //print_r($data10);
            return ['success' => true, 'data' => $data2];

    }
    public function actionInsert()
    {
        $all=Yii::$app->user->identity;
        $data=json_decode(Yii::$app->request->getRawBody(), true);


        //print_r($data);


        if (!empty($data) and !empty($all->customer_id) AND !empty($data['0'])){
            $data1=array();
            foreach ($data as $d) {
                $cust_id = $all->customer_id;
                $cate_id = $d;
                try {
                    $customercategory = new CustomerCategory;
                    $customercategory->customer_id = $cust_id;
                    $customercategory->profession_id = $cate_id;
                    $customercategory->save();
                    $data1[$cate_id]="Added";
                } catch (\Exception $e) {
                    $customercategory = "Already Exit";
                    $data1[$cate_id]="Already Exit";
                }
            }
        }else{
            $customercategory="Missing parameters";
        }

        return ['success' => true, 'data' =>$data1];

    }

}


