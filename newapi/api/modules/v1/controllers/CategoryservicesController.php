<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use yii\rest\Controller;
use api\modules\v1\models\Category;
use api\modules\v1\models\Service;
use api\modules\v1\models\CustomerCategory;
use yii;
use yii\db\Query;
use yii\web\JsonParser;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\CompositeAuth;
use yii\web\Response;
use DateTime;
/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class CategoryservicesController extends Controller
{
    //public $modelClass = 'app\models\User';
    //public $modelClass = 'api\modules\v1\models\Country';

    /**
     * @return string
     */
    /*public function behaviors(){
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className()
        ];

        return $behaviors;
    }*/
    public function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    public function actionIndex()
    {

        $data12 = json_decode(Yii::$app->request->getRawBody(), true);


        if (isset($data12['id']) && isset($data12['lng']) && isset($data12['lat'])) {
          $id = $data12['id'];

            $data = array();
            $data2 = array();
            $data10 = array();
            $CategoryList = Category::find()->where(['pro_parent' => $id])->all();
            foreach ($CategoryList as $category) {
                $data1['id'] = $category['profession_id'];
                $data1['name'] = $category['profession_name'];
                $data1['image'] = "http://boocked.com" . $category['image'];
                $data1['pressed_image'] = "http://boocked.com" . $category['image_pressed'];
                array_push($data, $data1);
            }
            $query = new Query;

            if ($id==1 || $id==0){
                $query->select([
                        'services.*',
                        'services.service_id as sid',
                        'business_information.*',
                        'service_offer.*']
                )
                    ->from('services')
                    ->join('INNER JOIN', 'business_information',
                        'services.business_id=business_information.business_id')
                    ->join('LEFT JOIN', 'service_offer',
                        'services.service_id=service_offer.service_id');
                    if($id==1) {
                        $query->where(['featured' => $id]);
                    }
                if (isset($data12['search_keyword']) && $data12['search_keyword']!='' && !empty($data12['search_keyword'])) {
                    $service_search = $data12['search_keyword'];
                    $query->andWhere(['like', 'services.service_name', $service_search]);
                }
            }else {
                $query->select([
                        'services.*',
                        'services.service_id as sid',
                        'business_information.*',
                        'service_offer.*']
                )
                    ->from('services')
                    ->join('INNER JOIN', 'business_information',
                        'services.business_id=business_information.business_id')
                    ->join('LEFT JOIN', 'service_offer',
                        'services.service_id=service_offer.service_id')
               ;//     ->where(['profession' => $id]);
                if (isset($data12['search_keyword']) && $data12['search_keyword']!='' && !empty($data12['search_keyword'])) {
                    $service_search = $data12['search_keyword'];
                    $query->andWhere(['like', 'services.service_name', $service_search]);
                }
            }
            $command = $query->createCommand();
            $data5 = $command->queryAll();
          
            foreach ($data5 as $service5) {
                $service50["service_id"] = $service5["sid"];
                $service50["service_name"] = $service5["service_name"];
                $service50["service_description"] = $service5["service_description"];
                $service50["service_duration"] = $service5["service_duration"];
                $service50["service_price"] = $service5["service_price"];
                $service50["service_capacity"] = $service5["service_capacity"];
                $service50["service_status"] = $service5["service_status"];
                $service50["service_cat_id"] = $service5["service_cat_id"];
                $service50["business_id"] = $service5["business_id"];
                $service50["featured"] = $service5["featured"];
                if (empty($service5["service_image"])) {
                    $service50["service_image"] = "http://boocked.com/admin/web/uploads/5215service.png";
                } else {
                    $service50["service_image"] = "http://boocked.com/admin/web/" . $service5["service_image"];
                }

                $service50["google_address"] = $service5["google_address"];
                $service50["business_name"] = $service5["business_name"];
                $service50["business_description"] = $service5["business_description"];
                $service50["business_phone"] = $service5["business_phone"];
                $service50["profession"] = $service5["profession"];
                $service50["business_subdomain"] = $service5["business_subdomain"];
                $service50["business_status"] = $service5["business_status"];
                // $service50["distance"]=$service5["distance"];

                ///////////////////////

                $query1 = new Query;
                $query1->select([
                    "COUNT('review_status') as row",
                    "AVG(review_status) as 'avg'"
                ])
                    ->from('reviews')
                    ->where(['service_id' => $service5["sid"]]);
                $command1 = $query1->createCommand();
                $data51 = $command1->queryAll();
                if (empty($data51[0]["avg"])) {
                    $data51[0]["avg"] = 0;
                }
                $service50["service_rating_percentage"] = $data51[0]["avg"];
                $service50["service_rating_percentage_total"] = 4;
                $service50["service_rating_count"] = $data51[0]["row"];
                ///////////////////////

                if (!empty($service5['lat']) AND !empty($service5['lng'])) {
                    $service50['distance'] = $this->distance($data12['lat'], $data12['lng'], $service5['lat'], $service5['lng'], "M");
                } else {
                    $service50['distance'] = 1000;
                }
                if ($service5['offer_start'] != "" AND $service5['offer_end'] != "") {
                    $now = new DateTime(date("Y-m-d"));
                    $start = new DateTime($service5['offer_start']);
                    $end = new DateTime($service5['offer_end']);
                    if ($end >= $now) {
                        $RemainingDays = $now->diff($end);
                        $TotalDays = $start->diff($end);
                        $service50["service_offer_remainingdays"] = $RemainingDays->days;
                        $service50["service_offer_totaldays"] = $TotalDays->days;

                        $service50["service_discount_price"] = $service5["discount"];
                        $service50["service_off_price"] = round($service5["service_price"]*((100-$service5["discount"])/100),2);
                        $service50["service_off_price_percentage"] = $service5["discount"];
                        $showoffer=$service5["discount"];
                        $service50["service_offer_end_date"] = date("Y-m-d",strtotime($service5['offer_end']));
                        $service50["service_offer_end_time"] = "23:59:59";
                        //$service50["service_off_price"] = $service5["service_price"]-$service5["discount"];


                    } else {
                        $service50["service_offer_remainingdays"] = 0;
                        $service50["service_offer_totaldays"] = 0;
                        $service50["service_discount_price"] = 0;
                        $service50["service_off_price"] = 0;
                        $service50["service_off_price_percentage"] = 0;
                        $service50["service_offer_end_date"] = 0;
                        $service50["service_offer_end_time"] = 0;
                        $service50["service_off_price"] = 0;
                    }
                } else {
                    $service50["service_offer_remainingdays"] = 0;
                    $service50["service_offer_totaldays"] = 0;
                    $service50["service_discount_price"] = 0;
                    $service50["service_off_price"] = 0;
                    $service50["service_off_price_percentage"] = 0;
                    $service50["service_offer_end_date"] = 0;
                    $service50["service_offer_end_time"] = 0;
                    $service50["service_off_price"] = 0;
                }
                array_push($data10, $service50);
            }


            usort($data10, function ($a, $b) {
                return strnatcmp($a['distance'], $b['distance']);
            });
            //$data2['categories'] = $data;
            $data2['services'] = $data10;
            return ['success' => true, 'data' => $data2];
        }
    }

}


