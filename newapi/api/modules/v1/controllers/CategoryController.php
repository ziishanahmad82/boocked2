<?php

namespace api\modules\v1\controllers;

use yii\rest\ActiveController;
use yii\rest\Controller;
use api\modules\v1\models\Category;
use api\modules\v1\models\CustomerCategory;
use yii;

use yii\web\JsonParser;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\CompositeAuth;
use yii\web\Response;
/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class CategoryController extends Controller
{
    //public $modelClass = 'app\models\User';
    //public $modelClass = 'api\modules\v1\models\Country';

    /**
     * @return string
     */
    public function behaviors(){
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className()
        ];

        return $behaviors;
    }
    public function actionIndex()
    {

        $CategoryList = Category::find()->where(['pro_parent'=>0])->all();
        $data=array();
        $data1['id'] = 0;
        $data1['name']='All Categories';
        $data1['image']="";
        $data1['pressed_image']="";
        array_push($data,$data1);
        foreach ($CategoryList as $category){
            $data1['id']=$category['profession_id'];
            $data1['name']=$category['profession_name'];
            $data1['image']="http://boocked.com".$category['image'];
            $data1['pressed_image']="http://boocked.com".$category['image_pressed'];
            array_push($data,$data1);
        }
        return ['success' => true, 'data' =>$data];
    }
    public function actionInsert()
    {
        $all=Yii::$app->user->identity;
        $data=json_decode(Yii::$app->request->getRawBody(), true);


        //print_r($data);


        if (!empty($data) and !empty($all->customer_id) AND !empty($data['0'])){
            $data1=array();
            foreach ($data as $d) {
                $cust_id = $all->customer_id;
                $cate_id = $d;
                try {
                    $customercategory = new CustomerCategory;
                    $customercategory->customer_id = $cust_id;
                    $customercategory->profession_id = $cate_id;
                    $customercategory->save();
                    $data1[$cate_id]="Added";
                } catch (\Exception $e) {
                    $customercategory = "Already Exit";
                    $data1[$cate_id]="Already Exit";
                }
            }
        }else{
            $customercategory="Missing parameters";
        }

        return ['success' => true, 'data' =>$data1];

    }
}


