<?php

namespace api\modules\v1\controllers;


use yii\rest\Controller;
use api\modules\v1\models\Customers;
use common\models\User;
use api\modules\v1\models\CustomersSearch;
use yii;
use Twilio;
use Services_Twilio;
use yii\web\JsonParser;
use yii\filters\auth\CompositeAuth;
use yii\filters\auth\HttpBasicAuth;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\auth\QueryParamAuth;
use yii\helpers;
use yii\base\Security;


/**
 * Country Controller API
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class NewaccountController extends Controller
{


    public function actionNewaccount()
    {

        $request = Yii::$app->request;
//$data=json_decode(Yii::$app->request->getRawBody(), true);
        $uploads = \yii\web\UploadedFile::getInstanceByName('upfile');
        $img_link="";
        if (!empty($uploads)) {
            $img = 'uploads/' . date("Y-m-d_H_i_s.") . $uploads->name;
            $uploads->saveAs($img);
            $img_link = "/newapi/api/web/" . $img;
        }

        $name = $request->post('number');
        if (!empty($name) AND !empty($request->post('email')) AND !empty($request->post('first')) AND !empty($request->post('last'))) {

            $CustomerList = Customers::find()->where(['like', 'cell_phone', $name])->all();
            $data1 = array();
            if (empty($CustomerList)) {
                $customers = new Customers;
                $customers->cell_phone = $name;
                $customers->email = $request->post('email');
                $customers->first_name = $request->post('first');
                $customers->last_name = $request->post('last');
                $customers->profile_image = $img_link;
                $customers->auth_key = $this->generateRandomString(30);
                $customers->save();
                $CustomerList = Customers::find()->where(['cell_phone' => $name])->one();

                if (!empty($CustomerList)) {

                    $data1['first_name'] = $CustomerList['first_name'];
                    $data1['last_name'] = $CustomerList['last_name'];
                    $data1['email'] = $CustomerList['email'];
                    $data1['cell_phone'] = $CustomerList['cell_phone'];
                    $data1['auth_key'] = $CustomerList['auth_key'];
                    $data1['profile_image'] = $CustomerList['profile_image'];

                } else {
                    $data1 = array();
                }
                return ['success' => true,'is_registered'=>true, 'message' => 'Successfully Register', 'data' => $data1];
            } else {
                return ['success' => true,'is_registered'=>false, 'message' => 'Already Register', 'data' => $data1];
            }
        } else {
            return ['success' => false, 'message' => 'missing parameters'];
        }

    }
    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@#$%^&*()';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

