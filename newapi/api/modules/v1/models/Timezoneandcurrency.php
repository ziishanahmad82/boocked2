<?php

namespace api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "timezoneandcurrency".
 *
 * @property integer $tnc_id
 * @property string $timezone
 * @property string $timeformat
 * @property string $dateformat
 * @property string $currency
 * @property string $currencyformat
 * @property integer $business_id
 */
class Timezoneandcurrency extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'timezoneandcurrency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['timezone', 'timeformat', 'dateformat', 'currency', 'currencyformat', 'business_id'], 'required'],
            [['business_id'], 'integer'],
            [['timezone', 'timeformat', 'dateformat', 'currency', 'currencyformat'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tnc_id' => 'Tnc ID',
            'timezone' => 'Timezone',
            'timeformat' => 'Timeformat',
            'dateformat' => 'Dateformat',
            'currency' => 'Currency',
            'currencyformat' => 'Currencyformat',
            'business_id' => 'Business ID',
        ];
    }
}
