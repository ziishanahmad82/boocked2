<?php

namespace api\modules\v1\models;

use Yii;

/**
 * This is the model class for table "staff_common_name".
 *
 * @property integer $stf_id
 * @property string $staff_singular
 * @property string $staff_plural
 * @property integer $business_id
 */
class StaffCommonName extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'staff_common_name';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['staff_singular', 'staff_plural', 'business_id'], 'required'],
            [['business_id'], 'integer'],
            [['staff_singular', 'staff_plural'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'stf_id' => 'Stf ID',
            'staff_singular' => 'Staff Singular',
            'staff_plural' => 'Staff Plural',
            'business_id' => 'Business ID',
        ];
    }
    public function commonRelatedNames()
    {
        $business_id = Yii::$app->user->identity->business_id;
        $modele = StaffCommonName::find()
            ->where(['business_id' => $business_id])
            ->one();

        return [ $modele
        ];
    }
}
