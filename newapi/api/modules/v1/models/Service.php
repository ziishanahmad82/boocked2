<?php
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;

/**
 * Country Model
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class Service extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'services';
    }

    public function getbusiness() {
        return  $this->hasOne(Business::className(), ['business_id' => 'business_id']);
    }
    public function relations()
    {
        return array(
            'Business'=>array(self::HAS_ONE, 'Business', 'business_id'),
            'WeeklyRecurringTime'=>array(self::HAS_ONE, 'WeeklyRecurringTime', 'service_id'),
        );
    }
    public static function servicebusiness()
    {
        $service = Service::find()
            ->select('business_information.*')
            ->join('INNER JOIN','business_information', 'business_information.business_id=services.business_id')
            ->where(['featured'=>1])
            ->all();
        return $service;
    }



}