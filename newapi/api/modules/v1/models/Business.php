<?php
namespace api\modules\v1\models;
use \yii\db\ActiveRecord;
/**
 * Country Model
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class Business extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'business_information';
    }

    public function attributeLabels()
    {
        return [
            'business_id' => 'Business ID',
            'business_name' => 'Business Name',
            'business_description' => 'Business Description',
            'address' => 'Address',
            'country' => 'Country',
            'state' => 'State',
            'city' => 'City',
            'zip' => 'Zip',
            'logo' => 'Logo',
            'business_phone' => 'Business Phone',
            'profession' => 'Profession',
            'business_status' => 'Business Status',
        ];
    }
    public function relations()
    {
        return array(
            'service'=>array(self::BELONGS_TO, 'Service', 'business_id'),

        );
    }

    public static function servicebusiness()
    {
        $service = Service::find()
            ->select('business_information.*')
            ->join('INNER JOIN','business_information', 'business_information.business_id=services.business_id')
            ->where(['featured'=>1])
            ->all();
        return $service;
    }



}