<?php
namespace api\modules\v1\models;
use api\modules\v1\Module;
use yii\base\Model;
use \yii\db\ActiveRecord;
/**
 * Country Model
 *
 * @author Budi Irawan <deerawan@gmail.com>
 */
class Sms extends Model
{
   // public $sms;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'countries';
    }

    public function rules()
    {
        return [
            [['number'], 'required'],
        ];
    }


}