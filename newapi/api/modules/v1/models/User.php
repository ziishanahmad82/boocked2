<?php

namespace api\modules\v1\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use api\modules\v1\Module;
use yii\base\Model;
use \yii\db\ActiveRecord;

/**
 * This is the model class for table "customers".
 *
 * @property integer $customer_id
 * @property string $learners_permit_number
 * @property string $first_name
 * @property string $middle_name
 * @property string $last_name
 * @property string $date_of_birth
 * @property string $email
 * @property string $cell_phone
 * @property string $test_type
 * @property string $payment_terms
 * @property string $status
 * @property integer $confirmed
 * @property integer $third_user_id
 *
 * @property CustomerReservedTimeslots[] $customerReservedTimeslots
 */
class User extends \yii\db\ActiveRecord
{
    

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return static::findOne(['auth_key' => $token]);
    }

    public static function tableName()
    {
        return 'user';
    }
    public static function primaryKey()
    {
        return ['id'];
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['displayname', 'username', 'email'], 'required'],
        ];
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}
