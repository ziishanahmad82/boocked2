<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'modules' => [
        'v1' => [
            'basePath' => '@app/modules/v1',
            'class' => 'api\modules\v1\Module'
        ]
    ],
    'components' => [
        'user' => [
            'identityClass' => 'common\models\User',
            // 'enableSession' => false,
            'loginUrl' => null,
        ],
        'request' => [

            'enableCookieValidation' => false,

            'enableCsrfValidation' => false,

        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'enableStrictParsing' => true,
            'showScriptName' => false,
            'rules' => [
                [
                    'class' => 'yii\rest\UrlRule',
                     'controller' => 'v1/country',
                    /*'controller'    => [ 'v1/country' ],
                    'extraPatterns' => [
                        'GET new' => 'index'
                    ],*/
                    'tokens' => [
                        '{id}' => '<id:\\w+>'
                    ]

                ]/*,
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/country',
                    'only' => [ 'new' ],
                ]*/,[
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [ 'v1/verifyemail' ],
                    'extraPatterns' => [
                        'POST' => 'verifyemail'
                    ],
                    'pluralize'     => false
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [ 'v1/newaccount' ],
                    'extraPatterns' => [
                        'POST' => 'newaccount',
                    ],
                    'pluralize'     => false
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [ 'v1/updateaccount' ],
                    'extraPatterns' => [
                        'POST' => 'index',
                    ],
                    'pluralize'     => false
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [ 'v1/sms' ],
                    'extraPatterns' => [
                        'POST' => 'index',
                    ],
                    'pluralize'     => false
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [ 'v1/home' ],
                    'extraPatterns' => [
                        'POST' => 'index',
                    ],
                    'pluralize'     => false
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [ 'v1/booking' ],
                    'extraPatterns' => [
                        'POST' => 'index',
                        'POST delete' => 'delete',
                    ],
                    'pluralize'     => false
                ],[
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [ 'v1/categoryservices' ],
                    'extraPatterns' => [
                        'POST' => 'index',
                    ],
                    'pluralize'     => false
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [ 'v1/service' ],
                    'extraPatterns' => [
                        'POST' => 'index',
                    ],
                    'pluralize'     => false
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [ 'v1/staff' ],
                    'extraPatterns' => [
                        'POST' => 'index',
                    ],
                    'pluralize'     => false
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [ 'v1/offers' ],
                    'extraPatterns' => [
                        'POST' => 'index',
                    ],
                    'pluralize'     => false
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [ 'v1/myschedule' ],
                    'extraPatterns' => [
                        'POST' => 'index',
                        'POST delete' => 'delete',
                        'POST update' => 'update',
                    ],
                    'pluralize'     => false
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [ 'v1/checkavailbility' ],
                    'extraPatterns' => [
                        'POST' => 'index',
                        'POST slot' => 'dateslots',
                        'POST timeslot' => 'timeslots',
                    ],
                    'pluralize'     => false
                ],
                [
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [ 'v1/search' ],
                    'extraPatterns' => [
                        'POST' => 'index',
                        'POST recent'=> 'recent'
                    ],
                    'pluralize'     => false
                ],[
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [ 'v1/deleteuser' ],
                    'extraPatterns' => [
                        'GET' => 'index',
                    ],
                    'pluralize'     => false
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/category',
                    'extraPatterns' => [
                        'GET' => 'index',
                        'POST add' => 'insert',
                    ],
                    'pluralize'     => false

                ], [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/category',
                    'only' => [ 'add', ],
                    'pluralize' => false,
                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => 'v1/admin/signup',
                    'extraPatterns' => [
                        'POST' => 'index',
                    ],
                    'pluralize'     => false

                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/admin/common'],
                    'extraPatterns' => [
                        'GET countries' => 'countries',
                        'GET states' => 'states',
                        'GET cities' => 'cities',
                        'GET professions' => 'professions',
                         'GET currencies' => 'currencies'
                    ],
                    'pluralize'     => false

                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/admin/site'],
                    'extraPatterns' => [
                        'POST timezone_currency' => 'timezone_currency',
                    ],
                    'pluralize'     => false

                ],
                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/admin/business-information'],
                    'extraPatterns' => [
                        'POST update' => 'update',
                    ],
                    'pluralize'     => false

                ],
            ],
        ]
    ],
    'params' => $params,
];



